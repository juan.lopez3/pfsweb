<?php namespace Bootstrap;

use Monolog\Logger as Monolog;
use Illuminate\Log\Writer;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Foundation\Bootstrap\ConfigureLogging as BaseConfigureLogging;
use Monolog\Handler\StreamHandler;

class ConfigureLogging extends BaseConfigureLogging
{

    /**
     * OVERRIDE PARENT
     * Configure the Monolog handlers for the application.
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     * @param  \Illuminate\Log\Writer  $log
     * @return void
     */
    protected function configureDailyHandler(Application $app, Writer $log)
    {
        // Stream handlers

		$bubble = false;
		$first_path = storage_path("/logs/".php_sapi_name()."_".get_current_user()."_".date("Y_m_d"));
        $infoStreamHandler    = new StreamHandler($first_path."_laravel_info.log", Monolog::INFO, $bubble);
        $warningStreamHandler = new StreamHandler($first_path."_laravel_warning.log", Monolog::WARNING, $bubble);
        $errorStreamHandler   = new StreamHandler($first_path."_laravel_error.log", Monolog::ERROR, $bubble);

        // push handlers
        $monolog = $log->getMonolog();
        $monolog->pushHandler($infoStreamHandler);
        $monolog->pushHandler($warningStreamHandler);
        $monolog->pushHandler($errorStreamHandler);
    }
}
