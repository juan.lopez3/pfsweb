<?php return array (
  'codeToName' => 
  array (
    32 => 'space',
    33 => 'exclam',
    34 => 'quotedbl',
    35 => 'numbersign',
    36 => 'dollar',
    37 => 'percent',
    38 => 'ampersand',
    39 => 'quotesingle',
    40 => 'parenleft',
    41 => 'parenright',
    42 => 'asterisk',
    43 => 'plus',
    44 => 'comma',
    45 => 'hyphen',
    46 => 'period',
    47 => 'slash',
    48 => 'zero',
    49 => 'one',
    50 => 'two',
    51 => 'three',
    52 => 'four',
    53 => 'five',
    54 => 'six',
    55 => 'seven',
    56 => 'eight',
    57 => 'nine',
    58 => 'colon',
    59 => 'semicolon',
    60 => 'less',
    61 => 'equal',
    62 => 'greater',
    63 => 'question',
    64 => 'at',
    65 => 'A',
    66 => 'B',
    67 => 'C',
    68 => 'D',
    69 => 'E',
    70 => 'F',
    71 => 'G',
    72 => 'H',
    73 => 'I',
    74 => 'J',
    75 => 'K',
    76 => 'L',
    77 => 'M',
    78 => 'N',
    79 => 'O',
    80 => 'P',
    81 => 'Q',
    82 => 'R',
    83 => 'S',
    84 => 'T',
    85 => 'U',
    86 => 'V',
    87 => 'W',
    88 => 'X',
    89 => 'Y',
    90 => 'Z',
    91 => 'bracketleft',
    92 => 'backslash',
    93 => 'bracketright',
    94 => 'asciicircum',
    95 => 'underscore',
    96 => 'grave',
    97 => 'a',
    98 => 'b',
    99 => 'c',
    100 => 'd',
    101 => 'e',
    102 => 'f',
    103 => 'g',
    104 => 'h',
    105 => 'i',
    106 => 'j',
    107 => 'k',
    108 => 'l',
    109 => 'm',
    110 => 'n',
    111 => 'o',
    112 => 'p',
    113 => 'q',
    114 => 'r',
    115 => 's',
    116 => 't',
    117 => 'u',
    118 => 'v',
    119 => 'w',
    120 => 'x',
    121 => 'y',
    122 => 'z',
    123 => 'braceleft',
    124 => 'bar',
    125 => 'braceright',
    126 => 'asciitilde',
    128 => 'uni20AC',
    130 => 'quotesinglbase',
    131 => 'florin',
    132 => 'quotedblbase',
    133 => 'ellipsis',
    134 => 'dagger',
    135 => 'daggerdbl',
    136 => 'circumflex',
    137 => 'perthousand',
    138 => 'Scaron',
    139 => 'guilsinglleft',
    140 => 'OE',
    142 => 'Zcaron',
    145 => 'quoteleft',
    146 => 'quoteright',
    147 => 'quotedblleft',
    148 => 'quotedblright',
    149 => 'bullet',
    150 => 'endash',
    151 => 'emdash',
    152 => 'tilde',
    153 => 'trademark',
    154 => 'scaron',
    155 => 'guilsinglright',
    156 => 'oe',
    158 => 'zcaron',
    159 => 'Ydieresis',
    160 => 'space',
    161 => 'exclamdown',
    162 => 'cent',
    163 => 'sterling',
    164 => 'currency',
    165 => 'yen',
    166 => 'brokenbar',
    167 => 'section',
    168 => 'dieresis',
    169 => 'copyright',
    170 => 'ordfeminine',
    171 => 'guillemotleft',
    172 => 'logicalnot',
    173 => 'hyphen',
    174 => 'registered',
    175 => 'overscore',
    176 => 'degree',
    177 => 'plusminus',
    178 => 'twosuperior',
    179 => 'threesuperior',
    180 => 'acute',
    181 => 'mu1',
    182 => 'paragraph',
    183 => 'middot',
    184 => 'cedilla',
    185 => 'onesuperior',
    186 => 'ordmasculine',
    187 => 'guillemotright',
    188 => 'onequarter',
    189 => 'onehalf',
    190 => 'threequarters',
    191 => 'questiondown',
    192 => 'Agrave',
    193 => 'Aacute',
    194 => 'Acircumflex',
    195 => 'Atilde',
    196 => 'Adieresis',
    197 => 'Aring',
    198 => 'AE',
    199 => 'Ccedilla',
    200 => 'Egrave',
    201 => 'Eacute',
    202 => 'Ecircumflex',
    203 => 'Edieresis',
    204 => 'Igrave',
    205 => 'Iacute',
    206 => 'Icircumflex',
    207 => 'Idieresis',
    208 => 'Eth',
    209 => 'Ntilde',
    210 => 'Ograve',
    211 => 'Oacute',
    212 => 'Ocircumflex',
    213 => 'Otilde',
    214 => 'Odieresis',
    215 => 'multiply',
    216 => 'Oslash',
    217 => 'Ugrave',
    218 => 'Uacute',
    219 => 'Ucircumflex',
    220 => 'Udieresis',
    221 => 'Yacute',
    222 => 'Thorn',
    223 => 'germandbls',
    224 => 'agrave',
    225 => 'aacute',
    226 => 'acircumflex',
    227 => 'atilde',
    228 => 'adieresis',
    229 => 'aring',
    230 => 'ae',
    231 => 'ccedilla',
    232 => 'egrave',
    233 => 'eacute',
    234 => 'ecircumflex',
    235 => 'edieresis',
    236 => 'igrave',
    237 => 'iacute',
    238 => 'icircumflex',
    239 => 'idieresis',
    240 => 'eth',
    241 => 'ntilde',
    242 => 'ograve',
    243 => 'oacute',
    244 => 'ocircumflex',
    245 => 'otilde',
    246 => 'odieresis',
    247 => 'divide',
    248 => 'oslash',
    249 => 'ugrave',
    250 => 'uacute',
    251 => 'ucircumflex',
    252 => 'udieresis',
    253 => 'yacute',
    254 => 'thorn',
    255 => 'ydieresis',
  ),
  'isUnicode' => false,
  'FullName' => 'Arial',
  'FontName' => 'ArialMT',
  'FamilyName' => 'Arial',
  'Weight' => 'Normal',
  'IsFixedPitch' => 'false',
  'ItalicAngle' => '0.00',
  'FontBBox' => 
  array (
    0 => '-222',
    1 => '-212',
    2 => '2028',
    3 => '905',
  ),
  'Ascender' => '728',
  'Descender' => '-210',
  'XHeight' => '519',
  'CapHeight' => '716',
  'UnderlinePosition' => '-292',
  'UnderlineThickness' => '150',
  'EncodingScheme' => 'FontSpecific',
  'StartCharMetrics' => '865',
  'C' => 
  array (
    32 => 278,
    33 => 278,
    34 => 355,
    35 => 556,
    36 => 556,
    37 => 889,
    38 => 667,
    39 => 191,
    40 => 333,
    41 => 333,
    42 => 389,
    43 => 584,
    44 => 278,
    45 => 333,
    46 => 278,
    47 => 278,
    48 => 556,
    49 => 556,
    50 => 556,
    51 => 556,
    52 => 556,
    53 => 556,
    54 => 556,
    55 => 556,
    56 => 556,
    57 => 556,
    58 => 278,
    59 => 278,
    60 => 584,
    61 => 584,
    62 => 584,
    63 => 556,
    64 => 1015,
    65 => 667,
    66 => 667,
    67 => 722,
    68 => 722,
    69 => 667,
    70 => 611,
    71 => 778,
    72 => 722,
    73 => 278,
    74 => 500,
    75 => 667,
    76 => 556,
    77 => 833,
    78 => 722,
    79 => 778,
    80 => 667,
    81 => 778,
    82 => 722,
    83 => 667,
    84 => 611,
    85 => 722,
    86 => 667,
    87 => 944,
    88 => 667,
    89 => 667,
    90 => 611,
    91 => 278,
    92 => 278,
    93 => 278,
    94 => 469,
    95 => 556,
    96 => 333,
    97 => 556,
    98 => 556,
    99 => 500,
    100 => 556,
    101 => 556,
    102 => 278,
    103 => 556,
    104 => 556,
    105 => 222,
    106 => 222,
    107 => 500,
    108 => 222,
    109 => 833,
    110 => 556,
    111 => 556,
    112 => 556,
    113 => 556,
    114 => 333,
    115 => 500,
    116 => 278,
    117 => 556,
    118 => 500,
    119 => 722,
    120 => 500,
    121 => 500,
    122 => 500,
    123 => 334,
    124 => 260,
    125 => 334,
    126 => 584,
    128 => 556,
    130 => 222,
    131 => 556,
    132 => 333,
    133 => 1000,
    134 => 556,
    135 => 556,
    136 => 333,
    137 => 1000,
    138 => 667,
    139 => 333,
    140 => 1000,
    142 => 611,
    145 => 222,
    146 => 222,
    147 => 333,
    148 => 333,
    149 => 350,
    150 => 556,
    151 => 1000,
    152 => 333,
    153 => 1000,
    154 => 500,
    155 => 333,
    156 => 944,
    158 => 500,
    159 => 667,
    160 => 278,
    161 => 333,
    162 => 556,
    163 => 556,
    164 => 556,
    165 => 556,
    166 => 260,
    167 => 556,
    168 => 333,
    169 => 737,
    170 => 370,
    171 => 556,
    172 => 584,
    173 => 333,
    174 => 737,
    175 => 333,
    176 => 400,
    177 => 549,
    178 => 333,
    179 => 333,
    180 => 333,
    181 => 576,
    182 => 537,
    183 => 278,
    184 => 333,
    185 => 333,
    186 => 365,
    187 => 556,
    188 => 834,
    189 => 834,
    190 => 834,
    191 => 611,
    192 => 667,
    193 => 667,
    194 => 667,
    195 => 667,
    196 => 667,
    197 => 667,
    198 => 1000,
    199 => 722,
    200 => 667,
    201 => 667,
    202 => 667,
    203 => 667,
    204 => 278,
    205 => 278,
    206 => 278,
    207 => 278,
    208 => 722,
    209 => 722,
    210 => 778,
    211 => 778,
    212 => 778,
    213 => 778,
    214 => 778,
    215 => 584,
    216 => 778,
    217 => 722,
    218 => 722,
    219 => 722,
    220 => 722,
    221 => 667,
    222 => 667,
    223 => 611,
    224 => 556,
    225 => 556,
    226 => 556,
    227 => 556,
    228 => 556,
    229 => 556,
    230 => 889,
    231 => 500,
    232 => 556,
    233 => 556,
    234 => 556,
    235 => 556,
    236 => 278,
    237 => 278,
    238 => 278,
    239 => 278,
    240 => 556,
    241 => 556,
    242 => 556,
    243 => 556,
    244 => 556,
    245 => 556,
    246 => 556,
    247 => 549,
    248 => 611,
    249 => 556,
    250 => 556,
    251 => 556,
    252 => 556,
    253 => 500,
    254 => 556,
    255 => 500,
    'overscore' => 552,
    'middot' => 333,
    '.null' => 0,
    'AEacute' => 1000,
    'Abreve' => 667,
    'Alpha' => 667,
    'Alphatonos' => 667,
    'Amacron' => 667,
    'Aogonek' => 667,
    'Aringacute' => 667,
    'Beta' => 667,
    'Cacute' => 722,
    'Ccaron' => 722,
    'Ccircumflex' => 722,
    'Cdot' => 722,
    'Chi' => 667,
    'Dcaron' => 722,
    'Delta' => 668,
    'Dslash' => 722,
    'Ebreve' => 667,
    'Ecaron' => 667,
    'Edot' => 667,
    'Emacron' => 667,
    'Eng' => 723,
    'Eogonek' => 667,
    'Epsilon' => 667,
    'Epsilontonos' => 784,
    'Eta' => 722,
    'Etatonos' => 838,
    'Gamma' => 551,
    'Gbreve' => 778,
    'Gcedilla' => 778,
    'Gcircumflex' => 778,
    'Gdot' => 778,
    'H18533' => 604,
    'H18543' => 354,
    'H18551' => 354,
    'H22073' => 604,
    'Hbar' => 722,
    'Hcircumflex' => 722,
    'IJ' => 735,
    'Ibreve' => 278,
    'Idot' => 278,
    'Imacron' => 278,
    'Iogonek' => 278,
    'Iota' => 278,
    'Iotadieresis' => 278,
    'Iotatonos' => 384,
    'Itilde' => 278,
    'Jcircumflex' => 500,
    'Kappa' => 667,
    'Kcedilla' => 667,
    'Lacute' => 556,
    'Lambda' => 668,
    'Lcaron' => 556,
    'Lcedilla' => 556,
    'Ldot' => 556,
    'Lslash' => 556,
    'Mu' => 833,
    'Nacute' => 722,
    'Ncaron' => 722,
    'Ncedilla' => 722,
    'Nu' => 722,
    'Obreve' => 778,
    'Odblacute' => 778,
    'Ohm' => 768,
    'Omacron' => 778,
    'Omega' => 748,
    'Omegatonos' => 752,
    'Omicron' => 778,
    'Omicrontonos' => 774,
    'Oslashacute' => 778,
    'Phi' => 798,
    'Pi' => 722,
    'Psi' => 835,
    'Racute' => 722,
    'Rcaron' => 722,
    'Rcedilla' => 722,
    'Rho' => 667,
    'SF010000' => 708,
    'SF020000' => 708,
    'SF030000' => 708,
    'SF040000' => 708,
    'SF050000' => 708,
    'SF060000' => 708,
    'SF070000' => 708,
    'SF080000' => 708,
    'SF090000' => 708,
    'SF100000' => 708,
    'SF110000' => 625,
    'SF190000' => 708,
    'SF200000' => 708,
    'SF210000' => 708,
    'SF220000' => 708,
    'SF230000' => 708,
    'SF240000' => 708,
    'SF250000' => 708,
    'SF260000' => 708,
    'SF270000' => 708,
    'SF280000' => 708,
    'SF360000' => 708,
    'SF370000' => 708,
    'SF380000' => 708,
    'SF390000' => 708,
    'SF400000' => 708,
    'SF410000' => 708,
    'SF420000' => 708,
    'SF430000' => 708,
    'SF440000' => 708,
    'SF450000' => 708,
    'SF460000' => 708,
    'SF470000' => 708,
    'SF480000' => 708,
    'SF490000' => 708,
    'SF500000' => 708,
    'SF510000' => 708,
    'SF520000' => 708,
    'SF530000' => 708,
    'SF540000' => 708,
    'Sacute' => 667,
    'Scedilla' => 667,
    'Scircumflex' => 667,
    'Sigma' => 618,
    'Tau' => 611,
    'Tbar' => 611,
    'Tcaron' => 611,
    'Tcedilla' => 611,
    'Theta' => 778,
    'Ubreve' => 722,
    'Udblacute' => 722,
    'Umacron' => 722,
    'Uogonek' => 722,
    'Upsilon' => 667,
    'Upsilondieresis' => 667,
    'Upsilontonos' => 855,
    'Uring' => 722,
    'Utilde' => 722,
    'Wacute' => 944,
    'Wcircumflex' => 944,
    'Wdieresis' => 944,
    'Wgrave' => 944,
    'Xi' => 650,
    'Ycircumflex' => 667,
    'Ygrave' => 667,
    'Zacute' => 611,
    'Zdot' => 611,
    'Zeta' => 611,
    'abreve' => 556,
    'aeacute' => 889,
    'afii00208' => 1000,
    'afii08941' => 556,
    'afii10017' => 667,
    'afii10018' => 656,
    'afii10019' => 667,
    'afii10020' => 542,
    'afii10021' => 677,
    'afii10022' => 667,
    'afii10023' => 667,
    'afii10024' => 923,
    'afii10025' => 604,
    'afii10026' => 719,
    'afii10027' => 719,
    'afii10028' => 583,
    'afii10029' => 656,
    'afii10030' => 833,
    'afii10031' => 722,
    'afii10032' => 778,
    'afii10033' => 719,
    'afii10034' => 667,
    'afii10035' => 722,
    'afii10036' => 611,
    'afii10037' => 635,
    'afii10038' => 760,
    'afii10039' => 667,
    'afii10040' => 740,
    'afii10041' => 667,
    'afii10042' => 917,
    'afii10043' => 937,
    'afii10044' => 792,
    'afii10045' => 885,
    'afii10046' => 656,
    'afii10047' => 719,
    'afii10048' => 1010,
    'afii10049' => 722,
    'afii10050' => 489,
    'afii10051' => 865,
    'afii10052' => 542,
    'afii10053' => 719,
    'afii10054' => 667,
    'afii10055' => 278,
    'afii10056' => 278,
    'afii10057' => 500,
    'afii10058' => 1057,
    'afii10059' => 1010,
    'afii10060' => 854,
    'afii10061' => 583,
    'afii10062' => 635,
    'afii10065' => 556,
    'afii10066' => 573,
    'afii10067' => 531,
    'afii10068' => 365,
    'afii10069' => 583,
    'afii10070' => 556,
    'afii10071' => 556,
    'afii10072' => 669,
    'afii10073' => 458,
    'afii10074' => 559,
    'afii10075' => 559,
    'afii10076' => 437,
    'afii10077' => 583,
    'afii10078' => 687,
    'afii10079' => 552,
    'afii10080' => 556,
    'afii10081' => 542,
    'afii10082' => 556,
    'afii10083' => 500,
    'afii10084' => 458,
    'afii10085' => 500,
    'afii10086' => 823,
    'afii10087' => 500,
    'afii10088' => 573,
    'afii10089' => 521,
    'afii10090' => 802,
    'afii10091' => 823,
    'afii10092' => 625,
    'afii10093' => 719,
    'afii10094' => 521,
    'afii10095' => 510,
    'afii10096' => 750,
    'afii10097' => 542,
    'afii10098' => 411,
    'afii10099' => 556,
    'afii10100' => 365,
    'afii10101' => 510,
    'afii10102' => 500,
    'afii10103' => 222,
    'afii10104' => 278,
    'afii10105' => 222,
    'afii10106' => 906,
    'afii10107' => 812,
    'afii10108' => 556,
    'afii10109' => 437,
    'afii10110' => 500,
    'afii10145' => 719,
    'afii10193' => 552,
    'afii52305' => 394,
    'afii52306' => 515,
    'afii52364' => 588,
    'afii52399' => 413,
    'afii52400' => 207,
    'afii52957' => 244,
    'afii57381' => 526,
    'afii57388' => 319,
    'afii57391' => 638,
    'afii57392' => 526,
    'afii57393' => 526,
    'afii57394' => 526,
    'afii57395' => 526,
    'afii57396' => 526,
    'afii57397' => 526,
    'afii57398' => 526,
    'afii57399' => 526,
    'afii57400' => 526,
    'afii57401' => 526,
    'afii57403' => 319,
    'afii57407' => 356,
    'afii57409' => 413,
    'afii57410' => 394,
    'afii57411' => 207,
    'afii57412' => 432,
    'afii57413' => 207,
    'afii57414' => 638,
    'afii57415' => 207,
    'afii57416' => 713,
    'afii57417' => 282,
    'afii57418' => 713,
    'afii57419' => 713,
    'afii57420' => 563,
    'afii57421' => 563,
    'afii57422' => 563,
    'afii57423' => 337,
    'afii57424' => 337,
    'afii57425' => 489,
    'afii57426' => 489,
    'afii57427' => 821,
    'afii57428' => 821,
    'afii57429' => 1098,
    'afii57430' => 1098,
    'afii57431' => 582,
    'afii57432' => 582,
    'afii57433' => 544,
    'afii57434' => 544,
    'afii57440' => 207,
    'afii57440_2' => 125,
    'afii57440_3' => 1000,
    'afii57440_4' => 2000,
    'afii57441' => 789,
    'afii57442' => 582,
    'afii57443' => 601,
    'afii57444' => 506,
    'afii57445' => 338,
    'afii57446' => 526,
    'afii57447' => 282,
    'afii57448' => 432,
    'afii57449' => 638,
    'afii57450' => 638,
    'afii57451' => 201,
    'afii57451_2' => 201,
    'afii57452' => 237,
    'afii57452_2' => 237,
    'afii57453' => 201,
    'afii57453_2' => 201,
    'afii57454' => 201,
    'afii57454_2' => 201,
    'afii57455' => 209,
    'afii57455_2' => 209,
    'afii57456' => 201,
    'afii57456_2' => 201,
    'afii57457' => 211,
    'afii57457_2' => 211,
    'afii57458' => 165,
    'afii57458_2' => 165,
    'afii57460' => 750,
    'afii57461' => 319,
    'afii57470' => 450,
    'afii57506' => 713,
    'afii57507' => 563,
    'afii57508' => 489,
    'afii57509' => 812,
    'afii57534' => 282,
    'afii57555' => 638,
    'afii57567' => 812,
    'afii61248' => 885,
    'afii61289' => 323,
    'afii61352' => 1073,
    'afii62753' => 229,
    'afii62754' => 229,
    'afii62755' => 432,
    'afii62756' => 229,
    'afii62757' => 244,
    'afii62758' => 244,
    'afii62759' => 588,
    'afii62760' => 229,
    'afii62761' => 244,
    'afii62762' => 244,
    'afii62763' => 713,
    'afii62764' => 375,
    'afii62765' => 244,
    'afii62766' => 244,
    'afii62767' => 713,
    'afii62768' => 244,
    'afii62769' => 244,
    'afii62770' => 713,
    'afii62771' => 530,
    'afii62772' => 530,
    'afii62773' => 526,
    'afii62774' => 530,
    'afii62775' => 530,
    'afii62776' => 526,
    'afii62777' => 530,
    'afii62778' => 530,
    'afii62779' => 526,
    'afii62780' => 337,
    'afii62781' => 337,
    'afii62782' => 489,
    'afii62783' => 489,
    'afii62784' => 531,
    'afii62785' => 531,
    'afii62786' => 821,
    'afii62787' => 531,
    'afii62788' => 531,
    'afii62789' => 821,
    'afii62790' => 846,
    'afii62791' => 846,
    'afii62792' => 1098,
    'afii62793' => 846,
    'afii62794' => 846,
    'afii62795' => 1098,
    'afii62796' => 582,
    'afii62797' => 582,
    'afii62798' => 582,
    'afii62799' => 582,
    'afii62800' => 582,
    'afii62801' => 582,
    'afii62802' => 526,
    'afii62803' => 394,
    'afii62804' => 450,
    'afii62805' => 526,
    'afii62806' => 394,
    'afii62807' => 450,
    'afii62808' => 268,
    'afii62809' => 263,
    'afii62810' => 789,
    'afii62811' => 268,
    'afii62812' => 263,
    'afii62813' => 582,
    'afii62815' => 394,
    'afii62816' => 601,
    'afii62817' => 207,
    'afii62818' => 207,
    'afii62819' => 506,
    'afii62820' => 394,
    'afii62821' => 394,
    'afii62822' => 338,
    'afii62823' => 244,
    'afii62824' => 244,
    'afii62825' => 526,
    'afii62827' => 394,
    'afii62828' => 375,
    'afii62829' => 432,
    'afii62830' => 588,
    'afii62831' => 244,
    'afii62832' => 244,
    'afii62833' => 588,
    'afii62834' => 544,
    'afii62835' => 601,
    'afii62836' => 544,
    'afii62837' => 601,
    'afii62838' => 544,
    'afii62839' => 601,
    'afii62840' => 544,
    'afii62841' => 601,
    'afii62843' => 526,
    'afii62844' => 526,
    'afii62845' => 526,
    'afii62881' => 211,
    'afii62881_2' => 211,
    'afii62882' => 226,
    'afii62882_2' => 226,
    'afii62883' => 211,
    'afii62883_2' => 211,
    'afii62884' => 211,
    'afii62884_2' => 211,
    'afii62885' => 211,
    'afii62885_2' => 211,
    'afii62886' => 211,
    'afii62886_2' => 211,
    'afii62956' => 244,
    'afii62958' => 713,
    'afii62959' => 530,
    'afii62960' => 530,
    'afii62961' => 526,
    'afii62962' => 489,
    'afii62964' => 933,
    'afii62965' => 394,
    'afii62966' => 515,
    'afii62967' => 933,
    'afii63167' => 526,
    'afii64060' => 319,
    'afii64061' => 319,
    'afii64184' => 616,
    'alpha' => 578,
    'alphatonos' => 578,
    'amacron' => 556,
    'anoteleia' => 278,
    'aogonek' => 556,
    'approxequal' => 549,
    'aringacute' => 556,
    'arrowboth' => 1000,
    'arrowdown' => 500,
    'arrowleft' => 1000,
    'arrowright' => 1000,
    'arrowup' => 500,
    'arrowupdn' => 500,
    'arrowupdnbse' => 500,
    'beta' => 575,
    'block' => 708,
    'breve' => 333,
    'cacute' => 500,
    'caron' => 333,
    'ccaron' => 500,
    'ccircumflex' => 500,
    'cdot' => 500,
    'chi' => 525,
    'circle' => 604,
    'club' => 656,
    'commaaccent' => 333,
    'dcaron' => 615,
    'delta' => 557,
    'diamond' => 510,
    'dieresistonos' => 333,
    'dkshade' => 729,
    'dmacron' => 556,
    'dnblock' => 708,
    'dotaccent' => 333,
    'dotlessi' => 278,
    'ebreve' => 556,
    'ecaron' => 556,
    'edot' => 556,
    'emacron' => 556,
    'eng' => 556,
    'eogonek' => 556,
    'epsilon' => 446,
    'epsilontonos' => 446,
    'equivalence' => 583,
    'estimated' => 600,
    'eta' => 556,
    'etatonos' => 556,
    'exclamdbl' => 500,
    'female' => 750,
    'fi' => 500,
    'filledbox' => 604,
    'filledrect' => 1000,
    'fiveeighths' => 834,
    'fl' => 500,
    'fraction' => 167,
    'franc' => 556,
    'gamma' => 500,
    'gbreve' => 556,
    'gcedilla' => 556,
    'gcircumflex' => 556,
    'gdot' => 556,
    'glyph1100' => 319,
    'glyph1101' => 319,
    'glyph1102' => 319,
    'greaterequal' => 549,
    'hbar' => 556,
    'hcircumflex' => 556,
    'heart' => 594,
    'house' => 604,
    'hungarumlaut' => 333,
    'ibreve' => 278,
    'ij' => 444,
    'imacron' => 278,
    'increment' => 612,
    'infinity' => 713,
    'integral' => 274,
    'integralbt' => 604,
    'integraltp' => 604,
    'intersection' => 719,
    'invbullet' => 604,
    'invcircle' => 604,
    'invsmileface' => 1052,
    'iogonek' => 222,
    'iota' => 222,
    'iotadieresis' => 222,
    'iotadieresistonos' => 222,
    'iotatonos' => 222,
    'itilde' => 278,
    'jcircumflex' => 222,
    'kappa' => 500,
    'kcedilla' => 500,
    'kgreenlandic' => 500,
    'lacute' => 222,
    'lambda' => 500,
    'lcaron' => 292,
    'lcedilla' => 222,
    'ldot' => 334,
    'lefttorightmark' => 0,
    'lessequal' => 549,
    'lfblock' => 708,
    'longs' => 222,
    'lozenge' => 494,
    'lslash' => 222,
    'ltshade' => 708,
    'male' => 750,
    'minus' => 584,
    'minute' => 187,
    'mu' => 576,
    'musicalnote' => 500,
    'musicalnotedbl' => 750,
    'nacute' => 556,
    'napostrophe' => 604,
    'ncaron' => 556,
    'ncedilla' => 556,
    'nonmarkingreturn' => 278,
    'notequal' => 549,
    'nsuperior' => 365,
    'nu' => 500,
    'obreve' => 556,
    'odblacute' => 556,
    'ogonek' => 333,
    'omacron' => 556,
    'omega' => 781,
    'omegatonos' => 781,
    'omicron' => 556,
    'omicrontonos' => 556,
    'oneeighth' => 834,
    'openbullet' => 354,
    'orthogonal' => 979,
    'oslashacute' => 611,
    'partialdiff' => 494,
    'peseta' => 1094,
    'phi' => 648,
    'pi' => 690,
    'pi1' => 549,
    'product' => 823,
    'psi' => 713,
    'quotereversed' => 222,
    'racute' => 333,
    'radical' => 549,
    'radicalex' => 333,
    'rcaron' => 333,
    'rcedilla' => 333,
    'revlogicalnot' => 584,
    'rho' => 569,
    'righttoleftmark' => 0,
    'ring' => 333,
    'rtblock' => 708,
    'sacute' => 500,
    'scedilla' => 500,
    'scircumflex' => 500,
    'second' => 354,
    'seveneighths' => 834,
    'shade' => 708,
    'sigma' => 617,
    'sigma1' => 482,
    'smileface' => 1021,
    'spade' => 531,
    'summation' => 713,
    'sun' => 917,
    'tau' => 395,
    'tbar' => 278,
    'tcaron' => 375,
    'tcedilla' => 278,
    'theta' => 556,
    'threeeighths' => 834,
    'tonos' => 333,
    'triagdn' => 990,
    'triaglf' => 990,
    'triagrt' => 990,
    'triagup' => 990,
    'ubreve' => 556,
    'udblacute' => 556,
    'umacron' => 556,
    'undercommaaccent' => 333,
    'underscoredbl' => 552,
    'uogonek' => 556,
    'upblock' => 708,
    'upsilon' => 547,
    'upsilondieresis' => 547,
    'upsilondieresistonos' => 547,
    'upsilontonos' => 547,
    'uring' => 556,
    'utilde' => 556,
    'wacute' => 722,
    'wcircumflex' => 722,
    'wdieresis' => 722,
    'wgrave' => 722,
    'xi' => 448,
    'ycircumflex' => 500,
    'ygrave' => 500,
    'zacute' => 500,
    'zdot' => 500,
    'zerowidthjoiner' => 0,
    'zerowidthnonjoiner' => 0,
    'zeta' => 441,
  ),
  'CIDtoGID_Compressed' => true,
  'CIDtoGID' => 'eJwDAAAAAAE=',
  '_version_' => 6,
);