<?php return array (
  'sans-serif' => 
  array (
    'normal' => DOMPDF_DIR . '/lib/fonts/Helvetica',
    'bold' => DOMPDF_DIR . '/lib/fonts/Helvetica-Bold',
    'italic' => DOMPDF_DIR . '/lib/fonts/Helvetica-Oblique',
    'bold_italic' => DOMPDF_DIR . '/lib/fonts/Helvetica-BoldOblique',
  ),
  'times' => 
  array (
    'normal' => DOMPDF_DIR . '/lib/fonts/Times-Roman',
    'bold' => DOMPDF_DIR . '/lib/fonts/Times-Bold',
    'italic' => DOMPDF_DIR . '/lib/fonts/Times-Italic',
    'bold_italic' => DOMPDF_DIR . '/lib/fonts/Times-BoldItalic',
  ),
  'times-roman' => 
  array (
    'normal' => DOMPDF_DIR . '/lib/fonts/Times-Roman',
    'bold' => DOMPDF_DIR . '/lib/fonts/Times-Bold',
    'italic' => DOMPDF_DIR . '/lib/fonts/Times-Italic',
    'bold_italic' => DOMPDF_DIR . '/lib/fonts/Times-BoldItalic',
  ),
  'courier' => 
  array (
    'normal' => DOMPDF_DIR . '/lib/fonts/Courier',
    'bold' => DOMPDF_DIR . '/lib/fonts/Courier-Bold',
    'italic' => DOMPDF_DIR . '/lib/fonts/Courier-Oblique',
    'bold_italic' => DOMPDF_DIR . '/lib/fonts/Courier-BoldOblique',
  ),
  'helvetica' => 
  array (
    'normal' => DOMPDF_DIR . '/lib/fonts/Helvetica',
    'bold' => DOMPDF_DIR . '/lib/fonts/Helvetica-Bold',
    'italic' => DOMPDF_DIR . '/lib/fonts/Helvetica-Oblique',
    'bold_italic' => DOMPDF_DIR . '/lib/fonts/Helvetica-BoldOblique',
  ),
  'zapfdingbats' => 
  array (
    'normal' => DOMPDF_DIR . '/lib/fonts/ZapfDingbats',
    'bold' => DOMPDF_DIR . '/lib/fonts/ZapfDingbats',
    'italic' => DOMPDF_DIR . '/lib/fonts/ZapfDingbats',
    'bold_italic' => DOMPDF_DIR . '/lib/fonts/ZapfDingbats',
  ),
  'symbol' => 
  array (
    'normal' => DOMPDF_DIR . '/lib/fonts/Symbol',
    'bold' => DOMPDF_DIR . '/lib/fonts/Symbol',
    'italic' => DOMPDF_DIR . '/lib/fonts/Symbol',
    'bold_italic' => DOMPDF_DIR . '/lib/fonts/Symbol',
  ),
  'serif' => 
  array (
    'normal' => DOMPDF_DIR . '/lib/fonts/Times-Roman',
    'bold' => DOMPDF_DIR . '/lib/fonts/Times-Bold',
    'italic' => DOMPDF_DIR . '/lib/fonts/Times-Italic',
    'bold_italic' => DOMPDF_DIR . '/lib/fonts/Times-BoldItalic',
  ),
  'monospace' => 
  array (
    'normal' => DOMPDF_DIR . '/lib/fonts/Courier',
    'bold' => DOMPDF_DIR . '/lib/fonts/Courier-Bold',
    'italic' => DOMPDF_DIR . '/lib/fonts/Courier-Oblique',
    'bold_italic' => DOMPDF_DIR . '/lib/fonts/Courier-BoldOblique',
  ),
  'fixed' => 
  array (
    'normal' => DOMPDF_DIR . '/lib/fonts/Courier',
    'bold' => DOMPDF_DIR . '/lib/fonts/Courier-Bold',
    'italic' => DOMPDF_DIR . '/lib/fonts/Courier-Oblique',
    'bold_italic' => DOMPDF_DIR . '/lib/fonts/Courier-BoldOblique',
  ),
  'dejavu sans' => 
  array (
    'bold' => DOMPDF_DIR . '/lib/fonts/DejaVuSans-Bold',
    'bold_italic' => DOMPDF_DIR . '/lib/fonts/DejaVuSans-BoldOblique',
    'italic' => DOMPDF_DIR . '/lib/fonts/DejaVuSans-Oblique',
    'normal' => DOMPDF_DIR . '/lib/fonts/DejaVuSans',
  ),
  'dejavu sans light' => 
  array (
    'normal' => DOMPDF_DIR . '/lib/fonts/DejaVuSans-ExtraLight',
  ),
  'dejavu sans condensed' => 
  array (
    'bold' => DOMPDF_DIR . '/lib/fonts/DejaVuSansCondensed-Bold',
    'bold_italic' => DOMPDF_DIR . '/lib/fonts/DejaVuSansCondensed-BoldOblique',
    'italic' => DOMPDF_DIR . '/lib/fonts/DejaVuSansCondensed-Oblique',
    'normal' => DOMPDF_DIR . '/lib/fonts/DejaVuSansCondensed',
  ),
  'dejavu sans mono' => 
  array (
    'bold' => DOMPDF_DIR . '/lib/fonts/DejaVuSansMono-Bold',
    'bold_italic' => DOMPDF_DIR . '/lib/fonts/DejaVuSansMono-BoldOblique',
    'italic' => DOMPDF_DIR . '/lib/fonts/DejaVuSansMono-Oblique',
    'normal' => DOMPDF_DIR . '/lib/fonts/DejaVuSansMono',
  ),
  'dejavu serif' => 
  array (
    'bold' => DOMPDF_DIR . '/lib/fonts/DejaVuSerif-Bold',
    'bold_italic' => DOMPDF_DIR . '/lib/fonts/DejaVuSerif-BoldItalic',
    'italic' => DOMPDF_DIR . '/lib/fonts/DejaVuSerif-Italic',
    'normal' => DOMPDF_DIR . '/lib/fonts/DejaVuSerif',
  ),
  'dejavu serif condensed' => 
  array (
    'bold' => DOMPDF_DIR . '/lib/fonts/DejaVuSerifCondensed-Bold',
    'bold_italic' => DOMPDF_DIR . '/lib/fonts/DejaVuSerifCondensed-BoldItalic',
    'italic' => DOMPDF_DIR . '/lib/fonts/DejaVuSerifCondensed-Italic',
    'normal' => DOMPDF_DIR . '/lib/fonts/DejaVuSerifCondensed',
  ),
  'metaplusbold' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'a082309e0e609d7c8ba3b97c4533f896',
  ),
  'metaplusbookroman' => 
  array (
    'normal' => DOMPDF_FONT_DIR . '62f91b45caccd9fffc9fcf81b06049c6',
  ),
  'myriadpro-regular' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'a8c26935d46b659a4f231091bc1a3c9c',
  ),
  'swissboldcondensed' => 
  array (
    'normal' => DOMPDF_FONT_DIR . '08ce6602a800623001d9f7cb4f99df73',
  ),
  'swisscondensed' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'bff2324c36412add1bdeb845832b735d',
  ),
  'swisslightcondensed' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'fef41e010b4eaf92e4a8437a55ad8231',
  ),
  'arial' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'de9043dbef508ce8c38c521929f0720f',
  ),
  'montserrat' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'f86fa52db42b72e62cfa645e44b5208e',
  ),
  'gotham' => 
  array (
    'normal' => DOMPDF_FONT_DIR . '318c2157460bcc996cd5f448286530e8',
  ),
  'gotham-light' => 
  array (
    'normal' => DOMPDF_FONT_DIR . '318c2157460bcc996cd5f448286530e8',
  ),
  'gotham-medium' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'b4bea2b0b548e47b87fe2d04753c2334',
  ),
  'gotham-bold' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'd49a89644ff078dedef668da5bbbbce5',
  ),
  'gotham-black' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'e7388393dfc9612d299024d308885be9',
  ),
) ?>