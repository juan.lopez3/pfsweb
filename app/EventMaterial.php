<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EventMaterial extends Model {

	protected $table = "event_materials";
	
	protected $guarded = ['id'];

}
