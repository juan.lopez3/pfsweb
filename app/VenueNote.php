<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class VenueNote extends Model {

	//
	public function user() {

		return $this->belongsTo('App\User');
	}

	public function venue() {

		return $this->belongsTo('App\Venue');
	}
}
