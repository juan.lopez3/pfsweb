<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model {

	protected $table = "reports";
	
	protected $guarded = ['id'];
	
	public function user() {
		
		return $this->belongsTo('App\User');
	}
}
