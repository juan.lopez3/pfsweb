<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{

    use SoftDeletes;

    protected $table = "events";

    protected $guarded = ['id'];

    public function country()
    {

        return $this->belongsTo('App\Country');
    }

    public function venue()
    {

        return $this->belongsTo('App\Venue');
    }

    public function eventType()
    {

        return $this->belongsTo('App\EventType');
    }

    public function sponsors()
    {

        return $this->belongsToMany('App\Sponsor')->withPivot('type', 'stand_number', 'id', 'category_1', 'category_2');
    }

    public function eventMaterials()
    {

        return $this->hasMany('App\EventMaterial');
    }

    public function eventFaqs()
    {

        return $this->hasMany('App\EventFaq');
    }

    public function tabs()
    {

        return $this->belongsToMany('App\EventTab', 'event_tab', 'event_id', 'tab_id');
    }

    public function tfiContact()
    {

        return $this->belongsTo('App\User', 'tfi_contact_id');
    }

    public function emailTemplates()
    {

        return $this->belongsToMany('App\EmailTemplate', 'email_template_event', 'event_id', 'email_template_id')->withPivot('reminder', 'template', 'active', 'subject');
    }

    public function schedule()
    {

        return $this->hasOne('App\EventBasicSchedule');
    }

    public function feedbacks()
    {

        return $this->hasMany('App\EventFeedback')->orderBy('position');
    }

    public function atendees()
    {
        return $this->belongsToMany('App\User', 'event_atendees')->withPivot('id', 'registration_status_id', 'checkin_time', 'checkout_time', 'sponsors_contact', 'first_time_attendee', 'paid', 'manual_booking', 'api_booking', 'fee_waived', 'cancel_reason', 'onsite_booking', 'comitee', 'notes', 'created_at', 'updated_at');
    }

    public function atendeesOnlyForReport()
    {
        // Se separo esto porque ocasiona error en varias vistas del cms
        return $this->belongsToMany('App\User', 'event_atendees')
            ->withPivot('id', 'registration_status_id', 'checkin_time', 'checkout_time', 'sponsors_contact', 'first_time_attendee', 'paid', 'manual_booking', 'api_booking', 'fee_waived', 'cancel_reason', 'onsite_booking', 'comitee', 'notes', 'created_at', 'updated_at')
            ->join('registration_status', 'event_atendees.registration_status_id', '=', 'registration_status.id')
            ->select('users.*', 'registration_status.title as status_title', 'event_atendees.*');

    }

    public function atendeesByRoles($role_1, $role_2)
    {

        return $this->atendees()->where(function ($query) use ($role_1, $role_2) {
            $query->where('role_id', $role_1)->orWhere('role_id', $role_2);
        })->with('role', 'subRoles')->get();
    }

    public function atendeesByRole($role)
    {

        return $this->atendees->where('role_id', $role);
    }

    public function costs()
    {

        return $this->hasMany('App\EventCost');
    }

    public function theme()
    {
        return $this->belongsTo('App\Theme');
    }

    public function onSiteDetails()
    {

        return $this->hasOne('App\EventOnSiteDetail');
    }

    public function technologies()
    {

        return $this->belongsToMany('App\Technology')->withPivot('quantity', 'provided_by');
    }

    public function venueSpaces()
    {

        return $this->belongsToMany('App\VenueRoom')->withPivot('id', 'meeting_space_name', 'layout_style', 'num_of_tables', 'setup_note');
    }

    public function scheduleTimeSlots()
    {

        return $this->hasMany('App\EventScheduleSlot');
    }

    public function scheduleSessions()
    {

        return $this->hasManyThrough('App\EventScheduleSlotSession', 'App\EventScheduleSlot', 'event_id', 'event_schedule_slot_id');
    }

    public function region()
    {

        return $this->belongsTo('App\Region');
    }

    public function files()
    {

        return $this->belongsToMany('App\File');
    }

    public function registrationAnswers()
    {

        return $this->hasMany('App\RegistrationAnswer');
    }

    public function gallery()
    {
        return $this->hasMany('App\EventGallery');
    }

    public function sentEmails()
    {
        return $this->hasMany('App\SentEmail');
    }

    public function scheduleExplanationTexts()
    {
        return $this->hasMany('App\ScheduleExplanationText');
    }

    /**
     * Checks if there are any free sessions for event
     */
    public function checkAvailability()
    {

        $full = false;

        // if accept all - return not full
        if ($this->registration_type_id == 2) {
            return false;
        }

        $attendees_tot = $this->atendees()->whereIn('registration_status_id', [\Config::get('registrationstatus.booked'), \Config::get('registrationstatus.waitlisted')])->count();

        foreach ($this->scheduleTimeSlots as $slot) {

            if (!$slot->bookable) {
                continue;
            }

            if ($slot->slot_capacity <= $slot->attendees()->whereIn('registration_status_id', [\Config::get('registrationstatus.booked'), \Config::get('registrationstatus.waitlisted')])->count() || $slot->slot_capacity <= $attendees_tot) {

                $full = true;
                break;
            }
        }

        return $full;
    }
}
