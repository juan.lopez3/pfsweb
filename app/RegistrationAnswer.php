<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class RegistrationAnswer extends Model {

	protected $table = "registration_answers";
	
	protected $protected = ['id'];

	public function event() {

		return $this->belongsTo('App\Event');
	}

	public function attendee() {

		return $this->belongsTo('App\EventAttendee');
	}
}
