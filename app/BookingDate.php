<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingDate extends Model
{
    protected $fillable = ['title', 'from', 'to'];
}
