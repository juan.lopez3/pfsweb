<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class SessionType extends Model {

	protected $table = "session_types";
	
	protected $fillable = ['title', 'description', 'color', 'include_in_cpd', 'include_in_feedback'];
	
}
