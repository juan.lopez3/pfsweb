<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TabNote extends Model {

	protected $table = "tab_notes";
	
	protected $fillable = ['note', 'venue_id', 'tab_id', 'event_id', 'user_id'];
	
	public function noteTab() {
		
		return $this->belongsTo('App\EventTab');
	}
	
	public function noteEvent() {
		
		return $this->belongsTo('App\Event');
	}

	public function user() {

		return $this->belongsTo('App\User');
	}
}
