<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EventTab extends Model {

	protected $table = "event_tabs";
	
	protected $fillable = ['title','slug'];
	
	public function events() {
		
		return $this->belongsToMany('App\Event', 'event_tab', 'event_id', 'tab_id');
	}
	
	public function notes() {
		
		return $this->hasMany('App\TabNote', 'tab_id');
	}
}
