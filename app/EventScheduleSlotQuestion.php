<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EventScheduleSlotQuestion extends Model {

	protected $table = "event_schedule_slot_questions";
	
	protected $guarded = ["id"];

	public $timestamps = false;
}
