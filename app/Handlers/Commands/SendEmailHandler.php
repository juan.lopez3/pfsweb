<?php namespace App\Handlers\Commands;

use App\Commands\SendEmail;


use App\Events\EmailWasSent;

use App\EventAttendee;
use App\EventAttendeeSlotSession;
use App\Payment;
use App\SessionType;
use App\SponsorSetting;
use App\Sponsor;
use App\UserAction;

class SendEmailHandler {
	
	/**
	 * Create the command handler.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the command.
	 *
	 * @param  SendEmail  $command
	 * @return void
	 */
	public function handle(SendEmail $command)
	{
        $event_id = (is_array($command->event) && sizeof($command->event)) ? $command->event->id : null;
        $user_id = (is_array($command->user) && sizeof($command->user)) ? $command->user->id : null;
        $subject = $command->subject;
        $message_text = $command->message_text;
        $sent_email = event(new EmailWasSent($command->to, $subject, $message_text, $user_id, $event_id, $command->campaign));
        $sent_email = $sent_email[0];

        // USER SHORTCODES
        $title = "";
        $first_name = "";
        $last_name = "";
        $profile_link = "";
        $edit_profile_link = "";
        $feedback_link = "";
        $certificate_link = "";
        $my_events_link = "";
        $non_member_confirmation_text = "";
        $non_member_thankyou_text = "";
        $decline_invitation = "";
        $accept_invitation = "";

        $profile_link = $this->prepareLink($sent_email->id, $sent_email->to, route('profile'));
        $edit_profile_link = $this->prepareLink($sent_email->id, $sent_email->to, route('profile.edit'));
        $feedback_link = $this->prepareLink($sent_email->id, $sent_email->to, route('events.feedback', $command->event->slug));
        $certificate_link = $this->prepareLink($sent_email->id, $sent_email->to, route('events.getCertificate', $command->event->slug));


        if (isset($command->user) && !empty($command->user)) {

            $title = $command->user->title;
            $first_name = $command->user->first_name;
            $last_name = $command->user->last_name;
            $my_events_link = $this->prepareLink($sent_email->id, $sent_email->to, route('profile.myEvents'));
            $accept_invitation = $this->prepareLink($sent_email->id, $sent_email->to, route('events.acceptInvitation', $command->event->slug));
            $decline_invitation = $this->prepareLink($sent_email->id, $sent_email->to, route('events.declineInvitation', $command->event->slug));

            if ($command->user->role_id == role('non_member')) {
                $non_member_confirmation_text = ', and will be happy to talk you through the many other services and benefits available to members. In the meantime if you would like to find out more about Personal Finance Society membership please <a href="http://www.thepfs.org/membership/">click here</a>.';
                $non_member_thankyou_text .= "We hope you enjoyed the event and found attendance a valuable use of your time. The Personal Finance Society provides a comprehensive face to face CPD programme to its members every year, which includes conferences and specialist events. We pride ourselves on delivering independent and business relevant knowledge and other content that can be used practically in your day to day role.";

                // if email template registration confirmation and user has already 1 event booked
                if ($command->user->events()->where('registration_status_id', config('registrationstatus.booked'))->count() > 1) {
                    $non_member_confirmation_text .= "We note that you have previously attended a Personal Finance Society event as a guest, so please note you will be prompted to become a full member before you try to register for any future events";
                    $non_member_thankyou_text .= 'As you have taken advantage of being a guest delegate at Personal Finance Society events we would recommend that you join as a full member now so that you can continue to attend events and enjoy the wider benefits of membership. <a href="http://www.thepfs.org/membership">Click here</a> to find out more and apply online. Alternatively you can call our customer service team on +44 (0)20 8530 0852';
                } else {
                    $non_member_thankyou_text .= 'Now you have experienced the quality of a Personal Finance Society event we hope you consider becoming a full member.  There are also a wide range of additional resources available to members online; <a href="http://www.thepfs.org/membership">click here</a> to find out more about the range of services that you would have access to as a member.';
                }
            }
            
        }

        $search = ['TITLE', 'FIRST_NAME', 'LAST_NAME', 'PROFILE_LINK', 'EDIT_PROFILE', 'FEEDBACK', 'CERTIFICATE', 'MY_EVENTS', 'NON_MEMBER_CONFIRMATION_TEXT', 'NON_MEMBER_THANKYOU_TEXT', 'ACCEPT_INVITATION', 'DECLINE_INVITATION'];
        $replace = [$title, $first_name, $last_name, $profile_link, $edit_profile_link, $feedback_link, $certificate_link, $my_events_link, $non_member_confirmation_text, $non_member_thankyou_text, $accept_invitation, $decline_invitation];
        $message_text = str_replace($search, $replace, $message_text);

        # EVENT SHORTCODES
        $schedule_start = "";
        $schedule_end = "";
        $hashtags = "";
        $booked_slots = "";
        $waitlisted_slots = "";
        $all_event_sessions = "";
        $all_event_sessions_times = "";
        $sessions_duration = 0;
        $payment_text = "";

        if (isset($command->event) && !empty($command->event)) {

            $data = $command->event->scheduleTimeSlots()->orderBy('start')->first();
            if (is_array ($data) && sizeof($data)) $schedule_start = date("H:i", strtotime($data->start));

            $data = $command->event->scheduleTimeSlots()->orderBy('end', 'DESC')->first();
            if (is_array ($data) && sizeof($data)) $schedule_end = date("H:i", strtotime($data->end));

            foreach (explode(",", $command->event->hashtag) as $ht) {
                $hashtags .= "#" . $ht . " ";
            }
            /* Add event price, payment, etc into email */
            if (isset($command->user) && !empty($command->user)) {
                if ($command->event->price > 0.00 || $command->event->price_nonmember > 0.00 && $command->event->payment_enabled) { 
                    $attendee = EventAttendee::where('user_id', $command->user->id)->where('event_id', $command->event->id)->first();
                    $payment = Payment::where('attendee_id', (string)$attendee->id)->first();
                    $title =  $command->event->title;   
                    $now = date("Y-m-d H:i:s");

                    if ($command->event->price > 0.00 && $command->user->role_id == role('member')  && $command->event->payment_enabled) { 
                        $price = $command->event->price;
                        $payment_text .= "<p><strong>Your payment</strong><br /><br />Thank you very much for your purchase,<br /><br />Your payment has been successful for the event: $title,<br />below the data or your payment:<br /><br /><strong>Amount:&nbsp;</strong>$price<br /><strong>Currency:</strong>GBP<br /><strong>Brand:</strong>$payment->paymentBrand<br /><strong>Card Holder:</strong>$payment->holder<br /><strong>Checkout Id:</strong>$payment->checkout_id<br /><strong>Date: </strong>$now</p>";
                        
                    } 
                    if ($command->event->price_nonmember > 0.00 &&  $command->user->role_id != role('member') && $command->event->payment_enabled) {
                        $price = $command->event->price_nonmember;
                        $payment_text .= "<p><strong>Your payment</strong><br /><br />Thank you very much for your purchase,<br /><br />Your payment has been successful for the event: $title,<br />below the data or your payment:<br /><br /><strong>Amount:&nbsp;</strong>$price<br /><strong>Currency:</strong>GBP<br /><strong>Brand:</strong>$payment->paymentBrand<br /><strong>Card Holder:</strong>$payment->holder<br /><strong>Checkout Id:</strong>$payment->checkout_id<br /><strong>Date: </strong>$now</p>";
                    }
                }
            } 

            // EVENT SPONSORS
            $delegates_number = $command->event->atendees()->where('registration_status_id', config('registrationstatus.booked'))->whereIn('role_id', [role('member'), role('non_member')])->count();
            
            if ($command->event->eventType->id == 42) {

                $sponsors_text = "";
                $sponsors = Sponsor::where('order', '>', 0)
                    ->orderBy('order', 'ASC')
                    ->limit(20)->get();

                foreach ($command->event->sponsors()->where('type', 0)->where('event_sponsor.order', '>', 0)->orderBy('event_sponsor.order', 'ASC')->limit(19)->get() as $sponsor) {
                    $logo = config('app.url') . "uploads/sponsors/" . $sponsor->logo;
                    $sponsors_text .= "<a href=\"" . $sponsor->website . "\" targer=\"_blank\"><img height=\"60\" src=\"" . $logo . "\" /></a> ";
                }

                if ($command->event->eventType->has_sponsor) {

                    $command->today = $command->event->event_date_from;
                    $quarterly_sponsors = SponsorSetting::where('date_from', '<=', $command->today)->where('date_to', '>=', $command->today)->get();

                    foreach ($quarterly_sponsors as $sponsor) {

                        for ($i = 1; $i <= 10; $i++) {
                            $logo_id = "logo_" . $i;
                            if (empty($sponsor->{$logo_id})) continue;

                            $sp1 = $sponsors->find($sponsor->{$logo_id});
                            $logo = config('app.url') . "uploads/sponsors/" . $sp1->logo;
                            $sponsors_text .= "<a href=\"" . $sp1->website . "\" targer=\"_blank\"><img height=\"60\" src=\"" . $logo . "\" /></a> ";
                        }
                    }
                }

            }else{

                $sponsors_text = "";
                $sponsors = Sponsor::all();

                foreach ($command->event->sponsors()->where('type', 0)->get() as $sponsor) {
                    $logo = config('app.url') . "uploads/sponsors/" . $sponsor->logo;
                    $sponsors_text .= "<a href=\"" . $sponsor->website . "\" targer=\"_blank\"><img height=\"60\" src=\"" . $logo . "\" /></a> ";
                }

                if ($command->event->eventType->has_sponsor) {

                    $command->today = $command->event->event_date_from;
                    $quarterly_sponsors = SponsorSetting::where('date_from', '<=', $command->today)->where('date_to', '>=', $command->today)->get();

                    foreach ($quarterly_sponsors as $sponsor) {

                        for ($i = 1; $i <= 10; $i++) {
                            $logo_id = "logo_" . $i;
                            if (empty($sponsor->{$logo_id})) continue;

                            $sp1 = $sponsors->find($sponsor->{$logo_id});
                            $logo = config('app.url') . "uploads/sponsors/" . $sp1->logo;
                            $sponsors_text .= "<a href=\"" . $sp1->website . "\" targer=\"_blank\"><img height=\"60\" src=\"" . $logo . "\" /></a> ";
                        }
                    }
                }
            }

            $meeting_rooms = "";
            $meeting_rooms_setup = "";
            $layouts = array(0 => 'N/a', 1 => 'Cabaret', 2 => 'Theatre');

            foreach ($command->event->venueSpaces as $space) {

                $meeting_rooms .= $space->pivot->meeting_space_name . "<br/>";
                $meeting_rooms_setup .= $space->pivot->meeting_space_name . " Layout: " . $layouts[$space->pivot->layout_style] . " Tables: " . $space->pivot->num_of_tables . "<br/>";
            }

            $av_equipment = "<table><tr><td>Quantity</td><td>Item</td><td>Provided by</td></tr>";
            foreach ($command->event->technologies as $tech) {
                $av_equipment .= "<tr>";
                $av_equipment .= '<td>' . $tech->title . "</td><td>" . $tech->pivot->quantity . "</td><td>" . $tech->pivot->provided_by . "</td>";
                $av_equipment .= "</tr>";
            }
            $av_equipment .= "</table>";

            $hosts = "";
            foreach ($command->event->atendeesByRole(role('host')) as $host) {
                $hosts .= $host->first_name . ' ' . $host->last_name . ' ';
                if (!empty($host->mobile)) $hosts .= $host->mobile . '; ';
                if (!empty($host->phone)) $hosts .= $host->phone . '; ';
                $hosts .= "<br/>";
            }

            $tfi_contacts = "";
            foreach ($command->event->atendeesByRole(role('event_manager'), role('event_coordinator')) as $contact) {
                $tfi_contacts .= $contact->first_name . ' ' . $contact->last_name . ' ';
                if (!empty($contact->mobile)) $tfi_contacts .= $contact->mobile . '; ';
                if (!empty($contact->phone)) $tfi_contacts .= $contact->phone . '; ';
                $tfi_contacts .= "<br/>";

            }

            $venue_costs = "";
            $command->total = 0;
            $attendees = $command->event->atendees()->where('registration_status_id', config('registrationstatus.booked'))->count();

            foreach ($command->event->costs()->where('venue_cost', 1)->get() as $cost) {

                $venue_costs .= $cost->title . ' £' . $cost->amount;
                if ($cost->per_person) $venue_costs .= " (per person)";
                $venue_costs .= " " . $attendees . " (attendees) ";
                if ($cost->contingency_no) $venue_costs .= $cost->contingency_no . " (contingency)";
                else $venue_costs .= "n/a";

                if ($cost->per_person) {
                    $line_total = $cost->amount * ($attendees + $cost->contingency_no);
                } else {
                    $line_total = $cost->amount;
                }

                $command->total += $line_total;
                $venue_costs .= " £" . $line_total . " <br/>";
            }

            $venue_costs .= "<p><b>Total Venue Cost (as of a certain date):</b> £" . $command->total . "</p>";

            $first_slot = $command->event->scheduleTimeSlots()->where('bookable', 1)->orderBy('start')->skip(0)->take(1)->first();
            $second_slot = $command->event->scheduleTimeSlots()->where('bookable', 1)->orderBy('start')->skip(1)->take(1)->first();

            $both_slots = EventAttendee::where('event_id', $command->event->id)->whereHas('slots', function ($q) use ($first_slot, $second_slot) {
                $q->registration_status_id = \Config::get('registrationstatus.booked');
                if (is_array($first_slot) && sizeof($first_slot)) $q->id = $first_slot->id;
                if (is_array($second_slot) && sizeof($second_slot)) $q->id = $second_slot->id;
            })->count();

            $first_slot = (is_array($first_slot) && sizeof($first_slot)) ? $first_slot->attendees()->where('registration_status_id', \Config::get('registrationstatus.booked'))->count() : 0;
            $second_slot = (is_array($second_slot) && sizeof($second_slot)) ? $second_slot->attendees()->where('registration_status_id', \Config::get('registrationstatus.booked'))->count() : 0;

            $search = [
                'EVENT_NAME',
                'EVENT_DESCRIPTION',
                'EVENT_LINK',
                'EDIT_MY_EVENT',
                'EVENT_CONTRIBUTORS',
                'EVENT_START',
                'EVENT_END',
                'EVENT_REGION',
                'EVENT_SPONSORS',
                'EVENT_AGENDA',
                'MEETING_ROOMS_SETUP',
                'MEETING_ROOMS',
                'DELEGATES_NUMBER',
                'EVENT_HASHTAGS',
                'DATE_TODAY',
                'VENUE_ATTENDEE_LIST',
                'VENUE_COSTS',

                'AV_EQUIPMENT',
                'HOSTESSES',
                'DUTY_MANAGER',
                'AV_CONTACT',
                'TFI_CONTACTS',

                'ATTENDEES_FIRST_SLOT',
                'ATTENDEES_SECOND_SLOT',
                'ATTENDEES_BOTH_SLOT',
                'PAYMENT_TEXT'
            ];
            $replace = [

                $command->event->title,
                $command->event->description,
                $this->prepareLink($sent_email->id, $sent_email->to, route('events.view', $command->event->slug)),
                $this->prepareLink($sent_email->id, $sent_email->to, route('events.editBooking', $command->event->slug)),
                $this->prepareLink($sent_email->id, $sent_email->to, route('events.contributors', $command->event->slug)),
                date("d/m/Y", strtotime($command->event->event_date_from)),
                date("d/m/Y", strtotime($command->event->event_date_to)),
                (is_array($command->event->region) && sizeof($command->event->region)) ? $command->event->region->title : '',
                $sponsors_text,
                $this->prepareLink($sent_email->id, $sent_email->to, route('events.agenda', $command->event->slug)),
                $meeting_rooms_setup,
                $meeting_rooms,
                $delegates_number,
                $hashtags,
                date("d/m/Y"),
                $this->prepareLink($sent_email->id, $sent_email->to, route('events.attendeesList', $command->event->slug)),
                $venue_costs,
                $av_equipment,
                $hosts,
                (is_array($command->event->onSiteDetails) && sizeof($command->event->onSiteDetails)) ? $command->event->onSiteDetails->duty_manager_name . ' ' . $command->event->onSiteDetails->duty_manager_number : '',
                (is_array($command->event->onSiteDetails) && sizeof($command->event->onSiteDetails)) ? $command->event->onSiteDetails->av_technician_name . ' ' . $command->event->onSiteDetails->av_technician_number : '',
                $tfi_contacts,
                $first_slot,
                $second_slot,
                $both_slots,
                $payment_text
            ];
            $message_text = str_replace($search, $replace, $message_text);
            $subject = str_replace($search, $replace, $subject);

            $slots = $command->event->scheduleTimeSlots()->where('bookable', 1)->orderBy('start')->with(['sessions' => function ($query) {
                $query->orderBy('session_start');
                $query->with('contributors');
            }])->get();

            foreach ($slots as $slot) {

                foreach ($slot->sessions as $session) {

                    // sessions and times in rows
                    $all_event_sessions_times .= "<p>" . date("H:i", strtotime($session->session_start)) . " - " . date("H:i", strtotime($session->session_end)) . "<b>" . $session->title . "</b></p>";

                    $all_event_sessions .= "<p>" . date("H:i", strtotime($session->session_start)) . " - " . date("H:i", strtotime($session->session_end)) . "<b>" . $session->title . "</b></p>";

                    // join all contributors with bio
                    foreach ($session->contributors as $c) {

                        $all_event_sessions .= "<p>" . $c->title . " " . $c->badge_name . "</p>";
                        $all_event_sessions .= $c->bio . "<br/>";
                    }

                    $all_event_sessions .= "<br/> <hr/>";
                }
            }
        }

        // =====================================================================================================

        if (isset($command->user) && !empty($command->user) && isset($command->event) && !empty($command->event)) {

            $sessions_types_cpd = SessionType::where('include_in_cpd', 1)->lists('id');

            $registration = $command->event->atendees()->wherePivot('user_id', $command->user->id)
                ->wherePivot('registration_status_id', \Config::get('registrationstatus.booked'))->first();

            // if registration was found check for registered slots
            if ($registration) {

                $command->event_attendee_id = $registration->pivot->id;
                $slots_id = EventAttendeeSlotSession::where('event_attendee_id', $command->event_attendee_id)
                    ->where(function ($q) {
                        $q->where('registration_status_id', \Config::get('registrationstatus.booked'));
                        $q->orWhere('registration_status_id', \Config::get('registrationstatus.waitlisted'));
                    })
                    ->get();

                // collect all booked slots by attendee
                foreach ($slots_id as $slot_reg) {

                    $slot = $command->event->scheduleTimeSlots()->find($slot_reg->event_schedule_slot_id);

                    if (!$slot) continue;

                    if ($slot_reg->registration_status_id == \Config::get('registrationstatus.booked')) {
                        $booked_slots .= date("H:i", strtotime($slot->start)) . ' - ' . date("H:i", strtotime($slot->end)) . ' ' . $slot->title . "<br/>";
                    } else {
                        $booked_slots .= date("H:i", strtotime($slot->start)) . ' - ' . date("H:i", strtotime($slot->end)) . ' ' . $slot->title . " (Waiting list)<br/>";
                        $waitlisted_slots .= date("H:i", strtotime($slot->start)) . ' - ' . date("H:i", strtotime($slot->end)) . ' ' . $slot->title . " (Waiting list)<br/>";
                    }

                    // get slot sessions. and check if session type includes in CPD time calculation
                    $slot_sessions = $slot->sessions;

                    foreach ($slot_sessions as $session) {

                        if (in_array($session->session_type_id, $sessions_types_cpd))
                            $sessions_duration += (strtotime($session->session_end) - strtotime($session->session_start)) / 60;
                    }
                }
            }

            $sessions_duration = round($sessions_duration);

            $minutes = $sessions_duration % 60;
            $hours = ($sessions_duration - $minutes) / 60;

            $sessions_duration = $hours . " hours and " . $minutes . " minutes";
        }

        $search = ['BOOKED_SESSIONS', 'WAITLISTED_SESSIONS', 'SESSIONS_DURATION', 'ALL_EVENT_SESSIONS', 'EVENT_SESSIONS_TIMES',
            'EVENT_SCHEDULE_START',
            'EVENT_SCHEDULE_END'];
        $replace = [$booked_slots, $waitlisted_slots, $sessions_duration, $all_event_sessions, $all_event_sessions_times, $schedule_start, $schedule_end];
        $message_text = str_replace($search, $replace, $message_text);

        # VENUE SHORTCODES
        $venue_name = "";
        $venue_county = "";
        $venue_postcode = "";
        $venue_address = "";
        $venue_email = "";
        $venue_phone = "";
        $venue_contact = "";
        $venue_min_guaranteed = "";

        if (sizeof($command->event) && sizeof($command->event->venue)) {

            $venue_name = $command->event->venue->name;
            $venue_county = $command->event->venue->county;
            $venue_postcode = $command->event->venue->postcode;
            $venue_address = $command->event->venue->address;
            $venue_email = $command->event->venue->email;
            $venue_phone = $command->event->venue->phone;
            $venue_contact = (is_array($command->event->venue->contactPerson) && sizeof($command->event->venue->contactPerson)) ? $command->event->venue->contactPerson->first_name . ' ' . $command->event->venue->contactPerson->last_name : '';
            $venue_min_guaranteed = $command->event->venue->min_guaranteed;
        }

        $search = [
            'EVENT_VENUE_NAME', 'EVENT_VENUE_COUNTY', 'EVENT_VENUE_POSTCODE', 'EVENT_VENUE_ADDRESS',
            'EVENT_VENUE_EMAIL', 'EVENT_VENUE_PHONE', 'VENUE_CONTACT', 'MIN_GUARANTEED_NUMBERS'
        ];
        $replace = [$venue_name, $venue_county, $venue_postcode, $venue_address, $venue_email, $venue_phone, $venue_contact, $venue_min_guaranteed];
        $message_text = str_replace($search, $replace, $message_text);

        // sessions data by id
        $pattern = "/SESSION_TYPE_[0-9]*/";
        preg_match_all($pattern, $message_text, $matches);
        $matches = $matches[0];

        foreach ($matches as $m) {

            $type_id = substr($m, 13);

            // find all sessions and replace with session title start - end
            $sessions = $command->event->scheduleSessions()->where('session_type_id', $type_id)->get();
            $replace = "";

            // collect tag sessions
            foreach ($sessions as $s) {
                $replace .= date("H:i", strtotime($s->session_start)) . ' - ' . date("H:i", strtotime($s->session_end)) . ' <br/> ';
            }

            $message_text = str_replace($m, $replace, $message_text);
        }

        // =====================================================================================================
        // BOOKED SESSIONS SHORTCODES
        if (isset($command->booked_sessions) && !empty($command->booked_sessions)) {

            $session_name = !empty($command->booked_sessions['session_name']) ? $command->booked_sessions['session_name'] : "";

            $search = ['SESSION_NAME'];
            $replace = [$session_name];
            $message_text = str_replace($search, $replace, $message_text);
        }

        // add to user history that the email was sent
        if (isset($command->user)) {

            $command->user_action = new UserAction();
            $command->user_action->user_id = $command->user->id;
            $command->user_action->action = "emailSent";
            $command->user_action->details = "Email was sent to user. " . $subject;
            $command->user_action->save();
        }

        if (!empty($command->from)) {
            $command->from = $command->from;
        } else {
            $command->from = (isset($command->event) && isset($command->event->eventType->emailAddress)) ? $command->event->eventType->emailAddress->email : \Config::get('mail.host_email');
        }

        $sent_email->subject = $subject;
        $sent_email->template = $message_text;
        $sent_email->save();

        if (!empty($command->cc)) {
            $command->cc = array_map('trim', explode(';', $command->cc));
        }

        // TRACKING

        // Links tracking. Find all links and replace with link click log service
        /* moved to tag replacement
        $pattern = '/\b(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)[-A-Z0-9+&@#\/%=~_|$?!:,.]*[A-Z0-9+&@#\/%=~_|$]/i';
        preg_match_all($pattern, $message_text, $matches);

        if (!empty($matches[0])) {
            $matches = $matches[0];
            $replace = array();

            foreach ($matches as $link) {
                $replace[] = route('emails.logClick', [$sent_email->id, $sent_email->to])."?url=".$link;
            }

            $message_text = str_replace($matches, $replace, $message_text);
        }
        */
        // attach tracking pixel if user is in db
        $message_text .= ' <img src="' . route('emails.markOpened', [$sent_email->id, $sent_email->to]) . '?img=1px.png" />';

		\Mail::send('emails.default', ['main_content' => $message_text, 'event' => $command->event], function($message) use($command, $subject) {
		    $message->from($command->from);
		    $message->to($command->to);
		    $message->subject($subject);
            if (!empty($command->cc)) $message->cc($command->cc);
		});
	}

    private function prepareLink($email_id, $to, $link)
    {
        return route('emails.logClick', [$email_id, $to])."?url=".$link;
    }

}
