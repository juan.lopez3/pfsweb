<?php namespace App\Handlers\Events;

use App\Events\DataWasManipulated;
use App\UserAction;

class LogUserAction{
	
	public $user_action;
	
	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct(UserAction $user_action)
	{
		$this->user_action = $user_action;
	}

	/**
	 * Handle the event.
	 *
	 * @param  UserActionWasFired  $event
	 * @return void
	 */
	public function handle(DataWasManipulated $event)
	{
		$this->user_action->create([
			'user_id' => $event->user_id,
			'action' => $event->action,
			'details' => \Config::get('actiontypes.'.$event->action).' '. $event->additional_info,
			'ip' => $event->ip,
			'user_agent' => $event->user_agent
		]);
	}

}
