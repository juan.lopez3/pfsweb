<?php namespace App\Handlers\Events;

use App\Events\EmailWasSent;
use App\SentEmail;
use App\Events\DataWasManipulated;


class EmailWasSentFixation {

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  EmailWasSent  $event
	 * @return void
	 */
	public function handle(EmailWasSent $event)
	{
		$sent_email = new SentEmail();
		$sent_email->to = $event->to;
		$sent_email->subject = $event->subject;
		$sent_email->template = $event->template;
		$sent_email->user_id = $event->user_id;
		$sent_email->event_id = $event->event_id;
		$sent_email->campaign = $event->campaign;
		$sent_email->save();
		event(new DataWasManipulated('emailSent', 'Event ID: '.$event->event_id.', To: '.$event->to.', Subject: '.$event->subject));

		return $sent_email;
	}

}
