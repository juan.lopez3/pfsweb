<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EventCost extends Model {

	protected $table = "event_costs";
	
	protected $guarded = ['id'];
	
	public function event() {
		
		return $this->belongsTo('App\Event');
	}
	
}
