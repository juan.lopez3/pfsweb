<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EventAtendeeFavouriteSession extends Model {

	public $guarded = ['id'];

	public $timestamps = false;

}
