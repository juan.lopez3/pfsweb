<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DietaryRequirement extends Model {

	protected $table = "dietary_requirements";
	
	protected $guarded = ['id'];
}
