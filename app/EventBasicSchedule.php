<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EventBasicSchedule extends Model {

	protected $table = "event_basic_schedules";
	
	protected $guarded = ['id'];
	
}
