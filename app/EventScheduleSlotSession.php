<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EventScheduleSlotSession extends Model {

	protected $table = "event_schedule_slot_sessions";
	
	protected $guarded = ['id'];
	
	public function contributors() {

		return $this->belongsToMany('App\User', 'event_schedule_slot_session_user', 'slot_session_id', 'user_id')
            ->withPivot('type')
            ->orderByRaw("FIELD(pivot_type, 'Speaker', 'Chair', 'Chairperson', 'Stage Host')")
            ->orderBy('first_name');
	}
	
	public function type() {
		
		return $this->belongsTo('App\SessionType', 'session_type_id');
	}

    public function meetingSpace() {
        return $this->belongsTo('App\EventVenueRoom');
    }

    public function registrations() {
	    return $this->belongsToMany('App\EventAttendee', 'event_atendee_favourite_sessions', 'slot_session_id', 'event_attendee_id');
    }
	
	public function slot() {
		
		return $this->belongsTo('App\EventScheduleSlot', 'event_schedule_slot_id');
	}

	public function feedbacks() {

		return $this->hasMany('App\EventFeedback', 'session_id')->orderBy('position');
	}
}
