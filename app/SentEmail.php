<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class SentEmail extends Model {

	protected $table = "sent_emails";
	
	protected $guarded = ['id'];
	
	public function user() {

		return $this->belongsTo('App\User');
	}

	public function event() {

		return $this->belongsTo('App\Event');
	}

	public function clicksLog() {
	    return $this->hasMany('App\ClickedLinksLog');
    }
}
