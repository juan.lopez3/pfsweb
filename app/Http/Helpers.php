<?php

define('IMAGE', 'uploads/images');
define('FILE', 'uploads/files');
define('SPONSOR', 'uploads/sponsors');
define('PROFILE', 'uploads/profile_images');
define('BRANDING', 'uploads/branding');
define('AGENDA', 'uploads/agendas');
define('APP', 'uploads/app');
define('VENUE', 'uploads/venue_files');
define('GALLERY', 'uploads/gallery');

function uploadFile($file, $location, $width = null, $height = null) {
	
	if ($file) {
		
		$extension = $file->getClientOriginalExtension();
		$file_name = str_replace(" ", "", $file->getClientOriginalName());
        
        $i = 1;
        $found = file_exists($location.'/'.$file_name);
        
        while ($found) {
            
            if (!file_exists($location.'/'.$i.$file_name)) {
                
                $file_name = $i.$file_name;
                $found = false; // file name is free, no need o iterate more
            }
            
            $i++;
        }
        
        // move the file
		if (!empty($width)) {
		    \Image::make($file)->widen($width)->save($location.'/'.$file_name);
		} elseif (!empty($height)) {
		    \Image::make($file)->highten($height)->save($location.'/'.$file_name);
		} else {
		    $file->move($location, $file_name);
		}
		
		return $file_name;
	}
}

function removeFile($name, $location) {
	
	if (file_exists($location.'/'.$name)) {
		
		unlink($location.'/'.$name);
	}
}

/**
 * Helper function to check roles
 */
function hasRoles($user_role_id, $roles = array())
{
	if(in_array($user_role_id, $roles)) {
		
		return true;
	}
	
	return false;
}

/**
 * Short way to get role id by name
 */
function role($name)
{
	return \Config::get('roles.'.$name);
}

/*
|--------------------------------------------------------------------------
| Detect Active Route
|--------------------------------------------------------------------------
|
| Compare given route with current route and return output if they match.
| Very useful for navigation, marking if the link is active.
|
*/
function isActiveRoute($route, $output = "active") {
    if (Route::currentRouteName() == $route) return $output;
}

/*
|--------------------------------------------------------------------------
| Detect Active Routes
|--------------------------------------------------------------------------
|
| Compare given routes with current route and return output if they match.
| Very useful for navigation, marking if the link is active.
|
*/
function areActiveRoutes(Array $routes, $output = "active") {
    foreach ($routes as $route) {
    	
        if (Route::currentRouteName() == $route) return $output;
    }
}

/**
 * Array helper function for 1 to many relation sync
 */
function keysCreated($new, $old)
{
    return array_keys(array_diff_key($new, $old));
}

function keysDeleted($new, $old)
{
    return array_keys(array_diff_key($old, $new));
}
