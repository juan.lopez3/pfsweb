<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateEventSettingsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        	'client_name' => 'required',
        	'event_name' => 'required',
        	'event_date_from' => 'required',
        	'event_date_to' => 'required',
            'vat' => 'numeric|min:0|max:100',
        ];
    }
}
