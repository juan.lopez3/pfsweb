<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateTimeSlotRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			//'title' => 'required',
			'start' => 'required',
			'end' => 'required',
			//'meeting_space_id' => 'required',
			'learning_objectives' => 'max:500'
		];
	}

}
