<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateUserRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'title' => 'required',
			'email' => 'required|email',
			'cc_email' => 'email',
			'first_name' => 'required|max:40',
			'last_name' => 'required|max:40',
			'postcode' => 'required|postcode',
			'phone' => 'max:15',
			'mobile' => 'max:15',
			'profile_image' => 'mimes:jpg,jpeg,bmp,png,gif|max:1500',
			'bio' => 'max:2500',
			'phone' => 'regex:/^[\d\s()+-]+$/',
			'mobile' => 'regex:/^[\d\s()+-]+$/'
		];
	}

}
