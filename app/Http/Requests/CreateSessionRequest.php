<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateSessionRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'title' => 'required|max:200',
			'event_schedule_slot_id' => 'required',
			'session_start' =>'required',
			'session_end' => 'required'
		];
	}

}
