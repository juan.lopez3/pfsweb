<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SubmitBookingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        	'title' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'organisation_name' => 'required',
            'city' => 'required',
            'country_id' => 'required',
            'qualification_id' => 'required',
            'specialism_id' => 'required'
        ];
    }
}
