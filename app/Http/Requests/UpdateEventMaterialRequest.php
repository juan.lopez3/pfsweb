<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateEventMaterialRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'document_name' => 'required|max:100',
			'display_name' => 'required|max:100'
		];
	}

}
