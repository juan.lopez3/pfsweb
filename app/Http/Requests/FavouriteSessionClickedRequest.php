<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class FavouriteSessionClickedRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'session_id' => 'required|numeric',
            'checked' => 'required',
            'event_id' => 'required|numeric'
		];
	}

}
