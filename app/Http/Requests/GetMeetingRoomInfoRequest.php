<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class GetMeetingRoomInfoRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'meeting_room_id' => 'required',
			'layout_style_id' => 'required'
		];
	}

}
