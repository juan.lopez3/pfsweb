<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CopyEventRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'title' => 'required|unique:events|max:255',
			'old_event_id' => 'required'
		];
	}

}
