<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateEventRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'title' => 'required|max:255',
			'event_type_id' => 'required',
			'event_date_from' => 'required',
			'browser_title' => 'required|max:255',
			'banner' => 'mimes:jpeg,bmp,png,gif|max:1500',
			'description' => 'max:10000',
            'agenda_pdf_upload' => 'mimes:pdf|max:16000',
		];
	}

}
