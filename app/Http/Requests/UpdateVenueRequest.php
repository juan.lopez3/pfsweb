<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateVenueRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' => 'required|max:50',
			'email' => 'email',
			'address' => 'required|max:170',
			'city' => 'required|max:40',
			'county' => 'required|max:50',
			'postcode' => 'required|postcode',
			'venue_phone' => 'regex:/^[\d\s()+-]+$/'
		];
	}

}
