<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'admin', 'middleware' => 'backend', 'csrf'], function() {
	
	Route::pattern('id', '[0-9]+');
	Route::pattern('id2', '[0-9]+');
	
    Route::get('/','Admin\DashboardController@index');
    
	#Admin Dashboard
	Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'Admin\DashboardController@index']);
	Route::get('customEmails', ['as' => 'customEmails', 'uses' => 'customEmails@handle']);
	
	#Users
	Route::group(['middleware'=>'permissions', 'permissions' => [role('super_administrator'), role('event_manager'), role('event_coordinator')]], function(){
		Route::get('users', ['as' => 'users.list', 'uses' => 'Admin\UserController@index']);
		Route::get('users/datatable', ['as' => 'users.dataTable', 'uses' => 'Admin\UserController@usersDatatable']);
		Route::get('users/{id}/edit', ['as' => 'users.edit', 'uses' => 'Admin\UserController@edit']);
        Route::get('users/{id}/login', ['as' => 'users.login', 'uses' => 'Admin\UserController@login']);
	});
	
	Route::get('users/{id}/delete', ['as' => 'users.delete', 'uses' => 'Admin\UserController@destroy', 'middleware'=>'permissions', 'permissions' => [role('super_administrator')]]);
	Route::get('users/getlist', ['as' => 'users.getList', 'uses' => 'Admin\UserController@getList']);
	
	Route::group(['middleware'=>'permissions', 'permissions' => [role('super_administrator'), role('event_manager'), role('event_coordinator')]], function(){
		Route::get('users/create', ['as' => 'users.create', 'uses' => 'Admin\UserController@create']);
		Route::get('users/{id}/listactions', ['as' => 'users.listActions', 'uses' => 'Admin\UserController@indexActions']);
		Route::post('users/create', ['as' => 'users.store', 'uses' => 'Admin\UserController@store']);
		Route::patch('users/{id}', ['as' => 'users.update', 'uses' => 'Admin\UserController@update']);
	});
	
	Route::get('users/{id}/profile', ['as' => 'users.profile', 'uses' => 'Admin\UserController@profile']);
	Route::patch('users/{id}/profile', ['as' => 'users.updateProfile', 'uses' => 'Admin\UserController@updateProfile']);
		
	#Events
	Route::get('events/geteventslist', ['as' => 'events.geteventslist', 'uses' =>'Admin\EventController@getEventsList']);
	
	Route::group(['middleware'=>'permissions', 'permissions' => [role('super_administrator'), role('event_manager'),role('regional_coordinator'), role('pfs_staff'), role('venue_staff'), role('host'), role('event_coordinator')]], function(){
		Route::get('events/forthcoming', ['as' => 'events.forthcoming', 'uses' => 'Admin\EventController@indexForthcoming']);
		Route::get('events/previous', ['as' => 'events.previous', 'uses' => 'Admin\EventController@indexPrevious']);
		Route::get('events/archive', ['as' => 'events.archived', 'uses' => 'Admin\EventController@indexArchive']);
	});
    
    Route::group(['middleware'=>'permissions', 'permissions' => [role('super_administrator'), role('event_manager'), role('event_coordinator')]], function(){    
		Route::get('events/{id}/edit', ['as' => 'events.edit', 'uses' => 'Admin\EventController@edit']);
	
		Route::get('events/create', ['as' => 'events.create', 'uses' => 'Admin\EventController@create']);
		Route::get('events/{id}/delete', ['as' => 'events.delete', 'uses' => 'Admin\EventController@deleteEvent']);
		Route::get('events/{id}/restore', ['as' => 'events.restore', 'uses' => 'Admin\EventController@restoreEvent']);		
		Route::get('events/{id}/deleteBanner', ['as' => 'events.deleteBanner', 'uses' => 'Admin\EventController@deleteBanner']);
        Route::get('events/{id}/deleteAppBanner', ['as' => 'events.deleteAppBanner', 'uses' => 'Admin\EventController@deleteAppBanner']);
		Route::get('events/{id}/detachvenue', ['as' => 'events.detachVenue', 'uses' => 'Admin\EventController@detachVenue']);
		Route::post('events/copy', ['as' => 'events.copy', 'uses' => 'Admin\EventController@copyEvent']);
		Route::post('events/create', ['as' => 'events.store', 'uses' => 'Admin\EventController@store']);
		Route::post('events/{id}', ['as' => 'events.update', 'uses' => 'Admin\EventController@update']);
		Route::post('events/{id}/attachfile', ['as' => 'events.attachFile', 'uses' => 'Admin\EventController@attachFile']);
		Route::get('events/{id}/detachfile/{id2}', ['as' => 'events.detachFile', 'uses' => 'Admin\EventController@detachFile']);
		Route::post('events/uploadbanner', ['as' => 'events.uploadBanner', 'uses' => 'Admin\EventController@uploadBanner']);
        Route::post('events/uploadgallery', ['as' => 'events.uploadGallery', 'uses' => 'Admin\EventController@uploadGallery']);
        Route::get('events/{id}/deletegallery/{id2}', ['as' => 'events.deleteGallery', 'uses' => 'Admin\EventController@deleteGallery']);
        Route::post('events/uploadappbanner', ['as' => 'events.uploadAppBanner', 'uses' => 'Admin\EventController@uploadAppBanner']);
		Route::post('events/{id}/addTabNote', ['as' => 'events.notes.addNote', 'uses' =>'Admin\EventController@addTabNote']);
		Route::post('events/{id}/addsponsor', ['as' => 'events.addSponsor', 'uses' => 'Admin\EventSponsorController@create']);
        Route::get('events/{id}/archive', ['as' => 'events.archive', 'uses' => 'Admin\EventController@destroy']);
        
        Route::post('events/sponsors/updatetype', ['as' => 'events.sponsors.updateType', 'uses' => 'Admin\EventSponsorController@updateType']);
        Route::post('events/sponsors/updatestandnumber', ['as' => 'events.sponsors.updateStandNumber', 'uses' => 'Admin\EventSponsorController@updateStandNumber']);
        Route::post('events/sponsors/updatecategory/{category}', ['as' => 'events.sponsors.updateCategory', 'uses' => 'Admin\EventSponsorController@updateCategory']);

        Route::post('events/uploadFloorPlan', ['as' => 'events.uploadFloorPlan', 'uses' => 'Admin\EventController@uploadFloorPlan']);
        Route::get('events/{id}/removeFloorPlan', ['as' => 'events.removeFloorPlan', 'uses' => 'Admin\EventController@removeFloorPlan']);
		Route::get('events/{id}/deletesponsor/{id2}', ['as' => 'events.destroySponsor', 'uses' => 'Admin\EventSponsorController@destroy']);
	});
	
    Route::group(['middleware'=>'permissions', 'permissions' => [role('super_administrator'), role('event_manager'), role('event_coordinator'), role('host')]], function(){
    	# Emails
    	Route::get('events/{id}/emails/filter', ['as' => 'events.emails.filter', 'uses' => 'Admin\EmailController@index']);
    	Route::post('events/{id}/emails/filter', ['as' => 'events.emails.create', 'uses' => 'Admin\EmailController@create']);
    	Route::post('events/{id}/emails/savetemplates', ['as' => 'events.emails.saveTemplates', 'uses' => 'Admin\EmailController@savetemplates']);
    	Route::post('events/{id}/emails/send', ['as' => 'events.emails.send', 'uses' => 'Admin\EmailController@send']);
    	
    	# Statistics
    	Route::get('events/{id}/statistics', ['as' => 'events.statistics', 'uses' => 'Admin\EventStatisticController@index']);
    });
        
	Route::group(['middleware'=>'permissions', 'permissions' => [role('super_administrator'), role('event_manager'), role('event_coordinator')]], function(){
	    #EVENTS COSTS
        Route::get('events/{id}/costs', ['as' => 'events.costs', 'uses' => 'Admin\EventCostController@index']);
		Route::post('events/{id}/costs', ['as' => 'events.costs.update', 'uses' => 'Admin\EventCostController@update']);
	});
	
	#EVENTS SCHEDULE BUILDER
	Route::get('events/{id}/schedulebuilder', ['as' => 'events.scheduleBuilder', 'uses' => 'Admin\EventScheduleController@index']);
	
	
	Route::group(['middleware'=>'permissions', 'permissions' => [role('super_administrator'), role('event_manager'), role('event_coordinator')]], function(){
		
		#time slots
		Route::post('events/{id}/schedulebuilder/createtimeslot', ['as' => 'events.scheduleBuilder.storeTimeSlot', 'uses' => 'Admin\EventScheduleController@storeTimeSlot']);
		Route::get('events/{id}/schedulebuilder/deletetimeslot/{id2}', ['as' => 'events.scheduleBuilder.deleteTimeSlot', 'uses' => 'Admin\EventScheduleController@destroyTimeSlot']);
		Route::get('events/{id}/schedulebuilder/edittimeslot/{id2}', ['as' => 'events.scheduleBuilder.editTimeSlot', 'uses' => 'Admin\EventScheduleController@editTimeSlot']);
		Route::post('events/{id}/schedulebuilder/copyschedule', ['as' => 'events.scheduleBuilder.copySchedule', 'uses' => 'Admin\EventScheduleController@copySchedule']);
		Route::patch('events/{id}/schedulebuilder/edittimeslot/{id2}', ['as' => 'events.scheduleBuilder.updateTimeSlot', 'uses' => 'Admin\EventScheduleController@updateTimeSlot']);
        Route::post('events/schedulebuilder/updateSessionsOrder', ['as' => 'events.scheduleBuilder.updateSessionsOrder', 'uses' => 'Admin\EventScheduleController@updateSessionsOrder']);

		Route::put('events/{id}/schedulebuilder/updateexplanationtext', ['as' => 'events.scheduleBuilder.updateExplanationText', 'uses' => 'Admin\EventScheduleController@updateExplanationText']);
		
		#sessions
		Route::post('events/{id}/schedulebuilder/createsession', ['as' => 'events.scheduleBuilder.storeSession', 'uses' => 'Admin\EventScheduleController@storeSession']);
        Route::post('events/{id}/schedulebuilder/updatescheduleversion', ['as' => 'events.scheduleBuilder.updateScheduleVersion', 'uses' => 'Admin\EventScheduleController@updateScheduleVersion']);
		Route::get('events/{id}/schedulebuilder/deletesession/{id2}', ['as' => 'events.scheduleBuilder.deleteSession', 'uses' => 'Admin\EventScheduleController@destroySession']);
		Route::get('events/{id}/schedulebuilder/editsession/{id2}', ['as' => 'events.scheduleBuilder.editSession', 'uses' => 'Admin\EventScheduleController@editSession']);
		Route::patch('events/{id}/schedulebuilder/editsession/{id2}', ['as' => 'events.scheduleBuilder.updateSession', 'uses' => 'Admin\EventScheduleController@updateSession']);
		
        Route::get('events/{id}schedulebuilder/generateCPDLog', ['as' => 'events.scheduleBuilder.generateCPDLog', 'uses' => 'Admin\EventScheduleController@generateCPDLog']);
        
		# SCHEDULE BUILDER AJAX
		Route::post('events/{id}/schedulebuilder/updatevisibility', ['as' => 'events.scheduleBuilder.updateVisibility', 'uses' =>'Admin\EventScheduleController@updateVisibility']);
		Route::post('events/{id}/schedulebuilder/updatevisibilityApp', ['as' => 'events.scheduleBuilder.updateVisibilityApp', 'uses' =>'Admin\EventScheduleController@updateVisibilityApp']);
	});
	
	
	#EVENT ON SITE DETAILS
	Route::get('events/{id}/onsitedetails', ['as' => 'events.onsiteDetails', 'uses' => 'Admin\EventOnSiteDetailController@index']);
	
	#ON SITE CHECKIN - OUT, REG, EDIT
	Route::get('events/{id}/onsite', ['as' => 'events.onsite', 'uses' => 'Admin\OnSiteController@index']);
	Route::get('events/{id}/onsite/registration', ['as' => 'events.onsite.registration', 'uses' => 'Admin\OnSiteController@create']);
	Route::get('events/{id}/onsite/attendees', ['as' => 'events.onsite.attendees', 'uses' => 'Admin\OnSiteController@getAttendees']);
	
    Route::put('events/onsite/updateattendeecheckin', ['as' => 'events.onsite.updateAttendeeCheckin', 'uses' => 'Admin\OnSiteController@updateAttendeeCheckin']);
    Route::put('events/onsite/updateattendeechechout', ['as' => 'events.onsite.updateAttendeeCheckout', 'uses' => 'Admin\OnSiteController@updateAttendeeCheckout']);
	Route::put('events/{id}/onsite/updateListAttendance', ['as' => 'events.onsite.updateListAttendance', 'uses' => 'Admin\OnSiteController@updateListAttendanceCheck']);
    Route::post('events/{id}/onsite/importrecordedattendance', ['as' => 'events.onsite.importRecordedAttendance', 'uses' => 'Admin\OnSiteController@importRecordedAttendance']);

    Route::get('events/onsite/clearcheckin', ['as' => 'events.onsite.clearcheckin', 'uses' => 'Admin\OnSiteController@clearCheckin']);
    Route::get('events/onsite/clearcheckout', ['as' => 'events.onsite.clearcheckout', 'uses' => 'Admin\OnSiteController@clearCheckout']);
    
	Route::post('events/{id}/onsite/registration/{user_id}', ['as' => 'events.onsite.store', 'uses' => 'Admin\OnSiteController@store']);
	Route::post('events/{id}/onsite/checkin', ['as' => 'events.onsite.checkin', 'uses' => 'Admin\OnSiteController@checkin']);
	Route::post('events/{id}/onsite/checkout', ['as' => 'events.onsite.checkout', 'uses' => 'Admin\OnSiteController@checkout']);
	
	Route::group(['middleware'=>'permissions', 'permissions' => [role('super_administrator'), role('event_manager'), role('event_coordinator')]], function(){
		Route::post('events/{id}/onsitedetails', ['as' => 'events.onsiteDetails.update', 'uses' => 'Admin\EventOnSiteDetailController@update']);
		Route::post('events/onsitedetails/getmeetingroominfo', ['as' => 'events.onsiteDetails.getMeetingRoomInfo', 'uses' => 'Admin\EventOnSiteDetailController@getMeetingRoomInfo']);
		Route::post('events/onsitedetails/getpersoninfo', ['as' => 'events.onsiteDetails.getPersonInfo', 'uses' => 'Admin\EventOnSiteDetailController@getPersonInfo']);
		Route::get('events/{id}/onsitedetails/checksessions', ['as' => 'events.onsiteDetails.checkSessions', 'uses' => 'Admin\EventOnSiteDetailController@checkSpaceInSessions']);
	});
		
	#Events contributor TAB
	Route::get('events/contributors/getecontributorslist', ['as' => 'events.contributors.getlist', 'uses' =>'Admin\EventContributorController@getContributorsList']);
    Route::post('events/contributors/togglecomitee', ['as' => 'events.contributors.toggleComitee', 'uses' =>'Admin\EventContributorController@toggleComitee']);
	Route::group(['middleware'=>'permissions', 'permissions' => [role('super_administrator'), role('event_manager'), role('event_coordinator')]], function(){
		Route::get('events/{id}/contributors/{id2}/delete', ['as' => 'events.contributors.delete', 'uses' => 'Admin\EventContributorController@destroy']);
		Route::post('events/{id}/contributors/copy', ['as' => 'events.contributors.copy', 'uses' => 'Admin\EventContributorController@copy']);
		Route::post('events/{id}/contributors/store', ['as' => 'events.contributors.store', 'uses' => 'Admin\EventContributorController@store']);
		Route::get('events/{id}/contributors/isuserinsessions', ['as' => 'events.contributors.checkContributor', 'uses' => 'Admin\EventContributorController@isUserInSessions']);
		Route::get('events/{id}/contributors/checkcontributors', ['as' => 'events.contributors.checkContributors', 'uses' => 'Admin\EventContributorController@checkSessionContributors']);
	});
	
	#Events feedback TAB
	Route::get('events/{id}/feedback/{id2}/delete', ['as' => 'events.feedback.delete', 'uses' => 'Admin\EventFeedbackController@destroy']);
	
    
    Route::get('events/{id}/leavefeedback/{id2}', ['as' => 'events.feedbacks.leave', 'uses' => 'Admin\EventFeedbackController@leaveFeedback']);
    Route::put('events/{id}/leavefeedback/{id2}', ['as' => 'events.feedbacks.submit', 'uses' => 'Admin\EventFeedbackController@submitFeedback']);
    
	Route::group(['middleware'=>'permissions', 'permissions' => [role('super_administrator'), role('event_manager'), role('event_coordinator')]], function(){

        Route::post('events/feedback/reposition', ['as' => 'events.feedback.reposition', 'uses' => 'Admin\EventFeedbackController@reposition']);
		Route::get('events/{id}/feedback/{id2}/edit', ['as' => 'events.feedback.edit', 'uses' => 'Admin\EventFeedbackController@edit']);
		Route::patch('events/{id}/feedback/{id2}', ['as' => 'events.feedback.update', 'uses' => 'Admin\EventFeedbackController@update']);
		Route::post('events/{id}feedback/store', ['as' => 'events.feedback.storeFromTab', 'uses' => 'Admin\EventFeedbackController@store']);
		Route::get('events/{id}feedback/generate', ['as' => 'events.feedback.generate', 'uses' => 'Admin\EventFeedbackController@generate']);
		Route::get('events/{id}feedback/generatePDF', ['as' => 'events.feedback.generatePDF', 'uses' => 'Admin\EventFeedbackController@generatePDF']);
	});
	
	
	Route::group(['middleware'=>'permissions', 'permissions' => [role('super_administrator'), role('event_manager'), role('event_coordinator')]], function(){
		#Events venue TAB
		Route::get('venues/getvenueslist', ['as' => 'venues.getvenueslist', 'uses' =>'Admin\VenueController@getVenuesList']);
		Route::get('events/venue/{id}/deleteImage/{id2}', ['as' => 'events.venue.deleteImage', 'uses' => 'Admin\VenueController@deleteImage']);
		Route::get('events/venue/checkroom', ['as' => 'events.venue.chechRoom', 'uses' => 'Admin\VenueController@checkRoom']);
		
		Route::post('events/venue/copy', ['as' => 'events.venue.copy', 'uses' => 'Admin\VenueController@copyVenue']);
		Route::post('events/venue/create', ['as' => 'events.venue.store', 'uses' => 'Admin\VenueController@store']);
		Route::post('events/venue/uploadbanner', ['as' => 'events.venue.uploadImage', 'uses' => 'Admin\VenueController@uploadImage']);
		
		Route::patch('events/{id}/venue/{id2}', ['as' => 'events.venue.update', 'uses' => 'Admin\VenueController@update']);
		
		#Event Basic schdule TAB
		Route::post('events{id}/schedule', ['as' => 'events.schedule.update', 'uses' => 'Admin\EventBasicScheduleController@update']);
	});
		
	
	#Event atendees
	Route::get('events/{id}/attendees', ['as' => 'events.atendees', 'uses' => 'Admin\EventAtendeeController@index']);
	Route::get('events/getAttendeeProfile', ['as' => 'users.getAttendeeProfile', 'uses' => 'Admin\EventAtendeeController@getAttendeeProfile']);
	
	Route::group(['middleware'=>'permissions', 'permissions' => [role('super_administrator'), role('event_manager'), role('event_coordinator'), role('host'), role('regional_coordinator')]], function(){
		Route::get('events/{id}/attendees/getattendeeslist', ['as' => 'events.atendees.getAttendeesList', 'uses' => 'Admin\EventAtendeeController@getAttendeesList']);
		Route::post('events/{id}/attendees/attachattendee', ['as' => 'event.atendees.attachattendee', 'uses' => 'Admin\EventAtendeeController@attachAttendee']);
		Route::post('events/{id}/attendees/invite', ['as' => 'event.atendees.invite', 'uses' => 'Admin\EventAtendeeController@sendInvitation']);
		Route::post('events/{id}/attendees/importattendees', ['as' => 'event.atendees.importAttendees', 'uses' => 'Admin\EventAtendeeController@importAttendees']);
		Route::get('/downloadExcel', ['as' => 'event.atendees.downloadExcel', 'uses' => 'Admin\EventAtendeeController@downloadExcel']);
	});
	
	Route::group(['middleware'=>'permissions', 'permissions' => [role('super_administrator'), role('event_manager'), role('event_coordinator')]], function(){
		Route::get('events/{id}/atendees/{id2}/show', ['as' => 'events.atendees.show', 'uses' => 'Admin\EventAtendeeController@show']);
		Route::post('events/{id}/atendees/{id2}/updatestatus', ['as' => 'events.atendees.updateStatus', 'uses' => 'Admin\EventAtendeeController@updateStatus']);
		Route::post('events/atendee/{id}/updatebookingstatus', ['as' => 'events.atendees.updateBookingStatus', 'uses' => 'Admin\EventAtendeeController@updateBookedSession']);
		Route::post('events/atendee/{id}/updatepaidstatus', ['as' => 'events.atendees.updatePaidStatus', 'uses' => 'Admin\EventAtendeeController@updatePaidStatus']);
        Route::post('events/atendee/{id}/updatenotes', ['as' => 'events.atendees.updateNotes', 'uses' => 'Admin\EventAtendeeController@updateNotes']);
        Route::post('events/atendee/{id}/updateManuallyCpd', ['as' => 'events.atendees.updateManuallyCpd', 'uses' => 'Admin\EventAtendeeController@updateManuallyCpd']);
        Route::post('events/atendee/{id}/updatemanualbookingstatus', ['as' => 'events.atendees.updateManualBookingStatus', 'uses' => 'Admin\EventAtendeeController@updateManualBookingStatus']);
        Route::post('events/atendee/{id}/updatefeewaived', ['as' => 'events.atendees.updatefeeWaived', 'uses' => 'Admin\EventAtendeeController@updatefeeWaived']);
        Route::post('events/atendee/{id}/updateonsitebookingstatus', ['as' => 'events.atendees.updateOnsiteBookingStatus', 'uses' => 'Admin\EventAtendeeController@updateOnsiteBookingStatus']);
        Route::post('events/atendee/{id}/updatecancellationreason', ['as' => 'events.atendees.updateCancellationReason', 'uses' => 'Admin\EventAtendeeController@updateCancellationReason']);
		Route::get('events/{id}/atendees/delete/{id2}', ['as' => 'events.atendees.delete', 'uses' => 'Admin\EventAtendeeController@destroy']);
	});
	
	
	Route::group(['middleware'=>'permissions', 'permissions' => [role('super_administrator'), role('event_manager'), role('event_coordinator')]], function(){
		#Events Event materials TAB
		Route::get('events/materials/{id}/deleteall', ['as' => 'events.materials.deleteAll', 'uses' => 'Admin\EventMaterialController@deleteAll']);
		Route::get('events/materials/{id}/delete/{id2}', ['as' => 'events.materials.delete', 'uses' => 'Admin\EventMaterialController@destroy']);
		Route::get('events/materials/{id}/edit', ['as' => 'events.materials.edit', 'uses' => 'Admin\EventMaterialController@edit']);
		
		Route::post('events/materials/store', ['as' => 'events.materials.store', 'uses' => 'Admin\EventMaterialController@store']);
		Route::patch('events/materials/{id}', ['as' => 'events.materials.update', 'uses' => 'Admin\EventMaterialController@update']);
		
		#Events FAQS TAB
		Route::get('events/faqs/{id}/deleteall', ['as' => 'events.faqs.deleteAll', 'uses' => 'Admin\EventFaqController@deleteAll']);
		Route::get('events/faqs/{id}/delete/{id2}', ['as' => 'events.faqs.delete', 'uses' => 'Admin\EventFaqController@destroy']);
		Route::get('events/faqs/{id}/create', ['as' => 'events.faqs.create', 'uses' => 'Admin\EventFaqController@create']);
		Route::get('events/faqs/{id}/edit', ['as' => 'events.faqs.edit', 'uses' => 'Admin\EventFaqController@edit']);
		
		Route::post('events/faqs/reposition', ['as' => 'events.faqs.reposition', 'uses' => 'Admin\EventFaqController@reposition']);
		Route::post('events/faqs/store', ['as' => 'events.faqs.store', 'uses' => 'Admin\EventFaqController@store']);
		Route::post('events/faqs/{id}/import', ['as' => 'events.faqs.import', 'uses' => 'Admin\EventFaqController@import']);
		
		Route::patch('events/faqs/{id}', ['as' => 'events.faqs.update', 'uses' => 'Admin\EventFaqController@update']);
	});
	
	Route::group(['middleware'=>'permissions', 'permissions' => [role('super_administrator'), role('event_manager'), role('event_coordinator'), role('host'), role('regional_coordinator')]], function(){
	    
        #Venues MENU
        Route::get('venues', ['as' => 'venues.list', 'uses' => 'Admin\VenueController@index']);
        Route::get('venues/{id}/edit', ['as' => 'venues.edit', 'uses' => 'Admin\VenueController@edit']);
        
		Route::get('venues/create', ['as' => 'venues.create', 'uses' => 'Admin\VenueController@create']);
		Route::post('venues/create', ['as' => 'venues.store', 'uses' => 'Admin\VenueController@storeFromMenu']);
		Route::post('venues/{id}/addnote', ['as' => 'venues.addNote', 'uses' => 'Admin\VenueController@addNote']);
        Route::post('venuenotes/changestatus', ['as' => 'venues.notes.changeStatus', 'uses' => 'Admin\VenueController@changeStatus']);
        Route::post('venuenotes/reorder', ['as' => 'venues.notes.reorder', 'uses' => 'Admin\VenueController@reorder']);
        Route::post('venuenotes/{id}/addcomment', ['as' => 'venues.notes.addComment', 'uses' => 'Admin\VenueController@addComment']);
		Route::post('venues/{id}/addnotetab', ['as' => 'venues.addNoteTab', 'uses' => 'Admin\VenueController@addNoteTab']);
		Route::patch('venues/{id}', ['as' => 'venues.update', 'uses' => 'Admin\VenueController@updateFromMenu']);
        
        Route::post('venues/uploadfile', ['as' => 'venues.uploadFile', 'uses' => 'Admin\VenueController@uploadFile']);
        Route::get('venues/removefile/{id}', ['as' => 'venues.removeFile', 'uses' => 'Admin\VenueController@removeFile']);
        Route::get('events/{id}/venues/removefile/{id2}', ['as' => 'events.venues.removeFile', 'uses' => 'Admin\VenueController@removeVenueFile']);
	});
	
	
	#Sponsors MENU
	Route::group(['middleware'=>'permissions', 'permissions' => [role('super_administrator'), role('event_manager'), role('event_coordinator')]], function(){
		Route::get('sponsors', ['as' => 'sponsors', 'uses' => 'Admin\SponsorController@index']);
	});
	
	Route::group(['middleware'=>'permissions', 'permissions' => [role('super_administrator'), role('event_manager'), role('event_coordinator')]], function(){
		Route::get('sponsors/create', ['as' => 'sponsors.create', 'uses' => 'Admin\SponsorController@create']);
		Route::get('sponsors/{id}/edit', ['as' => 'sponsors.edit', 'uses' => 'Admin\SponsorController@edit']);
		Route::get('sponsors/{id}/delete', ['as' => 'sponsors.delete', 'uses' => 'Admin\SponsorController@destroy']);
		Route::post('sponsorss/create', ['as' => 'sponsors.store', 'uses' => 'Admin\SponsorController@store']);
		Route::patch('sponsors/{id}', ['as' => 'sponsors.update', 'uses' => 'Admin\SponsorController@update']);
		Route::post('sponsor/uploadsponsorlogo', ['as' => 'sponsors.uploadLogo', 'uses' => 'Admin\SponsorController@uploadLogo']);
		Route::get('sponsors/{id}/deleteLogo', ['as' => 'sponsors.deleteLogo', 'uses' => 'Admin\SponsorController@destroyLogo']);
	});
	
	#FILES
	Route::group(['middleware'=>'permissions', 'permissions' => [role('super_administrator'), role('event_manager'), role('event_coordinator')]], function(){
		Route::get('files', ['as' => 'files', 'uses' => 'Admin\FileController@index']);
        Route::get('files/list', ['as' => 'files.list', 'uses' => 'Admin\FileController@filesList']);
		Route::get('files/create', ['as' => 'files.create', 'uses' => 'Admin\FileController@create']);
		Route::get('files/{id}/edit', ['as' => 'files.edit', 'uses' => 'Admin\FileController@edit']);
		Route::get('files/{id}/delete', ['as' => 'files.delete', 'uses' => 'Admin\FileController@destroy']);
		Route::post('files/create', ['as' => 'files.store', 'uses' => 'Admin\FileController@store']);
		Route::post('files/{id}/updatefile', ['as' => 'files.updateFile', 'uses' => 'Admin\FileController@updateFile']);
		Route::patch('files/{id}', ['as' => 'files.update', 'uses' => 'Admin\FileController@update']);
	});
	
	#Sent Emails log
	Route::group(['middleware'=>'permissions', 'permissions' => [role('super_administrator'), role('event_manager'), role('event_coordinator')]], function(){
		Route::get('emails', ['as' => 'emails', 'uses' => 'Admin\SentEmailController@index']);
		Route::get('emails/{id}/view', ['as' => 'emails.view', 'uses' => 'Admin\SentEmailController@show']);
		Route::get('emails/datatable', ['as' => 'emails.dataTable', 'uses' => 'Admin\SentEmailController@dataTable']);
	});
	
	#Reports MENU
	
    Route::group(['middleware'=>'permissions', 'permissions' => [role('super_administrator'), role('event_manager'),role('regional_coordinator'),role('member'), role('non_member'),  role('event_coordinator'), role('venue_staff'), role('pfs_staff')]], function(){
        Route::get('reports/client', ['as' => 'reports.client', 'uses' => 'Admin\ReportController@client']);
        
        // standard reports
        Route::post('reports/eventslot', ['as' => 'reports.standard.eventSlot', 'uses' => 'Admin\ReportController@eventSlot']);
		Route::post('reports/eventslotByRegion', ['as' => 'reports.standard.eventSlotByRegion', 'uses' => 'Admin\ReportController@eventSlotByRegionByQuarters']);
	   
		Route::post('reports/quaterbookings', ['as' => 'reports.standard.quaterBookings', 'uses' => 'Admin\ReportController@quaterBookings']);
        Route::post('reports/totalattendees', ['as' => 'reports.standard.totalAttendees', 'uses' => 'Admin\ReportController@totalAttendees']);
        Route::post('reports/regionalbookings', ['as' => 'reports.standard.regionalBookings', 'uses' => 'Admin\ReportController@regionalBookings']);
        Route::post('reports/attendancedesignation', ['as' => 'reports.standard.attendanceDesignation', 'uses' => 'Admin\ReportController@attendanceDesignation']);
        Route::post('reports/registrationquestions', ['as' => 'reports.standard.registrationQuestions', 'uses' => 'Admin\ReportController@registrationQuestions']);
        Route::post('reports/slotwaitinglist', ['as' => 'reports.standard.slotWaitingList', 'uses' => 'Admin\ReportController@slotWaitingList']);
        Route::post('reports/slotbookings', ['as' => 'reports.standard.slotBookings', 'uses' => 'Admin\ReportController@slotBookings']);
        Route::post('reports/attendeelist', ['as' => 'reports.standard.attendeeList', 'uses' => 'Admin\ReportController@attendeeList']); 
        Route::post('reports/attendeelistByDates', ['as' => 'reports.standard.attendeeListByDates', 'uses' => 'Admin\ReportController@attendeeListDates']);
        Route::post('reports/attendeeliststatus', ['as' => 'reports.standard.attendeeListStatus', 'uses' => 'Admin\ReportController@attendeeListStatus']);
        Route::post('reports/attendeelistsponsors', ['as' => 'reports.standard.attendeeListSponsors', 'uses' => 'Admin\ReportController@attendeeListSponsors']);
        Route::post('reports/feedbacks', ['as' => 'reports.standard.feedbacks', 'uses' => 'Admin\ReportController@feedback']);
        Route::post('reports/feedbackaverages', ['as' => 'reports.standard.feedbackAverages', 'uses' => 'Admin\ReportController@feedbackAverages']);
        Route::post('reports/openedemails', ['as' => 'reports.standard.openedEmails', 'uses' => 'Admin\ReportController@openedEmails']);
        Route::post('reports/clickedlinks', ['as' => 'reports.standard.clickedLinks', 'uses' => 'Admin\ReportController@clickedLinks']);
        Route::post('reports/emailcampaigns', ['as' => 'reports.standard.emailCampaigns', 'uses' => 'Admin\ReportController@emailCampaigns']);
        Route::post('reports/pfsmaster', ['as' => 'reports.standard.pfsMaster', 'uses' => 'Admin\ReportController@pfsMaster']);
        Route::post('reports/selectedsessions', ['as' => 'reports.standard.selectedSessions', 'uses' => 'Admin\ReportController@selectedSessions']);
    });
    
	Route::group(['middleware'=>'permissions', 'permissions' => [role('super_administrator'), role('event_manager'), role('regional_coordinator'), role('event_coordinator'), role('venue_staff'), role('pfs_staff')]], function(){
		Route::get('reports', ['as' => 'reports', 'uses' => 'Admin\ReportController@index']);
		Route::get('reports/custom', ['as' => 'reports.custom', 'uses' => 'Admin\ReportController@custom']);
		Route::get('reports/getconditionselect', ['as' => 'reports.getConditionSelect', 'uses' => 'Admin\ReportController@getConditionSelect']);
		Route::post('reports/generate', ['as' => 'reports.custom.generate', 'uses' => 'Admin\ReportController@generateCustomReport']);
		Route::post('reports/regenerate', ['as' => 'reports.custom.regenerate', 'uses' => 'Admin\ReportController@customRegenerate']);
		Route::get('reports/create', ['as' => 'reports.create', 'uses' => 'Admin\ReportController@create']);
		Route::get('reports/standard', ['as' => 'reports.standard', 'uses' => 'Admin\ReportController@standard']);
		Route::get('reports/{id}/edit', ['as' => 'reports.edit', 'uses' => 'Admin\ReportController@edit']);
		Route::get('reports/{id}/delete', ['as' => 'reports.delete', 'uses' => 'Admin\ReportController@destroy']);
		Route::patch('reports/{id}', ['as' => 'reports.update', 'uses' => 'Admin\ReportController@update']);
		
        # OUTSIDE REPORTS
        Route::get('reports/outside', ['as' => 'reports.outside', 'uses' => 'Admin\OutsideReportController@index']);
        Route::get('reports/outside/create', ['as' => 'reports.outside.create', 'uses' => 'Admin\OutsideReportController@create']);
        Route::get('reports/outside/{id}/edit', ['as' => 'reports.outside.edit', 'uses' => 'Admin\OutsideReportController@edit']);
        Route::get('reports/outside/{id}/delete', ['as' => 'reports.outside.delete', 'uses' => 'Admin\OutsideReportController@destroy']);
        Route::post('reports/outside/create', ['as' => 'reports.outside.store', 'uses' => 'Admin\OutsideReportController@store']);
        Route::patch('reports/outside/{id}', ['as' => 'reports.outside.update', 'uses' => 'Admin\OutsideReportController@update']);
        
		
		
		# REPORT TEMPLATES
		Route::get('reports/template', ['as' => 'reports.template', 'uses' => 'Admin\ReportTemplateController@index']);
		Route::get('reports/template/create', ['as' => 'reports.template.create', 'uses' => 'Admin\ReportTemplateController@create']);
		Route::get('reports/template/{id}/edit', ['as' => 'reports.template.edit', 'uses' => 'Admin\ReportTemplateController@edit']);
		Route::get('reports/template/{id}/delete', ['as' => 'reports.template.delete', 'uses' => 'Admin\ReportTemplateController@destroy']);
		Route::post('reports/template/create', ['as' => 'reports.template.store', 'uses' => 'Admin\ReportTemplateController@store']);
		Route::patch('reports/template/{id}', ['as' => 'reports.template.update', 'uses' => 'Admin\ReportTemplateController@update']);
		
		Route::post('reports/template/generate', ['as' => 'reports.template.generate', 'uses' => 'Admin\ReportTemplateController@generate']);
	});
	
	Route::group(['middleware'=>'permissions', 'permissions' => [role('super_administrator'), role('event_manager'), role('event_coordinator')]], function(){
		
		#Settings MENU
		Route::get('settings', ['as'=>'settings', 'uses' => function(){return view('admin.settings.list');}]);
		Route::get('settings/flushcache', ['as' => 'settings.flushCache', function(){ \Cache::flush(); return redirect()->route('settings'); }]);
		
        # MOBILE APP SETTINGS
        Route::get('settings/apitext', ['as' => 'settings.apiText', 'uses' => 'Admin\MobileAppTextController@edit']);
        Route::put('settings/apitext', ['as' => 'settings.apiText.update', 'uses' => 'Admin\MobileAppTextController@update']);
		
		#EventTypes
		Route::get('settings/eventtypes', ['as' => 'settings.eventTypes', 'uses' => 'Admin\EventTypeController@index']);
		Route::get('settings/eventtype/create', ['as' => 'settings.eventTypes.create', 'uses' => 'Admin\EventTypeController@create']);
		Route::get('settings/eventtype/{id}/edit', ['as' => 'settings.eventTypes.edit', 'uses' => 'Admin\EventTypeController@edit']);
		Route::get('settings/eventtype/{id}/delete', ['as' => 'settings.eventTypes.delete', 'uses' => 'Admin\EventTypeController@destroy']);
		Route::post('settings/eventtype/create', ['as' => 'settings.eventTypes.store', 'uses' => 'Admin\EventTypeController@store']);
		Route::patch('settings/eventtype/{id}', ['as' => 'settings.eventTypes.update', 'uses' => 'Admin\EventTypeController@update']);
		Route::post('settings/eventtype/uploadbranding', ['as' => 'settings.eventTypes.uploadBranding', 'uses' => 'Admin\EventTypeController@uploadBranding']);
        Route::post('settings/eventtype/uploademailbranding', ['as' => 'settings.eventTypes.uploadEmailBranding', 'uses' => 'Admin\EventTypeController@uploadEmailBranding']);
        Route::post('settings/eventtype/uploadappbranding', ['as' => 'settings.eventTypes.uploadAppBranding', 'uses' => 'Admin\EventTypeController@uploadAppBranding']);
		Route::post('settings/eventtype/{id}/attachfile', ['as' => 'settings.eventTypes.attachFile', 'uses' => 'Admin\EventTypeController@attachFile']);
        Route::get('settings/eventtype/{id}/detachfile/{id2}', ['as' => 'settings.eventTypes.detachFile', 'uses' => 'Admin\EventTypeController@detachFile']);
        
		#EventTypeSteps
		Route::get('settings/eventtypesteps/{id}', ['as' => 'settings.eventTypeSteps', 'uses' => 'Admin\EventTypeStepsController@show']);
		Route::get('settings/eventtypestep/{id}/create', ['as' => 'settings.eventTypeSteps.create', 'uses' => 'Admin\EventTypeStepsController@create']);
		Route::get('settings/eventtypestep/{id}/edit/{id2}', ['as' => 'settings.eventTypeSteps.edit', 'uses' => 'Admin\EventTypeStepsController@edit']);
		Route::get('settings/eventtypestep/{id}/delete{id2}', ['as' => 'settings.eventTypeSteps.delete', 'uses' => 'Admin\EventTypeStepsController@destroy']);
		Route::post('settings/eventtypestep/{id}/create', ['as' => 'settings.eventTypeSteps.store', 'uses' => 'Admin\EventTypeStepsController@store']);
		Route::patch('settings/eventtypestep/{id}', ['as' => 'settings.eventTypeSteps.update', 'uses' => 'Admin\EventTypeStepsController@update']);

        # SUBMENU
        Route::get('settings/submenu', ['as' => 'settings.submenu', 'uses' => 'Admin\SubmenuController@index']);
        Route::get('settings/submenu/create', ['as' => 'settings.submenu.create', 'uses' => 'Admin\SubmenuController@create']);
        Route::get('settings/submenu/{id}/edit', ['as' => 'settings.submenu.edit', 'uses' => 'Admin\SubmenuController@edit']);
        Route::get('settings/submenu/{id}/delete', ['as' => 'settings.submenu.delete', 'uses' => 'Admin\SubmenuController@destroy']);
        Route::post('settings/submenu/create', ['as' => 'settings.submenu.store', 'uses' => 'Admin\SubmenuController@store']);
		Route::put('settings/submenu/{id}', ['as' => 'settings.submenu.update', 'uses' => 'Admin\SubmenuController@update']);
		
		# HIGHLIGHTS
        Route::get('settings/highlights', ['as' => 'settings.highlights', 'uses' => 'Admin\HighlightsController@index']);
        Route::post('settings/highlights/create', ['as' => 'settings.highlights.store', 'uses' => 'Admin\HighlightsController@store']);
        Route::get('settings/highlights/{id}/delete', ['as' => 'settings.highlights.delete', 'uses' => 'Admin\HighlightsController@destroy']);

		#EventTypeStep Form
		Route::get('settings/eventtypesteps/{id}/stepform', ['as' => 'settings.eventTypeStep.form.edit', 'uses' => 'Admin\TypeStepFormController@index']);
		Route::patch('settings/eventtypesteps/{id}/stepform', ['as' => 'settings.eventTypeStep.form.update', 'uses' => 'Admin\TypeStepFormController@update']);
		
		#Sponsors settings
		Route::get('settings/sponsors', ['as' => 'settings.sponsors', 'uses' => 'Admin\SponsorSettingController@index']);
		
		Route::get('settings/sponsors/create', ['as' => 'settings.sponsors.create', 'uses' => 'Admin\SponsorSettingController@create']);
		Route::get('settings/sponsors/{id}/edit', ['as' => 'settings.sponsors.edit', 'uses' => 'Admin\SponsorSettingController@edit']);
		Route::get('settings/sponsors/{id}/deleterow', ['as' => 'settings.sponsors.deleteRow', 'uses' => 'Admin\SponsorSettingController@destroyRow']);
		
		Route::post('settings/sponsors/delete', ['as' => 'settings.sponsors.delete', 'uses' => 'Admin\SponsorSettingController@destroy']);
		Route::post('settings/sponsors/typeupdate', ['as' => 'settings.sponsors.typeUpdate', 'uses' => 'Admin\SponsorSettingController@typeUpdate']);
		Route::post('settings/sponsors/create', ['as' => 'settings.sponsors.store', 'uses' => 'Admin\SponsorSettingController@store']);
		Route::post('settings/sponsors/addsponsor', ['as' => 'settings.sponsors.addSponsor', 'uses' => 'Admin\SponsorSettingController@addSponsor']);
		Route::patch('settings/sponsors/{id}', ['as' => 'settings.sponsors.update', 'uses' => 'Admin\SponsorSettingController@update']);
		
		#SessionTypes
		Route::get('settings/sessiontypes', ['as' => 'settings.sessionTypes', 'uses' => 'Admin\SessionTypeController@index']);
		Route::get('settings/sessiontypes/create', ['as' => 'settings.sessionTypes.create', 'uses' => 'Admin\SessionTypeController@create']);
		Route::get('settings/sessiontypes/{id}/edit', ['as' => 'settings.sessionTypes.edit', 'uses' => 'Admin\SessionTypeController@edit']);
		Route::get('settings/sessiontypes/{id}/delete', ['as' => 'settings.sessionTypes.delete', 'uses' => 'Admin\SessionTypeController@destroy']);
		Route::post('settings/sessiontypes/create', ['as' => 'settings.sessionTypes.store', 'uses' => 'Admin\SessionTypeController@store']);
		Route::patch('settings/sessiontypes/{id}', ['as' => 'settings.sessionTypes.update', 'uses' => 'Admin\SessionTypeController@update']);
		
		#Technique
		Route::get('settings/technologies', ['as' => 'settings.technologies', 'uses' => 'Admin\TechnologyController@index']);
		Route::get('settings/technologies/create', ['as' => 'settings.technologies.create', 'uses' => 'Admin\TechnologyController@create']);
		Route::get('settings/technologies/{id}/edit', ['as' => 'settings.technologies.edit', 'uses' => 'Admin\TechnologyController@edit']);
		Route::get('settings/technologies/{id}/delete', ['as' => 'settings.technologies.delete', 'uses' => 'Admin\TechnologyController@destroy']);
		Route::post('settings/technologies/create', ['as' => 'settings.technologies.store', 'uses' => 'Admin\TechnologyController@store']);
		Route::patch('settings/technologies/{id}', ['as' => 'settings.technologies.update', 'uses' => 'Admin\TechnologyController@update']);
		
		#Attendee types
		Route::get('settings/attendeetypes', ['as' => 'settings.attendeeTypes', 'uses' => 'Admin\AttendeeTypeController@index']);
		Route::get('settings/attendeetypes/create', ['as' => 'settings.attendeeTypes.create', 'uses' => 'Admin\AttendeeTypeController@create']);
		Route::get('settings/attendeetypes/{id}/edit', ['as' => 'settings.attendeeTypes.edit', 'uses' => 'Admin\AttendeeTypeController@edit']);
		Route::get('settings/attendeetypes/{id}/delete', ['as' => 'settings.attendeeTypes.delete', 'uses' => 'Admin\AttendeeTypeController@destroy']);
		Route::post('settings/attendeetypes/create', ['as' => 'settings.attendeeTypes.store', 'uses' => 'Admin\AttendeeTypeController@store']);
		Route::patch('settings/attendeetypes/{id}', ['as' => 'settings.attendeeTypes.update', 'uses' => 'Admin\AttendeeTypeController@update']);
		
		#Regions
		Route::get('settings/regions', ['as' => 'settings.regions', 'uses' => 'Admin\RegionController@index']);
		Route::get('settings/regions/create', ['as' => 'settings.regions.create', 'uses' => 'Admin\RegionController@create']);
		Route::get('settings/regions/{id}/edit', ['as' => 'settings.regions.edit', 'uses' => 'Admin\RegionController@edit']);
		Route::get('settings/regions/{id}/delete', ['as' => 'settings.regions.delete', 'uses' => 'Admin\RegionController@destroy']);
		Route::post('settings/regions/create', ['as' => 'settings.regions.store', 'uses' => 'Admin\RegionController@store']);
		Route::patch('settings/regions/{id}', ['as' => 'settings.regions.update', 'uses' => 'Admin\RegionController@update']);
		
		#EmailTemplates
		Route::get('settings/emailtemplates', ['as' => 'settings.emailTemplates', 'uses' => 'Admin\EmailTemplateController@index']);;
		Route::get('settings/emailtemplates/create', ['as' => 'settings.emailTemplates.create', 'uses' => 'Admin\EmailTemplateController@create']);
		Route::get('settings/emailtemplates/loadtemplate', ['as' => 'settings.emailTemplates.loadtemplate', 'uses' => 'Admin\EmailTemplateController@loadtemplate']);
		Route::post('settings/emailtemplates/create', ['as' => 'settings.emailTemplates.store', 'uses' => 'Admin\EmailTemplateController@store']);
		Route::get('settings/emailtemplates/{id}/edit', ['as' => 'settings.emailTemplates.edit', 'uses' => 'Admin\EmailTemplateController@edit']);
		Route::patch('settings/emailtemplates/{id}', ['as' => 'settings.emailTemplates.update', 'uses' => 'Admin\EmailTemplateController@update']);
		Route::get('settings/emailtemplates/{id}/delete', ['as' => 'settings.emailTemplates.delete', 'uses' => 'Admin\EmailTemplateController@destroy']);
        Route::get('settings/emailtemplates/listarchive', ['as' => 'settings.emailTemplates.listArchive', 'uses' => 'Admin\EmailTemplateController@listArchive']);
        Route::get('settings/emailtemplates/{id}/unarchive', ['as' => 'settings.emailTemplates.unarchive', 'uses' => 'Admin\EmailTemplateController@unarchive']);
		
		#Email Addresses
		Route::get('settings/emailaddresses', ['as' => 'settings.emailAddresses', 'uses' => 'Admin\EmailAddressController@index']);
		Route::get('settings/emailaddresses/create', ['as' => 'settings.emailAddresses.create', 'uses' => 'Admin\EmailAddressController@create']);
		Route::get('settings/emailaddresses/{id}/edit', ['as' => 'settings.emailAddresses.edit', 'uses' => 'Admin\EmailAddressController@edit']);
		Route::get('settings/emailaddresses/{id}/delete', ['as' => 'settings.emailAddresses.delete', 'uses' => 'Admin\EmailAddressController@destroy']);
		Route::post('settings/emailaddresses/create', ['as' => 'settings.emailAddresses.store', 'uses' => 'Admin\EmailAddressController@store']);
		Route::patch('settings/emailaddresses/{id}', ['as' => 'settings.emailAddresses.update', 'uses' => 'Admin\EmailAddressController@update']);
		
        #ISSUE TRACKER
        Route::get('issues', ['as' => 'issues', 'uses' => 'Admin\IssueController@index']);;
        Route::get('issues/create', ['as' => 'issues.create', 'uses' => 'Admin\IssueController@create']);
        Route::get('issues/{id}/confirm', ['as' => 'issues.confirm', 'uses' => 'Admin\IssueController@confirm']);
        Route::post('issues/create', ['as' => 'issues.store', 'uses' => 'Admin\IssueController@store']);
        Route::get('issues/{id}/edit', ['as' => 'issues.edit', 'uses' => 'Admin\IssueController@edit']);
        Route::put('issues/{id}', ['as' => 'issues.update', 'uses' => 'Admin\IssueController@update']);
        Route::post('issues/uploadsponsorlogo', ['as' => 'issues.uploadImage', 'uses' => 'Admin\IssueController@uploadImage']);
        
        # THEMES
        Route::get('settings/themes', ['as' => 'settings.themes', 'uses' => 'Admin\ThemeController@index']);
        Route::get('settings/themes/create', ['as' => 'settings.themes.create', 'uses' => 'Admin\ThemeController@create']);
        Route::get('settings/themes/{id}/edit', ['as' => 'settings.themes.edit', 'uses' => 'Admin\ThemeController@edit']);
        Route::get('settings/themes/{id}/delete', ['as' => 'settings.themes.delete', 'uses' => 'Admin\ThemeController@destroy']);
        Route::post('settings/themes/create', ['as' => 'settings.themes.store', 'uses' => 'Admin\ThemeController@store']);
        Route::patch('settings/themes/{id}', ['as' => 'settings.themes.update', 'uses' => 'Admin\ThemeController@update']);
        Route::post('settings/themes//upload', ['as' => 'settings.themes.upload', 'uses' => 'Admin\ThemeController@upload']);
        
		#About
		Route::get('settings/about', ['as' => 'settings.about', function(){ return view('admin.settings.about');}]);
	});
});

Route::get('admin/login', ['as' => 'admin.login', 'uses' => 'Auth\AuthController@getLogin']);