<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Country;
use App\User;
use App\UserFeedback;
use App\Event;
use App\AttendeeType;
use App\RegistrationStatus;
use App\DietaryRequirement;
use App\EventAttendee;
use Carbon\Carbon;
use App\Venue;

use App\Events\DataWasManipulated;

class UserController extends Controller {
	
	public $user;
	public $log_desc = "self profile";
	
	public function __construct(User $user)
	{
		$this->user = $user;
	}
	/**
	 * Display the specified resource.
	 *
	 * @return Response
	 */
	public function show()
	{
		$reminders = $this->getUserReminders();
		$next_event = \Auth::user()->events()->where('event_date_from', '>=', date("Y-m-d"))->orderBy('event_date_from')
			->wherePivot('registration_status_id', \Config::get('registrationstatus.booked'))->first();
		
		return view('profile.view')
			->with('reminders', $reminders)
			->with('next_event', $next_event);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @return Response
	 */
	public function edit()
	{
		$breadcrubms = array(
			'Home' => url('http://www.thepfs.org'),
			'Events' => url('/'),
			'My Events' => route('profile.myEvents'),
			'Edit Profile' => ''
		);
		
		return view('profile.edit')->with('user', $this->user->find(\Auth::user()->id))
			->with('attendee_types', AttendeeType::lists('title', 'id'))
			->with('breadcrumbs', $breadcrubms)
            ->with('countries', Country::lists('name', 'id'))
			->with('dietary_requirements', DietaryRequirement::lists('title', 'id'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @return Response
	 */
	public function update(Requests\UpdateProfileRequest $request)
	{
		$user = $this->user->find(\Auth::user()->id);

		if($request->has('password_confirmation')) {
            $user->password = bcrypt($request->get('password_confirmation'));
            $pw_update = true;
		}
		
		$validtor_user = User::where('email', $request['email'])->where('id', '<>', \Auth::user()->id)->first();
		if (!empty($validtor_user))
		{
            \Session::flash('error', 'The entered email is already used by another user');
			return redirect()->route('profile.update');
		}

		if ($user->role_id == config('roles.non_member')) {
            $user->title = $request->get('title');
            $user->first_name = $request->get('first_name');
            $user->last_name = $request->get('last_name');
            $user->email = $request->get('email');
            $user->cc_email = $request->get('cc_email');
            $user->phone = $request->get('phone');
            $user->mobile = $request->get('mobile');
            $user->fca_number = $request->get('fca_number');
            $user->postcode = $request->get('postcode');
            $user->country_id = $request->get('country_id');
            $user->company = $request->get('company');
        }

		$user->job_title = $request->get('job_title');
		$user->badge_name = $request->get('badge_name');
		$user->profile_image = $request->get('profile_image');
		$user->dietary_requirement_id = $request->get('dietary_requirement_id');
		$user->dietary_requirement_other = $request->get('dietary_requirement_other');
		$user->special_requirements = $request->get('special_requirements');
		$user->bio = $request->get('bio');
		$user->save();
		
		/*
		$sub_roles = array();
		
		if(in_array($request->get('role_id'), [\Config::get('roles.member'), \Config::get('roles.non_member')]))
			if($request->has('user_roles_sub_id')) $sub_roles = $request->get('user_roles_sub_id');
			
		$user->subRoles()->sync($sub_roles);
		*/
		 
		$attendee_types = array();
		
		if ($request->has('attendee_types')) $attendee_types = $request->get('attendee_types');
			
		$user->attendeeTypes()->sync($attendee_types);
		
		event(new DataWasManipulated('actionUpdate', $this->log_desc));

		if (isset($pw_update)) {
            \Session::flash('success', 'You have successfully updated your profile. Password successfully changed.');
        } else {
            \Session::flash('success', 'You have successfully updated your profile');
        }
		
		return redirect()->route('profile.myEvents');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @return Response
	 */
	public function events()
	{
		$user = $this->user->find(\Auth::user()->id);

		$next_event = $user->events()->where('event_date_from', '>=', date("Y-m-d"))->orderBy('event_date_from')
			->wherePivot('registration_status_id', \Config::get('registrationstatus.booked'))->first();

		$recommended_events = $this->getUserRecommendedEvents(\Auth::user()->id);
		
		$reminders = $this->getUserReminders();
		
		$breadcrubms = array(
			'Home' => url('http://www.thepfs.org'),
			'Events' => url('/'),
			'My Events' => route('profile.myEvents')
		);
		
		return view('profile.events')->with('user', $user)
			->with('breadcrumbs', $breadcrubms)
			->with('next_event', $next_event)
			->with('reminders', $reminders)
			->with('recommended_events', $recommended_events);
	}
	
	/**
	 * Get recommended evens for user by previous events hashtags
	 * 
	 * @param int user_id who to generate recommended events list
	 * @param int previous_events number of events to use history
	 * @param int limit number of recommended events
	 * @param int distance pull events in 50miles radius from user postcode location
	 * 
	 * @return collection of recommended events
	 */
	private function getUserRecommendedEvents($user_id, $previous_events = 4, $limit = 20, $distance = 50)
	{
		$last_hashes_processed = [];
		$user = $this->user->find($user_id);
		$last_hashes = $user->events()->orderBy('event_date_from', 'DESC')->take($previous_events)->lists('hashtag');
		$registered_events = $user->events->lists('id');
		
		foreach($last_hashes as $lh) {
			
			foreach(explode(",", $lh) as $tag) {
				
				if(!empty($tag)) $last_hashes_processed[] = trim($tag);
			}
		}
		
		if(empty($last_hashes_processed)) array_push($last_hashes_processed, 'conference');
		$last_hashes_processed = implode("|", $last_hashes_processed);
		
		$venues_inrange = [];
		
		if(!empty($user->postcode)) {
            $venues = Venue::all();

            // filter venues
            $address = simplexml_load_file("http://maps.googleapis.com/maps/api/geocode/xml?address=".$user->postcode."&sensor=false&components=country:uk");

			if ($address->status == 'OK') {
                $filter_lat = $address->result->geometry->location->lat;
                $filter_lng = $address->result->geometry->location->lng;

                foreach($venues as $v) {

                    if(empty($v->latitude) || empty($v->longitude)) continue;
                    if($this->distance(floatval($filter_lat), floatval($filter_lng), $v->latitude, $v->longitude) <= $distance)
                        array_push($venues_inrange, $v->id);
                }
            }
		}
		
		$events = Event::where('event_date_from', '>=', date("Y-m-d"))
			->where('publish', 1)
			->where(function($q) use($last_hashes_processed, $user, $venues_inrange) {
				
				$q->where('hashtag', 'REGEXP', $last_hashes_processed);
				if(!empty($user->postcode) && !empty($venues_inrange)) {
					$q->orWhereIn('venue_id', $venues_inrange);
				}
			})
			->whereNotIn('id', $registered_events);
		
		$events = $events->take($limit)->get();
		
		return $events;
	}
	
	/**
	 * Service to upload sponsor logo
	 * 
	 * @return  string saved file name
	 */
	public function uploadLogo(Request $request) {
		
		// upload sponsor logo
		if($request->file('myfile')) {
			
			$saved_file = uploadFile($request->file('myfile'), PROFILE, 150);
			
			return response()->json($saved_file);
		}
		
		return null;
	}
	
	/**
	 * Generate user reminders
	 */
	 public function getUserReminders()
	 {
	 	$reminders = array();
		
		// get reminders about invitation
		$registrations = EventAttendee::where('user_id', \Auth::user()->id)
				->where('registration_status_id', [\Config::get('registrationstatus.invited')])
				->where('created_at', '>=', Carbon::now())
				->with('event')
				->get();
		
		foreach($registrations as $r) {
			
			array_push($reminders, "You have an invitation to '".$r->event->title."'");
		}
		
		//get pas events with feedbacks where user attended
		$registrations = \Auth::user()->events()
				->wherePivot('registration_status_id', \Config::get('registrationstatus.booked'))
				->whereNotNull('checkin_time')
				->where('event_date_from', '<', Carbon::now())
				->with('feedbacks')
				->get();
		
		foreach($registrations as $event) {
			
			// check if event has any feedback questions
			if(!sizeof($event->feedbacks)) continue;
			
			//if has check if user has filled the form, if no push the reminder
			if(!UserFeedback::where('event_id', $event->id)->where('user_id', \Auth::user()->id)->count())
				array_push($reminders, '<a href="'.route('events.feedback', $event->slug).'">Complete feedback</a> for event "'.$event->title.'"');
		}
		
		return $reminders;
	 }

	 public function getUserFeedback($event_id, $feedback_id = null)
     {
         dd(1);
         try {
             UserFeedback::findOrFail(11111);
         } catch (\Exception $e) {
             dd($e);
             return response()->json(['success'=>false, 'error' => $e->getMessage()], 400);
         }
     }

	/**
	 * Calculate distance between 2 geolocation points
	 * 
	 * 	@param flaot  $lat1 first point latitude
	 * 	@param flaot  $lng1 first point langitude
	 * 	@param flaot  $lat1 second point latitude
	 * 	@param flaot  $lng1 second point langitude
	 */
	private function distance($lat1, $lon1, $lat2, $lon2, $unit = "M")
	{
		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);
	 	
		if ($unit == "K") {
			return ($miles * 1.609344);
		} else if ($unit == "N") {
			return ($miles * 0.8684);
		} else {
			return $miles;
		}
	}

}
