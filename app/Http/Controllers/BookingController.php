<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Braintree\Configuration;
use Braintree\ClientToken;
use Braintree\Transaction;
use Braintree\Customer;

use App\User;
use App\Country;
use App\Hotel;
use App\ScheduleGroup;
use App\ScheduleChoice;
use App\Qualification;
use App\Specialism;
use App\Voucher;
use App\EventSetting;
use App\EmailTemplate;

class BookingController extends Controller
{
	public function __construct() {
		
		Configuration::environment(\Config::get('payment.environment'));
		Configuration::merchantId(\Config::get('payment.merchantId'));
		Configuration::publicKey(\Config::get('payment.publicKey'));
		Configuration::privateKey(\Config::get('payment.privateKey'));
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function index()
	{
		$payment_token = $clientToken = ClientToken::generate();
		
		$schedule_groups = ScheduleGroup::whereHas('scheduleChoices.bookingDates', function($q) {
			$q->where('from', '<=', date("Y-m-d"));
			$q->where('to', '>=', date("Y-m-d"));
			$q->where('active', 1);
		})->with('scheduleChoices.bookingDates')
		->where('active', 1)
		->orderBy('position')->get();
		
    	$accommodation = Hotel::whereHas('rooms', function($q){
			$q->where('active_from', '<=', date("Y-m-d"));
			$q->where('active_to', '>=', date("Y-m-d"));
			
    	})->with('rooms')
    	->orderBy('title')
    	->get();
		
		$booked_schedule = \Auth::user()->scheduleChoices()->lists('id')->all();
		$booked_accommodation = \Auth::user()->rooms()->lists('price', 'id')->all();
		
		$es = EventSetting::first();
		
		if(\Auth::user()->booking_status == 1 && \Auth::user()->paid > 0) {
			\Session::set('step', 5);
		} elseif(\Auth::user()->booking_status == 1) {
			\Session::set('step', 4);
		} elseif (\Auth::user()->booking_status == 0 && empty(\Auth::user()->first_name)) {
			\Session::set('step', 1);
		}
		
		if(sizeof(\Auth::user()->voucher)) {
			$def_reg_discount = \Auth::user()->voucher->registration_discount;
			$def_acc_discount = \Auth::user()->voucher->accommodation_discount;
		} else {
			$def_reg_discount = 0;
			$def_acc_discount = 0;
		}
		
		return view('booking.index')->with('payment_token', $payment_token)
			->with('phone_codes', Country::select('phone_code', DB::raw('CONCAT(phone_code, " - ", name) AS full_code'))->orderBy('phone_code')->lists('full_code', 'phone_code')->all())
			->with('schedule_groups', $schedule_groups)
			->with('accommodation', $accommodation)
			->with('countries', Country::lists('name', 'id')->all())
			->with('qualifications', Qualification::lists('title', 'id')->all())
			->with('specialisms', Specialism::lists('title', 'id')->all())
			->with('booked_schedule', $booked_schedule)
			->with('booked_accommodation', $booked_accommodation)
			->with('es', $es)
			->with('def_reg_discount', $def_reg_discount)
			->with('def_acc_discount', $def_acc_discount)
			->with('grand_total', 0)
			->with('automaticsessionbooking', true);
    }
	
	public function submitBooking(Requests\SubmitBookingRequest $request)
	{
		// Store new attendee
    	$attendee = \Auth::user();
		$attendee->title = $request->get('title');
		$attendee->first_name = $request->get('first_name');
		$attendee->last_name = $request->get('last_name');
		$attendee->suffix = $request->get('suffix');
		$attendee->telephone_code = $request->get('telephone_code');
		$attendee->telephone_phone = $request->get('telephone_phone');
		$attendee->mobile_code = $request->get('mobile_code');
		$attendee->mobile_phone = $request->get('mobile_phone');
		$attendee->job_title = $request->get('job_title');
		$attendee->badge_name = $request->get('badge_name');
		$attendee->organisation_name = $request->get('organisation_name');
		$attendee->city = $request->get('city');
		$attendee->dietary_requirements = $request->get('dietary_requirements');
		$attendee->country_id = $request->get('country_id');
		$attendee->qualification_id = $request->has('qualification_id') ? $request->get('qualification_id') : null;
		$attendee->specialism_id = $request->has('specialism_id') ? $request->get('specialism_id') : null;
		$attendee->receipt_required = $request->get('receipt_required');
		$attendee->receipt_address_name = $request->get('receipt_address_name');
		$attendee->receipt_address_address_1 = $request->get('receipt_address_address_1');
		$attendee->receipt_address_address_2 = $request->get('receipt_address_address_2');
		$attendee->receipt_address_city = $request->get('receipt_address_city');
		$attendee->receipt_address_postcode = $request->get('receipt_address_postcode');
		$attendee->receipt_address_country_id = $request->has('receipt_address_country_id') ? $request->get('receipt_address_country_id') : null;
		$attendee->receipt_email = $request->get('receipt_email');
		$attendee->receipt_tax_number = $request->get('receipt_tax_number');
		$attendee->receipt_po_number = $request->get('receipt_po_number');
		$attendee->online = 1;
		$attendee->used_voucher = (Voucher::where('code', $request->get('used_voucher'))->count()) ? $request->get('used_voucher') : ''; //save just valid vouchers
		$attendee->save();
		
		// save schedule choices
		if($request->has('choices')) {
			
			$attendee->scheduleChoices()->sync($request->get('choices'));
		}
		
		// save accommodation
		if($request->has('bookable_rooms')) {
			
			$sync = array();
			
			foreach($request->get('bookable_rooms') as $r) {
				
				if($request->has('rooms'.$r) && $request->get('rooms'.$r) != "0") {
					
					$sync[$r] = array(
						'price' => $request->get('rooms'.$r),
						'occupancy' => $request->get('occupancy'.$r.$request->get('rooms'.$r))
					);
				}
			}

			$attendee->rooms()->sync($sync);
		}
		
		\Session::set('step', 4);
		
		return redirect()->route('booking');
	}
	
	/**
	 * Process Braintree payment
	 */
	public function checkout(Requests\CheckoutRequest $request) {	
		
		$attendee = \Auth::user();
		$result = Transaction::sale([
			'amount' => $request->get('charge_amount'),
			
			'paymentMethodNonce' => $request->get('payment_method_nonce'),
			'options' => [
				'submitForSettlement' => true
			],
			'customer' => [
			    'firstName' => $attendee->first_name,
			    'lastName' => $attendee->last_name,
			    'email' => $attendee->email
			  ],
		]);
		
		if($result->success) {
			
			$attendee->paid = $request->get('charge_amount');
			$attendee->booking_status = 1;
			$attendee->payment_date = date("Y-m-d H:i:s");
			$attendee->save();
			
			// generate pdfs and sent confirmation email
			$this->sendConfirmation($attendee);
			
			\Session::flash('success', 'Thank you for booking. Your payment was successfully processed.');
			\Session::set('step', 5);
		} else {
			$errors = " <b>Card transaction declined! Please check your credit card details</b>";
			foreach($result->errors->deepAll() as $error) {
				
				$errors .= "<br/>".$error->attribute . ": " . $error->code . " " . $error->message;
			}
			
			\Session::flash('error', $errors);
			\Session::set('step', 4);
		}
		
		return redirect()->route('booking');
    }
	
	public function amendBooking()
	{
		$payment_token = $clientToken = ClientToken::generate();
		
		$schedule_groups = ScheduleGroup::whereHas('scheduleChoices.bookingDates', function($q) {
			$q->where('from', '<=', date("Y-m-d"));
			$q->where('to', '>=', date("Y-m-d"));
			$q->where('active', 1);
		})->with('scheduleChoices.bookingDates')
		->where('active', 1)
		->orderBy('position')->get();
		
    	$accommodation = Hotel::whereHas('rooms', function($q){
			$q->where('active_from', '<=', date("Y-m-d"));
			$q->where('active_to', '>=', date("Y-m-d"));
			
    	})->with('rooms')
    	->orderBy('title')
    	->get();
		
		$booked_schedule = \Auth::user()->scheduleChoices()->lists('id')->all();
		$booked_accommodation = \Auth::user()->rooms()->lists('id')->all();
		
		$es = EventSetting::first();
		
		if(sizeof(\Auth::user()->voucher)) {
			$def_reg_discount = \Auth::user()->voucher->registration_discount;
			$def_acc_discount = \Auth::user()->voucher->accommodation_discount;
		} else {
			$def_reg_discount = 0;
			$def_acc_discount = 0;
		}
		
		return view('booking.amendBooking')->with('payment_token', $payment_token)
			->with('schedule_groups', $schedule_groups)
			->with('accommodation', $accommodation)
			->with('booked_schedule', $booked_schedule)
			->with('booked_accommodation', $booked_accommodation)
			->with('es', $es)
			->with('def_reg_discount', $def_reg_discount)
			->with('def_acc_discount', $def_acc_discount)
			->with('grand_total', 0);
	}

	public function amendBookingUpdate(Request $request)
	{
		$attendee = \Auth::user();
		
		$result = Transaction::sale([
			'amount' => $request->get('charge_amount'),
			
			'paymentMethodNonce' => $request->get('payment_method_nonce'),
			'options' => [
				'submitForSettlement' => true
			],
			'customer' => [
			    'firstName' => $attendee->first_name,
			    'lastName' => $attendee->last_name,
			    'email' => $attendee->email
			  ],
		]);
		
		if($result->success) {
			
			$attendee->paid = $attendee->paid + $request->get('charge_amount');
			$attendee->booking_status = 1;
			$attendee->save();
			
			// attach booked sessions / accommodation
			if ($request->has('choices')) {
				foreach($request->get('choices') as $choice) {
					
					$attendee->scheduleChoices()->attach($choice);
				}
			}
			
			// save accommodation
			if($request->has('bookable_rooms')) {
				
				$sync = array();
				
				foreach($request->get('bookable_rooms') as $r) {
					
					if($request->has('rooms'.$r) && $request->get('rooms'.$r) != "0") {
						
						$sync[$r] = array(
							'price' => $request->get('rooms'.$r),
							'occupancy' => $request->get('occupancy'.$r.$request->get('rooms'.$r))
						);
					}
				}
	
				$attendee->rooms()->attach($sync);
			}
			
			// generate pdfs and sent confirmation email
			$this->sendConfirmation($attendee);
			
			\Session::flash('success', 'Thank you for booking. Your payment was successfully processed.');
			\Session::set('step', 5);
			return redirect()->route('booking');
		} else {
			$errors = " <b>Card transaction declined! Please check your credit card details</b>";
			foreach($result->errors->deepAll() as $error) {
				
				$errors .= "<br/>".$error->attribute . ": " . $error->code . " " . $error->message;
			}
			
			\Session::flash('error', $errors);
			return redirect()->route('amendBooking');
		}
	}
	
	public function validateVoucher(Request $request){
		
		$valid = Voucher::where('code', $request->get('voucher'))->select('registration_discount', 'accommodation_discount')->first();
		
		if(sizeof($valid)) return \Response::json($valid);
		
		return \Response::json(0);
	}
	
	public function validateScheduleCoupon(Request $request){
		
		$valid = ScheduleChoice::where('coupon', $request->get('coupon'))->first();
		
		if(sizeof($valid)) {
			
			return \Response::json(md5($valid->coupon));
		}
		
		return \Response::json(0);
	}
	
	/**
	 * Generate all required PDFs and send confirmation email
	 */
	private function sendConfirmation($attendee)
	{
		$confirmation_template = EmailTemplate::where('slug', 'completed-registration')->first();
		$es = EventSetting::first();
		
		$message = $confirmation_template->template;
		$search = ['[TITLE]', '[FIRST_NAME]', '[LAST_NAME]', '[EVENT_NAME]', '[EVENT_LOCATION]'];
		
		$replace = [$attendee->title, $attendee->first_name, $attendee->last_name, $es->event_name, $es->event_location];
		$message_text = str_replace($search, $replace, $message);
		
		$attachments = array();
		
		// generate summary PDF
		$attachments['summary_pdf'] = $this->generateSummaryPDF($attendee);
		
		$data['message_text'] = $message_text;
		
		\Mail::send('emails.plain', $data, function ($message) use($attendee, $attachments, $es) {
		    $message->from('events@integrity-events.com', 'Integrity Events');
			
			$message->subject($es->event_name.': Confirmation');
			
		    //$message->to('almantas@aelite.co.uk');
			
			$message->to($attendee->email);
			//$message->bcc('events@integrity-events.com');
			
			foreach($attachments as $a) {
				$message->attach($a);
			}
		});
		
		if(!empty($attendee->receipt_email) && $attendee->receipt_required) {
			
			// generate receipt PDF
			$attachments['receipt_pdf'] = $this->generateReceiptPDF($attendee);
			
			$receipt_template = EmailTemplate::where('slug', 'booking-receipt')->first();
			$message = $receipt_template->template;
			$search = ['[TITLE]', '[FIRST_NAME]', '[LAST_NAME]', '[EVENT_NAME]', '[EVENT_LOCATION]', '[RECEIPT_NAME]'];
			$replace = [$attendee->title, $attendee->first_name, $attendee->last_name, $es->event_name, $es->event_location, $attendee->receipt_address_name];
			$message_text = str_replace($search, $replace, $message);
			
			\Mail::send('emails.plain', $data, function ($message) use($attendee, $attachments, $es) {
			    $message->from('events@integrity-events.com', 'Integrity Events');
				
				$message->subject($es->event_name.': Confirmation');
				
			    //$message->to('almantas@aelite.co.uk');
				$message->to($attendee->receipt_email);
				//$message->bcc('events@integrity-events.com');
				
				foreach($attachments as $a) {
					$message->attach($a);
				}
			});
		}
	}
	
	private function generateSummaryPDF($attendee) {
		
		$es =EventSetting::first();
		$data = array('es'=>$es);
		
		$pdf = \PDF::loadView('PDF.summary', $data);
		$file = "generated_pdfs/summary-".$attendee->id."-".$attendee->first_name.".pdf";
		$pdf->save($file);
		return $file;
	}
	
	private function generateReceiptPDF($attendee) {
		
		$es =EventSetting::first();
		$data = array('es'=>$es);
		
		$pdf = \PDF::loadView('PDF.receipt', $data);
		$file = "generated_pdfs/receipt-".$attendee->id."-".$attendee->first_name.".pdf";
		$pdf->save($file);
		
		return $file;
	}
	
	/**
	 * Clean schedule choice and accommodation reservations if attendee selected but haven't paid
	 */
	public function cleanBookings() {
		
		$reservation_time = date("Y-m-d H:i:s",strtotime("-30 minutes", strtotime(date("Y-m-d H:i:s"))));
		
		// clear schedule choice bookings
		$attendees = User::where('role_id', \Config::get('roles.attendee'))
			->where('booking_status', 0)
			->has('scheduleChoices')
			->with('scheduleChoices')
			->get();
		
		foreach($attendees as $a) {
			
			foreach($a->scheduleChoices as $ch) {
				
				
				if($ch->pivot->created_at < $reservation_time) {
					
					//if choice booked longer before reservation time limit then remove reservation
					$a->scheduleChoices()->detach($ch->id);
				}
			}
		}
		
		// clear accommodation bookings
		$attendees = User::where('role_id', \Config::get('roles.attendee'))
			->where('booking_status', 0)
			->has('rooms')
			->with('rooms')
			->get();
		
		foreach ($attendees as $a) {
			
			foreach($a->rooms as $room) {
				
				if($room->pivot->created_at < $reservation_time) {
					
					$a->rooms()->detach($room->id);
				}
			}
		}
		
		echo 'Successfully Finished!';
	}
}
