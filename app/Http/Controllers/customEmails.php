<?php namespace App\Http\Controllers;

use App\Commands\SendEmail;
use App\EmailTemplate;
use App\Event;
use App\EventAttendee;
use App\EventAttendeeSlotSession;
use App\Http\Controllers\Controller;
use Illuminate\Console\Command;
use Illuminate\Contracts\Bus\Dispatcher;

class customEmails extends Controller
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'waitlist';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Moves attendee from waiting list to booked for slot and sends notification email to attendee';

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle(Dispatcher $dispatcher)
    {

		
        $templateId = \Config::get('emailtemplates.waitinglist');

        $event = Event::
            where('id', 442)
            ->with('scheduleTimeSlots.attendees', 'eventType.emailTemplates')->first();

        // collect attendees from waitinglist for slot
        $attendees = EventAttendee::
            where('registration_status_id', \Config::get('registrationstatus.booked'))
            ->where('event_id', $event->id)
            ->where('updated_at', '>', '2018-10-08 17:15:00')
            ->where('updated_at', '<', '2018-10-08 17:15:20')
            ->get();

        $message = "";
        $subject = "";

        //get template in eventType by template Id, in this case is the waitlist

        $tableName = (new EmailTemplate())->getTable();
        $template = $event->eventType->emailTemplates()->where($tableName . '.id', $templateId)->first();

        /**
         * The program init without template.So, not send email, Now, if exist template with the id
         * the active = 1 and get the template and subjet by message email.
         * The second if: Now, if the template is active and exist another custom template replace
         * the message and subject. Else if don't exist another custom template and template of message
         * is disabled, this can't send mail,
         */
        $active = 0;
        if ($template) {
            $active = 1;
            $message = $template->template;
            $subject = $template->subject;
            //check for custom template
            $custom_template = $event->emailTemplates()->where($tableName . '.id', $templateId)->first();

            if ($custom_template && $custom_template->pivot->active) {
                $message = $custom_template->pivot->template;
                $subject = $custom_template->pivot->subject;
            } elseif ($custom_template && $custom_template->pivot->active == 0) {
                // don't send
                $active = 0;
            }

        }

        foreach ($attendees as $attendee) {

            //$attendee->registration_status_id = \Config::get('registrationstatus.booked');
            //$attendee->save();
			$booked_sessions = [];
			$booked_sessions['session_name'] = "";
            foreach ($event->scheduleTimeSlots as $slot) {
                $booked_sessions['session_name'] .= " " . $slot->title;
            }
            // if email template active send notificatino about move waitlist > booked
            $user = $attendee->user;
			echo $user->email;continue;
            $dispatcher->dispatch
			(new SendEmail
                ($user->email, $user->cc_email, $subject,
                    $message, $user, $event, $booked_sessions)
            );

        }

        return [true];

    }


/*}catch(\Exeception  $e){
var_dump($e->getMessage());
}*/

}
