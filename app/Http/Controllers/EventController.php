<?php namespace App\Http\Controllers;

use App\EventScheduleSlotSession;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use Illuminate\Http\Request;
use App\Event;
use App\Venue;
use App\User;
use App\AttendeeType;
use App\EventAttendee;
use App\EventAttendeeSlotSession;
use App\EmailTemplate;
use App\Commands\SendEmail;
use App\Payment;
use App\RegistrationStatus;
use App\SponsorSetting;
use App\Sponsor;
use App\DietaryRequirement;
use App\DietaryRequirementNew;
use App\EventScheduleSlot;
use App\EventScheduleSlotQuestion;
use App\RegistrationAnswer;
use App\SessionType;
use App\EventAtendeeFavouriteSession;
use App\Events\DataWasManipulated;
use GuzzleHttp\Client;

class EventController extends Controller {

	/**
	 * Display a list of events
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		\Input::flash();
		$center_point = array('lat'=> '53.775', 'long'=>'-1.50862');
		
		$filter_date_from = \Cache::remember('filter_date_from', 1440, function(){
			
			$selector = [];
			
			for($i = 0; $i < 11; $i++) {
				$date = strtotime(date("Y-m", strtotime(date("F Y"))) . " +".$i." month");
				$selector[date("Y-m-01", $date)] = date("F Y", $date);
			}
			
			return $selector;
		});
		
		$filter_date_to = \Cache::remember('filter_date_to', 1440, function(){
			
			$selector = [];
			
			for($i = 0; $i < 11; $i++) {
				$date = strtotime(date("Y-m", strtotime(date("F Y"))) . " +".$i." month");
				$selector[date("Y-m-t", $date)] = date("F Y", $date);
			}
			
			return $selector;
		});
		
		
		// load events by filter. Use cached by default
		$events = Event::where('publish', 1)->where('invite_only', 0)->orderBy('event_date_from');
		// if filter is empty load all events if available from cache or database
		if(sizeof($request->all()) == 0) {
			$events_j = \Cache::remember('events_j', 180, function() use($events){
                
                return $events->where('event_date_from', '>=', date("Y-m-d"))->with('venue')->get();
            });
            //  aqui
			$events = \Cache::remember('events', 180, function() use($events){
				return $events->where('event_date_from', '>=', date("Y-m-d"))->with('venue')->paginate(9);
			});

			// load from cache/db JSON event for map
			$events_json = \Cache::remember('events_json', 180, function() use($events_j){
				
				$e_json = [];
				
				foreach($events_j as $e) {
					
					$venue = !empty($e->venue) ? '<b>Venue:</b> '.$e->venue->name.'<br/>' : '';
					$link = !empty($e->outside_link) ? $e->outside_link : route('events.view', $e->slug);
					$e_json[] = [
						'id' => $e->id,
						'title' => $e->title,
						'lat' => !empty($e->venue) ? $e->venue->latitude : '',
						'lng' => !empty($e->venue) ? $e->venue->longitude : '',
						'description' => "<center><b>".$e->title."</b><br/>".date("l d F Y", strtotime($e->event_date_from))."<br/><br/>".$venue."</center>".substr($e->description, 0, 200)."<br/><br/><center><a href=\"".$link."\" class=\"btn btn-xs \">Full Details</a></center>",
					];
				}
				return json_encode($e_json);
			});
		} else {
			// Filter  events by date_from
			if($request->has('from') && $request->get('from') != 'placeholder') {
				$events->where('event_date_from', '>=', $request->get('from'));
			} else {	
				$events->where('event_date_from', '>=', date('Y-m-d'));
			}
			// Filter events by date_to
			if($request->has('to') && !empty($request->get('to'))){
				$events->where('event_date_from', '<=', $request->get('to'));
			}
			// Filter events by city
			if($request->has('address') && !empty($request->get('address'))){
				$venues = Venue::select('id')->where('city','LIKE','%'.$request->get('address').'%')->get();
				if($request->get('from') != 'placeholder'){
					$events->where(function ($query) use($venues){
						foreach($venues as $v){
							$query->orWhere('venue_id',$v->id);
						}
					});
				} else {
					//$events->orWhere('venue_id',$v->id);
					$events->where(function($query) use($venues) {
						foreach($venues as $v){
							$query->orWhere('event_date_to','>=',date('Y-m-d'));
							$query->where('venue_id',$v->id);
						}
					});
				}
			}
			// Filter events by keyword
			if($request->has('keywords') && !empty($request->get('keywords'))){
				//if($request->get('address') != ''){
					$events->where('title','LIKE','%'.$request->get('keywords').'%');
				//} else {
				//	$events->orWhere('title','LIKE','%'.$request->get('keywords').'%');
				//}
			}	
			// Filter events by distance
			if($request->has('range')) {

				$distance = $request->has('range') ? $request->get('range') : 45;
				$address = simplexml_load_file("https://maps.googleapis.com/maps/api/geocode/xml?address=".$request->get('address')."&sensor=false&key=AIzaSyCFo1QWcA_GRpH7Q_jV14HF2PJ_3qzuiB8&components=country:uk");
				
				if (!(!isset($address->status) || $address->status != "OK"
				|| !isset($address->result) && !isset($address->result->geometry)))
				{
				
				$filter_lat = $address->result->geometry->location->lat;
				$filter_lng = $address->result->geometry->location->lng;
				
				$center_point['lat'] = $address->result->geometry->location->lat;
				$center_point['long'] = $address->result->geometry->location->lng;
				
				$venues = \Cache::remember('venues', 2880, function(){
					
					return Venue::all();
				});
				
				$venues_inrange = [];
				
				foreach($venues as $v) {
					if(empty($v->latitude) || empty($v->longitude)) continue;
					if($this->distance(floatval($filter_lat), floatval($filter_lng), $v->latitude, $v->longitude) <= $distance) 
						array_push($venues_inrange, $v->id);
				}
				$events->whereIn('venue_id', $venues_inrange);
				}
			} 	


			$events_json = $events->with('venue')->get();
			$events = $events->paginate(9);
			$e_json = [];
			
			foreach($events_json as $e) {
				
				if(sizeof($e->venue)) {
					
					$link = !empty($e->outside_link) ? $e->outside_link : route('events.view', $e->slug);
					$e_json[] = [
						'id' => $e->id,
						'title' => $e->title,
						'lat' => $e->venue->latitude,
						'lng' => $e->venue->longitude,
						'description' => "<center><b>".$e->title."</b><br/>".date("l d m Y", strtotime($e->event_date_from))."</center><br/>".substr($e->description, 0, 200)."<br/><br/><center><a href=\"".$link."\" class=\"btn btn-xs \">Full Details</a></center>",
					];
				}
			}
				
			$events_json = json_encode($e_json);
		}

		if($request->has('range')) {
			
			if($request->get('range') < 5) $zoom_level = 12;
			elseif($request->get('range') <= 15) $zoom_level = 11;
			elseif($request->get('range') <=50 ) $zoom_level = 8;
			else $zoom_level = 7;
			
		} else {
			$zoom_level = 6;
		}
		
		$breadcrubms = array(
			'Home' => url('http://www.thepfs.org'),
			'Events' => url('/')
		);
		return view('index')->with('events', $events)
			->with('breadcrumbs', $breadcrubms)
			->with('events_json', json_encode($events_json))
			->with('filter_date_from', $filter_date_from)
			->with('filter_date_to', $filter_date_to)
			->with('zoom_level', $zoom_level)
			->with('center_point', $center_point);

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  string  $slug
	 * @return Response
	 */
	public function show($slug)
	{
	    $event = \Cache::remember('event.'.$slug, 15, function() use ($slug){
	        return Event::where('slug', $slug)->with('venue', 'sponsors', 'tabs', 'eventFaqs')->first();
	    });
		
		$quarterly_sponsors = [];
		
		if(isset($event->eventType->has_sponsor)) {
			
			$today = $event->event_date_from;
			$quarterly_sponsors = \Cache::remember('qsponsor.'.$slug, 15, function() use($today){
			    return SponsorSetting::where('date_from', '<=', $today)->where('date_to', '>=', $today)->get();
			});
		}
		
		$slots = \Cache::remember('slots.'.$slug, 15, function() use ($event){
		    
		    return $event->scheduleTimeSlots()->orderBy('start')->with(['sessions' => function($query){
		        $query->orderBy('session_start');
                $query->with('contributors');
		    }])->get();
		});
        
		$sessions = \Cache::remember('sessions.'.$slots, 15, function() use ($event){
		   return $event->scheduleSessions()->with(['contributors' => function($query){
		       $query->orderBy('badge_name');
		   }])->get(); 
		});

		$assigned_contributors = false;
		
		foreach($sessions as $s) {
			if(sizeof($s->contributors)) $assigned_contributors = true;
		}

		$cpd_time = \Cache::remember('cpd.'.$slug, 15, function() use ($sessions){
		    return $this->calculateCPD($sessions);
		});
		
        $sponsors = \Cache::remember('sponsors', 15, function() use($event){
            return Sponsor::all();
        });
		
		$breadcrubms = array(
			'Home' => url('http://www.thepfs.org'),
			'Events' => url('/'),
			$event->title => route('events.view', $event->slug)
		);
		
        $event_tabs = $event->tabs()->lists('id');
        
		return view('events.view')->with('event', $event)
			->with('quarterly_sponsors', $quarterly_sponsors)
			->with('slots', $slots)
			->with('breadcrumbs', $breadcrubms)
			->with('cpd_time', $cpd_time)
			->with('assigned_contributors', $assigned_contributors)
			->with('sessions', $sessions)
            ->with('event_tabs', $event_tabs)
            ->with('sponsors', $sponsors);
	}

	public function externalSchedule($slug)
    {
        $event = \Cache::remember('event.'.$slug, 360, function() use ($slug){
            return Event::where('slug', $slug)->first();
        });

        $event_days = \Cache::remember('event-days.'.$slug, 360, function() use ($event){
            $date = $event->event_date_from;
            $event_days = array();

            while (strtotime($date) <= strtotime($event->event_date_to)) {
                $day = date("Y-m-d", strtotime($date));
                $event_days[] = $day;
                $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
            }

            return $event_days;
        });

        $day_slots = \Cache::remember('day-slots.'.$slug, 360, function() use ($event){
            $date = $event->event_date_from;
            $day_slots = array();

            while (strtotime($date) <= strtotime($event->event_date_to)) {
                $day = date("Y-m-d", strtotime($date));
                $day_slots[$day] = $event->scheduleTimeSlots()->where('start_date', $day)->orderBy('start')->with('sessions.type', 'sessions.meetingSpace')->get();
                $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
            }

            return $day_slots;
        });

        $schedule_header = \Cache::remember('schedule-header.'.$slug, 360, function() use ($event){
            $date = $event->event_date_from;
            $schedule_header = array();

            while (strtotime($date) <= strtotime($event->event_date_to)) {
                $day = date("Y-m-d", strtotime($date));
                $ds = $event->scheduleTimeSlots()->where('start_date', $day)->orderBy('start')->with('sessions.meetingSpace')->get();

                // take maximum columns slot and generate header
                $widest_row = $ds->where('columns', $ds->max('columns'))->first();
                for ($i = 1; $i <= $ds->max('columns'); $i++) {
                    $session = $widest_row->sessions->where('order_column', $i)->first();
                    $schedule_header[$day][] = sizeof($session) ? $session->meetingSpace->meeting_space_name : "";
                }

                $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
            }

            return $schedule_header;
        });

        return view('events.external_schedule')->with(compact('event'))
            ->with('day_slots', $day_slots)
            ->with('schedule_header', $schedule_header)
            ->with('event_days', $event_days);
    }

    /**
     *
     * @param Requests\FavouriteSessionClickedRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function favouriteSessionClicked(Requests\FavouriteSessionClickedRequest $request)
    {
        try {
            $event = Event::findOrFail($request->get('event_id'));
            $registration = EventAttendee::where('event_id', $request->get('event_id'))->where('user_id', auth()->user()->id)->first();
            // find new selected session
            $session = EventScheduleSlotSession::findOrFail($request->get('session_id'));
            $crossing_sessions = array();

            // find if registration has selected session within slot or create new instance
            if ($request->get('checked') !== 'true') {
                // if deselected remove
                EventAtendeeFavouriteSession::where('event_attendee_id', $registration->id)
                    ->where('slot_session_id', $session->id)
                    ->delete();

            } else {
                // find sessions that are within selected session time and in the same slot and remove them. only 1 session can be selected within time range
                $crossing_sessions = EventScheduleSlotSession::where('event_schedule_slot_id', $session->event_schedule_slot_id)
                    ->where(function($o) use($session) {
                        $o->where(function($q) use($session) {
                            // for sessions that are longer than selected session
                            $q->where('session_start', '<=', $session->session_start);
                            $q->where('session_end', '>=', $session->session_end);
                        });
                        $o->orWhere(function($c) use ($session){
                            // for sessions that are shorter than selected session
                            $c->where('session_start', '>=', $session->session_start);
                            $c->where('session_end', '<=', $session->session_end);
                        });
                        // if start or end is within selected session. Just a small part overlaps
                        $o->orWhere(function($n) use($session) {
                            $n->where('session_end', '>', $session->session_start);
                            $n->where('session_start', '<', $session->session_start);
                        });
                        $o->orWhere(function($m) use($session) {
                            $m->where('session_start', '<', $session->session_end);
                            $m->where('session_end', '>', $session->session_end);
                        });
                    })
                    ->where('selectable', 1)
                    ->where('id', '<>', $session->id)
                    ->lists('id');

                // remove crossing selections
                if (sizeof($crossing_sessions)) EventAtendeeFavouriteSession::where('event_attendee_id', $registration->id)->whereIn('slot_session_id', $crossing_sessions)->delete();

                // save new selection
                EventAtendeeFavouriteSession::firstOrCreate([
                    'event_schedule_slot_id' => $session->event_schedule_slot_id,
                    'event_attendee_id' => $registration->id,
                    'slot_session_id' => $session->id
                ]);
            }

            return response()->json($crossing_sessions, 200);
        } catch (\Exception $e) {
            dd($e->getMessage());
            return response()->json(['error' => 'Error processing data'], 500);
        }
    }
	
	/**
	 * Calculate sessions CPD hours
	 */
	public function calculateCPD($sessions)
    {
	 	
		// get session types which are used in CPD hours calculation
		$cpd_session_types = SessionType::where('include_in_cpd', 1)->lists('id');
		
		$hours = array();
		$duration = 0;
		
		foreach($sessions as $s) {
			
			if(in_array($s->session_type_id, $cpd_session_types)) {
				
				$duration += (strtotime($s->session_end) - strtotime($s->session_start))/60;
			}
		}
		
		$minutes = $duration % 60;
		$hours['min'] = $minutes;
		
		$h = ($duration-$minutes)/60;
		$hours['hours'] = $h;
		
		return $hours;
	 }
	
	/**
	 *  Show event booking form
	 * 	
	 * 	@param  string  $slug
	 * 	@return show event booking form
	 */
	public function book($slug)
	{
		$event = Event::where('slug', $slug)->first();
        
        // if event is invite only check if user has invitation
        if ($event->invite_only) {
            
            $event_attendee = EventAttendee::where('event_id', $event->id)
                ->where('user_id', \Auth::user()->id)
                ->where('registration_status_id', config('registrationstatus.invited'))->first();
            
            // if invitation not found - go home mate
            if (!sizeof($event_attendee)) {
                return redirect()->route('events.view', $event->slug);
            }
        }
		$schedule_slots = $event->scheduleTimeSlots()->lists('id');
		$slot_questions = EventScheduleSlotQuestion::whereIn('event_schedule_slot_id', $schedule_slots)->get();
		
		// don't allow booking for past events
		if($event->event_date_from < date("Y-m-d")) return redirect()->route('profile.myEvents');
		
		$breadcrubms = array(
			'Home' => url('http://www.thepfs.org'),
			'Events' => url('/'),
			$event->title => route('events.view', $event->slug),
			'Book' => ''
		);

		return view('events.book')->with('event', $event)
			->with('dietary_requirements', DietaryRequirementNew::lists('title', 'id'))
			->with('breadcrumbs', $breadcrubms)
			->with('attendee_types', AttendeeType::lists('title', 'id'))
			->with('slot_questions', $slot_questions);
	}
	
	/**
	 * Method used for registration attendee types
	 */
	 public function updateAttendeeTypes(Request $request)
	 {
	 	$attendee = \Auth::user();
		$attendee->cc_email = $request->get('cc_email');
		$attendee->special_requirements = $request->get('special_requirements');
		$attendee->dietary_requirement_id = $request->get('dietary_requirement_id');
		$attendee->dietary_requirement_other = $request->get('dietary_requirement_other');
		$attendee->save();
		
		$attendee_types = $request->has('attendee_types') ? $request->get('attendee_types') : array();
		$attendee->attendeeTypes()->sync($attendee_types);
	 }
	
	/**
	 * Method to update profile information when user is redirected back from PFS system
	 */
	 public function updateProfile(Request $request, $pin, $email)
	 {
		 $pin = !empty($pin) ? $pin : $email;
		 $service_url = "https://www.cii-hk.com/webservices/membership/Pfs.GetUserById.service?Id=".$pin;
		/* $service_url = "https://www.thepfs.org/webservices/membership/Pfs.GetUserById.service?Id=".$pin; */
		$u = json_decode(file_get_contents($service_url));
		
		if(!isset($u->Error)) {
			
			$user = \Auth::user();
			$user->title = $u->Title;
			$user->first_name = $u->Forenames;
			$user->last_name = $u->Surname;
			//$user->badge_name = $u->Forenames.' '.$u->Surname;
			$user->pin = $u->Pin;
			$user->email = $u->Email;
			$user->phone = $u->Phone;
			$user->mobile = $u->Mobile;
			$user->role_id = ($u->Member) ? role('member') : role('non_member');
			
			// 0 address - home; 1 - office
			$user->postcode = (!empty($u->Addresses[0]->PostCode)) ? $u->Addresses[0]->PostCode : $u->Addresses[1]->PostCode;
			$user->company = $u->Addresses[1]->Line1OrCompany;
			$user->save();
			
			event(new DataWasManipulated('actionUpdate', "profile via 'Edit in PFS'"));
		}
		
		if ($request->has('redirect')) return redirect($request->get('redirect'));
		
		return redirect()->route('events.book', $slug);
	 }
	
	/**
	 * Submit event booking
	 * If any sessions are full insert to waiting list
	 * send comfirmation email about successfull registration
	 * if confirmation data was changed update profile data also
	 * 
	 * 	@param  string  $slug
	 */
	public function submitBooking(Requests\RegisterRequest $request, $slug)
	{
		$user = \Auth::user();
		$event = Event::where('slug', $slug)->first();
		
		/* Check if user has a booking to the event */
		$user_has_attendee = EventAttendee::where('event_id', $event->id)->where('user_id', $user->id)->first();
		if(isset($user_has_attendee)) {
			return redirect()->route('events.view', $event->slug)->withErrors("You have existing registration for this event, Please go to 'My Events' and edit registration");die;
		} else { 
			$member_type = (Auth::user()->role_id == role('member')) ? true : false;
			if ($member_type == true) {
				$user_member_type = 1;
			} else {
				$user_member_type = 0;
			}
			$waitlisted = false;


			
			if ($event->invite_only) {
				
				$event_attendee = EventAttendee::where('event_id', $event->id)
					->where('user_id', $user->id)
					->where('registration_status_id', config('registrationstatus.invited'))->first();
				
				// if event is invite only and user is not invited don't show this page
				if (sizeof($event_attendee)) {
					// clear existing registration before booking
					$event_attendee->delete();
				}
			}
			
			# non members are allowed to register just for 2 event at the time
			if(hasRoles($user->role_id, [role('non_member')]) && $event->id != 680 && !isset($event->payment_enabled)) {
				
				$event_year = date("Y", strtotime($event->event_date_from));
				
				//check how many active registrations user has
				$registrations = EventAttendee::where('user_id', $user->id)
					->where('created_at', '>=', $event_year.'-01-01')
					->where('created_at', '<=', $event_year.'-12-31')
					->whereIn('registration_status_id', [config('registrationstatus.booked'), config('registrationstatus.waitlisted')])
					->count();
				
				if($registrations >= 2) {
					
					return redirect()->route('events.book', $slug)->withErrors("Non PFS members are entitled to register for 2 events per year - <a href=\"http://www.thepfs.org/membership\">click here</a> to find out more about membership");;
				}
			}
			
			// check if user has registration for this event
			if(is_array($event->atendees()->find($user->id)) && sizeof($event->atendees()->find($user->id)) > 0) return redirect()->route('events.view', $event->slug)->withErrors("You have existing registration for this event. Please go to 'My Events' and edit registration");
			
			# update confirmed user data
			if ($user->role_id == config('roles.non_member')) {
				$user->title = $request->get('title');
				$user->first_name = $request->get('first_name');
				$user->last_name = $request->get('last_name');
				$user->phone = $request->get('phone');
				$user->mobile = $request->get('mobile');
				$user->postcode = $request->get('postcode');
				$user->company = $request->get('company');
			}

			$user->badge_name = $request->get('badge_name');
			$user->dietary_requirement_id = $request->get('dietary_requirement_id');
			$user->dietary_requirement_other = $request->get('dietary_requirement_other');
			$user->special_requirements = $request->get('special_requirements');
			//$user->email = $request->get('email');
			$user->cc_email = $request->get('cc_email');
			$user->save();
			
			$attendee_types = $request->has('attendee_types') ? $request->get('attendee_types') : array();
			$user->attendeeTypes()->sync($attendee_types);
				
			$booked_sessions = [];
			$duration = 0;
			$sessions_text = "";
			
			
			$rr = $user->events()->attach($event->id, [
				'sponsors_contact'=>$request->get('sponsors_contact'), 
				'first_time_attendee' => $request->get('first_time_attendee'),
				'created_at'=>date("Y-m-d H:i:s"), 
				'updated_at' => date("Y-m-d H:i:s")]); #booked
			
			$event_attendee = $event->atendees()->find($user->id);
			$event_attendee_id = $event_attendee->pivot->id;
			
			//save slot questions
			$question_id = 0;
			
			while($request->has('question_'.$question_id)){

				// if doesn't have answer just continue
				if (!$request->has('registration_answer_'.$question_id)) {
					$question_id++;
					continue;
				}
				$answer = new RegistrationAnswer();
				$answer->event_id = $event->id;
				$answer->event_atendee_id = $event_attendee_id;
				$answer->question = $request->get('question_'.$question_id);
				$answer->answer = $request->get('registration_answer_'.$question_id);
				$answer->save();
				
				$question_id++;
			}
			
			//save event type questions
			$question_id = 0;
			
			while($request->has('dyn_question_'.$question_id)){
				// if doesn't have answer just continue
				if (!$request->has('dyn_answer_'.$question_id)) {
					$question_id++;
					continue;
				}
				$answer = new RegistrationAnswer();
				$answer->event_id = $event->id;
				$answer->event_atendee_id = $event_attendee_id;
				$answer->question = $request->get('dyn_question_'.$question_id);
				$answer->answer = $request->get('dyn_answer_'.$question_id);
				$answer->save();
				
				$question_id++;
			}

			if($request->has('sessions')){
			
				foreach ($request->get('sessions') as $session) {
					
					$slot = $event->scheduleTimeSlots()->find($session);

					#comprobamos la capacidad del time slot
					if($event->registration_type_id == config('registrationtype.waitinglist') && $slot->slot_capacity <= $slot->attendees()->whereIn('registration_status_id', [config('registrationstatus.booked'), config('registrationstatus.waitlisted')])->count() ) {  
						
						#full add to waiting list
						if ($event->price > 0.00 && $user_member_type == 1 && $event->payment_enabled) { 
							$registration_status = \Config::get('registrationstatus.waiting_payment');
						} elseif ($event->price_nonmember > 0.00 && $user_member_type == 0 && $event->payment_enabled) {
							$registration_status = \Config::get('registrationstatus.waiting_payment');
						} else {
							$registration_status = \Config::get('registrationstatus.waitlisted');
						}
						$waitlisted = true;
					} else {
						# there is some space register
						if ($event->price > 0.00 && $user_member_type == 1) { 
							$registration_status = \Config::get('registrationstatus.waiting_payment');
						} elseif ($event->price_nonmember > 0.00 && $user_member_type == 0 && $event->payment_enabled) {
							$registration_status = \Config::get('registrationstatus.waiting_payment');
						}else {
							$registration_status = \Config::get('registrationstatus.booked');
						}
						$waitlisted = false;
					}
					
					$new_session_slot =  EventAttendeeSlotSession::where('event_attendee_id', $event_attendee_id)->where('event_schedule_slot_id', $session)->first();

						if(!($new_session_slot)) { 
							$new_session_slot = new EventAttendeeSlotSession();
						}

						$new_session_slot->event_attendee_id = $event_attendee_id;
						$new_session_slot->event_schedule_slot_id = $session;
						$new_session_slot->registration_status_id = $registration_status;
						$new_session_slot->save();
				}
			} else {
				/* this was commented because the event has no capacity field any longer */
				// if (!$event->capacity 
				//     || $event->capacity >= $event->atendees()->whereIn('registration_status_id', [config('registrationstatus.booked'), config('registrationstatus.waitlisted')])->count()) {
					$waitlisted = false;
				// } else {
				//     $waitlisted = true;
				// }
			}
			
			# send confirmation email
			//check for email template inside the eventType

			if($waitlisted) {
				
				// all sessions waitlisted
				// update event booking status

				if ($event->price > 0.00 && $user_member_type == 1 && $event->payment_enabled) { 
					$registration_status = \Config::get('registrationstatus.waiting_payment');
				} elseif ($event->price_nonmember > 0.00 && $user_member_type == 0 && $event->payment_enabled) {
					$registration_status = \Config::get('registrationstatus.waiting_payment');
				} else {
					$registration_status = \Config::get('registrationstatus.waitlisted');
				}
				$event_attendee->pivot->registration_status_id = $registration_status;
				$event_attendee->pivot->save();
				$template = $event->eventType->emailTemplates->find(\Config::get('emailtemplates.registration_confirmation_waitlist'));

			} else {
				if ($event->price > 0.00 && $user_member_type == 1 && $event->payment_enabled) { 
					$registration_status = \Config::get('registrationstatus.waiting_payment');
				} elseif ($event->price_nonmember > 0.00 && $user_member_type == 0 && $event->payment_enabled) {
					$registration_status = \Config::get('registrationstatus.waiting_payment');
				} else {
					$registration_status = \Config::get('registrationstatus.booked');
				}
				$event_attendee->pivot->registration_status_id = $registration_status;
				$event_attendee->pivot->save();

				if ($event->eventType->id == 66 || $event->eventType->id == 72) {
					$template = $event->eventType->emailTemplates->find(\Config::get('emailtemplates.registration_confirmation_power_live'));
				} else { 
					$template = $event->eventType->emailTemplates->find(\Config::get('emailtemplates.registration_confirmation'));
				}
			}

			if($template) {
				$active = 1;
				$message = $template->template;
				$subject = $template->subject;
				
				//check for custom  email template inside the event
				if($waitlisted) {
					$custom_template = $event->emailTemplates->find(\Config::get('emailtemplates.registration_confirmation_waitlist'));
				} else {
					if ($event->eventType->id == 66 || $event->eventType->id == 72) {
					$custom_template = $event->emailTemplates->find(\Config::get('emailtemplates.registration_confirmation_power_live'));
					} else { 
						$custom_template = $event->emailTemplates->find(\Config::get('emailtemplates.registration_confirmation'));
					}
				}
				if($custom_template && $custom_template->pivot->active) {
					
					$message = $custom_template->pivot->template;
					$subject = $custom_template->pivot->subject;
				} elseif($custom_template && $custom_template->pivot->active == 0) {
					// don't send
					$active = 0;
				}
				if ($event->price > 0.00 && $user_member_type == 1 && $event->payment_enabled) { 
					$active = 1;
				} elseif ($event->price_nonmember > 0.00 && $user_member_type == 0 && $event->payment_enabled) {
					$active = 1;
				} elseif ($active) {
					$this->dispatch(new SendEmail($user->email, $user->cc_email, $subject, $message, $user, $event));
				}
			}
			
			// send invitations
			$template = EmailTemplate::find(\Config::get('emailtemplates.invitation'));
			
			if ($request->has('invitees') && sizeof($template)) {
				
				$invitees = $request->get('invitees');
				$invitees = str_replace(";", ",", $invitees);
				$invitees = explode(",", $invitees);
				
				foreach($invitees as $invitee) {
					
					$email = trim($invitee);

					if (!empty($email)) {
						$this->dispatch(new SendEmail($email, null,  $template->subject, $template->template, $user, $event));
					}
				}
			}
			
			event(new DataWasManipulated('eventRegister', $event->title.' ID: '.$event->id));
			
			if ($event->price > 0.00 && $user_member_type == 1 && $event->payment_enabled) { 
				return redirect()->route('events.payment', ['attendee_id' => $event_attendee_id]);
			} elseif ($event->price_nonmember > 0.00 && $user_member_type == 0 && $event->payment_enabled) {
				return redirect()->route('events.payment', ['attendee_id' => $event_attendee_id]);
			} else { 
				return redirect()->route('profile.myEvents');
			}
		}
	}

	/**
	 * Show booking for to edit registration (edit booking)
	 */
	public function editRegistration($slug)
	{
		$event = Event::where('slug', $slug)->first();
		
        // if user is editing invited registration check if he was invited
        if ($event->invite_only) {
            
            $event_attendee = $event->atendees()->find(\Auth::user()->id);
            
            // if event is invite only and user is not invited don't show this page
            if ($event_attendee->pivot->registration_status_id != config('registrationstatus.invited')) {
                return redirect()->route('events.view', $event->slug);
            } else {
                return redirect()->route('events.book', $event->slug);
            }
        }

        $event_attendee = $event->atendees()->find(auth()->user()->id);
		$schedule_slots = $event->scheduleTimeSlots()->lists('id');
		$slot_questions = EventScheduleSlotQuestion::whereIn('event_schedule_slot_id', $schedule_slots)->get();
		
		$event_attendee_id = $event->atendees()->find(\Auth::user()->id)->pivot->id;
		$booked_slots = EventAttendeeSlotSession::where('event_attendee_id',$event_attendee_id)->lists('event_schedule_slot_id');
		$booked_slots_full = EventAttendeeSlotSession::where('event_attendee_id',$event_attendee_id)->get();
		
		$slot_questions_answers = RegistrationAnswer::where('event_id', $event->id)->where('event_atendee_id', $event_attendee_id)->lists('answer', 'question');
		
		$breadcrubms = array(
			'Home' => url('http://www.thepfs.org'),
			'Events' => url('/'),
			$event->title => route('events.view', $event->slug),
			'View Details' => route('events.editBooking', $event->slug),
			'Edit Booking' => ''
		);

		return view('events.book')
			->with('event', $event)
            ->with('event_attendee', $event_attendee)
			->with('breadcrumbs', $breadcrubms)
			->with('amendment', true)
			->with('booked_slots', $booked_slots)
			->with('booked_slots_full', $booked_slots_full)
			->with('slot_questions_answers', $slot_questions_answers)
			->with('dietary_requirements', DietaryRequirement::lists('title', 'id'))
			->with('attendee_types', AttendeeType::lists('title', 'id'))
			->with('slot_questions', $slot_questions);
	}
	
	/**
	 * Update registration. Just registered sessions are available to update
	 * 
	 * 	@param  string  $slug  event slug
	 * 	@return  redirect to my event
	 */
	public function amendRegistration(Request $request, $slug)
	{
		$user = \Auth::user();
		$event = Event::where('slug', $slug)->first();
		
		$member_type = (Auth::user()->role_id == role('member')) ? true : false;
		if ($member_type == true) {
			$user_member_type = 1;
		} else {
			$user_member_type = 0;
		}
		$booked_sessions = [];
		$duration = 0;
		$sessions_text = "";
		
		# update confirmed user data
		//$user->title = $request->get('title');
		$user->badge_name = $request->get('badge_name');
		//$user->first_name = $request->get('first_name');
		//$user->last_name = $request->get('last_name');
		$user->dietary_requirement_id = $request->get('dietary_requirement_id');
		$user->dietary_requirement_other = $request->get('dietary_requirement_other');
		$user->special_requirements = $request->get('special_requirements');
		//$user->email = $request->get('email');
		$user->cc_email = $request->get('cc_email');
		//$user->postcode = $request->get('postcode');
		//$user->company = $request->get('company');
		$user->save();
		
		$event_attendee = $event->atendees()->find($user->id);
        //$event_attendee->pivot->registration_status_id = config('registrationstatus.booked');
        //$event_attendee->pivot->save();
        
		$event_attendee_id = $event_attendee->pivot->id;
		
		/**
		 * 1) if session unselected - cancelled. If all sessions unselected - event cancelled
		 * 2) if new session selected check if there is space. yes - book, 2 - to waiting list
		 * 3) 
		 */
		//dd(array_diff($old, $new));// find deleted
		//dd(array_diff($new, $old)); // find new
		
		$old_booked_sessions = EventAttendeeSlotSession::where('event_attendee_id',$event_attendee_id)->lists('event_schedule_slot_id');
		$new_booked_sessions = $request->has('sessions') ? $request->get('sessions') : [];
		$new_booked_sessions = array_map('intval', $new_booked_sessions);
		
		
		# find deleted change status to cancelled
		$deleted = array_diff($old_booked_sessions, $new_booked_sessions);
		if(!empty($deleted)) {
			
			$deleted = EventAttendeeSlotSession::where('event_attendee_id',$event_attendee_id)->whereIn('event_schedule_slot_id', $deleted)->get(); #cancelled
			
			foreach($deleted as $d) {
				
				$d->registration_status_id = \Config::get('registrationstatus.cancelled');
				$d->save();
			}
		}
		
		# find new and attach sessions
		$new = array_diff($new_booked_sessions, $old_booked_sessions);
		
		foreach($new as $session) {
			
			$slot = $event->scheduleTimeSlots()->find($session);
			
			if($slot->slot_capacity <= $slot->attendees()->whereIn('registration_status_id', [\Config::get('registrationstatus.booked'), \Config::get('registrationstatus.waitlisted')])->count()) {
				
				#full add to waiting list
				if ($event->price > 0.00 && $user_member_type == 1 && $event->payment_enabled) { 
					$registration_status = \Config::get('registrationstatus.waiting_payment');
				} elseif ($event->price_nonmember > 0.00 && $user_member_type == 0 && $event->payment_enabled) {
					$registration_status = \Config::get('registrationstatus.waiting_payment');
				} else {
					$registration_status = \Config::get('registrationstatus.waitlisted');
				}
			} else {
				
				# there is some space register
				if ($event->price > 0.00 && $user_member_type == 1 && $event->payment_enabled) { 
					$registration_status = \Config::get('registrationstatus.waiting_payment');
				} elseif ($event->price_nonmember > 0.00 && $user_member_type == 0 && $event->payment_enabled) {
					$registration_status = \Config::get('registrationstatus.waiting_payment');
				}else {
					$registration_status = \Config::get('registrationstatus.booked');
				}
			}
			
			$new_session_slot = new EventAttendeeSlotSession();
			$new_session_slot->event_attendee_id = $event_attendee_id;
			$new_session_slot->event_schedule_slot_id = $session;
			$new_session_slot->registration_status_id = $registration_status;
			$new_session_slot->save();
		}
		
		
		# if old cancelled and new booked
		$old_booked_sessions_cancelled = EventAttendeeSlotSession::where('event_attendee_id',$event_attendee_id)->where('registration_status_id', \Config::get('registrationstatus.cancelled'))->lists('event_schedule_slot_id');
		$renew = array_intersect($old_booked_sessions_cancelled, $new_booked_sessions);
		$waitlisted = false;
        
		foreach($renew as $session) {
			
			$slot = $event->scheduleTimeSlots()->find($session);
			
			if($slot->slot_capacity <= $slot->attendees()->whereIn('registration_status_id', [\Config::get('registrationstatus.booked'), \Config::get('registrationstatus.waitlisted')])->count() && $event->registration_type_id == \Config::get('registrationtype.waitinglist')) {
				
				#full add to waiting list
				if ($event->price > 0.00 && $user_member_type == 1 && $event->payment_enabled) { 
					$registration_status = \Config::get('registrationstatus.waiting_payment');
				} elseif ($event->price_nonmember > 0.00 && $user_member_type == 0 && $event->payment_enabled) {
					$registration_status = \Config::get('registrationstatus.waiting_payment');
				} else {
					$registration_status = \Config::get('registrationstatus.waitlisted');
				}
			} else {
				
				# there is some space register
				if ($event->price > 0.00 && $user_member_type == 1) { 
					$registration_status = \Config::get('registrationstatus.waiting_payment');
				} elseif ($event->price_nonmember > 0.00 && $user_member_type == 0 && $event->payment_enabled) {
					$registration_status = \Config::get('registrationstatus.waiting_payment');
				}else {
					$registration_status = \Config::get('registrationstatus.booked');
					$waitlisted = false;
				}
                $event_attendee->pivot->registration_status_id = $registration_status;
                $event_attendee->pivot->save();
			}
			
			$update_slot = EventAttendeeSlotSession::where('event_attendee_id', $event_attendee_id)->where('event_schedule_slot_id', $session)->first();
			$update_slot->registration_status_id = $registration_status;
			$update_slot->save();
		}
		
		RegistrationAnswer::where('event_id', $event->id)->where('event_atendee_id', $event_attendee_id)->delete();
		/**
		 * SAVE SLOT QUESTIONS
		 */
		$question_id = 0;
		
		while($request->has('question_'.$question_id)){
            // if doesn't have answer just continue
            if (!$request->has('registration_answer_'.$question_id)) {
                $question_id++;
                continue;
            }
			$answer = new RegistrationAnswer();
			$answer->event_id = $event->id;
			$answer->event_atendee_id = $event_attendee_id;
			$answer->question = $request->get('question_'.$question_id);
			$answer->answer = $request->get('registration_answer_'.$question_id);
			$answer->save();
			
			$question_id++;
		}
		
		/**
		 * SAVE EVENT TYPE QUESTIONS
		 */
		$question_id = 0;
		
		while($request->has('dyn_question_'.$question_id)){
            // if doesn't have answer just continue
            if (!$request->has('dyn_answer_'.$question_id)) {
                $question_id++;
                continue;
            }
			$answer = new RegistrationAnswer();
			$answer->event_id = $event->id;
			$answer->event_atendee_id = $event_attendee_id;
			$answer->question = $request->get('dyn_question_'.$question_id);
			$answer->answer = $request->get('dyn_answer_'.$question_id);
			$answer->save();
			
			$question_id++;
		}
		
		# calculate times and prepare text and send email
		$mail_slots = EventAttendeeSlotSession::where('event_attendee_id', $event_attendee_id)
			->whereIn('registration_status_id', [\Config::get('registrationstatus.booked'), \Config::get('registrationstatus.waitlisted')])
			->get();
		
		foreach($mail_slots as $session) {
			
			$slot = $event->scheduleTimeSlots()->find($session->event_schedule_slot_id);
			
			$slot_sessions = $slot->sessions()->with('type')->whereHas('type', function($q){
				$q->where('include_in_cpd', 1);
			})->get();
			
			foreach($slot_sessions as $ss) {
				
				$duration += (strtotime($ss->session_end) - strtotime($ss->session_start))/60;
			}
			
			$sessions_text .= $slot->title."<br/>";
		}
		
		$minutes = $sessions_text % 60;
		$hours = ($duration-$minutes)/60;
		$booked_sessions['sessions_text'] = $sessions_text;
		$booked_sessions['duration'] = $hours." hours and ".$minutes." minutes";
		
		# send confirmation email
		$template = $event->eventType->emailTemplates->where('id', \Config::get('emailtemplates.registration_amendment'))->first();
		
		if(sizeof($template) > 0) {
			
			$active = 1;
			$message = $template->template;
			$subject = $template->subject;
			
			//check for custom template
			$custom_template = $event->emailTemplates->where('id', \Config::get('emailtemplates.registration_amendment'))->first();
			
			if(sizeof($custom_template) > 0 && $custom_template->pivot->active) {
				
				$message = $custom_template->pivot->template;
				$subject = $custom_template->pivot->subject;
			} elseif(sizeof($custom_template) > 0 && $custom_template->pivot->active == 0) {
				// don't send
				$active = 0;
			}
			if ($event->price > 0.00 && $user_member_type == 1 && $event->payment_enabled) { 
				$active = 1;
			} elseif ($event->price_nonmember > 0.00 && $user_member_type == 0 && $event->payment_enabled) {
				$active = 1;
			} elseif ($active) {
				$this->dispatch(new SendEmail($user->email, $user->cc_email, $subject, $message, $user, $event, $booked_sessions));
			}
		}
		
		// send invitations
		$template = EmailTemplate::find(\Config::get('emailtemplates.invitation'));
        
        if ($request->has('invitees') && sizeof($template)) {
            
            $invitees = explode(",", $request->get('invitees'));
            
            foreach($invitees as $invitee) {
                
                $email = trim($invitee);
                
                if (!empty($email)) {
                    $this->dispatch(new SendEmail($email, null, $template->subject, $template->template, $user, $event));
                }
            }
        }
		
		event(new DataWasManipulated('actionUpdate', 'registration details for event: '.$event->title.' ID: '.$event->id));
		
		\Session::flash('success', 'You have successfully amended registration for event '.$event->title);
		
		if ($event->price > 0.00 && $user_member_type == 1 && $event->payment_enabled) { 
			return redirect()->route('events.payment', ['attendee_id' => $event_attendee_id]);
		} elseif ($event->price_nonmember > 0.00 && $user_member_type == 0 && $event->payment_enabled) {
			return redirect()->route('events.payment', ['attendee_id' => $event_attendee_id]);
		} else { 
			return redirect()->route('profile.myEvents');
		}
	}
	
	/**
	 * Cancel booking. Change registration status for event and sessions to cancel (2)
	 */
	public function cancelBooking(Request $request, $slug)
	{
		$event = Event::where('slug', $slug)->first();
		$user = \Auth::user();
		$event_attendee = EventAttendee::where('user_id', \Auth::user()->id)->where('event_id', $event->id)->first();
		
		// if booking cancelles < 48h then its late cancellation
		$cancellation_status = ($event->late_cancellation_date >= date("Y-m-d H:i:s") || empty($event->late_cancellation_date)) ? config('registrationstatus.cancelled') : config('registrationstatus.late_cancelled'); 
		
		
		$event_attendee->update(['registration_status_id' => $cancellation_status, 'cancel_reason' => $request->get('cancel_reason')]);
		
		foreach(EventAttendeeSlotSession::where('event_attendee_id', $event_attendee->id)->get() as $slot_session) {
			
			$slot_session->registration_status_id = $cancellation_status;
			$slot_session->save();
		}
		
		# send cancel confirm email
		// 5 = late cancellation
		$email_template_id = ($cancellation_status == 5) ? \Config::get('emailtemplates.registration_late_cancel') : \Config::get('emailtemplates.registration_cancel');
		
		$template = $event->eventType->emailTemplates->where('id', $email_template_id)->first();
		
		if(sizeof($template) > 0) {
			
			$active = 1;
			$message = $template->template;
			$subject = $template->subject;
			
			//check for custom template
			$custom_template = $event->emailTemplates->where('id', $email_template_id)->first();
			
			if(sizeof($custom_template) > 0 && $custom_template->pivot->active) {
				
				$message = $custom_template->pivot->template;
				$subject = $custom_template->pivot->subject;
			} elseif(sizeof($custom_template) > 0 && $custom_template->pivot->active == 0) {
				// don't send
				$active = 0;
			}
			
			if($active) {
				$this->dispatch(new SendEmail($user->email, $user->cc_email, $subject, $message, $user, $event));
			}
		}
		
		event(new DataWasManipulated('eventCancel', 'for event: '.$event->title.' ID: '.$event->id));
		
		return redirect()->route('profile.myEvents');
	}
	
	/**
	 * Calculate distance between 2 geolocation points
	 * 
	 * 	@param flaot  $lat1 first point latitude
	 * 	@param flaot  $lng1 first point langitude
	 * 	@param flaot  $lat1 second point latitude
	 * 	@param flaot  $lng1 second point langitude
	 */
	private function distance($lat1, $lon1, $lat2, $lon2, $unit = "M")
	{
		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);
	 	
		if ($unit == "K") {
			return ($miles * 1.609344);
		} else if ($unit == "N") {
			return ($miles * 0.8684);
		} else {
			return $miles;
		}
	}
	
	/**
	 * Renew cancelled event registration. Changes event booking status to booked, and clears booked slots, redirects to booking page
	 */
	public function renewBooking($slug)
	{
		$event = Event::where('slug', $slug)->first();
		$user = \Auth::user();
		$event_attendee = EventAttendee::where('event_id', $event->id)->where('user_id', $user->id)->first();
		$event_attendee->registration_status_id = \Config::get('registrationstatus.booked');
		$event_attendee->save();
		
		// delete all booked slots
		EventAttendeeSlotSession::where('event_attendee_id', $event_attendee->id)->delete();
		
		return redirect()->route('events.editRegistration', $slug);
	}
	
	/**
	 * Show event edit form
	 */
	public function editBooking($slug)
	{
		$event = Event::where('slug', $slug)->first();
		$registration = EventAttendee::where('event_id', $event->id)->where('user_id', auth()->user()->id)->first();
		$event_attendee_id = $registration->id;
		$booked_sessions = EventAttendeeSlotSession::where('event_attendee_id',$event_attendee_id)->get();
		$booked_sessions_ids = EventAttendeeSlotSession::where('event_attendee_id',$event_attendee_id)->whereIn('registration_status_id', [\Config::get('registrationstatus.booked'), \Config::get('registrationstatus.waitlisted')])->lists('event_schedule_slot_id');
		
		$event_materials = $event->eventMaterials()->where('date_live', '<=', date("Y-m-d"))
			->where(function($query){
				$query->where('role_visibility', 'LIKE', '%'.\Auth::user()->role_id.'%')
					->orWhere('role_visibility', null);
					
				$query->orWhere('subrole_visibility', null);
				
				foreach(\Auth::user()->subRoles->lists('id') as $sr) {
					
					$query->orWhere('subrole_visibility', 'LIKE', '%'.$sr.'%');
				}
			})->get();
		
		$event_materials = $event_materials->merge($event->files);
        $date = $event->event_date_from;
        $event_days = array();
        $day_slots = array();
        $schedule_header = array();

        while (strtotime($date) <= strtotime($event->event_date_to)) {
            $day = date("Y-m-d", strtotime($date));
            $event_days[] = $day;
            $ds = $event->scheduleTimeSlots()->where('start_date', $day)->orderBy('start')->with('sessions.type', 'sessions.meetingSpace')->get();
            $day_slots[$day] = $ds;

            // take maximum columns slot and generate header
            if ($event->schedule_version == 1) {
                $widest_row = $ds->where('columns', $ds->max('columns'))->first();
                for ($i = 1; $i <= $ds->max('columns'); $i++) {
                    $session = $widest_row->sessions->where('order_column', $i)->first();
                    $schedule_header[$day][] = sizeof($session) ? $session->meetingSpace->meeting_space_name : "";
                }
            }

            $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
        }

        // form personal schedule
        $personal_schedule = array();
        $favourite_sessions = array();
        if ($event->schedule_version == 1) {
            $favourite_sessions = $registration->favouriteSessions->lists('slot_session_id');
            $sessions = EventScheduleSlotSession::whereIn('id', $favourite_sessions)->with('type', 'contributors', 'slot')->orderBy('session_start')->get();

            foreach ($sessions as $session) {
                $personal_schedule[$session->slot->start_date][] = $session;
            }
        }


		$breadcrubms = array(
			'Home' => url('http://www.thepfs.org'),
			'Events' => url('/'),
			$event->title => route('events.view', $event->slug),
			'View Details' => route('events.editBooking', $event->slug)
		);

		return view('profile.editBooking')->with('event', $event)
			->with('user_attendee_types', \Auth::user()->attendeeTypes->lists('id'))
			->with('event_materials', $event_materials)
			->with('booked_sessions', $booked_sessions)
			->with('breadcrumbs', $breadcrubms)
            ->with('event_days', $event_days)
            ->with('day_slots', $day_slots)
            ->with('schedule_header', $schedule_header)
            ->with('personal_schedule', $personal_schedule)
            ->with('favourite_sessions', $favourite_sessions)
			->with('booked_sessions_ids', $booked_sessions_ids);
			//->with('next_event', \Auth::user()->events()->orderBy('event_date_from')->first());
	}
	
	/**
	 * Generate certificate for event if user has attended event
	 */
	public function getCertificate($slug)
	{
		# don't allow to print certificate if user hasn't attended event
		$event = Event::where('slug', $slug)->first();
		$user = \Auth::user();
		
		if(sizeof($event->atendees()->where('user_id', \Auth::user()->id)->first()) < 1 || 
			(sizeof($event->atendees()->where('user_id', \Auth::user()->id)->first()) >0 && $event->atendees()->where('user_id', \Auth::user()->id)->first()->pivot->attended == 0 && $event->atendees()->where('user_id', \Auth::user()->id)->first()->pivot->attended != 1))
			//return redirect()->route('events.editBooking', $slug)->withError('Our records show that You have not attended this event. Please contact support');
		
		$registration = $event->atendees()->find($user->id);
		$sessions_text = "";
		$sessions_duration = 0;
		
        $sessions_types_cpd = SessionType::where('include_in_cpd', 1)->lists('id');

        // Calculate CPD time for user by check in - out times
        if (empty($registration->pivot->checkin_time) || empty($registration->pivot->checkout_time)) {
            // attendee must be checed in and checked out to receive CPD hours
            $sessions_duration = 0;
        } elseif (isset($event->manually_cpd)) {
			$sessions_duration = $registration->pivot->manually_cpd;
        } else {
            if ($event->schedule_version == 1 || $event->schedule_version == 0) {
                $checkin = $registration->pivot->checkin_time;
                $checkout = $registration->pivot->checkout_time;
                $event_date = $event->event_date_from;

                foreach($event->scheduleTimeSlots()->where('bookable', 1)->get() as $slot) {

                    foreach($slot->sessions as $session) {
                        // if session time is included to CPD calculation
                        if(in_array($session->session_type_id, $sessions_types_cpd)) {

                            $from = ($checkin < $session->slot->start_date." ".$session->session_start) ? $session->slot->start_date." ".$session->session_start : $checkin;
                            $to = ($checkout > $session->slot->start_date." ".$session->session_end) ? $session->slot->start_date." ".$session->session_end : $checkout;

                            if (strtotime($to) - strtotime($from) > 0) {
                                $session_duration = (strtotime($to) - strtotime($from))/60;
                                $session_duration = round($session_duration);
                                $sessions_duration += $session_duration;

                                $sessions_text .= '
                            <tr>
                                <td width="75%">'.$session->title.'</td>
                                <td width="25%" align="right"><b>'.$session_duration.' minutes</b></td>
                            </tr>
                            ';
                            }
                        }
                    }
                }
            }
        }

		if (isset($event->manually_cpd)) {
			$sessions_duration = $event->manually_cpd;

			$duration = $sessions_duration." minutes";

        } else {
			$minutes = $sessions_duration % 60;
			
			$hours = ($sessions_duration-$minutes)/60;
			$sessions_text = $sessions_text;
			if ($hours == 1) {
				$duration = $hours." hour and ".$minutes." minutes";
			} else {
				$duration = $hours." hours and ".$minutes." minutes";
			}
		}
		

		$pdf = \PDF::loadView('events.certificate', ['user'=>$user, 'event'=>$event, 'sessions' => $sessions_text, 'duration' => $duration]);
		
		event(new DataWasManipulated('actionCreate', 'new certificate for event: '.$event->title.' ID: '.$event->id));
		
		return $pdf->download($user->first_name."_".$user->last_name." ".$event->title.".pdf");
	}

	/**
	 * AJAX service to get slot details for summary of booked session display
	 */
	 public function getSessionDetails($slot_id)
	 {
	 	$slot = EventScheduleSlot::find($slot_id);
		
		return "<b>".date("H:i", strtotime($slot->start))." - ".date("H:i", strtotime($slot->end))." ".$slot->title."</b>";
	 }
     
     /**
      * Accept invitation
      */
    public function acceptInvitation($slug)
    {
        return redirect()->route('events.book', $slug);
    }
    
    /**
     * decline invitation and send email about successfully declined event
     */
    public function declineInvitation($slug)
    {
        $event = Event::where('slug', $slug)->first();
        $user = \Auth::user();
        $event_attendee = $event->atendees()->find($user->id);
        $event_attendee->pivot->registration_status_id = config('registrationstatus.declined');
        $event_attendee->pivot->save();
        
        $template = EmailTemplate::find(\Config::get('emailtemplates.invitation_declined'));
        
        $this->dispatch(new SendEmail($user->email, $user->cc_email, $template->subject, $template->template, $user, $event));
        
        return redirect()->route('profile.myEvents');
    }
}
