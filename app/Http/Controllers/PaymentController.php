<?php namespace App\Http\Controllers;

use App\EventAttendee;
use App\EventAttendeeSlotSession;
use App\Events\DataWasManipulated;
use App\Http\Controllers\Controller;
use App\Payment;
use Auth;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Commands\SendEmail;

class PaymentController extends Controller
{

    private $payment;
    public $log_desc = "Payment";

    public function __construct(Payment $payment)
    {

        $this->payment = $payment;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Make a payment
     *
     * @return Response
     */
    public function payment(Request $request, $id)
    {
        $axcess_parameters = "";
        $event_attendee = EventAttendee::findOrFail($id);
        $event = $event_attendee->event;
        $test_user_email = 'digital@tfigroup.com';
        if ($event->payment_enabled) {
            $member = (Auth::user()->role_id == role('member')) ? true : false;

            if ($member == true) {
                $price = $event->price;
            } else {
                $price = $event->price_nonmember;
            }
            $client = new Client(); //GuzzleHttp\Client

            $params = [
                'merchantTransactionId' => $id,
                'amount' => $price,
                'currency' => 'GBP',
                'paymentType' => 'DB',
                //'testMode' => 'INTERNAL',
            ];

            if ($event_attendee->user) {
                if (isset($event_attendee->user->first_name)) {
                    $params["customer.givenName"] = $event_attendee->user->first_name;
                }

                if (isset($event_attendee->user->middle_name)) {
                    $params["customer.middleName"] = $event_attendee->user->middle_name;
                }

                if (isset($event_attendee->user->last_name)) {
                    $params["customer.surname"] = $event_attendee->user->last_name;
                }

                if (isset($event_attendee->user->email)) {
                    $params["customer.email"] = $event_attendee->user->email;
                } else { 
                    $params["customer.email"] = $test_user_email;
                }

            }
            //testing cards; https://axcessms.docs.oppwa.com/reference/parameters#testing
            $result = $client->post(
                \Config::get('paymentgateway.url') . '/v1/checkouts', [
                    'form_params' => \Config::get('paymentgateway.config') + $params,
                ]
            );
            $axcess_parameters = json_decode($result->getBody()->getContents());
        }
        return view('events.payment')
            ->with('axcess_parameters', $axcess_parameters)
            ->with('event', $event)
            ->with('price', $price);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $path = $request->resourcePath;

        $client = new Client(); //GuzzleHttp\Client

        $result = $client->get(
            \Config::get('paymentgateway.url') . $path, [
                'form_params' => \Config::get('paymentgateway.config'),
            ]
        );

        $axcess_parameters = json_decode($result->getBody()->getContents());

        if (preg_match("/^(000\.000\.|000\.100\.1|000\.[36])/", $axcess_parameters->result->code)) {

            $status = \Config::get('registrationstatus.booked');

        } elseif (preg_match("/^(000\.400\.0[^3]|000\.400\.100)/", $axcess_parameters->result->code)) {

            $status = \Config::get('registrationstatus.complete_manuall_review');

        } elseif (preg_match("/^(000\.200)/", $axcess_parameters->result->code)) {

            $status = \Config::get('registrationstatus.pending');

        } elseif (preg_match("/^(800\.400\.5|100\.400\.500)/", $axcess_parameters->result->code)) {

            $status = \Config::get('registrationstatus.pending');

        } elseif (preg_match("/^(000\.400\.[1][0-9][1-9]|000\.400\.2)/", $axcess_parameters->result->code)) {

            $status = \Config::get('registrationstatus.declined');

        } elseif (preg_match("/^(800\.[17]00|800\.800\.[123])/", $axcess_parameters->result->code)) {

            $status = \Config::get('registrationstatus.declined_by_bank');

        } elseif (preg_match("/^(200\.[123]|100\.[53][07]|800\.900|100\.[69]00\.500)/", $axcess_parameters->result->code)) {

            $status = \Config::get('registrationstatus.rejections_reference_validation');

        } elseif (preg_match("/^(700\.[1345][05]0)/", $axcess_parameters->result->code)) {

            $status = \Config::get('registrationstatus.rejections_reference_validation');

        }

        /* SAVE PAYMENT */

        $payment = $this->payment->create([
            'id_axcess' => $axcess_parameters->id,
            'paymentBrand' => $axcess_parameters->paymentBrand,
            'amount' => isset($axcess_parameters->amount) ? $axcess_parameters->amount : 0.00,
            'currency' => isset($axcess_parameters->currency) ? $axcess_parameters->currency : null,
            'status_result' => isset($status) ? $status : null,
            'description_result' => $axcess_parameters->result->description,
            'holder' => $axcess_parameters->card->holder,
            'checkout_id' => $axcess_parameters->ndc,
            'attendee_id' => $axcess_parameters->merchantTransactionId]);

        /* Update Attendees Registration Status */

        $event_attendee = EventAttendee::findOrFail($axcess_parameters->merchantTransactionId);
        $event = $event_attendee->event;
        $event_attendee->registration_status_id = $status;
        $event_attendee->save();

        /* Update Attendees slot Sessions Registration Status */

        $event_attendee_slot_sessions = EventAttendeeSlotSession::where('event_attendee_id', $axcess_parameters->merchantTransactionId)->get();

        foreach ($event_attendee_slot_sessions as $event_attendee_slot_session) {
            $event_attendee_slot_session->registration_status_id = $status;
            $event_attendee_slot_session->save();
        }

        /* Send email whit payment detail */
        if ($status == \Config::get('registrationstatus.booked')) { 
            if ($event->eventType->id || 66 && $event->eventType->id == 72) {
                $template = $event->eventType->emailTemplates->find(\Config::get('emailtemplates.registration_confirmation_power_live'));
            } else { 
                $template = $event->eventType->emailTemplates->find(\Config::get('emailtemplates.registration_confirmation'));
            }
            if($template) {
                $user = Auth::user();
                $active = 1;
                $message = $template->template;
                $subject = $template->subject;
                
                //check for custom  email template inside the event
                if ($event->eventType->id || 66 && $event->eventType->id == 72) {
                    $custom_template = $event->emailTemplates->find(\Config::get('emailtemplates.registration_confirmation_power_live'));
                } else { 
                    $custom_template = $event->emailTemplates->find(\Config::get('emailtemplates.registration_confirmation'));
                }
                
                if($custom_template && $custom_template->pivot->active) {
                    $message = $custom_template->pivot->template;
                    $subject = $custom_template->pivot->subject;
                } elseif($custom_template && $custom_template->pivot->active == 0) {
                    // don't send
                    $active = 0;
                }
                if ($active) {
                    $this->dispatch(new SendEmail($user->email, $user->cc_email, $subject, $message, $user, $event));
                }
            }
        }

        event(new DataWasManipulated('actionCreate', $this->log_desc . $payment->name));

        return view('payment.detailPayment')
            ->with('status', $status)
            ->with('event', $event)
            ->with('description', $axcess_parameters->result->description)
            ->with('merchantTransactionId', $axcess_parameters->merchantTransactionId);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function updateStatusPayment(Request $request, String $id)
    {
        $payment = $this->payment->find($id);

        $payment->fill($request->input())->save();
        event(new DataWasManipulated('actionUpdate', $this->log_desc . $payment->name));

        return redirect()->route('payments');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Process purshase status from placetopay via POST
     * (Rejected, accepted purshase)
     *
     * @param Request $request
     * @return void
     */
    public function paymentCompleted(Request $request)
    {
        $request = $request->json()->all();
        $status = $request['result']['code'];
        $payment_id = $request['id'];
        $payment = $this->payment->find($payment_id);

        return $this->updateStatusPayment($order_reference, $payment);
    }

    public function viewSlotAttendees($id)
    {
        $status = 1;
        $event_attendee_slot_sessions = EventAttendeeSlotSession::where('event_attendee_id', $id)->get();

        foreach ($event_attendee_slot_sessions as $event_attendee_slot_session) {
            $event_attendee_slot_session->registration_status_id = $status;
            var_dump($event_attendee_slot_session);
            $event_attendee_slot_session->save();
        }

        return $event_attendee_slot_sessions;
    }

}
