<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Event;

use Illuminate\Http\Request;

class PublicReportController extends Controller {

	public function attendeesList($slug)
	{
	    $event = Event::where('slug', $slug)->first();
        
        if(!sizeof($event)) return "Event was not found!";
        $data[] = array('','','','','','', $event->title.' attendees list');
        $columns = array('Title', 'First name', 'Last name', 'Company', 'Dietary req.', 'Special req.', 'Member', 'First time attendee', 'Contributor status', 'PFS Designation', 'Manual sign in', 'Manual sign out');
        
        $data[] = $columns;
        
        foreach($event->atendees()->wherePivot('registration_status_id', config('registrationstatus.booked'))->orderBy('last_name')->get() as $attendee) {
            
            $data[] = array(
                $attendee->title,
                $attendee->first_name,
                $attendee->last_name,
                $attendee->company,
                ($attendee->dietaryRequirement)? $attendee->dietaryRequirement->title: ""." ".$attendee->dietary_requirement_other,
                $attendee->special_requirements,
                ($attendee->role_id == role('member')) ? "Y" : "N",
                ($attendee->pivot->first_time_attendee) ? "Y" : "N",
                implode(", ", $attendee->subroles->lists('title')),
                implode(", ", $attendee->attendeeTypes()->whereIn('id', [3,4,5,7,11])->lists('title'))
            );
        }
        
        \Excel::create($event->title.' Attendee list', function($excel) use($data) {
        
            $excel->sheet('Attendee list', function($sheet) use($data) {
        
                $sheet->setOrientation('landscape');
                $sheet->cell('A1:K1', function($cell){
                    $cell->setBackground('#CFBEE5');
                    $cell->setAlignment('center');
                    $cell->setFontsize('16');
                });
                
                $sheet->fromArray($data, null, 'A1', false, false);
            });
        })->download('xls');
	}
    
    /**
     * LIVE Contributors list for public access via email tag
     */
    public function contributors($slug)
    {
        $event = Event::where('slug', $slug)->first();
        if (!sizeof($event)) return "Event was not found!";
        
        $pdf = \PDF::loadView('events.contributorsReport', ['event'=> $event]);
        return $pdf->download($event->title." Contributors.pdf");
    }
}
