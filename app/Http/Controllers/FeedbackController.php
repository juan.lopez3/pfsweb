<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Event;
use App\UserFeedback;
use App\EventFeedback;

class FeedbackController extends Controller {

	/**
	 * Show feedback form
	 *
	 * @param  string  $slug  event unique slug
	 * @return Response
	 */
	public function index($slug)
	{
		$event = Event::where('slug', $slug)->first();
        $user_feedback = UserFeedback::where('event_id', $event->id)->where('user_id', auth()->user()->id)->get();
        // check if user already left feedback
        if ($event->eventType->feedback_type == 'traditional' && UserFeedback::where('event_id', $event->id)->where('user_id', \Auth::user()->id)->count()) return redirect()->route('profile.myEvents');
        
		$event_feedback = EventFeedback::where('event_id', $event->id)->whereNull('session_id')->orderBy('position')->get();
		
		$breadcrubms = array(
			'Events' => url('/'),
			$event->title => route('events.view', $event->slug),
			'Feedback' => ''
		);

		return view('events.feedback.list')
			->with('event', $event)
            ->with('user_feedback', $user_feedback)
			->with('event_feedback', $event_feedback)
			->with('breadcrumbs', $breadcrubms);
	}

	/**
	 * Store event feedback
	 * 
	 * @param  string  $slug  event unique slug
	 * @return Response
	 */
	public function store(Request $request, $slug)
	{
		$event = Event::where('slug', $slug)->first();
		
		foreach($request->except('_token') as $fb_id => $fb) {
			
			$feedback = UserFeedback::firstOrNew(['event_id' => $event->id, 'user_id' => \Auth::user()->id, 'feedback_id' => $fb_id]);
			$feedback->user_id = \Auth::user()->id;
			$feedback->event_id = $event->id;
			$feedback->feedback_id = $fb_id;
            
			$feedback->answer = is_array($fb) ? implode(", ", $fb) : $fb;
			$feedback->save();
		}
		
		\Session::set('success', 'Thank you for leaving feedback for event '.$event->title);
		
		return redirect()->route('profile.myEvents');
	}

}
