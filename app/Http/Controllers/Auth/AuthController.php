<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\SignUpCheckEmailRequest;
use Illuminate\Http\Request;
use App\Country;
use App\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\AttendeeType;
use App\DietaryRequirement;

use App\Events\DataWasManipulated;

class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers;
	
	protected $redirectAfterLogout = 'https://www.thepfs.org/logout.aspx';
	
    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogout()
    {
        // log the action
        event(new DataWasManipulated('logout', ''));
        if (auth()->user()->role_id == config('roles.member')) {
            $this->auth->logout();
            return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
        } else {
            $this->auth->logout();
            return redirect('/');
        }
    }
    
	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;

		$this->middleware('guest', ['except' => 'getLogout']);
	}

	public function getLogin(Request $request)
    {
        if (\Route::currentRouteName() === "admin.login") return view('admin.auth.login');

        return view('auth.login');
    }

	/**
	 * Handle a login request to the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function postLogin(Request $request)
	{
		$this->validate($request, [
			'email' => 'required|email', 'password' => 'required',
		]);

		$credentials = $request->only('email', 'password');

		if ($this->auth->attempt($credentials, $request->has('remember')))
		{
		    event(new DataWasManipulated('login', ''));
			return redirect()->intended($this->redirectPath());
		}
		
		//login by pin
		$user = User::where('email', $credentials['email'])->where('pin', $credentials['password'])->first();
		if (!empty($user))
		{
			\Auth::loginUsingId($user->id);
            event(new DataWasManipulated('login', ''));
			return redirect()->intended($this->redirectPath());
		}
        \Session::flash('credentials_error', 'These credentials do not match our records. Please note, as a non-PFS member, your are required to login for events using an Events Password, and this is not related to any password you may have used to sign in to your general PFS / CII account (although you are free to use the same password if you wish). If you are not sure, please click ‘Forgotten password’ to set or reset your Events Password now');

		return redirect($this->loginPath())
					->withInput($request->only('email', 'remember'));
	}
	
	/**
	 * override. default redirect front/back regarding the user role
	 *
	 * @return string
	 */
	public function redirectPath()
	{
		if (property_exists($this, 'redirectPath'))
		{
			return $this->redirectPath;
		}
		
		$default = (in_array(\Auth::user()->role_id, [\Config::get('roles.member'), \Config::get('roles.non_member')])) ? '/' : '/admin/dashboard';
		
		return property_exists($this, 'redirectTo') ? $this->redirectTo : $default;
	}
	
	/**
	 * Display register form
	 */
	public function getRegister()
	{
		return view('auth.register')->with('countries', Country::lists('name', 'id'));
	}
	public function getAuthenticatehashlink2(Request $request){


		Log::error('User '.$new_user->id.' not synced. '.$e->getMessage());
		die();
	}
	/**
	 * Redirect to location from different system before authenticate
	 */
	public function getAuthenticatehashlink(Request $request)
	{
		if($request->has('auth') && $request->has('email')) {
			
            if ($request->has('pin')) $user = User::where('pin', $request->get('pin'))->first();
			if (empty($user)) $user = User::where('email', $request->get('email'))->first();
			
			if(!empty($user)) {
				
				// if no pin, user doesnt exist in events system - sync the user
				$id = ($request->has('pin')) ? $request->get('pin') : $request->get('email');
				$user = $this->processProfile($id, 2, $user->id);
				$m_name = !empty($user->middle_name) ? " ".$user->middle_name : "";
				$user_hash = sha1($user->first_name.$m_name.$user->last_name.$user->email.\Config::get('app.key'));
				
				if($request->get('auth') == $user_hash) {
					
					//authenticate
					\Auth::loginUsingId($user->id);
                    
                    // log the action
                    event(new DataWasManipulated('login', '-  Single sign on'));
					
					if($request->has('link')) {
						return redirect(urldecode($request->get('link')));
					}
				}
			} else {
				// user not fount in the systems - create new user profile and log him into the system
				$id = ($request->has('pin')) ? $request->get('pin') : $request->get('email');
				$new_user = $this->processProfile($id, 1); // craete profile
				
				//authenticate
				\Auth::loginUsingId($new_user->id);
                
                
				
				if($request->has('link')) {
					return redirect(urldecode($request->get('link')));
				}
			}
		} elseif(!$request->has('email')) {
		    return redirect('/')->withErrors('Email field is empty! Please go to your profile and add your email');
		}
		
		return redirect('/');
	}

	public function signUp()
    {
        return view('auth.pre_register');
    }

    public function signUpCheck(Request $request)
    {
        $user = User::where('email', $request->get('email'))->first();

        // if user exist show him message to use login
        if (sizeof($user)) return view('auth.user_exists')->with('email', $request->get('email'));

        // user in the system with this email doesn't exist, he can register
        \Session::set('email', $request->get('email'));
        return redirect('/auth/register');
    }

	/**
	 * Create or update user profile
	 * 
	 * @param id string pin or email
	 * @param action int function actions: 1- new; 2 - update
	 * @param user_id int
	 * @return created/updated user id
	 */
	private function processProfile($id, $action = 1, $user_id = null) {
		
		$service_url = "https://www.cii-hk.com/webservices/membership/Pfs.GetUserById.service?Id=".$id;
		/* $service_url = "https://www.thepfs.org/webservices/membership/Pfs.GetUserById.service?Id=".$id; */
		$u = json_decode(file_get_contents($service_url));
		
		// loop while some data was returned
		if(!isset($u->Error)) {
			
			switch ($action) {
				case '1':
					// create new
					$new_user = new User();
					$new_user->badge_name = $u->Forenames.' '.$u->Surname;
					break;
				
				case '2':
					// update
					$new_user = User::findOrFail($user_id);
					break;
			}
			
			if(empty($u->Email)) return $new_user;
			
			$new_user->title = $u->Title;
            //$new_user->first_name = $u->Forenames;
            $names = explode(" ", $u->Forenames);
            if (!empty($names[0])) $new_user->first_name = $names[0];
            if (!empty($names[1])) {
                $middle_name = "";
                
                for ($i=1; $i < sizeof($names); $i++) { 
                    if ($i > 1) $middle_name .= " ";
                    $middle_name .= $names[$i];
                }
                
                $new_user->middle_name = $middle_name;
            }
			$new_user->last_name = $u->Surname;
			$new_user->pin = $u->Pin;
			$new_user->email = $u->Email;
			$new_user->phone = $u->Phone;
			$new_user->mobile = $u->Mobile;
			$new_user->role_id = ($u->Member) ? role('member') : role('non_member');
			
			// 0 address - home; 1 - office
			$new_user->postcode = (!empty($u->Addresses[0]->PostCode)) ? $u->Addresses[0]->PostCode : $u->Addresses[1]->PostCode;
			$new_user->company = $u->Addresses[1]->Line1OrCompany;
			$new_user->save();
			
			// sync attendee types
			/*
			* Commented out because this service doesn;t return designations. Designations will be synced every 24h with syncusercii job
			$attendee_types = AttendeeType::lists('id', 'title');
			$assigned_attendee_types = array();
			
			// check if assigned attendee types that do not come from CII and save it so the sync would not force to lose vital data
			if ($new_user->attendeeTypes()->where('id', 17)->count()) array_push($assigned_attendee_types, 17); // 17	Paraplanner
			if ($new_user->attendeeTypes()->where('id', 18)->count()) array_push($assigned_attendee_types, 18); // 18	Business owner
			if ($new_user->attendeeTypes()->where('id', 19)->count()) array_push($assigned_attendee_types, 19); // 19	Key decision maker in an advisory firm
			
			if(!empty($u->PfsClass) && isset($attendee_types[$u->PfsClass])) array_push($assigned_attendee_types, $attendee_types[$u->PfsClass]);
			
			foreach($u->Designations as $designation) {
				
				if(isset($attendee_types[$designation])) array_push($assigned_attendee_types, $attendee_types[$designation]);
			}
			
			$new_user->attendeeTypes()->sync($assigned_attendee_types);
			*/
			
			return $new_user;
		}
	}

}
