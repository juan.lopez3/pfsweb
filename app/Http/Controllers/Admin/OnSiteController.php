<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\Event;
use App\EventAttendee;
use App\AttendeeType;
use App\DietaryRequirement;
use App\Commands\SendEmail;
use App\Events\DataWasManipulated;
use App\EventAttendeeSlotSession;
use App\EventAttendeeAttendance;

use Illuminate\Http\Request;

class OnSiteController extends Controller {
	
	protected $event;
	
	public function __construct(Event $event) {
		
		$this->event = $event;
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($id)
	{
	    $event = $this->event->findOrFail($id);
	    $attendees = $event->atendees()->where('registration_status_id', config('registrationstatus.booked'))->with('subRoles', 'role', 'feedbackAnswers')->get();
		
		return view('admin.events.onsiteLive.list')
		  ->with('event', $event)
          ->with('attendees', $attendees);
	}

	/**
	 * Create, edit registration
	 *
	 * @return Response
	 */
	public function create(Requests\OnsiteRegistrationRequest $request, $event_id)
	{
		$event = $this->event->findOrFail($event_id);
		$event_attendee = $event->atendees()->where('user_id', $request->get('user_id'))->first();
		
		$attended = sizeof($event_attendee) ? true : false;
		$attendee = User::findOrFail($request->get('user_id'));
		
		$booked_slots = array();
		$booked_slots_full = array();
		if (sizeof($event_attendee)) {
			$booked_slots = EventAttendeeSlotSession::where('event_attendee_id',$event_attendee->pivot->id)->lists('event_schedule_slot_id');
			$booked_slots_full = EventAttendeeSlotSession::where('event_attendee_id',$event_attendee->pivot->id)->get();
		}
		
		return view('admin.events.onsiteLive.registration')
			->with('event', $event)
			->with('dietary_requirements', DietaryRequirement::lists('title', 'id'))
			->with('attendee_types', AttendeeType::lists('title', 'id'))
			->with('attended', $attended)
			->with('booked_slots', $booked_slots)
			->with('booked_slots_full', $booked_slots_full)
			->with('attendee', $attendee);
			
	}
	
	/**
	 * Create new or edit existing registration
	 */
	public function store(Request $request, $event_id, $user_id)
	{
		$user = User::findOrFail($user_id);
		$event = Event::findOrFail($event_id);
		$event_attendee = $event->atendees()->where('user_id', $user->id)->first();
		$redirect = route('events.onsite.registration', $event->id)."?user_id=".$user->id;
		
		# non members are allowed to register just for 2 event at the time
		if(sizeof($event_attendee) && hasRoles($user->role_id, [role('non_member')]) && $event->id != 680) {
			
			$event_year = date("Y", strtotime($event->event_date_from));
			
			//check how many active registrations user has
			$registrations = EventAttendee::where('user_id', $user->id)
                ->where('created_at', '>=', $event_year.'-01-01')
                ->where('created_at', '<=', $event_year.'-12-31')
                ->whereIn('registration_status_id', [config('registrationstatus.booked'), config('registrationstatus.waitlisted')])
                ->count();
			
			if($registrations >= 2) {
				
				return redirect($redirect)->withErrors("Non PFS members are entitled to register for 2 events per year.");;
			}
		}
		
		// if existing registration found, else - new registration
		if (sizeof($event_attendee)) {
			
			/**
			 * 1) if session unselected - cancelled. If all sessions unselected - event cancelled
			 * 2) if new session selected check if there is space. yes - book, 2 - to waiting list
			 * 3) 
			 */
			//dd(array_diff($old, $new));// find deleted
			//dd(array_diff($new, $old)); // find new
			
			$old_booked_sessions = EventAttendeeSlotSession::where('event_attendee_id', $event_attendee->pivot->id)->lists('event_schedule_slot_id');
			$new_booked_sessions = $request->has('sessions') ? $request->get('sessions') : [];
			$new_booked_sessions = array_map('intval', $new_booked_sessions);
			
			
			# find deleted change status to cancelled
			$deleted = array_diff($old_booked_sessions, $new_booked_sessions);
			
			if(!empty($deleted)) {
				
				$deleted = EventAttendeeSlotSession::where('event_attendee_id',$event_attendee->pivot->id)->whereIn('event_schedule_slot_id', $deleted)->get(); #cancelled
				
				foreach($deleted as $d) {
					
					$d->registration_status_id = \Config::get('registrationstatus.cancelled');
					$d->save();
				}
			}
			
			# find new and attach sessions
			$new = array_diff($new_booked_sessions, $old_booked_sessions);
			
			foreach($new as $session) {
				
				$slot = $event->scheduleTimeSlots()->find($session);
				
				if($slot->slot_capacity <= $slot->attendees->count()) {
					
					#full add to waiting list
					$registration_status = \Config::get('registrationstatus.waitlisted');
				} else {
					
					# there is some space register
					$registration_status = $registration_status = \Config::get('registrationstatus.booked');
				}
				
				$new_session_slot = new EventAttendeeSlotSession();
				$new_session_slot->event_attendee_id = $event_attendee->pivot->id;
				$new_session_slot->event_schedule_slot_id = $session;
				$new_session_slot->registration_status_id = $registration_status;
				$new_session_slot->save();
			}
			
			
			# if old cancelled and new booked
			$old_booked_sessions_cancelled = EventAttendeeSlotSession::where('event_attendee_id',$event_attendee->pivot->id)->where('registration_status_id', \Config::get('registrationstatus.cancelled'))->lists('event_schedule_slot_id');
			$renew = array_intersect($old_booked_sessions_cancelled, $new_booked_sessions);
			
			foreach($renew as $session) {
				
				$slot = $event->scheduleTimeSlots()->find($session);
				
				if($slot->slot_capacity <= $slot->attendees->count() && $event->registration_type == \Config::get('registrationtype.waitinglist')) {
					
					#full add to waiting list
					$registration_status = \Config::get('registrationstatus.waitlisted');
				} else {
					
					# there is some space register
					$registration_status = $registration_status = \Config::get('registrationstatus.booked');
				}
				
				$update_slot = EventAttendeeSlotSession::where('event_attendee_id', $event_attendee->pivot->id)->where('event_schedule_slot_id', $session)->first();
				$update_slot->registration_status_id = $registration_status;
				$update_slot->save();
			}
			
			\Session::flash('success', 'Registration was successfully updated for '.$user->title.' '.$user->first_name.' '.$user->last_name);
		} else {
			$rr = $user->events()->attach($event->id, ['created_at'=>date("Y-m-d H:i:s"), 'onsite_booking' => '1', 'updated_at' => date("Y-m-d H:i:s")]); #booked
			$event_attendee = $event->atendees()->find($user->id);
			$event_attendee_id = $event_attendee->pivot->id;
			
			if($request->has('sessions')){
		
				foreach ($request->get('sessions') as $session) {
					
					$slot = $event->scheduleTimeSlots()->find($session);
					
					if($slot->slot_capacity <= $slot->attendees->count() && $event->registration_type == \Config::get('registrationtype.waitinglist')) {
						
						#full add to waiting list
					$registration_status = \Config::get('registrationstatus.waitlisted');
					$waitlisted = true;
				} else {
					
					# there is some space register
					$registration_status = \Config::get('registrationstatus.booked');
					$waitlisted = false;
				}
				
				$new_session_slot =  EventAttendeeSlotSession::where('event_attendee_id', $event_attendee_id)->where('event_schedule_slot_id', $session)->first();
                    
                    if(!sizeof($new_session_slot)) { 
                        $new_session_slot = new EventAttendeeSlotSession();
                    }

                    $new_session_slot->event_attendee_id = $event_attendee_id;
                    $new_session_slot->event_schedule_slot_id = $session;
                    $new_session_slot->registration_status_id = $registration_status;
                    $new_session_slot->save();
				}
			} else {
				
				$waitlisted = false;
			}
			
			# send confirmation email
			/*
			if($waitlisted) {
				
				// all sessions waitlisted
				// update event booking status
				$event_attendee->pivot->registration_status_id = \Config::get('registrationstatus.waitlisted');
				$event_attendee->pivot->save();
				
				$template = $event->eventType->emailTemplates->where('id', \Config::get('emailtemplates.registration_confirmation_waitlist'))->first();
			} else {
				
				$template = $event->eventType->emailTemplates->where('id', \Config::get('emailtemplates.registration_confirmation'))->first();
			}
			
			if(sizeof($template) > 0) {
				
				$active = 1;
				$message = $template->template;
				$subject = $template->subject;
				
				//check for custom template
				if($waitlisted) {
					$custom_template = $event->emailTemplates->where('id', \Config::get('emailtemplates.registration_confirmation_waitlist'))->first();
				} else {
					$custom_template = $event->emailTemplates->where('id', \Config::get('emailtemplates.registration_confirmation'))->first();
				}
				
				if(sizeof($custom_template) > 0 && $custom_template->pivot->active) {
					
					$message = $custom_template->pivot->template;
					$subject = $custom_template->pivot->subject;
				} elseif(sizeof($custom_template) > 0 && $custom_template->pivot->active == 0) {
					// don't send
					$active = 0;
				}
				
				if($active) {
					
					$this->dispatch(new SendEmail($user->email, $subject, $message, $user, $event));
					
					if(!empty($user->cc_email)){
						$this->dispatch(new SendEmail($user->cc_email, $subject, $template, $user, $event));
					}
				}
			}
			*/
			\Session::flash('success', 'New registration created for '.$user->title.' '.$user->first_name.' '.$user->last_name.'. Now you can check in.');
			//event(new DataWasManipulated('eventRegister', $event->title.' ID: '.$event->id));
		}
		
		return redirect()->route('events.onsite', $event_id);
	}
	
	public function getAttendees(Request $request, $id)
	{
		if ($request->ajax()) {
			$event = $this->event->find($id);
			$attendees = $event->atendees()
			->where(function($q) use ($request) {
				$q->where('first_name', 'like', '%'.$request->get('term').'%');
				$q->orWhere('last_name', 'like', '%'.$request->get('term').'%');
				$q->orWhere('pin', 'like', '%'.$request->get('term').'%');
			})
			->wherePivot('registration_status_id', \Config::get('registrationstatus.booked'))->take(30)->get();
			
			return $attendees->toJson();
		}
	}
	
	/**
	 * Attendee checkin to event
	 */
	public function checkin(Requests\CheckinRequest $request, $event_id)
	{
		$event_attendee = EventAttendee::findOrFail($request->get('checkin-attendee_id'));
		
		$event_attendee->checkin_time = date("Y-m-d H:i", strtotime($request->get('checkin-time')));
		$event_attendee->save();
		
		\Session::flash('success', '<b>'.$event_attendee->user->first_name.' '.$event_attendee->user->last_name.'</b> was successfully checked in!');
		
		return redirect()->route('events.onsite', $event_id);
	}
	
	/**
	 * Attendee checkin to event
	 */
	public function checkout(Requests\CheckoutRequest $request, $event_id)
	{
		$event_attendee = EventAttendee::findOrFail($request->get('checkout-attendee_id'));
		
		// check if attendee checked in
		if (is_null($event_attendee->checkin_time)) {
			\Session::flash('error', '<b>'.$event_attendee->user->first_name.' '.$event_attendee->user->last_name.'</b> has not checked in to this event. Please check in first!');
		
			return redirect()->route('events.onsite', $event_id);
		}
		
		$event_attendee->checkout_time = date("Y-m-d H:i", strtotime($request->get('checkout-time')));
		$event_attendee->save();
		
		\Session::flash('success', '<b>'.$event_attendee->user->first_name.' '.$event_attendee->user->last_name.'</b> was successfully checked out!');
		
		return redirect()->route('events.onsite', $event_id);
	}
	 /**
	 * Check in all and check out all from the list selected
     * Import event attendance.
     * @param Request $request
     * @param $event_id
     */
	public function updateListAttendanceCheck(Request $request, $event_id)
     { 
			$type_check= $request->get('command');
			$check=$request;
			$event_id= $event_id;
		// check if the param you receive is to check out, then send to the corresponding function
			if($type_check == 'CHECK-OUT ALL'){
				self::updateListAttendanceCheckout($check, $event_id);
				return redirect()->route('events.onsite', $event_id);
			}
		//or if param is to check in and send to the fuction
			if($type_check == 'CHECK-IN ALL'){
				self::updateListAttendanceCheckin($check, $event_id);
				return redirect()->route('events.onsite', $event_id);
			}

	 }
     public function updateListAttendanceCheckout($request, $event_id)
     { 
		 	//receive the parameters and start performing the sql query
			$time = date("Y-m-d H:i", strtotime($request->get('checkout_all')));
			
			// if checkout button was clicket update checkout list
			EventAttendee::where('event_id', $event_id)
				->where('registration_status_id', \Config::get('registrationstatus.booked'))
				->whereNotNull('checkin_time')
				->whereNull('checkout_time')->update(['checkout_time' => $time]);
			\Session::flash('success', 'Check out list was successfully updated!');
	 }
	 public function updateListAttendanceCheckin($request, $event_id)
     { 			
		 	//receive the parameters and start performing the sql query
			$time = date("Y-m-d H:i", strtotime($request->get('checkin_all')));
			
			// if checkin button it was not clicket update checkin list
			EventAttendee::where('event_id', $event_id)
				->where('registration_status_id', \Config::get('registrationstatus.booked'))
				->whereNull('checkin_time')->update(['checkin_time' => $time]);
			\Session::flash('success', 'Check in list was successfully updated!');
     }

    /**
     * Import event attendance.
     * @param Request $request
     * @param $event_id
     * @return \Illuminate\Http\RedirectResponse
     */
     public function importRecordedAttendance(Request $request, $event_id)
     {
         \Excel::load($request->file('file'), function ($reader) use($event_id) {
             $event = Event::findOrFail($event_id);
             $failed_import = array();

             foreach ($reader->toArray() as $row) {
                 try {
                     $registration = EventAttendee::findOrFail($row['registration_id']);
                     // import file structure: 1st column registration_id, 2nd if yes event attended, if number session
                     if (strtolower($row['attendance']) == 'yes') {
                         $registration->update(['checkin_time' => $event->event_date_from.' 00:00:01', 'checkout_time' => $event->event_date_to.' 23:59:59']);
                     } elseif(is_numeric($row['attendance'])) {
                         // if is number session attendance
                         EventAttendeeAttendance::firstOrCreate([
                             'event_attendee_id' => $row['registration_id'],
                             'session_id' => $row['attendance'],
                         ]);

                         // if this is the first record session check user in to the event
                         if (empty($registration->checkin_time)) $registration->update(['checkin_time' => $event->event_date_from.' 00:00:01', 'checkout_time' => $event->event_date_to.' 23:59:59']);
                     }
                 } catch (\Exception $e) {
                     \Log::error('Error importing attendance: '.$e->getMessage());
                     $failed_import[] = array($row['registration_id'], $e->getMessage());
                 }
             }

             // if error found in import return file with failed registrations
             if (!empty($failed_import)) {
                 $data = $failed_import;
                 \Excel::create('Failed import registrations', function($excel) use($data) {

                     $excel->sheet('Failed registrations', function($sheet) use($data) {

                         $sheet->setOrientation('landscape');
                         $sheet->fromArray($data, null, 'A1', true, false);
                     });
                 })->download('xls');
             }
         });

         return redirect()->route('events.onsite', $event_id);
     }
     
     
     public function updateAttendeeCheckin(Request $request)
     {
         if ($request->ajax()) {
             $time = date("Y-m-d H:i:s");
             
             if ($request->has('attendee_id')) {
                 
                 EventAttendee::where('id', $request->get('attendee_id'))->update(['checkin_time' => $time]);
                 return \Response::json(date("d/m/Y H:i", strtotime($time)));
             }
             
             return \Response::json(false);
        }
     }
     
     public function updateAttendeeCheckout(Request $request)
     {
         if ($request->ajax()) {
             $time = date("Y-m-d H:i:s");
             
             if ($request->has('attendee_id')) {
                 
                 EventAttendee::where('id', $request->get('attendee_id'))->update(['checkout_time' => $time]);
                 return \Response::json(date("d/m/Y H:i", strtotime($time)));
             }
             
             return \Response::json(false);
         }
     }
     
     /**
      * set checkin time to null
      */
     public function clearCheckin(Request $request)
     {
         $event_attendee = EventAttendee::findOrFail($request->get('attendee_id'));
         $event_attendee->checkin_time = null;
         $event_attendee->save();
         
         \Session::flash('success', 'Check in time cleared!');
         
         return redirect()->route('events.onsite', $event_attendee->event_id);
     }
     
     /**
      * set checkout time to null
      */
     public function clearCheckout(Request $request)
     {
         $event_attendee = EventAttendee::findOrFail($request->get('attendee_id'));
         $event_attendee->checkout_time = null;
         $event_attendee->save();
         
         \Session::flash('success', 'Check out time cleared!');
         
         return redirect()->route('events.onsite', $event_attendee->event_id);
     }
}
