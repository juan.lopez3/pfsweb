<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Submenu;
use App\Events\DataWasManipulated;

class SubmenuController extends Controller {

    public $log_desc = "Submenu ";

    /**
     * Display a listing of the sessions.
     *
     * @return list of sessions
     */
    public function index()
    {
        return view('admin.settings.submenu.list')->with('submenu', Submenu::all());
    }

    /**
     * Show the form for creating a new session.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.settings.submenu.create');
    }

    /**
     * Store a newly created session in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $submenu = Submenu::create($request->all());
        event(new DataWasManipulated('actionCreate', $this->log_desc.$submenu->title));

        return redirect()->route('settings.submenu');
    }

    /**
     * Show the form for editing the specified session.
     *
     * @param  int  $id  session type id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin.settings.submenu.edit')->with('submenu', Submenu::findOrFail($id));
    }

    /**
     * Update the specified session in storage.
     *
     * @param  int  $id  session type id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $submenu = Submenu::findOrFail($id);
        $submenu->title = $request->get('title');
        $submenu->url = $request->get('url');
        $submenu->save();
        event(new DataWasManipulated('actionUpdate', $this->log_desc.$submenu->title));

        return redirect()->route('settings.submenu');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id  session type id
     * @return Response
     */
    public function destroy($id)
    {
        $submenu = Submenu::findOrFail($id);
        if ($submenu->delete()) event(new DataWasManipulated('actionDelete', $this->log_desc.$submenu->title));

        return redirect()->route('settings.submenu');
    }

}
