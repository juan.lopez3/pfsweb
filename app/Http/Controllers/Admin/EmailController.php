<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Event;
use App\User;
use App\UserRole;
use App\UserRoleSub;
use App\EventAttendeeSlotSession;
use App\EmailAddress;
use App\Commands\SendEmail;
use App\EmailTemplate;
use App\RegistrationAnswer;
use App\Events\DataWasManipulated;
use App\SentEmail;

class EmailController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($id)
	{
		$event = Event::find($id);
		$sessions = $event->scheduleTimeSlots()->where('bookable', 1)->select('event_schedule_slots.id', \DB::raw('CONCAT_WS(" ", DATE_FORMAT(start,"%H:%i"), "-", DATE_FORMAT(end,"%H:%i"), event_schedule_slots.title) AS full_name'))->orderBy('start')->lists('full_name', 'id');
		
		return view('admin.events.emails.create')->with('event', $event)
			->with('user_roles', UserRole::lists('title', 'id'))
			->with('user_roles_sub', UserRoleSub::lists('title', 'id'))
			->with('questions', RegistrationAnswer::where('event_id', $id)->distinct()->lists('question', 'question'))
			->with('sessions', $sessions)
			->with('email_templates', EmailTemplate::lists('type', 'id'))
            ->with('senders', EmailAddress::lists('email', 'email'));
	}

	/**
	 * Show filtered users
	 *
	 * @param  int  $id  event id
	 * @return Response
	 */
	public function create(Request $request, $id)
	{
		$event = Event::find($id);
		$sessions = $event->scheduleTimeSlots()->where('bookable', 1)->select('event_schedule_slots.id', \DB::raw('CONCAT_WS(" ", DATE_FORMAT(start,"%H:%i"), "-", DATE_FORMAT(end,"%H:%i"), event_schedule_slots.title) AS full_name'))->orderBy('start')->lists('full_name', 'id');
		$filtered_attendees = $event->atendees();
		$attendee_ids = array();
		
		if($request->has('name')) 
			$filtered_attendees->where(function($q) use($request){
				$q->where('first_name', 'like', '%'.$request->get('name').'%');
				$q->orWhere('last_name', 'like', '%'.$request->get('name').'%');
			});
		
		if($request->has('booking_status')) $filtered_attendees->where('registration_status_id', $request->get('booking_status'));
		if($request->has('user_roles')) $filtered_attendees->whereIn('role_id', $request->get('user_roles'));
        if ($request->has('user_roles_sub')) $filtered_attendees->whereHas('subRoles', function ($q) use($request){
            $q->whereIn('id', $request->get('user_roles_sub'));
        });
		if($request->has('sessions')) {
			$attendee_ids = EventAttendeeSlotSession::whereIn('event_schedule_slot_id', $request->get('sessions'))->lists('event_attendee_id');
		}
		
        
		if($request->has('attended')) {
		    if($request->get('attended') == 0) {
		        $filtered_attendees->whereNull('checkin_time');
		    } else {
		        $filtered_attendees->whereNotNull('checkin_time');
		    }
        }
		
		if($request->has('question') && !empty($request->get('question'))) {
			
			$reg_att_ids = RegistrationAnswer::where('event_id', $id)
				->where('question', $request->get('question'))
				->where('answer', 'like', '%'.$request->get('answer').'%')
				->lists('event_atendee_id');
				
			$attendee_ids = array_merge($reg_att_ids, $attendee_ids);
		}
		
		if($request->has('sessions')) $filtered_attendees = $filtered_attendees->whereIn('event_atendees.id', $attendee_ids);
		$filtered_attendees = $filtered_attendees->get();
		
		return view('admin.events.emails.create')->with('event', $event)
			->with('user_roles', UserRole::lists('title', 'id'))
			->with('user_roles_sub', UserRoleSub::lists('title', 'id'))
			->with('sessions', $sessions)
			->with('filtered_attendees', $filtered_attendees)
			->with('questions', RegistrationAnswer::where('event_id', $id)->distinct()->lists('question', 'question'))
			->with('email_templates', EmailTemplate::lists('type', 'id'))
            ->with('senders', EmailAddress::lists('email', 'email'));
	}	

	/**
	 * send email for filtered users
	 *
	 * @param  int  $id event id
	 * @return Response
	 */
	public function send(Requests\SendEmailRequest $request, $id)
	{
		$event = Event::find($id);
		$to = str_replace(",", ";", $request->get('to'));
		$receivers = explode(";", $to);
		$campaign = null;

        if ($request->has('campaign')) {
            $campaign = $request->get('campaign');
            // check if campaign unique
            $i = 1;
            while (SentEmail::where('campaign', $campaign)->count()) {
                $campaign .= '-'.$i;
                $i++;
            }
        }

		foreach($receivers as $receiver) {
			if(!empty($receiver))
				$this->dispatch(new SendEmail(trim($receiver), $request->get('cc'), $request->get('subject'), $request->get('message'), null, $event, null, $request->get('from'), $campaign));
		}
		
		if($request->has('receipients')) {
			
			$attendees = User::whereIn('id', $request->get('receipients'))->get();
			
			foreach($attendees as $attendee) {
			
				$this->dispatch(new SendEmail($attendee->email, $request->get('cc'), $request->get('subject'), $request->get('message'), $attendee, $event, null, $request->get('from'), $campaign));
			}
		}
		
		\Session::flash('success', 'Email sending is in progress. All emails will be sent shortly. You can continue.');
						
		return redirect()->route('events.emails.filter', $id);
	}
	
	/**
	 * Saves email event custom configuration
	 */	
	public function saveTemplates(Request $request, $event_id) {
		
		// check if there is existing custom configuration. If yes update if no initialize and save
		$event = Event::find($event_id);
		$sync = array();
		
		//initialize and save
		
		foreach($request->get('template_id') as $tid) {
			
			$active = ($request->has('active'.$tid)) ? 1 : 0;
			$reminder = ($request->has('reminder'.$tid)) ? $request->get('reminder'.$tid) : null;
			$template = $request->get('template'.$tid);
			$subject = $request->get('subject'.$tid);
			$sync[$tid] = array(
				'reminder' => $reminder,
				'template' => $template,
				'subject' => $subject,
				'active' => $active
			);
		}
		
		// sync
		$event->emailTemplates()->sync($sync);
		event(new DataWasManipulated('actionUpdate', 'email template for event ID:'.$event->id));
		\Session::flash('success', 'Email template configuration has been successfully updated!');
		
		return redirect()->route('events.emails.filter', $event_id);
	}
}
