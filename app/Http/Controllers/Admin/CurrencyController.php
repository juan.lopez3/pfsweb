<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Currency;

class CurrencyController extends Controller
{
	
	private $currency;
	
	public function __construct(Currency $currency) {
		
		$this->currency = $currency;
		
		$this->middleware('superadmin');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.settings.currencies.list')->with('currencies', $this->currency->all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.settings.currencies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $currency = $this->currency->create($request->all());
		
		if($currency) \Session::flash('success', 'Currency was successfully created!');
		
		return redirect()->route('settings.currencies');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		return view('admin.settings.currencies.edit')->with('currency', $this->currency->findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $currency = $this->currency->findOrFail($id);
		$currency->fill($request->all())->save();
		\Session::flash('success', 'Currency was successfully updated!');
		
		return redirect()->route('settings.currencies');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->currency->destroy($id);
		\Session::flash('success', 'Currency was successfully deleted!');
    }
}
