<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\BookingDate;

class BookingDateController extends Controller
{
	
	private $booking_date;
	
	public function __construct(BookingDate $booking_date) {
		
		$this->booking_date = $booking_date;
		
		$this->middleware('superadmin');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.schedules.bookingDates.list')->with('booking_dates', $this->booking_date->all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.schedules.bookingDates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $booking_date = $this->booking_date->create([
        	'title' => $request->get('title'),
        	'from' => date("Y-m-d", strtotime($request->get('from'))),
        	'to' => date("Y-m-d", strtotime($request->get('to'))),
        ]);
		
		if($booking_date) \Session::flash('success', 'Booking date was successfully created!');
		
		return redirect()->route('schedules.bookingDates');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		return view('admin.schedules.bookingDates.edit')->with('booking_date', $this->booking_date->findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $booking_date = $this->booking_date->findOrFail($id);
		$booking_date->title = $request->get('title');
        $booking_date->from = date("Y-m-d", strtotime($request->get('from')));
        $booking_date->to = date("Y-m-d", strtotime($request->get('to')));
		$booking_date->save();
        	
		\Session::flash('success', 'Booking date was successfully updated!');
		
		return redirect()->route('schedules.bookingDates');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->booking_date->destroy($id);
		\Session::flash('success', 'Booking date was successfully deleted!');
    }
}
