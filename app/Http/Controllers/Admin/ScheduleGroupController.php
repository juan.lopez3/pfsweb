<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\ScheduleGroup;

class ScheduleGroupController extends Controller
{
	
	private $schedule_group;
	
	public function __construct(ScheduleGroup $schedule_group) {
		
		$this->schedule_group = $schedule_group;
		
		$this->middleware('superadmin');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.schedules.scheduleGroups.list')->with('schedule_groups', $this->schedule_group->orderBy('position')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.schedules.scheduleGroups.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $schedule_group = $this->schedule_group->create($request->all());
		
		if($schedule_group) \Session::flash('success', 'Schedule Group was successfully created!');
		
		return redirect()->route('schedules.scheduleGroups');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		return view('admin.schedules.scheduleGroups.edit')->with('schedule_group', $this->schedule_group->findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $schedule_group = $this->schedule_group->findOrFail($id);
		$schedule_group->title = $request->get('title');
		$schedule_group->validated = ($request->has('validated')) ? 1 : 0;
		$schedule_group->active = ($request->has('active')) ? 1 : 0;
		$schedule_group->save();
		
		\Session::flash('success', 'Schedule Group was successfully updated!');
		
		return redirect()->route('schedules.scheduleGroups');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->schedule_group->destroy($id);
		\Session::flash('success', 'Schedule Group was successfully deleted!');
    }
	
	public function reorder(Request $request)
	{
		if($request->has('items')) {
			$i = 0;
			foreach($request->get('items') as $id) {
				
				$i++;
				$item = $this->schedule_group->find($id);
				$item->position = $i;
				$item->save();
			}
			
			return response()->json(array('success' => true));
		} else {
			return response()->json(array('success' => false));
		}
	}
}
