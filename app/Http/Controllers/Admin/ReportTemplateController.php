<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\ReportTemplate;
use App\Events\DataWasManipulated;
use App\Event;
use App\EventAttendee;
use App\EventAttendeeSlotSession;
use App\SessionType;
use App\SponsorSetting;
use App\Sponsor;

class ReportTemplateController extends Controller {
	
	public $report_template;
	public $log_desc = "Report template ";
	
	
	public function __construct(ReportTemplate $report_template)
	{
		$this->report_template = $report_template;
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('admin.reports.template.list')->with('report_templates', $this->report_template->all())->with('events', Event::lists('title', 'id'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('admin.reports.template.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Requests\CreateReportTemplateRequest $request)
	{
		$report_template = $this->report_template->create($request->all());
		event(new DataWasManipulated('actionCreate', $this->log_desc.$report_template->title));
		
		return redirect()->route('reports.template');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return view('admin.reports.template.edit')->with('report_template', $this->report_template->find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Requests\CreateReportTemplateRequest $request, $id)
	{
		$report_template = $this->report_template->find($id);
		$report_template->fill($request->input())->save();
		
		return redirect()->route('reports.template');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$report_template = $this->report_template->find($id);
		event(new DataWasManipulated('actionDelete', $this->log_desc.$report_template->name));
		$report_template->delete();
		
		return redirect()->route('reports.template');
	}
	
	/**
	 * Generate report from report template in WORD
	 */
	public function generate(Requests\GenerateReportRequest $request)
	{
		$report_template = $this->report_template->find($request->get('report_template_id'));
		$title = $report_template->title;
		$report_template = $report_template->template;
		
		$event = Event::find($request->get('event_id'));
		
		$search = array("\n", "\r", "\t");
		$replace = array("","","");
		// clean ckeditor tags
		$report_template = str_replace($search, $replace, $report_template);
		
		// replace shorttags
		$report_template = $this->replaceShorttags($report_template, $event);
		
        echo $report_template;
        die;
        // throwing many errors doc file so putting html temporarily
        
		//generate word document
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		
		$section = $phpWord->addSection();
		\PhpOffice\PhpWord\Shared\Html::addHtml($section, $report_template);
		
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$filename = $title.date("Y_m_d_h_i").".docx";
		$objWriter->save('../storage/reports/'.$filename);
		
		return redirect(\Config::get('app.root')."storage/reports/".$filename);
	}
	
	/**
	 * method to replace short tags for template report
	 */
	private function replaceShorttags($template, $event = null) {
		
		// EVENT SHORTCODES
		if(isset($event) && !empty($event)) {
			
			$hashtags = "";
			
			foreach(explode(",", $event->hashtag) as $ht) {
				$hashtags .= "#".$ht." ";
			}
			
			// EVENT SPONSORS
			$sponsors_text = "";
			$event_sponsors = $event->sponsors()->lists('id');
			$delegates_number = $event->atendees()->whereIn('role_id', [role('member'), role('non_member')])->count();
			
			if($event->eventType->has_sponsor) {
				
				$today = date("Y-m-d");
				$quarterly_sponsors = SponsorSetting::where('date_from', '<=', $today)->where('date_to', '>=', $today)->get();
				
				foreach($quarterly_sponsors as $qs) {
					
					if(!empty($qs->logo_1)) array_push($event_sponsors, $qs->logo_1);
					if(!empty($qs->logo_2)) array_push($event_sponsors, $qs->logo_2);
					if(!empty($qs->logo_3)) array_push($event_sponsors, $qs->logo_3);
					if(!empty($qs->logo_4)) array_push($event_sponsors, $qs->logo_4);
				}
			}
			
			$meeting_rooms = "";
			$meeting_rooms_setup = "";
			$layouts = array(0 => 'N/a', 1=>'Cabaret', 2=>'Theatre');
			
			foreach($event->venueSpaces as $space) {
				
				$meeting_rooms .= $space->pivot->meeting_space_name."<br/>";
				$meeting_rooms_setup .= $space->pivot->meeting_space_name." Layout: ".$layouts[$space->pivot->layout_style]." Tables: ".$space->pivot->num_of_tables."<br/>";
			}
			
			$event_sponsors = array_unique($event_sponsors);
			
			foreach(Sponsor::whereIn('id', $event_sponsors)->get() as $sponsor) {
				$sponsors_text .= $sponsor->name."<br/>";
			}
			
			$av_equipment = "<table><tr><td>Quantity</td><td>Item</td><td>Provided by</td></tr>";
            foreach($event->technologies as $tech) {
                $av_equipment .= "<tr>";
                $av_equipment .= '<td>'.$tech->title."</td><td>".$tech->pivot->quantity."</td><td>".$tech->pivot->provided_by."</td>";
                $av_equipment .= "</tr>";
            }
            $av_equipment .= "</table>";
			
			$hosts = "";
			foreach($event->atendeesByRole(role('host')) as $host) {
				$hosts .= $host->first_name.' '.$host->last_name.' ';
				if(!empty($host->mobile)) $hosts .= $host->mobile.'; ';
				if(!empty($host->phone)) $hosts .= $host->phone.'; ';
				$hosts .= "<br/>";
			}
			
			$tfi_contacts = "";
			foreach($event->atendeesByRole(role('event_manager'), role('event_coordinator')) as $contact) {
				$tfi_contacts .= $contact->first_name.' '.$contact->last_name.' ';
				if(!empty($contact->mobile)) $tfi_contacts .= $contact->mobile.'; ';
				if(!empty($contact->phone)) $tfi_contacts .= $contact->phone.'; ';
				$tfi_contacts .= "<br/>";
				
			}
            
            $venue_costs = "<table width=\"100%\">";
            $total = 0;
            
            foreach ($event->costs()->where('venue_cost', 1)->get() as $cost) {
                
                $venue_costs .= "<tr>";
                
                $attendees = $event->atendees()->where('registration_status_id', config('registrationstatus.booked'))->count();
                $venue_costs .= "<td>".$cost->title.'</td><td>£'.$cost->amount;
                if ($cost->per_person) $venue_costs.= " (per person)";
                $venue_costs .= "</td>";
                
                $venue_costs .= "<td>".$attendees." (attendees)</td>";
                
                $venue_costs .= "<td>";
                
                if ($cost->contingency_no) $venue_costs .= $cost->contingency_no." contingency";
                    else $venue_costs .= " n/a";
                    
                $venue_costs .= "</td>";
                    
                if($cost->per_person) {
                    $line_total = $cost->amount * ($attendees + $cost->contingency_no);
                } else {
                    $line_total = $cost->amount;
                }
                
                $total += $line_total;
                $venue_costs .= "<td>£".$line_total."</td>";
                $venue_costs .= "</tr>";
            }
            
            $venue_costs .= "</table>";
            $venue_costs .= "<p><b>Total Venue Cost:</b> £".$total."</p>";
			
			$first_slot = $event->scheduleTimeSlots()->where('bookable', 1)->orderBy('start')->skip(0)->take(1)->first();
			$second_slot = $event->scheduleTimeSlots()->where('bookable', 1)->orderBy('start')->skip(1)->take(1)->first();
			
			$both_slots = EventAttendee::where('event_id', $event->id)->whereHas('slots', function($q) use($first_slot, $second_slot) {
				$q->registration_status_id = \Config::get('registrationstatus.booked');
				if(sizeof($first_slot)) $q->id = $first_slot->id;
				if(sizeof($second_slot)) $q->id = $second_slot->id;
			})->count();
			
			$first_slot = (sizeof($first_slot)) ? $first_slot->attendees()->where('registration_status_id', \Config::get('registrationstatus.booked'))->count() : 0;
			$second_slot = (sizeof($second_slot)) ? $second_slot->attendees()->where('registration_status_id', \Config::get('registrationstatus.booked'))->count() : 0; 
			
			$search = [
				'EVENT_NAME',
				'EVENT_TYPE',
				'EVENT_DESCRIPTION',
				'EVENT_LINK',
				'EVENT_EDIT',
				'EVENT_START',
				'EVENT_END',
				'EVENT_REGION',
				'EVENT_SPONSORS',
				'MEETING_ROOMS_SETUP',
				'MEETING_ROOMS',
				'DELEGATES_NUMBER',
				'EVENT_HASHTAGS',
				'DATE_TODAY',
				'VENUE_COSTS',
				'AV_EQUIPMENT',
				'HOSTESSES',
				'DUTY_MANAGER',
				'AV_CONTACT',
				'TFI_CONTACTS',
				
				'ATTENDEES_FIRST_SLOT',
				'ATTENDEES_SECOND_SLOT',
				'ATTENDEES_BOTH_SLOT',
			];
			
			$replace = [
				$event->title,
				$event->eventType->title,
				$event->description,
				route('events.view', $event->slug),
				route('events.editBooking', $event->slug),
				date("d/m/Y", strtotime($event->event_date_from)),
				date("d/m/Y", strtotime($event->event_date_to)),
				$event->region->title,
				$sponsors_text,
				$meeting_rooms_setup,
				$meeting_rooms,
				$delegates_number,
				$hashtags,
				date("d/m/Y"),
				$venue_costs,
				$av_equipment,
				$hosts,
				sizeof($event->onSiteDetails) ? $event->onSiteDetails->duty_manager_name.' '.$event->onSiteDetails->duty_manager_number : '',
				sizeof($event->onSiteDetails) ? $event->onSiteDetails->av_technician_name.' '.$event->onSiteDetails->av_technician_number : '',
				$tfi_contacts,
				$first_slot,
				$second_slot,
				$both_slots
			];
			
			$template = str_replace($search, $replace, $template);
			
            # ATTENDEES DIETARY AND SPECIAL REQUIREMENTS
            $d_s_requirements = "";
            $c = $event->atendees()->where(function($q){
                $q->where('special_requirements', '!=', '');
                $q->orWhere('dietary_requirement_id', '!=', '1');
            })->get();
            
            // 1 dietary - no dietary
            foreach($event->atendees()->where(function($q){
                $q->where('special_requirements', '!=', '');
                $q->orWhere('dietary_requirement_id', '!=', '1');
            })->get() as $attendee) {
                
                $d_s_requirements .= "<p>".$attendee->title." ".$attendee->first_name." ".$attendee->last_name.",";
                
                if($attendee->dietary_requirement_id <> 1){
                    $d_s_requirements .= " Dietary requirements: ". $attendee->dietaryRequirement->title;
                    if (!empty($attendee->dietary_requirement_other)) $d_s_requirements .= " ".$attendee->dietary_requirement_other;
                    $d_s_requirements .= ",";
                }
                
                if(!empty($attendee->special_requirements)) $d_s_requirements .= " Special requirements: ".$attendee->special_requirements;
                
                $d_s_requirements .= "</p>";
            }
            
            $template = str_replace("DIETARY_SPECIAL_REQUIREMENTS", $d_s_requirements, $template);
            
			# VENUE
			if(sizeof($event->venue)) {
				$search = [
					'EVENT_VENUE_NAME',
					'EVENT_VENUE_COUNTY',
					'EVENT_VENUE_POSTCODE',
					'EVENT_VENUE_ADDRESS',
					'EVENT_VENUE_EMAIL',
					'EVENT_VENUE_PHONE',
					'VENUE_CONTACT',
					'MIN_GUARANTEED_NUMBERS'
				];
				
				$replace = [
					$event->venue->name,
					$event->venue->county,
					$event->venue->postcode,
					$event->venue->address,
					$event->venue->email,
					$event->venue->phone,
					(sizeof($event->venue->contactPerson)) ? $event->venue->contactPerson->first_name.' '.$event->venue->contactPerson->last_name : '',
					$event->venue->min_guaranteed
				];
				
				$template = str_replace($search, $replace, $template);
				
				# VENUE ROOMS
				if(sizeof($event->venue->rooms)) {
					
					$rooms = "";
					foreach($event->venue->rooms as $r) {
						$rooms .= "Room: ".$r->room_name." Capacity: ".$r->capacity."; Cabaret capacity: ".$r->cabaret_capacity."; Theatre capacity: ".$r->theatre_capacity."; Max tables: ".$r->max_tables."<br/>";
					}
					
					$template = str_replace("EVENT_VENUE_ROOMS", $rooms, $template);
				}
			}
			
			// =====================================================================================================
			$all_event_sessions = "";
			$all_event_sessions_times = "";
			$contributors = "";
			$contributors_bio = "";
			$sessions_duration = 0;
			$schedule_start = "";
			$schedule_end = "";
			
			$data = $event->scheduleTimeSlots()->orderBy('start')->first();
			if(sizeof($data)) $schedule_start = date("H:i", strtotime($data->start));
			
			$data = $event->scheduleTimeSlots()->orderBy('end', 'DESC')->first();
			if(sizeof($data)) $schedule_end = date("H:i", strtotime($data->end));
			
			$sessions_types_cpd = SessionType::where('include_in_cpd', 1)->lists('id');
			
			foreach ($event->scheduleTimeSlots as $slot) {
				
				// get slot sessions. and check if session type includes in CPD time calculation
				$slot_sessions = $slot->sessions;
				
				foreach($slot_sessions as $session) {
					
					if(in_array($session->session_type_id, $sessions_types_cpd))
						$sessions_duration += (strtotime($session->session_end) - strtotime($session->session_start))/60;
					
					// sessions and times in rows
					$all_event_sessions_times .= date("h:i", strtotime($session->session_start))." - ".date("h:i", strtotime($session->session_end))." <b>".$session->title."</b><br/>";
					
					$all_event_sessions .= date("h:i", strtotime($session->session_start))." - ".date("h:i", strtotime($session->session_end))." <b>".$session->title." </b><br/>";
					
					// join all contributors with bio
					foreach($session->contributors as $c) {
						
						$all_event_sessions .= $c->title." ".$c->first_name." ".$c->last_name."<br/>";
						$all_event_sessions .= $c->bio."<br/>";
						
						$contributors .= $c->title." ".$c->badge_name."<br/>";
						$contributors_bio = $c->title." ".$c->badge_name."<br/>".$c->bio."<br/><br/>";
					}
					
					$all_event_sessions .= "<hr/>";
				}
			}
			
			$minutes = $sessions_duration % 60;
			$hours = ($sessions_duration-$minutes)/60;
			
			$sessions_duration = $hours." hours and ".$minutes." minutes";
			
			$search = [
				'SESSIONS_DURATION',
				'ALL_EVENT_SESSIONS',
				'EVENT_SESSIONS_TIMES',
				'SESSION_CONTRIBUTORS_BIO',
				'SESSION_CONTRIBUTORS',
				'EVENT_SCHEDULE_START',
				'EVENT_SCHEDULE_END'
			];
			$replace = [$sessions_duration, $all_event_sessions, $all_event_sessions_times, $contributors_bio, $contributors, $schedule_start, $schedule_end];
			$template = str_replace($search, $replace, $template);
			
			// sessions data by id
			$pattern = "/SESSION_TYPE_[0-9]*/";
			preg_match_all($pattern, $template, $matches);
			$matches = $matches[0];
			
			foreach($matches as $m) {
				
				$type_id = substr($m, 13);
				
				// find all sessions and replace with session title start - end
				$sessions = $event->scheduleSessions()->where('session_type_id', $type_id)->get();
				$replace = "";
				
				// collect tag sessions
				foreach($sessions as $s) {
					$replace .= date("h:i", strtotime($s->session_start)).' - '.date("h:i", strtotime($s->session_end)).' <br/> ';
				}
				
				$template = str_replace($m, $replace, $template);
			}
		}
		
		return $template;
	}

}
