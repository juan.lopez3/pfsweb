<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Event;
use App\EventCost;
use App\Events\DataWasManipulated;

class EventCostController extends Controller {
	
	protected $event;
	protected $event_cost;
	public $log_desc = "Event costs for event ";
	
	public function __construct(Event $event, EventCost $event_cost)
	{
		$this->event = $event;
		$this->event_cost = $event_cost;
	}
	/**
	 * Display a listing of the event costs.
	 * @param int $id event id
	 * @return show list of event costs
	 */
	public function index($id)
	{
		$event = $this->event->find($id);
		return view('admin.events.costs.edit')->with('event', $event)->with('atendees', $event->atendees()->count());
	}

	

	/**
	 * Update the specified event costs in storage.
	 *
	 * @param  int  $id event id
	 * @return redirect to event costs
	 */
	public function update(Request $request, $id)
	{
		$event = $this->event->find($id);
		$event->costs()->delete();
		
		// insert new values
		$costs = array();
		
		for ($i=0; $i < sizeof($request->get('title')); $i++) {
			
			$cost = new EventCost([
				'title' => $request->get('title')[$i],
				'amount' => $this->sanitizeAmount($request->get('amount')[$i]),
				'per_person' => (isset($request->get('per_person')[$i])) ? 1 : 0,
				'venue_cost' => (isset($request->get('venue_cost')[$i])) ? 1 : 0,
				'contingency_no' => $request->get('contingency_no')[$i]
			]);
			
			$costs[] = $cost;
		}
		
		$event->costs()->saveMany($costs);
		
		event(new DataWasManipulated('actionUpdate', $this->log_desc.$event->title));
		
		return redirect()->route('events.costs', $id);
	}
	
	/**
	 * Method for removing input mask
	 * @param double $amount
	 * @return sanitized amount
	 */
	private function sanitizeAmount($amount) {
		
		$amount = str_replace('£', '', $amount);
		$amount = str_replace('_', '', $amount);
		$amount = str_replace(',', '', $amount);
		$amount = str_replace(' ', '', $amount);
		
		return $amount;
	}

}
