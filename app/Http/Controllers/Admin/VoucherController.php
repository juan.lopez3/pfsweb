<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Voucher;

class VoucherController extends Controller
{
	
	private $voucher;
	
	public function __construct(Voucher $voucher) {
		
		$this->voucher = $voucher;
		
		$this->middleware('superadmin');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.vouchers.list')->with('vouchers', $this->voucher->all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.vourhcers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\CreateVoucherRequest $request)
    {
        $voucher = $this->voucher->create($request->all());
		
		if($voucher) \Session::flash('success', 'Voucher was successfully created!');
		
		return redirect()->route('vouchers');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		return view('admin.vouchers.edit')->with('voucher', $this->voucher->findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\CreateVoucherRequest $request, $id)
    {
        $voucher = $this->voucher->findOrFail($id);
		$voucher->fill($request->all())->save();
		\Session::flash('success', 'Voucher was successfully updated!');
		
		return redirect()->route('vouchers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->voucher->destroy($id);
		\Session::flash('success', 'Voucher was successfully deleted!');
    }
}
