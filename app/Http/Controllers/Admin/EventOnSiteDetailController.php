<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\EventOnSiteDetail;
use App\Event;
use App\Technology;
use App\VenueRoom;
use App\User;
use App\EventVenueRoom;
use App\EventScheduleSlot;
use App\Events\DataWasManipulated;

class EventOnSiteDetailController extends Controller {
	
	private $onsite_detail;
	private $event;
	public $log_desc = "On-Site Details for Event ";
	
	public function __construct(EventOnSiteDetail $onsite_detail, Event $event) {
		
		$this->onsite_detail = $onsite_detail;
		$this->event = $event;
	}
	
	/**
	 * Display a listing of the on side details.
	 * @param id event id
	 * @return show on side details form
	 */
	public function index($id)
	{
		return view('admin.events.onsiteDetails.edit')->with('event', $this->event->find($id))
		->with('technologies', Technology::orderBy('title')->lists('title', 'id'))
		->with('hosts', User::select('id', DB::raw('CONCAT(first_name, " ", last_name) AS full_name'))->whereIn('role_id', [role('host')])->orderBy('first_name')->lists('full_name', 'id'))
		->with('tfi_staff', User::select('id', DB::raw('CONCAT(first_name, " ", last_name) AS full_name'))->whereIn('role_id', [role('event_coordinator'), role('event_manager')])->orderBy('first_name')->lists('full_name', 'id'));
	}

	/**
	 * Update the specified on side details in storage.
	 *
	 * @param  int  $id event id
	 * @return redirect to on side details
	 */
	public function update(Request $request, $id)
	{
		$event = $this->event->find($id);
		
		if (count($event->onSiteDetails)) {
			
			$onsite_detail = $this->onsite_detail->where('event_id', $id)->first();
			$onsite_detail->duty_manager_name = $request->get('duty_manager_name');
			$onsite_detail->duty_manager_number = $request->get('duty_manager_number');
			$onsite_detail->av_technician_name = $request->get('av_technician_name');
			$onsite_detail->av_technician_number = $request->get('av_technician_number');
			$onsite_detail->venue_note = $request->get('venue_note');
			$onsite_detail->save();
			
			event(new DataWasManipulated('actionUpdate', $this->log_desc.$event->title));
		} else {
			
			// create new record
			// venue staff
			$onsite_detail = $this->onsite_detail->create([
				'event_id' => $id,
				'duty_manager_name' => $request->get('duty_manager_name'),
				'duty_manager_number' => $request->get('duty_manager_number'),
				'av_technician_name' => $request->get('av_technician_name'),
				'av_technician_number' => $request->get('av_technician_number'),
				'venue_note' => $request->get('venue_note'),
			]);
			
			event(new DataWasManipulated('actionCreate', $this->log_desc.$event->title));
		}
		
		// venue staff
		//host person
		
		//detach hosts and add new hosts
		$detach = $event->atendeesByRole(role('host'))->lists('id');
		
		// if detaching empty array ir would remove all event related atendees
		if (empty($detach)) $detach = [0];
		
		$event->atendees()->detach($detach);
		
		if ($request->has('hosts_id')) {
		
			$event->atendees()->attach(array_unique($request->get('hosts_id')));
		}

		// tfi contact
		//detach hosts and add new hosts
		$detach = $event->atendeesByRole(role('event_coordinator'))->lists('id');
		
		// if detaching empty array ir would remove all event related atendees
		if (empty($detach)) $detach = [0];
		
		$event->atendees()->detach($detach);
		
		if ($request->has('tfi_contacts_id')) {
			
			$event->atendees()->attach(array_unique($request->get('tfi_contacts_id')));
		}
		
		//meeting space setup
		$meeting_spaces = array();
		
		$old = EventVenueRoom::where('event_id', $event->id)->lists('id');
		$new = ($request->has('venue_space_id')) ? $request->get('venue_space_id') : array();
		$deleted = array_diff($old, $new);
		
		if(!empty($deleted)) {
			
			EventVenueRoom::destroy($deleted);
		}
		
		//update previously added meeting spaces
		for($i=0; $i < sizeof($request->get('venue_space_id')); $i++) {
			
			$space = EventVenueRoom::find($request->get('venue_space_id')[$i]);
			$space->timestamps = false;
			$space->meeting_space_name = $request->get('meeting_space_name')[$i];
			$space->layout_style = $request->get('space_layout_style')[$i];
			$space->num_of_tables = !empty($request->get('num_of_tables')[$i]) ? $request->get('num_of_tables')[$i] : null;
			$space->setup_note = !empty($request->get('setup_note')[$i]) ? $request->get('setup_note')[$i] : null;
			$space->save();
		}
		
		// attach new meeting spaces
		$meeting_spaces = array();
		
		for($i=sizeof($request->get('venue_space_id')); $i < sizeof($request->get('venue_room_id')); $i++) {
			
			$meeting_spaces[$request->get('venue_room_id')[$i]] = array(
				'meeting_space_name' => $request->get('meeting_space_name')[$i],
				'layout_style' => $request->get('space_layout_style')[$i],
				'num_of_tables' => !empty($request->get('num_of_tables')[$i]) ? $request->get('num_of_tables')[$i] : null,
				'setup_note' => !empty($request->get('setup_note')[$i]) ? $request->get('setup_note')[$i] : null
			); 
		}
		
		$event->venueSpaces()->attach($meeting_spaces);
		
		//equipment
		$tech_sync = array();
		
		if ($request->has('technology_id')) {
		
			for($i=0; $i < sizeof($request->get('technology_id')); $i++) {
				
				$tech_sync[$request->get('technology_id')[$i]] = array(
																	'quantity' => $request->get('quantity')[$i],
																	'provided_by' => $request->get('provided_by')[$i]
																); 
			}
		}
		$event->technologies()->sync($tech_sync);

		return redirect()->route('events.onsiteDetails', $id);
	}
	
	/**
	 * Service to get room info by room id
	 * @param int $layout_style_id
	 * @return room info
	 */
	public function getMeetingRoomInfo(Requests\GetMeetingRoomInfoRequest $request) {
		
		if ($request->ajax()) {
			
			$layout_id = $request->get('layout_style_id');
			
			$room = VenueRoom::find($request->get('meeting_room_id'));
			
			switch ($layout_id) {
				case '1':
					$layout_capacity = $room->cabaret_capacity;
					break;
				case '2':
					$layout_capacity = $room->theatre_capacity;
					break;
				default:
					$layout_capacity = '';
					break;
			}
			
			$room_info = [
				'id' => $room->id,
				'layout_style_id' => $layout_id,
				'room_name' => $room->room_name,
				'max_tables' => $room->max_tables,
				'layout_capacity' => $layout_capacity
			];
			
			return response()->json($room_info);
		} else {
			return null;
		}
	}
	
	/**
	 * Get user info
	 * @param int $user_id
	 * @return user info
	 */
	public function getPersonInfo(Requests\GetPersonInfoRequest $request) {
	
		if ($request->ajax()) {
			
			$user = User::find($request->get('user_id'));
			
			return response()->json($user);
		} else {
			return null;
		}
	}
	
	/**
	 * Service to check if the meeting space is assigned to any sessions
	 * 
	 * @param  int  $id  event id
	 * @return true/false if meeting space is assigned
	 */
	public function checkSpaceInSessions(Request $request, $id) {
		
		if ($request->ajax()) {
			
			$sessions = EventScheduleSlot::where('event_id', $id)->where('meeting_space_id', $request->get('meeting_space_id'))->count();
			
			if($sessions > 0) {
				
				return response()->json(true);
			} else {
				
				return response()->json(false);
			}
		} else {
			
			return null;
		}
	}

}
