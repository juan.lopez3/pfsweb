<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Theme;
use App\Events\DataWasManipulated;

class ThemeController extends Controller {
    
    private $region;
    public $log_desc = "Theme ";
    
    /**
     * Display a listing of the themes.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.settings.themes.list')->with('themes', Theme::all());
    }

    /**
     * Show the form for creating a new theme.
     *
     * @return show region create form
     */
    public function create()
    {
        return view('admin.settings.themes.create');
    }

    /**
     * Store a newly created theme in storage.
     *
     * @return redirect to themes list
     */
    public function store(Request $request)
    {
        $theme = Theme::create($request->all());
        event(new DataWasManipulated('actionCreate', $this->log_desc.$theme->title));
        
        return redirect()->route('settings.themes');
    }

    /**
     * Show the form for editing the specified theme.
     *
     * @param  int  $id theme id
     */
    public function edit($id)
    {
        return view('admin.settings.themes.edit')->with('theme', Theme::findOrFail($id));
    }

    /**
     * Update the specified theme in storage.
     *
     * @param  int  $id theme id
     * @return redirect to themes list
     */
    public function update(Request $request, $id)
    {
        $theme = Theme::findOrFail($id);
        $theme->fill($request->input())->save();
        event(new DataWasManipulated('actionUpdate', $this->log_desc.$theme->title));
        
        return redirect()->route('settings.themes');
    }

    /**
     * Remove the specified region from storage.
     *
     * @param  int  $id region id
     * @return redirect to region list
     */
    public function destroy($id)
    {
        $theme = Theme::findOrFail($id); 
        if ($theme->delete()) event(new DataWasManipulated('actionDelete', $this->log_desc.$theme->title));
        
        return redirect()->route('settings.themes');
    }
    
    /**
     * upload any image
     */
    public function upload(Request $request)
    {
        // upload sponsor logo
        if($request->file('myfile')) {
            
            $saved_file = uploadFile($request->file('myfile'), BRANDING);
            
            return response()->json($saved_file);
        }
        
        return null;
    }

}
