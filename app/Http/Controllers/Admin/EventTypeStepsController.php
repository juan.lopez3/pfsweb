<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\EventTypeStep;
use App\EventType;
use App\Events\DataWasManipulated;

class EventTypeStepsController extends Controller {
	
	private $event_type_step;
	public $log_desc = "Event Type Step ";
	
	public function __construct(EventTypeStep $event_type_step) {
		
		$this->event_type_step = $event_type_step;
	}

	/**
	 * Show the form for creating a new event type step.
	 * @param int $id event type id
	 * @return show event type step create form
	 */
	public function create($id)
	{
		$next_order = $this->event_type_step->where('event_type_id', $id)->count() + 1;
		 
		return view('admin.settings.eventTypeSteps.create')->with('id', $id)->with('next_order', $next_order);
	}

	/**
	 * Store a newly created event type step in storage.
	 *
	 * @param int $id event type id
	 * @return redirect to event type step list
	 */
	public function store(Requests\CreateEventTypeStepRequest $request, $id)
	{
		$event_type_step = $this->event_type_step->create($request->all());
		event(new DataWasManipulated('actionCreate', $this->log_desc.$event_type_step->title));
		
		return redirect()->route('settings.eventTypeSteps', $id);
	}

	/**
	 * Display the specified event type step.
	 *
	 * @param  int  $id event type id
	 * @return Response
	 */
	public function show($id)
	{
		$event_type = EventType::find($id);
		
		return view('admin.settings.eventTypeSteps.list')->with('event_type', $event_type)
				->with('event_type_steps', $this->event_type_step->where('event_type_id', $id)->get());
	}

	/**
	 * Show the form for editing the specified event type step.
	 *
	 * @param  int  $id event id
	 * @param  int  $id2 event type step id
	 * @return show event type step edit form
	 */
	public function edit($id, $id2)
	{
		return view('admin.settings.eventTypeSteps.edit')->with('id', $id)->with('event_type_step', $this->event_type_step->find($id2));
	}

	/**
	 * Update the specified event type step in storage.
	 *
	 * @param  int  $id event type id
	 * @return redirect to event type step list
	 */
	public function update(Requests\CreateEventTypeStepRequest $request, $id)
	{
		$event_type_step = $this->event_type_step->find($id);
		$event_type_step->fill($request->input())->save();
		event(new DataWasManipulated('actionUpdate', $this->log_desc.$event_type_step->title));
		
		return redirect()->route('settings.eventTypeSteps', $event_type_step->event_type_id);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id  event type id
	 * @param  int  $id2  event type step id
	 * @return Response
	 */
	public function destroy($id, $id2)
	{
		$event_type_step = $this->event_type_step->find($id2);
		event(new DataWasManipulated('actionDelete', $this->log_desc.$event_type_step->title));
		$event_type_step->delete();
		
		return redirect()->route('settings.eventTypeSteps', $id);
	}

}
