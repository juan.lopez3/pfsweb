<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\MobileAppText;

use App\Events\DataWasManipulated;

class MobileAppTextController extends Controller {

	

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @return Response
	 */
	public function edit()
	{
	    $settings = MobileAppText::first();
        
		return view('admin.settings.appText.edit')->with('settings', $settings);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @return Response
	 */
	public function update(Request $request)
	{
		$settings = MobileAppText::firstOrNew([]);
        $settings->sign_up = $request->get("sign_up");
        $settings->forgot_pin = $request->get("forgot_pin");
        $settings->exchange_business_cards = $request->get("exchange_business_cards");
        $settings->no_connection = $request->get("no_connection");
        $settings->save();
        
        event(new DataWasManipulated('actionUpdate', ' mobile app text'));
        
        return redirect()->route('settings');
	}

}
