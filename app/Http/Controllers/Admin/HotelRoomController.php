<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Hotel;
use App\HotelRoom;
use App\EventSetting;

class HotelRoomController extends Controller
{
	
	private $hotel_room;
	
	public function __construct(HotelRoom $hotel_room, Hotel $hotel) {
		
		$this->hotel_room = $hotel_room;
		$this->hotel = $hotel;
		
		$this->middleware('superadmin');
	}
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		return view('admin.hotels.rooms.edit')->with('hotel', $this->hotel->findOrFail($id))->with('es', EventSetting::first());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$hotel = $this->hotel->findOrFail($id);
		$old_hotel_rooms = $hotel->rooms->lists('id')->toArray();
		$new_hotel_rooms = $request->has('room_id') ? $request->get('room_id') : array();
		$new_counter = 9999;
		$delete = array_diff($old_hotel_rooms, $new_hotel_rooms); // deleted items
		
		foreach($delete as $d) {
			
			$this->hotel_room->destroy($d);
		}
		
		for ($i=0; $i < sizeof($request->get('booking_night')); $i++) { 
			
			$r_id = "";
			
			if(!empty($request->get('room_id')[$i])) {
				
				// update
				$r_id = $request->get('room_id')[$i];
				$hotel_room = $this->hotel_room->find($request->get('room_id')[$i]);
			} else {
				
				//new
				$hotel_room = new HotelRoom();
			}
			
			$hotel_room->hotel_id = $id;
			$hotel_room->booking_night = date("Y-m-d", strtotime($request->get('booking_night')[$i]));
			$hotel_room->single_price = $request->get('single_price')[$i];
			$hotel_room->double_price = $request->get('double_price')[$i];
			
			if ($request->has('taxable_'.$r_id)) {
				$taxable = 1;
			} else {
				if($request->has('taxable_'.$new_counter)) {
					$taxable = 1;
					$new_counter++;
				} else {
					$taxable = 0;
					$new_counter++;
				}
			}
			$hotel_room->taxable = $taxable;
			$hotel_room->active_from = date("Y-m-d", strtotime($request->get('active_from')[$i]));
			$hotel_room->active_to = date("Y-m-d", strtotime($request->get('active_to')[$i]));
			$hotel_room->capacity = $request->get('capacity')[$i];
			$hotel_room->save();
		}
		
		\Session::flash('success', 'Hotel '.$hotel->title.' rooms were successfully updated!');
		
		return redirect()->route('hotels');
    }
}
