<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Specialism;

class SpecialismController extends Controller
{
	
	private $specialism;
	
	public function __construct(Specialism $specialism) {
		
		$this->specialism = $specialism;
		
		$this->middleware('superadmin');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.settings.specialisms.list')->with('specialisms', $this->specialism->all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.settings.specialisms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $specialism = $this->specialism->create($request->all());
		
		if($specialism) \Session::flash('success', 'Specialism was successfully created!');
		
		return redirect()->route('settings.specialisms');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		return view('admin.settings.specialisms.edit')->with('specialism', $this->specialism->findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $specialism = $this->specialism->findOrFail($id);
		$specialism->fill($request->all())->save();
		\Session::flash('success', 'Specialism was successfully updated!');
		
		return redirect()->route('settings.specialisms');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->specialism->destroy($id);
		\Session::flash('success', 'Specialism was successfully deleted!');
    }
}
