<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\EventTypeStep;
use App\TypeStepForm;
use App\Events\DataWasManipulated;

class TypeStepFormController extends Controller {
	
	protected $et_step;
	protected $step_form;
	public $log_desc = "Type Step Form ";
	
	public function __construct(EventTypeStep $et_step, TypeStepForm $step_from)
	{
		$this->et_step = $et_step;
		$this->step_form = $step_from;
	}
	/**
	 * Display a listing of the type step list.
	 *
	 * @return Response
	 */
	public function index($id)
	{
		return view('admin.settings.eventTypeStepForms.edit')->with('step', $this->et_step->find($id));
	}

	/**
	 * Update the specified type step in storage.
	 *
	 * @param  int  $id type step id
	 * @return redirect to type step list
	 */
	public function update(Request $request, $id)
	{
		
		$et_step = $this->et_step->find($id);
		
		if (!empty($et_step->form->form_title)) {
			
			$step_form = $this->step_form->where('event_type_step_id', $id)->first();
			$step_form->form_title = $request->get('form_title');
			$step_form->form_description = $request->get('form_description');
            if ($step_form->form_json != $request->get('form_json'))
			     $step_form->form_json = ($request->get('form_json') != "0") ? $this->replaceFormJson($request->get('form_json')) : "";
			$step_form->save();
		} else {
			
			$step_form = new TypeStepForm([
				'form_title' => $request->get('form_title'),
				'form_description' => $request->get('form_description'),
				'form_json' => $this->replaceFormJson($request->get('form_json'))]);
			
			$et_step->form()->save($step_form);
		}
		
		event(new DataWasManipulated('actionUpdate', $this->log_desc.$request->get('form_title')));
		
		return redirect()->route('settings.eventTypeSteps', $et_step->event_type_id);
	}
	
	/**
	 * Method to sanitarise string, remove unwanted tag in order to be able to generate form on edit
	 * @param string $json
	 * @return sanitarized string
	 */
	public function replaceFormJson($json = "") {
		
		$json = substr($json, 10);
		$json = substr($json, 0, strlen($json) - 1);
		
		return $json;
	}

}
