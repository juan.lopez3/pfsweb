<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;

use Illuminate\Http\Request;
use App\Event;
use App\AttendeeType;
use App\EventAttendee;
use App\EventAttendeeSlotSession;
use App\User;
use App\DietaryRequirement;
use App\UserAction;
use App\Payment;
use App\RegistrationStatus;
use App\Events\DataWasManipulated;
use App\EmailTemplate;
use App\SentEmail;
use App\Commands\SendEmail;
use App\UserRole;
use App\UserRoleSub;
use Response;


class EventAtendeeController extends Controller {

	protected $event;
	
	public function __construct(Event $event) {
		
		$this->event = $event;
	}
	
	/**
	 * Display listing for atendees
	 * @param int $id event id
	 * @return list of attendees for event
	 */
	public function index($id)
	{
        return view('admin.events.atendees.list')
        ->with('event', $this->event->find($id))
        ->with('registration_status', RegistrationStatus::lists('title', 'id'));
	}
	
	/**
	 * Load data for fancybox
	 * 
	 * @param  int  $user_id
	 * @param  int  $attendee_id
	 * 
	 * @return fancybox view of attendee for event
	 */
	public function show($event_id, $user_id)
	{
		$event = $this->event->find($event_id);
		$user = User::find($user_id);
		$attendee_slots = EventAttendeeSlotSession::where('event_attendee_id', $event->atendees()->where('user_id', $user_id)->first()->pivot->id)->whereIn('registration_status_id', [\Config::get('registrationstatus.booked'), \Config::get('registrationstatus.waitlisted'), \Config::get('registrationstatus.waiting_payment')])->get();
        $event_attendee = $event->atendees->where('id', $user->id)->first();
        $event_attendee_manually_cpd = EventAttendee::where('event_id',$event_id)->where('user_id', $user_id)->first();
        $payments = Payment::where('attendee_id', $event_attendee->pivot->id)->get();
        $emails = SentEmail::where('to', $user->email)->orderBy('created_at', 'desc')->take(10)->get();

		return view('admin.events.atendees.profile')
			->with('event', $event)
            ->with('event_attendee', $event_attendee)
            ->with('event_attendee_cpd', $event_attendee_manually_cpd)
			->with('registration_status', RegistrationStatus::lists('title', 'id'))
			->with('user', $user)
            ->with('booked_sessions', $attendee_slots)
            ->with('payments', $payments)
            ->with('emails', $emails);
	}
	
	/**
	 * Remove the specified attendee from storage.
	 *
	 * @param  int  $id event id
	 * @param  int $atendee_id atendee id
	 * @return redirect to event attendees list
	 */
	public function destroy($id, $atendee_id)
	{
		$event = $this->event->find($id)->atendees()->detach($atendee_id);
		
		return redirect()->route('events.atendees', $id);
	}
	
	/**
	 * Service to update attendee registration status for event
	 * 
	 * @param  int  $event_id
	 * @param  int  $attendee_id
	 */
	public function updateStatus(Requests\UpdateEventAttendeeStatusRequest $request, $event_id, $user_id)
	{
		if ($request->ajax()) {
			
			$event = $this->event->find($event_id);
			$attendee = $event->atendees()->where('user_id', $user_id)->first();
			
			// if status changed to deleted remove sessions
			if($request->get('value') == 2) {
				
				$attendee_slots = EventAttendeeSlotSession::where('event_attendee_id', $attendee->pivot->id)->delete();
			} else {
				
				//set session status same as event registration status
				$attendee_slots = EventAttendeeSlotSession::where('event_attendee_id', $attendee->pivot->id)->get();
				foreach($attendee_slots as $slot) {
					
					$slot->registration_status_id = $request->get('value');
					$slot->save();
				}
			}
			
			$attendee->pivot->registration_status_id = $request->get('value');
			$attendee->pivot->save();
			
			event(new DataWasManipulated('actionUpdate', "attendee: ".$attendee->first_name." ".$attendee->last_name." registration status for event:".$event->title));
			
			return 1;
		} else {
			
			return null;
		}
	}
	
	/**
	 * Update attendee booked sessions If ch/box checked attach, if unchecked detach
	 * 
	 * @param  int  $event_attendee_id foreign to event_atendees table
	 * 
	 * @return 1/0 if success
	 */
	public function updateBookedSession(Requests\UpdateBookedSessionRequest $request, $attendee_id)
	{
		if ($request->ajax()) {
			
			// if there is no status - means status not booked - remove, else change status or insert new
			if(!empty($request->get('status'))) {
					
				// try to find existing booking if attendee hasn't been booked before create new booking
				$new_session_slot = EventAttendeeSlotSession::where('event_attendee_id', $attendee_id)
					->where('event_schedule_slot_id', $request->get('schedule_slot_id'))->first();
			    
				if(!sizeof($new_session_slot)) $new_session_slot = new EventAttendeeSlotSession();
				
				$new_session_slot->event_attendee_id = $attendee_id;
				$new_session_slot->event_schedule_slot_id = $request->get('schedule_slot_id');
				$new_session_slot->registration_status_id = $request->get('status');
				$new_session_slot->save();
			} else {
				EventAttendeeSlotSession::where('event_attendee_id', $attendee_id)
				->where('event_schedule_slot_id', $request->get('schedule_slot_id'))
				->delete();
			}
			
			event(new DataWasManipulated('actionUpdate', "attendee booked session attendee ID:".$attendee_id.' session slot ID:'.$request->get('schedule_slot_id')));
			
			return 1;
		}
		
		return null;
	}
	
	/**
	 * AJAX service to update attendee payment status if not attended or late cancelled
	 */
	public function updatePaidStatus(Request $request, $attendee_id)
	{
		if ($request->ajax()) {
			
			$event_attendee = EventAttendee::findOrFail($attendee_id);
			$event_attendee->paid = ($request->get('paid') == 'true') ? 1 : 0;
			$event_attendee->save();
			
			event(new DataWasManipulated('actionUpdate', "attendee paid status for user ID:".$event_attendee->user_id.', Event ID: '.$event_attendee->event_id.', Paid: '.$request->get('paid')));
			
			return 1;
		}
		
		return null;
	}
	
    public function updateNotes(Request $request, $attendee_id)
    {
        if ($request->ajax()) {
            
            $event_attendee = EventAttendee::findOrFail($attendee_id);
            $event_attendee->notes = $request->get('notes');
            $event_attendee->save();
            
            event(new DataWasManipulated('actionUpdate', "attendee notes successfully updated for user ID:".$event_attendee->user_id));
            
            return 1;
        }
        
        return null;
    }

    public function updateManuallyCpd(Request $request, $attendee_id)
    {
        if ($request->ajax()) {
            
            $event_attendee = EventAttendee::findOrFail($attendee_id);
            $event_attendee->manually_cpd = $request->get('manually_cpd');
            $event_attendee->save();
            
            event(new DataWasManipulated('actionUpdate', "attendee CPD hours successfully updated for user ID:".$event_attendee->user_id));
            
            return 1;
        }
        
        return null;
    }
    
    /**
     * AJAX service to update attendee manual booking status
     */
    public function updateManualBookingStatus(Request $request, $attendee_id)
    {
        if ($request->ajax()) {
            
            $event_attendee = EventAttendee::findOrFail($attendee_id);
            $event_attendee->manual_booking = ($request->get('manual_booking') == 'true') ? 1 : 0;
            $event_attendee->save();
            
            event(new DataWasManipulated('actionUpdate', "attendee manual booking status for user ID:".$event_attendee->user_id.', Event ID: '.$event_attendee->event_id));
            
            return 1;
        }
        
        return null;
    }
    
    /**
     * AJAX service to update attendee manual booking status
     */
    public function updatefeeWaived(Request $request, $attendee_id)
    {
        if ($request->ajax()) {
            
            $event_attendee = EventAttendee::findOrFail($attendee_id);
            $event_attendee->fee_waived = ($request->get('fee_waived') == 'true') ? 1 : 0;
            $event_attendee->save();
            
            event(new DataWasManipulated('actionUpdate', "attendee cancellation fee waived status for user ID:".$event_attendee->user_id.', Event ID: '.$event_attendee->event_id));
            
            return 1;
        }
        
        return null;
    }
    
    /**
     * AJAX service to update attendee manual booking status
     */
    public function updateOnsiteBookingStatus(Request $request, $attendee_id)
    {
        if ($request->ajax()) {
            
            $event_attendee = EventAttendee::findOrFail($attendee_id);
            $event_attendee->onsite_booking = ($request->get('onsite_booking') == 'true') ? 1 : 0;
            $event_attendee->save();
            
            event(new DataWasManipulated('actionUpdate', "attendee onsite booking status for user ID:".$event_attendee->user_id.', Event ID: '.$event_attendee->event_id));
            
            return 1;
        }
        
        return null;
    }
    
    /**
     * 
     */
    public function updateCancellationReason(Request $request, $attendee_id)
    {
        $event_attendee = EventAttendee::findOrFail($attendee_id);
        $event_attendee->cancel_reason = $request->get('cancel_reason');
        $event_attendee->save();
        
        event(new DataWasManipulated('actionUpdate', "attendee cancellation reason for user ID:".$event_attendee->user_id.', Event ID: '.$event_attendee->event_id));
        
        return $attendee_id;
    }
    
	/**
	 * Manually attach attendee
	 */
	public function attachAttendee(Requests\AttachAttendeeRequest $request, $event_id)
	{
		// if user is not registered register
		$event = $this->event->find($event_id);
		
		if($event->atendees()->where('user_id', $request->get('attendee_id'))->count() == 0) {
            
            //creates an event_attende
            $event->atendees()->attach($request->get('attendee_id'));

            //creates a booking for timeslot because is required for CPD calcs
            //$tss = $event->scheduleTimeSlots()->get();    
            //$ea  = $event->atendees()->where('user_id', $request->get('attendee_id'))->first();         
            $ea = \App\EventAttendee::
            where('user_id', $request->get('attendee_id'))
            ->where('event_id',$event->id)
            ->first();
            $timeslots_ids = $request->get('timeslot_ids');
            if (count( $timeslots_ids )){
                foreach($timeslots_ids as $timeslot_id){
                    $ea->slots()->create(['event_schedule_slot_id' =>  $timeslot_id]);
                }
            }
            
            event(new DataWasManipulated('actionCreate', "attendee. Manually registered attendee: for event ID:".$event_id));
            
            // craete event for attendee
            $event_action = new UserAction();
            $event_action->user_id = $request->get('attendee_id');
            $event_action->action = "actionCreate";
            $event_action->details = "Was registered to event ".$event->title." by ".Auth::user()->first_name." ".Auth::user()->last_name;
            $event_action->save();
        }
        
		
		return redirect()->route('events.atendees', $event_id);
	}
	
	/**
	 * Possible attendees list for manual registration
	 * 
	 * 	@return attendes list in json
	 */
	public function getAttendeesList(Request $request, $event_id)
	{
		if ($request->ajax()) {
			$registered = $this->event->find($event_id)->atendees()->lists('user_id');
			
			$attendees = User::whereNotIn('id', $registered)
				->where(function($q) use($request) {
					$q->where('pin', 'like', '%'.$request->get('term').'%');
					$q->orWhere('first_name', 'like', '%'.$request->get('term').'%');
					$q->orWhere('last_name', 'like', '%'.$request->get('term').'%');
				})
				->orderBy('first_name')
				->select('id', 'pin', 'title', 'first_name', 'last_name')->take(50)->get();
			
			return response()->json($attendees);
		}
		
		return null;
	}
	
	/**
	 * send event invitation by pin(membership number)
	 */
	public function sendInvitation(Requests\InviteAttendeesRequest $request, $id)
	{
		$event = $this->event->find($id);
		$template = EmailTemplate::find(\Config::get('emailtemplates.invitation_admin'));
		$not_found = array();
		
		foreach(explode(",", $request->get('pins')) as $pin) {
			
			$user = User::where('pin', trim($pin))->first();
			
			if(sizeof($user)) {
				
                // if user doesn't have existing registration register him with invited status
                $registration = $event->atendees()->wherePivot('user_id', $user->id)->count();
                
                if (!$registration) {
                    
                    // registration not found - create
                    $user_invitation = $user->events()->attach($event->id, ['registration_status_id'=>config('registrationstatus.invited'), 'created_at'=>date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s")]);
                    
                    $this->dispatch(new SendEmail($user->email, $user->cc_email, $template->subject, $template->template, $user, $event));
                }
			} else {
				
				array_push($not_found, $pin);
			}
		}
		\Session::flash('success', 'Invitations were sent to all members that were found by membership number');
		
		if(!empty($not_found)) return redirect()->route('events.atendees', $event->id)->withErrors('Pins that were not found: '.implode(", ", $not_found));
		
		return redirect()->route('events.atendees', $event->id);
	}

    /**
     * Upload attendees list and book on the event
     */
	public function importAttendees(Requests\ImportAttendeesRequest $request, $event_id)
    {
       // try {
            $existing_users = array();
            $event = Event::findOrFail($event_id);
            
            \Excel::load($request->file('file_upload'), function ($reader) use($existing_users, $event) {
                
                $slots = $event->scheduleTimeSlots()->where('bookable', 1)->get();
                $bookable_slots = array();
                $waitlisted = true;

                // find out what booking status too use. If at least 1 slot is not full status will be booked.
                if ($event->schedule_version == 1) {
                    // for new version session are bookable so will use event capacity
                    if ($event->capacity > $event->atendees()->whereIn('registration_status_id', [config('registrationstatus.booked'), config('registrationstatus.waitlisted')])->count()) $waitlisted = false;
                } else {
                    // old version slots are bookable
                    foreach ($slots as $slot) {

                        $s = new EventAttendeeSlotSession();
                        $s->event_schedule_slot_id = $slot->id;

                        if($slot->slot_capacity <= $slot->attendees()->whereIn('registration_status_id', [config('registrationstatus.booked'), config('registrationstatus.waitlisted')])->count() ) {

                            #full add to waiting list
                            $s->registration_status_id = config('registrationstatus.waitlisted');
                        } else {

                            # there is some space register
                            $s->registration_status_id = config('registrationstatus.booked');
                            $waitlisted = false;
                        }

                        $bookable_slots[] = $s;
                    }
                }

                foreach ($reader->toArray() as $row) {
                    // check if user exist. if exist don't update anything just notes. Else create new user
                    $user = User::where('email', $row['email'])->first();

                    if (!sizeof($user)) {
                        $user = new User();

                        //$user->pin = $row['pin'];
                        $user->password = bcrypt($row['password']);
                        $user->title = !empty($row['title']) ? $row['title'] : "";
                        $user->first_name = $row['first_name'];
                        $user->middle_name = $row['middle_name'];
                        $user->last_name = $row['last_name'];
                        $user->company = $row['company'];
                        $user->email = trim($row['email']);
                        $user->cc_email   = isset($row['cc_email']) ? $row['cc_email'] : null;
                        $user->postcode   = isset($row['postcode']) ? $row['postcode'] : "SW7 2RL";
                        $user->job_title  = isset($row['job_title']) ? $row['job_title'] : null;
                        $user->pfs_class  = isset($row['pfs_class']) ? $row['pfs_class'] : null;
                        $user->chartered  = isset($row['chartered']) ? $row['chartered'] : 0;
                        $user->badge_name = isset($row['badge_name']) ? $row['badge_name'] : null;
                        $user->phone = isset($row['phone']) ? $row['phone'] : null;
                        $user->mobile = isset($row['mobile']) ? $row['mobile'] : null;

                        // try to find dietary requirement
                        if (!empty($row['dietary_requirements'])) {
                            $d_r = DietaryRequirement::where('title', $row['dietary_requirements'])->first();
                            if (sizeof($d_r)) $user->dietary_requirement_id = $d_r->id;
                        }
                        $user->dietary_requirement_other = isset($row['dietary_requirement_other'])?$row['dietary_requirement_other']:"";
                        $user->invited_by = isset($row['invited_by'])?$row['invited_by']:"";
                        $user->special_requirements = isset($row['special_requirements'])?$row['special_requirements']:"";
                        $user->bio = isset($row['bio'])?$row['bio']:"";
                        $user->available_in_chat = isset($row['available_in_chat']) ? $row['available_in_chat'] : 0;

                        $role = config('roles.uploaded_attendee');

                        // if role not empty find role id by name in the DB
                        if (!empty($row['role'])) {
                            $user_role = UserRole::where('title', $row['role'])->first();
                            if (sizeof($user_role)) $role = $user_role->id;
                        }

                        $user->role_id = $role;
                        $user->save();

                        // sync sub roles
                        if (!empty($row['sub_roles'])) {
                            $import_sub_roles = array_map('trim', explode(',', $row['sub_roles']));
                            $sub_roles_sync = array();

                            // try to find id in the db
                            foreach ($import_sub_roles as $sr) {

                                $sub_role = UserRoleSub::where('title', $sr)->first();
                                if (sizeof($sub_role)) array_push($sub_roles_sync, $sub_role->id);
                            }

                            //sync
                            $user->subRoles()->sync($sub_roles_sync);
                        }

                        // sync attendee types
                        if (!empty($row['attendee_types'])) {
                            $import_attendee_types = array_map('trim', explode(',', $row['attendee_types']));
                            $attendee_types_sync = array();

                            // collect attendee type id by name
                            foreach ($import_attendee_types as $at) {
                                $type = AttendeeType::where('title', $at)->first();

                                if (sizeof($type)) array_push($attendee_types_sync, $type->id);
                            }

                            // sync
                            $user->attendeeTypes()->sync($attendee_types_sync);
                        }

                        $message = "Dear $user->first_name $user->last_name, <br/> We are happy to inform that a new user account has been successfully created for you on the PFS Events portal.";
                        $message .= 'You can log in to the system, book events or access any events that have been booked for you by clicking <a href="https://events.thepfs.org/public/profile/events">here</a>. <br/>';
                        $message .= "<p><b>Your username: </b>$user->email <br/><b>Your password: </b>".$row['password']."</p>";

                        // send notification to new user with his details and login instructions
                       $this->dispatch(new SendEmail($user->email, $user->cc, "New account created", $message, $user, $event));
                    }

                    // book created user on event and bookable slots
                    $status = $waitlisted ? config('registrationstatus.waitlisted') : config('registrationstatus.booked');

                    // check if user has existing registration. If has update notes if no create new registration
                    $event_attendee = $event->atendees()->find($user->id);

                    // if existing registration found
                    if (sizeof($event_attendee)) {
                        $event_attendee->pivot->notes .= ' '.(isset($row['notes'])?$row['notes']:"");
                        $event_attendee->pivot->save();
                    } else {
                        $event_attendee = EventAttendee::create(['user_id'=>$user->id, 'event_id'=>$event->id, 'registration_status_id' => $status, 'notes' => $row['notes']]);

                        foreach ($bookable_slots as $slot) {

                            EventAttendeeSlotSession::create([
                                'event_attendee_id'      => $event_attendee->id,
                                'event_schedule_slot_id' => $slot->event_schedule_slot_id,
                                'registration_status_id' => $slot->registration_status_id
                            ]);
                        }
                    }

                    // craete action history for imported user
                    $event_action = new UserAction();
                    $event_action->user_id = $user->id;
                    $event_action->action = "actionCreate";
                    $event_action->details = "Was imported using upload attendees by ".Auth::user()->first_name." ".Auth::user()->last_name." and booked onto event ID: ".$event->id;
                    $event_action->save();
                }
                
                event(new DataWasManipulated('actionCreate', "Uploaded attendees for event ID:".$event->id));
                //$message = (empty($existing_users)) ? "" : "Import successfull, but some users were already in the database: ".implode(", ", $existing_users);
                
                return redirect()->route('events.atendees', $event->id);
            });
            
            return redirect()->route('events.atendees', $event->id);
      //  } catch (\Exception $e) {
            
          //  return redirect()->route('events.atendees', $event_id)->withErrors("Error importing user: ".$e->getMessage());
        //}
    }

    /**
	 * Download template excel exmaple
	 * 
	 */
    public function downloadExcel()
    {
        $file= public_path(). "/download/template.csv";

        $headers = array(
                'Content-Type: application/vnd.ms-excel',
                );

        return Response::download($file, 'template.csv', $headers);
    }
	
	
	/**
	 * Get user details
	 * 
	 * @param int user_id
	 * 	@return attendes list in json
	 */
	public function getAttendeeProfile(Request $request)
	{
		if ($request->ajax()) {
			
			$event_attendee = EventAttendee::find($request->get('event_attendee_id'));
			if (sizeof($event_attendee))
				return response()->json($event_attendee->user);
		}
		
		return null;
	}
}
