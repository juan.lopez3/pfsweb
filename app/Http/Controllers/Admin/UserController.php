<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use App\Country;
use App\UserRole;
use App\UserRoleSub;
use App\AttendeeType;
use App\DietaryRequirement;
use App\SentEmail;
use App\Events\DataWasManipulated;

class UserController extends Controller {
	
	private $user;
	public $log_desc = "profile for user ";
	
	public function __construct(User $user, UserRole $role) {

		$this->user = $user;
		$this->role = $role;
	}
	
	/**
	 * Display a listing of the users.
	 *
	 * @return show users list
	 */
	public function index()
	{
		return view('admin.users.list');
	}
	
	public function usersDatatable(Request $request){
		
		$search = $request->get('search');
		$search = $search["value"];
		$order = $request->get('order');
		$total = $this->user->where('role_id', '>=', \Auth::user()->role_id)->count();
		$table_fields = array('pin', 'first_name', 'last_name', 'email', 'postcode', 'role_id');
		$order_field = isset($table_fields[$order[0]['column']])?$table_fields[$order[0]['column']]:"last_name";
		
		// use skip and take
		$users = $this->user
			->where('role_id', '>=', \Auth::user()->role_id)
			->where(function($query) use ($search){
				$query->where('pin', 'like', '%'.$search.'%')
					->orWhere('first_name', 'like', '%'.$search.'%')
					->orWhere('last_name', 'like', '%'.$search.'%')
					->orWhere('email', 'like', '%'.$search.'%')
					->orWhere('postcode', 'like', '%'.$search.'%')
					->orWhere('company', 'like', '%'.$search.'%');
			})
			->orWhereHas('role', function($q) use($search){
				$q->where('title', 'like', '%'.$search.'%');
			})
			->select('id', 'pin', 'first_name', 'last_name', 'email', 'postcode', 'company', 'role_id')
			->with('role');
		
		$filtered = $users->count();
		
		$users = $users->skip($request->get('start'))
			->take($request->get('length'))
			->orderBy($order_field, $order[0]['dir'])
			->get();
		
		$data = array();
		
		foreach($users as $u) {
			
			$actions = '<a href="'.route('users.edit', $u->id).'"><button type="button" class="btn btn-warning btn-xs" title="Edit"><span class="glyphicon glyphicon-pencil"></span></button></a>
							<a href="'.route('users.listActions', $u->id).'"><button type="button" class="btn btn-info btn-xs" title="Actions history"><span class="md md-history"></span></button></a>';
			    
			    if(hasRoles(\Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
                $actions .= '<a href="'.route('users.login', $u->id).'"><button type="button" class="btn btn-default btn-xs" title="Log in as user"><span class="glyphicon glyphicon-eye-open"></span></button></a>';
				
				$actions .= '<button data-href="'.route('users.delete', $u->id).'" type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-trash"></span></button>';
			
			
			$data[] = array(
				$u->pin,
				$u->first_name,
				$u->last_name,
				$u->email,
				$u->postcode,
				$u->company,
				$u->role->title,
				$actions
			);
		}
		
		$return = array(
			'data' => $data,
			'recordsTotal' => $total,
			'recordsFiltered' => $filtered
		);
		
		return json_encode($return);
	}
	
	/**
	 * Display a listing user actions list
	 *
	 * @return show users actions list
	 */
	public function indexActions(Request $request, $id)
	{
		$request->flash();
		
		$user = $this->user->find($id);
		$actions_select = \Config::get('actiontypes');
		$actions_select = array_add($actions_select, 'all', 'All');
		asort($actions_select);
		
		$actions = $user->actions()->where('user_id', $id);
		
		if($request->has('date_from')) $actions->where('created_at', '>=', $request->get('date_from'));
		if($request->has('date_to')) $actions->where('created_at', '<=', $request->get('date_to'));
		if($request->has('action_select') && $request->get('action_select') != 'all') $actions->where('action', $request->get('action_select'));
		
		$actions = $actions->get();
		
		//dd($actions);
		
		return view('admin.users.listActions')->with('user', $user)
			->with('actions', $actions)
			->with('actions_select', $actions_select);
	}
	
	/**
	 * Show the form for creating a new user.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('admin.users.create')->with('roles', $this->getUserRoles())
			->with('user_roles_sub', UserRoleSub::lists('title', 'id'))
			->with('attendee_types', AttendeeType::lists('title', 'id'))
            ->with('countries', Country::lists('name', 'id'))
			->with('dietary_requirements', DietaryRequirement::lists('title', 'id'));
	}

	/**
	 * Store a newly created user in storage.
	 *
	 * @return redirect to users list
	 */
	public function store(Requests\CreateUserRequest $request)
	{
		$saved_file = "";
		if($request->file('profile_image')) {
			
			$saved_file = uploadFile($request->file('profile_image'), PROFILE);
		}
		
		$user = $this->user->create([
			'title' => $request->get('title'),
			'first_name' => $request->get('first_name'),
			'last_name' => $request->get('last_name'),
			'password' => Hash::make($request->get('password')),
			'email' => $request->get('email'),
			'cc_email' => $request->get('cc_email'),
			'postcode' => $request->get('postcode'),
			'company' => $request->get('company'),
			'job_title' => $request->get('job_title'),
			'phone' => $request->get('phone'),
			'mobile' => $request->get('mobile'),
			
			'badge_name' => $request->get('badge_name'),
			'dietary_requirement_id' => $request->get('dietary_requirement_id'),
			'dietary_requirement_other' => $request->get('dietary_requirement_other'),
			'special_requirements' => $request->get('special_requirements'),
			'bio' => $request->get('bio'),
			'profile_image' => $saved_file,
			
			'role_id' => $request->get('role_id')
		]);
		
		$sub_roles = array();
		
		if(in_array($request->get('role_id'), [\Config::get('roles.member'), \Config::get('roles.non_member')]))
			if($request->has('user_roles_sub_id')) $sub_roles = $request->get('user_roles_sub_id');
			
		$user->subRoles()->sync($sub_roles);
		$attendee_types = array();
		
		if ($request->has('attendee_types')) $attendee_types = $request->get('attendee_types');
			
		$user->attendeeTypes()->sync($attendee_types);
		
		event(new DataWasManipulated('actionCreate', $this->log_desc.$user->first_name." ".$user->last_name.". ID:".$user->id));
		
		return redirect()->route('users.list');
	}

	/**
	 * Show the form for editing the specified user.
	 *
	 * @param  int  $id  user id
	 * @return show user edit form
	 */
	public function edit($id)
	{
		$user = $this->user->find($id);
        $emails = SentEmail::where('to', $user->email)->orderBy('created_at', 'desc')->take(15)->get();
		
		// prevent changing higher role user
		if($user->role_id < \Auth::user()->role_id) return redirect()->route('users.list');
		
		return view('admin.users.edit')->with('user', $user)
			->with('roles', $this->getUserRoles())
			->with('user_roles_sub', UserRoleSub::lists('title', 'id'))
			->with('attendee_types', AttendeeType::lists('title', 'id'))
            ->with('countries', Country::lists('name', 'id'))
			->with('dietary_requirements', DietaryRequirement::lists('title', 'id'))
            ->with('emails', $emails);
	}

	/**
	 * Update the specified user in storage.
	 *
	 * @param  int  $id  user id
	 * @return redirect to users list
	 */
	public function update($id, Requests\UpdateUserRequest $request)
	{
		$user = $this->user->find($id);
		
		if($request->file('profile_image'))
			$user->profile_image = uploadFile($request->file('profile_image'), PROFILE);
		
		if($request->has('password'))
			$user->password = Hash::make($request->get('password'));
        if ($request->has('pin')) $user->pin = $request->get('pin');
		$user->title = $request->get('title');
		$user->first_name = $request->get('first_name');
		$user->last_name = $request->get('last_name');
		$user->email = $request->get('email');
		$user->cc_email = $request->get('cc_email');
		$user->phone = $request->get('phone');
		$user->mobile = $request->get('mobile');
		$user->postcode = $request->get('postcode');
		$user->country_id = $request->get('country_id');
		$user->company = $request->get('company');
		$user->job_title = $request->get('job_title');
		$user->badge_name = $request->get('badge_name');
		$user->dietary_requirement_id = $request->get('dietary_requirement_id');
		$user->dietary_requirement_other = $request->get('dietary_requirement_other');
		$user->special_requirements = $request->get('special_requirements');
		$user->bio = $request->get('bio');
		$user->role_id = $request->get('role_id');
		$user->save();
		
		$sub_roles = array();
		
		if(in_array($request->get('role_id'), [\Config::get('roles.member'), \Config::get('roles.non_member')]))
			if($request->has('user_roles_sub_id')) $sub_roles = $request->get('user_roles_sub_id');
			
		$user->subRoles()->sync($sub_roles);
		$attendee_types = array();
		
		if ($request->has('attendee_types')) $attendee_types = $request->get('attendee_types');
			
		$user->attendeeTypes()->sync($attendee_types);
		
		event(new DataWasManipulated('actionUpdate', $this->log_desc.$user->first_name." ".$user->last_name.". ID:".$user->id));
			
		return redirect()->route('users.list');
	}
	
	/**
	 * Update profile
	 *
	 * @param  int  $id  user id
	 * @return redirect to dashboard
	 */
	public function updateProfile($id, Requests\UpdateUserRequest $request)
	{
		$user = $this->user->find($id);
		
		if($request->file('profile_image'))
			$user->profile_image = uploadFile($request->file('profile_image'), PROFILE);
		
		if($request->has('password'))
			$user->password = Hash::make($request->get('password'));

		$user->title = $request->get('title');
		$user->first_name = $request->get('first_name');
		$user->last_name = $request->get('last_name');
		$user->email = $request->get('email');
		$user->cc_email = $request->get('cc_email');
		$user->phone = $request->get('phone');
		$user->mobile = $request->get('mobile');
		$user->postcode = $request->get('postcode');
		$user->company = $request->get('company');
		$user->job_title = $request->get('job_title');
		$user->badge_name = $request->get('badge_name');
		$user->dietary_requirement_id = $request->get('dietary_requirement_id');
		$user->dietary_requirement_other = $request->get('dietary_requirement_other');
		$user->special_requirements = $request->get('special_requirements');
		$user->bio = $request->get('bio');
		$user->save();
		
		$sub_roles = array();
		
		if(in_array($request->get('role_id'), [\Config::get('roles.member'), \Config::get('roles.non_member')]))
			if($request->has('user_roles_sub_id')) $sub_roles = $request->get('user_roles_sub_id');
			
		$user->subRoles()->sync($sub_roles);
		$attendee_types = array();
		
		if ($request->has('attendee_types')) $attendee_types = $request->get('attendee_types');
			
		$user->attendeeTypes()->sync($attendee_types);
		
		event(new DataWasManipulated('actionCreate', $this->log_desc.$user->first_name." ".$user->last_name.". ID:".$user->id));
			
		return redirect()->route('dashboard');
	}

	/**
	 * Remove the specified user from storage.
	 *
	 * @param  int  $id  user id
	 * @return redirect to users list
	 */
	public function destroy($id)
	{
		$user = $this->user->findOrFail($id);
		event(new DataWasManipulated('actionDelete', $this->log_desc.$user->first_name." ".$user->last_name.". ID:".$user->id));
		$user->destroy($id);
		
		return redirect()->route('users.list');
	}
	
	/**
	 * Show user profile
	 * 
	 * @param  int  $id  user id
	 * @return  show user profile
	 */
	public function profile($id)
	{
		if($id != \Auth::user()->id) return redirect()->route('dashboard');
		
		return view('admin.users.profile')->with('user', $this->user->find($id))
			->with('roles', $this->getUserRoles())
			->with('user_roles_sub', UserRoleSub::lists('title', 'id'))
			->with('attendee_types', AttendeeType::lists('title', 'id'))
			->with('dietary_requirements', DietaryRequirement::lists('title', 'id'));
	}

	/**
	 * Get loged in user possible roles to create/edit user
	 * 
	 * @return return roles under logged user role
	 */
	private function getUserRoles()
	{
		
		$roles = $this->role;
		
		if(\Auth::user()->role_id == 2) $roles = $roles->where('id', '>=', 2);
		if(\Auth::user()->role_id > 2) $roles = $roles->where('id', '>', 3);
		
		$roles = $roles->lists('title', 'id');
		
		return $roles;
	}
	
	/**
	 * Users
	 * 
	 * 	@return attendes list in json
	 */
	public function getList(Request $request)
	{
		if ($request->ajax()) {
			
			$users = User::where(function($q) use($request) {
					$q->where('pin', 'like', '%'.$request->get('term').'%');
					$q->orWhere('first_name', 'like', '%'.$request->get('term').'%');
					$q->orWhere('last_name', 'like', '%'.$request->get('term').'%');
				})
				->orderBy('first_name')
				->select('id', 'pin', 'title', 'first_name', 'last_name')->take(50)->get();
			
			return response()->json($users);
		}
		
		return null;
	}
    
    /**
     * Log in as user
     * 
     * @param int $userid
     * $return redirect to index
     */
    public function login($user_id)
    {
        $admin = \Auth::user();
        $user = $this->user->findOrFail($user_id);
        
        if ($user->role_id <=3) return redirect()->route('users.list');
        
        event(new DataWasManipulated('actionCreate', 'Manager logged in to user ID: '.$user->id.' profile'));
        \Auth::logout();
        \Auth::login($user);
        event(new DataWasManipulated('actionCreate', 'Manager ID: '.$admin->id.' logged in using login as user function'));
        
        return redirect()->route('events');
    }
}
