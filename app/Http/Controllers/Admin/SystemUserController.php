<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\Country;

class SystemUserController extends Controller
{
	
	private $user;
	
	public function __construct(User $user) {
		
		$this->user = $user;
		
		$this->middleware('superadmin');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.settings.systemUsers.list')->with('users', $this->user->where('role_id', \Config::get('roles.admin'))->orWhere('role_id', \Config::get('roles.manager'))->get())
			->with('phone_code', Country::select('phone_code', DB::raw('CONCAT(phone_code, " - ", name) AS full_code'))->orderBy('name')->lists('full_code', 'phone_code')->all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.settings.systemUsers.create')->with('phone_code', Country::select('phone_code', DB::raw('CONCAT(phone_code, " - ", name) AS full_code'))->orderBy('name')->lists('full_code', 'phone_code')->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\CreateSystemUserRequest $request)
    {
        $user = new User();
		$user->first_name = $request->get('first_name');
		$user->last_name = $request->get('last_name');
		$user->email = $request->get('email');
		$user->password = bcrypt($request->get('password'));
		$user->telephone_code = $request->get('telephone_code');
		$user->telephone_phone = $request->get('telephone_phone');
		$user->mobile_code = $request->get('mobile_code');
		$user->mobile_phone = $request->get('mobile_phone');
		$user->role_id = \Config::get('roles.admin');
		$user->save();
		
		if($user) \Session::flash('success', 'System user was successfully created!');
		
		return redirect()->route('settings.sysUsers');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		return view('admin.settings.systemUsers.edit')->with('user', $this->user->findOrFail($id))
			->with('phone_code', Country::select('phone_code', DB::raw('CONCAT(phone_code, " - ", name) AS full_code'))->orderBy('name')->lists('full_code', 'phone_code')->all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\UpdateSystemUserRequest $request, $id)
    {
        $user = $this->user->findOrFail($id);
		$user->first_name = $request->get('first_name');
		$user->last_name = $request->get('last_name');
		$user->email = $request->get('email');
		
		if($user->password != $request->get('password')) {
			$user->password = bcrypt($request->get('password'));
		}
		
		$user->telephone_code = $request->get('telephone_code');
		$user->telephone_phone = $request->get('telephone_phone');
		$user->mobile_code = $request->get('mobile_code');
		$user->mobile_phone = $request->get('mobile_phone');
		$user->save();
		
		\Session::flash('success', 'System user was successfully updated!');
		
		return redirect()->route('settings.sysUsers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	if($id == 1) {
    		\Session::flash('error', 'You can not delete the main user!');
			return;
    	} 
        $this->user->destroy($id);
		\Session::flash('success', 'System users wa successfully deleted!');
    }
}
