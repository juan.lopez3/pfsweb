<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Event;
use App\EventSponsor;
use App\Events\DataWasManipulated;

class EventSponsorController extends Controller {

	protected $event;
	
	public $log_desc = "Event sponsors for event ";
	
	public function __construct(Event $event)
	{
		$this->event = $event;
	}
	
	/**
	 * attach sponsor to event
	 * 
	 *  @param int $id event id
	 * 	@return redirect to event settings
	 */
	public function create(Request $request, $id)
	{
		$event = $this->event->find($id);
		
		if($request->has('sponsor_id')) $event->sponsors()->attach($request->get('sponsor_id'));
		
		event(new DataWasManipulated('actionCreate', $this->log_desc.$event->title));
		
		return redirect()->route('events.edit', $id);
	}
	
	/**
	 * 	Detach sponsor from event
	 * 
	 *  @param int $id event id
	 * 	@param int $sponsor_id
	 * 	@return redirect to event settings sponsor tab
	 */
	public function destroy($id, $sponsor_id)
	{
		$event = $this->event->find($id);
		$event->sponsors()->detach($sponsor_id);
		event(new DataWasManipulated('actionDelete', $this->log_desc.$event->title));
		
		return redirect()->route('events.edit', $id);
	}
    
    /**
     * Update event's sponsor type
     */
    public function updateType(Request $request)
    {
        if ($request->ajax()) {
            $sponsor = EventSponsor::findOrFail($request->get('sponsor_id'));
            $sponsor->type = $request->get('type');
            $sponsor->save();
            
            return response()->json('success');
        }
        
        return null;
    }
    
    /**
     * Update event's sponsor type
     */
    public function updateStandNumber(Request $request)
    {
        if ($request->ajax()) {
            $sponsor = EventSponsor::findOrFail($request->get('sponsor_id'));
            $sponsor->stand_number = $request->get('stand_number');
            $sponsor->save();
            
            return response()->json('success');
        }
        
        return null;
    }

    public function updateCategory(Request $request, $category)
    {
        if ($request->ajax()) {
            $sponsor = EventSponsor::findOrFail($request->get('sponsor_id'));
            $sponsor->{$category} = $request->get('category');
            $sponsor->save();

            return response()->json('success');
        }

        return null;
    }
}
