<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Venue;
use App\VenueFile;
use App\Event;
use App\User;
use App\VenueRoom;
use App\VenueNote;
use App\EventVenueRoom;
use App\EventScheduleSlot;
use App\Events\DataWasManipulated;

class VenueController extends Controller {
	
	private $venue;
	public $log_desc = "Event venue for event ";
	
	public function __construct(Venue $venue, Event $event) {
		
		$this->venue = $venue;
		$this->event = $event;
	} 
	
	/**
	 * Display a listing of the venues.
	 *
	 * @return Response
	 */
	public function index()
	{
		// if role is venue staff, show just assigned venues
		if(\Auth::user()->role_id == role('venue_staff')) {
			$venues = $this->venue->where('venue_staff_id', \Auth::user()->id)->get();
		} elseif(\Auth::user()->role_id == role('host')) {
				$venue_ids = \Auth::user()->events->lists('venue_id');
				$venues = $this->venue->whereIn('id', $venue_ids)->get();
		} else {
			$venues = $this->venue->all();
		}
		
		return view('admin.venues.list')->with('venues', $venues);
	}

	/**
	 * Show the form for creating a new resource. when open from venues list
	 *
	 * @return showvenue crate from
	 */
	public function create()
	{
		return view('admin.venues.create')
			->with('tfi_staff', User::select('id', \DB::raw('CONCAT(first_name, " ", last_name) AS full_name'))->whereIn('role_id', [role('venue_staff')])->orderBy('first_name')->lists('full_name', 'id'));
	}

	/**
	 * Store a newly created venue in storage.
	 *
	 * @param int id event id
	 * @return redirect to event venues tab
	 */
	public function store(Requests\CreateVenueRequest $request)
	{
		$venue = $this->venue->create([
			'name' => $request->get('name'),
			'address' => $request->get('address'),
			'city' => $request->get('city'),
			'county' => $request->get('county'),
			'postcode' => $request->get('postcode')
		]);
		
		$event = $this->event->find($request->get('event_id'));
		$event->venue_id = $venue->id;
		$event->save();
		event(new DataWasManipulated('actionCreate', $this->log_desc.$event->title));
		
		return redirect()->route('events.edit', $event->id);
	}

	/**
	 * Show the form for editing the specified venue. When opened from venues list
	 *
	 * @param  int  $id venue id
	 * @return Response
	 */
	public function edit($id)
	{
		return view('admin.venues.edit')
		->with('venue', $this->venue->find($id))
		->with('tfi_staff', User::select('id', \DB::raw('CONCAT(first_name, " ", last_name) AS full_name'))->whereIn('role_id', [role('venue_staff')])->orderBy('first_name')->lists('full_name', 'id'));
	}
	
	/**
	 * Store venue when opened from venues list
	 * 
	 * @return redirect to venues list
	 */
	 public function storeFromMenu(Requests\UpdateVenueRequest $request)
	 {
		$venue = $this->venue->create([
			'name' => $request->get('name'),
			'address' => $request->get('address'),
			'city' => $request->get('city'),
			'county' => $request->get('county'),
			'postcode' => $request->get('postcode'),
			'details' => $request->get('details'),
			'bedrooms' => $request->get('bedrooms')?$request->get('bedrooms'):0,
			'email' => $request->get('email'),
			'website' => $request->get('website'),
			'venue_staff_id' => $request->has('venue_staff_id') ? $request->get('venue_staff_id') : null,
			'venue_phone' => $request->get('venue_phone'),
			'show_map' => $request->has('show_map'),
			'longitude' => $request->get('longitude'),
			'latitude' => $request->get('latitude'),
			'rating' => $request->get('rating'),
			'min_guaranteed' => $request->get('min_guaranteed')?$request->get('min_guaranteed'):0,
			'commission' => $request->get('commission')?$request->get('commission'):0
		]);
		
		if($request->has('room_name')) {
			
			$rooms = array();
			
			for($i=0; $i < sizeof($request->get('room_name')); $i++) {
				
				$room = new VenueRoom([
					'room_name' => $request->get('room_name')[$i],
					'capacity' => $request->get('capacity')[$i],
					'cabaret_capacity' => $request->get('cabaret_capacity')[$i],
					'theatre_capacity' => $request->get('theatre_capacity')[$i],
					'max_tables' => $request->get('max_tables')[$i],
				]);
				$rooms[] = $room;
			}
			
			$venue->rooms()->saveMany($rooms);
		}
		event(new DataWasManipulated('actionCreate', $this->log_desc.'Venue: '.$venue->name));
		
		return redirect()->route('venues.list');
	 }
	 
	 /**
	 * Update venue when open from venues list
	 *
	 *	@param int $id  venue id
	 *  @return  redirect to venues list 
	 */
	public function updateFromMenu(Requests\UpdateVenueRequest $request, $id)
	{
		$venue = $this->venue->find($id);
		$venue->fill([
			'name' => $request->get('name'),
			'address' => $request->get('address'),
			'city' => $request->get('city'),
			'county' => $request->get('county'),
			'postcode' => $request->get('postcode'),
			'details' => $request->get('details'),
			'bedrooms' => $request->get('bedrooms')?$request->get('bedrooms'):0,
			'email' => $request->get('email'),
			'website' => $request->get('website'),
			'venue_staff_id' => $request->has('venue_staff_id') ? $request->get('venue_staff_id') : null,
			'venue_phone' => $request->get('venue_phone'),
			'show_map' => $request->has('show_map'),
			'longitude' => $request->get('longitude'),
			'latitude' => $request->get('latitude'),
			'rating' => $request->get('rating'),
			'min_guaranteed' => $request->get('min_guaranteed')?$request->get('min_guaranteed'):0,
			'commission' => ($request->get('commission'))?intval($request->get('commission')):0
		])->save();
		
		// remove deleted venue rooms and add new if were added
		$old = $venue->rooms->lists('id');
		$new = ($request->has('room_id')) ? $request->get('room_id') : array();
		$deleted = array_diff($old, $new);
		
		if (!empty($deleted)) {
			
			VenueRoom::destroy($deleted);
		}
		
		if($request->has('room_name')) {
			
			//update previously added meeting spaces
			for($i=0; $i < sizeof($request->get('room_id')); $i++) {
				
				$room = VenueRoom::find($request->get('room_id')[$i]);
				$room->room_name = $request->get('room_name')[$i];
				$room->capacity = $request->get('capacity')[$i]?$request->get('capacity')[$i]:0;
				$room->cabaret_capacity = $request->get('cabaret_capacity')[$i]?$request->get('cabaret_capacity')[$i]:0;
				$room->theatre_capacity = $request->get('theatre_capacity')[$i]?$request->get('theatre_capacity')[$i]:0;
				$room->max_tables = ($request->get('max_tables')[$i])?$request->get('max_tables')[$i]:0;
				$room->save();
			}
			
			//insert new rooms
			$rooms = array();
			
			for($i=sizeof($request->get('room_id')); $i < sizeof($request->get('room_name')); $i++) {
				
				$room = new VenueRoom([
					'room_name' => $request->get('room_name')[$i],
					'capacity' => ($request->get('capacity')[$i])?$request->get('capacity')[$i]:0,
					'cabaret_capacity' => $request->get('cabaret_capacity')[$i]?$request->get('cabaret_capacity')[$i]:0,
					'theatre_capacity' => $request->get('theatre_capacity')[$i]?$request->get('theatre_capacity')[$i]:0,
					'max_tables' => ($request->get('max_tables')[$i])?$request->get('max_tables')[$i]:0,
				]);
				$rooms[] = $room;
			}
			
			$venue->rooms()->saveMany($rooms);
		}
		event(new DataWasManipulated('actionUpdate', $this->log_desc.'Venue: '.$venue->name));
		
		return redirect()->route('venues.list');
	}
		
	/**
	 * Update the specified venue in storage.
	 *
	 * @param  int  $id event id
	 * @param  int  $venue_id
	 * @return Response
	 */
	public function update(Requests\UpdateVenueRequest $request, $id, $venue_id)
	{
		$venue = $this->venue->find($venue_id);
		$venue->fill([
			'name' => $request->get('name'),
			'address' => $request->get('address'),
			'city' => $request->get('city'),
			'county' => $request->get('county'),
			'postcode' => $request->get('postcode'),
			'details' => $request->get('details'),
			'bedrooms' => $request->get('bedrooms')?$request->get('bedrooms'):0,
			'email' => $request->get('email'),
			'website' => $request->get('website'),
			'venue_staff_id' => $request->has('venue_staff_id') ? $request->get('venue_staff_id') : null,
			'venue_phone' => $request->get('venue_phone'),
			'show_map' => $request->has('show_map'),
			'longitude' => $request->get('longitude'),
			'latitude' => $request->get('latitude'),
			'rating' => $request->get('rating'),
			'min_guaranteed' => $request->get('min_guaranteed')?$request->get('min_guaranteed'):0,
			'commission' => $request->get('commission')?$request->get('commission'):0
		])->save();
		
		// remove deleted venue rooms and add new if were added
		$old = $venue->rooms->lists('id');
		$new = ($request->has('room_id')) ? $request->get('room_id') : array();
		$deleted = array_diff($old, $new);
		
		if (!empty($deleted)) {
			
			VenueRoom::destroy($deleted);
		}
		
		if($request->has('room_name')) {
			
			//update previously added meeting spaces
			for($i=0; $i < sizeof($request->get('room_id')); $i++) {
				
				$room = VenueRoom::find($request->get('room_id')[$i]);
				$room->room_name = $request->get('room_name')[$i];
				$room->capacity = $request->get('capacity')[$i];
				$room->cabaret_capacity = $request->get('cabaret_capacity')[$i];
				$room->theatre_capacity = $request->get('theatre_capacity')[$i];
				$room->max_tables = $request->get('max_tables')[$i];
				$room->save();
			}
			
			//insert new rooms
			$rooms = array();
			
			for($i=sizeof($request->get('room_id')); $i < sizeof($request->get('room_name')); $i++) {
				
				$room = new VenueRoom([
					'room_name' => $request->get('room_name')[$i],
					'capacity' => $request->get('capacity')[$i],
					'cabaret_capacity' => $request->get('cabaret_capacity')[$i],
					'theatre_capacity' => $request->get('theatre_capacity')[$i],
					'max_tables' => $request->get('max_tables')[$i],
				]);
				$rooms[] = $room;
			}
			
			$venue->rooms()->saveMany($rooms);
		}
		event(new DataWasManipulated('actionUpdate', $this->log_desc.'Venue: '.$venue->name));
		
		return redirect()->route('events.edit', $id);
	}
	
	/**
	 * Service to get list of venuesfor autocomplete, by keywords
	 * 
	 * @param string $term get param keyword
	 * @return  list of venues
	 */ 
	public function getVenuesList(Request $request) {
		
		if ($request->ajax()) {
			
			$search = $request->get('term');
			$venues = $this->venue->where('name','like', '%'.$search.'%')->select('id', 'name')->get();
			
			return $venues->toJson();
		} else {
			
			return null;
		}
	}
	
	/**
	 * Attach venue to event that was used with previous event
	 * 
	 * @return redirect to event venues tab
	 */
	public function copyVenue(Requests\CopyVenueRequest $request) {
		
		$event = $this->event->find($request->get('event_id'));
		
		$event->venue_id = $request->get('old_venue_id');
		$event->save();
		event(new DataWasManipulated('actionCreate', $this->log_desc.$event->title));
		
		return redirect()->route('events.edit', $event->id);
	}
	
	/**
	 * Upload venue image
	 * 
	 * @return  saved file name
	 */
	public function uploadImage(Request $request) {
		
		// upload banner if selected.
		if($request->file('myfile')) {
			
			$saved_file = uploadFile($request->file('myfile'), IMAGE);
			
			$venue = $this->venue->find($request->get('venue_id'));
			
			if (empty($venue->image_1)) {
				
				$venue->image_1 = $saved_file;
			} else {
				
				$venue->image_2 = $saved_file;
			}
			
			$venue->save();
			
			return response()->json($saved_file);
		}
		event(new DataWasManipulated('actionCreate', 'image for venue '.$venue->name));
		
		return null;
	}
	
	/**
	 * Remove venue image
	 * 
	 * @param  int $id venue id
	 * @param  int $image_id
	 * @return redirect to event venue tab
	 */
	public function deleteImage($id, $image_id) {
		
		$event = Event::find($id);
		
		
		$venue = $this->venue->find($event->venue_id);
		$name = 'image_'.$image_id;
		removeFile($venue->$name, IMAGE);
		$venue->$name = "";
		$venue->save();
		event(new DataWasManipulated('actionDelete', 'image for venue '.$venue->name));
		
		return redirect()->route('events.edit', $id);
	}
	
	/**
	 * Service to check if the meeting spaces in the room have any schedule sessions.
	 * 
	 * @param  int  $id  room id
	 * @return  true/false  if room's meeting spaces have any sessions assigned
	 */
	public function checkRoom(Request $request)
	{
		if ($request->ajax()) {
				
			$meeting_spaces = EventVenueRoom::where('venue_room_id', $request->get('venue_room_id'))->lists('id');
			$sessions = EventScheduleSlot::whereIn('meeting_space_id', $meeting_spaces)->count();
			
			// any sessions in room's meeting spaces
			if($sessions > 0) {
				return response()->json(true);
			} else {
				return response()->json(false);
			}
		} else {
			
			return null;
		}
	}
	
	/**
	 * id venue id
	 */
	public function addNote(Request $request, $id)
	{
		$venue_note = new VenueNote();
		$venue_note->venue_id = $id;
        $venue_note->priority = $request->get('priority');
        $venue_note->tags = $request->get("tags");
		$venue_note->note = $request->get('note');
		$venue_note->user_id = \Auth::user()->id;
		$venue_note->save();
		
		return redirect()->route('venues.edit', $id);
	}
	
	/**
	 * id event id
	 */
	public function addNoteTab(Request $request, $id)
	{
		$event = $this->event->findOrFail($id);
		
		$venue_note = new VenueNote();
		$venue_note->venue_id = $event->venue_id;
        $venue_note->priority = $request->get('priority');
        $venue_note->tags = $request->get("tags");
		$venue_note->note = $request->get('note');
		$venue_note->user_id = \Auth::user()->id;
		$venue_note->save();
		
		return redirect()->route('events.edit', $id);
	}
    
    /**
     * Upload and attach file to the venue
     */
    public function uploadFile(Requests\UploadVenueFileRequest $request)
    {
        $file = $request->file('myfile');
        
        if($file) {
            
            $saved_file = uploadFile($file, VENUE);
            
            $venue_file = new VenueFile();
            $venue_file->venue_id = $request->get('venue_id');
            $venue_file->document_name = $saved_file;
            $venue_file->display_name = $file->getClientOriginalName();
            $venue_file->type = $file->getClientOriginalExtension();
            $venue_file->size = $file->getClientSize();
            $venue_file->save();
            
            event(new DataWasManipulated('actionCreate', ' file: '.$saved_file.', venue id: '.$request->get('venue_id')));
            
            return response()->json($saved_file);
        }
        
        return null;
    }
    
    public function removeFile($file_id)
    {
        $file = VenueFile::findOrFail($file_id);
        if ($file->delete()) event(new DataWasManipulated('actionDelete', ' file: '.$file->document_name.', venue id: '.$file->venue_id));
        
        if (file_exists('uploads/venue_files/'.$file->document_name)) unlink('uploads/venue_files/'.$file->document_name);
        
        return redirect()->route('venues.edit', $file->venue_id);
    }
    
    public function removeVenueFile($event_id, $file_id)
    {
        $file = VenueFile::findOrFail($file_id);
        if ($file->delete()) event(new DataWasManipulated('actionDelete', ' file: '.$file->document_name.', venue id: '.$file->venue_id));
        
        if (file_exists('uploads/venue_files/'.$file->document_name)) unlink('uploads/venue_files/'.$file->document_name);
        
        return redirect()->route('events.edit', $event_id);
    }
    
    public function changeStatus(Requests\ChangeVenueNoteStatusRequest $request) {
        
        $note = VenueNote::findOrFail($request->get('venue_id'));
        $note->priority = $request->get('priority');
        $note->save();
        
        return 1;
    }
    
    /**
     * Add venue note comment
     */
    public function addComment(Request $request, $note_id) {
        
        $note = VenueNote::findOrFail($note_id);
        $note->comment = $request->get('comment');
        $note->save();
        
        return redirect()->route('venues.edit', $note->venue_id);
    }
    
    /**
     * Drag and drop ability to reorder venue notes within priority
     */
    public function reorder(Request $request)
    {
        if($request->has('item')) {
            $i = 0;
            foreach($request->get('item') as $id) {
                
                $i++;
                $item = VenueNote::find($id);
                $item->order = $i;
                $item->save();
            }
            
            return \Response::json(array('success' => true));
        } else {
            return \Response::json(array('success' => false), 400);
        }
    }
}
