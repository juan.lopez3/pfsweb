<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Event;

class EventStatisticController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($id)
	{
		$event = Event::with('atendees')->find($id);
		
		return view('admin.events.statistics.list')->with('event', $event)
			->with('sessions_chart_data', $this->getSessionsStats($event))
			->with('event_registrations_data', $this->getEventRegistrations($event));
	}
	
	private function getEventRegistrations($event)
	{
		/*old query 
		$first_reg = $event->atendees()->whereIn('role_id', [role('member'), role('non_member')])
			->where('registration_status_id', \Config::get('registrationstatus.booked'))
			->orderBy('event_atendees.created_at')->first(); */

		$first_reg = $event->atendees()->where('registration_status_id', \Config::get('registrationstatus.booked'))
		->orderBy('event_atendees.created_at')->first();
		
		if(sizeof($first_reg) > 0) {
			
			$start = $month = strtotime($first_reg->pivot->created_at);
			$end = strtotime(date("Y-m-d"));
			$end = strtotime("+1 days", $end);
			
			while($month <= $end)
			{
				/*Old query
				 $data[] = [
					'date' => date('d/m/Y', $month),
					'registrations' => $event->atendees()->whereIn('role_id', [role('member'), role('non_member')])
											->where('registration_status_id', \Config::get('registrationstatus.booked'))
											->where('event_atendees.created_at', '>=', date("Y-m-d 00:00:00", $month))
											->where('event_atendees.created_at', '<=', date("Y-m-d 23:59:59", $month))->count()
				]; */
				$data[] = [
					'date' => date('d/m/Y', $month),
					'registrations' => $event->atendees()->where('registration_status_id', \Config::get('registrationstatus.booked'))
											->where('event_atendees.created_at', '>=', date("Y-m-d 00:00:00", $month))
											->where('event_atendees.created_at', '<=', date("Y-m-d 23:59:59", $month))->count()
				];
				
				$month = strtotime("+1 day", $month);
			}
			
			return json_encode($data);
		}
		
		return null;
	}
	
	private function getSessionsStats ($event)
	{
		$slots = $event->scheduleTimeSlots()->where('bookable', 1)->with('attendees')->get();
		$data = array();
		
		foreach($slots as $slot) {
			$booked = $slot->attendees()->where('registration_status_id', \Config::get('registrationstatus.booked'))->count();
			
			$data[] = [
				'x' => $slot->title." Capacity: ".$slot->slot_capacity,
				'booked' => $booked,
				'remaining' => $slot->slot_capacity - $booked,
				'waitlisted' => $slot->attendees()->where('registration_status_id', \Config::get('registrationstatus.waitlisted'))->count()
			];
		}
		
		return json_encode($data);
	}

}
