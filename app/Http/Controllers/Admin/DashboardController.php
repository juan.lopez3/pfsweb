<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use Illuminate\Http\Request;
use App\Event;
use App\EventAttendee;
use App\User;
use App\UserAction;
use App\AttendeeType;
use Carbon\Carbon;

use App\SentEmail;
use App\EventAttendeeSlotSession;

use App\Commands\SendEmail;


class DashboardController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('admin.dashboard.list')->with('events_registrations', $this->getEventsRegistrations());
	}
	
	private function getEventsRegistrations()
	{
	    $data = \Cache::remember('dashboard-events', 10, function(){
	    
    		$attendees = EventAttendee::where('registration_status_id', \Config::get('registrationstatus.booked'))->orderBy('created_at')->get();
    		$first_reg = $attendees->first();
    		
    		if(sizeof($first_reg) > 0) {
    			
    			$start = $month = strtotime(Carbon::now()->subMonths(3));
    			$end = strtotime(date("Y-m-d"));
    			$end = strtotime("+1 days", $end);
    			
    			while($month <= $end)
    			{
    				$data[] = [
    					'date' => date('d/m/Y', $month),
    					'registrations' => EventAttendee
    						//::leftJoin('users', 'users.id', '=', 'event_atendees.user_id')
    						::where('registration_status_id', \Config::get('registrationstatus.booked'))
    						//->whereIn('role_id', [role('member'), role('non_member')])
    						->where('event_atendees.created_at', '>=', date("Y-m-d 00:00:00", $month))
    						->where('event_atendees.created_at', '<=', date("Y-m-d 23:59:59", $month))
    						->count()
    				];
    				
    				$month = strtotime("+1 day", $month);
    			}
    			
    			return json_encode($data);
    		}
        });
		
		return $data;
	}
    
    
    
    public function usersLive($period = 30, $interval = 5)
    {
        $to = Carbon::now();
        $from = Carbon::now()->subMinutes($period);
        $data = array();
        
        $iterate = TRUE;
        
        while ($iterate) {
            
            $interval_to = $from->addMinutes($interval);
            
            $live_count = UserAction::where('action', 'login')
                ->where(DB::raw('date(created_at)'), '>=', $from)
                ->where(DB::raw('date(created_at)'), '>=', $interval_to)
                ->count();
            
            $data[] = array(
                'x' => $from->toDateTimeString(),
                'y' => $live_count
            );
            
            $from = $interval_to;
            
            $date1 = $from->toDateTimeString();
            $date2 = $to->toDateTimeString();
            $date1 = strtotime($date1);
            $date2 = strtotime($date2);
            
            if ($date1 > $date2) $iterate = FALSE;
        }
        //dd($data);
        return response()->json(json_encode($data));
    }
    
    public function fix()
    {
        // 23473 - 22502
        $emails = SentEmail::where('id', '>=', 23317)->where('id', '<=', 23473)
            ->where('subject', 'like', 'Your booking%')->get();
        
        $inserted = array();
        
        foreach ($emails as $email) {
            
            //$event = Event::find($email->event_id);
            $event = Event::find($email->event_id);
            
            // check if has existing registration for event if not create
            $registration = EventAttendee::where('event_id', $event->id)->where('user_id', $email->user_id)->first();
            
            //if registration existing found
            if (sizeof($registration)) continue;
            
            $new_reg = new EventAttendee();
            $new_reg->user_id = $email->user_id;
            $new_reg->event_id = $email->event_id;
            $new_reg->created_at = $email->created_at;
            $new_reg->updated_at = $email->updated_at;
            $new_reg->save();
            
            // search for booked slots in the email
            foreach ($event->scheduleTimeSlots as $slot) {
                
                $found = strpos($email->template, $slot->title);
                
                if ($found) {
                    
                    //book on slot
                    $slot_booking = new EventAttendeeSlotsession();
                    $slot_booking->event_attendee_id = $new_reg->id;
                    $slot_booking->event_schedule_slot_id = $slot->id;
                    $slot_booking->created_at = $email->created_at;
                    $slot_booking->updated_at = $email->updated_at;
                    $slot_booking->save();
                }
            }
        }
        
        dd('success');
    }
	
}
