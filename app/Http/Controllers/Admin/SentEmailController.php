<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Events\DataWasManipulated;

use Illuminate\Http\Request;
use App\SentEmail;
use App\ClickedLinksLog;

class SentEmailController extends Controller
{
    private $sent_emails;
    
    public function __construct(SentEmail $sent_emails)
    {
        $this->sent_emails = $sent_emails;
    }
    
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.sentEmails.list');
    }
    
    public function show($id)
    {
        return view('admin.sentEmails.view')->with('sent_email', $this->sent_emails->find($id));
    }
    
    public function dataTable(Request $request)
    {
        $search = $request->get('search');
        $search = $search["value"];
        $order = $request->get('order');
        $table_fields = array('created_at', 'first_name', 'last_name', 'event', 'to', 'subject');
        $order_field = $table_fields[$order[0]['column']];
        $total = $this->sent_emails->count();
        
        // use skip and take
        $emails = $this->sent_emails
            ->where(function ($query) use ($search) {
                $query->where('to', 'like', '%'.$search.'%');
                $query->orWhere('subject', 'like', '%'.$search.'%');
                //->orWhere('template', 'like', '%'.$search.'%')
                    //->orWhere('created_at', 'like', '%'.$search.'%');
            })
            ->orWhereHas('event', function ($q) use ($search) {
                $q->where('title', 'like', '%'.$search.'%');
            })
            /*
            ->orWhereHas('user', function($q) use($search){
                $q->Where('first_name', 'like', '%'.$search.'%');
                $q->orWhere('last_name', 'like', '%'.$search.'%');
            })
             *
             */
            ->with('user', 'event')
            ->select('id', 'to', 'subject', 'user_id', 'event_id', 'opened', 'created_at');
        
        $filtered = $emails->count();
        
        $emails = $emails->skip($request->get('start'))
            ->take($request->get('length'))
            ->orderBy($order_field, $order[0]['dir'])
            ->get();
        
        $data = array();
        
        foreach ($emails as $u) {
            $actions = '<a href="'.route('emails.view', $u->id).'"><button type="button" class="btn btn-info btn-xs" title="View Message"><span class="glyphicon glyphicon-envelope"></span></button></a>';

            $data[] = array(
                $u->id,
                date("d/m/Y H:i:s", strtotime($u->created_at)),
                sizeof($u->event) ? $u->event->title : '',
                sizeof($u->user) ? $u->user->first_name : '',
                sizeof($u->user) ? $u->user->last_name : '',
                $u->to,
                $u->subject,
                (!empty($u->opened)) ? date("d/m/y H:i", strtotime($u->opened)) : "",
                $actions
            );
        }
        
        $return = array(
            'data' => $data,
            'recordsTotal' => $total,
            'recordsFiltered' => $filtered
        );
        
        return json_encode($return);
    }

    /**
     * Marks email as read and returns 1px image
     * @param $email
     */
    public function markOpened($sent_email_id, $email)
    {
        $sent_email = SentEmail::find($sent_email_id);

        // if sent email found and email match mark as opened
        if (sizeof($sent_email) && $email == $sent_email->to) {
            $sent_email->update(['opened' => date("Y-m-d H:i:s")]);

            // log to user history about opened email
            event(new DataWasManipulated('emailOpened', 'Send Email ID: '.$sent_email->id));
        }

        // open the file in a binary mode
        $name = 'images/1px.png';
        $fp = fopen($name, 'rb');

        // send the right headers
        header("Content-Type: image/png");
        header("Content-Length: " . filesize($name));

        // dump the picture and stop the script
        fpassthru($fp);
        exit;
    }

    /** Log clicked link and redirect
     * @param $sent_email_id
     * @param $email
     */
    public function logClick(Request $request, $sent_email_id, $email)
    {
        $sent_email = SentEmail::find($sent_email_id);

        if (sizeof($sent_email) && $email == $sent_email->to) {
            try {
                //log link click
                ClickedLinksLog::create(
                    [
                      'sent_email_id' => $sent_email->id,
                      
                      'link' => ($request->get('url'))?$request->get('url'):\Request::route()->getName()
                    ]
                );
            } catch (Exception $e) {
                echo 'Caught exception: ',  $e->getMessage(), "\n";
            }
        }

        return redirect($request->get('url'));
    }
}
