<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\User;
use App\EventAttendee;
use App\EventAttendeeSlotSession;
use App\Event;
use App\EventScheduleSlotSessionContributor;
use App\Events\DataWasManipulated;

class EventContributorController extends Controller {

	public $log_desc = "Contributors for event ";
	/**
	 * Store contributor.
	 * @param int $id  event id
	 * @return redirect to event settings contributors tab
	 */
	public function store(Requests\CreateContributorRequest $request, $id)
	{
		$event = Event::find($id);
		
		//if contributor was not added then attach. Avoid same contributor to add twice
		if(!in_array($request->get('contributor_id'), $event->atendees->lists('id'))) {
			$event_attendee = new EventAttendee();
            $event_attendee->user_id = $request->get('contributor_id');
            $event_attendee->event_id = $event->id;
            $event_attendee->save();
            
            // book on slots if event has
            foreach ($event->scheduleTimeSlots as $slot) {
                
                $s = new EventAttendeeSlotSession();
                $s->event_attendee_id = $event_attendee->id;
                $s->event_schedule_slot_id = $slot->id;
                $s->save();
            }
            
			event(new DataWasManipulated('actionCreate', $this->log_desc.$event->title));
		}
		
		return redirect()->route('events.edit', $id);
	}

	/**
	 * Remove the specified contributor from storage.
	 *
	 * @param  int  $id event id
	 * @param int $atendee_id attendee id
	 * @return redirect to event settings contributors tab
	 */
	public function destroy($id, $atendee_id)
	{
		Event::find($id)->atendees()->detach($atendee_id);
		event(new DataWasManipulated('actionDelete', $this->log_desc.$id.' Attendee ID: '.$atendee_id));
		
		return redirect()->route('events.edit', $id);
	}
	
	/**
	 * Copy contributors from previous event
	 * @param int id event id
	 * @param int old_c_event_id old event id
	 * @return redirect to event settings
	 */
	public function copy(Request $request, $id) {
		
		$contributors = Event::find($request->get('old_c_event_id'))->atendees()->with('subRoles')->whereIn('role_id', [role('member'), role('non_member')])->get();
		$contributors_assign = array();
		$event = Event::find($id);
		$event_attendees = $event->atendees->lists('id');
		
		foreach($contributors as $contr) {
			
			// if member, non member is assigned with subroles - he's contributor. > copy
			if(sizeof($contr->subRoles) > 0) {
				
				// check if user was not already assigned. if no - insert into attendees
				if(!in_array($contr->id, $event_attendees)) $event->atendees()->attach([$contr->id]);
			}
			
		}
		
		event(new DataWasManipulated('actionCreate', $this->log_desc.'ID: '.$id));
		return redirect()->route('events.edit', $id);
	}
	
	/**
	 * Service to get list of users that are contributors for autocomplete, by keywords
	 * @param string $term get param keyword
	 * @return list of contributors by search term
	 */ 
	public function getContributorsList(Request $request) {
		
		if ($request->ajax()) {
			
			$search = $request->get('term');
			$contributors = User::whereIn('role_id', [role('member'), role('non_member')])
				->where('badge_name','like', '%'.$search.'%')
				->has('subRoles')
				->select('id', 'badge_name')->get();
			
			return $contributors->toJson();
		} else {
			
			return null;
		}
	}
	
	/**
	 * Service to check if contributor that we want to delete is assigned to sessions
	 * 
	 * @param  int  $id  event id
	 * @param  int  $user_id  
	 * @return treu/false if user is assigned to any session
	 */
	public function isUserInSessions(Request $request, $id)
	{
	 	if ($request->ajax()) {
		 	$sessions = Event::find($id)->scheduleSessions->lists('id');
			$session_contributors = EventScheduleSlotSessionContributor::whereIn('slot_session_id', $sessions)->lists('user_id');
			$assigned = in_array($request->get('user_id'), $session_contributors);
			
			return response()->json($assigned);
		} else {
			
			return null;
		}
	}
	
	/**
	 * Service to check if event schedule has any contributors
	 * 
	 * @param  int  $id  event id
	 * @return treu/false if user is assigned to any session
	 */
	public function checkSessionContributors(Request $request, $id)
	{
	 	if ($request->ajax()) {
		 	$sessions = Event::find($id)->scheduleSessions->lists('id');
			$session_contributors = EventScheduleSlotSessionContributor::whereIn('slot_session_id', $sessions)->lists('user_id');
			$assigned = empty($session_contributors) ? false: true;
			
			return response()->json($assigned);
		} else {
			
			return null;
		}
	}
    
    public function toggleComitee(Request $request)
    {
        if ($request->ajax()) {
            $event_attendee = EventAttendee::find($request->get('event_attendee_id'));
            $event_attendee->comitee = $request->get('comitee');
            $event_attendee->save();
            
            return response()->json(true);
        }
        
        return response()->json(false);
    }
}
