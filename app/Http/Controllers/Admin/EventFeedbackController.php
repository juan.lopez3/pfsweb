<?php namespace App\Http\Controllers\Admin;

use App\EventFaq;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\EventFeedback;
use App\Event;
use App\User;
use App\SponsorSetting;
use App\Sponsor;
use App\UserFeedback;
use App\Events\DataWasManipulated;

class EventFeedbackController extends Controller {

	public $log_desc = "Feedback For event ID: ";

	/**
	 * Store a newly created feedback in storage.
	 *
	 * @param int $id event id
	 * @return redirect to event feedback tab
	 */
	public function store(Requests\CreateFeedbackRequest $request, $id)
	{
		EventFeedback::create([
			'question' => $request->get('question'),
			'answer' => $request->get('answer'),
			'type' => $request->get('type'),
			'checkin_required' => $request->has('checkin_required'),
			'event_id' => $id,
			'session_id' => ($request->has('session_id')) ? $request->get('session_id') : null
		]);
		
		event(new DataWasManipulated('actionCreate', $this->log_desc.$id));
		
		return redirect()->route('events.edit', $id);
	}
	
	/**
	 * Show the form for editing the specified faq.
	 *
	 * @param int $event id
	 * @param int $feedback_id
	 * @return show feedback edit form
	 */
	public function edit($event_id, $feedbaack_id)
	{
		$event = Event::find($event_id);
		$sessions = $event->scheduleSessions()->select('event_schedule_slot_sessions.id', \DB::raw('CONCAT_WS(" ", DATE_FORMAT(session_start,"%H:%i"), "-", DATE_FORMAT(session_end,"%H:%i"), event_schedule_slot_sessions.title) AS full_name'))->orderBy('session_start')->lists('full_name', 'id');
		return view('admin.events.feedbacks.edit')->with('event', $event)
			->with('feedback', EventFeedback::find($feedbaack_id))
			->with('sessions', $sessions);
	}
	
	/**
	 * Update the specified feedback in storage.
	 *
	 * @param  int  $event_id
	 * @param  int  $feedback_id
	 * @return redirect to event faq tab
	 */
	public function update(Requests\CreateFeedbackRequest $request, $event_id, $feedback_id)
	{
		$feedback = EventFeedback::find($feedback_id);
		$feedback->fill($request->input());
		$feedback->session_id = $request->has('session_id') ? $request->get('session_id') : null;
        $feedback->checkin_required = $request->has('checkin_required');
        $feedback->save();
		
		event(new DataWasManipulated('actionUpdate', 'ID: '.$event_id.' Feedback ID: '.$feedback->id));
		
		return redirect()->route('events.edit', $event_id);
	}
	
	/**
	 * Remove the specified feedback from storage.
	 *
	 * @param  int  $id event id
	 * @param  int  $feed_id feedback id
	 * @return redirect to event feedback tab
	 */
	public function destroy($id, $feed_id)
	{
		EventFeedback::destroy($feed_id);
		
		event(new DataWasManipulated('actionDelete', $this->log_desc.$id));
		
		return redirect()->route('events.edit', $id);
	}
	
    /**
      * Leave or edit feedback for the user
      */
    public function leaveFeedback($event_id, $user_id)
    {
        $event = Event::findOrFail($event_id);
        $user = User::findOrFail($user_id);
        $event_feedback = EventFeedback::where('event_id', $event->id)->whereNull('session_id')->get();
        $feedback_answers = $user->feedbackAnswers->lists('answer', 'feedback_id');
        
        return view('admin.events.feedbacks.leave_feedback')
            ->with('event', $event)
            ->with('feedback_answers', $feedback_answers)
            ->with('user', $user)
            ->with('event_feedback', $event_feedback);
    }
    
    /**
     * Save or update user's feedback
     */
    public function submitFeedback(Request $request, $event_id, $user_id)
    {
        $event = Event::findOrFail($event_id);
        $user = User::findOrFail($user_id);
        
        // if updating remove old values
        if (sizeof($user->feedbackAnswers)) $user->feedbackAnswers()->delete();
        
        foreach($request->except('_token', '_method') as $fb_id => $fb) {
            
            $feedback = new UserFeedback();
            $feedback->user_id = $user->id;
            $feedback->event_id = $event->id;
            $feedback->feedback_id = $fb_id;
            
            $feedback->answer = is_array($fb) ? implode(", ", $fb) : $fb;
            $feedback->save();
        }
        
        event(new DataWasManipulated('actionCreate', ' filled feedback form for user ID: '.$user_id.' event ID: '.$event_id));
        
        return redirect()->route('events.onsite', $event_id);
    }
    
	/**
	 * Generate feedback from schedule sessions
	 * 
	 * 	@param int  $event_id
	 */
	public function generate($event_id)
	{
		$event = Event::find($event_id);
		
		foreach($event->scheduleSessions()->orderBy('session_start')->with('type')->get() as $session) {
			if ($session->type->include_in_feedback) {
			
				EventFeedback::create([
    				'question' => "Speaker",
    				'answer' => "1;2;3;4;5;6;7;8;9;10",
    				'type' => 1, #rating
    				'event_id' => $event_id,
    				'session_id' => $session->id
    			]);
    			
    			EventFeedback::create([
    				'question' => "Content",
    				'answer' => "1;2;3;4;5;6;7;8;9;10",
    				'type' => 1, #rating
    				'event_id' => $event_id,
    				'session_id' => $session->id
    			]);
                
                EventFeedback::create([
                    'question' => "Request contact from speaker / company",
                    'answer' => "Please contact me",
                    'type' => 2, #checkbox
                    'event_id' => $event_id,
                    'session_id' => $session->id
                ]);
                
                EventFeedback::create([
                    'question' => "Feedback comments / reasons for contact request",
                    'answer' => "",
                    'type' => 5, #text
                    'event_id' => $event_id,
                    'session_id' => $session->id
                ]);
            }
            
			if(!empty($session->question)) {
				
				EventFeedback::create([
				'question' => $session->question,
				'answer' => "",
				'type' => 3, #option
				'event_id' => $event_id,
				'session_id' => $session->id
			]);
			}
		}

		# pdf related additional
        EventFeedback::create([
            'question' => "Chairman session",
            'answer' => "1;2;3;4;5;6;7;8;9;10",
            'type' => 1, #rating
            'event_id' => $event_id
        ]);
        
        EventFeedback::create([
            'question' => "Education Officer session",
            'answer' => "1;2;3;4;5;6;7;8;9;10",
            'type' => 1, #rating
            'event_id' => $event_id
        ]);
        
        EventFeedback::create([
            'question' => "Membership Officer session",
            'answer' => "1;2;3;4;5;6;7;8;9;10",
            'type' => 1, #rating
            'event_id' => $event_id
        ]);

        EventFeedback::create([
            'question' => "Chartered Champion session",
            'answer' => "1;2;3;4;5;6;7;8;9;10",
            'type' => 1, #rating
            'event_id' => $event_id
        ]);
        
        EventFeedback::create([
            'question' => "Content",
            'answer' => "1;2;3;4;5;6;7;8;9;10",
            'type' => 1, #rating
            'event_id' => $event_id
        ]);
        
        EventFeedback::create([
            'question' => "Venue",
            'answer' => "1;2;3;4;5;6;7;8;9;10",
            'type' => 1, #rating
            'event_id' => $event_id
        ]);
        
        EventFeedback::create([
            'question' => "Catering",
            'answer' => "1;2;3;4;5;6;7;8;9;10",
            'type' => 1, #rating
            'event_id' => $event_id
        ]);
        
        EventFeedback::create([
            'question' => "Organisation",
            'answer' => "1;2;3;4;5;6;7;8;9;10",
            'type' => 1, #rating
            'event_id' => $event_id
        ]);
        
        EventFeedback::create([
            'question' => "Any other comments:",
            'answer' => "",
            'type' => 5, #text
            'event_id' => $event_id
        ]);
        
        EventFeedback::create([
            'question' => "Are there any topics you would like to see covered at future events? If so, please let us know below:",
            'answer' => "",
            'type' => 5, #text
            'event_id' => $event_id
        ]);
        
        EventFeedback::create([
			'question' => "Do you feel today’s presentations met their stated learning objectives? If no, please explain",
			'answer' => "Yes;No",
			'type' => 6, #rad/bton
			'event_id' => $event_id
		]);	
		EventFeedback::create([
			'question' => "If no, please explain",
			'answer' => "",
			'type' => 5, #text
			'event_id' => $event_id
		]);	
        
		event(new DataWasManipulated('actionCreate', $this->log_desc.$event_id.". Feedback created by generating questions from schedule"));
		
		return redirect()->route('events.edit', $event_id);
	}

    public function reposition(Request $request)
    {
        if($request->has('item')) {
            $i = 0;
            foreach($request->get('item') as $id) {

                $i++;
                $item = EventFeedback::find($id);
                $item->position = $i;
                $item->save();
            }

            return Response::json(array('success' => true));
        } else {
            return Response::json(array('success' => false));
        }
    }

	/**
	 * Generate feedback word file
	 */
	public function generatePDF($event_id)
	{
		$event = Event::find($event_id);
		
		$slots = $event->scheduleTimeSlots()->orderBy('start')->with(['sessions' => function($query){
                $query->orderBy('session_start');
                $query->with('contributors', 'type');
            }])->get();
		
        $quarterly_sponsors = [];
        $sponsors = Sponsor::all();
        
        if($event->eventType->has_sponsor) {
            
            $today = $event->event_date_from;
            $quarterly_sponsors = SponsorSetting::where('date_from', '<=', $today)->where('date_to', '>=', $today)->get();
		}

		/* El cliente pide la siguiente solicitud:
		*  "The PFS team want to make some updates to the feedback form (PDF) but only for the Regional Q2 2018".
		*  Por ende para el evento regional q2 2018 que tiene un id = 37 se quema con el pdf "pdf_form_q22018.blade.php"
		*  Para los demas tipos de eventos se deja el pdf "pdf_form.blade.php"  
		*/ 
		$regional_q2_2018_id = 37;
		if( $event->event_type_id == $regional_q2_2018_id ){
			$pdf = \PDF::loadView('admin.events.feedbacks.pdf_form_q22018', ['event'=>$event, 'slots' => $slots, 'sponsors'=>$sponsors, 'quarterly_sponsors' => $quarterly_sponsors]);
		}else{
			$pdf = \PDF::loadView('admin.events.feedbacks.pdf_form', ['event'=>$event, 'slots' => $slots, 'sponsors'=>$sponsors, 'quarterly_sponsors' => $quarterly_sponsors]);			
		}
        
        return $pdf->download($event->title." feedback form.pdf");
        
		// // INITIALISE WORD DOC AND INSERT HEADER
		
		// $section->addText('Feedback form for '.$event->region->title.' Regional Conference '.date("j F, Y",strtotime($event->event_date_from)), $header, ['align'=>'center']);
// 		
		// # YOUR DETAILS
		// $details = $section->addTable('border');
		// $details->addRow();
		// $details->addCell(9000)->addText('Your details', ['size' => 14, 'bold' => true]);
// 		
		// $details = $section->addTable('border');
		// $details->addRow();
		// $details->addCell(2000)->addText('Name', $header);
		// $details->addCell(7000);
// 		
		// $details->addRow();
		// $details->addCell(2000)->addText('Company', $header);
		// $details->addCell(7000);
// 		
		// $details->addRow();
		// $details->addCell(2000)->addText('Email address', $header);
		// $details->addCell(7000);
// 		
		// $section->addTextBreak();
		// $section->addText('Session feedback', ['bold' => true]);
		// $section->addText('Please rate the following sessions between 1 (poor) and 10 (excellent):', ['italic' => true]);
// 		
		// #FEEDBACK
// 		
		// foreach($event->scheduleSessions()->orderBy('session_start')->has('feedbacks')->with('feedbacks')->get() as $session) {
// 		
			// $fb_table = $section->addTable('border');
			// $fb_table->addRow();
			// $fb_table->addCell(9000)->addText(date("H:i",strtotime($session->session_start))." - ".date("H:i",strtotime($session->session_end))." ".$session->title, $header);
// 			
			// foreach($session->feedbacks()->whereNotNull('session_id')->get() as $fb) {
// 				
				// switch ($fb->type) {
					// case 1:
						// # RATING
						// $fb_table = $section->addTable('border');
						// $fb_table->addRow();
						// $fb_table->addCell(4000)->addText($fb->question, $header, ['align' => 'right']);
						// $options = explode(";", $fb->answer);
						// $width = 5000 / sizeof($options);
// 						
						// foreach($options as $opt) {
// 							
							// $fb_table->addCell($width)->addText($opt, null, ['align' => 'center']);
						// }
// 						
						// break;
					// case 2:
						// $opt_table = $section->addTable('border');
						// $opt_table->addRow()->addCell(9000)->addText($fb->question, $header);
						// $opt_table = $section->addTable('border');
						// $opt_table->addRow();
						// $options = explode(";", $fb->answer);
						// $width = 9000 / sizeof($options);
// 						
						// foreach($options as $opt) {
// 							
							// $opt_table->addCell($width)->addText($opt, null, ['align' => 'center']);
						// }
// 						
						// break;
					// case 3:
						// $opt_table = $section->addTable('border');
						// $opt_table->addRow();
						// $opt_table->addCell(9000)->addText($fb->question, $header);
						// $opt_table = $section->addTable('border');
						// $opt_table->addRow();
						// $options = explode(";", $fb->answer);
						// $width = 9000 / sizeof($options);
// 						
						// foreach($options as $opt) {
// 							
							// $opt_table->addCell($width)->addText($opt, null, ['align' => 'center']);
						// }
// 						
						// break;
					// case 4:
// 						
						// $opt_table = $section->addTable('border');
						// $opt_table->addRow()->addCell(9000)->addText($fb->question, $header);
						// $opt_table = $section->addTable('border');
						// $opt_table->addRow();
						// $options = explode(";", $fb->answer);
						// $width = 9000 / sizeof($options);
// 						
						// foreach($options as $opt) {
// 							
							// $opt_table->addCell($width)->addText($opt, null, ['align' => 'center']);
						// }
// 						
						// break;
					// case 5:
// 						
						// $question_table = $section->addTable('border');
						// $question_table->addRow(1000);
						// $question_table->addCell(9000)->addText($fb->question, $header);
// 						
						// break;
// 						
					// default:
// 						
						// break;
				// }
			// }
		// }
// 
		// # NOT LINKED QUESTION
// 		
		// $event_feedback = EventFeedback::where('event_id', $event->id)->whereNull('session_id')->get();
		// if(sizeof($event_feedback)) $section->addText('Please rate the event in general out of 10', ['bold' => true]);
// 		
		// foreach($event_feedback as $fb) {
// 			
			// switch ($fb->type) {
				// case 1:
					// # RATING
					// $fb_table = $section->addTable('border');
					// $fb_table->addRow();
					// $fb_table->addCell(4000)->addText($fb->question, $header, ['align' => 'right']);
					// $options = explode(";", $fb->answer);
					// $width = 5000 / sizeof($options);
// 					
					// foreach($options as $opt) {
// 						
						// $fb_table->addCell($width)->addText($opt, null, ['align' => 'center']);
					// }
// 					
					// break;
				// case 2:
					// $opt_table = $section->addTable('border');
					// $opt_table->addRow()->addCell(9000)->addText($fb->question, $header);
					// $opt_table = $section->addTable('border');
					// $opt_table->addRow();
					// $options = explode(";", $fb->answer);
					// $width = 9000 / sizeof($options);
// 					
					// foreach($options as $opt) {
// 						
						// $opt_table->addCell($width)->addText($opt, null, ['align' => 'center']);
					// }
// 					
					// break;
				// case 3:
					// $opt_table = $section->addTable('border');
					// $opt_table->addRow();
					// $opt_table->addCell(9000)->addText($fb->question, $header);
					// $opt_table = $section->addTable('border');
					// $opt_table->addRow();
					// $options = explode(";", $fb->answer);
					// $width = 9000 / sizeof($options);
// 					
					// foreach($options as $opt) {
// 						
						// $opt_table->addCell($width)->addText($opt, null, ['align' => 'center']);
					// }
// 					
					// break;
				// case 4:
// 					
					// $opt_table = $section->addTable('border');
					// $opt_table->addRow()->addCell(9000)->addText($fb->question, $header);
					// $opt_table = $section->addTable('border');
					// $opt_table->addRow();
					// $options = explode(";", $fb->answer);
					// $width = 9000 / sizeof($options);
// 					
					// foreach($options as $opt) {
// 						
						// $opt_table->addCell($width)->addText($opt, null, ['align' => 'center']);
					// }
// 					
					// break;
				// case 5:
// 					
					// $question_table = $section->addTable('border');
					// $question_table->addRow(1000);
					// $question_table->addCell(9000)->addText($fb->question, $header);
// 					
					// break;
// 					
				// default:
// 					
					// break;
			// }
		// }
// 		
		// # THANK YOU
		// $thankyou = $section->addTable('border');
		// $thankyou->addRow()->addCell(9000)->addText('Thank you for completing questionnaire. Your feedback will help us to develop these sessions in the future', $header);
// 		
		// // SAVE REPORT AND PUSH DOWNLOAD
		// $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		// $filename = "Feedback form.docx";
		// $objWriter->save('../storage/reports/'.$filename);
		// return redirect(\Config::get('app.root')."storage/reports/".$filename);
	}
}
