<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\EventSetting;
use App\Currency;

class EventSettingController extends Controller
{
	
	private $event_setting;
	
	public function __construct(EventSetting $event_setting) {
		
		$this->event_setting = $event_setting;
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.eventSettings.show')->with('es', $this->event_setting->findOrFail($id));
    }
	
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		return view('admin.eventSettings.edit')->with('event_setting', $this->event_setting->findOrFail($id))->with('currencies', Currency::lists('code', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $event_setting = $this->event_setting->findOrFail($id);
		$event_setting->client_name = $request->get('client_name');
		$event_setting->client_sub_header = $request->get('client_sub_header');
		$event_setting->event_name = $request->get('event_name');
		$event_setting->event_location = $request->get('event_location');
		$event_setting->event_date_from = $request->has('event_date_from') ? date("Y-m-d", strtotime($request->get('event_date_from'))): date("Y-m-d");
		$event_setting->event_date_to = $request->has('event_date_to') ? date("Y-m-d", strtotime($request->get('event_date_to'))): date("Y-m-d");
		$event_setting->schedule_text = $request->get('schedule_text');
		$event_setting->vat = $request->get('vat');
		$event_setting->show_tax = $request->has('show_tax') ? 1 : 0;
		$event_setting->accommodation_external_text = $request->get('accommodation_external_text');
		$event_setting->accommodation_external = $request->has('accommodation_external') ? 1 : 0;
		$event_setting->accommodation_external_link = $request->get('accommodation_external_link');
		$event_setting->terms_conditions = $request->get('terms_conditions');
		$event_setting->currency_id = $request->get('currency_id');
		$event_setting->save();
		
		\Session::flash('success', 'Event Settings was successfully updated!');
		
		return redirect()->route('eventSettings.show', $id);
    }
}
