<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\EventFaq;
use App\Events\DataWasManipulated;

class EventFaqController extends Controller {

	protected $faq;
	public $log_desc = "FAQS ";
	
	public function __construct(EventFaq $faq)
	{
		$this->faq = $faq;
	}
	
	/**
	 * Show the form for creating a faq resource.
	 *
	 * @param int id $event id
	 * @return create faq form
	 */
	public function create($id)
	{
		return view('admin.events.faqs.create')->with('event_id', $id);
	}

	/**
	 * Store a newly created faq in storage.
	 *
	 * @return redirect to edit event faq tab
	 */
	public function store(Requests\CreateFaqRequest $request)
	{
		$this->faq->create([
		    'visibility' => $request->get('visibility'),
			'event_id' => $request->get('event_id'),
			'question' => $request->get('question'),
			'answer' => $request->get('answer'),
			'position' => 1
		]);
		event(new DataWasManipulated('actionCreate', $this->log_desc.'Event ID: '.$request->get('event_id')));
		
		return redirect()->route('events.edit', $request->get('event_id'));
	}

	/**
	 * Show the form for editing the specified faq.
	 *
	 * @param int id $event id
	 * @return show faq edit form
	 */
	public function edit($id)
	{
		return view('admin.events.faqs.edit')->with('faq', $this->faq->find($id))->with('event_id', $id);
	}

	/**
	 * Update the specified faq in storage.
	 *
	 * @param  int  $id
	 * @return redirect to event faq tab
	 */
	public function update(Requests\CreateFaqRequest $request, $id)
	{
		$faq = $this->faq->findOrFail($id);
        $faq->visibility = $request->get('visibility');
		$faq->question = $request->get('question');
		$faq->answer = $request->get('answer');
		$faq->save();
		event(new DataWasManipulated('actionUpdate', 'ID: '.$faq->id));
		
		return redirect()->route('events.edit', $faq->event_id);
	}

	/**
	 * Remove the specified faq from storage.
	 *
	 * @param int id $event id
	 * @param int $faq_id
	 * @return Response
	 */
	public function destroy($id, $faq_id)
	{
		$faq = $this->faq->find($faq_id);
		event(new DataWasManipulated('actionDelete', $this->log_desc.'ID: '.$faq->id));
		$faq->delete();
		
		return redirect()->route('events.edit', $id);
	}
	
	/**
	 * Delete all faqs
	 * @param int $id event id
	 * @return redirect to event faq tab
	 */
	public function deleteAll($id)
	{
		$this->faq->where('event_id', $id)->delete();
		event(new DataWasManipulated('actionDelete', 'All '.$this->log_desc.'Event ID: '.$id));
		
		return redirect()->route('events.edit', $id);
	}
	
	/**
	 * Import faqs from previous event
	 * @param int $id event id
	 * @return redirect to event faq tab
	 */
	public function import(Request $request, $id)
	{
		$old_faq = $this->faq->where('event_id', $request->get('old_event_id'))->get();
		
		if(!empty($old_faq)) {
			
			foreach($old_faq as $ofaq) {
				
				$new_faq = $ofaq->replicate();
				$new_faq->event_id = $id;
				$new_faq->save();
				
				event(new DataWasManipulated('actionCreate', $this->log_desc.'ID: '.$new_faq->id));
			}
		}
		
		return redirect()->route('events.edit', $id);
	}
	
	public function reposition(Request $request)
	{
		if($request->has('item')) {
			$i = 0;
			foreach($request->get('item') as $id) {
				
				$i++;
				$item = $this->faq->find($id);
				$item->position = $i;
				$item->save();
			}
			
			return Response::json(array('success' => true));
		} else {
			return Response::json(array('success' => false));
		}
	}

}
