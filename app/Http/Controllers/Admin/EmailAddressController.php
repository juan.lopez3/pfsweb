<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\EmailAddress;
use App\Events\DataWasManipulated;

class EmailAddressController extends Controller {
	
	private $email_address;
	public $log_desc = "Email Address ";
	
	public function __construct(EmailAddress $email_address) {
		
		$this->email_address = $email_address;
	}
	/**
	 * Display a listing of the region.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('admin.settings.emailAddresses.list')->with('email_addresses', $this->email_address->all());
	}

	/**
	 * Show the form for creating a new email address.
	 *
	 * @return show region create form
	 */
	public function create()
	{
		return view('admin.settings.emailAddresses.create');
	}

	/**
	 * Store a newly created email address in storage.
	 *
	 * @return redirect to region list
	 */
	public function store(Requests\CreateEmailAddressRequest $request)
	{
		$email_address = $this->email_address->create($request->all());
		event(new DataWasManipulated('actionCreate', $this->log_desc.$email_address->title));
		
		return redirect()->route('settings.emailAddresses');
	}

	/**
	 * Show the form for editing the specified region.
	 *
	 * @param  int  $id region id
	 * @return show region edit form
	 */
	public function edit($id)
	{
		return view('admin.settings.emailAddresses.edit')->with('email_address', $this->email_address->find($id));
	}

	/**
	 * Update the specified region in storage.
	 *
	 * @param  int  $id region id
	 * @return redirect to region list
	 */
	public function update(Requests\CreateEmailAddressRequest $request, $id)
	{
		$email_address = $this->email_address->find($id);
		$email_address->fill($request->input())->save();
		event(new DataWasManipulated('actionUpdate', $this->log_desc.$email_address->title));
		
		return redirect()->route('settings.emailAddresses');
	}

	/**
	 * Remove the specified region from storage.
	 *
	 * @param  int  $id region id
	 * @return redirect to region list
	 */
	public function destroy($id)
	{
		$email_address =$this->email_address->find($id); 
		event(new DataWasManipulated('actionDelete', $this->log_desc.$email_address->title));
		$email_address->delete();
		
		return redirect()->route('settings.emailAddresses');
	}

}
