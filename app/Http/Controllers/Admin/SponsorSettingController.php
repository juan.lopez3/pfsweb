<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\EventType;
use App\SponsorSetting;
use App\Sponsor;
use App\Events\DataWasManipulated;

class SponsorSettingController extends Controller {

	protected $event_type;
	protected $sponsor_setting;
	public $log_desc = "Sponsor Setttings ";
	
	public function __construct(EventType $event_type, SponsorSetting $sponsor_setting)
	{
		$this->event_type = $event_type;
		$this->sponsor_setting = $sponsor_setting;
	}
	/**
	 * Display a listing of the sponsor setting list.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('admin.settings.sponsors.list')->with('event_types', $this->event_type->all())
			->with('sponsors', $this->sponsor_setting->all())
			->with('sponsors_select', Sponsor::lists('name', 'id'));
	}

	/**
	 * Store a newly created sponsor setting in storage.
	 *
	 * @return Response
	 */
	public function store(Requests\CreateSponsorQuarterRequest $request)
	{
		$this->sponsor_setting->create([
			'title' => $request->get('title'),
			'date_from' => date("Y-m-d", strtotime(str_replace("/", "-", $request->get('date_from')))),
			'date_to' => date("Y-m-d", strtotime(str_replace("/", "-", $request->get('date_to'))))
		]);
		
		event(new DataWasManipulated('actionCreate', $this->log_desc));
		
		return redirect()->route('settings.sponsors');
	}

	/**
	 * Show the form for editing the specified sponsor.
	 *
	 * @param  int  $id  sponsor setting id
	 * @return Response
	 */
	public function edit($id)
	{
		return view('admin.settings.sponsors.edit')->with('sponsor_settings', $this->sponsor_setting->find($id));
	}

	/**
	 * Update the specified sponsor setting in storage.
	 *
	 * @param  int  $id sponsor setting id
	 * @return Response
	 */
	public function update(Requests\CreateSponsorQuarterRequest $request, $id)
	{
		$sponsor_setting = $this->sponsor_setting->find($id);
		
		$sponsor_setting->title = $request->get('title');
		$sponsor_setting->date_from = date("Y-m-d", strtotime(str_replace("/", "-", $request->get('date_from'))));
		$sponsor_setting->date_to = date("Y-m-d", strtotime(str_replace("/", "-", $request->get('date_to'))));
		$sponsor_setting->save();
		
		event(new DataWasManipulated('actionUpdate', $this->log_desc.$sponsor_setting->title));
		
		return redirect()->route('settings.sponsors');
	}
	
	public function addSponsor(Request $request)
	{
		
		if ($request->ajax()) {
			
			$sponsor_setting = $this->sponsor_setting->find($request->get('sponsor_setting_id'));
			$logo = 'logo_'.$request->get('logo_id');
			$sponsor_setting->$logo = $request->get('sponsor_select_id');
			$sponsor_setting->save();
			
			return 1;
		} else {
			return null;
		}
	}
	
	/**
	 * Remove Sponsor logo
	 * 
	 * @return redirect to sponsor setting list
	 */
	public function destroy(Request $request)
	{
		$sponsor_setting = $this->sponsor_setting->find($request->get('id'));
		$logo = 'logo_'.$request->get('logo_id');
		$sponsor_setting->$logo = "";
		$sponsor_setting->save();
		
		event(new DataWasManipulated('actionDelete', 'Deleted Logo '.$this->log_desc.$logo));
		
		return redirect()->route('settings.sponsors');
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id sponsor setting id
	 * @return Response
	 */
	public function destroyRow($id)
	{
		$sponsor_setting = $this->sponsor_setting->find($id);
		$sponsor_setting->delete();
		
		event(new DataWasManipulated('actionDelete', $this->log_desc.'row'));
		
		return redirect()->route('settings.sponsors');
	}
	
	/**
	 * Service for ajax sponsor type update
	 * 
	 * @return 1/0 if success
	 */
	public function typeUpdate(Request $request)
	{
		
		if ($request->ajax()) {
			
			if($request->get('value') == 'true') {
				$checked = 1;
			} else {
				$checked = 0;
			}
			
			$event_type = $this->event_type->find($request->get('id'));
			$event_type->fill(['has_sponsor' => $checked])->save();
			
			event(new DataWasManipulated('actionUpdate', $this->log_desc.'event type: '.$event_type->title));
			
			return 1;
		}
		
		return null;
	}
}
