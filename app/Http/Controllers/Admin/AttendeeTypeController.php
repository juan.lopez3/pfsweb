<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\AttendeeType;
use App\Events\DataWasManipulated;

class AttendeeTypeController extends Controller {
	
	private $attendee_type;
	public $log_desc = "Attendee type ";
	
	public function __construct(AttendeeType $attendee_type) {
		
		$this->attendee_type = $attendee_type;
	}
	/**
	 * Display a listing of the attendees types.
	 *
	 * @return show attendee type list
	 */
	public function index()
	{
		return view('admin.settings.attendeeTypes.list')->with('attendee_type', $this->attendee_type->all());
	}

	/**
	 * Show the form for creating a new attendees type.
	 *
	 * @return show attendee type create form
	 */
	public function create()
	{
		return view('admin.settings.attendeeTypes.create');
	}

	/**
	 * Store a newly created attendees type in storage.
	 *
	 * @return redirect to attendee type list
	 */
	public function store(Requests\CreateAttendeeTypeRequest $request)
	{
		$attendee_type = $this->attendee_type->create($request->all());
		event(new DataWasManipulated('actionCreate', $this->log_desc.$attendee_type->title));
		
		return redirect()->route('settings.attendeeTypes');
	}

	/**
	 * Show the form for editing the specified attendees type.
	 *
	 * @param  int  $id attendee type id
	 * @return show attendee type edit form
	 */
	public function edit($id)
	{
		return view('admin.settings.attendeeTypes.edit')->with('attendee_type', $this->attendee_type->find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id attendee type id
	 * @return redirect to attendee type list
	 */
	public function update(Requests\CreateTechniqueRequest $request, $id)
	{
		$attendee_type = $this->attendee_type->find($id);
		$attendee_type->fill($request->input())->save();
		event(new DataWasManipulated('actionUpdate', $this->log_desc.$attendee_type->title));
		
		return redirect()->route('settings.attendeeTypes');
	}

	/**
	 * Remove the specified attendees type from storage.
	 *
	 * @param  int  $id attendee type id
	 * @return redirect to attendee type list
	 */
	public function destroy($id)
	{
		$attendee_type =$this->attendee_type->find($id); 
		event(new DataWasManipulated('actionDelete', $this->log_desc.$attendee_type->title));
		$attendee_type->delete();
		
		return redirect()->route('settings.attendeeTypes');
	}

}
