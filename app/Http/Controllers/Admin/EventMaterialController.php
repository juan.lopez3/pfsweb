<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\UserRole;
use App\UserRoleSub;
use App\EventMaterial;
use App\Events\DataWasManipulated;

class EventMaterialController extends Controller {

	protected $event_material;
	public $log_desc = "Event Materials Event ID: ";
	
	public function __construct(EventMaterial $event_material){
		
		$this->event_material = $event_material;
	}


	/**
	 * Store a newly created event material in storage.
	 *
	 * @return saved file name
	 */
	public function store(Request $request)
	{
		$file = $request->file('myfile');
		$event_id = $request->get('event_id');
		
		if($file) {
			
			$saved_file = uploadFile($file, FILE);
			
			$this->event_material->create([
			'event_id' => $event_id,
			'document_name' => $saved_file,
			'display_name' => $file->getClientOriginalName(),
			'type' => $file->getClientOriginalExtension(),
			'size' => $file->getClientSize(),
			'date_live' => date('Y-m-d')]);
			
			event(new DataWasManipulated('actionCreate', $this->log_desc.$event_id.' Material: '.$saved_file));
			
			return response()->json($saved_file);
		}
		
		return null;
	}

	/**
	 * Show the form for editing the specified material storage.
	 *
	 * @param  int  $id
	 * @return show event material edit form
	 */
	public function edit($id)
	{
		return view('admin.events.materials.edit')->with('event_material', $this->event_material->find($id))
			->with('roles', UserRole::lists('title', 'id'))
			->with('sub_roles', UserRoleSub::lists('title', 'id'));
	}

	/**
	 * Update the specified event material in storage.
	 *
	 * @param  int  $id event id
	 * @return redirect to event event material tab 
	 */
	public function update(Requests\UpdateEventMaterialRequest $request, $id)
	{
		$material = $this->event_material->find($id);
		$material->display_name = $request->get('display_name');
		$material->type = $request->get('type');
		$material->size = $request->get('size');
		$material->date_live = date("Y-m-d", strtotime(str_replace("/", "-", $request->get('date_live'))));
		$material->role_visibility = $request->has('role_visibility') ? implode(",", $request->get('role_visibility')) : '';
		$material->subrole_visibility = $request->has('subrole_visibility') ? implode(",", $request->get('subrole_visibility')) : '';
		$material->save();
		event(new DataWasManipulated('actionUpdate', $this->log_desc.'ID: '.$request->get('event_id').'Material: '.$request->get('display_name')));
		
		return redirect()->route('events.edit', $material->event_id);
	}

	/**
	 * Remove the specified event material from storage.
	 *
	 * @param  int  $id event id
	 * @param in $material_id
	 * @return redirect to event event material tab
	 */
	public function destroy($id, $material_id)
	{
		$material = $this->event_material->find($material_id);
		removeFile($material->document_name, FILE);
		$this->event_material->destroy($material_id);
		event(new DataWasManipulated('actionDelete', $this->log_desc.'ID: '.$id.' Material: '.$material->document_name));
		
		return redirect()->route('events.edit', $id);
	}
	
	/**
	 * Remove event materials from DB and server
	 *
	 * @param  int  $id event_id
	 * @return redirect to event event material tab
	 */	
	public function deleteAll($id)
	{
		$materials = $this->event_material->where('event_id', $id)->get();
		
		foreach ($materials as $material) {
			
			removeFile($material->document_name, FILE);
			$this->event_material->destroy($material->id);
			event(new DataWasManipulated('actionDelete', $this->log_desc.'ID: '.$id.' Material: '.$material->document_name));
		}
		
		return redirect()->route('events.edit', $id);
	}

}
