<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\SessionType;
use App\Events\DataWasManipulated;

class SessionTypeController extends Controller {
	
	private $session_type;
	public $log_desc = "Session Type ";
	
	public function __construct(SessionType $session_type) {
		
		$this->session_type = $session_type;
	}
	/**
	 * Display a listing of the sessions.
	 *
	 * @return list of sessions
	 */
	public function index()
	{
		return view('admin.settings.sessionTypes.list')->with('session_types', $this->session_type->all());
	}

	/**
	 * Show the form for creating a new session.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('admin.settings.sessionTypes.create');
	}

	/**
	 * Store a newly created session in storage.
	 *
	 * @return Response
	 */
	public function store(Requests\CreateSessionTypeRequest $request)
	{
		$session_type = $this->session_type->create($request->all());
		event(new DataWasManipulated('actionCreate', $this->log_desc.$session_type->title));
		
		return redirect()->route('settings.sessionTypes');
	}

	/**
	 * Show the form for editing the specified session.
	 *
	 * @param  int  $id  session type id
	 * @return Response
	 */
	public function edit($id)
	{
		return view('admin.settings.sessionTypes.edit')->with('session_type', $this->session_type->find($id));
	}

	/**
	 * Update the specified session in storage.
	 *
	 * @param  int  $id  session type id
	 * @return Response
	 */
	public function update(Requests\CreateSessionTypeRequest $request, $id)
	{
		$session_type = $this->session_type->find($id);
        $session_type->title = $request->get('title');
        $session_type->description = $request->get('description');
        $session_type->color = $request->get('color');
        $session_type->include_in_cpd = ($request->has('include_in_cpd')) ? 1 : 0;
        $session_type->include_in_feedback = ($request->has('include_in_feedback')) ? 1 : 0;
        $session_type->save();
		event(new DataWasManipulated('actionUpdate', $this->log_desc.$session_type->title));

		return redirect()->route('settings.sessionTypes');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id  session type id
	 * @return Response
	 */
	public function destroy($id)
	{
		$session_type = $this->session_type->find($id);
		event(new DataWasManipulated('actionDelete', $this->log_desc.$session_type->title));
		$session_type->delete();
		
		return redirect()->route('settings.sessionTypes');
	}

}
