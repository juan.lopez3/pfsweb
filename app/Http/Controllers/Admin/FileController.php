<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\File;
use App\Events\DataWasManipulated;

class FileController extends Controller {
	
	private $file;
	public $log_desc = "File ";
	
	public function __construct(File $file) {
		
		$this->file = $file;
	}
	/**
	 * Display a listing of the file.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('admin.files.list')->with('files', $this->file->all());
	}

	/**
	 * Show the form for creating a new region.
	 *
	 * @return show file create form
	 */
	public function create()
	{
		return view('admin.files.create');
	}

	/**
	 * Store a newly created file in storage.
	 *
	 * @return redirect to region list
	 */
	public function store(Request $request)
	{
		$file = $request->file('myfile');
		
		if($file) {
			
			$saved_file = uploadFile($file, FILE);
			
			$this->file->create([
			'document_name' => $saved_file,
			'display_name' => $file->getClientOriginalName(),
			'type' => $file->getClientOriginalExtension(),
			'size' => $file->getClientSize()]);
			
			event(new DataWasManipulated('actionCreate', $this->log_desc.$saved_file));
			
			return response()->json($saved_file);
		}
		
		return null;
	}
	
	public function updateFile(Request $request, $id)
	{
		$file_upload = $request->file('myfile');
		
		if($file_upload) {
			
			$file = $this->file->findOrFail($id);
			
			//remove old file
			removeFile($file->document_name, FILE);
			$saved_file = uploadFile($file_upload, FILE);
			//$file->document_name = $saved_file;
			//don't change document name as the link must be the same if the file is updated
			rename(FILE.'/'.$saved_file, FILE.'/'.$file->document_name);
			$file->display_name = $file_upload->getClientOriginalName();
			$file->type = $file_upload->getClientOriginalExtension();
			$file->size = $file_upload->getClientSize();
			$file->save();
			
			event(new DataWasManipulated('actionCreate', $this->log_desc.$saved_file));
			
			return response()->json($saved_file);
		}
		
		return null;
	}

	/**
	 * Show the form for editing the specified region.
	 *
	 * @param  int  $id region id
	 * @return show region edit form
	 */
	public function edit($id)
	{
		return view('admin.files.edit')->with('file', $this->file->find($id));
	}

	/**
	 * Update the specified region in storage.
	 *
	 * @param  int  $id region id
	 * @return redirect to region list
	 */
	public function update(Request $request, $id)
	{
		$file = $this->file->find($id);
		$file->display_name = $request->get('display_name');
		$file->type = $request->get('type');
		$file->size = $request->get('size');
		$file->save();
		
		event(new DataWasManipulated('actionUpdate', $this->log_desc.$request->get('display_name')));
		
		return redirect()->route('files');
	}

	/**
	 * Remove the specified region from storage.
	 *
	 * @param  int  $id file id
	 * @return redirect to files list
	 */
	public function destroy($id)
	{
		$file = $this->file->findOrFail($id);
		removeFile($file->document_name, FILE);
		$this->file->destroy($id);
		event(new DataWasManipulated('actionDelete', $this->log_desc.$file->document_name));
		
		return redirect()->route('files');
	}
    
    /**
     * return list of central files
     */
    public function filesList(Request $request)
    {
        if ($request->ajax()) {
            
            $search = $request->get('term');
            $files = $this->file->where('document_name','like', '%'.$search.'%')->orWhere('display_name','like', '%'.$search.'%')->select('document_name', 'display_name')->get();
            
            return $files->toJson();
        } else {
            return null;
        }
    }

}
