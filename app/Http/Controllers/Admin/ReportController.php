<?php namespace App\Http\Controllers\Admin;

use App\AttendeeType;
use App\ClickedLinksLog;
use App\Event;
use App\EventAttendee;
use App\EventType;
use App\Http\Controllers\Controller;
use App\Payment;
use App\RegistrationAnswer;
use App\RegistrationStatus;
use App\Report;
use App\SavedReport;
use App\SentEmail;
use App\UserFeedback;
use App\UserRoleSub;
use DB;
use Illuminate\Http\Request;

// include report models

class ReportController extends Controller
{

    public $event;
    public $report;

    public function __construct(Event $event, Report $report)
    {
        $this->event = $event;
        $this->report = $report;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.reports.list')->with('reports', $this->report->with('user')->get());
    }

    /**
     * Show custom report form
     */
    public function custom()
    {
        $config = \Config::get('reports.Models');

        return view('admin.reports.custom')->with('models', $config)->with('saved_report', array());
    }

    /**
     * Shows list of reports which were selected to be visible for 3rd party
     */
    public function client()
    {
        return view('admin.reports.client')->with('client_reports', SavedReport::where('client_report', 1)->get());
    }

    /**
     * Regenerate custom report
     */
    public function customRegenerate(Request $request)
    {

        $saved_report = SavedReport::findOrFail($request->get('saved_report_id'));
        $saved_report = json_decode($saved_report->data);

        $config = \Config::get('reports.Models');

        return view('admin.reports.custom')->with('models', $config)->with('saved_report', $saved_report);
    }

    /**
     * remove saved report
     */
    public function destroy($id)
    {
        SavedReport::destroy($id);
        return redirect()->route('reports.client');
    }

    /**
     * AJAX service for custom report to get select input with value
     *
     * @param model string model name
     * @param title_field string select name
     * @param id_field string select value
     */
    public function getConditionSelect(Request $request)
    {
        $model = "App\\" . $request->get('model');
        $model_instance = new $model();

        $list = $model_instance->orderBy($request->get('title_field'))->lists($request->get('title_field'), $request->get('id_field'));

        $input = '<select name="' . $request->get('select_name') . '" class="form-control">';

        foreach ($list as $key => $value) {
            $input .= '<option value="' . $key . '">' . $value . '</option>';
        }

        $input .= '</select>';

        return $input;
    }

    /**
     * report type used for client reports. Different request is sent
     */
    public function generateCustomReport(Request $request, $report_type = 0)
    {
        if ($request->has('client_report_id')) {
            $sr = SavedReport::find($request->get('client_report_id'));
            $data = json_decode($sr->data, true);
            \Input::merge($data);
        }

        $config = \Config::get('reports.Models');
        $fields = $request->get('report_fields');
        $report_title = $request->get('report_title');
        $columns = array();
        $data = array();
        $selected_models = array();
        $selected_fields = array();
        $selected_conditions = array();
        $selected_order = array();
        $row = 0;

        // save request if save ch/box
        if ($request->has('report_save') && !$request->has('client_report_id')) {

            $sr = new SavedReport;
            $sr->name = $request->get('report_title');
            $sr->type = $request->get('report_type');
            $sr->data = json_encode($fields);
            $sr->save();
        }

        // save as client report. saves the whole query
        if ($request->has('client_report') && !$request->has('client_report_id')) {
            $sr = new SavedReport;
            $sr->name = $request->get('report_title');
            $sr->type = $request->get('report_type');
            $sr->data = json_encode($request->input());
            $sr->client_report = 1;
            $sr->save();
        }

        //collect and group all data such as order, labels, conditions and group to arrays, make easier to work. Collect labels
        foreach ($fields as $f) {

            if ($f == "") {continue;
            }

            // check if user entered custom label if not check in config if not use label from database
            $model_field = explode(".", $f);
            if (!empty($request->get('custom_label_' . $model_field[0] . '_' . $model_field[1]))) {
                $column = $request->get('custom_label_' . $model_field[0] . '_' . $model_field[1]);
            } else {
                $column = (isset($config[$model_field[0]]['FieldLabels'][$model_field[1]])) ? $config[$model_field[0]]['FieldLabels'][$model_field[1]] : $model_field[1];
            }

            array_push($columns, $column);
            array_push($selected_fields, $f);

            $temp_field = $model_field[0] . '_' . $model_field[1];

            if (!empty($request->get('order_' . $temp_field))) {
                $selected_order[$model_field[0]][$model_field[1]] = $request->get('order_' . $temp_field);
            }

            if (!empty($request->get('condition_' . $temp_field))) {

                $selected_conditions[$model_field[0]][$model_field[1]] = array(
                    'operator' => $request->get('operator_condition_' . $temp_field),
                    'value' => $request->get('condition_' . $temp_field),
                );
            }

            if (!in_array($model_field[0], $selected_models)) {array_push($selected_models, $model_field[0]);
            }
        }

        // make queries and form data array

        if (empty($selected_models)) {return redirect()->route('reports.custom');
        }

        $main_model = "App\\" . $selected_models[0];
        $main_model = new $main_model();

        //conditions
        if (isset($selected_conditions[$selected_models[0]]) && !empty($selected_conditions[$selected_models[0]])) {

            foreach ($selected_conditions[$selected_models[0]] as $condition_field => $condition) {

                $value = ($condition['operator'] == 'LIKE') ? "%" . $condition['value'] . "%" : $condition['value'];

                if ($request->get('condition') == "AND") {
                    $main_model = $main_model->where($condition_field, $condition['operator'], $value);
                } else {
                    //or
                    $main_model = $main_model->orWhere($condition_field, $condition['operator'], $value);
                }
            }
        }

        // ordering
        if (isset($selected_order[$selected_models[0]]) && !empty($selected_order[$selected_models[0]])) {

            foreach ($selected_order[$selected_models[0]] as $order_field => $order) {
                $main_model = $main_model->orderBy($order_field, $order);
            }
        }

        // attach all related models so no more queries sent to DB
        for ($i = 1; $i < sizeof($selected_models); $i++) {

            // temporarily commented out as sometimes model is overlayed $main_model = $main_model->with($selected_models[$i]);

            // conditions on related models
            //conditions
            if (isset($selected_conditions[$selected_models[$i]]) && !empty($selected_conditions[$selected_models[$i]])) {

                foreach ($selected_conditions[$selected_models[$i]] as $condition_field => $condition) {

                    $value = ($condition['operator'] == 'LIKE') ? "%" . $condition['value'] . "%" : $condition['value'];

                    if ($request->get('condition') == "AND") {

                        $main_model->whereHas(
                            $selected_models[$i], function ($query) use ($condition, $condition_field, $value) {
                                $query->where($condition_field, $condition['operator'], $value);
                            }
                        );
                    } else {
                        //or
                        $main_model->orWhereHas(
                            $selected_models[$i], function ($query) use ($condition, $condition_field, $value) {
                                $query->where($condition_field, $condition['operator'], $value);
                            }
                        );
                    }
                }
            }
        }

        $main_model = $main_model->get();
        $valor = null;
        // FORM MAIN DATA ARRAY
        foreach ($main_model as $main_data) {

            foreach ($selected_fields as $selected_field) {

                $model_field = explode(".", $selected_field);

                // for main model fields
                if ($model_field[0] == $selected_models[0]) {

                    // if column is foreign key take foreign key value. Else just take field from collection model
                    if (isset($config[$selected_models[0]]['ForeignKeys'][$model_field[1]])) {

                        $fk = $config[$selected_models[0]]['ForeignKeys'][$model_field[1]];
                        $valor = !empty($main_data->{$fk[0]}->{$fk[1]}) ? $main_data->{$fk[0]}->{$fk[1]} : "";
                    } else {
                        $field_name = $model_field[1];
                        $valor = $main_data->$field_name;
                    }
                    $data[$row][] = $valor;

                } else {

                    // for all related fields
                    $method = $config[$selected_models[0]]['AssocciatedModels'][$model_field[0]];

                    // if related collection exists
                    if (sizeof($main_data->$method)) {

                        if (isset($config[$selected_models[0]]['ForeignKeys'][$model_field[1]])) {
                            $fk = $config[$selected_models[0]]['ForeignKeys'][$model_field[1]];
                            $data[$row][] = !empty($main_data->$method->$fk[0]->$fk[1]) ? $main_data->$method->$fk[0]->$fk[1] : "";
                        } else {
                            $data[$row][] = $main_data->$method->$model_field[1];
                        }
                    } else {
                        $data[$row][] = "";
                    }
                }
            }

            $row++;
        }

        // show report
        switch ($request->get('report_type')) {
            case 'XLSX':

                Report::create(
                    [
                        'report_type' => 'Custom Report',
                        'filename' => $request->get('report_title') . '.xls',
                        'user_id' => \Auth::user()->id,
                    ]
                );

                \Excel::create(
                    $request->get('report_title'), function ($excel) use ($report_title, $columns, $data) {

                        $excel->sheet(
                            $report_title, function ($sheet) use ($report_title, $columns, $data) {

                                $sheet->setStyle(
                                    array(
                                        'font' => ['size' => 10, 'name' => 'Arial'],
                                    )
                                );
                                $sheet->setOrientation('landscape');
                                $sheet->setAutosize(false);

                                $sheet->loadView('admin.reports.tabularReport')->with('columns', $columns)
                                    ->with('data', $data)
                                    ->with('report_title', $report_title);
                            }
                        )->save('xls')->download('xls');

                    }
                );
                break;
            case 'PDF':

                Report::create(
                    [
                        'report_type' => 'Custom Report',
                        'filename' => $request->get('report_title') . '.pdf',
                        'user_id' => \Auth::user()->id,
                    ]
                );

                \Excel::create(
                    $request->get('report_title'), function ($excel) use ($report_title, $columns, $data) {

                        $excel->sheet(
                            $report_title, function ($sheet) use ($report_title, $columns, $data) {

                                $sheet->setStyle(
                                    array(
                                        'font' => ['size' => 10, 'name' => 'Arial'],
                                    )
                                );
                                $sheet->setOrientation('landscape');
                                $sheet->setAutosize(false);

                                $sheet->loadView('admin.reports.tabularReport')->with('columns', $columns)
                                    ->with('data', $data)
                                    ->with('report_title', $report_title);
                            }
                        )->save('pdf')->download('pdf');
                    }
                );

                break;
            default:
                //HTML
                return view('admin.reports.tabularReport')->with('columns', $columns)
                    ->with('data', $data)
                    ->with('report_title', $report_title);

                break;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.reports.create')->with('saved_reports', SavedReport::where('client_report', 0)->lists('name', 'id')); // client reports are saved with a full query, pull only not client reports
    }

    /**
     * Show standard report generation view
     */
    public function standard()
    {
        return view('admin.reports.standard')->with('events', $this->event->orderBy('title')->lists('title', 'id'))
            ->with('event_types', EventType::orderBy('title')->lists('title', 'id'));
        // ->with('campaigns', SentEmail::whereNotNull('campaign')->distinct()->lists('campaign', 'campaign'));
    }

    /**
     * Generate a downloadable document in XLS of events per year divided into quarters
     *
     *
     *  It is a function that once received the parameter $request from this generates the dates of the year divided into
     *  quarters and these are passed to the next function where it generates the sql queries where the array returns.
     *  Is taken to generate the XLS type document
     *
     * @param request $request
     */

    public function eventSlotByRegionByQuarters(Request $request)
    {

        // takes the year of the parameter $request
        $year = $request["year"];

        //generate the dates (Quarters)
        $limits_date = [
            [
                "from" => $year . '-01-01',
                "to" => $year . '-03-31',
            ],
            [
                "from" => $year . '-04-01',
                "to" => $year . '-06-30',
            ],
            [
                "from" => $year . '-07-01',
                "to" => $year . '-09-30',
            ],
            [
                "from" => $year . '-10-01',
                "to" => $year . '-12-31',
            ],
        ];

        // performs the following function from the foreach with the previous array as a limit
        // passing the corresponding parameters
        foreach ($limits_date as $index => $date) {
            $from = $date["from"];
            $to = $date["to"];
            $data[$index] = self::eventSlotByRegion($year, $from, $to, $request);
        }

        // get the returned array ($data[]) and create the XLS document
        \Excel::create(
            'Booking Summary by region year ' . $year, function ($excel) use ($data) {
                foreach ($data as $d) {
                    $excel->sheet(
                        'Quarters', function ($sheet) use ($d) {

                            $sheet->setOrientation('landscape');
                            $sheet->fromArray($d, null, 'A1', false, false);
                        }
                    );
                }
            }
        )->download('xls');

    }

    public function eventSlotByRegion($year, $from, $to, $request)
    {
        if ($request->has('client_report_id')) {
            $sr = SavedReport::find($request->get('client_report_id'));
            $input = json_decode($sr->data, true);
            \Input::merge($input);
            $request->offsetUnset('save_pfs_report');
        }

        $events = $this->event
            ->where('event_date_from', '>=', $from)
            ->where('event_date_from', '<=', $to)
            ->where('publish', 1);

        if (!empty($request->get('event_type_id')) && !empty($request->get('event_type_id')[0])) {
            $events = $events->whereIn('event_type_id', $request->get('event_type_id'));
        }

        $events = $events->with('scheduleTimeSlots.attendees')
            ->with('region')
            ->orderBy('event_date_from')
            ->get();

        $slots_count = 0;

        foreach ($events as $event) {
            if ($event->scheduleTimeSlots()->where('bookable', 1)->count() > $slots_count) {
                $slots_count = $event->scheduleTimeSlots->count();
            }
        }

        $columns = array('region_id', 'Region', 'Events');

        array_push($columns, 'Member  Attendees');
        array_push($columns, 'Non-Member  Attendees');
        array_push($columns, 'Other  Attendees');
        array_push($columns, 'Canceled Bookings');
        array_push($columns, 'invited Bookings');
        array_push($columns, 'waitlisted Bookings');
        array_push($columns, 'late_cancelled Bookings');
        array_push($columns, 'declined Bookings');
        array_push($columns, 'Total Bookings');

        $data[] = $columns;

        foreach ($events as $event) {

            $event_data = array();
            $event_data = array(
                $event->region_id,
                $event->title,
                0,
            );

            //attendees by timeslot inside event
            /*foreach($event->scheduleTimeSlots()->where('bookable', 1)->get() as $slot) {
            $slot_attendees = $slot->attendees()->where('registration_status_id', config('registrationstatus.booked'))->count();
            $val = $slot->title.' ('.$slot_attendees.' of '.$slot->slot_capacity.')';
            array_push($event_data, $val);
            array_push($event_data, (string)$slot_attendees);
            }

            for ($i=0; $i < $slots_count - $event->scheduleTimeSlots()->where('bookable', 1)->count(); $i++) {
            array_push($event_data, '');
            array_push($event_data, '');
            }*/

            //total Member attendees
            array_push($event_data, (string) $event->atendees()
                    ->where('registration_status_id', config('registrationstatus.booked'))
                    ->where('role_id', config('roles.member'))
                    ->count());

            //total NonMember attendees
            array_push($event_data, (string) $event->atendees()
                    ->where('registration_status_id', config('registrationstatus.booked'))
                    ->where('role_id', config('roles.non_member'))
                    ->count());

            //total Other attendees
            array_push($event_data, (string) $event->atendees()
                    ->where('registration_status_id', config('registrationstatus.booked'))
                    ->whereNotIn('role_id', [config('roles.non_member'), config('roles.member')])
                    ->count());

            //canceled attendees
            array_push($event_data, (string) $event->atendees()->where('registration_status_id', config('registrationstatus.cancelled'))->count());
            array_push($event_data, (string) $event->atendees()->where('registration_status_id', config('registrationstatus.invited'))->count());
            array_push($event_data, (string) $event->atendees()->where('registration_status_id', config('registrationstatus.waitlisted'))->count());
            array_push($event_data, (string) $event->atendees()->where('registration_status_id', config('registrationstatus.late_cancelled'))->count());
            array_push($event_data, (string) $event->atendees()->where('registration_status_id', config('registrationstatus.declined'))->count());

            //total bookings
            array_push($event_data, (string) $event->atendees()->count());

            $data[] = $event_data;
        }

        if ($request->has('save_pfs_report')) {
            $saved_report = new SavedReport();
            $saved_report->name = $request->get('saved_report_title');
            $saved_report->type = 'eventSlot';
            $saved_report->client_report = 1;
            $saved_report->data = json_encode($request->input());
            $saved_report->save();
        }

        $grouped = [];
        foreach ($data as $event) {

            if (!isset($grouped[$event[0]])) {

                $grouped[$event[0]] = $event;
                $grouped[$event[0]][2]++;
                //$grouped[$event[0]][0] = 0;
                /* foreach($event as $key=>$value){
            if ($key == 0) continue;

            if (is_numeric($value)){
            $grouped[$event[0]][$key] = $value;
            }
            }*/
            } else {
                $grouped[$event[0]][2]++;
                foreach ($event as $key => $value) {
                    if ($key == 0) {
                        continue;
                    }

                    if (is_numeric($value)) {
                        $grouped[$event[0]][$key] += $value;
                    }
                }
            }
        }
        //echo "<pre>";
        //var_dump($grouped);die;
        $data = $grouped;

        return $data;

    }

    public function eventSlot(Request $request)
    {
        if ($request->has('client_report_id')) {
            $sr = SavedReport::find($request->get('client_report_id'));
            $input = json_decode($sr->data, true);
            \Input::merge($input);
            $request->offsetUnset('save_pfs_report');
        }

        $from = ($request->has('from')) ? date("Y-m-d", strtotime(str_replace("/", "-", $request->get('from')))) : date("Y-m-d");
        $to = ($request->has('to')) ? date("Y-m-d", strtotime(str_replace("/", "-", $request->get('to')))) : date("Y-m-d");

        $columns = array('Event', 'Type', 'Quarter', 'Date');

        $events = $this->event
            ->where('event_date_from', '>=', $from)
            ->where('event_date_from', '<=', $to)
            ->where('publish', 1);

        if (!empty($request->get('event_type_id')) && !empty($request->get('event_type_id')[0])) {
            $events = $events->whereIn('event_type_id', $request->get('event_type_id'));
        }

        $events = $events->with('scheduleTimeSlots.attendees')
            ->orderBy('event_date_from')
            ->get();

        $slots_count = 0;

        foreach ($events as $event) {
            if ($event->scheduleTimeSlots()->where('bookable', 1)->count() > $slots_count) {$slots_count = $event->scheduleTimeSlots->count();
            }
        }

        for ($i = 0; $i < $slots_count; $i++) {
            array_push($columns, 'Session');
            array_push($columns, 'SessionAttendees');
        }

        array_push($columns, 'Member  Attendees');
        array_push($columns, 'Non-Member  Attendees');
        array_push($columns, 'Other  Attendees');
        array_push($columns, 'Canceled Bookings');
        array_push($columns, 'invited Bookings');
        array_push($columns, 'waitlisted Bookings');
        array_push($columns, 'late_cancelled Bookings');
        array_push($columns, 'declined Bookings');
        array_push($columns, 'Total Bookings');

        $data[] = $columns;

        foreach ($events as $event) {

            $event_data = array();
            $event_data = array(
                $event->title,
                $event->eventType->title,
                $this->calculateQuarter($event->event_date_from),
                date("d/m/Y", strtotime($event->event_date_from)),
            );

            //attendees by timeslot inside event
            foreach ($event->scheduleTimeSlots()->where('bookable', 1)->get() as $slot) {
                $slot_attendees = $slot->attendees()->where('registration_status_id', config('registrationstatus.booked'))->count();
                $val = $slot->title . ' (' . $slot_attendees . ' of ' . $slot->slot_capacity . ')';
                array_push($event_data, $val);
                array_push($event_data, (string) $slot_attendees);
            }

            for ($i = 0; $i < $slots_count - $event->scheduleTimeSlots()->where('bookable', 1)->count(); $i++) {
                array_push($event_data, '');
                array_push($event_data, '');
            }

            //total Member attendees
            array_push($event_data, (string) $event->atendees()
                    ->where('registration_status_id', config('registrationstatus.booked'))
                    ->where('role_id', config('roles.member'))
                    ->count());

            //total NonMember attendees
            array_push($event_data, (string) $event->atendees()
                    ->where('registration_status_id', config('registrationstatus.booked'))
                    ->where('role_id', config('roles.non_member'))
                    ->count());

            //total Other attendees
            array_push($event_data, (string) $event->atendees()
                    ->where('registration_status_id', config('registrationstatus.booked'))
                    ->whereNotIn('role_id', [config('roles.non_member'), config('roles.member')])
                    ->count());

            //canceled attendees
            array_push($event_data, (string) $event->atendees()->where('registration_status_id', config('registrationstatus.cancelled'))->count());
            array_push($event_data, (string) $event->atendees()->where('registration_status_id', config('registrationstatus.invited'))->count());
            array_push($event_data, (string) $event->atendees()->where('registration_status_id', config('registrationstatus.waitlisted'))->count());
            array_push($event_data, (string) $event->atendees()->where('registration_status_id', config('registrationstatus.late_cancelled'))->count());
            array_push($event_data, (string) $event->atendees()->where('registration_status_id', config('registrationstatus.declined'))->count());
            //total bookings
            array_push($event_data, (string) $event->atendees()->count());

            $data[] = $event_data;
        }

        if ($request->has('save_pfs_report')) {
            $saved_report = new SavedReport();
            $saved_report->name = $request->get('saved_report_title');
            $saved_report->type = 'eventSlot';
            $saved_report->client_report = 1;
            $saved_report->data = json_encode($request->input());
            $saved_report->save();
        }

        \Excel::create(
            'Booking Summary', function ($excel) use ($data) {

                $excel->sheet(
                    'Bookings', function ($sheet) use ($data) {

                        $sheet->setOrientation('landscape');
                        $sheet->fromArray($data, null, 'A1', false, false);
                    }
                );
            }
        )->download('xls');
    }

    /**
     * Regional bookings for quarter
     */
    public function regionalBookings(Request $request)
    {
        if ($request->has('client_report_id')) {
            $sr = SavedReport::find($request->get('client_report_id'));
            $input = json_decode($sr->data, true);
            \Input::merge($input);
            $request->offsetUnset('save_pfs_report');
        }

        $year = $request->get('year');
        $quater = $request->get('quater');

        $columns = array(
            'Event',
            'Quarter',
            'Date',
            'Session bookings',
        );

        $data[] = $columns;

        $from_month = 1 + ($quater - 1) * 3;
        $to_monnth = 3 + ($quater - 1) * 3;

        $from = date("Y-m-01", strtotime($year . '-' . $from_month));
        $to = date("Y-m-t", strtotime($year . '-' . $to_monnth));

        $events = $this->event
            ->where('event_date_from', '>=', $from)
            ->where('event_date_from', '<=', $to)
            ->where('publish', 1);

        if (!empty($request->get('event_type_id')) && !empty($request->get('event_type_id')[0])) {$events = $events->whereIn('event_type_id', $request->get('event_type_id'));
        }

        $events = $events->with('scheduleTimeSlots.attendees')
            ->orderBy('event_date_from')
            ->get();

        foreach ($events as $event) {

            $session_bookings = 0;

            $slot = $event->scheduleTimeSlots()->where('bookable', 1)->first();
            if (sizeof($slot)) {$session_bookings = $slot->attendees()->where('registration_status_id', \Config::get('registrationstatus.booked'))->count();
            }

            $data[] = array(
                $event->title,
                $quater,
                date("d/m/Y", strtotime($event->event_date_from)),
                (string) $session_bookings,
            );
        }

        if ($request->has('save_pfs_report')) {
            $saved_report = new SavedReport();
            $saved_report->name = $request->get('saved_report_title');
            $saved_report->type = 'regionalBookings';
            $saved_report->client_report = 1;
            $saved_report->data = json_encode($request->input());
            $saved_report->save();
        }

        \Excel::create(
            'Regional Booking', function ($excel) use ($data) {

                $excel->sheet(
                    'Bookings', function ($sheet) use ($data) {

                        $sheet->setOrientation('landscape');
                        $sheet->fromArray($data, null, 'A1', false, false);
                    }
                );
            }
        )->download('xls');
    }

    /**
     * Quarterly bookings
     */
    public function quaterBookings(Request $request)
    {
        if ($request->has('client_report_id')) {
            $sr = SavedReport::find($request->get('client_report_id'));
            $input = json_decode($sr->data, true);
            \Input::merge($input);
            $request->offsetUnset('save_pfs_report');
        }

        $columns = array(
            'Event',
            'Date',
            'Min guar. in venue',
            'Total Booked (inc cxl)',
            'Expected',
            'Actual attended',
            '% Attendance',
            'Manual',
            'Manual %',
            'On-site Bookings',
            'On-site %',
            'Cancelled',
            'Cancelled %',
            'No show',
            'No Show %',
            'Mobile APP',
            'Mobile APP %',
            'WEB',
            'WEB %',
        );

        $total_venue = 0;
        $total_booked = 0;
        $total_expected = 0;
        $total_attended = 0;
        $total_manual = 0;
        $tatal_onsite = 0;
        $total_cancelled = 0;
        $total_noshow = 0;

        //$title = ($request->has('event_type_id')) ? EventType::find($request->get('event_type_id'))->title : "";

        $data[] = array('', '', '', '', 'Booking Statistics');
        $data[] = $columns;

        $from = ($request->has('from')) ? date("Y-m-d", strtotime(str_replace("/", "-", $request->get('from')))) : date("Y-m-d");
        $to = ($request->has('to')) ? date("Y-m-d", strtotime(str_replace("/", "-", $request->get('to')))) : date("Y-m-d");

        $columns = array('Event', 'Quarter', 'Date');

        $from_month = date("n", strtotime($from));
        $to_monnth = date("n", strtotime($to));
        $year = date("Y", strtotime($from));

        for ($i = $from_month; $i <= $to_monnth; $i++) {

            $date = $year . "-" . $i;

            $data[] = array(date("F", strtotime($date)));

            $from = date("Y-m-01", strtotime($date));
            $to = date("Y-m-t", strtotime($date));

            $events = $this->event
                ->where('event_date_from', '>=', $from)
                ->where('event_date_from', '<=', $to)
                ->where('publish', 1);

            if (!empty($request->get('event_type_id')) && !empty($request->get('event_type_id')[0])) {$events = $events->whereIn('event_type_id', $request->get('event_type_id'));
            }

            $events = $events
                ->with('atendees', 'scheduleTimeSlots.attendees')
                ->orderBy('event_date_from')
                ->get();

            foreach ($events as $event) {

                $expected = $event->atendees()->whereNotIn('role_id', [config('roles.host'), config('roles.uploaded_attendee')])->where('registration_status_id', \Config::get('registrationstatus.booked'))->count();
                $attendees = $event->atendees()->whereNotIn('role_id', [config('roles.host'), config('roles.uploaded_attendee')])->count();
                $attended = $event->atendees()->whereNotIn('role_id', [config('roles.host'), config('roles.uploaded_attendee')])->where('registration_status_id', \Config::get('registrationstatus.booked'))->whereNotNull('checkin_time')->count();
                $onsite = $event->atendees()->whereNotIn('role_id', [config('roles.host'), config('roles.uploaded_attendee')])->where('onsite_booking', 1)->count();
                $cancelled = $event->atendees()->whereNotIn('role_id', [config('roles.host'), config('roles.uploaded_attendee')])->whereIn('registration_status_id', [config('registrationstatus.late_cancelled'), config('registrationstatus.cancelled')])->count();
                $noshow = $event->atendees()->whereNotIn('role_id', [config('roles.host'), config('roles.uploaded_attendee')])->where('registration_status_id', \Config::get('registrationstatus.booked'))->whereNull('checkin_time')->count();
                $api = $event->atendees()->where('api_booking', 1)->whereNotIn('role_id', [config('roles.host'), config('roles.uploaded_attendee')])->count();
                $web = $event->atendees()->where('api_booking', 0)->whereNotIn('role_id', [config('roles.host'), config('roles.uploaded_attendee')])->count();

                $manual_bookings = $event->atendees()->where('role_id', '!=', config('roles.host'))->where('registration_status_id', config('registrationstatus.booked'))->where('manual_booking', 1)->count();

                $data[] = array(
                    $event->title,
                    date("d/m/Y", strtotime($event->event_date_from)),
                    (sizeof($event->venue)) ? $event->venue->min_guaranteed : "",
                    (string) $attendees,
                    (string) $expected,
                    (string) $attended,
                    ($expected > 0) ? number_format($attended / $expected * 100) : '0',
                    (string) $manual_bookings,
                    ($manual_bookings > 0) ? number_format($manual_bookings / $attendees * 100, 2) : '0',
                    (string) $onsite,
                    ($attendees > 0) ? number_format($onsite / $attendees * 100) : '0',
                    (string) $cancelled,
                    ($attendees > 0) ? number_format($cancelled / $attendees * 100) : '0',
                    (string) $noshow,
                    ($expected > 0) ? number_format($noshow / $expected * 100) : '0',
                    (string) $api,
                    ($api > 0) ? number_format($api / $attendees * 100) : '0',
                    (string) $web,
                    ($web > 0) ? number_format($web / $attendees * 100) : '0',

                );

                if (sizeof($event->venue)) {$total_venue += $event->venue->min_guaranteed;
                }

                $total_booked += $attendees;
                $total_expected += $expected;
                $total_attended += $attended;
                $total_manual += $manual_bookings;
                $tatal_onsite += $onsite;
                $total_cancelled += $cancelled;
                $total_noshow += $noshow;
            }
        }

        // push totals
        $data[] = array(
            '',
            'Total:',
            (string) $total_venue,
            (string) $total_booked,
            (string) $total_expected,
            (string) $total_attended,
            '',
            (string) $total_manual,
            '',
            (string) $tatal_onsite,
            '',
            (string) $total_cancelled,
            '',
            (string) $total_noshow,
            '',
        );

        if ($request->has('save_pfs_report')) {
            $saved_report = new SavedReport();
            $saved_report->name = $request->get('saved_report_title');
            $saved_report->type = 'quaterBookings';
            $saved_report->client_report = 1;
            $saved_report->data = json_encode($request->input());
            $saved_report->save();
        }

        \Excel::create(
            'Booking statistics', function ($excel) use ($data) {

                $excel->sheet(
                    'Booking statistics', function ($sheet) use ($data) {

                        $sheet->cell(
                            'A1:O2', function ($cell) {
                                $cell->setBackground('#CFBEE5');
                                $cell->setAlignment('center');
                            }
                        );

                        $sheet->setOrientation('landscape');
                        $sheet->fromArray($data, null, 'A1', false, false);
                    }
                );
            }
        )->download('xls');
    }

    public function totalAttendees(Request $request)
    {
        if ($request->has('client_report_id')) {
            $sr = SavedReport::find($request->get('client_report_id'));
            $input = json_decode($sr->data, true);
            \Input::merge($input);
            $request->offsetUnset('save_pfs_report');
        }

        $from = ($request->has('from')) ? date("Y-m-d", strtotime(str_replace("/", "-", $request->get('from')))) : date("Y-m-d");
        $to = ($request->has('to')) ? date("Y-m-d", strtotime(str_replace("/", "-", $request->get('to')))) : date("Y-m-d");

        $columns = array('Event', 'Quarter', 'Date', 'Total Attendees');

        $events = $this->event
            ->where('event_date_from', '>=', $from)
            ->where('event_date_from', '<=', $to)
            ->where('publish', 1);

        if (!empty($request->get('event_type_id')) && !empty($request->get('event_type_id')[0])) {$events = $events->whereIn('event_type_id', $request->get('event_type_id'));
        }

        $events = $events->with('scheduleTimeSlots.attendees')
            ->orderBy('event_date_from')
            ->get();

        $data[] = $columns;

        foreach ($events as $event) {

            $data[] = array(
                $event->title,
                $this->calculateQuarter($event->event_date_from),
                date("d/m/Y", strtotime($event->event_date_from)),
                (string) $event->atendees()->where('registration_status_id', config('registrationstatus.booked'))->count(),
            );
        }

        if ($request->has('save_pfs_report')) {
            $saved_report = new SavedReport();
            $saved_report->name = $request->get('saved_report_title');
            $saved_report->type = 'totalAttendees';
            $saved_report->client_report = 1;
            $saved_report->data = json_encode($request->input());
            $saved_report->save();
        }

        \Excel::create(
            'Total Attendees ' . date("Y-m-d"), function ($excel) use ($data) {

                $excel->sheet(
                    'Bookings', function ($sheet) use ($data) {

                        $sheet->setOrientation('landscape');
                        $sheet->fromArray($data, null, 'A1', false, false);
                    }
                );
            }
        )->download('xls');
    }

    /**
     *
     */
    public function attendanceDesignation(Request $request)
    {
        if ($request->has('client_report_id')) {
            $sr = SavedReport::find($request->get('client_report_id'));
            $input = json_decode($sr->data, true);
            \Input::merge($input);
            $request->offsetUnset('save_pfs_report');
        }

        $columns = array(
            'Event',
            'Date',
            'Total on the day',
            'PFS Members',
            'Non PFS Members',
            'PFS Members %',
            'Non PFS Members %',
        );
        //$title = ($request->has('event_type_id')) ? EventType::find($request->get('event_type_id'))->title : "";

        $data[] = array('', '', '', '', 'Attendance Designation Statistics');
        $data[] = array('', '', '', '', '', '', '', '', '', '', '', '', '', '');

        $designations = AttendeeType::whereNotIn('id', array(12, 13, 14))->lists('title', 'id');

        foreach ($designations as $d) {

            if ($d === "PFS Members" || $d === "Non PFS Members") {continue;
            }
            array_push($columns, $d);
            array_push($columns, $d . ' %');

            if ($d == 'APFS' || $d == 'FPFS') {
                array_push($columns, $d . " Chartered");
                array_push($columns, $d . ' Not chartered');
            }

        }

        foreach (UserRoleSub::lists('title', 'id') as $contributor_type) {
            array_push($columns, $contributor_type);
            array_push($columns, $contributor_type . ' %');
        }

        $data[] = $columns;

        $from = ($request->has('from')) ? date("Y-m-d", strtotime(str_replace("/", "-", $request->get('from')))) : date("Y-m-d");
        $to = ($request->has('to')) ? date("Y-m-d", strtotime(str_replace("/", "-", $request->get('to')))) : date("Y-m-d");

        $from_month = date("n", strtotime($from));
        $to_monnth = date("n", strtotime($to));
        $year = date("Y", strtotime($from));

        for ($i = $from_month; $i <= $to_monnth; $i++) {

            $date = $year . "-" . $i;

            $data[] = array(date("F", strtotime($date)));

            $from = date("Y-m-01", strtotime($date));
            $to = date("Y-m-t", strtotime($date));

            $events = $this->event
                ->where('event_date_from', '>=', $from)
                ->where('event_date_from', '<=', $to)
                ->where('publish', 1);

            if (!empty($request->get('event_type_id')) && !empty($request->get('event_type_id')[0])) {$events = $events->whereIn('event_type_id', $request->get('event_type_id'));
            }

            $events = $events->with('atendees.attendeeTypes')
                ->orderBy('event_date_from')
                ->get();

            foreach ($events as $event) {

                //$expected = $event->atendees()->where('role_id', '!=', config('roles.host'))->where('registration_status_id', \Config::get('registrationstatus.booked'))->count();
                $attended = $event->atendees()->whereNotIn('role_id', [config('roles.host'), config('roles.uploaded_attendee')])->whereNotNull('checkin_time')->count();
                $attended_members = $event->atendees()->whereNotIn('role_id', [config('roles.host'), config('roles.uploaded_attendee')])->whereNotNull('checkin_time')->where('registration_status_id', \Config::get('registrationstatus.booked'))->where('role_id', \Config::get('roles.member'))->count();
                $attended_non_members = $event->atendees()->whereNotIn('role_id', [config('roles.host'), config('roles.uploaded_attendee')])->whereNotNull('checkin_time')->where('registration_status_id', \Config::get('registrationstatus.booked'))->where('role_id', \Config::get('roles.non_member'))->count();

                $temp_data = array(
                    $event->title,
                    date("d/m/Y", strtotime($event->event_date_from)),
                    (string) $attended,
                    (string) $attended_members,
                    (string) $attended_non_members,
                    ($attended > 0) ? number_format($attended_members / $attended * 100, 2) : '0',
                    ($attended > 0) ? number_format($attended_non_members / $attended * 100, 2) : '0',
                );

                // calculate attendance by each designation (attendee_type)
                foreach ($designations as $designation_id => $d) {

                    if ($d === "PFS Members" || $d === "Non PFS Members") {continue;
                    }

                    $count = $event->atendees()
                        ->whereNotNull('checkin_time')
                        ->whereNotIn('role_id', [config('roles.host'), config('roles.uploaded_attendee')])
                        ->where('registration_status_id', \Config::get('registrationstatus.booked'))
                        ->whereHas(
                            'attendeeTypes', function ($q) use ($designation_id) {
                                $q->where('id', $designation_id);
                            }
                        )
                        ->count();

                    array_push($temp_data, (string) $count); // bookings for designation type
                    $booking_percentage = ($attended > 0) ? number_format($count / $attended * 100, 2) : 0;
                    array_push($temp_data, (string) $booking_percentage); // booking percentage

                    if ($d == 'APFS' || $d == 'FPFS') {

                        array_push(
                            $temp_data, (string) $event->atendees()
                                ->whereNotNull('checkin_time')
                                ->where('role_id', '!=', config('roles.host'))
                                ->where('registration_status_id', \Config::get('registrationstatus.booked'))
                                ->where('chartered', 1)
                                ->whereHas(
                                    'attendeeTypes', function ($q) use ($designation_id) {
                                        $q->whereIn('id', [$designation_id]);
                                    }
                                )
                                ->count()
                        );

                        array_push(
                            $temp_data, (string) $event->atendees()
                                ->whereNotNull('checkin_time')
                                ->whereNotIn('role_id', [config('roles.host'), config('roles.uploaded_attendee')])
                                ->where('registration_status_id', \Config::get('registrationstatus.booked'))
                                ->where('chartered', 0)
                                ->whereHas(
                                    'attendeeTypes', function ($q) use ($designation_id) {
                                        $q->whereIn('id', [$designation_id]);
                                    }
                                )
                                ->count()
                        );
                    }
                }

                foreach (UserRoleSub::lists('title', 'id') as $id => $contributor_type) {
                    $attendees = $event->atendees()->whereNotNull('checkin_time')->where('registration_status_id', \Config::get('registrationstatus.booked'))->with('subRoles')->get();
                    $count = 0;

                    foreach ($attendees as $att) {
                        if (in_array($id, $att->subRoles()->lists('id'))) {$count++;
                        }
                    }

                    $count_percentage = ($attended > 0) ? number_format($count / $attended * 100, 2) : 0;
                    array_push($temp_data, (string) $count);
                    array_push($temp_data, $count_percentage . ' %');
                }

                $data[] = $temp_data;
            }

            $data[] = array(); // insert new line separator for each month
        }

        if ($request->has('save_pfs_report')) {
            $saved_report = new SavedReport();
            $saved_report->name = $request->get('saved_report_title');
            $saved_report->type = 'attendanceDesignation';
            $saved_report->client_report = 1;
            $saved_report->data = json_encode($request->input());
            $saved_report->save();
        }

        \Excel::create(
            'Attendance Designation Statistics', function ($excel) use ($data) {

                $excel->sheet(
                    'Attendance Designation', function ($sheet) use ($data) {
                        $sheet->cell(
                            'A1:M1', function ($cell) {
                                //$cell->setBackground('#CFBEE5');
                                $cell->setAlignment('center');
                                $cell->setFontsize('14');
                                $cell->setFontWeight('bold');
                            }
                        );

                        $sheet->setOrientation('landscape');
                        $sheet->fromArray($data, null, 'A1', false, false);
                    }
                );
            }
        )->download('xls');
    }

    public function registrationQuestions(Request $request)
    {
        if ($request->has('client_report_id')) {
            $sr = SavedReport::find($request->get('client_report_id'));
            $input = json_decode($sr->data, true);
            \Input::merge($input);
            $request->offsetUnset('save_pfs_report');
        }

        $columns = array('First name', 'Last name', 'Company', 'Job title', 'Email');

        $event = $this->event->with('atendees')->findOrFail($request->get('event_id'));

        $reg_questions = $event->registrationAnswers()->distinct('question')->select('question')->get();

        foreach ($reg_questions as $rq) {

            array_push($columns, $rq->question);
        }

        $data[] = $columns;

        foreach ($event->atendees()->where('registration_status_id', config('registrationstatus.booked'))->get() as $a) {

            $temp_data = array();
            $temp_data = array(
                $a->first_name,
                $a->last_name,
                $a->company,
                $a->job_title,
                $a->email,
            );

            // look for registration answers and push answers
            foreach ($reg_questions as $rq) {

                $answers = RegistrationAnswer::where('event_id', $event->id)->where('event_atendee_id', $a->pivot->id)->where('question', $rq->question)->get();
                $answer = "";

                foreach ($answers as $ans) {
                    if (!empty($ans->answer)) {
                        $answer .= $ans->answer . "; ";
                    }
                }

                array_push($temp_data, $answer);
            }

            $data[] = $temp_data;
        }

        if ($request->has('save_pfs_report')) {
            $saved_report = new SavedReport();
            $saved_report->name = $request->get('saved_report_title');
            $saved_report->type = 'registrationQuestions';
            $saved_report->client_report = 1;
            $saved_report->data = json_encode($request->input());
            $saved_report->save();
        }

        \Excel::create(
            'Registration questions', function ($excel) use ($data) {

                $excel->sheet(
                    'Bookings', function ($sheet) use ($data) {

                        $sheet->setOrientation('landscape');
                        $sheet->fromArray($data, null, 'A1', false, false);
                    }
                );
            }
        )->download('xls');
    }

    /**
     * Attendees who were waitlisted on event's slots
     */
    public function slotWaitingList(Request $request)
    {
        if ($request->has('client_report_id')) {
            $sr = SavedReport::find($request->get('client_report_id'));
            $input = json_decode($sr->data, true);
            \Input::merge($input);
            $request->offsetUnset('save_pfs_report');
        }

        $columns = array('Title', 'First name', 'Last name', 'PFS Number', 'Email');

        $event = $this->event->findOrFail($request->get('event_id'));
        $slots = $event->scheduleTimeSlots()->orderBy('start')->lists('title', 'id');
        $columns = array_merge($columns, $slots);

        $attendees = EventAttendee::where('event_id', $request->get('event_id'))
            ->whereHas(
                'slots', function ($q) {
                    $q->where('registration_status_id', \Config::get('registrationstatus.waitlisted'));
                }
            )->with('slots', 'user')->get();

        $data[] = $columns;

        foreach ($attendees as $event_attendee) {

            $temp_data = array();
            $temp_data = array(
                $event_attendee->user->title,
                $event_attendee->user->first_name,
                $event_attendee->user->last_name,
                $event_attendee->user->pin,
                $event_attendee->user->email,
            );

            foreach ($slots as $id => $s) {

                $waitlisted_slot = $event_attendee->slots()->where('event_schedule_slot_id', $id)->first();

                if (sizeof($waitlisted_slot) && $waitlisted_slot->registration_status_id == \Config::get('registrationstatus.waitlisted')) {
                    //array_push($temp_data, \Config::get('registrationstatus.'.$waitlisted_slot->registration_status_id).'('.date("d/m/Y", strtotime($waitlisted_slot->created_at)).')');
                    array_push($temp_data, date("d/m/Y", strtotime($waitlisted_slot->created_at)));
                } else {
                    array_push($temp_data, '');
                }
            }

            $data[] = $temp_data;
        }

        if ($request->has('save_pfs_report')) {
            $saved_report = new SavedReport();
            $saved_report->name = $request->get('saved_report_title');
            $saved_report->type = 'slotWaitingList';
            $saved_report->client_report = 1;
            $saved_report->data = json_encode($request->input());
            $saved_report->save();
        }

        \Excel::create(
            $event->title . ' Waiting list', function ($excel) use ($data) {

                $excel->sheet(
                    'Slot waiting list', function ($sheet) use ($data) {

                        $sheet->setOrientation('landscape');
                        $sheet->fromArray($data, null, 'A1', false, false);
                    }
                );
            }
        )->download('xls');
    }

    /**
     * Attendees with booked slots
     */
    public function slotBookings(Request $request)
    {
        if ($request->has('client_report_id')) {
            $sr = SavedReport::find($request->get('client_report_id'));
            $input = json_decode($sr->data, true);
            \Input::merge($input);
            $request->offsetUnset('save_pfs_report');
        }

        $columns = array('Title', 'First name', 'Last name', 'PFS Number', 'Email');

        $event = $this->event->findOrFail($request->get('event_id'));
        $slots = $event->scheduleTimeSlots()->orderBy('start')->lists('title', 'id');
        $columns = array_merge($columns, $slots);

        $attendees = EventAttendee::where('event_id', $request->get('event_id'))
            ->whereHas(
                'slots', function ($q) {
                    $q->where('registration_status_id', \Config::get('registrationstatus.booked'));
                }
            )->with('slots', 'user')->get();

        $data[] = $columns;

        foreach ($attendees as $event_attendee) {

            $temp_data = array();
            $temp_data = array(
                $event_attendee->user->title,
                $event_attendee->user->first_name,
                $event_attendee->user->last_name,
                $event_attendee->user->pin,
                $event_attendee->user->email,
            );

            foreach ($slots as $id => $s) {

                $slot = $event_attendee->slots()->where('event_schedule_slot_id', $id)->first();

                if (sizeof($slot) && $slot->registration_status_id == \Config::get('registrationstatus.booked')) {
                    array_push($temp_data, \Config::get('registrationstatus.' . $slot->registration_status_id) . '(' . date("d/m/Y", strtotime($slot->created_at)) . ')');
                } else {
                    array_push($temp_data, '');
                }
            }

            $data[] = $temp_data;
        }

        if ($request->has('save_pfs_report')) {
            $saved_report = new SavedReport();
            $saved_report->name = $request->get('saved_report_title');
            $saved_report->type = 'slotBookings';
            $saved_report->client_report = 1;
            $saved_report->data = json_encode($request->input());
            $saved_report->save();
        }

        \Excel::create(
            $event->title . ' Bookings', function ($excel) use ($data) {

                $excel->sheet(
                    'Slot bookings', function ($sheet) use ($data) {

                        $sheet->setOrientation('landscape');
                        $sheet->fromArray($data, null, 'A1', false, false);
                    }
                );
            }
        )->download('xls');
    }

    /**
     * Attendee list for event
     */
    public function attendeeList(Request $request)
    {

        if ($request->has('client_report_id')) {
            $sr = SavedReport::find($request->get('client_report_id'));
            $input = json_decode($sr->data, true);
            \Input::merge($input);
            $request->offsetUnset('save_pfs_report');
        }

        $event = $this->event->findOrFail($request->get('event_id'));

        $data[] = array('', '', '', '', '', '', $event->title . ' attendees list');
        $columns = array('ID', 'Pin', 'Title', 'First name', 'Last name', 'Badge name', 'Dietary req.', 'Special req.', 'Email', 'CC Email', 'Company', 'Postcode', 'Country', 'Mobile APP', 'Member', 'First time attendee', 'Checked-in', 'Sponsors Contact', 'Role', 'Sub Role', 'PFS Designation', 'Manual sign in', 'Manual sign out', 'Notes', 'Payment Amount', 'Payment Status', 'created_at', 'updated_at');

        $data[] = $columns;

        foreach ($event->atendees()->wherePivot('registration_status_id', config('registrationstatus.booked'))->with('role', 'subRoles', 'dietaryRequirement')->orderBy('last_name')->get() as $attendee) {
        $price = "";
        if ($attendee->role_id == role('member')) {
            $price = "price";
        } else {
            $price = "price_nonmember";
        }
        $pay = Payment::where('attendee_id', $attendee->pivot->id)->orderBy('created_at', 'DESC')->first();
        if(isset($pay) && !is_null($pay)) {
            if($pay->status_result == config('registrationstatus.booked')) {  
                
                $pay_status = 'PAYMENT COMPLETE';
            }
            if($pay->status_result == config('registrationstatus.complete_manuall_review')) {
                
                $pay_status = 'PAYMENT COMPLETE';
            }
            if($pay->status_result == config('registrationstatus.declined')) {
                
                $pay_status = 'PAYMENT DECLINE';
            }
            if($pay->status_result == config('registrationstatus.declined_by_bank')) {
                
                $pay_status = 'PAYMENT DECLINE BY BANK';
            }
            if($pay->status_result == config('registrationstatus.pending')) {
                
                $pay_status = 'PAYMENT PENDING';
            }    
            if($pay->status_result == config('registrationstatus.rejections_reference_validation')) {
                
                $pay_status = 'PAYMENT REJECTED';
            } 
        } else {
            $pay_status = "FREE";
        }

            $data[] = array(
                $attendee->pivot->id,
                $attendee->pin,
                $attendee->title,
                $attendee->first_name,
                $attendee->last_name,
                $attendee->badge_name,
                ($attendee->dietaryRequirement) ? $attendee->dietaryRequirement->title . " " . $attendee->dietary_requirement_other : "" . " " . $attendee->dietary_requirement_other,
                $attendee->special_requirements,
                $attendee->email,
                $attendee->cc_email,
                $attendee->company,
                $attendee->postcode,
                (sizeof($attendee->country)) ? $attendee->country->name : "",
                ($attendee->pivot->api_booking) ? "Y" : "N",
                ($attendee->role_id == role('member')) ? "Y" : "N",
                ($attendee->pivot->first_time_attendee) ? "Y" : "N",
                ($attendee->pivot->checkin_time) ? 'Y' : 'N',
                ($attendee->pivot->sponsors_contact) ? "Y" : "N",
                $attendee->role->title,
                implode(", ", $attendee->subroles->lists('title')),
                implode(", ", $attendee->attendeeTypes()->whereIn('id', [3, 4, 5, 7, 11])->lists('title')),
                '',
                '',
                $attendee->pivot->notes,
                ($event->$price) ? $event->$price : 0.00,
                $pay_status,
                $attendee->pivot->created_at,
                $attendee->pivot->updated_at,
            );

        }

        if ($request->has('save_pfs_report')) {
            $saved_report = new SavedReport();
            $saved_report->name = $request->get('saved_report_title');
            $saved_report->type = 'attendeeList';
            $saved_report->client_report = 1;
            $saved_report->data = json_encode($request->input());
            $saved_report->save();
        }

        \Excel::create(
            $event->title . ' Attendee list', function ($excel) use ($data) {

                $excel->sheet(
                    'Attendee list', function ($sheet) use ($data) {

                        $sheet->setOrientation('landscape');
                        $sheet->cell(
                            'A1:K1', function ($cell) {
                                $cell->setBackground('#CFBEE5');
                                $cell->setAlignment('center');
                                $cell->setFontsize('16');
                            }
                        );

                        $sheet->fromArray($data, null, 'A1', false, false);
                    }
                );
            }
        )->download('xls');
    }

    /**
     * Attendee list for Dates
     */
    public function attendeeListDates(Request $request)
    {

        if ($request->has('client_report_id')) {
            $sr = SavedReport::find($request->get('client_report_id'));
            $input = json_decode($sr->data, true);
            \Input::merge($input);
            $request->offsetUnset('save_pfs_report');
        }

        $from = ($request->has('from')) ? date("Y-m-d", strtotime(str_replace("/", "-", $request->get('from')))) : date("Y-m-d");
        $to = ($request->has('to')) ? date("Y-m-d", strtotime(str_replace("/", "-", $request->get('to')))) : date("Y-m-d");

        $attendees = EventAttendee::with('event', 'registrationStatus')
            ->where('created_at', '>=', $from)
            ->where('created_at', '<=', $to)
            ->get();

        $columns = array('ID', 'Pin','Title', 'First name', 'Last name', 'Badge name', 'Email', 'Company', 'Postcode','First time attendee','Event','Checked-in','Event Date','Status');

        $data[] = $columns;

        foreach ($attendees as $attendee) {

            $data[] = array(
                $attendee->user->id,
                $attendee->user->pin,
                $attendee->user->title,
                $attendee->user->first_name,
                $attendee->user->last_name,
                $attendee->user->badge_name,
                $attendee->user->email,
                $attendee->user->company,
                $attendee->user->postcode,
                ($attendee->first_time_attendee) ? "Y" : "N",
                isset($attendee->event) ? $attendee->event->title : "-",
                ($attendee->checkin_time) ? 'Y' : 'N',
                isset($attendee->event) ? $attendee->event->event_date_from : "-",
                isset($attendee->registrationStatus) ? $attendee->registrationStatus->title : "-",
            );

        }

        if ($request->has('save_pfs_report')) {
            $saved_report = new SavedReport();
            $saved_report->name = $request->get('saved_report_title');
            $saved_report->type = 'attendeeList';
            $saved_report->client_report = 1;
            $saved_report->data = json_encode($request->input());
            $saved_report->save();
        }

        \Excel::create(
            ' Attendee list', function ($excel) use ($data) {

                $excel->sheet(
                    'Attendee list', function ($sheet) use ($data) {

                        $sheet->setOrientation('landscape');
                        $sheet->cell(
                            'A1:N1', function ($cell) {
                                $cell->setBackground('#CFBEE5');
                                $cell->setAlignment('center');
                                $cell->setFontsize('16');
                            }
                        );

                        $sheet->fromArray($data, null, 'A1', false, false);
                    }
                );
            }
        )->download('xls');
    }

    /**
     * Attendee list status for event
     */
    public function attendeeListStatus(Request $request)
    {

        if ($request->has('client_report_id')) {
            $sr = SavedReport::find($request->get('client_report_id'));
            $input = json_decode($sr->data, true);
            \Input::merge($input);
            $request->offsetUnset('save_pfs_report');
        }

        $event = $this->event->findOrFail($request->get('event_id'));

        $data[] = array('', '', '', '', '', '', $event->title . ' attendees list');
        $columns = array('ID', 'Pin', 'Title', 'First name', 'Last name', 'status', 'status_updated', 'cancel_reason', 'Badge name', 'Dietary req.', 'Special req.', 'Email', 'CC Email', 'Company', 'Postcode', 'Country', 'Mobile APP', 'Member', 'First time attendee', 'Checked-in', 'Sponsors Contact', 'Role', 'Sub Role', 'PFS Designation', 'Manual sign in', 'Manual sign out', 'Notes', 'created_at', 'updated_at');

        $data[] = $columns;
        //registration_status_id
        foreach ($event->atendeesOnlyForReport()->with('role', 'subRoles', 'dietaryRequirement')->orderBy('last_name')->get() as $attendee) {

            $data[] = array(
                $attendee->pivot->id,
                $attendee->pin,
                $attendee->title,
                $attendee->first_name,
                $attendee->last_name,
                $attendee->status_title,
                $attendee->updated_at,
                $attendee->cancel_reason,
                $attendee->badge_name,
                ($attendee->dietaryRequirement) ? $attendee->dietaryRequirement->title . " " . $attendee->dietary_requirement_other : "" . " " . $attendee->dietary_requirement_other,
                $attendee->special_requirements,
                $attendee->email,
                $attendee->cc_email,
                $attendee->company,
                $attendee->postcode,
                (sizeof($attendee->country)) ? $attendee->country->name : "",
                ($attendee->pivot->api_booking) ? "Y" : "N",
                ($attendee->role_id == role('member')) ? "Y" : "N",
                ($attendee->pivot->first_time_attendee) ? "Y" : "N",
                ($attendee->pivot->checkin_time) ? 'Y' : 'N',
                ($attendee->pivot->sponsors_contact) ? "Y" : "N",
                $attendee->role->title,
                implode(", ", $attendee->subroles->lists('title')),
                implode(", ", $attendee->attendeeTypes()->whereIn('id', [3, 4, 5, 7, 11])->lists('title')),
                '',
                '',
                $attendee->pivot->notes,
                $attendee->pivot->created_at,
                $attendee->pivot->updated_at,
            );

        }

        if ($request->has('save_pfs_report')) {
            $saved_report = new SavedReport();
            $saved_report->name = $request->get('saved_report_title');
            $saved_report->type = 'attendeeList';
            $saved_report->client_report = 1;
            $saved_report->data = json_encode($request->input());
            $saved_report->save();
        }

        \Excel::create(
            $event->title . ' Attendee list', function ($excel) use ($data) {

                $excel->sheet(
                    'Attendee list', function ($sheet) use ($data) {

                        $sheet->setOrientation('landscape');
                        $sheet->cell(
                            'A1:K1', function ($cell) {
                                $cell->setBackground('#CFBEE5');
                                $cell->setAlignment('center');
                                $cell->setFontsize('16');
                            }
                        );

                        $sheet->fromArray($data, null, 'A1', false, false);
                    }
                );
            }
        )->download('xls');
    }

    /**
     * Event Attendee list for sponsors. Shows who have checked opt-in for sponsor email
     */
    public function attendeeListSponsors(Request $request)
    {
        if ($request->has('client_report_id')) {
            $sr = SavedReport::find($request->get('client_report_id'));
            $input = json_decode($sr->data, true);
            \Input::merge($input);
            $request->offsetUnset('save_pfs_report');
        }

        $event = $this->event->findOrFail($request->get('event_id'));

        $data[] = array('', '', $event->title . ' Attendees List');
        $columns = array('First name', 'Last name', 'Company', 'Post Code', 'Country', 'Checked-in', 'Email (where attendee has given permission to be contacted)');

        $data[] = $columns;
        $attendees = $event->atendees()
            ->wherePivot('registration_status_id', config('registrationstatus.booked'))
            ->whereIn('role_id', [config('roles.member'), role('roles.non_member')])
            ->orderBy('last_name')
            ->with('subRoles')
            ->get();

        foreach ($attendees as $attendee) {
            if (sizeof($attendee->subRoles)) {continue;
            }
            $data[] = array(
                $attendee->first_name,
                $attendee->last_name,
                $attendee->company,
                $attendee->postcode,
                (sizeof($attendee->country)) ? $attendee->country->name : "",
                ($attendee->pivot->checkin_time) ? 'Y' : 'N',
                ($attendee->pivot->sponsors_contact) ? $attendee->email : '',
            );
        }

        if ($request->has('save_pfs_report')) {
            $saved_report = new SavedReport();
            $saved_report->name = $request->get('saved_report_title');
            $saved_report->type = 'attendeeListSponsors';
            $saved_report->client_report = 1;
            $saved_report->data = json_encode($request->input());
            $saved_report->save();
        }

        \Excel::create(
            $event->title . ' Attendee list Sponsors', function ($excel) use ($data) {

                $excel->sheet(
                    'Attendee list', function ($sheet) use ($data) {

                        $sheet->setOrientation('landscape');
                        $sheet->cell(
                            'A1:E1', function ($cell) {
                                $cell->setBackground('#CFBEE5');
                                $cell->setAlignment('center');
                                $cell->setFontsize('16');
                            }
                        );

                        $sheet->cell(
                            'A2:E2', function ($cell) {

                                $cell->setFontsize('12');
                                $cell->setFontWeight('bold');
                            }
                        );

                        $sheet->fromArray($data, null, 'A1', false, false);
                    }
                );
            }
        )->download('xls');
    }

    /**
     * Feedbacks for every user in the event
     */
    public function feedback(Request $request)
    {
        if ($request->has('client_report_id')) {
            $sr = SavedReport::find($request->get('client_report_id'));
            $input = json_decode($sr->data, true);
            \Input::merge($input);
            $request->offsetUnset('save_pfs_report');
        }

        $event = $this->event->findOrFail($request->get('event_id'));

        $data[] = array('', '', '', '', '', '', $event->title . ' feedback');
        $columns = array('Title', 'First name', 'Last name', 'Email', 'Company', 'Sponsors Contact', 'Date');

        $event_feedbacks = $event->feedbacks->lists('id');
        // push column titles
        foreach ($event->feedbacks as $feedback) {

            if (!empty($feedback->session_id)) {
                $title = $feedback->session->title . '-' . $feedback->question;
            } else {
                $title = $feedback->question;
            }

            array_push($columns, $title);
        }

        $data[] = $columns;
        $attendees_no_answer = array();

        // push answers for each attendee
        $feedbacks_sum = array();
        $feedbacks_count = array();
        $attendees = $event->atendees()
            ->where('registration_status_id', config('registrationstatus.booked'))
        //this was removed eon 2018-10 because didn't make any sense
        //->whereIn('role_id', [config('roles.member'), config('roles.non_member')])
            ->with('feedbackAnswers')
            ->orderBy('sponsors_contact', 'desc')
            ->orderBy('last_name')->get();

        foreach ($attendees as $attendee) {
            $feedback_seq = $event_feedbacks;
            $avg_index = 0;

            $user_data = array(
                $attendee->title,
                $attendee->first_name,
                $attendee->last_name,
                $attendee->email,
                $attendee->company,
                ($attendee->pivot->sponsors_contact) ? 'Y' : 'N',
            );

            if (!sizeof($attendee->feedbackAnswers()->where('event_id', $event->id)->get())) {
                $user_data[] = '';
                $user_data[] = 'No feedback answers';
                $attendees_no_answer[] = $user_data;
                continue;
            }

            $first = true;
            $attendee_answers = $attendee->feedbackAnswers()->where('event_id', $event->id)->lists('answer', 'feedback_id');
            $first_answer = $attendee->feedbackAnswers->where('event_id', $event->id)->first();

            foreach ($feedback_seq as $question) {
                if ($first) {
                    $user_data[] = date("d/m/Y", strtotime($first_answer->created_at));
                    $first = false;
                }

                if (isset($attendee_answers[$question])) {
                    $user_data[] = $attendee_answers[$question];

                    // if answer is a number push value for average calculation

                    if (is_numeric($attendee_answers[$question])) {
                        // if ellement isset then add, else set
                        $feedbacks_sum[$avg_index] = (isset($feedbacks_sum[$avg_index])) ? $feedbacks_sum[$avg_index] + $attendee_answers[$question] : $attendee_answers[$question];
                        $feedbacks_count[$avg_index] = (isset($feedbacks_count[$avg_index])) ? $feedbacks_count[$avg_index] + 1 : 1;
                    } else {
                        $feedbacks_sum[$avg_index] = (isset($feedbacks_sum[$avg_index])) ? $feedbacks_sum[$avg_index] : '';
                        $feedbacks_count[$avg_index] = (isset($feedbacks_count[$avg_index])) ? $feedbacks_count[$avg_index] : 0;
                    }
                } else {
                    $user_data[] = '';
                    $feedbacks_sum[$avg_index] = (isset($feedbacks_sum[$avg_index])) ? $feedbacks_sum[$avg_index] : '';
                    $feedbacks_count[$avg_index] = (isset($feedbacks_count[$avg_index])) ? $feedbacks_count[$avg_index] : 0;
                }
                $avg_index++;
            }

            $data[] = $user_data;
        }

        // push averages line
        // empty columents for attendee details
        $averages = ['', '', '', '', '', '', ''];
        for ($i = 0; $i < sizeof($feedbacks_sum); $i++) {
            if (isset($feedbacks_sum[$i]) && is_numeric($feedbacks_sum[$i])) {
                $averages[] = round($feedbacks_sum[$i] / $feedbacks_count[$i], 1);
            } else {
                $averages[] = '';
            }
        }

        $data[] = $averages;

        // insert 2 empty lines ant then insert attendees with no answers
        $data[] = array();
        $data[] = array();
        $data = array_merge($data, $attendees_no_answer);

        if ($request->has('save_pfs_report')) {
            $saved_report = new SavedReport();
            $saved_report->name = $request->get('saved_report_title');
            $saved_report->type = 'feedbacks';
            $saved_report->client_report = 1;
            $saved_report->data = json_encode($request->input());
            $saved_report->save();
        }

        \Excel::create(
            $event->title . ' Feedback', function ($excel) use ($data) {

                $excel->sheet(
                    'Feedbacks', function ($sheet) use ($data) {

                        $sheet->setOrientation('landscape');

                        // document title
                        $sheet->cell(
                            'A1:K1', function ($cell) {
                                $cell->setBackground('#CFBEE5');
                                $cell->setAlignment('center');
                                $cell->setFontsize('16');
                            }
                        );

                        $sheet->setHeight(2, 40); // second row height
                        $sheet->cell(
                            'A2:AW2', function ($cell) {
                                $cell->setAlignment('right');
                                $cell->setFontWeight('bold');
                            }
                        );
                        // column titles wrapped
                        $sheet->getStyle('A2:AW2' . $sheet->getHighestRow())
                            ->getAlignment()->setWrapText(true);

                        $sheet->fromArray($data, null, 'A1', false, false);
                    }
                );
            }
        )->download('xls');
    }

    public function feedbackAverages(Request $request)
    {
        if ($request->has('client_report_id')) {
            $sr = SavedReport::find($request->get('client_report_id'));
            $input = json_decode($sr->data, true);
            \Input::merge($input);
            $request->offsetUnset('save_pfs_report');
        }

        $from = ($request->has('from')) ? date("Y-m-d", strtotime(str_replace("/", "-", $request->get('from')))) : date("Y-m-d");
        $to = ($request->has('to')) ? date("Y-m-d", strtotime(str_replace("/", "-", $request->get('to')))) : date("Y-m-d");
        $events = $this->event
            ->where('event_date_from', '>=', $from)
            ->where('event_date_from', '<=', $to)
            ->where('publish', 1);

        if (!empty($request->get('event_type_id')) && !empty($request->get('event_type_id')[0])) {$events = $events->whereIn('event_type_id', $request->get('event_type_id'));
        }

        $events = $events->with('feedbacks')->orderBy('event_date_from')->get();

        $event_types = EventType::whereIn('id', $request->get('event_type_id'))->lists('title');
        $data[] = array('Feedback summary report - ' . implode(", ", $event_types));
        $questions_row = array('Event Name'); // first column empty for
        $event_averages = array();

        foreach ($events as $event) {

            $current_row = array($event->title . ' Results Average');
            // loop through each event's feedback question (type = rating)
            foreach ($event->feedbacks()->where('type', 1)->with('session')->get() as $fb) {
                $avg = UserFeedback::where('event_id', $event->id)->where('feedback_id', $fb->id)->where('answer', '<>', '')->avg('answer');
                $average = round($avg, 1);

                // if question found insert in right column. if not add at the end
                $question = sizeof($fb->session) ? $fb->session->title . ' - ' . $fb->question : $fb->question;
                $existing_key = array_search($question, $questions_row);

                if ($existing_key === false) {
                    // not found
                    $items = array_push($questions_row, $question);
                    $current_row[$items - 1] = $average;
                } else {
                    // question found. Find column where to insert average
                    $current_row[$existing_key] = $average;
                }
            }

            $event_averages[] = $current_row;
        }

        // prepare generates averages grid for printing. Fill empty spaces
        $questions_count = sizeof($questions_row);
        foreach ($event_averages as $line => $average_row) {

            for ($i = 1; $i <= $questions_count; $i++) {
                if (!isset($average_row[$i])) {$average_row[$i] = '';
                }
            }
            // save changes
            $event_averages[$line] = $average_row;
        }

        // sort array so sequence of values is correct
        foreach ($event_averages as $line => $average_row) {
            ksort($event_averages[$line]);
        }

        // calculate total averages for each question. If no answer do not count as 0
        $total_averages = array('Total Averages');
        for ($i = 1; $i <= $questions_count; $i++) {
            $answers_count = 0;
            $answers_sum = 0;

            foreach ($event_averages as $average_row) {
                if (!empty($average_row[$i])) {
                    $answers_count++;
                    $answers_sum += $average_row[$i];
                }
            }
            $total_averages[$i] = ($answers_count > 0) ? round($answers_sum / $answers_count, 2) : 0;
        }

        // push all collected data and merge
        $data[] = $questions_row;
        $data = array_merge($data, $event_averages);
        $data[] = $total_averages;

        if ($request->has('save_pfs_report')) {
            $saved_report = new SavedReport();
            $saved_report->name = $request->get('saved_report_title');
            $saved_report->type = 'feedbackAverages';
            $saved_report->client_report = 1;
            $saved_report->data = json_encode($request->input());
            $saved_report->save();
        }

        \Excel::create(
            'Feedback Answers Averages', function ($excel) use ($data) {

                $excel->sheet(
                    'Answers averages', function ($sheet) use ($data) {
                        $sheet->setOrientation('landscape');
                        $sheet->fromArray($data, null, 'A1', false, false);
                        $sheet->cell(
                            'A1:G1', function ($cell) {
                                $cell->setFontWeight('bold');
                            }
                        );

                        $sheet->cell(
                            'A2:AN2', function ($cell) {
                                $cell->setFontColor('#FFFFFF');
                                $cell->setBackground('#990000');
                            }
                        );
                    }
                );
            }
        )->download('xls');
    }

    /**
     * Report on event basis. Sent emails vs opened emails
     *
     * @param Request $request
     */
    public function openedEmails(Request $request)
    {
        if ($request->has('client_report_id')) {
            $sr = SavedReport::find($request->get('client_report_id'));
            $input = json_decode($sr->data, true);
            \Input::merge($input);
            $request->offsetUnset('save_pfs_report');
        }

        $event = $this->event->findOrFail($request->get('event_id'));

        $data[] = array($event->title . ' sent emails vs opened emails');
        $data[] = array('Total emails sent', $event->sentEmails->count());
        $data[] = array('Total emails opened', $event->sentEmails()->whereNotNull('opened')->count());
        $data[] = array();
        $data[] = array('Grouped by email subject');
        $data[] = array('Subject', 'Sent', 'Opened');
        $grouped_sent = $event->sentEmails()->select('subject', DB::raw('count(*) as count'))->groupBy('subject')->get();

        foreach ($grouped_sent as $group) {
            $opened = SentEmail::where('subject', $group->subject)->whereNotNull('opened')->count();
            $data[] = array($group->subject, $group->count, $opened);
        }

        if ($request->has('save_pfs_report')) {
            $saved_report = new SavedReport();
            $saved_report->name = $request->get('saved_report_title');
            $saved_report->type = 'openedEmails';
            $saved_report->client_report = 1;
            $saved_report->data = json_encode($request->input());
            $saved_report->save();
        }

        \Excel::create(
            $event->title . ' Email openings', function ($excel) use ($data) {

                $excel->sheet(
                    'Email Openings', function ($sheet) use ($data) {

                        $sheet->setOrientation('landscape');
                        $sheet->cell(
                            'A1:C1', function ($cell) {
                                $cell->setBackground('#CFBEE5');
                                $cell->setAlignment('center');
                                $cell->setFontsize('16');
                            }
                        );

                        $sheet->fromArray($data, null, 'A1', true, false);
                    }
                );
            }
        )->download('xls');
    }

    public function clickedLinks(Request $request)
    {
        if ($request->has('client_report_id')) {
            $sr = SavedReport::find($request->get('client_report_id'));
            $input = json_decode($sr->data, true);
            \Input::merge($input);
            $request->offsetUnset('save_pfs_report');
        }

        $event = $this->event->findOrFail($request->get('event_id'));

        $data[] = array($event->title . ' sent emails vs opened emails');
        $data[] = array('Total emails sent', $event->sentEmails->count());
        $data[] = array('Total emails opened', $event->sentEmails()->whereNotNull('opened')->count());
        $data[] = array();
        $data[] = array('Grouped by email subject');
        $data[] = array('Subject', 'Sent', 'Opened');
        $grouped_sent = $event->sentEmails()->select('subject', DB::raw('count(*) as count'))->groupBy('subject')->get();

        foreach ($grouped_sent as $group) {
            $opened = SentEmail::where('subject', $group->subject)->whereNotNull('opened')->count();
            $data[] = array($group->subject, $group->count, $opened);

            // push links and opening results
            $data[] = array('Link', 'Clicks');
            $grouped_ids = SentEmail::where('event_id', $event->id)->where('subject', $group->subject)->lists('id');
            $clicked_links = ClickedLinksLog::whereIn('sent_email_id', $grouped_ids)->select('link', DB::raw('count(*) as count'))->groupBy('link')->get();

            foreach ($clicked_links as $clicked_link) {
                $data[] = array($clicked_link->link, $clicked_link->count);
            }

            // separate
            $data[] = array();
            $data[] = array();
        }

        if ($request->has('save_pfs_report')) {
            $saved_report = new SavedReport();
            $saved_report->name = $request->get('saved_report_title');
            $saved_report->type = 'clickedLinks';
            $saved_report->client_report = 1;
            $saved_report->data = json_encode($request->input());
            $saved_report->save();
        }

        \Excel::create(
            $event->title . ' Clicked links', function ($excel) use ($data) {

                $excel->sheet(
                    'Clicked Links', function ($sheet) use ($data) {

                        $sheet->setOrientation('landscape');
                        $sheet->cell(
                            'A1:C1', function ($cell) {
                                $cell->setBackground('#CFBEE5');
                                $cell->setAlignment('center');
                                $cell->setFontsize('16');
                            }
                        );

                        $sheet->fromArray($data, null, 'A1', true, false);
                    }
                );
            }
        )->download('xls');
    }

    public function emailCampaigns(Request $request)
    {
        if ($request->has('client_report_id')) {
            $sr = SavedReport::find($request->get('client_report_id'));
            $input = json_decode($sr->data, true);
            \Input::merge($input);
            $request->offsetUnset('save_pfs_report');
        }
        $campaign = $request->get('campaign');
        $campaign_emails = SentEmail::where('campaign', $campaign)->with('user', 'clicksLog')->get();
        $first_email = $campaign_emails->first();
        $event = $first_email->event;
        $data[] = array($campaign . ' email campaign report');
        $data[] = array('Total emails sent', $campaign_emails->count());
        $data[] = array('Total emails opened', SentEmail::where('campaign', $campaign)->whereNotNull('opened')->count());
        $data[] = array();
        $data[] = array('Email', 'Name Surname', 'Opened');

        foreach ($campaign_emails as $email) {

            $data[] = array(
                $email->to,
                $email->user->first_name . ' ' . $email->user->last_name,
                (!empty($email->opened)) ? date("d/m/y H:i", strtotime($email->opened)) : "",
            );

            // collect all clicked links by the user
            $user_links = $email->clicksLog()->select('link', DB::raw('count(*) as count'))->groupBy('link')->get();
            foreach ($user_links as $clicked_link) {
                $data[] = array($clicked_link->link, $clicked_link->count);
            }

            $data[] = array();
        }

        if ($request->has('save_pfs_report')) {
            $saved_report = new SavedReport();
            $saved_report->name = $request->get('saved_report_title');
            $saved_report->type = 'emailCampaigns';
            $saved_report->client_report = 1;
            $saved_report->data = json_encode($request->input());
            $saved_report->save();
        }

        \Excel::create(
            $campaign . ' email campaign report', function ($excel) use ($data) {

                $excel->sheet(
                    'Campaign report', function ($sheet) use ($data) {

                        $sheet->setOrientation('landscape');
                        $sheet->cell(
                            'A1:C1', function ($cell) {
                                $cell->setBackground('#CFBEE5');
                                $cell->setAlignment('center');
                                $cell->setFontsize('16');
                            }
                        );

                        $sheet->fromArray($data, null, 'A1', true, false);
                    }
                );
            }
        )->download('xls');
    }

    public function pfsMaster(Request $request)
    {
        if ($request->has('client_report_id')) {
            $sr = SavedReport::find($request->get('client_report_id'));
            $input = json_decode($sr->data, true);
            \Input::merge($input);
            $request->offsetUnset('save_pfs_report');
        }

        $from = ($request->has('from')) ? date("Y-m-d", strtotime(str_replace("/", "-", $request->get('from')))) : date("Y-m-d");
        $to = ($request->has('to')) ? date("Y-m-d", strtotime(str_replace("/", "-", $request->get('to')))) : date("Y-m-d");

        $columns = array('Event', 'First Name', 'Last Name', 'Email', 'Phone Number', 'Company', 'Status', 'Chartered', 'PFS Designation', 'Event Region', 'Event Type', 'Event Date');

        $events = $this->event
            ->where('event_date_from', '>=', $from)
            ->where('event_date_from', '<=', $to)
            ->where('publish', 1);

        if (!empty($request->get('event_type_id')) && !empty($request->get('event_type_id')[0])) {$events = $events->whereIn('event_type_id', $request->get('event_type_id'));
        }

        $events = $events->with('scheduleTimeSlots.attendees')
            ->orderBy('event_date_from')
            ->with('atendees.attendeeTypes', 'region', 'eventType')
            ->get();

        $data[] = $columns;

        foreach ($events as $event) {

            foreach ($event->atendees as $attendee) {
                $data[] = array(
                    $event->title,
                    $attendee->first_name,
                    $attendee->last_name,
                    $attendee->email,
                    $attendee->mobile,
                    $attendee->company,
                    config('registrationstatus.' . $attendee->pivot->registration_status_id),
                    ($attendee->chartered) ? "Yes" : "No",
                    implode(", ", $attendee->attendeeTypes()->whereIn('id', [3, 4, 5, 7, 11])->lists('title')),
                    $event->region->title,
                    $event->eventType->title,
                    date("d/m/Y", strtotime($event->event_date_from)),

                );
            }
        }

        if ($request->has('save_pfs_report')) {
            $saved_report = new SavedReport();
            $saved_report->name = $request->get('saved_report_title');
            $saved_report->type = 'pfsMaster';
            $saved_report->client_report = 1;
            $saved_report->data = json_encode($request->input());
            $saved_report->save();
        }

        \Excel::create(
            'PFS Master ' . date("Y-m-d"), function ($excel) use ($data) {

                $excel->sheet(
                    'Bookings', function ($sheet) use ($data) {

                        $sheet->setOrientation('landscape');
                        $sheet->fromArray($data, null, 'A1', false, false);
                    }
                );
            }
        )->download('xls');
    }

    public function selectedSessions(Request $request)
    {
        if ($request->has('client_report_id')) {
            $sr = SavedReport::find($request->get('client_report_id'));
            $input = json_decode($sr->data, true);
            \Input::merge($input);
            $request->offsetUnset('save_pfs_report');
        }

        $event = $this->event->findOrFail($request->get('event_id'));

        $data[] = array('', $event->title . ' selected sessions capacity');
        $columns = array('ID', 'Session Name', 'Date', 'Start', 'End', 'Meeting Space', 'Type', 'Session Capacity', 'Number Selected');
        $data[] = $columns;

        foreach ($event->scheduleSessions()->where('selectable', 1)->with('slot', 'type', 'meetingSpace', 'registrations')->get() as $session) {
            $data[] = array(
                $session->id,
                $session->title,
                date("d/m/Y", strtotime($session->slot->start_date)),
                date("H:i", strtotime($session->session_start)),
                date("H:i", strtotime($session->session_end)),
                $session->meetingSpace->meeting_space_name,
                $session->type->title,
                $session->capacity,
                $session->registrations->count(),
            );
        }

        if ($request->has('save_pfs_report')) {
            $saved_report = new SavedReport();
            $saved_report->name = $request->get('saved_report_title');
            $saved_report->type = 'selectedSessions';
            $saved_report->client_report = 1;
            $saved_report->data = json_encode($request->input());
            $saved_report->save();
        }

        \Excel::create(
            $event->title . ' selected sessions capacity ' . date("Y-m-d"), function ($excel) use ($data) {

                $excel->sheet(
                    'Selected Sessions', function ($sheet) use ($data) {

                        $sheet->setOrientation('landscape');
                        $sheet->cell(
                            'A1:E1', function ($cell) {
                                $cell->setBackground('#CFBEE5');
                                $cell->setAlignment('center');
                                $cell->setFontsize('16');
                            }
                        );
                        $sheet->cell(
                            'A2:E2', function ($cell) {
                                $cell->setFontWeight('bold');
                            }
                        );

                        $sheet->fromArray($data, null, 'A1', true, false);
                    }
                );
            }
        )->download('xls');
    }

    /**
     * Calculate quarder from date
     */
    private function calculateQuarter($date)
    {
        $month = date("m", strtotime($date));
        $quater = number_format(ceil($month / 3), 0);

        return $quater;
    }
}
