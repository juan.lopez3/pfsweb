<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\ScheduleChoice;
use App\BookingDate;
use App\EventSetting;
use App\ScheduleGroup;

class ScheduleChoiceController extends Controller
{
	
	private $schedule_choice;
	
	public function __construct(ScheduleChoice $schedule_choice) {
		
		$this->schedule_choice = $schedule_choice;
		
		$this->middleware('superadmin');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.schedules.scheduleChoices.list')->with('schedule_choices', $this->schedule_choice->all())
        	->with('booking_dates', BookingDate::all())
			->with('es', EventSetting::first())
			->with('schedule_groups', ScheduleGroup::lists('title', 'id')->all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.schedules.scheduleChoices.create')->with('booking_dates', BookingDate::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $schedule_choice = $this->schedule_choice->create($request->all());
		
		// sync schedule date value
		$sync = array();
		
		for ($i=0; $i < sizeof($request->get('booking_date_id')); $i++) { 
			
			$bd_id = $request->get('booking_date_id')[$i];
			
			$sync[$bd_id] = array(
				'price' => $request->get('booking_date_price')[$i],
				'taxable' => $request->has('booking_date_taxable_'.$bd_id)
			);
		}
		
		$schedule_choice->bookingDates()->sync($sync);
		
		if($schedule_choice) \Session::flash('success', 'Schedule choice was successfully created!');
		
		return redirect()->route('schedules.scheduleChoices');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		return view('admin.schedules.scheduleChoices.edit')->with('schedule_choice', $this->schedule_choice->with('bookingDates')->findOrFail($id))
			->with('booking_dates', BookingDate::all())
			->with('es', EventSetting::first())
			->with('schedule_groups', ScheduleGroup::lists('title', 'id')->all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $schedule_choice = $this->schedule_choice->findOrFail($id);
		
		// save schedule choice data
		$schedule_choice->title = $request->get('title');
		$schedule_choice->capacity = $request->get('capacity');
		$schedule_choice->coupon = $request->get('coupon');
		$schedule_choice->active = ($request->has('active')) ? 1 : 0;
		$schedule_choice->save();
		
		// sync schedule date value
		$sync = array();
		
		for ($i=0; $i < sizeof($request->get('booking_date_id')); $i++) { 
			
			$bd_id = $request->get('booking_date_id')[$i];
			
			$sync[$bd_id] = array(
				'price' => $request->get('booking_date_price')[$i],
				'taxable' => $request->has('booking_date_taxable_'.$bd_id)
			);
		}
		
		$schedule_choice->bookingDates()->sync($sync);
		
		\Session::flash('success', 'Schedule choice was successfully updated!');
		
		return redirect()->route('schedules.scheduleChoices');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->schedule_choice->destroy($id);
		\Session::flash('success', 'Schedule choice was successfully deleted!');
    }
}
