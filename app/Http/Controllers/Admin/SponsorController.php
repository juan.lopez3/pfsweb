<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Sponsor;
use App\Events\DataWasManipulated;

class SponsorController extends Controller {
	
	private $sponsor;
	public $log_desc = "Sponsor ";
	
	public function __construct(Sponsor $sponsor) {
		
		$this->sponsor = $sponsor;
	}
	/**
	 * Display a listing of the sponsors.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('admin.sponsors.list')->with('sponsors', $this->sponsor->all())->orderBy('name','ASC')->get();
	}

	/**
	 * Show the form for creating a new sponsor.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('admin.sponsors.create');
	}

	/**
	 * Store a newly created sponsors in storage.
	 *
	 * @return Response
	 */
	public function store(Requests\CreateSponsorRequest $request)
	{
		$sponsor = $this->sponsor->create($request->all());
		event(new DataWasManipulated('actionCreate', $this->log_desc.$sponsor->name));
		
		return redirect()->route('sponsors');
	}

	/**
	 * Show the form for editing the specified sponsor.
	 *
	 * @param  int  $id  sponsor id
	 * @return Response
	 */
	public function edit($id)
	{
		return view('admin.sponsors.edit')->with('sponsor', $this->sponsor->find($id));
	}

	/**
	 * Update the specified sponsor in storage.
	 *
	 * @param  int  $id  sponsor id
	 * @return Response
	 */
	public function update(Requests\CreateSponsorRequest $request, $id)
	{
		$sponsor = $this->sponsor->find($id);
		
		if (!empty($sponsor->logo) && ($sponsor->logo != $request->get('logo'))) removeFile($sponsor->logo, SPONSOR);
		
		$sponsor->fill($request->input())->save();
		event(new DataWasManipulated('actionUpdate', $this->log_desc.$sponsor->name));

		return redirect()->route('sponsors');
	}

	/**
	 * Remove the specified sponsor from storage.
	 *
	 * @param  int  $id  sponsor id
	 * @return Response
	 */
	public function destroy($id)
	{
		$sponsor = $this->sponsor->find($id);
		
		// if sponsor is assigned to any event don't allow to delete
		if(sizeof($sponsor->events)) return redirect()->route('sponsors')->withErrors('Sponsor is assigned to at least one event. Unable to delete sponsor!');
		
		event(new DataWasManipulated('actionDelete', $this->log_desc.$sponsor->name));
		$sponsor->delete();
		
		return redirect()->route('sponsors');
	}
	
	/**
	 * Service to upload sponsor logo
	 * 
	 * @return  string saved file name
	 */
	public function uploadLogo(Request $request) {
		
		// upload sponsor logo
		if($request->file('myfile')) {
			
			$saved_file = uploadFile($request->file('myfile'), SPONSOR);
			
			return response()->json($saved_file);
		}
		
		return null;
	}
	/**
	 * Remove the specified sponsor Advert from storage.
	 *
	 * @param  int  $id  sponsor id
	 * @return Response
	 */
	public function destroyLogo($sponsor_id)
	{
		$sponsor = $this->sponsor->find($sponsor_id);
        $sponsor->app_logo = null;
        $sponsor->save();
		
		return redirect()->route('sponsors');
	}

}
