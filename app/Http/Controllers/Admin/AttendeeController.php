<?php

namespace App\Http\Controllers\Admin;

use DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\EventSetting;
use App\Country;
use App\Hotel;
use App\ScheduleGroup;
use App\ScheduleChoice;
use App\Qualification;
use App\Specialism;


class AttendeeController extends Controller
{
	
	private $attendee;
	
	public function __construct(User $attendee) {
		
		$this->attendee = $attendee;
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.attendees.list')->with('attendees', $this->attendee->where('role_id', \Config::get('roles.attendee'))->with('rooms')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$schedule_groups = ScheduleGroup::whereHas('scheduleChoices.bookingDates', function($q) {
			$q->where('from', '<=', date("Y-m-d"));
			$q->where('to', '>=', date("Y-m-d"));
		})->with('scheduleChoices.bookingDates')->get();
		
    	$accommodation = Hotel::whereHas('rooms', function($q){
			$q->where('active_from', '<=', date("Y-m-d"));
			$q->where('active_to', '>=', date("Y-m-d"));
			
    	})->with('rooms')->get();
    	
		$es = EventSetting::first();
		
        return view('admin.attendees.create')
        	->with('phone_codes', Country::select('phone_code', DB::raw('CONCAT(phone_code, " - ", name) AS full_code'))->orderBy('name')->lists('full_code', 'phone_code')->all())
			->with('schedule_groups', $schedule_groups)
			->with('accommodation', $accommodation)
			->with('countries', Country::lists('name', 'id')->all())
			->with('qualifications', Qualification::lists('title', 'id')->all())
			->with('specialisms', Specialism::lists('title', 'id')->all())
			->with('es', $es);
    }
	
	public function show($id) {
		
		return view('admin.attendees.view')->with('attendee', $this->attendee->findOrFail($id))->with('es', EventSetting::first());
	}
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\CreateAttendeeRequest $request)
    {
    	// Store new attendee
    	$attendee = new User();
		$attendee->title = $request->get('title');
		$attendee->first_name = $request->get('first_name');
		$attendee->last_name = $request->get('last_name');
		$attendee->suffix = $request->get('suffix');
		$attendee->telephone_code = $request->get('telephone_code');
		$attendee->telephone_phone = $request->get('telephone_phone');
		$attendee->mobile_code = $request->get('mobile_code');
		$attendee->mobile_phone = $request->get('mobile_phone');
		$attendee->role_id = \Config::get('roles.attendee');
		$attendee->email = $request->get('email');
		$attendee->password = bcrypt($request->get('password'));
		$attendee->job_title = $request->get('job_title');
		$attendee->badge_name = $request->get('badge_name');
		$attendee->organisation_name = $request->get('organisation_name');
		$attendee->city = $request->get('city');
		$attendee->dietary_requirements = $request->get('dietary_requirements');
		$attendee->country_id = $request->get('country_id');
		$attendee->qualification_id = $request->has('qualification_id') ? $request->get('qualification_id') : null;
		$attendee->specialism_id = $request->has('specialism_id') ? $request->get('specialism_id') : null;
		$attendee->receipt_required = $request->get('receipt_required');
		$attendee->receipt_address_name = $request->get('receipt_address_name');
		$attendee->receipt_address_address_1 = $request->get('receipt_address_address_1');
		$attendee->receipt_address_address_2 = $request->get('receipt_address_address_2');
		$attendee->receipt_address_city = $request->get('receipt_address_city');
		$attendee->receipt_address_postcode = $request->get('receipt_address_postcode');
		$attendee->receipt_address_country_id = $request->has('receipt_address_country_id') ? $request->get('receipt_address_country_id') : null;
		$attendee->receipt_email = $request->get('receipt_email');
		$attendee->receipt_tax_number = $request->get('receipt_tax_number');
		$attendee->receipt_po_number = $request->get('receipt_po_number');
		$attendee->booking_status = 1;
		$attendee->online = 0;
		$attendee->paid = 0;
		$attendee->balance = 0;
		$attendee->used_voucher = $request->get('used_voucher');
		$attendee->save();
		
		// save schedule choices
		if($request->has('choices')) {
			
			$attendee->scheduleChoices()->sync($request->get('choices'));
		}
		
		// save accommodation
		if($request->has('bookable_rooms')) {
			
			$sync = array();
			
			foreach($request->get('bookable_rooms') as $r) {
				
				if($request->has('rooms'.$r) && $request->get('rooms'.$r) != "0") {
					
					$sync[$r] = array(
						'price' => $request->get('rooms'.$r),
						'occupancy' => $request->get('occupancy'.$r.$request->get('rooms'.$r))
					);
				}
			}

			$attendee->rooms()->sync($sync);
		}
		
		if($attendee) \Session::flash('success', 'Attendee was successfully created!');
		
		return redirect()->route('attendees');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$attendee = $this->attendee->findOrFail($id);
    	$schedule_groups = ScheduleGroup::whereHas('scheduleChoices.bookingDates', function($q) {
			$q->where('from', '<=', date("Y-m-d"));
			$q->where('to', '>=', date("Y-m-d"));
		})->with('scheduleChoices.bookingDates')->get();
		
    	$accommodation = Hotel::whereHas('rooms', function($q){
			$q->where('active_from', '<=', date("Y-m-d"));
			$q->where('active_to', '>=', date("Y-m-d"));
			
    	})->with('rooms')->get();
    	
		$es = EventSetting::first();
		
		return view('admin.attendees.edit')
			->with('attendee', $attendee)
			->with('phone_codes', Country::select('phone_code', DB::raw('CONCAT(phone_code, " - ", name) AS full_code'))->orderBy('name')->lists('full_code', 'phone_code')->all())
			->with('schedule_groups', $schedule_groups)
			->with('accommodation', $accommodation)
			->with('countries', Country::lists('name', 'id')->all())
			->with('qualifications', Qualification::lists('title', 'id')->all())
			->with('specialisms', Specialism::lists('title', 'id')->all())
			->with('es', $es);
	    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $attendee = $this->attendee->findOrFail($id);
		
		$attendee->title = $request->get('title');
		$attendee->first_name = $request->get('first_name');
		$attendee->last_name = $request->get('last_name');
		$attendee->suffix = $request->get('suffix');
		$attendee->telephone_code = $request->get('telephone_code');
		$attendee->telephone_phone = $request->get('telephone_phone');
		$attendee->mobile_code = $request->get('mobile_code');
		$attendee->mobile_phone = $request->get('mobile_phone');
		$attendee->email = $request->get('email');
		$attendee->job_title = $request->get('job_title');
		$attendee->badge_name = $request->get('badge_name');
		$attendee->organisation_name = $request->get('organisation_name');
		$attendee->city = $request->get('city');
		$attendee->dietary_requirements = $request->get('dietary_requirements');
		$attendee->country_id = $request->get('country_id');
		$attendee->qualification_id = $request->has('qualification_id') ? $request->get('qualification_id') : null;
		$attendee->specialism_id = $request->has('specialism_id') ? $request->get('specialism_id') : null;
		$attendee->receipt_required = $request->get('receipt_required');
		$attendee->receipt_address_name = $request->get('receipt_address_name');
		$attendee->receipt_address_address_1 = $request->get('receipt_address_address_1');
		$attendee->receipt_address_address_2 = $request->get('receipt_address_address_2');
		$attendee->receipt_address_city = $request->get('receipt_address_city');
		$attendee->receipt_address_postcode = $request->get('receipt_address_postcode');
		$attendee->receipt_address_country_id = $request->has('receipt_address_country_id') ? $request->get('receipt_address_country_id') : null;
		$attendee->receipt_email = $request->get('receipt_email');
		$attendee->receipt_tax_number = $request->get('receipt_tax_number');
		$attendee->receipt_po_number = $request->get('receipt_po_number');
		$attendee->online = 0;
		$attendee->used_voucher = $request->get('used_voucher');
		$attendee->save();
		
		if ($request->has('full_update')) {
			// save schedule choices
			if($request->has('choices')) {
				
				$attendee->scheduleChoices()->sync($request->get('choices'));
			}
			
			// save accommodation
			if($request->has('bookable_rooms')) {
				
				$sync = array();
				
				foreach($request->get('bookable_rooms') as $r) {
					
					if($request->has('rooms'.$r) && $request->get('rooms'.$r) != "0") {
						
						$sync[$r] = array(
							'price' => $request->get('rooms'.$r),
							'occupancy' => $request->get('occupancy'.$r.$request->get('rooms'.$r))
						);
					}
				}
				
				$attendee->rooms()->sync($sync);
			}
		}

		\Session::flash('success', 'Attendee was successfully updated!');
		
		return redirect()->route('attendees');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->attendee->destroy($id);
		\Session::flash('success', 'Attendee was successfully deleted!');
    }
	
	public function addNote(Request $request, $id) {
		
		$attendee = $this->attendee->findOrFail($id);
		$attendee->notes()->create([
			'note' => $request->get('note')
		]);
		
		\Session::flash('success', 'Note for attendee was successfully added!');
		
		return redirect()->route('attendees.show', $id);
	}
}
