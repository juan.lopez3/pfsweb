<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Hotel;
use App\Country;

class HotelController extends Controller
{
	
	private $hotel;
	
	public function __construct(Hotel $hotel) {
		
		$this->hotel = $hotel;
		
		$this->middleware('superadmin');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.hotels.list')->with('hotels', $this->hotel->with('country')->get())->with('countries', Country::lists('name', 'id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.vourhcers.create')->with('countries', Country::lists('name', 'id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hotel = $this->hotel->create($request->all());
		
		if($hotel) \Session::flash('success', 'Hotel was successfully created!');
		
		return redirect()->route('hotels');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		return view('admin.hotels.edit')->with('hotel', $this->hotel->findOrFail($id))->with('countries', Country::lists('name', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hotel = $this->hotel->findOrFail($id);
		$hotel->fill($request->all())->save();
		\Session::flash('success', 'Hotel was successfully updated!');
		
		return redirect()->route('hotels');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->hotel->destroy($id);
		\Session::flash('success', 'Hotel was successfully deleted!');
    }
}
