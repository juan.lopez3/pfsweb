<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use Mail;
use Carbon\Carbon;
use App\Issue;
use App\User;
use App\IssueChange;
use App\IssueAttachment;
use App\Commands\SendEmail;

use Illuminate\Http\Request;

class IssueController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    public function index(Request $request)
    {
        $issues = new Issue();

        if ($request->get('show-closed')) {
            $issues = $issues->get();
        } else {
            $issues = $issues->where('status', '<>', config('issues.closed'))->get();
        }

        return view('admin.issues.list')
            ->with('issues', $issues);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $users = User::select('id', DB::raw('CONCAT(first_name, " ", last_name) AS full_name'))
            ->where('role_id','<', 4)
			->orderBy('first_name')
            ->lists('full_name', 'id');
			

        return view('admin.issues.create')->with('users', $users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Requests\CreateIssueRequest $request)
    {
        $issue = Issue::create($request->input());
        $issue->due = date("Y-m-d", strtotime(str_replace("/", "-", $request->get('due'))));
        $issue->save();
        if (sizeof($request->file('file'))) {
            $saved_file = uploadFile($request->file('file'), 'ticket_attachments');
            IssueAttachment::create([
                'issue_id' => $issue->id,
                'name' => $saved_file,
                'saved_name' => $saved_file,
            ]);
        }

        $issue_change = new IssueChange();
        $issue_change->issue_id = $issue->id;
        $issue_change->user_id = auth()->user()->id;
        $issue_change->change = "Created support ticket. ".$request->get('message');
        $issue_change->save();

        // send email about new issue created
        $assigned_to = $issue->assignedTo;
        Mail::send('admin.emails.support_ticket', ['issue' => $issue, 'issue_change' => $issue_change], function ($m) use ($assigned_to, $issue) {
            $m->to($assigned_to->email)
                ->subject('New Support ticket (ID: '.$issue->id.') created. '.$issue->title);
        });

        Mail::send('admin.emails.support_ticket', ['issue' => $issue, 'issue_change' => $issue_change], function ($m) use ($issue) {
            $m->to(config('issues.admin_email'))
                ->subject('New Support ticket (ID: '.$issue->id.') created. '.$issue->title);
        });

        return redirect()->route('issues');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $users = User::select('id', DB::raw('CONCAT(first_name, " ", last_name) AS full_name'))
            ->where('role_id','<', 4)
            ->orderBy('first_name')
            ->lists('full_name', 'id');

        return view('admin.issues.edit')->with('issue', Issue::findOrFail($id))->with('users', $users);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Requests\CreateIssueRequest $request, $id)
    {
        $changes = "";
        $issue = Issue::findOrFail($id);

        if (sizeof($request->file('file'))) {
            $saved_file = uploadFile($request->file('file'), 'ticket_attachments');
            IssueAttachment::create([
                'issue_id' => $issue->id,
                'name' => $saved_file,
                'saved_name' => $saved_file,
            ]);
        }

        if($issue->title != $request->get('title')) $changes .= "Changed previous title ".$issue->title."<br/>";
        if($issue->priority != $request->get('priority')) $changes .= "Changed priority from ".config('issues.priority.'.$issue->priority)." to ".config('issues.priority.'.$request->get('priority'))."<br/>";
        if($issue->description != $request->get('description')) $changes .= "Updated description <br/>";
        if($issue->status != $request->get('status')) $changes .= "Changed status from ".config('issues.options.'.$issue->status)." to ".config('issues.options.'.$request->get('status'))."<br/>";
        if($issue->type != $request->get('type')) $changes .= "Changed type from ".config('issues.type.'.$issue->type)." to ".config('issues.type.'.$request->get('type'))."<br/>";

        if($issue->quote_min != $request->get('quote_min')) $changes .= "Changed min quote to ".$request->get('quote_min')." <br/>";
        if($issue->quote_max != $request->get('quote_max')) $changes .= "Changed max quote to ".$request->get('quote_max')." <br/>";
        if($issue->quote_final != $request->get('quote_final')) $changes .= "Changed final quote to ".$request->get('quote_final')." <br/>";

        $issue->fill($request->input());
        $issue->due = date("Y-m-d", strtotime(str_replace("/", "-", $request->get('due'))));
        $issue->save();

        if($request->has('message')) $changes .= "<b>Message:</b> ".$request->get('message');

        // if any changes made
        if(!empty($changes)) {
            $issue_change = new IssueChange();
            $issue_change->issue_id = $issue->id;
            $issue_change->user_id = auth()->user()->id;
            $issue_change->change = $changes;
            $issue_change->save();

            $assigned_to = $issue->assignedTo;
            Mail::send('admin.emails.support_ticket', ['issue' => $issue, 'issue_change' => $issue_change], function ($m) use ($assigned_to, $issue) {
                $m->to($assigned_to->email)
                    ->subject('Support ticket (ID: '.$issue->id.') updated. '.$issue->title);
            });

            Mail::send('admin.emails.support_ticket', ['issue' => $issue, 'issue_change' => $issue_change], function ($m) use ($issue) {
                $m->to(config('issues.admin_email'))
                    ->subject('Support ticket (ID: '.$issue->id.') updated. '.$issue->title);
            });
        }

        return redirect()->route('issues');
    }

    public function confirm($issue_id)
    {
        $issue = Issue::findOrFail($issue_id);
        $issue->status = config('issues.confirmed');
        $issue->confirmed = 1;
        $issue->save();

        $issue_change = new IssueChange();
        $issue_change->issue_id = $issue->id;
        $issue_change->user_id = auth()->user()->id;
        $issue_change->change = "Confirmed the task";
        $issue_change->save();

        Mail::send('admin.emails.support_ticket', ['issue' => $issue, 'issue_change' => $issue_change], function ($m) use ($issue) {
            $m->to(config('issues.admin_email'))
                ->subject('Support ticket (ID: '.$issue->id.') confirmed. '.$issue->title);
        });

        return redirect()->route('issues');
    }

    /**
     * Service to upload images
     *
     * @return  string saved file name
     */
    public function uploadImage(Request $request) {

        // upload sponsor logo
        if($request->file('myfile')) {

            $saved_file = uploadFile($request->file('myfile'), FILE);

            return response()->json($saved_file);
        }

        return null;
    }

}
