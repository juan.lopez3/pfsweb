<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\OutsideReport;
use App\EventType;
use Illuminate\Http\Request;
use App\Events\DataWasManipulated;
use Validator;

class OutsideReportController extends Controller {
    
    public $log_desc = "Outside report ";
    
    
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
	    $event_types = EventType::lists('title', 'id');
		return view('admin.reports.outside.list')->with('reports', OutsideReport::all())
            ->with('event_types', $event_types);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$validator = Validator::make($request->input(), [
			'event_type_id' => 'required',
		]);
		
		if ($validator->fails()) {

			return response()->json(['success'=>false, 'error' => $validator->messages()], 400);
		}

	    $from = str_replace("/", "-", $request->get('filter_from'));
        $to = str_replace("/", "-", $request->get('filter_to'));
        
		$report = new OutsideReport();
        $report->name = $request->get("name");
        $report->email = $request->get("email");
        $report->filter_from = date("Y-m-d", strtotime($from));
        $report->filter_to = date("Y-m-d", strtotime($to));
        $report->event_type_id = $request->get("event_type_id");
        $report->interval = $request->get("interval");
        $report->save();
        
        return redirect()->route('reports.outside');
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$report = OutsideReport::find($id);
        $event_types = EventType::lists('title', 'id');
        
        return view('admin.reports.outside.edit')->with('report', $report)
            ->with('event_types', $event_types);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$validator = Validator::make($request->input(), [
			'event_type_id' => 'required',
		]);
		
		if ($validator->fails()) {
			
			return response()->json(['success'=>false, 'error' => $validator->messages()], 400);
		}

		$from = str_replace("/", "-", $request->get('filter_from'));
        $to = str_replace("/", "-", $request->get('filter_to'));
        
        $report = OutsideReport::find($id);
        $report->name = $request->get("name");
        $report->email = $request->get("email");
        $report->filter_from = date("Y-m-d", strtotime($from));
        $report->filter_to = date("Y-m-d", strtotime($to));
        $report->event_type_id = $request->get("event_type_id");
        $report->interval = $request->get("interval");
        $report->save();
        
        return redirect()->route('reports.outside');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$report = OutsideReport::find($id); 
        event(new DataWasManipulated('actionDelete', $this->log_desc.$report->name));
        $report->delete();
        
        return redirect()->route('reports.outside');
	}

}
