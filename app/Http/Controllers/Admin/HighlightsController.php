<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Highlights;
use App\Event;
use App\Events\DataWasManipulated;

class HighlightsController extends Controller {

    public $log_desc = "highlights ";

    /**
     * Display a listing of the highlights.
     *
     * @return list of highlights
     */
    public function index()
    {
        $events = event::where('event_date_from', '>=', date('Y-m-d'))
        ->orderBy('created_at','desc')->get();

        $events_select = array();
		
		foreach($events as $event) {
			
			$events_select[$event->id] = $event->title;
		}


        return view('admin.settings.highlights.list')
        ->with('highlights', highlights::all())
        ->with('events', $events)
        ->with('events_select', $events_select);
    }

    /**
     * Store a newly created highlights in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        // return $request;

        if($request->has('events_id')) {
			
			// $highlights = array();
			
			foreach($request->get('events_id') as $key=>$q) {
				
				$highlights = new highlights([
					'event_id' => $q,
					'title' => $request->get('name')[$key],
                ]);
                
            $highlights->save();
            }
        }

        return redirect()->route('settings.highlights');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id  highlights id
     * @return Response
     */
    public function destroy($id)
    {
        $highlights = highlights::findOrFail($id);
        if ($highlights->delete()) event(new DataWasManipulated('actionDelete', $this->log_desc.$highlights->title));

        return redirect()->route('settings.highlights');
    }

}
