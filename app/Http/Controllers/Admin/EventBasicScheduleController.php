<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\EventBasicSchedule;
use App\Events\DataWasManipulated;

class EventBasicScheduleController extends Controller {

	private $schedule;
	public $log_desc = "Basic schedule for event ";
	
	public function __construct(EventBasicSchedule $schedule) {
		
		$this->schedule = $schedule;
	}
	
	/**
	 * Update event schedule in event settings schedule tab
	 * 
	 * 	@param int $id event id
	 * 	@return to event settings schedule tab
	 */
	public function update(Request $request, $id)
	{
		$schedule = $this->schedule->where('event_id', $id)->first();
		
		if (empty($schedule)) {
			
			// initialise new schedule
			$this->schedule->create([
				'event_id' => $id,
				'schedule' => $request->get('schedule'),
				'visible_website' => empty($request->get('visible_website')) ? 0 : 1,
				'visible_registration' => empty($request->get('visible_registration')) ? 0 : 1
			]);
		} else {
			
			$v_web = empty($request->get('visible_website')) ? 0 : 1;
			$v_reg = empty($request->get('visible_registration')) ? 0 : 1;
			
			// update existing schedule
			$schedule->fill([
				'schedule' => $request->get('schedule'),
				'visible_website' => $v_web,
				'visible_registration' => $v_reg
			])->save();
		}
		
		event(new DataWasManipulated('actionUpdate', $this->log_desc.'ID: '.$id));
		
		return redirect()->route('events.edit', $id);
	}
}
