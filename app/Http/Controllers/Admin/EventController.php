<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\User;
use App\Event;
use App\EventType;
use App\EventTab;
use App\TabNote;
use App\Sponsor;
use App\File;
use App\Venue;
use App\Region;
use App\EventFaq;
use App\EventFeedback;
use App\RegistrationStatus;
use App\EventScheduleSlotSession;
use App\Theme;
use App\EventGallery;
use App\Events\DataWasManipulated;

class EventController extends Controller {
	
	private $event;
	public $log_desc = "Event ";
	
	public function __construct(Event $event, EventType $event_type) {
		
		$this->event = $event;
		$this->event_type = $event_type;
	}

	/**
	 * Display a listing of the forthcoming events
	 *
	 * @return list of forthcoming events
	 */
	public function indexForthcoming()
	{
		$title = "Forthcoming events";
		$events = $this->event->with('venue', 'region')
					->where(function($q){
						$q->where('event_date_from', '>=', date('Y-m-d'));
						$q->orWhere('event_date_from', null);
					});
		
		// if role venue_staff, show just events that are in user assigned venues
		if(\Auth::user()->role_id == role('venue_staff')) {
			
			
			$assigned_venues_ids = Venue::where('venue_staff_id', \Auth::user()->id)->lists('id');
			$events = $events->whereIn('venue_id', $assigned_venues_ids);
		}
		
		// allow hostess events just she was assigned to
		if(\Auth::user()->role_id == role('host')) {
			
			$events = $events->whereHas('atendees', function($q) {
				$q->where('users.id', \Auth::user()->id);
			})->get();
		} else {
			
			$events = $events->get();
		}
		
		return view('admin.events.list')->with('title', $title)
			->with('events', $events);
	}
	
	/**
	 * Display a listing of the previous events
	 *
	 * @return list of previous events
	 */
	public function indexPrevious()
	{
		$title = "Previous events";
		
		$events = $this->event->with('venue', 'region')->where('event_date_from', '<', date('Y-m-d'));
		
		// if role venue_staff, show just events that are in user assigned venues
		if(\Auth::user()->role_id == role('venue_staff')) {
			
			$assigned_venues_ids = Venue::where('venue_staff_id', \Auth::user()->id)->lists('id');
			$events->whereIn('venue_id', $assigned_venues_ids);
		}
		
		// allow hostess events just she was assigned to
		if(\Auth::user()->role_id == role('host')) {
			
			$events = $events->whereHas('atendees', function($q) {
				$q->where('users.id', \Auth::user()->id);
			});
		}
		
		$events = $events->get();
		return view('admin.events.list')->with('title', $title)
										->with('events', $events);
	}

		/**
	 * Display a listing of the archive events
	 *
	 * @return list of previous events
	 */
	public function indexArchive()
	{
		
		$title = "Archive events";
		
		$events = Event::onlyTrashed();

	if(\Auth::user()->role_id == role('venue_staff')) {
			
			$assigned_venues_ids = Venue::where('venue_staff_id', \Auth::user()->id)->lists('id');
			$events->whereIn('venue_id', $assigned_venues_ids);
		}
		
		// allow hostess events just she was assigned to
		if(\Auth::user()->role_id == role('host')) {
			
			$events = $events->whereHas('atendees', function($q) {
				$q->where('users.id', \Auth::user()->id);
			});
		}
		
		$events = $events->get();
		return view('admin.events.archive')->with('title', $title)
										->with('events', $events);
	}
	



	/**
	 * Show the form for creating a new event.
	 *
	 * @return show event creation form
	 */
	public function create()
	{
		return view('admin.events.create')->with('event_types', $this->event_type->lists('title', 'id'));
	}

	/**
	 * Store new event
	 *
	 * @return redirect to event list
	 */
	public function store(Requests\CreateEventRequest $request)
	{
		$event = $this->event->create(['title' => $request->get('title'), 
			'browser_title' => $request->get('title'),
			'slug' => $this->generateSlug($request->get('title')),
			'event_type_id' => $request->get('event_type_id'),
			'event_date_from' => date("Y-m-d", strtotime("+1 month"))]);
		
		$event->tabs()->sync([1, 2, 3, 4, 5, 6, 7, 8, 9]);
		event(new DataWasManipulated('actionCreate', $this->log_desc.$event->title));
		
		return redirect()->route('events.edit', $event->id);
	}
	
	/**
	 * Create new event from previous event
	 * 
	 */
	public function copyEvent(Requests\CopyEventRequest $request)
	{

		$old_event = $this->event->find($request->get('old_event_id'));
		
		if (!empty($old_event)) {
			
			//old event found, copy the data
			$new_event = $old_event->replicate();
			$new_event->title = $request->get('title');
			$new_event->slug = $this->generateSlug($request->get('title'));
			$new_event->event_date_from = date("Y-m-d", strtotime("+1 month"));
			$new_event->publish = 0;
			$new_event->save();
			
			$new_event->tabs()->sync([1, 2, 3, 4, 5, 6, 7, 8, 9]);
			
			// replicate SPONSORS
			$sponsors = $old_event->sponsors->lists('id');
			if(!empty($sponsors)) $new_event->sponsors()->sync($sponsors);
			
			// replicate FAQS
			$faqs = array();
			foreach($old_event->eventFaqs as $f) {
				
				//var_dump($f->getAttributes());die;
                $faqs[] = new EventFaq(
                    ['question' => $f->question, 
                     'answer' => $f->answer,
                     'position' => $f->position,
                     'visibility' => $f->visibility
                     ]);
			}
			
		
			if(!empty($faqs)) $new_event->eventFaqs()->saveMany($faqs);
			
			// replicate FEEDBACK
//			$feedbacks = array();
//			foreach($old_event->feedbacks as $f) {
//				$feedbacks[] = new EventFeedback(['question' => $f->question, 'answer' => $f->answer, 'session_id' => $f->session_id, 'type' => $f->type]);
//			}
			
			//replicate cetral event files
			$files = $old_event->files->lists('id');
			$new_event->files()->sync($files);
			
			//if(!empty($feedbacks)) $new_event->feedbacks()->saveMany($feedbacks);
			
			event(new DataWasManipulated('actionCreate', $this->log_desc.$new_event->title));
		} else {
			
			return view('admin.events.create')->with('event_not_found', 'Event not found. Please select event name from the list!')
				->with('event_types', $this->event_type->lists('title', 'id'));
		}
		
		return redirect()->route('events.edit', $new_event->id);
	}


	/**
	 * Show the form for editing event
	 *
	 * @param  int  $id event id
	 * @return Response
	 */
	public function edit($id)
	{
		$event = $this->event->findOrFail($id);
		
		$tabs = EventTab::all();
		$event_tabs = $event->tabs()->lists('id');
		$site_tabs = array();
		
		foreach($tabs as $tab) {
		
			$checked = 0;
			
			if(in_array($tab->id, $event_tabs)) {
				$checked = 1;
			}
			
			$site_tabs[] = ['id'=>$tab->id, 'title' => $tab->title, 'checked' => $checked];
		}
		
		if(\Auth::user()->role_id > role('non_member')) {
			$event_materials = $event->eventMaterials()->where('role_visibility', 'LIKE', '%'.\Auth::user()->role_id.'%')->get();
		} else {
			$event_materials = $event->eventMaterials;
		}
		
		// merge event materials and assigned central files
		$event_materials = $event_materials->merge($event->files);
		
		$sessions = $event->scheduleSessions()->select('event_schedule_slot_sessions.id', \DB::raw('CONCAT_WS(" ", DATE_FORMAT(session_start,"%H:%i"), "-", DATE_FORMAT(session_end,"%H:%i"), event_schedule_slot_sessions.title) AS full_name'))->orderBy('session_start')->lists('full_name', 'id');
		
		return view('admin.events.edit')->with('event_types', $this->event_type->lists('title', 'id'))
			->with('site_tabs', $site_tabs)
			->with('event_materials', $event_materials)
			->with('event', $this->event->find($id))
			->with('sessions', $sessions)
			->with('sponsors', Sponsor::orderBy('name')->lists('name', 'id'))
			->with('regions', Region::orderBy('title')->lists('title', 'id'))
            ->with('themes', Theme::orderBy('title')->lists('title', 'id'))
			->with('files', File::orderBy('display_name')->lists('display_name', 'id'))
			->with('tfi_staff', User::select('id', \DB::raw('CONCAT(first_name, " ", last_name) AS full_name'))->whereIn('role_id', [role('venue_staff')])->orderBy('first_name')->lists('full_name', 'id'));
	}

	/**
	 * Update the eventin storage.
	 *
	 * @param  int  $id
	 * @return redirect to events list
	 */
	public function update(Requests\UpdateEventRequest $request, $id)
	{
		$event = $this->event->findOrFail($id);
		$event->title = $request->get('title');
        $event->theme_id = $request->get('theme_id');
		$event->description = $request->get('description');
		$event->registration_text = $request->get('registration_text');
		$event->capacity = $request->has('capacity') ? $request->get('capacity') : null;
		$event->price = $request->get('price');
		$event->price_nonmember = $request->get('price_nonmember');
		$event->payment_enabled = !empty($request->get('payment_enabled'))?$request->get('payment_enabled'):false;

		if($request->get('price') > 0.00 || $request->get('price_nonmember') > 0.00 && $request->get('payment_enabled') == 0) {
			$event->payment_enabled = 0;
		}
		if($request->get('price') > 0.00 || $request->get('price_nonmember') > 0.00 && $request->get('payment_enabled') == 1) {
			$event->payment_enabled = 1;
		}
		
		if($event->title != $request->get('title')) $event->slug = $this->generateSlug($request->get('title'));
		$event->region_id = $request->get('region_id');
		$event->county = $request->get('county');
		$event->event_type_id = $request->get('event_type_id');
		
		if ($request->has('event_date_from')) {
			$event->event_date_from = date("Y-m-d", strtotime(str_replace("/", "-", $request->get('event_date_from'))));
		}
		
		if ($request->has('event_date_to')) {
			$event->event_date_to = date("Y-m-d", strtotime(str_replace("/", "-", $request->get('event_date_to'))));
		}
		
		if ($request->has('cut_of_date')) {
			$event->cut_of_date = date("Y-m-d", strtotime(str_replace("/", "-", $request->get('cut_of_date'))));
		}

		if ($request->has('amendment_date')) {
			$event->amendment_date = date("Y-m-d", strtotime(str_replace("/", "-", $request->get('amendment_date'))));
		}
		
        if ($request->has('late_cancellation_date')) {
            $event->late_cancellation_date = date("Y-m-d H:i:s", strtotime(str_replace("/", "-", $request->get('late_cancellation_date'))));
		}
		
        if ($request->has('manually_cpd')) {
            $event->manually_cpd =  $request->get('manually_cpd');
        }

        $event->agenda_pdf = $request->get('agenda_pdf');
        if($request->file('agenda_pdf_upload')) {
            $saved_file = uploadFile($request->file('agenda_pdf_upload'), AGENDA);
            $event->agenda_pdf = $saved_file;
        }
        
		$event->notification_level 
		       = preg_replace("/[^0-9,.]/", "", $request->get('notification_level'));
		$event->browser_title = $request->get('browser_title');
		$event->outside_link = $request->get('outside_link');
		$event->tfi_contact_id 
		       = !empty($request->get('tfi_contact_id')) ? $request->get('tfi_contact_id') : null;
        $event->invite_only = !empty($request->get('invite_only'))?$request->get('invite_only'):false;
		$event->invitation_allowed = $request->get('invitation_allowed')?$request->get('invitation_allowed'):false;
		$event->registration_type_id = $request->get('registration_type_id');
		$event->hashtag = $request->get('hashtag');
		$event->publish      = !empty($request->get('publish'))?$request->get('publish'):false;
		$event->cpd_download = !empty($request->get('cpd_download'))?$request->get('cpd_download'):false;
		$event->app_enabled  = !empty($request->get('app_enabled'))?$request->get('app_enabled'):false;
		$event->local_committee  = !empty($request->get('local_committee'))?$request->get('local_committee'):false;

		$event->save();
		
		$active_tabs = $request->get('site_functions');
		
		if (!in_array(1, $active_tabs)) array_push($active_tabs, "1");
		$event->tabs()->sync($active_tabs);
		
		event(new DataWasManipulated('actionUpdate', $this->log_desc.$event->title));
		
		return redirect()->route('events.edit', $event->id);
	}
	
	/**
	 * Remove the event banner from storage.
	 *
	 * @param  int  $id event id
	 * @return Response
	 */
	public function deleteBanner($id) {
		
		$event = $this->event->find($id);
		
		removeFile($event->banner, IMAGE);
		$event->banner = "";
		$event->save();
		
		event(new DataWasManipulated('actionDelete', 'banner for'.$this->log_desc.$event->title));
		
		return redirect()->route('events.edit', $id);
	}
	
    /**
     * Remove the event banner from storage.
     *
     * @param  int  $id event id
     * @return Response
     */
    public function deleteAppBanner($id) {
        
        $event = $this->event->find($id);
        
        removeFile($event->app_banner, APP);
        $event->app_banner = "";
        $event->save();
        
        event(new DataWasManipulated('actionDelete', 'app banner for'.$this->log_desc.$event->title));
        
        return redirect()->route('events.edit', $id);
    }
    
	/**
	 * Generate unique slug for event by title
	 * @param string $tipTitle
	 * @return string generated unique slug
	 */
	private function generateSlug( $tipTitle ) {
		
		$slug = Str::slug( $tipTitle );
	    $slugs = $this->event->whereRaw("slug REGEXP '^{$slug}(-[0-9]*)?$'");
		
	    if ($slugs->count() === 0) {
	        
	        return $slug;
	    }
		
	    // Get the last matching slug
	    $lastSlug = $slugs->orderBy('slug', 'desc')->first()->slug;
		
	    // Strip the number off of the last slug, if any
	    $lastSlugNumber = intval(str_replace($slug . '-', '', $lastSlug));
		
	    // Increment/append the counter and return the slug we generated
	    return $slug . '-' . ($lastSlugNumber + 1);
	}
	
	/**
	 * Service to get list of events for autocomplete, by keywords
	 * @param string $term get param keyword
	 * @return list of previous events
	 */ 
	public function getEventsList(Request $request) {
		
		if ($request->ajax()) {
			
			$search = $request->get('term');
			$events = $this->event->where('title','like', '%'.$search.'%')->select('id', 'title')->get();
			
			return $events->toJson();
		} else {
			
			return null;
		}
	}
	
	/**
	 * Service to add tab notes
	 * @param int $id event id
	 * @return 1/0 if note successfully added
	 */
	public function addTabNote(Request $request, $id) {
		
		if ($request->ajax()) {
			
			TabNote::create([
				'note' => $request->get('note'),
				'event_id' => $id,
				'tab_id' => $request->get('tab_id'),
				'user_id' => \Auth::user()->id
			]);
			
			return 1;
		} else {
			
			return null;
		}
	}
	
	/** Upload banner file to server and update event banner in DB
	 * @return string uploaded file name
	 */
	public function uploadBanner(Request $request) {
		
		// upload banner if selected.
		if($request->file('myfile')) {
			
			$saved_file = uploadFile($request->file('myfile'), IMAGE);
			
			$event = $this->event->find($request->get('event_id'));
			$event->fill(['banner' => $saved_file])->save();
			
			event(new DataWasManipulated('actionCreate', 'banner for'.$this->log_desc.$event->title));
			
			return Response()->json($saved_file);
		}
		
		return null;
	}

    /**
     * archive event
     * @param $id
     */
	public function destroy($id)
    {
		$item = Event::findOrFail($id);
        if ($item->delete()) event(new DataWasManipulated('actionUpdate', 'archived event ID: '.$item->id));

        return redirect()->route('events.previous');
	}
	
	/**
     * archive event
     * @param $id
     */
	public function deleteEvent($id)
    {
		Event::withTrashed()->where('id', $id)->forceDelete();
        return redirect()->route('events.archived');
	}
	
		/**
     * Restore archive event
     * @param $id
     */
	public function restoreEvent($id)
    {
		Event::withTrashed()->where('id', $id)->restore();
        return redirect()->route('events.archived');
    }

    /**
     * @param $event_id
     * @param $gallery_id
     * @return \Illuminate\Http\RedirectResponse
     */

    public function deleteGallery($event_id, $gallery_id)
    {
        EventGallery::destroy($gallery_id);
        event(new DataWasManipulated('actionDelete', ' gallery image for event ID: '.$event_id));
        
        return redirect()->route('events.edit', $event_id);
    }
    
    public function uploadGallery(Request $request) {
        
        // upload banner if selected.
        if($request->file('myfile')) {
            
            $saved_file = uploadFile($request->file('myfile'), GALLERY);
            
            $event = $this->event->find($request->get('event_id'));
            
            $gallery = new EventGallery();
            $gallery->event_id = $event->id;
            $gallery->image = $saved_file;
            $gallery->save();
            
            event(new DataWasManipulated('actionCreate', 'gallery image for'.$this->log_desc.$event->title));
            
            return Response()->json($saved_file);
        }
        
        return null;
    }
    
    /** Upload banner file to server and update event banner in DB
     * @return string uploaded file name
     */
    public function uploadAppBanner(Request $request) {
        
        // upload banner if selected.
        if($request->file('myfile')) {
            
            $saved_file = uploadFile($request->file('myfile'), APP);
            
            $event = $this->event->find($request->get('event_id'));
            $event->fill(['app_banner' => $saved_file])->save();
            
            event(new DataWasManipulated('actionCreate', 'app banner for'.$this->log_desc.$event->title));
            
            return Response()->json($saved_file);
        }
        
        return null;
    }
	
	/**
	 * Detach venue from event
	 * @param int $event_id
	 */
	 public function detachVenue($event_id) {
	 	
		$event = $this->event->findOrFail($event_id);
		
		$event->venue_id = null;
		$event->save();
		
		event(new DataWasManipulated('actionDelete', 'venue from event '.$event->title));
		
		return redirect()->route('events.edit', $event_id);
	 }
	 
	/**
	* Attach central file to event
	*/
	public function attachFile(Requests\AttachEventFileRequest $request, $id)
	{
		$event = $this->event->findOrFail($id);
		$event->files()->attach($request->get('file_id'));
		
		event(new DataWasManipulated('actionCreate', ' central file. Assigned file ID: '. $request->get('file_id')));
		
		return redirect()->route('events.edit', $id);
	}

	/**
	 * Detach central file from event
	 */
	 public function detachFile($event_id, $file_id)
	 {
	 	$event = $this->event->findOrFail($event_id);
		$event->files()->detach($file_id);
		
		event(new DataWasManipulated('actionDelete', ' central file. Detached file ID: '. $file_id));
		
		return redirect()->route('events.edit', $event_id);
	 }
     
     public function uploadFloorPlan(Request $request)
     {
		// upload banner if selected.
        if($request->file('myfile')) {
			$saved_file = uploadFile($request->file('myfile'), FILE);
            
            $event = $this->event->find($request->get('event_id'));
            // return $saved_file;
            $event->fill(['floor_plan' => $saved_file])->save();
            
            event(new DataWasManipulated('actionCreate', 'app floor plan for'.$this->log_desc.$event->title));
            
            return Response()->json($saved_file);
        }
        
        return null;
     }
     
     public function removeFloorPlan($event_id) {
         
         $event = $this->event->find($event_id);
         $event->floor_plan = null;
         $event->save();
         
         return redirect()->route('events.edit', $event_id);
     }

}
