<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Region;
use App\Events\DataWasManipulated;

class RegionController extends Controller {
	
	private $region;
	public $log_desc = "Region ";
	
	public function __construct(Region $region) {
		
		$this->region = $region;
	}
	/**
	 * Display a listing of the region.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('admin.settings.regions.list')->with('regions', $this->region->all());
	}

	/**
	 * Show the form for creating a new region.
	 *
	 * @return show region create form
	 */
	public function create()
	{
		return view('admin.settings.regions.create');
	}

	/**
	 * Store a newly created region in storage.
	 *
	 * @return redirect to region list
	 */
	public function store(Requests\CreateRegionRequest $request)
	{
		$region = $this->region->create($request->all());
		event(new DataWasManipulated('actionCreate', $this->log_desc.$region->title));
		
		return redirect()->route('settings.regions');
	}

	/**
	 * Show the form for editing the specified region.
	 *
	 * @param  int  $id region id
	 * @return show region edit form
	 */
	public function edit($id)
	{
		return view('admin.settings.regions.edit')->with('region', $this->region->find($id));
	}

	/**
	 * Update the specified region in storage.
	 *
	 * @param  int  $id region id
	 * @return redirect to region list
	 */
	public function update(Requests\CreateRegionRequest $request, $id)
	{
		$region = $this->region->find($id);
		$region->fill($request->input())->save();
		event(new DataWasManipulated('actionUpdate', $this->log_desc.$region->title));
		
		return redirect()->route('settings.regions');
	}

	/**
	 * Remove the specified region from storage.
	 *
	 * @param  int  $id region id
	 * @return redirect to region list
	 */
	public function destroy($id)
	{
		$region =$this->region->find($id); 
		event(new DataWasManipulated('actionDelete', $this->log_desc.$region->title));
		$region->delete();
		
		return redirect()->route('settings.regions');
	}

}
