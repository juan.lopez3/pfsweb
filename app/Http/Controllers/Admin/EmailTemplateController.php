<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\EmailTemplate;
use App\EmailTemplateEvent;
use App\Events\DataWasManipulated;

class EmailTemplateController extends Controller {
	
	private $email_template;
	public $log_desc = "Email Template ";
	
	public function __construct(EmailTemplate $email_template) {
		
		$this->email_template = $email_template;
	}
	/**
	 * Display a listing of the email_template.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('admin.settings.emailTemplates.list')->with('email_templates', $this->email_template->all());
	}
	
	public function create()
	{
		return view('admin.settings.emailTemplates.create');
	}
	
	/**
	 * Show the form for editing the specified email template.
	 *
	 * @param  int  $id email template id
	 * @return show email template edit form
	 */
	public function edit($id)
	{
		return view('admin.settings.emailTemplates.edit')->with('email_template', $this->email_template->find($id))->with('tempalte_id', $id);
	}

	/**
	 * Update the specified email template in storage.
	 *
	 * @param  int  $id email template id
	 * @return redirect to email templates list
	 */
	public function update(Request $request, $id)
	{
		$email_template = $this->email_template->find($id);
		$email_template->fill($request->input())->save();
		event(new DataWasManipulated('actionUpdate', $this->log_desc.$email_template->type));
		
		return redirect()->route('settings.emailTemplates');
	}
	
	public function destroy($id) {
		$email_template = $this->email_template->find($id);

		if ($email_template->delete()) event(new DataWasManipulated('actionDelete', $this->log_desc.$email_template->title));
		
		return redirect()->route('settings.emailTemplates');
	}
    
    public function listArchive()
    {
        return view('admin.settings.emailTemplates.list_archive')->with('email_templates', $this->email_template->onlyTrashed()->get());
    }
    
	public function unArchive($id) {
		$email_template = $this->email_template->withTrashed()->find($id);
		$email_template->restore();
		
		return redirect()->route('settings.emailTemplates.listArchive');
	}

	public function store(Request $request)
	{   
		$data = $request->input();
		$data['reminder'] = intval($data['reminder'])?intval($data['reminder']):0;
	
		$email_template = $this->email_template->create($data);
		event(new DataWasManipulated('actionUpdate', $this->log_desc.$email_template->type));
		
		return redirect()->route('settings.emailTemplates');
	}
	
	/**
	 * Ajax service to load template content
	 */
	public function loadtemplate(Request $request)
	{
		if ($request->ajax()) {
			
            $custom_template = EmailTemplateEvent::where('event_id', $request->get('event_id'))->where('email_template_id', $request->get('template_id'))->first();
            
            if (sizeof($custom_template)) return $custom_template;
            
            $template_id = $request->get('template_id');
			$template = $this->email_template->find($template_id);
			$template = $template;
			
			return $template;
		} else {
			
			return null;
		}
	}
}
