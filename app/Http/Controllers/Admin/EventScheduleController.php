<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Event;
use App\EventAttendeeSlotSession;
use App\EventScheduleSlot;
use App\EventScheduleSlotSession;
use App\AttendeeType;
use App\SessionType;
use App\UserRoleSub;
use App\EventVenueRoom;
use App\Events\DataWasManipulated;
use App\EventScheduleSlotQuestion;
use App\ScheduleExplanationText;

class EventScheduleController extends Controller {
	
	private $event;
	private $schedule_slot;
	public $log_desc = "Event schedule ";
	
	public function __construct(Event $event, EventScheduleSlot $schedule_slot, EventScheduleSlotSession $slot_session) {
		
		$this->event = $event;
		$this->schedule_slot = $schedule_slot;
		$this->slot_session = $slot_session;
	}
	/**
	 * Display a listing of the event schedule.
	 *
	 * @param  int  $id  event id
	 * @return event schedule
	 */
	public function index($id)
	{
		$event = $this->event->find($id);
		$contributors = $event->atendeesByRoles(role('member'), role('non_member'));

		$contributors_select = array();
		
		foreach($contributors as $c) {
			
			if(sizeof($c->subRoles)) $contributors_select[$c->id] = $c->badge_name;
		}
		
		$slots = $event->scheduleTimeSlots()->orderBy('start')->get();
		$slots = $slots->groupBy('start');
        $contributor_types = UserRoleSub::lists('title', 'title');

        $date = $event->event_date_from;
        $event_days = array();
        $day_slots = array();
		$schedule_header = array();
	
		if (!$event->event_date_to || strtotime($event->event_date_to) < 0){
			$event->event_date_to = $event->event_date_from;
		}
		
        while (strtotime($date) <= strtotime($event->event_date_to)) {
            $day = date("Y-m-d", strtotime($date));
            $event_days[] = $day;
            $ds = $event->scheduleTimeSlots()->where('start_date', $day)->orderBy('start')->with('sessions.type', 'sessions.meetingSpace')->get();
            $day_slots[$day] = $ds;

            // take maximum columns slot and generate header
            if ($event->schedule_version == 1) {
                $widest_row = $ds->where('columns', $ds->max('columns'))->first();
                for ($i = 1; $i <= $ds->max('columns'); $i++) {
                    $session = $widest_row->sessions->where('order_column', $i)->first();
                    $schedule_header[$day][] = "";//sizeof($session) ? $session->meetingSpace->meeting_space_name : "";
                }
            }

            $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
        }

		return view('admin.events.schedules.list')->with('event', $event)
				->with('attendee_types', AttendeeType::all())
				->with('session_types', SessionType::lists('title', 'id'))
				->with('slots', $slots)
                ->with('day_slots', $day_slots)
                ->with('event_days', $event_days)
                ->with('schedule_header', $schedule_header)
				->with('meeting_room_spaces', EventVenueRoom::where('event_id', $event->id)->lists('meeting_space_name', 'id'))
				->with('contributors_select', $contributors_select)
                ->with('contributor_types', $contributor_types);
	}

	public function updateSessionsOrder(Request $request)
    {
        if($request->has('item')) {
            EventScheduleSlotSession::whereIn('id', $request->get('item'))->update(['order_column' => $request->get('order_column'), 'event_schedule_slot_id'=>$request->get('slot_id')]);
        }

        return response()->json(array('success' => true));
    }

	/**
	 * Store event schedule time slot
	 *
	 * @param  int $id event id
	 * @return redirect toevent schedule
	 */
	public function storeTimeSlot(Requests\CreateTimeSlotRequest $request, $id)
	{
		$event = $this->event->find($id);
		
		$time_slot = $this->schedule_slot->create([
			'title' => $request->get('title'),
			'event_id' => $id,
		    'meeting_space_id' => !empty($request->get('meeting_space_id'))?$request->get('meeting_space_id'):0,
            'start_date' => $request->get('start_date'),
			'start' => $request->get('start'),
			'end_date' => $request->get('end_date')?$request->get('end_date'):$request->get('start_date'),
			'end' => $request->get('end'),
			'show_start' => $request->has('show_start'),
            'show_end' => $request->has('show_end'),
			'bookable' => ($request->has('bookable')) ? 1 : 0,
			'stop_booking' => ($request->has('stop_booking')) ? 1 : 0,
			'available_chartered' => ($request->has('available_chartered')) ? 1 : 0,
			'notification_level' => $request->has('notification_level') ? intval($request->get('notification_level')) : 0,
			'slot_capacity' => $request->get('slot_capacity'),
            'columns' => $request->get('columns')
		]);
		
		if($request->has('attendee_types')) $time_slot->attendeeTypes()->sync($request->get('attendee_types'));
		
		if($request->has('questions')) {
			
			$questions = array();
			
			foreach($request->get('questions') as $key=>$q) {
				
				$questions[] = new EventScheduleSlotQuestion([
					'question' => $q,
					'type' => $request->get('types')[$key],
					'answers' => $request->get('answers')[$key],
					'available' => $request->get('available')[$key],
				]);
			}
			
			$time_slot->questions()->saveMany($questions);
		}
		
		event(new DataWasManipulated('actionCreate', $this->log_desc.'Time Slot created. START: '.$time_slot->start.' END: '.$time_slot->end.' Event: '.$event->title));
		
		return redirect()->route('events.scheduleBuilder', $id);
	}
	
	/**
	 * Show the form for editing the time slot resource.
	 *
	 * @param  int  $id event id
	 * @param  int  $slot_id time slot id
	 * @return redirect to event schedule
	 */
	public function editTimeSlot($id, $slot_id)
	{
		$event = $this->event->find($id);
		$time_slot = $this->schedule_slot->find($slot_id);
		$atendee_types = AttendeeType::all();
		$slot_atendee_types = $time_slot->attendeeTypes->lists('id');
		
		$at_checked = array();
		
		foreach($atendee_types as $at) {
				
			$at_checked[$at->id]['id'] = $at->id;
			$at_checked[$at->id]['title'] = $at->title;
			$at_checked[$at->id]['checked'] = (in_array($at->id, $slot_atendee_types)) ? 1 : 0;
		}
		
		return view('admin.events.schedules.editTimeSlot')
			->with('event', $event)
			->with('time_slot', $time_slot)
			->with('meeting_room_spaces', EventVenueRoom::where('event_id', $event->id)->lists('meeting_space_name', 'id'))
			->with('at_checked', $at_checked);
	}

	/**
	 * Update the specified time slot in storage.
	 *
	 * @param  int  $id event_id
	 * @param  int  $slot_id
	 * @return redirect to event schedule
	 */
	public function updateTimeSlot(Requests\CreateTimeSlotRequest $request, $id, $slot_id)
	{
		$event = $this->event->find($id);
		$time_slot = $this->schedule_slot->find($slot_id);
		
		$time_slot->event_id = $id;
		$time_slot->title = $request->get('title');
		if (!empty($request->get('meeting_space_id'))){
			$time_slot->meeting_space_id = $request->get('meeting_space_id');
		}
		
        $time_slot->start_date = $request->get('start_date');
		$time_slot->start = $request->get('start');
		$time_slot->end = $request->get('end');
        $time_slot->end_date = $request->get('end_date');
        $time_slot->show_start = $request->has('show_start');
        $time_slot->show_end = $request->has('show_end');
		$time_slot->bookable = ($request->has('bookable')) ? 1 : 0;
		$time_slot->stop_booking = ($request->has('stop_booking')) ? 1 : 0;
		$time_slot->available_chartered = ($request->has('available_chartered')) ? 1 : 0;
		$time_slot->notification_level = $request->has('notification_level') ? intval($request->get('notification_level')): 0;
		$time_slot->slot_capacity = $request->get('slot_capacity');
		$time_slot->columns = $request->get('columns');
		$time_slot->save();
		
        $attendee_types = !empty($request->get('attendee_types')) ? $request->get('attendee_types') : array();
		$time_slot->attendeeTypes()->sync($attendee_types);
		
		$time_slot->questions()->delete();
		
		if($request->has('questions')) {
			
			$questions = array();
			
			foreach($request->get('questions') as $key=>$q) {
				
				$questions[] = new EventScheduleSlotQuestion([
					'question' => $q,
					'type' => $request->get('types')[$key],
					'answers' => $request->get('answers')[$key],
					'available' => $request->get('available')[$key],
				]);
			}
			
			$time_slot->questions()->saveMany($questions);
		}
		
		event(new DataWasManipulated('actionUpdate', $this->log_desc.'Time Slot updated. START: '.$time_slot->start.' END: '.$time_slot->end.' Event: '.$event->title));
		
		return redirect()->route('events.scheduleBuilder', $id);
	}

	/**
	 * Remove the specified time slot from storage.
	 *
	 * @param  int  $id event id
	 * @param  int  @slot_id time slot_id
	 * @return redirect to event schedule
	 */
	public function destroyTimeSlot($id, $slot_id)
	{
		$event = $this->event->find($id);
		$this->schedule_slot->destroy($slot_id);
		event(new DataWasManipulated('actionDelete', $this->log_desc.'Time Slot deleted for event '.$event->title));
		
		return redirect()->route('events.scheduleBuilder', $id);
	}
	
	public function updateExplanationText(Request $request, $event_id)
    {
        $item = ScheduleExplanationText::firstOrNew(['event_id'=>$event_id, 'day'=>$request->get('day')]);
        $item->text = $request->get('text');
        $item->save();

        event(new DataWasManipulated('actionUpdate', $this->log_desc.'explanation text updated. Event ID: '.$event_id.' Day: '.$request->get('day')));

        return redirect()->route('events.scheduleBuilder', $event_id);
    }

	/**
	 * Store a newly created session in storage.
	 *
	 * @param  int $id event id
	 * @return redirect to event schedule
	 */
	public function storeSession(Requests\CreateSessionRequest $request, $id)
	{
		$event = $this->event->find($id);

		$session = $this->slot_session->create([
			'session_start' => $request->get('session_start'),
			'session_end' => $request->get('session_end'),
            'show_start' => ($request->has('show_start')) ? 1 : 0,
            'show_end' => ($request->has('show_end')) ? 1 : 0,
            'visible' => ($request->has('visible')) ? 1 : 0,
            'meeting_space_id' => $request->get('meeting_space_id'),
			'capacity' => $request->get('capacity'),
			'selectable' => $request->has('selectable'),
			'session_type_id' => $request->get('session_type_id'),
			'title' => $request->get('title'),
			'description' => $request->get('description'),
			'learning_objectives' => $request->get('learning_objectives'),
			'question' => $request->get('question'),
			'event_schedule_slot_id' => $request->get('event_schedule_slot_id'),
			'use_api_start' => ($request->has('use_api_start')) ? 1 : 0,
			'include_cpd_log' => ($request->has('include_cpd_log')) ? 1 : 0,
            'order_column' => $request->has('order_column') ? $request->get('order_column') : 1,
		]);
		
		$session_contributors = array();
		
		if ($request->has('contributor_id')) {
		
			for($i=0; $i < sizeof($request->get('contributor_id')); $i++) {
				
				// book contributor on slot
				// $request->get('event_schedule_slot_id')
				$event_attendee = $event->atendees()->where('user_id', $request->get('contributor_id')[$i])->first();
				
				EventAttendeeSlotSession::create([
					'event_attendee_id' => $event_attendee->pivot->id,
					'event_schedule_slot_id' => $request->get('event_schedule_slot_id'),
					'registration_status_id' => \Config::get('registrationstatus.booked')
				]);
				
				$session_contributors[$request->get('contributor_id')[$i]] = array(
																	'type' => $request->get('contributor_type')[$i],
																);
			}
		}
		
		$session->contributors()->sync($session_contributors);
		event(new DataWasManipulated('actionCreate', $this->log_desc.'Session"'.$session->title.'" was created for event '.$event->title));
		
		return redirect()->route('events.scheduleBuilder', $id);
	}
	
	/**
	 * Show edit page for sesion
	 *
	 * @param  int $id event id
	 * @return redirect to event schedule
	 */
	public function editSession($id, $session_id)
	{
		$event = $this->event->find($id);
		$contributors = $event->atendeesByRoles(role('member'), role('non_member'));
		$contributors_select = array();
		
		foreach($contributors as $c) {
				
			if(sizeof($c->subRoles)) $contributors_select[$c->id] = $c->badge_name;
		}
		
        $contributor_types = UserRoleSub::lists('title', 'title');

		return view('admin.events.schedules.editSession')
			->with('contributors_select', $contributors_select)
			->with('event', $event)
			->with('slot_session', $this->slot_session->find($session_id))
			->with('session_types', SessionType::lists('title', 'id'))
			->with('attendee_types', AttendeeType::all())
            ->with('meeting_room_spaces', EventVenueRoom::where('event_id', $event->id)->lists('meeting_space_name', 'id'))
            ->with('contributor_types', $contributor_types);
	}
	
	/**
	 * Update session in time slot
	 *
	 * @param  int $id event id
	 * @return redirect to event schedule
	 */
	public function updateSession(Requests\CreateSessionRequest $request, $id, $session_id)
	{
		$event = $this->event->find($id);
		
		$session = $this->slot_session->find($session_id);
		
		$session->session_start = $request->get('session_start');
		$session->session_end = $request->get('session_end');
		$session->session_type_id = $request->get('session_type_id');
        $session->capacity = $request->get('capacity');
		$session->selectable = $request->has('selectable');
        $session->meeting_space_id = $request->get('meeting_space_id');
		$session->title = $request->get('title');
		$session->description = $request->get('description');
		$session->learning_objectives = $request->get('learning_objectives');
		$session->question = $request->get('question');
		$session->event_schedule_slot_id = $request->get('event_schedule_slot_id');
		$session->show_start = ($request->has('show_start')) ? 1 : 0;
		$session->show_end = ($request->has('show_end')) ? 1 : 0;
		$session->visible = ($request->has('visible')) ? 1 : 0;
        $session->use_api_start = ($request->has('use_api_start')) ? 1 : 0;
		$session->include_cpd_log = ($request->has('include_cpd_log')) ? 1 : 0;
        $session->order_column = $request->has('order_column') ? $request->get('order_column') : 1;
		$session->save();
		
		$session_contributors = array();
		
		if ($request->has('contributor_id')) {
		
			for($i=0; $i < sizeof($request->get('contributor_id')); $i++) {
				
				$event_attendee = $event->atendees()->where('user_id', $request->get('contributor_id')[$i])->first();
				$booked = EventAttendeeSlotSession::where('event_schedule_slot_id', $request->get('event_schedule_slot_id'))->where('event_attendee_id', $event_attendee->pivot->id)->count();
				
				// if not booked - book contributor for slot
				if(!$booked) {
					EventAttendeeSlotSession::create([
						'event_attendee_id' => $event_attendee->pivot->id,
						'event_schedule_slot_id' => $request->get('event_schedule_slot_id'),
						'registration_status_id' => \Config::get('registrationstatus.booked')
					]);
				}
				$session_contributors[$request->get('contributor_id')[$i]] = array(
																	'type' => $request->get('contributor_type')[$i],
																); 
			}
		}
		
		$session->contributors()->sync($session_contributors);
		event(new DataWasManipulated('actionUpdate', $this->log_desc.'Session"'.$session->title.'" was updated for event '.$event->title));
		
		return redirect()->route('events.scheduleBuilder', $id);
	}
	
	/**
	 * Destroy session in time slot
	 *
	 * @param  int $id session id
	 * @return redirect to event schedule
	 */
	public function destroySession($id, $session_id)
	{
		$event = $this->event->find($id);
		$this->slot_session->destroy($session_id);
		event(new DataWasManipulated('actionDelete', $this->log_desc.'slot session deleted for event '.$event->title));
		
		return redirect()->route('events.scheduleBuilder', $id);
	}
	
	/**
	 * Service to update schedule event type visibility.
	 * @param  int  $id  event id
	 */
	public function updateVisibility(Request $request, $id)
	{
		if ($request->ajax()) {
			
			$event = $this->event->find($id);
			$event->main_schedule_visible_web = ($request->get('visible_on_web') == 'true') ? 1 : 0;
			$event->main_schedule_visible_reg = ($request->get('visible_on_reg') == 'true') ? 1 : 0;
			$event->mandatory_slots = ($request->get('mandatory_slots') == 'true') ? 1 : 0;
			$event->save();
			
			return 1;
		}
	}

	/**
	 * Service to update schedule event type visibility on app.
	 * @param  int  $id  event id
	 */
	public function updateVisibilityApp(Request $request, $id)
	{
		if ($request->ajax()) {
			
			$event = $this->event->find($id);
			$event->main_schedule_visible_app = ($request->get('visible_on_app') == 'true') ? 1 : 0;
			$event->save();
			
			return 1;
		}
	}
	
	/**
	 * Copy schedule from previous event
	 * copy contributors for each session
	 */
	public function copySchedule(Requests\CopySchedule $request, $event_id)
	{
		$event = $this->event->findOrFail($event_id);
		$old_event = $this->event->findOrFail($request->get('event_name_id'));
		
		if(sizeof($event) && sizeof($old_event)){
			
			// clear schedule before copy
			EventScheduleSlot::where('event_id', $event->id)->delete();
			
			// save each slot with sessions inside
			foreach($old_event->scheduleTimeSlots as $slot) {
				
				$new_slot = $slot->replicate();
				$new_slot->meeting_space_id = null;
				$new_slot->event_id = $event_id;

				//update timeslot date to match new event 
				$new_slot->start_date = $event->event_date_from;
				
				$new_slot->save();
				

				$new_slot->attendeeTypes()->sync($slot->attendeeTypes()->lists('id'));
				
				foreach($slot->sessions as $session) {
					
					$new_session = $session->replicate();
					$new_session->event_schedule_slot_id = $new_slot->id;
					$new_session->save();
					
					// copy session contributors
					$session_contributors = array();
					foreach ($session->contributors as $contr) {
						
						//check if contributors exists in event_attendees table if no insert
						if ($event->atendees()->where('user_id', $contr->id)->count() == 0) {
							$event->atendees()->attach($contr->id);
						}
						
						$event_attendee = $event->atendees()->where('user_id', $contr->id)->first();
						$booked = EventAttendeeSlotSession::where('event_schedule_slot_id', $new_slot->id)->where('event_attendee_id', $event_attendee->pivot->id)->count();
						
						// if not booked - book contributor for slot
						if(!$booked) {
							EventAttendeeSlotSession::create([
								'event_attendee_id' => $event_attendee->pivot->id,
								'event_schedule_slot_id' => $new_slot->id,
								'registration_status_id' => \Config::get('registrationstatus.booked')
							]);
						}
						
						$session_contributors[$contr->id] = array(
							'type' => $contr->pivot->type,
						); 
					}
					
					$new_session->contributors()->sync($session_contributors);
				}
			}
			
			event(new DataWasManipulated('actionDelete', $this->log_desc.' for event '.$event->title));
			event(new DataWasManipulated('actionCreate', $this->log_desc.'new schedule for event '.$event->title.', from event '.$old_event->title.' schedule.'));
		}
		return redirect()->route('events.scheduleBuilder', $event_id);
	}

	public function updateScheduleVersion(Request $request, $event_id)
    {
        $event = Event::findOrFail($event_id);
        $event->schedule_version = $request->get('schedule_version');
        $event->save();
        event(new DataWasManipulated('actionUpdate', $this->log_desc.' changed schedule version for event ID: '.$event->id));

        return response()->json([], 200);
    }

    public function generateCPDLog($event_id)
    {
        $event = Event::find($event_id);
        
        $slots = $event->scheduleTimeSlots()->orderBy('start')->with(['sessions' => function($query){
                $query->orderBy('session_start');
                $query->with('contributors', 'type');
            }])->get();
        
        $pdf = \PDF::loadView('admin.events.schedules.cpd_form', ['event'=>$event, 'slots' => $slots]);
        
        return $pdf->download($event->title." CPD Log.pdf");
    }

}
