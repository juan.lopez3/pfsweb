<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Technology;
use App\Events\DataWasManipulated;

class TechnologyController extends Controller {
	
	private $technology;
	public $log_desc = "Technology ";
	
	public function __construct(Technology $technology) {
		
		$this->technology = $technology;
	}
	/**
	 * Display a listing of the technology.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('admin.settings.technologies.list')->with('technologies', $this->technology->all());
	}

	/**
	 * Show the form for creating a new technology.
	 *
	 * @return show technology create form
	 */
	public function create()
	{
		return view('admin.settings.technologies.create');
	}

	/**
	 * Store a newly created technology in storage.
	 *
	 * @return redirect to technology list
	 */
	public function store(Requests\CreateTechniqueRequest $request)
	{
		$technology = $this->technology->create($request->all());
		event(new DataWasManipulated('actionCreate', $this->log_desc.$technology->title));
		
		return redirect()->route('settings.technologies');
	}

	/**
	 * Show the form for editing the specified technology.
	 *
	 * @param  int  $id technology id
	 * @return show technology edit form
	 */
	public function edit($id)
	{
		return view('admin.settings.technologies.edit')->with('technology', $this->technology->find($id));
	}

	/**
	 * Update the specified technology in storage.
	 *
	 * @param  int  $id technology id
	 * @return redirect to technology list
	 */
	public function update(Requests\CreateTechniqueRequest $request, $id)
	{
		$technology = $this->technology->find($id);
		$technology->fill($request->input())->save();
		event(new DataWasManipulated('actionUpdate', $this->log_desc.$technology->title));
		
		return redirect()->route('settings.technologies');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id technology id
	 * @return redirect to technology list
	 */
	public function destroy($id)
	{
		$technology =$this->technology->find($id); 
		event(new DataWasManipulated('actionDelete', $this->log_desc.$technology->title));
		$technology->delete();
		
		return redirect()->route('settings.technologies');
	}

}
