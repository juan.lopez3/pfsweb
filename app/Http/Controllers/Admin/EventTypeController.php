<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\EventType;
use App\EmailTemplate;
use App\File;
use App\Events\DataWasManipulated;

use App\EmailAddress;

class EventTypeController extends Controller {
	
	private $event_type;
	public $log_desc = "Event Type ";
	
	public function __construct(EventType $event_type) {
		
		$this->event_type = $event_type;
	}
	/**
	 * Display a listing of the event type.
	 *
	 * @return show event types list
	 */
	public function index()
	{
		return view('admin.settings.eventTypes.list')->with('event_types', $this->event_type->all())
			->with('email_templates', EmailTemplate::lists('type', 'id'))
			->with('email_addresses', EmailAddress::lists('email', 'id'))
            ->with('files', File::lists('display_name', 'id'));
	}

	/**
	 * Show the form for creating a new event type.
	 *
	 * @return show event type form
	 */
	public function create()
	{
		return view('admin.settings.eventTypes.create')
			->with('email_templates', EmailTemplate::lists('type', 'id'))
			->with('email_addresses', EmailAddress::lists('email', 'id'))
            ->with('files', File::lists('display_name', 'id'));
	}

	/**
	 * Store a newly created event type in storage.
	 *
	 * @return redirect to event type list
	 */
	public function store(Requests\CreateEventTypeRequest $request)
	{
		$event_type = $this->event_type->create($request->all());
		$event_type->emailTemplates()->attach($request->get('type_template'));
		
		event(new DataWasManipulated('actionCreate', $this->log_desc.$event_type->title));
		
		return redirect()->route('settings.eventTypes');
	}

	/**
	 * Show the form for editing the specified event type.
	 *
	 * @param  int  $id event type id
	 * @return show event type edit form
	 */
	public function edit($id)
	{
		return view('admin.settings.eventTypes.edit')->with('event_type', $this->event_type->find($id))
			->with('email_templates', EmailTemplate::lists('type', 'id'))
			->with('email_addresses', EmailAddress::lists('email', 'id'))
            ->with('files', File::lists('display_name', 'id'));
	}

	/**
	 * Update the specified event type in storage.
	 *
	 * @param  int  $id event type id
	 * @return redirect event type list
	 */
	public function update(Requests\CreateEventTypeRequest $request, $id)
	{
		$event_type = $this->event_type->find($id);
		
		if (!empty($event_type->branding_image) && ($event_type->branding_image != $request->get('branding_image'))) removeFile($event_type->branding_image, BRANDING);
		
		$selected_templates = $request->has('type_template') ? $request->get('type_template') : array();
        
		$event_type->fill($request->input());
		$event_type->exhibition = $request->has('exhibition') ? 1 : 0;
        $event_type->app_sponsors_active = $request->has('app_sponsors_active') ? 1 : 0;
        $event_type->app_additional_active = $request->has('app_additional_active') ? 1 : 0;
		$event_type->save();
		$event_type->emailTemplates()->sync($selected_templates);
		event(new DataWasManipulated('actionUpdate', $this->log_desc.$event_type->title));
		
		return redirect()->route('settings.eventTypes');
	}

	/**
	 * Remove the specified event type from storage.
	 *
	 * @param  int  $id event type id
	 * @return redirect to event type list
	 */
	public function destroy($id)
	{
		$event_type = $this->event_type->find($id);
		event(new DataWasManipulated('actionDelete', $this->log_desc.$event_type->title));
		$event_type->delete();
		
		return redirect()->route('settings.eventTypes');
	}
	
	/**
	 * Service to upload branding image
	 * 
	 * @return  string saved file name
	 */
	public function uploadBranding(Request $request) {
		
		// upload event type branding image
		if($request->file('myfile')) {
			
			$saved_file = uploadFile($request->file('myfile'), BRANDING);
			
			return response()->json($saved_file);
		}
		
		return null;
	}
    
    /**
     * Service to upload email branding
     * 
     * @return  string saved file name
     */
    public function uploadEmailBranding(Request $request) {
        
        // upload event type branding image
        if($request->file('myfile')) {
            
            $saved_file = uploadFile($request->file('myfile'), BRANDING);
            
            return response()->json($saved_file);
        }
        
        return null;
    }
    
    /**
     * Service to upload email branding
     * 
     * @return  string saved file name
     */
    public function uploadAppBranding(Request $request) {
        
        // upload event type branding image
        if($request->file('myfile')) {
            
            $saved_file = uploadFile($request->file('myfile'), APP);
            
            return response()->json($saved_file);
        }
        
        return null;
    }
    
    /**
     * attach file from the central file
     */
    public function attachFile(Request $request, $event_type)
    {
        $event_type = EventType::findOrFail($event_type);
        $event_type->files()->attach($request->get('file_id'));
        
        return redirect()->route('settings.eventTypes.edit', $event_type);
    }
    
    public function detachFile($event_type, $file_id)
    {
        $event_type = EventType::findOrFail($event_type);
        $event_type->files()->detach($file_id);
        
        return redirect()->route('settings.eventTypes.edit', $event_type);
    }
}
