<?php namespace App\Http\Controllers;

use App\Event;
use App\Http\Controllers\Controller;
use App\SessionType;
use App\Sponsor;
use App\SponsorSetting;
use File;

class AgendaController extends Controller
{
    /**
     * generate a downloadable document in PDF or DOCX of event agenda
     *
     * @param string $slug event slug title to load the event
     * @param string $format PDF or DOCX default PDF
     * @return document directly download document via http
     */
    public function generateAgenda($slug, $format = "pdf")
    {

        $event = Event::where('slug', $slug)->first();

        $filePath = \Cache::get('agenda.' . $slug . '_' . $format);
	
        //Find the file in cache and if the file is in cache, else
        if ($filePath) {
            // Verificar si el archivo existe
            // Si no existe se borra el key del cache
            if (!File::exists($filePath)) {
                \Cache::forget('agenda.' . $slug);
            }
        } 

        // check if agenda download is enabled for the event
        if (!$event->cpd_download) {
            return redirect()->route('events.view', $slug);
        }

        if ($format == "pdf" || $format == "pdf2") {

            //$agenda = \Cache::remember('agenda.' . $slug . '_' . $format, 30, function () use ($event, $format) {
        if ($event->id == 442 )
            $sponsors = Sponsor::where('order', '>', 0)
                                ->orderBy('order', 'ASC')
                                ->limit(20)->get();
        else
            $sponsors = Sponsor::all();

            $quarterly_sponsors = [];

            if ($event->eventType->has_sponsor) {

                $today = $event->event_date_from;
                $quarterly_sponsors = SponsorSetting::where('date_from', '<=', $today)->where('date_to', '>=', $today)->get();
            }

            $sessions = $event->scheduleSessions()->get();
            $cpd_time = $this->calculateCPD($sessions);
            $h = $cpd_time['hours'];
            if(isset($cpd_time['min'])){ 
                $min = $cpd_time['min'] > 0 ? $cpd_time['min']:"00";
            } else {
                $min = "00";
            }
            $cpd_time = $h.':'.$min;

            if ($format == "pdf") {
                $pdf = \PDF::loadView('events.agenda',
                    ['event' => $event, 'sponsors' => $sponsors,
                        'quarterly_sponsors' => $quarterly_sponsors, 'cpd_time' => $cpd_time, 'format' => $format]);
                $pdf->setPaper('a4', 'landscape');
                $pdf->save("agendas/" . $event->title . " Agenda.pdf");

                $agenda = "agendas/" . $event->title . " Agenda.pdf";
            }
            if ($format == "pdf2") {

                $pdf = \PDF::loadView('events.agenda_old',
                    ['event' => $event, 'sponsors' => $sponsors,
                        'quarterly_sponsors' => $quarterly_sponsors, 'cpd_time' => $cpd_time, 'format' => $format]);
                $pdf->save("agendas/" . $event->title . " Agenda.pdf");

                $agenda = "agendas/" . $event->title . " Agenda.pdf";
            }
            //});
        } else {

            //Generating DOCx using PHPWORD LIBRARY http://phpword.readthedocs.io/en/latest/
            $sponsors = Sponsor::all();

            $quarterly_sponsors = [];

            if ($event->eventType->has_sponsor) {

                $today = $event->event_date_from;
                $quarterly_sponsors = SponsorSetting::where('date_from', '<=', $today)->where('date_to', '>=', $today)->get();
            }

            $sessions = $event->scheduleSessions()->get();
            $cpd_time = $this->calculateCPD($sessions);

            $phpWord = new \PhpOffice\PhpWord\PhpWord();

            $section = $phpWord->addSection();
            //\PhpOffice\PhpWord\Shared\Html::addHtml($section, $contents, true, true);

            $tableName = 'tabla_0';
            $fancyTableStyle = array('borderSize' => 0, 'borderColor' => 'ffffff', 'cellMargin' => 180, 'alignment' => \PhpOffice\PhpWord\SimpleType\JcTable::CENTER, 'cellSpacing' => 500);
            $borderTableStyle = array('borderSize' => 0, 'borderColor' => 'ffffff');
            $fancyTableFirstRowStyle = array('borderBottomSize' => 18, 'borderBottomColor' => 'fffffff', 'bgColor' => 'fffffff');
            $fancyTableCellStyle = array('valign' => 'center');
            $fancyTableCellBtlrStyle = array('valign' => 'center', 'textDirection' => \PhpOffice\PhpWord\Style\Cell::TEXT_DIR_BTLR);
            $fancyTableFontStyle = array('bold' => true);
            $phpWord->addTableStyle($tableName, $fancyTableStyle, $fancyTableFirstRowStyle);

            $table = $section->addTable($tableName);
            /**-------------------------------------------------
             * Header information
             * -------------------------------------------------
             */
            $table->addRow(1000, $borderTableStyle);
            $cell = $table->addCell(2250, $borderTableStyle);
            $cell->addImage(
                public_path() . '/images/twitterpfs.png',
                array(
                    'width' => 200,
                    // 'height' => 60,
                    'marginTop' => -1,
                    'marginLeft' => -1,
                    'wrappingStyle' => 'behind',
                    'borderSize' => 0,
                    'borderColor' => 'ffffff',
                )
            );
            $cell->getStyle()->setGridSpan(8);
            $table->addCell(2000, $borderTableStyle)->addImage(
                public_path() . '/images/blank.png',
                array(
                    'width' => 50,
                    'height' => 40,
                    'marginTop' => -1,
                    'marginLeft' => -1,
                    'wrappingStyle' => 'behind',
                    'borderSize' => 0,
                    'borderColor' => 'ffffff',
                )
            );
            $cell = $table->addCell(3000, $borderTableStyle);
            $cell->addImage(
                public_path() . '/images/pfs.png',
                array(
                    'width' => 200,
                    // 'height' => 40,
                    'marginTop' => -1,
                    'marginLeft' => -1,
                    'wrappingStyle' => 'behind',
                    'borderSize' => 0,
                    'borderColor' => 'ffffff',
                )
            );
            $cell->getStyle()->setGridSpan(8);
            $table->addRow(500, $borderTableStyle);
            $cell = $table->addCell(9000, $borderTableStyle);
            $content = '<h3 style="font-weight:bold;margin-bottom:40px;font-size:14px;">Agenda ' . $event->title . '</h3>';
            $cell->getStyle()->setGridSpan(12);
            \PhpOffice\PhpWord\Shared\Html::addHtml($cell, $content);

            /**------------------------------------------------------
             * Venue and date information, It's located in the header
             * ------------------------------------------------------
             */

            $table->addRow(200, $borderTableStyle);
            $cell = $table->addCell(4000);
            $cell->addText("Venue:");
            $cell->getStyle()->setGridSpan(6);
            $content = "{$event->venue->name}, {$event->venue->address}, {$event->venue->city}";
            if (!empty($event->venue->country)) {
                $content .= ", {$event->venue->country}";
            }
            $content .= " {$event->venue->postcode}";
            //Second Cell
            $cell = $table->addCell();
            $cell->getStyle()->setGridSpan(10);
            \PhpOffice\PhpWord\Shared\Html::addHtml($cell, $content);

            $table->addRow(200, $borderTableStyle);
            $cell = $table->addCell(2000);
            $cell->addText("Date:");
            $cell->getStyle()->setGridSpan(6);
            $content = date('l d M Y', strtotime($event->event_date_from));
            if ($event->event_date_from != $event->event_date_to) {
                $content .= "-" . date('d M Y', strtotime($event->event_date_to));
            }
            //Second Cell
            $cell = $table->addCell();
            $cell->getStyle()->setGridSpan(4);
            \PhpOffice\PhpWord\Shared\Html::addHtml($cell, $content);

            $table->addRow(200, $borderTableStyle);
            $cell = $table->addCell(9000, $borderTableStyle);
            $content = 'In association with our Partners in Professionalism:';
            $cell->getStyle()->setGridSpan(10);
            \PhpOffice\PhpWord\Shared\Html::addHtml($cell, $content);

            $table->addRow(500, $borderTableStyle);

            foreach ($event->sponsors()->where('type', 0)->get() as $index => $sponsor) {
                if (!empty($sponsor->logo)) {
                    if ($index == 4) {
                        $table->addRow(500);
                    }
                    $cell = $table->addCell(2250);
                    $cell->addImage(
                        public_path() . '/uploads/sponsors/' . $sponsor->logo,
                        array(
                            'width' => 100,
                            // 'height' => 30,
                            'marginTop' => -1,
                            'marginLeft' => -1,
                            // 'wrappingStyle' => 'behind',
                        )
                    );
                    $cell->getStyle()->setGridSpan(4);

                } else {
                    $sponsor->name;
                }
            }

            /**---------------------------------------------------------
             * @START Calendar printing
             * ---------------------------------------------------------
             */

            $previus_session = false;
            foreach ($event->scheduleSessions()->with('contributors')->with('slot')->orderBy('start_date')->orderBy('session_start')->get() as $session) {
                //  return $event->scheduleSessions()->with('contributors')->with('slot')->orderBy('start_date')->orderBy('session_start')->get();
                if (!($previus_session) || $previus_session->slot->start_date != $session->slot->start_date) {
                    //@START DATE TITLE TABLE
                    $styles = array(
                        'bold' => true,
                        'bgColor' => 'f3eeee',
                        'alignment' => 'center',
                        'align' => 'center',
                        'textAlignment' => 'center',
                        'valign' => 'center',
                        'borderSize' => 0,
                        'borderColor' => 'ffffff',

                    );
                    $noBorder = array(
                        'borderSize' => 0,
                        'borderColor' => 'ffffff',

                    );
                    $table->addRow(200, $borderTableStyle);
                    $cell = $table->addCell(9000, $styles);
                    $cell->addText(date("j-M-Y", strtotime($session->slot->start_date)), $styles);
                    $cell->getStyle()->setGridSpan(13);
                }
                //@END DATE TITLE TABLE

                // //@START HOURS TABLE
                $table->addRow(200, $borderTableStyle);
                $cell = $table->addCell(4000, $noBorder);
                $cell->addText(date("H:i", strtotime($session->session_start)) . " - " . date("H:i", strtotime($session->session_end)), ['color' => '#482488', 'borderSize' => 0]);
                $cell->getStyle()->setGridSpan(6);
                //@END HOURS TABLE

                //@START TITLE EVENT TABLE
                $cell = $table->addCell(4000, $noBorder);
                $content = "<b>" . $session->title . "</b>";
                $content = str_replace(' & ', ' and ', $content);
                $content = str_replace(' &amp; ', ' and ', $content);
                $cell->getStyle()->setGridSpan(13);
                \PhpOffice\PhpWord\Shared\Html::addHtml($cell, $content);
                // //@END TITLE EVENT TABLE

                // //@START OBJTIVES EVENT TABLE
                if (!empty($session->learning_objectives)) {
                    $content = $session->learning_objectives;
                    $content = str_replace(' & ', ' and ', $content);
                    $content = str_replace(' &amp; ', ' and ', $content);
                    \PhpOffice\PhpWord\Shared\Html::addHtml($cell, $content);
                }
                //@END OBJTIVES EVENT TABLE

                // //@START CONTRIBUTORS EVENT TABLE
                if (count($session->contributors)) {
                    foreach ($session->contributors as $contr) {
                        $content = $contr->badge_name;
                        $content = $contr->company;
                        $content = $contr->job_role;
                    }
                    $content = str_replace(' & ', ' and ', $content);
                    $content = str_replace(' &amp; ', ' and ', $content);
                    \PhpOffice\PhpWord\Shared\Html::addHtml($cell, $content);
                }
                //@END CONTRIBUTORS EVENT TABLE

                $previus_session = $session;

            }
            /**---------------------------------------------------------
             * @END Calendar printing
             * ---------------------------------------------------------
             */

            /**----------------------------------------------------------------
             *Footer information
             *-----------------------------------------------------------------
             */
            $section->addText('');
            $header = array(
                // 'size' => 16, 'bold' => true
            );
            $section->addText(htmlspecialchars('Total CPD provided: ' . $cpd_time["hours"] . 'h' . $cpd_time["min"] . 'min'), $header);
            $section->addText('');
            $section->addText(htmlspecialchars("The content in each session has been carefully selected and can be considered for both structured and unstructured CPD hours, depending how this activity addressed each individual's personal development needs."), $header);
            $section->addText('');
            $section->addText(htmlspecialchars("<strong>Structured CPD</strong> is the undertaking of any formal learning activity designed to meet a specific learning outcome (this is what an individual is expected to know, understand or do as a result of his or her learning)."), $header);
            $section->addText('');
            $section->addText(htmlspecialchars("<strong>Unstructured CPD</strong> is any activity an individual considers has met a learning outcome, but which may not have been specifically designed to meet their development needs."), $header);
            $section->addText('');

            $section->addImage(
                public_path() . '/images/footerpfs.png',
                array(
                    'width' => 400,
                    'marginTop' => 1000,
                    'marginLeft' => -1,
                    'wrappingStyle' => 'behind',
                )
            );

            //-----------------------------------------------
            //add scheduleSessions table

            $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
            $objWriter->save($event->title . '.docx');

            //$file = "agendas/Lancashire, Merseyside and Cumbria Regional Conference Q3 2018 Agenda.docx";
            $file = $event->title . ".docx";
            return response()->download(public_path($file));
            //return response()->download($file, $name);

        }

        return redirect($agenda);

    }

    /**
     * Calculate sessions CPD hours
     */
    private function calculateCPD($sessions)
    {

        // get session types which are used in CPD hours calculation
        $cpd_session_types = SessionType::where('include_in_cpd', 1)->lists('id');

        $hours = array();
        $duration = 0;

        foreach ($sessions as $s) {

            if (in_array($s->session_type_id, $cpd_session_types)) {

                $duration += (strtotime($s->session_end) - strtotime($s->session_start)) / 60;
            }
        }

        $minutes = $duration % 60;
        $hours['min'] = $minutes;

        $h = ($duration - $minutes) / 60;
        $hours['hours'] = $h;

        return $hours;
    }
}
