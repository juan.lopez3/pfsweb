<?
if ($event->schedule_version == 1) {
                // for new version need to check only if checked in for session
                $registration = EventAttendee::where('user_id', $user->id)->where('event_id', $event->id)->first();

                foreach ($registration->attendedSessions()->distinct()->get() as $session) {
                    if(in_array($session->session_type_id, $sessions_types_cpd)) {

                        if (strtotime($session->session_end) - strtotime($session->session_start) > 0) {
                            $session_duration = (strtotime($session->session_end) - strtotime($session->session_start))/60;
                            $session_duration = round($session_duration);
                            $sessions_duration += $session_duration;

                            $sessions_text .= '
                            <tr>
                                <td width="75%">'.$session->title.'</td>
                                <td width="25%" align="right"><b>'.$session_duration.' minutes</b></td>
                            </tr>
                            ';
                        }
                    }
                }
            } 