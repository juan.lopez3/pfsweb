<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\User;
use App\UserFeedback;
use App\UserDocument;
use App\File;
use App\Event;

use Validator;
use App\Http\Controllers\Controller;
use JWTAuth;

class UserController extends Controller {

	/**
     * Get user data
     * 
     * @param int $user_id
     * @return json user profile details
     */
    public function profile($user_id)
    {
        try {
            $user = User::findOrFail($user_id);
            
            $data = array(
                'id' => $user->id,
                'name' => $user->first_name,
                'lastname' => $user->last_name,
                'badge_name' => $user->badge_name,
                'email' => $user->email,
                'cc_email' => $user->cc_email,
                'dietary_requirement_id' => $user->dietary_requirement_id,
                'dietary_requirement_other' => $user->dietary_requirement_other,
                'special_requirements' => $user->special_requirements,
                'postcode' => $user->postcode,
                'company' => $user->company,
                'title' => $user->title,
                'mobilePhone' => $user->mobile,
                'contactPhone' => $user->phone,
                'attendee_types' => $user->attendeeTypes->lists('id')
            );
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    /**
     * Get user data
     * 
     * @param int $user_id
     * @return json user profile details
     */
    public function events($user_id)
    {
        try {
            $user = User::with('events.region', 'feedbackAnswers')->findOrFail($user_id);
            $data = array();
            
            foreach ($user->events()->orderBy('event_date_from')->with('region')->get() as $event) {
                
                $data[] = array(
                    'id' => $event->id,
                    'name' => $event->title,
                    'exhibition' => $event->eventType->exhibition,
                    'location' => sizeof($event->region) ? $event->region->title : '',
                    'image' => !empty($event->app_banner) ? config('app.url').'uploads/app/'.$event->app_banner: '',
                    'weburl' => route('events.view', $event->slug),
                    'eventtype' => array(
                        'id' => $event->event_type_id,
                         'name' => $event->eventType->title,
                         'appDescription' => $event->eventType->app_description,
                         'color' => $event->eventType->app_color, 
                         'app_branding' => !empty($event->eventType->app_branding) ? config('app.url').'uploads/app/'.$event->eventType->app_branding : '',
                         'icon' => !empty($event->eventType->app_icon) ? config('app.url').'uploads/app/'.$event->eventType->app_icon : '',
                         'promotionalBanner' => !empty($event->eventType->app_promotional_banner) ? config('app.url').'uploads/app/'.$event->eventType->app_promotional_banner : '', 
                    ),
                    'date' => $event->event_date_from,
                    'feedback' => sizeof($user->feedbackAnswers) ? true : false,
                    'checkin' => !empty($event->pivot->checkin_time) ? true : false,
                    'checkout' => !empty($event->pivot->checkout_time) ? true : false,
                    'bookingstatus' => $event->pivot->registration_status_id,
                    'app_sponsors_active' => $event->eventType->app_sponsors_active ? true : false,
                    'map' => sizeof($event->venue) ? array('lat' => $event->venue->latitude, 'lng' => $event->venue->longitude, 'zoom' => 6) : array(),
                    'hashtag' => $event->hashtag,
                );
            }
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    /**
     * Save user feedback question
     * @param int user_id 
     * 
     * @response saved feedback question
     */
    public function submitFeedback(Request $request, $user_id)
    {
        try {
            $validator = Validator::make($request->input(), [
                'eventID' => 'required|numeric',
                'answers' => 'required'
            ]);
            
            if ($validator->fails()) {
                
                return response()->json(['success'=>true, 'error' => $validator->messages()], 200);
            }
            
            foreach ($request->get('answers') as $answer) {
                $feedback = UserFeedback::firstOrNew(['user_id' => $user_id, 'feedback_id' => $answer['questionID'], 'event_id' => $request->get('eventID')]);
                $feedback->user_id = $user_id;
                $feedback->event_id = $request->get('eventID');
                $feedback->feedback_id = $answer['questionID'];
                $feedback->answer = is_array($answer['answer']) ? implode(", ", $answer['answer']) : $answer['answer'];
                $feedback->save();
            }
            
            return response()->json(['success'=>true]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 200);
        }
    }
    
    /**
     * Get list of user favourite documents
     * 
     * @param int user_id
     * @return json array of user favourite documents
     */
    public function userDocuments($user_id)
    {
        try {
            $user = User::findOrFail($user_id);
            $data = array();
            
            foreach ($user->documents as $document) {
                
                $data[] = array(
                    'id' => $document->id,
                    'file' => array(
                        'id' => $document->file->id,
                        'name' => $document->file->display_name,
                        'url' => config('app.url').'uploads/files/'.$document->file->document_name
                    )
                    
                );
            }
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    /**
     * Get list of user favourite documents
     * 
     * @param int document id
     * @return json of document
     */
    public function getDocument($document_id)
    {
        try {
            $document = File::findOrFail($document_id);
            
            $data = array(
                'id' => $document->id,
                'name' => $document->display_name,
                'url' => config('app.url').'uploads/files/'.$document->document_name
            );
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    /**
     * Add document to favourite list
     * @param int user_id
     * @return json collection of stored object
     */
    public function storeUserDocument(Request $request, $user_id)
    {
        try {
            $validator = Validator::make($request->input(), [
                'documentID' => 'required|numeric',
            ]);
            
            if ($validator->fails()) {
                
                return response()->json(['success'=>false, 'error' => $validator->messages()], 400);
            }
            
            $document = new UserDocument();
            $document->user_id = $user_id;
            $document->file_id = $request->get('documentID');
            $document->save();
            
            return response()->json(['success'=>true, 'data'=> $document]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    /**
     * Removes document from user favourite list
     * 
     * @param int document_id
     */
    public function destroyUserDocument($document_id)
    {
        try {
            UserDocument::destroy($document_id);
            
            return response()->json(['success'=>true]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    /**
     * Check if user has left feedback for the event
     */
    public function checkFeedback($user_id, $event_id)
    {
        try {
            $feedback_left = UserFeedback::where('user_id', $user_id)->where('event_id', $event_id)->count();
            $event = Event::findOrFail($event_id);
            $event_attendee = $event->atendees->find($user_id);
            
            $check_in = sizeof($event_attendee) ? $event_attendee->pivot->checkin_time : "";
            
            return response()->json(['success'=>true, 'data'=> ['feedback_required' => (!$feedback_left && !empty($check_in))]]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
}
