<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\File;
use App\Event;

use App\Http\Controllers\Controller;
use JWTAuth;

class DocumentController extends Controller {

	/**
     * Get document details
     * 
     * @param int document_id
     * @return json document information
     */
    public function show($document_id)
    {
        try {
            $file = File::findOrFail($document_id);
            $data = array();
            
            $data = array(
                'id' => $file->id,
                'name' => $file->display_name,
                'url' => config('app.url').'uploads/files/'.$file->document_name
            );
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    /**
     * Get documents assigned to event
     * 
     * @param int event_id
     * @return json event documents
     */
    public function eventDocuments($event_id)
    {
        try {
            $event = Event::findOrFail($event_id);
            $data = array();
            
            foreach ($event->files as $file) {
                $data[] = array(
                    'id' => $file->id,
                    'name' => $file->display_name,
                    'url' => config('app.url').'uploads/files/'.$file->document_name
                );
            }
            
            foreach ($event->eventMaterials as $file) {
                $data[] = array(
                    'id' => $file->id,
                    'name' => $file->display_name,
                    'url' => config('app.url').'uploads/files/'.$file->document_name
                );
            }
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
}
