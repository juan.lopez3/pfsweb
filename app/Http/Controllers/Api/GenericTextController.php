<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\MobileAppText;

class GenericTextController extends Controller {
    
    /**
     * Get a list of generic text
     */
	public function index() {
	    
        try {
            $text = MobileAppText::first();
            
            $data = array();
            
            if (!empty($text)) {
                $data['sign_up'] = $text->sign_up;
                $data['forgot_pin'] = $text->forgot_pin;
                $data['exchange_business_cards'] = $text->exchange_business_cards;
                $data['no_connection'] = $text->no_connection;
            }
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    /**
     * Get generic text by type
     * @param string type text type
     */
    public function getText($type) {
        
        try {
            $text = MobileAppText::first();
            
            $data = array();
            
            if (!empty($text)) {
                $data = $text->{$type};
            }
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }

}
