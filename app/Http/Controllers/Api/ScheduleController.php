<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\Event;
use App\User;
use App\EventScheduleSlotSession;
use App\SessionType;
use App\EventAtendeeFavouriteSession;

use App\Http\Controllers\Controller;
use JWTAuth;

class ScheduleController extends Controller {

	/**
     * Get event sessions with slot information
     * 
     * @param int event_id
     * @return json array of schedule sessions
     */
    public function eventSessions($event_id)
    {
        try {
            $event = Event::findOrFail($event_id);
            $data = array();

            foreach ($event->scheduleSessions()->with('slot.attendees', 'slot.event')->orderBy('session_start')->get() as $session) {
                
                $data[] = array(
                    'id' => $session->id,
                    'name' => $session->title,
                    'date' => ($session->slot->start_date == '0000-00-00') ? $session->slot->event->event_date_from : $session->slot->start_date,
                    'start' => $session->session_start,
                    'end' => $session->session_end,
                    'type' => $session->type->title,
                    'description' => $session->description,
                    'learning_objectives' => $session->learning_objectives,
                    'location' => (sizeof($session->meetingSpace)) ? $session->meetingSpace->meeting_space_name : '',
                    'Timeslot' => array(
                        'title' => $session->slot->title,
                        'bookable' => $session->slot->bookable ? true : false,
                        'capacity' => $session->slot->slot_capacity,
                        'booked' => $session->slot->attendees()->where('registration_status_id', config('registrationstatus.booked'))->count()
                    )
                );
            }
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * Get schedule session information
     * 
     * @param int session_id
     * @return json session object
     */
    public function getSession($session_id)
    {
        try {
            $session = EventScheduleSlotSession::findOrFail($session_id);
            $data = array();

            $data = array(
                'name' => $session->title,
                'date' => $session->slot->event->event_date_from,
                'date' => ($session->slot->start_date == '0000-00-00') ? $session->slot->event->event_date_from : $session->slot->start_date,
                'end' => $session->session_end,
                'type' => $session->type->title,
                'description' => $session->description,
                'learning_objectives' => $session->learning_objectives,
                'location' => (sizeof($session->meetingSpace)) ? $session->meetingSpace->meeting_space_name : '',
                'Timeslot' => array(
                    'title' => $session->slot->title,
                    'bookable' => $session->slot->bookable ? true : false,
                    'capacity' => $session->slot->slot_capacity,
                    'booked' => $session->slot->attendees()->where('registration_status_id', config('registrationstatus.booked'))->count()
                )
            );
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * Get session speakers
     * 
     * @param int session_id
     * @return json session speakers list
     */
    public function sessionsSpeakers($session_id)
    {
        try {
            $session = EventScheduleSlotSession::findOrFail($session_id);
            $data = array();
            
            foreach ($session->contributors()->orderBy('last_name')->get() as $speaker) {
                
                $data[] = array(
                    'id' => $speaker->id,
                    'first_name' => $speaker->first_name,
                    'last_name' => $speaker->last_name,
                    'description' => $speaker->bio,
                    'role' => $speaker->pivot->type,
                    'picture' => !empty($speaker->profile_image) ? config('app.url').'uploads/profile_images/'.$speaker->profile_image : '',
                    'documents' => array(
                        'id' => '',
                        'name' => '',
                        'url' => ''
                    )
                );
            }
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    /**
     * Get speaker information
     * 
     * @param int speaker_id
     * @return json speaker information
     */
    public function speaker($speaker_id)
    {
        try {
            $speaker = User::findOrFail($speaker_id);
            $data = array();
            
            $data = array(
                'id' => $speaker,
                'first_name' => $speaker->first_name,
                'last_name' => $speaker->last_name,
                'description' => $speaker->bio,
                'picture' => !empty($speaker->profile_image) ? config('app.url').'uploads/profile_images/'.$speaker->profile_image : '',
                'documents' => array(
                    'id' => '',
                    'name' => '',
                    'url' => ''
                )
            );
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    /**
     * Get session comitees
     * 
     * @param int session_id
     * @return json list of comitees
     */
    public function sessionComittees($session_id)
    {
        try {
            $session = EventScheduleSlotSession::findOrFail($session_id);
            $data = array();
            $event = $session->slot->event;
            $event_comittees = array();
            
            if (sizeof($event)) {
                
                $event_comittees = $event->atendees()->where('registration_status_id', config('registrationstatus.booked'))->where('comitee', 1)->lists('user_id');
            }
            
            foreach ($session->contributors()->whereIn('user_id', $event_comittees)->orderBy('last_name')->get() as $speaker) {
                
                $data = array(
                    'id' => $speaker->id,
                    'first_name' => $speaker->first_name,
                    'last_name' => $speaker->last_name,
                    'description' => $speaker->bio,
                    'picture' => !empty($speaker->profile_image) ? config('app.url').'uploads/profile_images/'.$speaker->profile_image : '',
                    'documents' => array(
                        'id' => '',
                        'name' => '',
                        'url' => ''
                    )
                );
            }
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    /**
     * Get comiteedetails
     * 
     * @param int comitee_id
     * @return json comitee details
     */
    public function comitee($comitee_id)
    {
        try {
            $comitee = User::findOrFail($comitee_id);
            $data = array();
            
            $data = array(
                'id' => $comitee->id,
                'first_name' => $comitee->first_name,
                'last_name' => $comitee->last_name,
                'description' => $comitee->bio,
                'picture' => !empty($comitee->profile_image) ? config('app.url').'uploads/profile_images/'.$comitee->profile_image : '',
                'documents' => array(
                    'id' => '',
                    'name' => '',
                    'url' => ''
                )
            );
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    /**
     * Get CPD time for event
     * 
     * @param int event_id
     * @return json cpd time 
     */
    public function cpd($event_id)
    {
        try {
            $event = Event::findOrFail($event_id);
            $data = array();
            $duration = 0;
            
            // get session types which are used in CPD hours calculation
            $cpd_session_types = SessionType::where('include_in_cpd', 1)->lists('id');
            
            foreach($event->scheduleSessions as $s) {
                
                if(in_array($s->session_type_id, $cpd_session_types)) {
                    
                    $duration += (strtotime($s->session_end) - strtotime($s->session_start))/60;
                }
            }
            
            $data = array(
                'cpd_time' => $duration,
            );
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * Personal schedule favourite sessions
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function favouriteSessions(Request $request, $event_id)
    {
        try {
            $registration = EventAttendee::where('event_id', $event_id)->where('user_id', auth()->user()->id)->first();
            $favourite_sessions = $registration->favouriteSessions->lists('event_schedule_slot_session_id');
            $sessions = EventScheduleSlotSession::whereIn('id', $favourite_sessions)->with('type', 'contributors', 'slot')->orderBy('session_start')->get();
            $personal_schedule = array();

            foreach ($sessions as $session) {
                $personal_schedule[$session->slot->start_date][] = $session;
            }

            return response()->json(['success' => true, 'data'=>json_encode($personal_schedule)]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage(), 400]);
        }
    }

}
