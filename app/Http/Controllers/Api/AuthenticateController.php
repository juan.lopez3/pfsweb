<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use Illuminate\Http\Request;

class AuthenticateController extends Controller {
    
    
    public function login() {
        
        $user = User::first();
        $token = JWTAuth::fromUser($user);
        
        dd($token);
        
        return response()->json($events, 200);
    }
    public function test() {
        
        echo ":)";
    }
    
    
	public function authenticate(Request $request)
    {
        // grab credentials from the request
        try {
            $user = User::where('email', $request->get('email'))->where('pin', $request->get('pin'))->first();
            
            // attempt to verify the credentials and create a token for the user
            if (empty($user)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        
        $token = JWTAuth::fromUser($user);
        
        $data = array(
            'id' => $user->id,
            'name' => $user->first_name,
            'lastname' => $user->last_name,
            'badge_name' => $user->badge_name,
            'email' => $user->email,
            'cc_email' => $user->cc_email,
            'dietary_requirement_id' => $user->dietary_requirement_id,
            'dietary_requirement_other' => $user->dietary_requirement_other,
            'special_requirements' => $user->special_requirements,
            'postcode' => $user->postcode,
            'company' => $user->company,
            'title' => $user->title,
            'mobilePhone' => $user->mobile,
            'contactPhone' => $user->phone,
            'attendee_types' => $user->attendeeTypes->lists('id'),
            'token' => $token
        );
        
        // all good so return the token
        return response()->json($data);
    }
    
    public function authrequest()
    {
        return view('authrequest');
    }
    
    /**
     * Checks if token is valid
     */
    public function checkToken($token)
    {
        try {
            $user = JWTAuth::toUser($token);
            
            if (sizeof($user)) {
                return response()->json(['success' => true]);
            } else {
                return response()->json(['success' => false]);
            }
            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
    
    public function logout($token)
    {
        try {
            JWTAuth::invalidate($token);
            return response()->json(['success' => true]);
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

}
