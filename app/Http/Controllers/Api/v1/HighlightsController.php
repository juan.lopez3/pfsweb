<?php namespace App\Http\Controllers\Api\v1;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Highlights;
use App\Event;
use App\Events\DataWasManipulated;

class HighlightsController extends Controller {

    public $log_desc = "highlights ";

    /**
     * Display a listing of the highlights.
     *
     * @return list of highlights
     */
    public function index()
    {
        try {

            $highlights =  highlights::all();
            
           
            foreach ($highlights as $highlight) {

                $event = event::where('id', $highlight->event_id)->get()->first();

                $data[] = array(
                    'HighlightID' => $highlight->id,
                    'EventID' => $highlight->event_id,
                    'Title' => $highlight->title,
                    'DateFrom'=> $event->event_date_from,
                    'DateTo'=> $event->event_date_to,
                    'Venue' => sizeof($event->region) ? $event->region->title : ''
                );

            }

            return response()->json(['success'=>true, 'data'=>$data]);

        }catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }

    }
}
