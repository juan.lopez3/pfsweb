<?php namespace App\Http\Controllers\Api\v1;

use Validator;
use App\Http\Requests;
use Illuminate\Http\Request;

use App\User;
use App\UserNote;

use App\Http\Controllers\Controller;
use JWTAuth;

class UserNoteController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($user_id)
	{
		try {
            $user = User::findOrFail($user_id);
            $data = array();
            
            foreach ($user->notes as $note) {
                
                $data[] = array(
                    'noteid' => $note->id,
                    'eventid' => $note->event_id,
                    'content' => $note->content,
                );
            }
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
	}

	/**
	 * Store a newly created resource in storage.
	 *
     * @param int user_id
	 * @return Response
	 */
	public function store(Request $request, $user_id)
	{
		try {
            $validator = Validator::make($request->input(), [
                'eventid' => 'required|numeric',
            ]);
            
            if ($validator->fails()) {
                
                return response()->json(['success'=>false, 'error' => $validator->messages()], 400);
            }
            
            $note = new UserNote();
            $note->user_id = $user_id;
            $note->event_id = $request->get('eventid');
            $note->content = $request->get('content');
            $note->save();
            
            return response()->json(['success'=>true, 'data'=> $note]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($note_id)
	{
		try {
            $note = UserNote::findOrFail($note_id);
            $data = array();
            
            $data[] = array(
                'noteid' => $note->id,
                'eventid' => $note->event_id,
                'content' => $note->content,
            );
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $user_id, $note_id)
	{
		try {
            $validator = Validator::make($request->input(), [
                //'eventid' => 'required|numeric',
            ]);
            
            if ($validator->fails()) {
                
                return response()->json(['success'=>false, 'error' => $validator->messages()], 400);
            }
            
            $note = UserNote::findOrFail($note_id);
            //$note->user_id = $user_id;
            //$note->event_id = $request->get('eventid');
            $note->content = $request->get('content');
            $note->save();
            
            return response()->json(['success'=>true, 'data'=> $note]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $note_id
	 * @return Response
	 */
	public function destroy($note_id)
	{
		try {
            UserNote::destroy($note_id);
            
            return response()->json(['success'=>true]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
	}

}
