<?php namespace App\Http\Controllers\Api\v1;

use App\EventScheduleSlotSession;
use App\Http\Requests;
use Illuminate\Http\Request;

use App\Event;
use App\EventAttendee;
use App\User;
use App\Venue;
use App\EventFaq;
use App\Sponsor;
use App\SponsorSetting;
use App\EventFeedback;

use App\Http\Controllers\Controller;
use JWTAuth;

class EventController extends Controller {
    
    /**
     * Get forthcoming events. Used by CII
     * 
     * @return json array of future events
     */
    public function ciiEvents()
    {
        try {
            $events = Event::where('publish', 1)->where('event_date_from', '>=', date("Y-m-d"))->with('venue')->get();
            $data = array();
            
            foreach ($events as $event) {
                $data[] = array(
                    'EventID' => $event->id,
                    'EventDateTimeBegin' => $event->event_date_from,
                    'EventDateTimeEnd' => $event->event_date_to,
                    'EventName' => $event->title,
                    'Uri' => route('events.view', $event->slug),
                    'PortalName' => 'The Personal Finance Society',
                    'LocationLatitude' => (sizeof($event->venue)) ? $event->venue->latitude : '',
                    'LocationLongitude' => (sizeof($event->venue)) ? $event->venue->longitude : '',
                    'PostCode' => (sizeof($event->venue)) ? $event->venue->postcode : '',
                    'LocationName' => (sizeof($event->venue)) ? $event->venue->name : '',
                    'EventDesc' => $event->description,
                );
            }
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * Get all events in the system
     * 
     * @return json array of future events
     */
    public function events()
    {
        try {
            $events = Event::with('eventType', 'venue', 'region')->get();
            $data = array();
            
            foreach ($events as $event) {
                
                $app_additional = array();
                
                if (sizeof($event->eventType) && $event->eventType->app_additional_active) {
                    $app_additional = array(
                        'title' => $event->eventType->app_additional_title,
                        'content' => $event->eventType->app_additional_content
                    );
                }
                
                // calculate event start and end times from the schedule sessions
                $start_time = $event->scheduleSessions()->where('use_api_start', 1)->orderBy('session_start')->first();
                
                // if none sessions were selected to use then use the first slot time
                if (!sizeof($start_time)) $start_time = $event->scheduleSessions()->orderBy('session_start')->first();
                
                // if found any data use start time otherwise use default 8:30
                $start_time = sizeof($start_time) ? $start_time->session_start : "08:30:00";
                
                $end_time = $event->scheduleSessions()->orderBy('session_end', 'desc')->first();
                $end_time = (sizeof($end_time)) ? $end_time->session_end : "17:00:00";
                
                $data[] = array(
                    'id' => $event->id,
                    'name' => $event->title,
                    'exhibition' => $event->eventType->exhibition ? true : false,
                    'date_from' => $event->event_date_from.' '.$start_time,
                    'date_to' => $event->event_date_to.' '.$end_time,
                    'cut_of_date'=>$event->cut_of_date, 
                    'location' => sizeof($event->region) ? $event->region->title : '',
                    'image' => !empty($event->app_banner) ? config('app.url').'uploads/app/'.$event->app_banner: '',
                    'weburl' => route('events.view', $event->slug),
                    'eventtype' => array(
                        'id' => $event->event_type_id,
                         'name' => $event->eventType->title,
                         'appDescription' => $event->eventType->app_description,
                         'color' => $event->eventType->app_color,
                         'app_branding' => !empty($event->eventType->app_branding) ? config('app.url').'uploads/app/'.$event->eventType->app_branding : '',
                         'icon' => !empty($event->eventType->app_icon) ? config('app.url').'uploads/app/'.$event->eventType->app_icon : '',
                         'promotionalBanner' => !empty($event->eventType->app_promotional_banner) ? config('app.url').'uploads/app/'.$event->eventType->app_promotional_banner : '',
                         'additional_app_section' => $app_additional,
                    ),
                    'Appenabled' => $event->app_enabled ? true : false,
                    'feedback_enabled' => in_array(config('tabs.tab_7'), $event->tabs->lists('id')) ? true : false,
                    'feedback_type' => $event->eventType->feedback_type,
                    'app_sponsors_active' => $event->eventType->app_sponsors_active ? true : false,
                    'local_committee' => $event->local_committee ? true : false,
                    'map' => sizeof($event->venue) ? array('lat' => $event->venue->latitude, 'lng' => $event->venue->longitude, 'zoom' => 6) : array(),
                    'hashtag' => $event->hashtag,
                    'schedule_version' => $event->schedule_version
                );
            }
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    /**
     * Get specific event data
     * 
     * @param int $event_id
     * @return json array of future events
     */
    public function eventData($event_id)
    {
        try {
            $event = Event::findOrFail($event_id);
            
            $app_additional = array();
            
            if (sizeof($event->eventType) && $event->eventType->app_additional_active) {
                $app_additional = array(
                    'title' => $event->eventType->app_additional_title,
                    'content' => $event->eventType->app_additional_content
                );
            }
            
            // calculate event start and end times from the schedule sessions
            $start_time = $event->scheduleSessions()->where('use_api_start', 1)->orderBy('session_start')->first();
            
            // if none sessions were selected to use then use the first slot time
            if (!sizeof($start_time)) $start_time = $event->scheduleSessions()->orderBy('session_start')->first();
            
            // if found any data use start time otherwise use default 8:30
            $start_time = sizeof($start_time) ? $start_time->session_start : "08:30:00";
            
            $end_time = $event->scheduleSessions()->orderBy('session_end', 'desc')->first();
            $end_time = (sizeof($end_time)) ? $end_time->session_end : "17:00:00";
            
            $data = array(
                'id' => $event->id,
                'name' => $event->title,
                'exhibition' => $event->eventType->exhibition ? true : false,
                'date_from' => $event->event_date_from.' '.$start_time,
                'date_to' => $event->event_date_to.' '.$end_time,
                'cut_of_date'=>$event->cut_of_date, 
                'location' => sizeof($event->region) ? $event->region->title : '',
                'image' => !empty($event->app_banner) ? config('app.url').'uploads/app/'.$event->app_banner: '',
                'weburl' => route('events.view', $event->slug),
                'eventtype' => array(
                    'id' => $event->event_type_id,
                     'name' => $event->eventType->title,
                     'appDescription' => $event->eventType->app_description,
                     'color' => $event->eventType->app_color,
                     'app_branding' => !empty($event->eventType->app_branding) ? config('app.url').'uploads/app/'.$event->eventType->app_branding : '',
                     'icon' => !empty($event->eventType->app_icon) ? config('app.url').'uploads/app/'.$event->eventType->app_icon : '',
                     'promotionalBanner' => !empty($event->eventType->app_promotional_banner) ? config('app.url').'uploads/app/'.$event->eventType->app_promotional_banner : '',
                     'additional_app_section' => $app_additional, 
                ),
                'Appenabled' => $event->app_enabled ? true : false,
                'feedback_enabled' => in_array(config('tabs.tab_7'), $event->tabs->lists('id')) ? true : false,
                'feedback_type' => $event->eventType->feedback_type,
                'app_sponsors_active' => $event->eventType->app_sponsors_active ? true : false,
                'local_committee' => $event->local_committee ? true : false,
                'map' => sizeof($event->venue) ? array('lat' => $event->venue->latitude, 'lng' => $event->venue->longitude, 'zoom' => 6) : array(),
                'hashtag' => $event->hashtag,
                'schedule_version' => $event->schedule_version,
                'programme_enabled' => ($event->main_schedule_visible_app) ? true : false,
                'cpd_manually' => isset($event->manually_cpd) ? $event->manually_cpd : ''
            );
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    
    /**
     * Get all events in the system
     * 
     * @return json array of future events
     */
    public function activeEvents()
    {
        try {
            $events = Event::where('app_enabled', 1)->where('publish', 1)->with('eventType', 'venue', 'region')->get();
            $data = array();
            
            foreach ($events as $event) {
                
                $app_additional = array();
                
                if (sizeof($event->eventType) && $event->eventType->app_additional_active) {
                    $app_additional = array(
                        'title' => $event->eventType->app_additional_title,
                        'content' => $event->eventType->app_additional_content
                    );
                }
                
                // calculate event start and end times from the schedule sessions
                $start_time = $event->scheduleSessions()->where('use_api_start', 1)->orderBy('session_start')->first();
                
                // if none sessions were selected to use then use the first slot time
                if (!sizeof($start_time)) $start_time = $event->scheduleSessions()->orderBy('session_start')->first();
                
                // if found any data use start time otherwise use default 8:30
                $start_time = sizeof($start_time) ? $start_time->session_start : "08:30:00";
                
                $end_time = $event->scheduleSessions()->orderBy('session_end', 'desc')->first();
                $end_time = (sizeof($end_time)) ? $end_time->session_end : "17:00:00";
                
                $data[] = array(
                    'id' => $event->id,
                    'name' => $event->title,
                    'exhibition' => $event->eventType->exhibition ? true : false,
                    'date_from' => $event->event_date_from.' '.$start_time,
                    'date_to' => $event->event_date_to.' '.$end_time,
                    'location' => sizeof($event->region) ? $event->region->title : '',
                    'image' => !empty($event->app_banner) ? config('app.url').'uploads/app/'.$event->app_banner: '',
                    'weburl' => route('events.view', $event->slug),
                    'eventtype' => array(
                        'id' => $event->event_type_id,
                         'name' => $event->eventType->title,
                         'appDescription' => $event->eventType->app_description,
                         'color' => $event->eventType->app_color, 
                         'app_branding' => !empty($event->eventType->app_branding) ? config('app.url').'uploads/app/'.$event->eventType->app_branding : '',
                         'icon' => !empty($event->eventType->app_icon) ? config('app.url').'uploads/app/'.$event->eventType->app_icon : '',
                         'promotionalBanner' => !empty($event->eventType->app_promotional_banner) ? config('app.url').'uploads/app/'.$event->eventType->app_promotional_banner : '',
                         'additional_app_section' => $app_additional, 
                    ),
                    'Appenabled' => $event->app_enabled ? true : false,
                    'feedback_enabled' => in_array(config('tabs.tab_7'), $event->tabs->lists('id')) ? true : false,
                    'feedback_type' => $event->eventType->feedback_type,
                    'app_sponsors_active' => $event->eventType->app_sponsors_active ? true : false,
                    'local_committee' => $event->local_committee ? true : false,
                    'map' => sizeof($event->venue) ? array('lat' => $event->venue->latitude, 'lng' => $event->venue->longitude, 'zoom' => 6) : array(),
                    'hashtag' => $event->hashtag,
                    'schedule_version' => $event->schedule_version
                );
            }

            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * Get all events in the system
     * 
     * @return json array of future events
     */
    public function activeEventsByType($type_id)
    {
        try {
            $events = Event::where('app_enabled', 1)->where('publish', 1)->where('event_type_id', $type_id)->with('eventType', 'venue', 'region')->get();
            $data = array();
            
            foreach ($events as $event) {
                
                $app_additional = array();
                
                if (sizeof($event->eventType) && $event->eventType->app_additional_active) {
                    $app_additional = array(
                        'title' => $event->eventType->app_additional_title,
                        'content' => $event->eventType->app_additional_content
                    );
                }
                
                // calculate event start and end times from the schedule sessions
                $start_time = $event->scheduleSessions()->where('use_api_start', 1)->orderBy('session_start')->first();
                
                // if none sessions were selected to use then use the first slot time
                if (!sizeof($start_time)) $start_time = $event->scheduleSessions()->orderBy('session_start')->first();
                
                // if found any data use start time otherwise use default 8:30
                $start_time = sizeof($start_time) ? $start_time->session_start : "08:30:00";
                
                $end_time = $event->scheduleSessions()->orderBy('session_end', 'desc')->first();
                $end_time = (sizeof($end_time)) ? $end_time->session_end : "17:00:00";
                
                $data[] = array(
                    'id' => $event->id,
                    'name' => $event->title,
                    'exhibition' => $event->eventType->exhibition ? true : false,
                    'date_from' => $event->event_date_from.' '.$start_time,
                    'date_to' => $event->event_date_to.' '.$end_time,
                    'location' => sizeof($event->region) ? $event->region->title : '',
                    'image' => !empty($event->app_banner) ? config('app.url').'uploads/app/'.$event->app_banner: '',
                    'weburl' => route('events.view', $event->slug),
                    'eventtype' => array(
                        'id' => $event->event_type_id,
                         'name' => $event->eventType->title,
                         'appDescription' => $event->eventType->app_description,
                         'color' => $event->eventType->app_color, 
                         'app_branding' => !empty($event->eventType->app_branding) ? config('app.url').'uploads/app/'.$event->eventType->app_branding : '',
                         'icon' => !empty($event->eventType->app_icon) ? config('app.url').'uploads/app/'.$event->eventType->app_icon : '',
                         'promotionalBanner' => !empty($event->eventType->app_promotional_banner) ? config('app.url').'uploads/app/'.$event->eventType->app_promotional_banner : '',
                         'additional_app_section' => $app_additional,
                    ),
                    'Appenabled' => $event->app_enabled ? true : false,
                    'feedback_enabled' => in_array(config('tabs.tab_7'), $event->tabs->lists('id')) ? true : false,
                    'feedback_type' => $event->eventType->feedback_type,
                    'app_sponsors_active' => $event->eventType->app_sponsors_active ? true : false,
                    'local_committee' => $event->local_committee ? true : false,
                    'map' => sizeof($event->venue) ? array('lat' => $event->venue->latitude, 'lng' => $event->venue->longitude, 'zoom' => 6) : array(),
                    'hashtag' => $event->hashtag,
                    'schedule_version' => $event->schedule_version
                );
            }

            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * Get all forthcoming events
     * 
     * @param int $event_id
     * @return json array of future events
     */
    public function forthcomingEvents()
    {
        try {
            $events = Event::where('app_enabled', 1)->where('publish', 1)->where('event_date_from', '>=', date("Y-m-d"))->with('eventType', 'venue', 'region')->get();
            $data = array();
            
            foreach ($events as $event) {
                
                $app_additional = array();
                
                if (sizeof($event->eventType) && $event->eventType->app_additional_active) {
                    $app_additional = array(
                        'title' => $event->eventType->app_additional_title,
                        'content' => $event->eventType->app_additional_content
                    );
                }
                
                // calculate event start and end times from the schedule sessions
                $start_time = $event->scheduleSessions()->where('use_api_start', 1)->orderBy('session_start')->first();
                
                // if none sessions were selected to use then use the first slot time
                if (!sizeof($start_time)) $start_time = $event->scheduleSessions()->orderBy('session_start')->first();
                
                // if found any data use start time otherwise use default 8:30
                $start_time = sizeof($start_time) ? $start_time->session_start : "08:30:00";
                
                $end_time = $event->scheduleSessions()->orderBy('session_end', 'desc')->first();
                $end_time = (sizeof($end_time)) ? $end_time->session_end : "17:00:00";
                
                $data[] = array(
                    'id' => $event->id,
                    'name' => $event->title,
                    'exhibition' => $event->eventType->exhibition ? true : false,
                    'date_from' => $event->event_date_from.' '.$start_time,
                    'date_to' => $event->event_date_to.' '.$end_time,
                    'location' => sizeof($event->region) ? $event->region->title : '',
                    'image' => !empty($event->app_banner) ? config('app.url').'uploads/app/'.$event->app_banner: '',
                    'weburl' => route('events.view', $event->slug),
                    'eventtype' => array(
                        'id' => $event->event_type_id,
                         'name' => $event->eventType->title,
                         'appDescription' => $event->eventType->app_description,
                         'color' => $event->eventType->app_color, 
                         'app_branding' => !empty($event->eventType->app_branding) ? config('app.url').'uploads/app/'.$event->eventType->app_branding : '',
                         'icon' => !empty($event->eventType->app_icon) ? config('app.url').'uploads/app/'.$event->eventType->app_icon : '',
                         'promotionalBanner' => !empty($event->eventType->app_promotional_banner) ? config('app.url').'uploads/app/'.$event->eventType->app_promotional_banner : '',
                         'additional_app_section' => $app_additional, 
                    ),
                    'Appenabled' => $event->app_enabled ? true : false,
                    'feedback_enabled' => in_array(config('tabs.tab_7'), $event->tabs->lists('id')) ? true : false,
                    'feedback_type' => $event->eventType->feedback_type,
                    'app_sponsors_active' => $event->eventType->app_sponsors_active ? true : false,
                    'local_committee' => $event->local_committee ? true : false,
                    'map' => sizeof($event->venue) ? array('lat' => $event->venue->latitude, 'lng' => $event->venue->longitude, 'zoom' => 6) : array(),
                    'hashtag' => $event->hashtag,
                    'schedule_version' => $event->schedule_version,
                );
            }

            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * Get all forthcoming events filtered by event type
     * 
     * @param int $event_id
     * @return json array of future events
     */
    public function forthcomingEventsByType($type_id)
    {
        try {
            $events = Event::where('app_enabled', 1)->where('publish', 1)->where('event_date_from', '>=', date("Y-m-d"))->where('event_type_id', $type_id)->with('eventType', 'venue', 'region')->get();
            $data = array();
            
            foreach ($events as $event) {
                
                $app_additional = array();
                
                if (sizeof($event->eventType) && $event->eventType->app_additional_active) {
                    $app_additional = array(
                        'title' => $event->eventType->app_additional_title,
                        'content' => $event->eventType->app_additional_content
                    );
                }
                
                // calculate event start and end times from the schedule sessions
                $start_time = $event->scheduleSessions()->where('use_api_start', 1)->orderBy('session_start')->first();
                
                // if none sessions were selected to use then use the first slot time
                if (!sizeof($start_time)) $start_time = $event->scheduleSessions()->orderBy('session_start')->first();
                
                // if found any data use start time otherwise use default 8:30
                $start_time = sizeof($start_time) ? $start_time->session_start : "08:30:00";
                
                $end_time = $event->scheduleSessions()->orderBy('session_end', 'desc')->first();
                $end_time = (sizeof($end_time)) ? $end_time->session_end : "17:00:00";
                
                $data[] = array(
                    'id' => $event->id,
                    'name' => $event->title,
                    'exhibition' => $event->eventType->exhibition ? true : false,
                    'date_from' => $event->event_date_from.' '.$start_time,
                    'date_to' => $event->event_date_to.' '.$end_time,
                    'location' => sizeof($event->region) ? $event->region->title : '',
                    'image' => !empty($event->app_banner) ? config('app.url').'uploads/app/'.$event->app_banner: '',
                    'weburl' => route('events.view', $event->slug),
                    'eventtype' => array(
                        'id' => $event->event_type_id,
                         'name' => $event->eventType->title,
                         'appDescription' => $event->eventType->app_description,
                         'color' => $event->eventType->app_color, 
                         'app_branding' => !empty($event->eventType->app_branding) ? config('app.url').'uploads/app/'.$event->eventType->app_branding : '',
                         'icon' => !empty($event->eventType->app_icon) ? config('app.url').'uploads/app/'.$event->eventType->app_icon : '',
                         'promotionalBanner' => !empty($event->eventType->app_promotional_banner) ? config('app.url').'uploads/app/'.$event->eventType->app_promotional_banner : '',
                         'additional_app_section' => $app_additional, 
                    ),
                    'Appenabled' => $event->app_enabled ? true : false,
                    'feedback_enabled' => in_array(config('tabs.tab_7'), $event->tabs->lists('id')) ? true : false,
                    'feedback_type' => $event->eventType->feedback_type,
                    'app_sponsors_active' => $event->eventType->app_sponsors_active ? true : false,
                    'local_committee' => $event->local_committee ? true : false,
                    'map' => sizeof($event->venue) ? array('lat' => $event->venue->latitude, 'lng' => $event->venue->longitude, 'zoom' => 6) : array(),
                    'hashtag' => $event->hashtag,
                    'schedule_version' => $event->schedule_version
                );
            }

            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    /**
     * Get events near location in selected range
     * 
     * @param int $event_id
     * @return json array of future events
     */
    public function eventsNearMe(Requests\GetEventsNearMeRequest $request)
    {
        try {
            
            $distance = $request->has('distance') ? $request->get('distance') : 50;
            $address = simplexml_load_file("https://maps.googleapis.com/maps/api/geocode/xml?address=".$request->get('address')."&sensor=false&components=country:uk&key=AIzaSyCFo1QWcA_GRpH7Q_jV14HF2PJ_3qzuiB8");
            $filter_lat = $address->result->geometry->location->lat;
            $filter_lng = $address->result->geometry->location->lng;
            
            $venues = \Cache::remember('venues-api', 1440, function(){
                
                return Venue::all();
            });
            
            $venues_inrange = [];
            
            foreach($venues as $v) {
                if(empty($v->latitude) || empty($v->longitude)) continue;
                if($this->distance(floatval($filter_lat), floatval($filter_lng), $v->latitude, $v->longitude) <= $distance) 
                    array_push($venues_inrange, $v->id);
            }
            
            $events = Event::where('app_enabled', 1)->where('publish', 1)->where('event_date_from', '>=', date("Y-m-d"))->whereIn('venue_id', $venues_inrange)->with('eventType', 'venue', 'region')->get();
            $data = array();
            
            foreach ($events as $event) {
                
                $app_additional = array();
                
                if (sizeof($event->eventType) && $event->eventType->app_additional_active) {
                    $app_additional = array(
                        'title' => $event->eventType->app_additional_title,
                        'content' => $event->eventType->app_additional_content
                    );
                }
                
                // calculate event start and end times from the schedule sessions
                $start_time = $event->scheduleSessions()->where('use_api_start', 1)->orderBy('session_start')->first();
                
                // if none sessions were selected to use then use the first slot time
                if (!sizeof($start_time)) $start_time = $event->scheduleSessions()->orderBy('session_start')->first();
                
                // if found any data use start time otherwise use default 8:30
                $start_time = sizeof($start_time) ? $start_time->session_start : "08:30:00";
                
                $end_time = $event->scheduleSessions()->orderBy('session_end', 'desc')->first();
                $end_time = (sizeof($end_time)) ? $end_time->session_end : "17:00:00";
                
                $data[] = array(
                    'id' => $event->id,
                    'name' => $event->title,
                    'exhibition' => $event->eventType->exhibition ? true : false,
                    'date_from' => $event->event_date_from.' '.$start_time,
                    'date_to' => $event->event_date_to.' '.$end_time,
                    'location' => sizeof($event->region) ? $event->region->title : '',
                    'image' => !empty($event->app_banner) ? config('app.url').'uploads/app/'.$event->app_banner: '',
                    'weburl' => route('events.view', $event->slug),
                    'eventtype' => array(
                        'id' => $event->event_type_id,
                         'name' => $event->eventType->title,
                         'appDescription' => $event->eventType->app_description,
                         'color' => $event->eventType->app_color, 
                         'app_branding' => !empty($event->eventType->app_branding) ? config('app.url').'uploads/app/'.$event->eventType->app_branding : '',
                         'icon' => !empty($event->eventType->app_icon) ? config('app.url').'uploads/app/'.$event->eventType->app_icon : '',
                         'promotionalBanner' => !empty($event->eventType->app_promotional_banner) ? config('app.url').'uploads/app/'.$event->eventType->app_promotional_banner : '',
                         'additional_app_section' => $app_additional, 
                    ),
                    'Appenabled' => $event->app_enabled ? true : false,
                    'feedback_enabled' => in_array(config('tabs.tab_7'), $event->tabs->lists('id')) ? true : false,
                    'feedback_type' => $event->eventType->feedback_type,
                    'app_sponsors_active' => $event->eventType->app_sponsors_active ? true : false,
                    'local_committee' => $event->local_committee ? true : false,
                    'map' => sizeof($event->venue) ? array('lat' => $event->venue->latitude, 'lng' => $event->venue->longitude, 'zoom' => 6) : array(),
                    'hashtag' => $event->hashtag,
                    'schedule_version' => $event->schedule_version
                );
            }

            return response()->json(['success'=>true, 'data'=>$data]);
            } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * Calculate distance between 2 geolocation points
     * 
     *  @param flaot  $lat1 first point latitude
     *  @param flaot  $lng1 first point langitude
     *  @param flaot  $lat1 second point latitude
     *  @param flaot  $lng1 second point langitude
     */
    private function distance($lat1, $lon1, $lat2, $lon2, $unit = "M")
    {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);
        
        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }
    
    /**
     * All registered users for event
     * 
     * @param int $event_id
     * @return json array of event attendees
     */
    public function registeredUsers($event_id)
    {
        try {
            $event = Event::findOrFail($event_id);
            $data = array();
            
            foreach ($event->atendees()->where('registration_status_id', config('registrationstatus.booked'))->orderBy('last_name')->get() as $attendee) {
                $data[] = array(
                    'id' => $attendee->id,
                    'first_name' => $attendee->first_name,
                    'last_name' => $attendee->last_name,
                    'company' => $attendee->company,
                    'avaliableinchat' => $attendee->available_in_chat ? true : false,
                    'checkin' => !empty($attendee->pivot->checkin_time) ? true : false,
                    'checkout' => !empty($attendee->pivot->checkout_time) ? true : false,
                    'bookingstatus' => $attendee->pivot->registration_status_id
                );
            }
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    /**
     * All checked ina ttendees for event
     * 
     * @param int $event_id
     * @return json array of event attendees
     */
    public function checkedinUsers($event_id)
    {
        try {
            $event = Event::whereHas('atendees', function($q){
                $q->whereNotNull('checkin_time');
            })->with('atendees')->findOrFail($event_id);
            $data = array();
            
            foreach ($event->atendees as $attendee) {
                $data[] = array(
                    'id' => $attendee->id,
                    'name' => $attendee->first_name,
                    'company' => $attendee->company,
                    'avaliableinchat' => $attendee->available_in_chat ? true : false,
                    'checkin' => !empty($attendee->pivot->checkin_time) ? true : false,
                    'checkout' => !empty($attendee->pivot->checkout_time) ? true : false,
                    'bookingstatus' => $attendee->pivot->registration_status_id
                );
            }
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }

    
    /**
     * Get feedbakc questions for event
     * 
     * @param int $event_id
     * @return json array of feedback questions
     */
    public function feedbacks($event_id)
    {
        try {
            $event = Event::findOrFail($event_id);
            $data = array();
            $session_feedbacks = array();
            $event_feedbacks = array();
            
            foreach ($event->scheduleSessions()->orderBy('session_start')->has('feedbacks')->with('feedbacks', 'contributors')->get() as $session) {
                
                $questions = array();
                foreach ($session->feedbacks as $feedback) {
                    
                    $questions[] = array(
                        'id' => $feedback->id,
                        'title' => $feedback->question,
                        'type' => config('api.v1.feedback_types.'.$feedback->type),
                        'answers' => array_map('trim', explode(';', $feedback->answer))
                    );
                }
                
                $session_feedbacks[$session->title] = array(
                    'time' => date("H:i", strtotime($session->session_start)).' - '.date("H:i", strtotime($session->session_end)),
                    'contributors' => $session->contributors()->select('first_name', 'last_name', 'company')->get()->toArray(),
                    'questions' => $questions
                );
            }
            
            $e_f = EventFeedback::where('event_id', $event->id)->whereNull('session_id')->orderBy('position')->get();
            
            foreach ($e_f as $feedback) {
                
                $event_feedbacks[] = array(
                    'id' => $feedback->id,
                    'title' => $feedback->question,
                    'type' => config('api.v1.feedback_types.'.$feedback->type),
                    'answers' => array_map('trim', explode(';', $feedback->answer))
                );
            }
            
            $data['session_feedbacks'] = $session_feedbacks;
            $data['event_feedbacks'] = $event_feedbacks;
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * Return only event related feedback
     * @param $event_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function feedbacksEvent($event_id)
    {
        try {
            $event = Event::findOrFail($event_id);
            $data = array();
            $event_feedbacks = array();

            // get questions where checkin is not required
            $e_f = EventFeedback::where('event_id', $event->id)->whereNull('session_id')->where('checkin_required', 0)->orderBy('position')->get();

            // if user logged in and checkin in to the event get questions where checkin is required
            $user = auth()->user();
            if (sizeof($user)) {
                // find registration
                $registration = EventAttendee::where('user_id', $user->id)->where('event_id', $event)->first();
                if (sizeof($registration) && !empty($registration->checkin_time)) {
                    // get and merge questions
                    $e_f_checkin_required = EventFeedback::where('event_id', $event->id)->whereNull('session_id')->where('checkin_required', 1)->orderBy('position')->get();
                    $e_f = $e_f->merge($e_f_checkin_required);
                }
            }

            foreach ($e_f as $feedback) {

                $event_feedbacks[] = array(
                    'id' => $feedback->id,
                    'title' => $feedback->question,
                    'type' => config('api.v1.feedback_types.'.$feedback->type),
                    'answers' => array_map('trim', explode(';', $feedback->answer))
                );
            }

            $data = $event_feedbacks;

            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {

            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * Return only session related feedback
     * @param $event_id
     * @param $session_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function feedbacksSession($event_id, $session_id)
    {
        try {
            $event = Event::findOrFail($event_id);
            $data = array();
            $session = EventScheduleSlotSession::findOrFail($session_id);
            $s_f = $session->feedbacks()->where('checkin_required', 0)->get();

            $user = auth()->user();
            if (sizeof($user)) {
                // find registration
                $registration = EventAttendee::where('user_id', $user->id)->where('event_id', $event)->first();
                if (sizeof($registration) && !empty($registration->checkin_time)) {
                    // get and merge questions
                    $s_f_checkin_required = $session->feedbacks()->where('checkin_required', 1)->get();
                    $s_f = $s_f->merge($s_f_checkin_required);
                }
            }

            foreach ($s_f as $feedback) {

                $data[] = array(
                    'id' => $feedback->id,
                    'title' => $feedback->question,
                    'type' => config('api.v1.feedback_types.'.$feedback->type),
                    'answers' => array_map('trim', explode(';', $feedback->answer))
                );
            }

            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {

            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    /**
     * Event venue information
     * 
     * @param int $event_id
     * @return json venue details
     */
    public function venue($event_id)
    {
        try {
            $event = Event::findOrFail($event_id);
            $data = array();
            
            if (sizeof($event->venue)) {
                
                $address = $event->venue->address;
                if (!empty($event->venue->city)) $address .= ", ".$event->venue->city;
                if (!empty($event->venue->country)) $address .= ", ".$event->venue->country;
                
                $images = array();
                
                if (!empty($event->venue->image_1)) $images[] = config('app.url').'uploads/images/'.$event->venue->image_1;
                if (!empty($event->venue->image_2)) $images[] = config('app.url').'uploads/images/'.$event->venue->image_2;
                
                $data = array(
                    'name' => $event->venue->name,
                    'description' => $event->venue->details,
                    'address' => $address,
                    'url' => $event->venue->website,
                    'map' => sizeof($event->venue) ? array('lat' => $event->venue->latitude, 'lng' => $event->venue->longitude, 'zoom' => 6) : array(),
                    'images' => $images
                );
            }
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    /**
     * Get all event sponsors
     * 
     * @param int event id
     * @return json array of sponsors for the event
     */
    public function eventSponsors($event_id)
    {
        try {
            $event = Event::findOrFail($event_id);
            $data = array();
            $categories = array();

            if($event_id == 442) {
                
                foreach ($event->sponsors()->where('type', 0)->where('event_sponsor.order', '>', 0)->orderBy('event_sponsor.order', 'ASC')->limit(19)->get() as $sponsor) {
                    if (!empty($sponsor->pivot->category_1)) $categories[] = $sponsor->pivot->category_1;
                    if (!empty($sponsor->pivot->category_2)) $categories[] = $sponsor->pivot->category_2;

                    $data[] = array(
                        'id' => $sponsor->id,
                        'company' => $sponsor->name,
                        'logo' => !empty($sponsor->logo) ? config('app.url').'uploads/sponsors/'.$sponsor->logo : '',
                        'description' => $sponsor->info,
                        'website' => $sponsor->website,
                        'documents' => array(),
                        'stand_number' => $sponsor->pivot->stand_number,
                        'categories' => $categories,
                        'advert' => (!empty($sponsor->app_logo) && $event->eventType->app_sponsors_active) ? config('app.url').'uploads/sponsors/'.$sponsor->app_logo : ''
                    );
                }
            }else{
                foreach ($event->sponsors()->where('type', 0)->get() as $sponsor) {
                    if (!empty($sponsor->pivot->category_1)) $categories[] = $sponsor->pivot->category_1;
                    if (!empty($sponsor->pivot->category_2)) $categories[] = $sponsor->pivot->category_2;

                    $data[] = array(
                        'id' => $sponsor->id,
                        'company' => $sponsor->name,
                        'logo' => !empty($sponsor->logo) ? config('app.url').'uploads/sponsors/'.$sponsor->logo : '',
                        'description' => $sponsor->info,
                        'website' => $sponsor->website,
                        'documents' => array(),
                        'stand_number' => $sponsor->pivot->stand_number,
                        'categories' => $categories,
                        'advert' => (!empty($sponsor->app_logo) && $event->eventType->app_sponsors_active) ? config('app.url').'uploads/sponsors/'.$sponsor->app_logo : ''
                    );
                }
            }
            if($event->eventType->has_sponsor) {
                
                $today = $event->event_date_from;
                $quarterly_sponsors = SponsorSetting::where('date_from', '<=', $today)->where('date_to', '>=', $today)->get();
                $sponsors = Sponsor::all();
                
                foreach ($quarterly_sponsors as $qs) {
                        
                    for ($i=1; $i <= 10; $i++) {
                        $logo_id = 'logo_'.$i;
                        $sponsor = $sponsors->find($qs->{$logo_id});
                        
                        if (!empty($sponsor)) {
                            $data[] = array(
                                'id' => $sponsor->id,
                                'company' => $sponsor->name,
                                'logo' => !empty($sponsor->logo) ? config('app.url').'uploads/sponsors/'.$sponsor->logo : '',
                                'description' => $sponsor->info,
                                'website' => $sponsor->website,
                                'documents' => array(),
                                'stand_number' => '',
                                'advert' => (!empty($sponsor->app_logo) && $event->eventType->app_sponsors_active) ? config('app.url').'uploads/sponsors/'.$sponsor->app_logo : ''
                            );
                        }
                    }
                }
            }
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * Get all event exhibitors
     * 
     * @param int event id
     * @return json array of sponsors for the event
     */
    public function eventExhibitors($event_id)
    {
        try {
            $event = Event::findOrFail($event_id);
            $data = array();
            
            foreach ($event->sponsors as $sponsor) {
                $data[] = array(
                    'id' => $sponsor->id,
                    'company' => $sponsor->name,
                    'logo' => !empty($sponsor->logo) ? config('app.url').'uploads/sponsors/'.$sponsor->logo : '',
                    'description' => $sponsor->info,
                    'website' => $sponsor->website,
                    'documents' => array(),
                    'stand_number' => $sponsor->pivot->stand_number,
                    'advert' => (!empty($sponsor->app_logo) && $event->eventType->app_sponsors_active) ? config('app.url').'uploads/sponsors/'.$sponsor->app_logo : ''
                );
            }
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    /**
     * Get sponsor details
     * 
     * @param int sponsor_id
     * @return json array of sponsor details
     */
    public function sponsor($sponsor_id)
    {
        try {
            $sponsor = Sponsor::findOrFail($sponsor_id);
            $data = array();
            
            $data = array(
                'id' => $sponsor->id,
                'company' => $sponsor->name,
                'logo' => !empty($sponsor->logo) ? config('app.url').'uploads/sponsors/'.$sponsor->logo : '',
                'description' => $sponsor->info,
                'website' => $sponsor->website,
                'documents' => array(),
                'stand_number' => '',
                'categories' => array(),
                'advert' => (!empty($sponsor->app_logo)) ? config('app.url').'uploads/sponsors/'.$sponsor->app_logo : ''
            );
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    /**
     * Get event all FAQ
     * 
     * @param int event_id
     * @return json array of faqs
     */
    public function eventFaqs($event_id)
    {
        try {
            $event = Event::findOrFail($event_id);
            $data = array();
            
            foreach (
            $event->eventFaqs()->where(function($q){
                $q->where('visibility', 1)->orWhere('visibility', 3);
            })->orderBy('position')->get() as $faq) {
                $data[] = array(
                    'id' => $faq->id,
                    'question' => $faq->question,
                    'answer' => $faq->answer
                );
            }
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    /**
     * Get FAQ details
     * 
     * @param int faq_id
     * @return json array of faq information
     */
    public function faq($faq_id)
    {
        try {
            $faq = EventFaq::findOrFail($faq_id);
            $data = array();
            
            $data[] = array(
                'id' => $faq->id,
                'question' => $faq->question,
                'answer' => $faq->answer

            );
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    /**
     * Get event all speakers
     * 
     * @param int event_id
     * @return json array of speakers
     */
    public function getSpeakers($event_id)
    {
        try {
            $event = Event::findOrFail($event_id);
            $data = array();
            
            $sessions = $event->scheduleSessions()->with('contributors')->get();
            $speakers_pushed = array();
            
            foreach ($sessions as $s) {
                
                foreach ($s->contributors as $speaker) {
                    
                    if(in_array($speaker->id, $speakers_pushed)) continue;
                    
                    $data[] = array(
                    'id' => $speaker->id,
                    'first_name' => $speaker->first_name,
                    'last_name' => $speaker->last_name,
                    'description' => $speaker->bio,
                    'picture' => !empty($speaker->profile_image) ? config('app.url').'uploads/profile_images/'.$speaker->profile_image : '',
                    );
                    
                    array_push($speakers_pushed, $speaker->id);
                }
            }
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * Get event all localcommittee
     * 
     * @param int event_id
     * @return json array of comittees
     */
    public function getLocalcommittee($event_id)
    {
        try {
            $event = Event::findOrFail($event_id);
            $data = array();
            
            $committee = $event->atendees()->where('registration_status_id', config('registrationstatus.booked'))->where('comitee', 1)->get();
            
            foreach ($committee as $speaker) {
                
                $data[] = array(
                'id' => $speaker->id,
                'first_name' => $speaker->first_name,
                'last_name' => $speaker->last_name,
                'description' => $speaker->bio,
                'picture' => !empty($speaker->profile_image) ? config('app.url').'uploads/profile_images/'.$speaker->profile_image : '',
                );
            }
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    /**
     * Exhibition floor plan if event type as exhibition
     * 
     * @param int id event id
     */
    public function floorPlan($id)
    {
        try {
            $event = Event::findOrFail($id);
            $data = array();
            
            if ($event->eventType->exhibition && !empty($event->floor_plan)) $data[] = config('app.url').'uploads/files/'.$event->floor_plan;
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
}
