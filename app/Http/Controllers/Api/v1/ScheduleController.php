<?php namespace App\Http\Controllers\Api\v1;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\EventAttendee;
use App\Event;
use App\User;
use App\EventScheduleSlotSession;
use App\SessionType;
use App\EventAtendeeFavouriteSession;

use App\Http\Controllers\Controller;
use JWTAuth;

class ScheduleController extends Controller {

	/**
     * Get event sessions with slot information
     * 
     * @param int event_id
     * @return json array of schedule sessions
     */
    public function eventSessions($event_id)
    {
        try {
            $event = Event::findOrFail($event_id);
            $data = array();
            
            foreach ($event->scheduleSessions()->with('slot.attendees', 'slot.event')->orderBy('session_start')->get() as $session) {
                
                $data[] = array(
                    'id' => $session->id,
                    'name' => $session->title,
                    'date' => ($session->slot->start_date == '0000-00-00') ? $session->slot->event->event_date_from : $session->slot->start_date,
                    'start' => $session->session_start,
                    'end' => $session->session_end,
                    'type' => $session->type->title,
                    'description' => $session->description,
                    'learning_objectives' => $session->learning_objectives,
                    'location' => (sizeof($session->meetingSpace)) ? $session->meetingSpace->meeting_space_name : '',
                    'Timeslot' => array(
                        'title' => $session->slot->title,
                        'bookable' => $session->slot->bookable ? true : false,
                        'capacity' => $session->slot->slot_capacity,
                        'booked' => $session->slot->attendees()->where('registration_status_id', config('registrationstatus.booked'))->count()
                    )
                );
            }
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * Get schedule session information
     * 
     * @param int session_id
     * @return json session object
     */
    public function getSession($session_id)
    {
        try {
            $session = EventScheduleSlotSession::findOrFail($session_id);
            $data = array();

            $data = array(
                'name' => $session->title,
                'date' => ($session->slot->start_date == '0000-00-00') ? $session->slot->event->event_date_from : $session->slot->start_date,
                'start' => $session->session_start,
                'end' => $session->session_end,
                'type' => $session->type->title,
                'description' => $session->description,
                'learning_objectives' => $session->learning_objectives,
                'location' => (sizeof($session->meetingSpace)) ? $session->meetingSpace->meeting_space_name : '',
                'Timeslot' => array(
                    'title' => $session->slot->title,
                    'bookable' => $session->slot->bookable ? true : false,
                    'capacity' => $session->slot->slot_capacity,
                    'booked' => $session->slot->attendees()->where('registration_status_id', config('registrationstatus.booked'))->count()
                )
            );
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * Get session speakers
     * 
     * @param int session_id
     * @return json session speakers list
     */
    public function sessionsSpeakers($session_id)
    {
        try {
            $session = EventScheduleSlotSession::findOrFail($session_id);
            $data = array();
            
            foreach ($session->contributors()->orderBy('last_name')->get() as $speaker) {
                $data[] = array(
                    'id' => $speaker->id,
                    'first_name' => $speaker->first_name,
                    'last_name' => $speaker->last_name,
                    'description' => $speaker->bio,
                    'role' => $speaker->pivot->type,
                    'picture' => !empty($speaker->profile_image) ? config('app.url').'uploads/profile_images/'.$speaker->profile_image : '',
                    'documents' => array(
                        'id' => '',
                        'name' => '',
                        'url' => ''
                    )
                );
            }
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    /**
     * Get speaker information
     * 
     * @param int speaker_id
     * @return json speaker information
     */
    public function speaker($speaker_id)
    {
        try {
            $speaker = User::findOrFail($speaker_id);
            $data = array();
            
            $data = array(
                'id' => $speaker,
                'first_name' => $speaker->first_name,
                'last_name' => $speaker->last_name,
                'description' => $speaker->bio,
                'picture' => !empty($speaker->profile_image) ? config('app.url').'uploads/profile_images/'.$speaker->profile_image : '',
                'documents' => array(
                    'id' => '',
                    'name' => '',
                    'url' => ''
                )
            );
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    /**
     * Get session comitees
     * 
     * @param int session_id
     * @return json list of comitees
     */
    public function sessionComittees($session_id)
    {
        try {
            $session = EventScheduleSlotSession::findOrFail($session_id);
            $data = array();
            $event = $session->slot->event;
            $event_comittees = array();
            
            if (sizeof($event)) {
                
                $event_comittees = $event->atendees()->where('registration_status_id', config('registrationstatus.booked'))->where('comitee', 1)->lists('user_id');
            }
            
            foreach ($session->contributors()->whereIn('user_id', $event_comittees)->orderBy('last_name')->get() as $speaker) {
                
                $data = array(
                    'id' => $speaker->id,
                    'first_name' => $speaker->first_name,
                    'last_name' => $speaker->last_name,
                    'description' => $speaker->bio,
                    'picture' => !empty($speaker->profile_image) ? config('app.url').'uploads/profile_images/'.$speaker->profile_image : '',
                    'documents' => array(
                        'id' => '',
                        'name' => '',
                        'url' => ''
                    )
                );
            }
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    /**
     * Get comiteedetails
     * 
     * @param int comitee_id
     * @return json comitee details
     */
    public function comitee($comitee_id)
    {
        try {
            $comitee = User::findOrFail($comitee_id);
            $data = array();
            
            $data = array(
                'id' => $comitee->id,
                'first_name' => $comitee->first_name,
                'last_name' => $comitee->last_name,
                'description' => $comitee->bio,
                'picture' => !empty($comitee->profile_image) ? config('app.url').'uploads/profile_images/'.$comitee->profile_image : '',
                'documents' => array(
                    'id' => '',
                    'name' => '',
                    'url' => ''
                )
            );
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }

    public function multidaySchedule($event_id)
    {
        try {
            $event = Event::findOrFail($event_id);

            $day_slots = \Cache::remember('day-slots-apiv1.' . $event->id, 1, function () use ($event) {
                $day_slots = array();
                $date = $event->event_date_from;

                while (strtotime($date) <= strtotime($event->event_date_to)) {
                    $day = date("Y-m-d", strtotime($date));
                    //$day_slots[$day] = $event->scheduleTimeSlots()->where('start_date', $day)->orderBy('start')->with('sessions.type')->get();

                    foreach ($event->scheduleTimeSlots()->where('start_date', $day)->orderBy('start')->with('sessions.type')->get() as $slot) {
                        // form slot's sessions array
                        $slot_sessions = array();
                        foreach ($slot->sessions()->orderBy('session_start')->with('type', 'contributors')->get() as $session) {

                            $contributors = array();

                            foreach ($session->contributors as $speaker) {
                                $contributors[] = array(
                                    'id' => $speaker->id,
                                    'first_name' => $speaker->first_name,
                                    'last_name' => $speaker->last_name,
                                    'description' => $speaker->bio,
                                    'picture' => !empty($speaker->profile_image) ? config('app.url').'uploads/profile_images/'.$speaker->profile_image : '',
                                    'documents' => array(
                                        'id' => '',
                                        'name' => '',
                                        'url' => ''
                                    )
                                );
                            }

                            $slot_sessions[] = array(
                                'id' => $session->id,
                                'name' => $session->title,
                                'start' => date("H:i", strtotime($session->session_start)),
                                'end' => date("H:i", strtotime($session->session_end)),
                                'show_start' => ($session->show_start) ? true : false,
                                'show_end' => ($session->show_end) ? true : false,
                                'type' => $session->type->title,
                                'description' => $session->description,
                                'learning_objectives' => $session->learning_objectives,
                                'selectable' => ($session->selectable) ? true : false,
                                'capacity' => $session->capacity,
                                'meeting_space' => (sizeof($session->meetingSpace)) ? $session->meetingSpace->meeting_space_name : '',
                                'has_contributors' => sizeof($session->contributors) ? true : false,
                                'contributors' => $contributors,
                                'color' => isset($session->type->color) ? $session->type->color : '',
                            );
                        }

                        $day_slots[$day][] = array(
                            'id' => $slot->id,
                            'start' => date("H:i", strtotime($slot->start)),
                            'end' => date("H:i", strtotime($slot->end)),
                            'show_start' => ($slot->show_start) ? true : false,
                            'show_end' => ($slot->show_end) ? true : false,
                            'sessions' => $slot_sessions
                        );
                    }
                    $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
                }

                return $day_slots;
            });

            return response()->json(['success' => true, 'data' => $day_slots]);
        } catch (\Exception $e) {
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * @param Request $request
     * @param $event_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function favouriteSessionClicked(Request $request, $event_id)
    {
        try {
            if (!$request->has('checked')) return response()->json(['error' => 'checked parameter is required'], 400);
            $event = Event::findOrFail($event_id);
            $registration = EventAttendee::where('event_id', $event_id)->where('user_id', auth()->user()->id)->first();
            // find new selected session
            $session = EventScheduleSlotSession::findOrFail($request->get('session_id'));

            $crossing_sessions = array();
            // find if registration has selected session within slot or create new instance
            if ($request->get('checked') !== 'true') {
                // if deselected remove
                EventAtendeeFavouriteSession::where('event_attendee_id', $registration->id)
                    ->where('slot_session_id', $session->id)
                    ->delete();

            } else {
                if ($session->selectable) {
                    // find sessions that are within selected session time and in the same slot and remove them. only 1 session can be selected within time range
                    $crossing_sessions = EventScheduleSlotSession::where('event_schedule_slot_id', $session->event_schedule_slot_id)
                        ->where(function($o) use($session) {
                            $o->where(function($q) use($session) {
                                // for sessions that are longer than selected session
                                $q->where('session_start', '<=', $session->session_start);
                                $q->where('session_end', '>=', $session->session_end);
                            });
                            $o->orWhere(function($c) use ($session){
                                // for sessions that are shorter than selected session
                                $c->where('session_start', '>=', $session->session_start);
                                $c->where('session_end', '<=', $session->session_end);
                            });
                            // if start or end is within selected session. Just a small part overlaps
                            $o->orWhere(function($n) use($session) {
                                $n->where('session_end', '>', $session->session_start);
                                $n->where('session_start', '<', $session->session_start);
                            });
                            $o->orWhere(function($m) use($session) {
                                $m->where('session_start', '<', $session->session_end);
                                $m->where('session_end', '>', $session->session_end);
                            });
                        })
                        ->where('selectable', 1)
                        ->where('id', '<>', $session->id)
                        ->lists('id');

                    // remove crossing selections
                    if (sizeof($crossing_sessions)) EventAtendeeFavouriteSession::where('event_attendee_id', $registration->id)->whereIn('slot_session_id', $crossing_sessions)->delete();

                    // save new selection
                    EventAtendeeFavouriteSession::firstOrCreate([
                        'event_schedule_slot_id' => $session->event_schedule_slot_id,
                        'event_attendee_id' => $registration->id,
                        'slot_session_id' => $session->id
                    ]);
                }
            }

            return response()->json($crossing_sessions, 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    public function personalSchedule($event_id)
    {
        try {
            $event = Event::findOrFail($event_id);
            // form personal schedule
            $registration = EventAttendee::where('event_id', $event_id)->where('user_id', auth()->user()->id)->first();
            $favourite_sessions = $registration->favouriteSessions->lists('slot_session_id');
            $sessions = EventScheduleSlotSession::whereIn('id', $favourite_sessions)->with('type', 'contributors', 'slot')->orderBy('session_start')->get();
            $personal_schedule = array();

            foreach ($sessions as $session) {
                $personal_schedule[$session->slot->start_date][] = array(
                    'id' => $session->id,
                    'name' => $session->title,
                    'start' => date("H:i", strtotime($session->session_start)),
                    'end' => date("H:i", strtotime($session->session_end)),
                    'show_start' => ($session->show_start) ? true : false,
                    'show_end' => ($session->show_end) ? true : false,
                    'type' => $session->type->title,
                    'description' => $session->description,
                    'learning_objectives' => $session->learning_objectives,
                    'selectable' => ($session->selectable) ? true : false,
                    'capacity' => $session->capacity,
                    'meeting_space' => (sizeof($session->meetingSpace)) ? $session->meetingSpace->meeting_space_name : '',
                    'has_contributors' => sizeof($session->contributors) ? true : false,
                );
            }

            return response()->json(['success'=>true, 'data' => $personal_schedule], 200);
        } catch (\Exception $e) {
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 400);
        }
    }

    /**
     * Get CPD time for event
     * 
     * @param int event_id
     * @return json cpd time 
     */
    public function cpd($event_id)
    {
        try {
            $event = Event::findOrFail($event_id);
            $data = array();
            $duration = 0;
            
            // get session types which are used in CPD hours calculation
            $cpd_session_types = SessionType::where('include_in_cpd', 1)->lists('id');
            
            foreach($event->scheduleSessions as $s) {
                
                if(in_array($s->session_type_id, $cpd_session_types)) {
                    
                    $duration += (strtotime($s->session_end) - strtotime($s->session_start))/60;
                }
            }
            
            $data = array(
                'cpd_time' => $duration,
            );
            
            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
}
