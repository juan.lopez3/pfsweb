<?php namespace App\Http\Controllers\Api\v1;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Validator;
use Hash;
use Mail;
use App\User;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use Illuminate\Http\Request;

class AuthenticateController extends Controller {
    
    
    public function login() {

        $user = User::find(5);
        $token = JWTAuth::fromUser($user);
        
        dd($token);
        
        return response()->json([], 200);
    }
    public function test() {
        
        echo ":)";
    }

    /** Main authentication method
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
	public function authenticate(Request $request)
    {
        // grab credentials from the request
        try {
            $user = User::where('email', $request->get('email'))->first();

            if (!sizeof($user)) return response()->json(['error' => 'invalid_credentials'], 401);

            // for members authenticate with pin, for non members authenticate with password
            if ($user->role_id == config('roles.member')) {
                if ($user->pin != $request->get('pin')) return response()->json(['error' => 'invalid_credentials'], 401);
            } else {
                if (!Hash::check($request->get('pin'), $user->password)) return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        
        $token = JWTAuth::fromUser($user);
        
        $data = array(
            'id' => $user->id,
            'name' => $user->first_name,
            'lastname' => $user->last_name,
            'badge_name' => $user->badge_name,
            'email' => $user->email,
            'cc_email' => $user->cc_email,
            'dietary_requirement_id' => $user->dietary_requirement_id,
            'dietary_requirement_other' => $user->dietary_requirement_other,
            'special_requirements' => $user->special_requirements,
            'postcode' => $user->postcode,
            'company' => $user->company,
            'title' => $user->title,
            'mobilePhone' => $user->mobile,
            'contactPhone' => $user->phone,
            'attendee_types' => $user->attendeeTypes->lists('id'),
            'token' => $token
        );
        
        // all good so return the token
        return response()->json($data);
    }
    
    public function authrequest()
    {
        return view('authrequest');
    }
    
    /**
     * Checks if token is valid
     */
    public function checkToken($token)
    {
        try {
            $user = JWTAuth::toUser($token);
            
            if (sizeof($user)) {
                return response()->json(['success' => true]);
            } else {
                return response()->json(['success' => false]);
            }
            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
    
    public function logout($token)
    {
        try {
            JWTAuth::invalidate($token);
            return response()->json(['success' => true]);
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Send shortcode for one time login to user
     */
    public function oneTimeLogin(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'email' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['success'=>false, 'error' => $validator->messages()], 400);
        }

        try {
            $user = User::where('email', $request->get('email'))->first();
            if(!sizeof($user)) return response()->json(['error' => 'User with '. $request->get('email').' was not found in the system'], 400);

            // if user found send him shortcode for one time login
            $shortcode = strtoupper(str_random(6));
            $user->login_shortcode = $shortcode;
            $user->login_shortcode_set = date("Y-m-d H:i:s");
            $user->save();

            // send email
            Mail::send('emails.shortcode', ['user' => $user, 'shortcode'=>$shortcode], function ($m) use ($user) {
                $m->to($user->email, $user->email)
                    ->subject('One time authentication short code');
            });

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Login using shortcode
     */
    public function shortcodeLogin(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'email' => 'required',
            'shortcode' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['success'=>false, 'error' => $validator->messages()], 400);
        }

        try {
            $user = User::where('email', $request->get('email'))->first();
            if (!sizeof($user)) return response()->json(['error' => 'User with email '.$request->get('email').' not found'], 400);

            $set_date = new \DateTime($user->login_shortcode_set);
            $set_date->modify("+12 hours");
            $set_date = $set_date->format("Y-m-d H:i:s");

            if (empty($user->login_shortcode_set) || $user->login_shortcode !== $request->get('shortcode') || $user->login_shortcode_set > date("Y-m-d H:i:s")) return response()->json(['error' => 'Shortcode used or expired.'], 400);
            $user->login_shortcode = null;
            $user->login_shortcode_set = null;
            $user->save();

            $token = JWTAuth::fromUser($user);

            $data = array(
                'id' => $user->id,
                'name' => $user->first_name,
                'lastname' => $user->last_name,
                'badge_name' => $user->badge_name,
                'email' => $user->email,
                'cc_email' => $user->cc_email,
                'dietary_requirement_id' => $user->dietary_requirement_id,
                'dietary_requirement_other' => $user->dietary_requirement_other,
                'special_requirements' => $user->special_requirements,
                'postcode' => $user->postcode,
                'company' => $user->company,
                'title' => $user->title,
                'mobilePhone' => $user->mobile,
                'contactPhone' => $user->phone,
                'attendee_types' => $user->attendeeTypes->lists('id'),
                'token' => $token
            );

            // all good so return the token
            return response()->json($data);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Password reset
     */
    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['success'=>false, 'error' => $validator->messages()], 400);
        }

        try {
            $user = auth()->user();
            $user->password = bcrypt($request->get('password'));
            $user->save();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
