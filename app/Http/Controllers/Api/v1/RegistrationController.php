<?php namespace App\Http\Controllers\Api\v1;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

use Auth;
use Illuminate\Http\Request;
use App\Event;
use App\AttendeeType;
use App\EventAttendeeSlotSession;
use App\EventAttendee;
use App\User;
use App\EmailTemplate;

use App\DietaryRequirement;
use App\DietaryRequirementNew;
use App\EventScheduleSlotQuestion;
use App\RegistrationAnswer;
use App\Commands\SendEmail;
use App\Events\DataWasManipulated;

class RegistrationController extends Controller {

	public function getRegistrationFields($event_id, $user_id)
    {
        try {
            $event = Event::findOrFail($event_id);
            $user = User::findOrFail($user_id);
            
            $step_1 = array(
                array(
                    'input_type' => 'text',
                    'field_name' => 'badge_name',
                    'field_display_name' => 'Badge Name',
                    'required' => false,
                    'value' => $user->badge_name,
                ),
                array(
                    'input_type' => 'select',
                    'field_name' => 'dietary_requirement_id',
                    'field_display_name' => 'Dietary Requirements',
                    'required' => true,
                    'options' => DietaryRequirementNew::lists('title', 'id'),
                    'value' => $user->dietary_requirement_id,
                ),
                array(
                    'input_type' => 'text',
                    'field_name' => 'dietary_requirement_other',
                    'field_display_name' => 'other',
                    'required' => false,
                    'value' => $user->dietary_requirement_other,
                ),
                array(
                    'input_type' => 'text',
                    'field_name' => 'special_requirements',
                    'field_display_name' => 'Special Requirements',
                    'required' => false,
                    'value' => $user->special_requirements,
                ),
                array(
                    'input_type' => 'text',
                    'field_name' => 'cc_email',
                    'field_display_name' => 'CC Email',
                    'required' => false,
                    'value' => $user->cc_email,
                ),
                array(
                    'input_type' => 'checkbox',
                    'field_name' => 'attendee_types[]',
                    'field_display_name' => 'My role is mainly:',
                    'required' => 'required',
                    'options' => AttendeeType::where('id', '>', 11)->lists('title', 'id')
               ),
            );
            
            $data['Confirm Details'] = $step_1;
            
            // step 2 slot questions
            $schedule_slots = $event->scheduleTimeSlots()->lists('id');
            $slot_questions = EventScheduleSlotQuestion::whereIn('event_schedule_slot_id', $schedule_slots)->get();
            $additional_questions = array();
            $counter = 0;

            foreach ($slot_questions as $question) {
                $additional_questions[] = array(
                    'input_type' => 'hidden',
                    'field_name' => 'question_'.$counter,
                    'value' => $question->question
                );
                
                $additional_questions[] = array(
                    'input_type' => $question->type,
                    'field_name' => 'registration_answer_'.$counter,
                    'field_display_name' => $question->question,
                    'required' => false,
                    'options' => explode(",", $question->answers),
                    'slot_id' => $question->event_schedule_slot_id,
                    'available_answer' => $question->available
                );

                $counter++;
            }
            
            if (!empty($additional_questions)) $data['Additional Questions'] = $additional_questions;
            
            // step 3 bookable slots
            $bookable_slots = array();
            $user_attendee_types = $user->attendeeTypes->lists('id');
            
            foreach ($event->scheduleTimeSlots()->where('bookable', 1)->where('stop_booking', 0)->orderBy('start')->get() as $slot) {
                
                $available = false;
                $slot_attendee_types = $slot->attendeeTypes->lists('id');
                
                foreach($user_attendee_types as $uat) {
                    
                    // if attendee has at least 1 attende type assigned to slot attendee types
                    if(in_array($uat, $slot_attendee_types)) {
                        $available = true;
                        break;
                    }
                }
                
                //check chartered status if attendee type not found
                if(!$available)
                    if($slot->available_chartered && Auth::user()->chartered) $available = true;
                
                // user doesn't have attendee type that is related with slots then don't give option to register for that slot
                if(!$available) continue;
                
                $full = ($slot->slot_capacity <= $slot->attendees->count() || $slot->stop_booking) ? true : false;
                
                $bookable_slots[] = array(
                    'slot_id' => $slot->id,
                    'input_type' => 'checkbox',
                    'field_name' => 'sessions[]',
                    'field_display_name' => date("H:i", strtotime($slot->start)).' - '.date("H:i", strtotime($slot->end)).' '.$slot->title,
                    'value' => $slot->id,
                    'required' => false,
                    'full' => $full
                );
            }
            
            $data['Select Sessions'] = $bookable_slots;
            $counter = 0;
            
            // Event type forms
            foreach ($event->eventType->typeSteps as $step) {
                
                if (empty($step->form->form_json)) continue;
                
                $step_fields = array();
                
                foreach(json_decode($step->form->form_json) as $field) {
                    
                    $options = array();
                    
                    if (!empty($field->field_options) && !empty($field->field_options->options)) {
                        foreach ($field->field_options->options as $opt) {
                            $options[$opt->label] = $opt->label;
                        }
                        
                    }
                    
                    $step_fields[] = array(
                        'input_type' => 'hidden',
                        'field_name' => 'dyn_question_'.$counter,
                        'value' => $field->label
                    );
                    
                    $step_fields[] = array(
                        'input_type' => $field->field_type,
                        'field_name' => 'dyn_answer_'.$counter,
                        'field_display_name' => $field->label,
                        'required' => $field->required,
                        'options' => $options
                    );
                    
                    $counter++;
                    //dd(json_decode($step->form->form_json));
                }
                
                $data[$step->title] = $step_fields;
            }
            
            
            // CONFIRM AND REGISTER
            $final_step = array(
                array(
                    'input_type' => 'option',
                    'field_name' => 'sponsors_contact',
                    'field_display_name' => 'These CPD events are made possible by working with the support of ‘Partners in Professionalism’. Please confirm if you are happy to be contacted by the participating Partners in relation to business relevant support arising from the subject matter of this event',
                    'required' => TRUE,
                    'options' => array('No', 'Yes')
                ),
                array(
                    'input_type' => 'option',
                    'field_name' => 'first_time_attendee',
                    'field_display_name' => 'Is this the first time you have attended a PFS event?',
                    'required' => TRUE,
                    'options' => array('No', 'Yes')
                ),
                array(
                    'input_type' => 'email',
                    'field_name' => 'invitees',
                    'field_display_name' => 'If you would like send details of this event to a colleague please enter their email address',
                    'placeholder' => 'To enter multiple email addresses, please separate each address with a comma',
                    'required' => FALSE
                ),
                array(
                    'input_type' => 'checkbox',
                    'field_name' => 'terms_and_conditions',
                    'field_display_name' => 'I agree with terms & conditions',
                    'required' => TRUE,
                    'value' => '1'
                ),
                
            );
            
            $data['Confirm & Register'] = $final_step;
            
            $registration_texts = array();
            $registration_texts['event_registration_text'] = $event->registration_text;

            $default_term_and_conditions = \Config::get('termsandconditions.default');
            $registration_texts['terms_conditions'] = !empty($event->eventType->terms_conditions) ? $event->eventType->terms_conditions : $default_term_and_conditions;
            
            return response()->json(['success'=>true, 'registration_fields'=>$data, 'registration_texts'=>$registration_texts]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }

    public function submitRegistration(Request $request, $event_id, $user_id)
    {
        try {
            
            $validator = Validator::make($request->input(), [
                'dietary_requirement_id' => 'required',
                'first_time_attendee' => 'required',
                'sponsors_contact' => 'required'
            ]);
            
            if ($validator->fails()) {
                
                return response()->json(['success'=>false, 'error' => $validator->messages()], 400);
            }
            
            $user = User::findOrFail($user_id);
            $event = Event::findOrFail($event_id);
            $waitlisted = false;
            
            if ($event->invite_only) {
                
                $event_attendee = EventAttendee::where('event_id', $event->id)
                    ->where('user_id', $user->id)
                    ->where('registration_status_id', config('registrationstatus.invited'))->first();
                
                // if event is invite only and user is not invited don't show this page
                if (sizeof($event_attendee)) {
                    // clear existing registration before booking
                    $event_attendee->delete();
                } else {
                    return response()->json(['success'=>false, 'error' => 'User is not invited to this event'], 500);
                }
            }
            
            # non members are allowed to register just for 2 event at the time
            if(hasRoles($user->role_id, [role('non_member')]) && $event->id != 680) {
                
                $event_year = date("Y", strtotime($event->event_date_from));
                
                //check how many active registrations user has
                $registrations = EventAttendee::where('user_id', $user->id)
                    ->where('created_at', '>=', $event_year.'-01-01')
                    ->where('created_at', '<=', $event_year.'-12-31')
                    ->whereIn('registration_status_id', [config('registrationstatus.booked'), config('registrationstatus.waitlisted')])
                    ->count();
                
                if($registrations >= 2) {
                    return response()->json(['success'=>false, 'error' => 'Non PFS members are entitled to register for 2 events per year'], 400);
                }
            }
            

            # update confirmed user data
            //$user->title = $request->get('title');
            $user->badge_name = $request->get('badge_name');
            //$user->first_name = $request->get('first_name');
            //$user->last_name = $request->get('last_name');
            $user->dietary_requirement_id = $request->get('dietary_requirement_id');
            $user->dietary_requirement_other = $request->get('dietary_requirement_other');
            $user->special_requirements = $request->get('special_requirements');
            //$user->email = $request->get('email');
            $user->cc_email = $request->get('cc_email');
            //$user->postcode = $request->get('postcode');
            //$user->company = $request->get('company');
            $user->save();
            
            $current_attendee_types = $user->attendeeTypes()->where('attendee_types.id', '<=', 11)->lists('id');
            $attendee_types = $request->has('attendee_types') ? $request->get('attendee_types') : array();
            $attendee_types = $current_attendee_types + $attendee_types;
            $user->attendeeTypes()->sync($attendee_types);
            
            $booked_sessions = [];
            $duration = 0;
            $sessions_text = "";

            // check if user has registration for this event
            $event_attendees_cancel_reason =  EventAttendee::where('user_id', $user->id)->where('event_id', $event->id)->first();

            if(sizeof($event_attendees_cancel_reason)) {
            // $event_attendees_cancel_reason = new EventAttendee();
            $event_attendees_cancel_reason->cancel_reason = NULL;
            $event_attendees_cancel_reason->save();
            }

            $event_attendee = $event->atendees()->find($user->id);
            if(!sizeof($event_attendee)) {
                    
                $rr = $user->events()->attach($event->id, [
                    'sponsors_contact'=>$request->get('sponsors_contact'),
                    'first_time_attendee' => $request->get('first_time_attendee'),
                    'api_booking' => 1,
                    'created_at'=>date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s")]); #booked
            }
            
            $event_attendee = $event->atendees()->find($user->id);
            $event_attendee_id = $event_attendee->pivot->id;
            
            //save slot questions
            $question_id = 0;
            
            while($request->has('question_'.$question_id)){
                // if doesn't have answer just continue
                if (!$request->has('registration_answer_'.$question_id)) {
                    $question_id++;
                    continue;
                }
                $answer = new RegistrationAnswer();
                $answer->event_id = $event->id;
                $answer->event_atendee_id = $event_attendee_id;
                $answer->question = $request->get('question_'.$question_id);
                $answer->answer = $request->get('registration_answer_'.$question_id);
                $answer->save();
                
                $question_id++;
            }
            
            //save event type questions
            $question_id = 0;
            
            while($request->has('dyn_question_'.$question_id)){
                // if doesn't have answer just continue
                if (!$request->has('dyn_answer_'.$question_id)) {
                    $question_id++;
                    continue;
                }
                $answer = new RegistrationAnswer();
                $answer->event_id = $event->id;
                $answer->event_atendee_id = $event_attendee_id;
                $answer->question = $request->get('dyn_question_'.$question_id);
                $answer->answer = $request->get('dyn_answer_'.$question_id);
                $answer->save();
                
                $question_id++;
            }
            
            if($request->has('sessions')){
            
                foreach ($request->get('sessions') as $session) {
                    
                    $slot = $event->scheduleTimeSlots()->find($session);
                    
                    if($event->registration_type_id == config('registrationtype.waitinglist') && $slot->slot_capacity <= $slot->attendees()->whereIn('registration_status_id', [\Config::get('registrationstatus.booked'), \Config::get('registrationstatus.waitlisted')])->count() ) {
                        
                        #full add to waiting list
                        $registration_status = \Config::get('registrationstatus.waitlisted');
                        $waitlisted = true;
                    } else {
                        
                        # there is some space register
                        $registration_status = \Config::get('registrationstatus.booked');
                        $waitlisted = false;
                    }

                    $new_session_slot =  EventAttendeeSlotSession::where('event_attendee_id', $event_attendee_id)->where('event_schedule_slot_id', $session)->first();
                    
                    if(!sizeof($new_session_slot)) { 
                        $new_session_slot = new EventAttendeeSlotSession();
                    }

                    $new_session_slot->event_attendee_id = $event_attendee_id;
                    $new_session_slot->event_schedule_slot_id = $session;
                    $new_session_slot->registration_status_id = $registration_status;
                    $new_session_slot->save();
                    
                }
            } else {
                /* this was commented because the event has no capacity field any longer */
                // if (!$event->capacity 
                //     || $event->capacity >= $event->atendees()->whereIn('registration_status_id', [config('registrationstatus.booked'), config('registrationstatus.waitlisted')])->count()) {
                    $waitlisted = false;
                // } else {
                //     $waitlisted = true;
                // }
            }
            
            # send confirmation email
            //check for email template inside the eventType
        
            if($waitlisted) {
                
                // all sessions waitlisted
                // update event booking status
                $event_attendee->pivot->registration_status_id = \Config::get('registrationstatus.waitlisted');
                $event_attendee->pivot->save();
                
                $template = $event->eventType->emailTemplates->where('id', \Config::get('emailtemplates.registration_confirmation_waitlist'))->first();

            } else {
                
                $event_attendee->pivot->registration_status_id = \Config::get('registrationstatus.booked');
                $event_attendee->pivot->save();

                $template = $event->eventType->emailTemplates->where('id', \Config::get('emailtemplates.registration_confirmation'))->first();
            }
            
            if(sizeof($template) > 0) {
                
                $active = 1;
                $message = $template->template;
                $subject = $template->subject;
                
                //check for custom template
                if($waitlisted) {
                    $custom_template = $event->emailTemplates->where('id', \Config::get('emailtemplates.registration_confirmation_waitlist'))->first();
                } else {
                    $custom_template = $event->emailTemplates->where('id', \Config::get('emailtemplates.registration_confirmation'))->first();
                }
                
                if(sizeof($custom_template) > 0 && $custom_template->pivot->active) {
                    
                    $message = $custom_template->pivot->template;
                    $subject = $custom_template->pivot->subject;
                } elseif(sizeof($custom_template) > 0 && $custom_template->pivot->active == 0) {
                    // don't send
                    $active = 0;
                }
                
                if($active) {
                    
                    $this->dispatch(new SendEmail($user->email, $user->cc_email, $subject, $message, $user, $event));
                }
            }
            
            // send invitations
            $template = EmailTemplate::find(\Config::get('emailtemplates.invitation'));
            
            if ($request->has('invitees') && sizeof($template)) {
                
                $invitees = $request->get('invitees');
                $invitees = str_replace(";", ",", $invitees);
                $invitees = explode(",", $invitees);
                
                foreach($invitees as $invitee) {
                    
                    $email = trim($invitee);
                    
                    if (!empty($email)) {
                        $this->dispatch(new SendEmail($email, null, $template->subject, $template->template, $user, $event));
                    }
                }
            }
            
            event(new DataWasManipulated('eventRegister', $event->title.' ID: '.$event->id));
            
            return response()->json(['success'=>true, 'data'=>array('attendee_id' => $event_attendee_id)]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }

    public function amendRegistration(Request $request, $event_id, $user_id)
    {
        try {
            $user = User::findOrFail($user_id);
            $event = Event::findOrFail($event_id);
            
            $booked_sessions = [];
            $duration = 0;
            $sessions_text = "";
            
            # update confirmed user data
            //$user->title = $request->get('title');
            $user->badge_name = $request->get('badge_name');
            //$user->first_name = $request->get('first_name');
            //$user->last_name = $request->get('last_name');
            $user->dietary_requirement_id = $request->get('dietary_requirement_id');
            $user->dietary_requirement_other = $request->get('dietary_requirement_other');
            $user->special_requirements = $request->get('special_requirements');
            //$user->email = $request->get('email');
            $user->cc_email = $request->get('cc_email');
            //$user->postcode = $request->get('postcode');
            //$user->company = $request->get('company');
            $user->save();
            
            $event_attendee = $event->atendees()->find($user->id);
            //$event_attendee->pivot->registration_status_id = config('registrationstatus.booked');
            //$event_attendee->pivot->save();
            
            $event_attendee_id = $event_attendee->pivot->id;
            
            /**
             * 1) if session unselected - cancelled. If all sessions unselected - event cancelled
             * 2) if new session selected check if there is space. yes - book, 2 - to waiting list
             * 3) 
             */
            //dd(array_diff($old, $new));// find deleted
            //dd(array_diff($new, $old)); // find new
            
            $old_booked_sessions = EventAttendeeSlotSession::where('event_attendee_id',$event_attendee_id)->lists('event_schedule_slot_id');
            $new_booked_sessions = $request->has('sessions') ? $request->get('sessions') : [];
            $new_booked_sessions = array_map('intval', $new_booked_sessions);
            
            
            # find deleted change status to cancelled
            $deleted = array_diff($old_booked_sessions, $new_booked_sessions);
            if(!empty($deleted)) {
                
                $deleted = EventAttendeeSlotSession::where('event_attendee_id',$event_attendee_id)->whereIn('event_schedule_slot_id', $deleted)->get(); #cancelled
                
                foreach($deleted as $d) {
                    
                    $d->registration_status_id = \Config::get('registrationstatus.cancelled');
                    $d->save();
                }
            }
            
            # find new and attach sessions
            $new = array_diff($new_booked_sessions, $old_booked_sessions);
            
            foreach($new as $session) {
                
                $slot = $event->scheduleTimeSlots()->find($session);
                
                if($slot->slot_capacity <= $slot->attendees()->whereIn('registration_status_id', [\Config::get('registrationstatus.booked'), \Config::get('registrationstatus.waitlisted')])->count()) {
                    
                    #full add to waiting list
                    $registration_status = \Config::get('registrationstatus.waitlisted');
                } else {
                    
                    # there is some space register
                    $registration_status = $registration_status = \Config::get('registrationstatus.booked');
                }
                
                $new_session_slot = new EventAttendeeSlotSession();
                $new_session_slot->event_attendee_id = $event_attendee_id;
                $new_session_slot->event_schedule_slot_id = $session;
                $new_session_slot->registration_status_id = $registration_status;
                $new_session_slot->save();
            }
            
            
            # if old cancelled and new booked
            $old_booked_sessions_cancelled = EventAttendeeSlotSession::where('event_attendee_id',$event_attendee_id)->where('registration_status_id', \Config::get('registrationstatus.cancelled'))->lists('event_schedule_slot_id');
            $renew = array_intersect($old_booked_sessions_cancelled, $new_booked_sessions);
            $waitlisted = false;
            
            foreach($renew as $session) {
                
                $slot = $event->scheduleTimeSlots()->find($session);
                
                if($slot->slot_capacity <= $slot->attendees()->whereIn('registration_status_id', [\Config::get('registrationstatus.booked'), \Config::get('registrationstatus.waitlisted')])->count() && $event->registration_type_id == \Config::get('registrationtype.waitinglist')) {
                    
                    #full add to waiting list
                    $registration_status = \Config::get('registrationstatus.waitlisted');
                } else {
                    
                    # there is some space register
                    $registration_status = $registration_status = \Config::get('registrationstatus.booked');
                    $waitlisted = false;
                    $event_attendee->pivot->registration_status_id = config('registrationstatus.booked');
                    $event_attendee->pivot->save();
                }
                
                $update_slot = EventAttendeeSlotSession::where('event_attendee_id', $event_attendee_id)->where('event_schedule_slot_id', $session)->first();
                $update_slot->registration_status_id = $registration_status;
                $update_slot->save();
            }
            
            RegistrationAnswer::where('event_id', $event->id)->where('event_atendee_id', $event_attendee_id)->delete();
            /**
             * SAVE SLOT QUESTIONS
             */
            $question_id = 0;
            
            while($request->has('question_'.$question_id)){
                // if doesn't have answer just continue
                if (!$request->has('registration_answer_'.$question_id)) {
                    $question_id++;
                    continue;
                }
                $answer = new RegistrationAnswer();
                $answer->event_id = $event->id;
                $answer->event_atendee_id = $event_attendee_id;
                $answer->question = $request->get('question_'.$question_id);
                $answer->answer = $request->get('registration_answer_'.$question_id);
                $answer->save();
                
                $question_id++;
            }
            
            /**
             * SAVE EVENT TYPE QUESTIONS
             */
            $question_id = 0;
            
            while($request->has('dyn_question_'.$question_id)){
                // if doesn't have answer just continue
                if (!$request->has('dyn_answer_'.$question_id)) {
                    $question_id++;
                    continue;
                }
                $answer = new RegistrationAnswer();
                $answer->event_id = $event->id;
                $answer->event_atendee_id = $event_attendee_id;
                $answer->question = $request->get('dyn_question_'.$question_id);
                $answer->answer = $request->get('dyn_answer_'.$question_id);
                $answer->save();
                
                $question_id++;
            }
            
            # calculate times and prepare text and send email
            $mail_slots = EventAttendeeSlotSession::where('event_attendee_id', $event_attendee_id)
                ->whereIn('registration_status_id', [\Config::get('registrationstatus.booked'), \Config::get('registrationstatus.waitlisted')])
                ->get();
            
            foreach($mail_slots as $session) {
                
                $slot = $event->scheduleTimeSlots()->find($session->event_schedule_slot_id);
                
                $slot_sessions = $slot->sessions()->with('type')->whereHas('type', function($q){
                    $q->where('include_in_cpd', 1);
                })->get();
                
                foreach($slot_sessions as $ss) {
                    
                    $duration += (strtotime($ss->session_end) - strtotime($ss->session_start))/60;
                }
                
                $sessions_text .= $slot->title."<br/>";
            }
            
            $minutes = $sessions_text % 60;
            $hours = ($duration-$minutes)/60;
            $booked_sessions['sessions_text'] = $sessions_text;
            $booked_sessions['duration'] = $hours." hours and ".$minutes." minutes";
            
            # send confirmation email
            $template = $event->eventType->emailTemplates->where('id', \Config::get('emailtemplates.registration_amendment'))->first();
            
            if(sizeof($template) > 0) {
                
                $active = 1;
                $message = $template->template;
                $subject = $template->subject;
                
                //check for custom template
                $custom_template = $event->emailTemplates->where('id', \Config::get('emailtemplates.registration_amendment'))->first();
                
                if(sizeof($custom_template) > 0 && $custom_template->pivot->active) {
                    
                    $message = $custom_template->pivot->template;
                    $subject = $custom_template->pivot->subject;
                } elseif(sizeof($custom_template) > 0 && $custom_template->pivot->active == 0) {
                    // don't send
                    $active = 0;
                }
                
                if($active) {
                    $this->dispatch(new SendEmail($user->email, $user->cc_email, $subject, $message, $user, $event, $booked_sessions));
                }
            }
            
            // send invitations
            $template = EmailTemplate::find(\Config::get('emailtemplates.invitation'));
            
            if ($request->has('invitees') && sizeof($template)) {
                
                $invitees = explode(",", $request->get('invitees'));
                
                foreach($invitees as $invitee) {
                    
                    $email = trim($invitee);
                    
                    if (!empty($email)) {
                        $this->dispatch(new SendEmail($email, $template->subject, $template->template, $user, $event));
                    }
                }
            }
            
            event(new DataWasManipulated('actionUpdate', 'registration details for event: '.$event->title.' ID: '.$event->id));
            
            
            return response()->json(['success'=>true, 'data'=>'']);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    public function getBookedSlots($event_id, $user_id)
    {
        try {
            $user = User::findOrFail($user_id);
            $event = Event::findOrFail($event_id);
            $event_attendee = $event->atendees()->find($user->id);
            
            if (empty($event_attendee)) return response()->json(['success'=>false, 'error' => 'Registration was not found'], 401);
            
            $event_attendee_id = $event_attendee->pivot->id;
            $booked_slots = EventAttendeeSlotSession::where('event_attendee_id',$event_attendee_id)->get();
            $data = array();
            
            foreach ($booked_slots as $slot) {
                
                $data[] = array(
                    'slot_id' => $slot->event_schedule_slot_id,
                    'registration_status_id' => $slot->registration_status_id
                );
            }
            
            return response()->json(['success'=>true, 'data'=> $data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    public function cancelRegistration(Request $request, $event_id, $user_id)
    {
        try {
            
            $validator = Validator::make($request->input(), [
                'cancel_reason' => 'required',
            ]);
            
            if ($validator->fails()) {
                
                return response()->json(['success'=>false, 'error' => $validator->messages()], 400);
            }
            
            $event = Event::findOrFail($event_id);
            $user = User::findOrFail($user_id);
            $event_attendee = EventAttendee::where('user_id', $user_id)->where('event_id', $event->id)->first();
            
            // if booking cancelles < 48h then its late cancellation
            $cancellation_status = ($event->cut_of_date <= date("Y-m-d") || empty($event->cut_of_date)) ? \Config::get('registrationstatus.late_cancelled') : \Config::get('registrationstatus.cancelled'); 
            $event_attendee->update(['registration_status_id' => $cancellation_status, 'cancel_reason' => $request->get('cancel_reason')]);
            
            foreach(EventAttendeeSlotSession::where('event_attendee_id', $event_attendee->id)->get() as $slot_session) {
                
                $slot_session->registration_status_id = $cancellation_status;
                $slot_session->save();
            }
            
            # send cancel confirm email
            // 5 = late cancellation
            $email_template_id = ($cancellation_status == 5) ? \Config::get('emailtemplates.registration_late_cancel') : \Config::get('emailtemplates.registration_cancel');
            
            $template = $event->eventType->emailTemplates->where('id', $email_template_id)->first();
            
            if(sizeof($template) > 0) {
                
                $active = 1;
                $message = $template->template;
                $subject = $template->subject;
                
                //check for custom template
                $custom_template = $event->emailTemplates->where('id', $email_template_id)->first();
                
                if(sizeof($custom_template) > 0 && $custom_template->pivot->active) {
                    
                    $message = $custom_template->pivot->template;
                    $subject = $custom_template->pivot->subject;
                } elseif(sizeof($custom_template) > 0 && $custom_template->pivot->active == 0) {
                    // don't send
                    $active = 0;
                }
                
                if($active) {
                    $this->dispatch(new SendEmail($user->email, $user->cc_email, $subject, $message, $user, $event));
                }
            }
            
            event(new DataWasManipulated('eventCancel', 'for event: '.$event->title.' ID: '.$event->id));
            
            return response()->json(['success'=>true]);
            
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    
    /**
     * Renew cancelled event registration. Changes event booking status to booked, and clears booked slots, redirects to booking page
     */
    public function renewRegistration(Request $request, $event_id, $user_id)
    {        
        try {
            
            $event_attendee = EventAttendee::where('user_id', $user_id)->where('event_id', $event_id)->first();
            $event_attendee->registration_status_id = \Config::get('registrationstatus.booked');
            $event_attendee->save();
            
            foreach(EventAttendeeSlotSession::where('event_attendee_id', $event_attendee->id)->get() as $slot_session) {
                
                $slot_session->registration_status_id = \Config::get('registrationstatus.booked');
                $slot_session->save();
            }
            
            return response()->json(['success'=>true]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
}
