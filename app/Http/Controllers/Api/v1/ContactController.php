<?php namespace App\Http\Controllers\Api\v1;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\Contact;
use App\ContactRequest;

use App\Http\Controllers\Controller;
use JWTAuth;

class ContactController extends Controller {

	/**
	 * List of user contacts
     * 
     * @param int user_id
	 * @return json array of user contacts
	 */
	public function index($user_id)
	{
		try {
		    $contacts = Contact::where('user_id', $user_id)->with('contact')->get();
            $data = array();
            
            foreach ($contacts as $contact) {
                
                $data[] = array(
                    'id' => $contact->contact->id,
                    'name' => $contact->contact->first_name,
                    'lastname' => $contact->contact->last_name,
                    'email' => $contact->contact->email,
                    'company' => $contact->contact->company,
                    'title' => $contact->contact->title,
                    'mobilePhone' => $contact->contact->mobile,
                    'contactPhone' => $contact->contact->phone
                );
            }

            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
	}
    
    /**
     * List of pending requests
     * 
     * @param int user_id
     * @return json array of pending requests
     */
    public function getContactRequests($user_id)
    {
        try {
            
            $contacts = ContactRequest::where('contact_id', $user_id)->with('user')->get();
            $data = array();
            
            foreach ($contacts as $contact) {
                
                $data[] = array(
                    'id' => $contact->user->id,
                    'name' => $contact->user->first_name,
                    'lastname' => $contact->user->last_name,
                    'email' => $contact->user->email,
                    'company' => $contact->user->company,
                    'title' => $contact->user->title,
                    'mobilePhone' => $contact->user->mobile,
                    'contactPhone' => $contact->user->phone
                );
            }

            return response()->json(['success'=>true, 'data'=>$data]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    /**
     * Create new contact
     *
     * @param  int  $user_id
     * @return boolean if created successfully
     */
    public function addContact(Request $request, $user_id)
    {
        try {
            $validator = Validator::make($request->input(), [
                'userID' => 'required|numeric',
            ]);
            
            if ($validator->fails()) return response()->json(['success'=>false, 'error' => $validator->messages()], 400);
            if ($user_id == $request->get('userID')) return response()->json(['success'=>false, 'error' =>'You can not add a contact with yourself'], 400);
            
            $contact = new Contact();
            $contact->user_id = $user_id;
            $contact->contact_id = $request->get('userID');
            $contact->save();
            
            // create reverse contact relation
            $contact = new Contact();
            $contact->contact_id = $user_id;
            $contact->user_id = $request->get('userID');
            $contact->save();
            
            return response()->json(['success'=>true]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    /**
     * Create new contact request
     *
     * @param  int  $user_id
     * @return boolean if created successfully
     */
    public function createRequest(Request $request, $user_id)
    {
        try {
            $validator = Validator::make($request->input(), [
                'userID' => 'required|numeric',
            ]);
            
            if ($validator->fails()) return response()->json(['success'=>false, 'error' => $validator->messages()], 400);
            if ($user_id == $request->get('userID')) return response()->json(['success'=>false, 'error' =>'You can not create contact request with yourself'], 400);
            
            $c_request = new ContactRequest();
            $c_request->user_id = $user_id;
            $c_request->contact_id = $request->get('userID');
            $c_request->save();
            
            return response()->json(['success'=>true]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    /**
     * Accept contact request
     *
     * @param  int  $user_id
     * @return boolean if created successfully
     */
    public function acceptRequest(Request $request, $user_id)
    {
        try {
            $validator = Validator::make($request->input(), [
                'userID' => 'required|numeric',
            ]);
            
            if ($validator->fails()) {
                
                return response()->json(['success'=>false, 'error' => $validator->messages()], 400);
            }
            
            // create contact
            $contact = new Contact();
            $contact->user_id = $user_id;
            $contact->contact_id = $request->get('userID');
            $contact->save();
            
            // create reverse contact relation
            $contact = new Contact();
            $contact->contact_id = $user_id;
            $contact->user_id = $request->get('userID');
            $contact->save();
            
            // delete request from pending list
            $c_request = ContactRequest::where('user_id', $user_id)->where('contact_id', $request->get('userID'))->first();
            if (!empty($c_request)) $c_request->delete();
            
            return response()->json(['success'=>true]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    /**
     * Decline contact request
     *
     * @param  int  $user_id
     * @return Response
     */
    public function declineRequest(Request $request, $user_id)
    {
        try {
            $validator = Validator::make($request->input(), [
                'userID' => 'required|numeric',
            ]);
            
            if ($validator->fails()) {
                
                return response()->json(['success'=>false, 'error' => $validator->messages()], 400);
            }
            
            $request = ContactRequest::where('user_id', $user_id)->where('contact_id', $request->get('userID'))->first();
            
            if (!empty($request)) $request->delete();
            
            return response()->json(['success'=>true]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }
    
    /**
     * Remove contact
     *
     * @param  int  $user_id
     * @return Response
     */
    public function destroyContact(Request $request, $user_id)
    {
        try {
            $validator = Validator::make($request->input(), [
                'userID' => 'required|numeric',
            ]);
            
            if ($validator->fails()) {
                
                return response()->json(['success'=>false, 'error' => $validator->messages()], 400);
            }
            
            $contact = Contact::where('user_id', $user_id)->where('contact_id', $request->get('userID'))->first();
            
            if (!empty($contact)) $contact->delete();
            
            return response()->json(['success'=>true]);
        } catch(\Exception $e) {
            
            return response()->json(['success'=>false, 'error' => $e->getMessage()], 500);
        }
    }

}
