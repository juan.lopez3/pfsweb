<?php

# OLD VERSION API

# ROUTES THAT NEEDS AUTHENTICATION
Route::group(['prefix' => 'api'], function() {
    Route::group(['middleware' => ['jwt.auth', 'cors']], function() {
        Route::pattern('id', '[0-9]+');
        Route::pattern('id2', '[0-9]+');

        # USER NOTES
        Route::get('users/{id}/notes', 'Api\UserNoteController@index');
        Route::get('users/{id}/notes/{id2}', 'Api\UserNoteController@show');
        Route::post('users/{id}/notes/add', 'Api\UserNoteController@store');
        Route::put('users/{id}/notes/update/{id2}', 'Api\UserNoteController@update');
        Route::delete('users/{id}/notes/remove', 'Api\UserNoteController@destroy');


        # MOBILE APP USER
        Route::get('users/{id}', 'Api\UserController@profile');
        Route::get('users/{id}/myevents', 'Api\UserController@events');
        Route::post('users/{id}/feedback', 'Api\UserController@submitFeedback');
        Route::get('users/{id}/feedback/{id2}', 'Api\UserController@checkFeedback');

        # USER DOCUMENTS
        Route::get('users/{id}/mydocs', 'Api\UserController@userDocuments');
        Route::get('mydocs/{id}', 'Api\UserController@getDocument');
        Route::put('users/{id}/mydocs/add', 'Api\UserController@storeUserDocument');
        Route::delete('users/{id}/mydocs/remove', 'Api\UserController@destroyUserDocument');

        # CONTACTS
        Route::get('users/{id}/contacts', 'Api\ContactController@index');
        Route::get('users/{id}/contactrequests', 'Api\ContactController@getContactRequests');
        Route::put('users/{id}/contacts/request', 'Api\ContactController@createRequest');
        Route::put('users/{id}/contacts/add', 'Api\ContactController@addContact');
        Route::put('users/{id}/request/accept', 'Api\ContactController@acceptRequest');
        Route::put('users/{id}/request/decline', 'Api\ContactController@declineRequest');
        Route::delete('users/{id}/request/remove', 'Api\ContactController@destroyContact');

        # REGISTRATION
        Route::get('events/{id}/bookedslots/{user_id}', 'Api\RegistrationController@getBookedSlots');
        Route::get('events/{id}/registrationfields/{id2}', 'Api\RegistrationController@getRegistrationFields');
        Route::post('events/{id}/register/{id2}', 'Api\RegistrationController@submitRegistration');
        Route::put('events/{id}/amendregistration/{id2}', 'Api\RegistrationController@amendRegistration');
        Route::post('events/{id}/cancel/{id2}', 'Api\RegistrationController@cancelRegistration');
        Route::post('events/{id}/renew/{id2}', 'Api\RegistrationController@renewRegistration');
    });

    #ROUTES THAT DON'T NEED AUTHENTICATION
    Route::group(['middleware' => 'cors'], function() {

        Route::pattern('id', '[0-9]+');
        Route::pattern('id2', '[0-9]+');

        # CII EVENT
        Route::get('ciievents', 'Api\EventController@ciiEvents');

        # MOBILE APP EVENTS
        Route::get('events', 'Api\EventController@events');
        Route::get('events/{id}', 'Api\EventController@eventData');
        Route::get('events/active', 'Api\EventController@activeEvents');
        Route::get('events/active/{id}', 'Api\EventController@activeEventsByType');
        Route::get('events/commingevents', 'Api\EventController@forthcomingEvents');
        Route::get('events/commingevents/{id}', 'Api\EventController@forthcomingEventsByType');
        Route::get('events/nearmeevents', 'Api\EventController@eventsNearMe');
        Route::get('events/{id}/registeredusers', 'Api\EventController@registeredUsers');
        Route::get('events/{id}/checkusers', 'Api\EventController@checkedinUsers');

        Route::get('events/{id}/speakers', 'Api\EventController@getSpeakers');
        Route::get('events/{id}/localcommittee', 'Api\EventController@getLocalcommittee');

        Route::get('events/{id}/floorplan', 'Api\EventController@floorPlan');

        Route::get('programmes/perevent/{id}', 'Api\ScheduleController@eventSessions');
        Route::get('programmes/{id}', 'Api\ScheduleController@getSession');
        Route::get('programmes/{id}/speakers', 'Api\ScheduleController@sessionsSpeakers');
        Route::get('speakers/{id}', 'Api\ScheduleController@speaker');

        Route::get('programmes/{id}/localcommittee', 'Api\ScheduleController@sessionComittees');
        Route::get('localcommittee/{id}', 'Api\ScheduleController@comitee');
        Route::get('documents/perevent/{id}', 'Api\DocumentController@eventDocuments');
        Route::get('documents/{id}', 'Api\DocumentController@show');
        Route::get('feedbacks/{id}', 'Api\EventController@feedbacks');
        Route::get('venues/{id}', 'Api\EventController@venue');
        Route::get('sponsors/perevent/{id}', 'Api\EventController@eventSponsors');
        Route::get('exhibitors/perevent/{id}', 'Api\EventController@eventExhibitors');
        Route::get('sponsors/{id}', 'Api\EventController@sponsor');
        Route::get('faqs/perevent/{id}', 'Api\EventController@eventFaqs');
        Route::get('faqs/{id}', 'Api\EventController@faq');
        Route::get('cpd/{id}', 'Api\ScheduleController@cpd');

        # MOBILE APP TEXT
        Route::get('generictext', 'Api\GenericTextController@index');
        Route::get('generictext/{type}', 'Api\GenericTextController@getText');

        //Route::get('authenticate/login', 'Api\AuthenticateController@login');
        //Route::get('authenticate/test', ['middleware' => ['jwt.auth'], 'uses' => 'Api\AuthenticateController@test']);

        Route::post('authenticate/login', 'Api\AuthenticateController@authenticate');
        Route::get('authenticate/checktoken/{token}', 'Api\AuthenticateController@checkToken');
        Route::get('authenticate/logout/{token}', 'Api\AuthenticateController@logout');
    });
});

//***************************** API VERSION 1 ROUTES *************************************/
# ROUTES THAT NEEDS AUTHENTICATION
Route::group(['prefix' => 'api/v1'], function() {
    Route::group(['middleware' => ['jwt.auth', 'cors']], function() {
        Route::pattern('id', '[0-9]+');
        Route::pattern('id2', '[0-9]+');

        # USER NOTES
        Route::get('users/{id}/notes', 'Api\v1\UserNoteController@index');
        Route::get('users/{id}/notes/{id2}', 'Api\v1\UserNoteController@show');
        Route::post('users/{id}/notes/add', 'Api\v1\UserNoteController@store');
        Route::put('users/{id}/notes/update/{id2}', 'Api\v1\UserNoteController@update');
        Route::delete('users/{id}/notes/remove', 'Api\v1\UserNoteController@destroy');

        Route::post('authenticate/changepassword', 'Api\v1\AuthenticateController@changePassword');

        # MOBILE APP USER
        Route::get('users/{id}', 'Api\v1\UserController@profile');
        Route::get('users/{id}/myevents', 'Api\v1\UserController@events');
        Route::post('users/{id}/feedback', 'Api\v1\UserController@submitFeedback');
        Route::get('users/{id}/feedback/{id2}', 'Api\v1\UserController@checkFeedback');

        # USER DOCUMENTS
        Route::get('users/{id}/mydocs', 'Api\v1\UserController@userDocuments');
        Route::get('mydocs/{id}', 'Api\v1\UserController@getDocument');
        Route::put('users/{id}/mydocs/add', 'Api\v1\UserController@storeUserDocument');
        Route::delete('users/{id}/mydocs/remove', 'Api\v1\UserController@destroyUserDocument');

        # CONTACTS
        Route::get('users/{id}/contacts', 'Api\v1\ContactController@index');
        Route::get('users/{id}/contactrequests', 'Api\v1\ContactController@getContactRequests');
        Route::put('users/{id}/contacts/request', 'Api\v1\ContactController@createRequest');
        Route::put('users/{id}/contacts/add', 'Api\v1\ContactController@addContact');
        Route::put('users/{id}/request/accept', 'Api\v1\ContactController@acceptRequest');
        Route::put('users/{id}/request/decline', 'Api\v1\ContactController@declineRequest');
        Route::delete('users/{id}/request/remove', 'Api\v1\ContactController@destroyContact');

        # REGISTRATION
        Route::get('events/{id}/bookedslots/{user_id}', 'Api\v1\RegistrationController@getBookedSlots');
        Route::get('events/{id}/registrationfields/{id2}', 'Api\v1\RegistrationController@getRegistrationFields');
        Route::post('events/{id}/register/{id2}', 'Api\v1\RegistrationController@submitRegistration');
        Route::put('events/{id}/amendregistration/{id2}', 'Api\v1\RegistrationController@amendRegistration');
        Route::post('events/{id}/cancel/{id2}', 'Api\v1\RegistrationController@cancelRegistration');
        Route::post('events/{id}/renew/{id2}', 'Api\v1\RegistrationController@renewRegistration');

        # SCHEDULE
        Route::put('events/{id}/schedule/favouritesessionclicked', 'Api\v1\ScheduleController@favouriteSessionClicked');
        Route::get('events/{id}/schedule/personal', 'Api\v1\ScheduleController@personalSchedule');
       
    });

    #ROUTES THAT DON'T NEED AUTHENTICATION
    Route::group(['middleware' => 'cors'], function() {

        Route::pattern('id', '[0-9]+');
        Route::pattern('id2', '[0-9]+');

        # CII EVENT
        Route::get('ciievents', 'Api\v1\EventController@ciiEvents');

        # MOBILE APP EVENTS
        Route::get('events', 'Api\v1\EventController@events');
        Route::get('events/{id}', 'Api\v1\EventController@eventData');
        Route::get('events/active', 'Api\v1\EventController@activeEvents');
        Route::get('events/active/{id}', 'Api\v1\EventController@activeEventsByType');
        Route::get('events/commingevents', 'Api\v1\EventController@forthcomingEvents');
        Route::get('events/commingevents/{id}', 'Api\v1\EventController@forthcomingEventsByType');
        Route::get('events/nearmeevents', 'Api\v1\EventController@eventsNearMe');
        Route::get('events/{id}/registeredusers', 'Api\v1\EventController@registeredUsers');
        Route::get('events/{id}/checkusers', 'Api\v1\EventController@checkedinUsers');

        Route::get('events/{id}/speakers', 'Api\v1\EventController@getSpeakers');
        Route::get('events/{id}/localcommittee', 'Api\v1\EventController@getLocalcommittee');

        Route::get('events/{id}/floorplan', 'Api\v1\EventController@floorPlan');

        Route::get('programmes/perevent/{id}', 'Api\v1\ScheduleController@eventSessions');
        Route::get('programmes/{id}', 'Api\v1\ScheduleController@getSession');
        Route::get('programmes/{id}/speakers', 'Api\v1\ScheduleController@sessionsSpeakers');
        Route::get('speakers/{id}', 'Api\v1\ScheduleController@speaker');
        Route::get('events/{id}/schedule', 'Api\v1\ScheduleController@multidaySchedule');

        Route::get('programmes/{id}/localcommittee', 'Api\v1\ScheduleController@sessionComittees');
        Route::get('localcommittee/{id}', 'Api\v1\ScheduleController@comitee');
        Route::get('documents/perevent/{id}', 'Api\v1\DocumentController@eventDocuments');
        Route::get('documents/{id}', 'Api\v1\DocumentController@show');
        Route::get('feedbacks/{id}', 'Api\v1\EventController@feedbacks');
        Route::get('feedbacks/{id}/event', 'Api\v1\EventController@feedbacksEvent');
        Route::get('feedbacks/{id}/session/{id2}', 'Api\v1\EventController@feedbacksSession');
        Route::get('venues/{id}', 'Api\v1\EventController@venue');
        Route::get('sponsors/perevent/{id}', 'Api\v1\EventController@eventSponsors');
        Route::get('exhibitors/perevent/{id}', 'Api\v1\EventController@eventExhibitors');
        Route::get('sponsors/{id}', 'Api\v1\EventController@sponsor');
        Route::get('faqs/perevent/{id}', 'Api\v1\EventController@eventFaqs');
        Route::get('faqs/{id}', 'Api\v1\EventController@faq');
        Route::get('cpd/{id}', 'Api\v1\ScheduleController@cpd');

        # HIGHLIGHTS
        Route::get('highlights', 'Api\v1\HighlightsController@index');

        # MOBILE APP TEXT
        Route::get('generictext', 'Api\v1\GenericTextController@index');
        Route::get('generictext/{type}', 'Api\v1\GenericTextController@getText');

        //Route::get('authenticate/login', 'Api\v1\AuthenticateController@login');
        //Route::get('authenticate/test', ['middleware' => ['jwt.auth'], 'uses' => 'Api\v1\AuthenticateController@test']);

        Route::post('authenticate/login', 'Api\v1\AuthenticateController@authenticate');
        Route::get('authenticate/checktoken/{token}', 'Api\v1\AuthenticateController@checkToken');
        Route::get('authenticate/logout/{token}', 'Api\v1\AuthenticateController@logout');

        Route::get('authenticate/onetimelogin', 'Api\v1\AuthenticateController@oneTimeLogin');
        Route::post('authenticate/shortcodelogin', 'Api\v1\AuthenticateController@shortcodeLogin');

    });
});