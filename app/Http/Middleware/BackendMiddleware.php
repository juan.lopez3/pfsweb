<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class BackendMiddleware {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if ($this->auth->guest())
		{
			if ($request->ajax())
			{
				abort(500);
			}
			else
			{
				return redirect()->guest('admin/login');
			}
		}
		
		// backend available for ever role except 4,5
		//if (in_array(\Auth::user()->role_id, [\Config::get('roles.member'), \Config::get('roles.non_member')])) {
			//abort(500);
		//}
		
		return $next($request);
	}

}
