<?php namespace App\Http\Middleware;

use Closure;
use App\User;
use Illuminate\Contracts\Auth\Guard;
use App\Events\DataWasManipulated;

class Authenticate {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{

		if ($this->auth->guest())
		{
			
			if ($request->ajax())
			{
				abort(500);
			}
			else
			{
				// if there is hash attached try to identify by hash
				if($request->has('auth') && ($request->has('email') || $request->has('pin'))) {
					
                    $user = User::where('pin', $request->get('pin'))->first();
                    
                    // if user not fount by pin try to find by email. new user may not have pin
                    if(!sizeof($user)) $user = User::where('email', $request->get('email'))->first();
					
					if(!empty($user)) {
					    $m_name = !empty($user->middle_name) ? " ".$user->middle_name : "";
						$user_hash = sha1($user->first_name.$m_name.$user->last_name.$user->email.\Config::get('app.key'));
						
						if($request->get('auth') == $user_hash) {
							
							//authenticate
							\Auth::loginUsingId($user->id);
                            
                            // log the action
                            event(new DataWasManipulated('login', '-  Single sign on'));
							
							return $next($request);
						}
					}
				}

				// show authentication options
                return view('auth.choose_login');

				//return redirect()->guest('auth/login');
			}
		}

		if (!in_array(\Auth::user()->role_id, [\Config::get('roles.member'), \Config::get('roles.non_member'), 1])) {
			$request->session()->flash('status-2', 'Should be a loged in  to use this functionality');
			return redirect()->route('events');
		}

		return $next($request);
	}

}
