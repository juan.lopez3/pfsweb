<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class PermissionsMiddleware {
	
	/**
	 * Create a new filter instance. Filter for top 3 roles
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}
	
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$permissions = $this->getPermissions($request);
		
		if(!in_array($this->auth->user()->role_id, $permissions)) {
			
			return abort(500);
		}
		
		return $next($request);
	}
	
	/**
     * Grab the permissions from the request
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Array
     */
    private function getPermissions($request)
    {
        $actions = $request->route()->getAction();
 
        return $actions['permissions'];
    }
	
}
