<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\RedirectResponse;

class RedirectIfAuthenticated {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		//if identified user tries to go to login again

		if ($this->auth->check())
		{
			if (in_array(\Auth::user()->role_id, [\Config::get('roles.member'), \Config::get('roles.non_member')])) {
					
				return new RedirectResponse(url('/'));
			} else {
				
				return new RedirectResponse(url('/admin/dashboard'));
			}
		}

		return $next($request);
	}

}
