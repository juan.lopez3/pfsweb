<?php namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Routing\Router;

class Kernel extends HttpKernel {

	public function __construct(Application $app, Router $router)
	{
		parent::__construct($app, $router);
	
		array_walk($this->bootstrappers, function(&$bootstrapper)
		{
			if($bootstrapper === 'Illuminate\Foundation\Bootstrap\ConfigureLogging')
			{
				$bootstrapper = 'Bootstrap\ConfigureLogging';
			}
		});
	}
	/**
	 * The application's global HTTP middleware stack.
	 *
	 * @var array
	 */
	protected $middleware = [
		'Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode',
		'Illuminate\Cookie\Middleware\EncryptCookies',
		'Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse',
		'Illuminate\Session\Middleware\StartSession',
		'Illuminate\View\Middleware\ShareErrorsFromSession',
		//'App\Http\Middleware\VerifyCsrfToken',
	];

	/**
	 * The application's route middleware.
	 *
	 * @var array
	 */
	protected $routeMiddleware = [
		'auth' 			=> 'App\Http\Middleware\Authenticate',
		'auth.basic' 	=> 'Illuminate\Auth\Middleware\AuthenticateWithBasicAuth',
		'guest' 		=> 'App\Http\Middleware\RedirectIfAuthenticated',
		'backend' 		=> 'App\Http\Middleware\BackendMiddleware',
		'permissions'	=> 'App\Http\Middleware\PermissionsMiddleware',
		'ssl' 			=> 'App\Http\Middleware\SSLMiddleware',
		'jwt.auth'      => 'Tymon\JWTAuth\Middleware\GetUserFromToken',
        'jwt.refresh'   => 'Tymon\JWTAuth\Middleware\RefreshToken',
        'csrf'          => 'Illuminate\Foundation\Http\Middleware\VerifyCsrfToken',
        'cors'          => 'App\Http\Middleware\Cors'
	];

}
