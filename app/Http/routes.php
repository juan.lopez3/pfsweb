<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

#Redirect to admin side

if(Request::is('admin*')) {
    require __DIR__.'/admin_routes.php';
}

if(Request::is('api/*')) {
    require __DIR__.'/api_routes.php';
}

//Route::get('admin', 'Admin\DashboardController@index');

Route::pattern('id', '[0-9]+');

#Events
Route::get('/', ['as' => 'events', 'uses' => 'EventController@index']);
Route::get('event/{slug}/view', ['as' => 'events.view', 'uses' => 'EventController@show']);
//Route::get('event/{slug}/agenda', ['as' => 'events.agenda', 'uses' => 'AgendaController@generateAgenda']);
Route::get('event/{slug}/agenda/{format?}', ['as' => 'events.agenda', 'uses' => 'AgendaController@generateAgenda']);
Route::get('event/{slug}/attendees', ['as' => 'events.attendeesList', 'uses' => 'PublicReportController@attendeesList']);
Route::get('event/{slug}/contributors', ['as' => 'events.contributors', 'uses' => 'PublicReportController@contributors']);
Route::get('event/{slug}/schedule', ['as' => 'events.externalSchedule', 'uses' => 'EventController@externalSchedule']);

#Profile
Route::get('profile/{pin}/update/{email}', ['as' => 'events.updateProfile', 'uses' => 'EventController@updateProfile']);

Route::group(['middleware' => 'auth', 'csrf'], function(){
	
	#Events
	Route::get('event/{slug}/book', ['as' => 'events.book', 'uses' => 'EventController@book']);
	Route::get('event/{slug}/updateprofile/{pin}', ['as' => 'events.updateProfile', 'uses' => 'EventController@updateProfile']);
	Route::get('event/{slug}/editbooking', ['as' => 'events.editBooking', 'uses' => 'EventController@editBooking']);
	Route::get('event/{slug}/renewbooking', ['as' => 'events.renewBooking', 'uses' => 'EventController@renewBooking']);
	Route::get('event/getsessiondetails/{id}', ['as' => 'events.getSessionDetails', 'uses' => 'EventController@getSessionDetails']);
	Route::post('event/{slug}/cancelbooking', ['as' => 'events.cancelBooking', 'uses' => 'EventController@cancelBooking']);
	Route::get('event/{slug}/getcertificate', ['as' => 'events.getCertificate', 'uses' => 'EventController@getCertificate']);
	Route::get('event/{slug}/editRegistration', ['as' => 'events.editRegistration', 'uses' => 'EventController@editRegistration']);
	Route::post('event/{slug}/amendRegistration', ['as' => 'events.amendRegistration', 'uses' => 'EventController@amendRegistration']);
	Route::post('profile/updateattendeetypes', ['as' => 'events.book.updateAttendeeTypes', 'uses' => 'EventController@updateAttendeeTypes']);
	Route::post('event/{slug}/submitbooking', ['as' => 'events.submitBooking', 'uses' => 'EventController@submitBooking']);

    Route::post('schedule/favouritesessionclicked', ['as' => 'events.favouriteSessionClicked', 'uses' => 'EventController@favouriteSessionClicked']);

    Route::get('event/{slug}/acceptinvitation', ['as' => 'events.acceptInvitation', 'uses' => 'EventController@acceptInvitation']);
    Route::get('event/{slug}/declineinvitation', ['as' => 'events.declineInvitation', 'uses' => 'EventController@declineInvitation']);
    
	# Feedback
	Route::get('event/{slug}/feedback', ['as' => 'events.feedback', 'uses' => 'FeedbackController@index']);
	Route::post('event/{slug}/feedback/store', ['as' => 'events.feedback.store', 'uses' => 'FeedbackController@store']);
	
	#Profile
	Route::get('profile/', ['as' => 'profile', 'uses' => 'UserController@show']);
	Route::get('profile/events', ['as' => 'profile.myEvents', 'uses' => 'UserController@events']);
	Route::get('profile/edit', ['as' => 'profile.edit', 'uses' => 'UserController@edit']);
	Route::patch('profile/edit', ['as' => 'profile.update', 'uses' => 'UserController@update']);
	Route::post('profile/uploadsponsorlogo', ['as' => 'profile.uploadLogo', 'uses' => 'UserController@uploadLogo']);
});

# EMAILS TRACKING
Route::get('emails/{id}/{email}/download', ['as' => 'emails.markOpened', 'uses' => 'Admin\SentEmailController@markOpened']);
Route::get('emails/{id}/{email}/redirect', ['as' => 'emails.logClick', 'uses' => 'Admin\SentEmailController@logClick']);
Route::get('auth/signup', ['as' => 'auth.signUp', 'uses' => 'Auth\AuthController@signUp']);
Route::post('auth/signup/check', ['as' => 'auth.signUp.check', 'uses' => 'Auth\AuthController@signUpCheck']);

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

# PAYMENT GATEWAY
// Route::post('payment/checkout/', ['as' => 'payment.checkout', 'uses' => 'PaymentController@store']);
Route::get('payment/checkout/', ['as' => 'payment.checkout', 'uses' => 'PaymentController@store']);
Route::get('event/{attendee_id}/payment', ['as' => 'events.payment', 'uses' => 'PaymentController@payment']);

