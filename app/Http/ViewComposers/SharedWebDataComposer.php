<?php
namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use App\Submenu;

class SharedWebDataComposer {

    public function compose(View $view) {
        
        $submenu = Submenu::orderBy('id', 'desc')->get();

        $view->with('submenu', $submenu);
    }
}