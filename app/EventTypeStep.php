<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EventTypeStep extends Model {

	protected $table = "event_type_steps";
	
	protected $fillable = ['title', 'description', 'order', 'event_type_id'];
	
	public function event_type() {
		
		return $this->belongsTo('App\EventType');
	}
	
	public function form() {
		
		return $this->hasOne('App\TypeStepForm');
	}
}
