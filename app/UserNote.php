<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UserNote extends Model {

	protected $guarded = ['id'];

	protected $fillable = ['user_id', 'event_id', 'content'];

	public function user() {
		$this->belongsTo('App\User');
	}

	public function event() {
		$this->belongsTo('App\Event');
	}
}
