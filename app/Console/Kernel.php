<?php namespace App\Console;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {
	public function __construct(Application $app, Dispatcher $events)
	{
		parent::__construct($app, $events);
	
		array_walk($this->bootstrappers, function(&$bootstrapper)
		{
			if($bootstrapper === 'Illuminate\Foundation\Bootstrap\ConfigureLogging')
			{
				$bootstrapper = 'Bootstrap\ConfigureLogging';
			}
		});
	}
	
	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'App\Console\Commands\SyncUsersCII',
		'App\Console\Commands\Inspire',
		'App\Console\Commands\EventThankyou',
		'App\Console\Commands\Reminder21',
		'App\Console\Commands\Reminder5',
		'App\Console\Commands\ReminderChairman',
		'App\Console\Commands\ReminderSpeaker',
		'App\Console\Commands\Waitlist',
		'App\Console\Commands\FullSessionsNotification',
		'App\Console\Commands\SyncNonMembersCII',
		'App\Console\Commands\OutsideReporting',
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
		//$schedule->command('inspire')->everyFiveMinutes();
		//$schedule->command('syncnonmemberscii')->daily()->at("02:00");
		$schedule->command('syncuserscii')->daily()->at("01:00");
		$schedule->command('eventthankyou')->daily()->at("20:00");
		$schedule->command('reminder21')->daily()->at("07:30");
		$schedule->command('reminder5')->daily()->at("07:00");
		$schedule->command('reminderchairman')->daily()->at("07:50");
		$schedule->command('reminderspeaker')->daily()->at("07:40");
		$schedule->command('waitlist')->cron('*/5 * * * *');
		$schedule->command('fullsessionsnotification')->daily()->at("10:10");
		$schedule->command('fullsessionsnotification')->daily()->at("15:45");
        
        $schedule->command('outsidereporting')->daily()->at("20:00");
	}

}
