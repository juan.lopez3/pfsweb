<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\User;
use App\AttendeeType;

class SyncNonMembersCII extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'syncnonmemberscii';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Sync non member users to CII DB.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$non_members = User::where('role_id', role('non_member'))->get();
        $service_url = "https://www.cii-hk.com/webservices/membership/Pfs.GetUserById.service?Id=%s";
        /* $service_url = "https://www.thepfs.org/webservices/membership/Pfs.GetUserById.service?Id=%s"; */
        $attendee_types = AttendeeType::lists('id', 'title');
        
        foreach($non_members as $user) {
            $id = empty($user->pin) ? $user->email : $user->pin;
            $u = json_decode(file_get_contents(sprintf($service_url, $id)));
            
            // loop while some data was returned
            if (!isset($u->Error)) {
                
                if(empty($u->Email)) continue;
                
                if (!empty($u->Pin))
                    $user = User::where('pin', $u->Pin)->first();
                
                if(empty($user)) {
                    $user = User::where('email', $u->Email)->first();
                }
                
                $user->title = $u->Title;
                //$user->first_name = $u->Forenames;
                $names = explode(" ", $u->Forenames);
                if (!empty($names[0])) $user->first_name = $names[0];
                
                if (!empty($names[1])) {
                    $middle_name = "";
                    for ($i=1; $i < sizeof($names); $i++) { 
                        if ($i > 1) $middle_name .= " ";
                        $middle_name .= $names[$i];
                    }
                    
                    $user->middle_name = $middle_name;
                }
                $user->last_name = $u->Surname;
                $user->pin = $u->Pin;
                $user->chartered = (empty($u->CharteredStatus)) ? 0 : 1;
                $user->phone = $u->Phone;
                $user->mobile = $u->Mobile;
                //$user->pfs_class = $u->PfsClass;
                
                // 0 address - home; 1 - office
                $user->postcode = (!empty($u->Addresses[0]->PostCode)) ? $u->Addresses[0]->PostCode : $u->Addresses[1]->PostCode;
                $user->company = $u->Addresses[1]->Line1OrCompany;
                $user->save();
                
                // attendee types
                /*
                $assigned_attendee_types = array();
                
                // check if assigned attendee types that do not come from CII and save it so the sync would not force to lose vital data
                if ($user->attendeeTypes()->where('id', 17)->count()) array_push($assigned_attendee_types, 17); // 17   Paraplanner
                if ($user->attendeeTypes()->where('id', 18)->count()) array_push($assigned_attendee_types, 18); // 18   Business owner
                if ($user->attendeeTypes()->where('id', 19)->count()) array_push($assigned_attendee_types, 19); // 19   Key decision maker in an advisory firm
                
                if(!empty($u->PfsClass) && isset($attendee_types[$u->PfsClass])) array_push($assigned_attendee_types, $attendee_types[$u->PfsClass]);
                
                foreach($u->Designations as $designation) {
                    
                    if(isset($attendee_types[$designation])) array_push($assigned_attendee_types, $attendee_types[$designation]);
                }
                
                $user->attendeeTypes()->sync($assigned_attendee_types);
                 * 
                 */
            }
        }

        //\Log::info("Synchronization with CII database completed! ".$sync_success." users out of ".$sync_total." were synchronized");
    }
}
