<?php namespace App\Console\Commands;

use App\Commands\SendEmail;
use App\EmailTemplate;
use App\Event;
use App\EventAttendee;
use App\EventAttendeeSlotSession;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Contracts\Bus\Dispatcher;

class Waitlist extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'waitlist';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Moves attendee from waiting list to booked for slot and sends notification email to attendee';

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle(Dispatcher $dispatcher)
    {

        $events = Event::where('event_date_from', '>', Carbon::now()->toDateString())
            ->with('scheduleTimeSlots.attendees', 'eventType.emailTemplates')->get();

        /**
         * Get id the templait by waitinglist
         */
        $templateId = \Config::get('emailtemplates.waitinglist');

        # notification about free space available
        foreach ($events as $event) {

            $message ="";
            $subject="";

            //get template in eventType by template Id, in this case is the waitlist

            $tableName = (new EmailTemplate())->getTable();
            $template = $event->eventType->emailTemplates()->where($tableName . '.id', $templateId)->first();

            /**
             * The program init without template.So, not send email, Now, if exist template with the id
             * the active = 1 and get the template and subjet by message email.
             * The second if: Now, if the template is active and exist another custom template replace
             * the message and subject. Else if don't exist another custom template and template of message
             * is disabled, this can't send mail,
             */
            $active = 0;
            if ($template) {
                $active = 1;
                $message = $template->template;
                $subject = $template->subject;
                //check for custom template
                $custom_template = $event->emailTemplates()->where($tableName . '.id', $templateId)->first();

                if ($custom_template && $custom_template->pivot->active) {
                    $message = $custom_template->pivot->template;
                    $subject = $custom_template->pivot->subject;
                } elseif ($custom_template && $custom_template->pivot->active == 0) {
                    // don't send
                    $active = 0;
                }

            }

            if ($event->mandatory_slots) {
                $this->bookAllEventSessions($event, $dispatcher, $active, $message, $subject);
            } else {
                $this->bookEventSessionsWithAvailability($event, $dispatcher, $active, $message, $subject);
            }

        }
        // \Log::info("CRONJOB Event waitinglist completed for event ");
    }

    protected function bookEventSessionsWithAvailability($event, $dispatcher, $active, $message, $subject)
    {

        foreach ($event->scheduleTimeSlots as $slot) {

            # if session has any people in waitinglist check if there is any space
            $free_space = $slot->slot_capacity - $slot->attendees()->where('registration_status_id', \Config::get('registrationstatus.booked'))->count();

            $booked_sessions['session_name'] = $slot->title;

            // if slot has any attendees in waitinglist and space available, move from waitinglist and send notification email
            if (!($slot->attendees()->where('registration_status_id', \Config::get('registrationstatus.waitlisted'))->count()
                && $free_space > 0)) {
                return;
            }

            // collect attendees from waitinglist for slot
            $attendees = $slot->attendees()
                ->where('registration_status_id', \Config::get('registrationstatus.waitlisted'))
                ->orderBy('created_at', 'ASC')
                ->take($free_space)->get();

            foreach ($attendees as $attendee) {
                //condition to avoid moving more people than allowed from waitlisted to booked.
                if ($free_space-- < 0) {
                    continue;
                }

                // change slot booking status
                $attendee->registration_status_id = \Config::get('registrationstatus.booked');

                $attendee->save();

                $user = EventAttendee::where('id', $attendee->event_attendee_id)->first();
                $user->registration_status_id = \Config::get('registrationstatus.booked');
                $user->save();

                $user = $user->user;

                //\Log::info("Event (".$event->title.") waitinglist email sent for user: ".$user->email)
                // if email template active send notificatino about move waitlist > booked
                if ($active) {
                    $dispatcher->dispatch
                        (new SendEmail
                        ($user->email, $user->cc_email, $subject,
                            $message, $user, $event, $booked_sessions)
                    );
                }
                // \Log::info("Event (".$event->title.") waitinglist email sent for user: ".$user->email);
            }

        }
    }

    protected function bookAllEventSessions($event, $dispatcher, $active, $message, $subject)
    {
        //load waitinglist users from event
        $total_booked = $event->atendees()->where('registration_status_id', \Config::get('registrationstatus.booked'))->count();

        $minimun_capacity = null;
        foreach ($event->scheduleTimeSlots as $slot) {
            if (!$minimun_capacity || $minimun_capacity > $slot->slot_capacity) {
                $minimun_capacity = $slot->slot_capacity;
            }

        }

        $free_space = $minimun_capacity - $total_booked;
        if ($free_space <= 0) {
            return;
        }

        $booked_sessions = ['session_name' => ""];

        // collect attendees from waitinglist for slot
        $attendees = EventAttendee::
            where('registration_status_id', \Config::get('registrationstatus.waitlisted'))
            ->where('event_id', $event->id)
            ->orderBy('created_at', 'ASC')
            ->take($free_space)->get();

        foreach ($attendees as $attendee) {

            $attendee->registration_status_id = \Config::get('registrationstatus.booked');
            $attendee->save();

            foreach ($event->scheduleTimeSlots as $slot) {

                $booked_sessions['session_name'] .= " " . $slot->title;

                $nslot = EventAttendeeSlotSession::firstOrCreate(
                    [
                        "event_attendee_id" => $attendee->id,
                        "event_schedule_slot_id" => $slot->id,
                    ]
                );

                $nslot->registration_status_id = \Config::get('registrationstatus.booked');
                $nslot->save();
            }
            // if email template active send notificatino about move waitlist > booked
            if ($active) {
              
                $user = $attendee->user;
                $dispatcher->dispatch
                    (new SendEmail
                    ($user->email, $user->cc_email, $subject,
                        $message, $user, $event, $booked_sessions)
                );
            }

        }
    }

/*}catch(\Exeception  $e){
var_dump($e->getMessage());
}*/
}
