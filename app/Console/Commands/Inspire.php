<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use Illuminate\Contracts\Bus\Dispatcher;
use App\User;
use App\Event;
use App\Commands\SendEmail;

class Inspire extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'inspire';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Display an inspiring quote';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle(Dispatcher $dispatcher)
	{
//        $user = User::first();
//        $event = Event::first();
//        $dispatcher->dispatch(new SendEmail('almantas@aelite.co.uk', null, 'subject', 'kas <a href="EVENT_LINK">event</a>', $user, $event));
        
	    /**
        $users = User::all();
        $i = 0;
        foreach ($users as $u) {
            
            if (empty($u->badge_name)) {
                $i++;
                $u->badge_name = $u->first_name.' '.$u->last_name;
                $u->save();
            } else {
                
                $middle_names = array_map('trim', explode(' ', $u->middle_name));
                
                foreach ($middle_names as $md) {
                    if (empty($md)) continue;
                    if (strpos($u->badge_name, $md) !== false) {
                        $i++;
                        $u->badge_name = $u->first_name.' '.$u->last_name;
                        $u->save();
                    }
                }
            }
        }
        
        dd($i);
        **/
		//$this->comment(PHP_EOL.Inspiring::quote().PHP_EOL);
		$service_url = "https://www.cii-hk.com/webservices/membership/Pfs.GetUserById.service?Id=001628355h";
		/* $service_url = "https://www.thepfs.org/webservices/membership/Pfs.GetUserById.service?Id=001628355h"; */
		//$service_url = "https://www.thepfs.org/webservices/membership/Pfs.GetMembers.service?page=1&itemsperpage=5";
        
		$u = json_decode(file_get_contents($service_url));
        
		dd($u);
	}

}
