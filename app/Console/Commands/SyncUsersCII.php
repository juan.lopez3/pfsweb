<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\User;
use App\AttendeeType;
use App\Commands\JobSyncUsersCII;
use Illuminate\Support\Facades\Queue;	

class SyncUsersCII extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'syncuserscii';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Syncronize users with CII database';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(User $user)
	{
		parent::__construct();
		$this->user = $user;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		// user CII service to load and update users in the database
		$page = 1;
		$items_per_page = 50;
		$service_url = "https://www.cii-hk.com/webservices/membership/Pfs.GetMembers.service?page=%d&itemsperpage=%d";
		/* $service_url = "https://www.thepfs.org/webservices/membership/Pfs.GetMembers.service?page=%d&itemsperpage=%d"; */
		
		$sync_total = 0;
		$sync_success = 0;
		$delay = 2;

		$users = json_decode(file_get_contents(sprintf($service_url, $page, $items_per_page)));
		$attendee_types = AttendeeType::lists('id', 'title');
		
		// loop while some data was returned
		while(!isset($users->Error)) {

			// call job: JobSyncUsersCII
			Queue::later($delay, new JobSyncUsersCII($users));
			$delay += 2;
			$page++;
			$users = json_decode(file_get_contents(sprintf($service_url, $page, $items_per_page)));
		}
		
		// \Log::info("Synchronization with CII database Queued!");
	}
}
