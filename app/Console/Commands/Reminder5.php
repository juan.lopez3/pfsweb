<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\EmailTemplate;
use Carbon\Carbon;
use Illuminate\Contracts\Bus\Dispatcher;
use App\Commands\SendEmail;
use App\Event;

class Reminder5 extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'reminder5';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Send event reminder to attendees';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle(Dispatcher $dispatcher)
	{
		$template = EmailTemplate::find(\Config::get('emailtemplates.reminder_5'));
		$before = !empty($template->reminder) ? $template->reminder : 5;
		
		# select all event
		$events = Event::where('event_date_from', Carbon::now()->addDays($before)->toDateString())->where('publish', 1)->with('atendees', 'eventType.emailTemplates')->get();
		
		# every event attendee receives reminder
		foreach ($events as $event) {
			
			$template = $event->eventType->emailTemplates->where('id', \Config::get('emailtemplates.reminder_5'))->first();
			$before = 5;
			$active = 0;
			
			if(sizeof($template) > 0) {
				
				$active = 1;
				$message = $template->template;
				$subject = $template->subject;
				$before = !empty($template->reminder) ? $template->reminder : 5;
				
				
				//check for custom template
				$custom_template = $event->emailTemplates->where('id', \Config::get('emailtemplates.reminder_5'))->first();
				
				if(sizeof($custom_template) > 0 && $custom_template->pivot->active) {
					
					$message = $custom_template->pivot->template;
					$subject = $custom_template->pivot->subject;
					if(!empty($custom_template->reminder)) $before = $custom_template->reminder;
				} elseif(sizeof($custom_template) > 0 && $custom_template->pivot->active == 0) {
					// don't send
					$active = 0;
				}
			}
			
			if(!$active) {
				continue;
			}
			
			foreach($event->atendees()->where('registration_status_id', config('registrationstatus.booked'))->with('subRoles')->get() as $attendee) {
				
				# send for members and not and if booking status booked
				if(!in_array($attendee->role_id, [role('member'), role('non_member')]) || $attendee->subRoles->count()) continue;
				
				$dispatcher->dispatch(new SendEmail($attendee->email, $attendee->cc_email, $subject, $message, $attendee, $event));
			}
			
			// \Log::info("CRONJOB Event reminder 5 email sent for event ".$event->title);
		}
	}

}
