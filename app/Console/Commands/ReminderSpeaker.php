<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\EmailTemplate;
use Carbon\Carbon;
use Illuminate\Contracts\Bus\Dispatcher;
use App\Commands\SendEmail;
use App\Event;

class ReminderSpeaker extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'reminderspeaker';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Sends reminder to event speakers';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle(Dispatcher $dispatcher)
	{
		$template = EmailTemplate::find(\Config::get('emailtemplates.speaker_confirmation'));
		$before = !empty($template->reminder) ? $template->reminder : 14;
		
		# select all event
		$events = Event::where('event_date_from', Carbon::now()->addDays($before)->toDateString())->where('publish', 1)->with('atendees', 'scheduleSessions', 'eventType.emailTemplates')->get();
		
		foreach($events as $event) {
			
			$template = $event->eventType->emailTemplates->where('id', \Config::get('emailtemplates.speaker_confirmation'))->first();
			$active = 0;
			
			if(sizeof($template) > 0) {
				
				$active = 1;
				$message = $template->template;
				$subject = $template->subject;
				
				//check for custom template
				$custom_template = $event->emailTemplates->where('id', \Config::get('emailtemplates.speaker_confirmation'))->first();
				
				if(sizeof($custom_template) > 0 && $custom_template->pivot->active) {
					
					$message = $custom_template->pivot->template;
					$subject = $custom_template->pivot->subject;
				} elseif(sizeof($custom_template) > 0 && $custom_template->pivot->active == 0) {
					// don't send
					$active = 0;
				}
			}
			
			if(!$active) {
				continue;
			}
			
			# for each event's sessions assigned speakers send mail
			foreach($event->scheduleSessions as $session) {
				
				foreach($session->contributors()->where('type', 'Speaker')->get() as $speaker) {
					
					$dispatcher->dispatch(new SendEmail($speaker->email, $speaker->cc_email, $subject, $message, $speaker, $event));
				}
			}
			
			// \Log::info("CRONJOB Event reminder for speakers sent for event ".$event->title);
		}
	}

}
