<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\EventAttendeeSlotSession;
use App\EmailTemplate;
use Carbon\Carbon;
use Illuminate\Contracts\Bus\Dispatcher;
use App\Commands\SendEmail;
use App\SponsorSetting;
use App\Sponsor;
use App\Event;

class EventThankyou extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'eventthankyou';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Sends thank you message to event attendees';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle(Dispatcher $dispatcher)
	{
		$events = Event::where('event_date_from', Carbon::now()->toDateString())->where('publish', 1)->with('atendees', 'eventType', 'sponsors', 'emailTemplates')->get();
		
		# every event attendee receives reminder
		foreach ($events as $event) {
			
			$template = $event->eventType->emailTemplates->where('id', \Config::get('emailtemplates.thankyou_event'))->first();
			
			$active = 0;
			
			if(sizeof($template) > 0) {
				
				$active = 1;
				$message = $template->template;
				$subject = $template->subject;
				
				//check for custom template
				$custom_template = $event->emailTemplates->where('id', \Config::get('emailtemplates.thankyou_event'))->first();
				
				if(sizeof($custom_template) > 0 && $custom_template->pivot->active) {
					
					$message = $custom_template->pivot->template;
					$subject = $custom_template->pivot->subject;
				} elseif(sizeof($custom_template) > 0 && $custom_template->pivot->active == 0) {
					// don't send
					$active = 0;
				}
			}
			
			if(!$active) {
				continue;
			}
			
			foreach($event->atendees()->whereNotNull('checkout_time')->where('registration_status_id', config('registrationstatus.booked'))->get() as $attendee) {
				
				if(!in_array($attendee->role_id, [role('member'), role('non_member')])) continue;
				
				$dispatcher->dispatch(new SendEmail($attendee->email, $attendee->cc_email, $subject, $message, $attendee, $event));
			}
		
			// \Log::info("CRONJOB Event thank you email sent for event ".$event->title);
		}
	}

}
