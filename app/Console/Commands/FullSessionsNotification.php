<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use App\Event;
use Carbon\Carbon;

class FullSessionsNotification extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'fullsessionsnotification';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Send notification about full sessions';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		# send notification about full sessions
		$events = Event::where('event_date_from', '>', Carbon::now()->toDateString())->where('publish', 1)
			->with('scheduleTimeSlots.attendees', 'eventType')->get();
			
		foreach($events as $event) {
			
			$to = sizeof($event->eventType->emailAddress) ? $event->eventType->emailAddress->email : \Config::get('mail.host_email');
			
			foreach($event->scheduleTimeSlots as $slot) {
				
				// if level 0 - no notifications, and session was not reported as full
				if($slot->notification_level > 0 && $slot->notified == 0 && !empty($slot->slot_capacity) && $slot->slot_capacity > 0) {
					
					// attendees percentage exceeds notification level
					if($slot->attendees()->whereIn('registration_status_id', [config('registrationstatus.booked'), config('registrationstatus.waitlisted')])->count() / $slot->slot_capacity * 100 >= $slot->notification_level) {
						
						# send notification
						\Mail::send('emails.fullsessionnotification', ['event' => $event, 'slot' => $slot], function($message) use($to) {
							
						    $message->to($to)->subject("Event full session notification!");
						});
                        
                        // \Log::info("CRONJOB Full slot ID: ".$slot->id.", Event ID: ".$event->id." notification sent to: ".$to);
					}
				}
			}
		}
	}

}
