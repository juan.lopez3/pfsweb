<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use Carbon\Carbon;
use Mail;
use App\Event;
use App\OutsideReport;

class OutsideReporting extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'outsidereporting';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Send outside reports.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
	    $reports = OutsideReport::all();
        
        foreach ($reports as $report) {
            
            $now = time(); // or your date as well
            $created_at = strtotime($report->created_at->toDateTimeString());
            $datediff = $now - $created_at;
            $days = floor($datediff/(60*60*24));
            
            // check if today is the day
            if ($days % $report->interval > 0) continue;
            
            $from = $report->filter_from;
            $to = $report->filter_to;
            
            $columns = array('Event', 'Quarter', 'Date');
            
            $events = Event::where('event_date_from', '>=', $from)
                ->where('event_date_from', '<=', $to)
                ->where('publish', 1);
           
           if(!empty($report->event_type_id)) $events = $events->where('event_type_id', $report->event_type_id);
           
            $events = $events->with('scheduleTimeSlots.attendees')
                ->orderBy('event_date_from')
                ->get();
            
            $slots_count = 0;
            
            foreach($events as $event) {
                if ($event->scheduleTimeSlots()->where('bookable', 1)->count() > $slots_count) $slots_count = $event->scheduleTimeSlots->count();
            }
            
            for ($i=0; $i < $slots_count; $i++) { 
                array_push($columns, '');
                array_push($columns, '');
            }
            
            array_push($columns, 'Total attendees');
            $data = array();
            $data[] = $columns;
            
            foreach($events as $event) {
                
                $event_data = array();
                $event_data = array(
                    $event->title,
                    $this->calculateQuarter($event->event_date_from),
                    date("d/m/Y", strtotime($event->event_date_from))
                );
                
                foreach($event->scheduleTimeSlots()->where('bookable', 1)->get() as $slot) {
                    $slot_attendees = $slot->attendees()->where('registration_status_id', config('registrationstatus.booked'))->count();
                    $val = $slot->title.' ('.$slot_attendees.' of '.$slot->slot_capacity.')';
                    array_push($event_data, $val);
                    array_push($event_data, (string)$slot_attendees);
                }
                
                for ($i=0; $i < $slots_count - $event->scheduleTimeSlots()->where('bookable', 1)->count(); $i++) { 
                    array_push($event_data, '');
                    array_push($event_data, '');
                }
                
                array_push($event_data, (string)$event->atendees()->where('registration_status_id', config('registrationstatus.booked'))->count());
                
                $data[] = $event_data;
            }
            
            \Excel::create('Booking Summary report', function($excel) use($data) {
            
                $excel->sheet('Bookings', function($sheet) use($data) {
            
                    $sheet->setOrientation('landscape');
                    $sheet->fromArray($data, null, 'A1', false, false);
                });
            })->save('xls');
            
            // send email with attached report
            Mail::raw('This is automatically generated events booking report from events.thepfs.org', function($message) use ($report) {
                $message->from('admin@events.thepfs.org');
                $message->to($report->email);
                $message->subject('Booking Summary report');
                $message->attach('storage/reports/Booking Summary report.xls');
            });
            
        }
        
        // \Log::info('Outside reporting completed');
	}

    /**
     * Calculate quarder from date
     */
    private function calculateQuarter($date)
    {
        $month = date("m", strtotime($date));
        $quater = number_format(ceil($month/3), 0);
        
        return $quater;
    }
}
