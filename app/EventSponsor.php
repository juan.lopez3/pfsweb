<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EventSponsor extends Model {

	protected $table = "event_sponsor";

	public $timestamps = false;
}
