<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduleChoiceUser extends Model
{
	protected $table = "schedule_choice_user";
}
