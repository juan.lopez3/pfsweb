<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UserFeedback extends Model {

	protected $table = "user_feedbacks";
	
	protected $guarded = ['id'];

	public function user() {

		return $this->belongsTo('App\User');
	}

	public function event() {

		return $this->belongsTo('App\Event');
	}

	public function feedback() {

		return $this->belongsTo('App\EventFeedback');
	}
}
