<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EventTechnology extends Model {

	protected $table = "event_technology";
	
	protected $fillable = ['event_id', 'technology_id', 'quantity', 'provided_by'];
	
}
