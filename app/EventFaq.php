<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EventFaq extends Model {

	protected $fillable = ['event_id', 'question', 'answer', 'position', 'visibility'];
	protected $table = "event_faqs";
	
	protected $guarded = ['id'];
}
