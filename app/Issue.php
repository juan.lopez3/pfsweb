<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Issue extends Model {

	protected $table = "issues";

    protected $fillable = ['title','description','status','type','confirmed','priority','quote_min','quote_max','quote_final','due','assigned_to',];

	public function changes() {

		return $this->hasMany('App\IssueChange');
	}

	public function assignedTo() {

		return $this->belongsTo('App\User', 'assigned_to');
	}

    public function attachments() {
        return $this->hasMany('App\IssueAttachment');
    }
}
