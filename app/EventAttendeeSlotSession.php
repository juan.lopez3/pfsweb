<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EventAttendeeSlotSession extends Model {

	protected $table = "event_attendee_slot_session";

	protected $guarded = ['id'];
	
	public function status() {
		
		return $this->belongsTo('App\RegistrationStatus', 'registration_status_id');
	}
	
	public function slot(){
		
		return $this->belongsTo('App\EventScheduleSlot', 'event_schedule_slot_id');
	}
}
