<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class VenueRoom extends Model {

	protected $table = "venue_rooms";

	protected $guarded = ['id'];
	
	public function venue() {
		
		return $this->belongsTo('App\Venue');
	}
}
