<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Sponsor extends Model {

	protected $table = "sponsors";
	
	protected $guarded = ['id'];
	
	public function events() {
		
		return $this->belongsToMany('App\Event');
	}
}
