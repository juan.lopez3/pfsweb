<?php namespace App\Events;

use App\Events\Event;
use Illuminate\Support\Facades\Request;
use Illuminate\Queue\SerializesModels;

class DataWasManipulated extends Event {

	use SerializesModels;
	
	public $user_id;
	public $action;
	public $additional_info;
	public $ip;
	public $user_agent;
	
	
	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct($action = "", $additional_info = "")
	{
		$this->user_id = empty(\Auth::user()->id) ? 1 : \Auth::user()->id;
		$this->action = $action;
		$this->additional_info = $additional_info;
		$this->ip = Request::getClientIp();
		$this->user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'No UserAgent';
	}

}
