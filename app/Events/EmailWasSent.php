<?php namespace App\Events;

use App\Events\Event;

use Illuminate\Queue\SerializesModels;

class EmailWasSent extends Event {

	use SerializesModels;

	public $to;
	public $subject;
	public $template;
	public $user_id;
	public $event_id;
	public $campaign;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct($to, $subject = "", $template = "", $user_id = null, $event_id = null, $campaign = null)
	{
		$this->to = $to;
        $this->subject = $subject;
        $this->template = $template;
		$this->user_id = $user_id;
        $this->event_id = $event_id;
		$this->campaign = $campaign;
	}

}
