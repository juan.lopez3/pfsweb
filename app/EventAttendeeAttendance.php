<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EventAttendeeAttendance extends Model {

	public $fillable = ['event_attendee_id', 'session_id'];

	public $timestamps = false;
}
