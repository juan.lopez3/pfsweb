<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class IssueChange extends Model {

	protected $table = "issue_changes";
	
	protected $guarded = ['id'];

	public function user() {

		return $this->belongsTo('App\User');
	}
}
