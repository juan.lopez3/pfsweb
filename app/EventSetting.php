<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventSetting extends Model
{
    
	public function currency() {
		
		return $this->belongsTo('\App\Currency');
	}
}
