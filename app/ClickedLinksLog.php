<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ClickedLinksLog extends Model {

	public $table = "clicked_links_log";

	public $guarded = ['id'];

}
