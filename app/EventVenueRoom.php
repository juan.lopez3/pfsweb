<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
*	M to M  model to make things work in meeting rooms spaces.
*
*/
class EventVenueRoom extends Model {

	protected $table = "event_venue_room";
	
	protected $guarded = ['id'];
	
}
