<?php namespace App\Commands;

use App\Commands\Command;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldBeQueued;

use App\Event;
use App\User;

class SendEmail extends Command implements ShouldBeQueued {

	public $to;
    public $cc;
	public $subject;
	public $message_text;
	public $user;
	public $event;
	public $booked_sessions;
	public $from;
    public $campaign;
	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($to, $cc = "", $subject, $message_text, $user = null, $event = null, $booked_sessions = null, $from = "", $campaign = null)
    {
        
        if (($event)) $event = Event::findOrFail($event->id);
        if (($user)) $user = User::findOrFail($user->id);

        $this->to = $to;
        $this->cc = $cc;
        $this->subject = $subject;
        $this->message_text = $message_text;
        $this->user = $user;
        $this->event = $event;
        $this->booked_sessions = $booked_sessions;
        $this->from = $from;
        $this->campaign = $campaign;
    }
}
