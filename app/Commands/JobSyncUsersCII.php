<?php namespace App\Commands;

use App\Commands\Command;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use App\User;
use App\AttendeeType;

class JobSyncUsersCII extends Command implements SelfHandling, ShouldBeQueued {

	use InteractsWithQueue, SerializesModels;

	public $users;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($users)
	{
		$this->users = $users;
	}
	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle()
	{
		$attendee_types = array_flip(AttendeeType::lists('title', 'id'));
		$sync_total=0;
		$sync_success = 0;
		$this->sync_total = 0;
		$this->sync_success = 0;
		// insert or update retrieved users

		foreach($this->users as $u) {
			$this->sync_total++;
			
			if(empty($u->Email)) continue;
			
			$this->sync_success++;
			$new_user = User::where('pin', $u->Pin)->first();

			
			if (!sizeof($new_user)) $new_user = User::where('email', $u->Email)->first();
			
			// if user exists, then update otherwise insert
			if(sizeof($new_user) == 0) {
				
				$new_user = new User();
				$new_user->badge_name = $u->Forenames.' '.$u->Surname;
			}
			
			$new_user->title = $u->Title;
			//$new_user->first_name = $u->Forenames;
			$names = explode(" ", $u->Forenames);
			if (!empty($names[0])) $new_user->first_name = $names[0];
			
			if (!empty($names[1])) {
				$middle_name = "";
				
				for ($i=1; $i < sizeof($names); $i++) { 
					if ($i > 1) $middle_name .= " ";
					$middle_name .= $names[$i];
				}
				
				$new_user->middle_name = $middle_name;
			}else{
				$new_user->middle_name = null;
			}
			$new_user->last_name = $u->Surname;
			$new_user->pin = $u->Pin;
			$new_user->chartered = (empty($u->CharteredStatus)) ? 0 : 1;
			$new_user->email = $u->Email;
			$new_user->phone = $u->Phone;
			$new_user->mobile = $u->Mobile;
			$new_user->role_id = role('member'); // serivice returns only members
			$new_user->pfs_class = $u->PfsClass;
			
			// 0 address - home; 1 - office
			$new_user->postcode = (!empty($u->Addresses[0]->PostCode)) ? $u->Addresses[0]->PostCode : $u->Addresses[1]->PostCode;
			$new_user->company = $u->Addresses[1]->Line1OrCompany;
			
			try {
				$new_user->save();
			} catch(\Exception $e) {
				// \Log::error('User '.$new_user->id.' not synced. '.$e->getMessage());
				continue;
			}
			
			// attendee types
			$assigned_attendee_types = array();
			
			// check if assigned attendee types that do not come from CII and save it so the sync would not force to lose vital data
			if ($new_user->attendeeTypes()->where('id', 17)->count()) array_push($assigned_attendee_types, 17); // 17	Paraplanner
			if ($new_user->attendeeTypes()->where('id', 18)->count()) array_push($assigned_attendee_types, 18); // 18	Business owner
			if ($new_user->attendeeTypes()->where('id', 19)->count()) array_push($assigned_attendee_types, 19); // 19	Key decision maker in an advisory firm
			
			if(!empty($u->PfsClass) && isset($attendee_types[$u->PfsClass])) array_push($assigned_attendee_types, $attendee_types[$u->PfsClass]);
			
			foreach($u->Designations as $designation) {
				
				if(isset($attendee_types[$designation])) array_push($assigned_attendee_types, $attendee_types[$designation]);
			}
			
			$new_user->attendeeTypes()->sync($assigned_attendee_types);
		}
		// \Log::info("JobSyncUsersCII Synchronization with CII database completed! ".$sync_success." users out of ".$sync_total." were synchronized");
	}

}
