<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Theme extends Model {

	protected $fillable = ['title','title_background_color','title_text_color','text_color','link_color','link_active_color','link_hover_color','link_visited_color','book_now_hover_color','twitter_image','background_image'];

}
