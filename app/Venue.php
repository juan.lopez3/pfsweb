<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Venue extends Model {

	protected $table = "venues";

	protected $guarded = ['id'];
	
	public function events() {
		
		return $this->hasMany('App\Event');
	}
	
	public function rooms() {
		
		return $this->hasMany('App\VenueRoom');
	}
	public function contactPerson() {

		return $this->belongsTo('App\User', 'venue_staff_id');
	}

	public function notes() {

		return $this->hasMany('App\VenueNote');
	}

	public function files() {
		return $this->hasMany('App\VenueFile');
	}
}
