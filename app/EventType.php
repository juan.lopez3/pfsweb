<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EventType extends Model {

	protected $table = "event_types";
	
	protected $fillable = ['exhibition', 'title', 'terms_conditions', 'amendment_text', 'cancel_text', 'has_sponsor', 'branding_image', 'email_branding', 'agenda_branding', 'app_branding', 'app_icon', 'app_description', 'app_promotional_banner', 'app_color', 'app_sponsors_active', 'app_additional_active', 'app_additional_title', 'app_additional_content', 'feedback_type', 'email_address_id'];
	
	public function events() {
		
		return $this->hasMany('App\Event');
	}
	
	public function typeSteps() {
		
		return $this->hasMany('App\EventTypeStep');
	}
	
	public function emailTemplates() {
		
		return $this->belongsToMany('App\EmailTemplate');
	}

	public function emailAddress() {

		return $this->belongsTo('App\EmailAddress');
	}

	public function files() {
		return $this->belongsToMany('App\File');
	}
}
