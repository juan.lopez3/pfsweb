<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EventScheduleSlot extends Model {

	protected $table = "event_schedule_slots";
	
	protected $guarded = ['id'];

	public function sessions() {

		return $this->hasMany('App\EventScheduleSlotSession');
	}

	public function event() {

		return $this->belongsTo('App\Event');
	}
	
	public function attendeeTypes() {
		
		return $this->belongsToMany('App\AttendeeType', 'event_schedule_slot_attendee_type');
	}
	
	public function meetingSpace() {
		
		return $this->belongsTo('App\EventVenueRoom');
	}
	
	public function attendees() {
		
		return $this->hasMany('App\EventAttendeeSlotSession');
	}

	public function questions() {

		return $this->hasMany('App\EventScheduleSlotQuestion');
	}
	
}
