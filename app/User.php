<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;
	
	protected $table = 'users';
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	
	protected $guarded = ['id'];
	
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [ 'password', 'remember_token' ,'confirmation_code', 'login_shortcode' ];

	public function role(){
		
		return $this->belongsTo('App\UserRole');
	}
	
	public function subRoles() {
		
		return $this->belongsToMany('App\UserRoleSub');
	}

	public function hasRoles($roles)
	{
		if ( $this->role()->whereIn('title', $roles)->count()) {
			return true;
		}	

		return false;
	}

	public function country() {
	    return $this->belongsTo('App\Country');
    }

	public function events() {
		
		return $this->belongsToMany('App\Event', 'event_atendees')->withPivot('registration_status_id', 'checkin_time', 'checkout_time', 'sponsors_contact', 'first_time_attendee', 'cancel_reason', 'paid', 'onsite_booking', 'manual_booking', 'fee_waived', 'notes', 'created_at');
	}

	public function members($query) {

		return $query->where('role_id', \Config::get('roles.member'));
	}

	public function nonMembers($query) {

		return $query->where('role_id', \Config::get('roles.non_member'));
	}
	
	public function actions() {
		
		return $this->hasMany('App\UserAction');
	}
	
	public function attendeeTypes() {
		
		return $this->belongsToMany('App\AttendeeType', 'user_attendee_type');
	}

	public function notes() {

		return $this->hasMany('App\UserNote');
	}
	
	public function bookedSessions() {
		
		return $this->hasMany('App\EventScheduleSlot');
	}

	public function dietaryRequirement() {

		return $this->belongsTo('App\DietaryRequirement');
	}
	
	public function documents() {

		return $this->hasMany('App\UserDocument');
	}

	public function feedbackAnswers() {
		
		return $this->hasMany('App\UserFeedback');
	}

	public function messages() {

		return $this->hasMany('App\UserMessage');
	}

	public function sentEmails() {

		return $this->hasMany('App\SentEmail');
	}
}
