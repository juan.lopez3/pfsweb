<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduleGroup extends Model
{
    protected $fillable = ['title', 'validated', 'position', 'active'];

    public function scheduleChoices() {

    	return $this->hasMany('\App\ScheduleChoice');
    }

    public function users() {

    	return $this->hasManyThrough('App\ScheduleChoiceUser', 'App\ScheduleChoice');
    }
}
