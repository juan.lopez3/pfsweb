<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model {

	use SoftDeletes;

    protected $table = "payments";

	protected $guarded = ['id'];
	
	public function attendees()
    {
        return $this->belongsTo('App\EventAttendee');
    }

}
