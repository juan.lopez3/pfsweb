<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MobileAppText extends Model {

	protected $table = "mobile_app_text";
	
	protected $guarded = ['id'];
}
