<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model {

	protected $table = "countries";

	protected $guarded = ['id'];
	
	public function venues(){
			
		return $this->hasMany('App\Venue');
	}
}
