<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactRequest extends Model {

	protected $guarded = ['id'];

	public function user() {

		return $this->belongsTo('App\User');
	}

	public function contact() {

		return $this->belongsTo('App\User', 'contact_id');
	}
}
