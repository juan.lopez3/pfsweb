<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailTemplateEvent extends Model {

	protected $table = "email_template_event";
	
	protected $projected = [];
}
