<?php namespace App\Services;

use App\User;
use Validator;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;

class Registrar implements RegistrarContract {

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function validator(array $data)
	{
		return Validator::make($data, [
			'first_name' => 'required|max:40',
			'last_name' => 'required|max:40',
			'postcode' => 'required|postcode',
			'email' => 'required|email|max:255|unique:users',
            'phone' => 'required',
            'job_title' => 'required',
			'company' => 'required',
			'password' => 'required|confirmed|min:5',
			'phone' => 'regex:/^[\d\s()+-]+$/',
			'mobile' => 'regex:/^[\d\s()+-]+$/',
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	public function create(array $data)
	{
//		$saved_file = "";
//		if(isset($data['profile_image'])) {
//
//			$saved_file = uploadFile($data['profile_image'], PROFILE);
//		}
		
		$user = User::create([
			'title' => $data['title'],
			'first_name' => $data['first_name'],
			'last_name' => $data['last_name'],
			'password' => bcrypt($data['password']),
			'email' => $data['email'],
			//'cc_email' => $data['cc_email'],
			'postcode' => $data['postcode'],
            'country_id' => $data['country_id'],
            'phone' => $data['phone'],
            'mobile' => $data['mobile'],
            'fca_number' => $data['fca_number'],
			'company' => $data['company'],
			'job_title' => $data['job_title'],
			
			//'badge_name' => $data['badge_name'],
			//'dietary_requirement_id' => $data['dietary_requirement_id'],
			//'special_requirements' => $data['special_requirements'],
			//'bio' => $data['bio'],
			//'profile_image' => $saved_file,
			
			'role_id' => config('roles.non_member'),
		]);
		
		//$attendee_types = arraY();
		
		//if (isset($data['attendee_types'])) $attendee_types = $data['attendee_types'];
		//$user->attendeeTypes()->sync($attendee_types);
		
		return $user;
	}

}
