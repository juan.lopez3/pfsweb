<?php namespace App\Services;

class Validator extends \Illuminate\Validation\Validator{

	public function validatePostcode($attribute, $value, $parameters){  
		// returning true as they don't want UK postcode validation any more
        return true;

		$pattern = "^(([gG][iI][rR] {0,}0[aA]{2})|((([a-pr-uwyzA-PR-UWYZ][a-hk-yA-HK-Y]?[0-9][0-9]?)|(([a-pr-uwyzA-PR-UWYZ][0-9][a-hjkstuwA-HJKSTUW])|([a-pr-uwyzA-PR-UWYZ][a-hk-yA-HK-Y][0-9][abehmnprv-yABEHMNPRV-Y]))) {0,}[0-9][abd-hjlnp-uw-zABD-HJLNP-UW-Z]{2}))$^";

		$valid = preg_match($pattern, $value);
		
		if($valid) return true;

		return false;
	}
}
