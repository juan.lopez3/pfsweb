<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendeeType extends Model {

	protected $table = "attendee_types";
	
	protected $fillable = ['title'];
	
}
