<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EventScheduleSlotSessionContributor extends Model {

	protected $table = "event_schedule_slot_session_user";
	
	protected $guarded = ['id'];
	
}
