<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class RegistrationStatus extends Model {

	protected $table = "registration_status";

	protected $guarded = ['id'];
	
	public function scopeCPD() {
		
		return $query->where('include_in_cpd', 1);
	}
}
