<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRoleSub extends Model {

	protected $table = 'user_roles_sub';
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = ['id'];
	
}
