<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeStepForm extends Model {

	protected $table = "type_step_forms";
	
	protected $guarded = ['id'];
	
}
