<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Sponsor;

class SponsorSetting extends Model {

	protected $table = "sponsor_settings";
	
	protected $guarded = ['id'];
	
	public function sponsor($sponsor_id) {
		
		return Sponsor::find($sponsor_id); 
	}
}
