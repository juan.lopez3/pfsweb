<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EventAttendee extends Model {

	protected $table = "event_atendees";
	
	protected $guarded = ['id'];
	
	public function registrationStatus() {
		
		return $this->belongsTo('App\RegistrationStatus');
	}
	
	public function slots() {
		
		return $this->hasMany('App\EventAttendeeSlotSession');
	}
	
	public function user() {
		
		return $this->belongsTo('App\User');
	}

	public function event() {

		return $this->belongsTo('App\Event');
	}

	public function registrationAnswers() {

		return $this->hasMany('App\RegistrationAnswer', 'event_attendee_id');
	}

	public function favouriteSessions() {
	    return $this->hasMany('App\EventAtendeeFavouriteSession', 'event_attendee_id');
    }

    public function attendedSessions() {
	    return $this->belongsToMany('App\EventScheduleSlotSession', 'event_attendee_attendances', 'event_attendee_id', 'session_id');
	}
	
	public function payment() {
		
		return $this->hasMany('App\Payment');
	}
	
}
