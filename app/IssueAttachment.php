<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IssueAttachment extends Model
{
    public $guarded = ['id'];
}
