<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DietaryRequirementNew extends Model {

	protected $table = "dietary_requirements_new";
	
	protected $guarded = ['id'];
}
