<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EventOnSiteDetail extends Model {

	protected $table = "event_onsite_details";
	
	protected $guarded = ['id'];
}
