<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduleChoice extends Model
{
	protected $fillable = ['title', 'capacity', 'coupon', 'schedule_group_id', 'active'];
    
	public function bookingDates() {
		
		return $this->belongsToMany('\App\BookingDate')->withPivot('price', 'taxable');
	}

	public function users() {

		return $this->belongsToMany('\App\User')->withPivot('created_at');
	}

	public function scheduleGroup() {

		return $this->belongsTo('\App\scheduleGroup');
	}
}
