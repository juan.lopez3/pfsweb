<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
	protected $fillable = ['title', 'address', 'city', 'postcode', 'country_id'];
    
	public function country() {
		
		return $this->belongsTo('\App\Country');
	}

	public function rooms() {

		return $this->hasMany('\App\HotelRoom');
	}
}
