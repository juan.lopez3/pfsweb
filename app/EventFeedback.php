<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EventFeedback extends Model {

	protected $table = "event_feedbacks";
	
	protected $guarded = ['id'];
	
	public function session() {
		
		return $this->belongsTo('App\EventScheduleSlotSession');
	}
}
