<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Highlights extends Model {

	public $table = "highlights";

	public $fillable = ['event_id','title'];

}