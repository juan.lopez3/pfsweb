#!/bin/bash

# Check if gedit is running
# -x flag only match processes whose name (or command line if -f is
# specified) exactly match the pattern. 
chmod -Rf 777 /mnt/BLOCKSTORAGE/home/162016.cloudwaysapps.com/fvvjmemwug/public_html/storage/logs
chmod -Rf 777 /mnt/BLOCKSTORAGE/home/162016.cloudwaysapps.com/fvvjmemwug/public_html/storage/framework
if pgrep -f "php /home/master/applications/fvvjmemwug/public_html/artisan queue:work redis" > /dev/null
then
    echo "Running"
    php /home/master/applications/fvvjmemwug/public_html/artisan queue:restart &
else
    php /home/master/applications/fvvjmemwug/public_html/artisan queue:work redis  --sleep=3 --tries=3 --daemon &
fi

if pgrep -f "php /home/master/applications/fvvjmemwug/public_html/artisan queue:listen" > /dev/null
then
    echo "Runninng"
else
    php /home/master/applications/fvvjmemwug/public_html/artisan queue:listen
fi

