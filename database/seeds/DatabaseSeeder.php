<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('CountriesSeeder');
		$this->call('EmailTemplatesSeeder');
		$this->call('EventTabsSeeder');
		$this->call('EventTypesSeeder');
		$this->call('RegionsSeeder');
		$this->call('RegistrationStatusSeeder');
		$this->call('SessionTypesSeeder');
		$this->call('DietaryRequirementsSeeder');
		$this->call('UserRolesSeeder');
		$this->call('UserRolesSubSeeder');
		$this->call('UsersSeeder');
		$this->call('AttendeeTypesSeeder');
	}

}
