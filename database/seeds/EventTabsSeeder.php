<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class EventTabsSeeder extends Seeder {

	public function run()
	{

		\App\EventTab::create([
			'title' => 'Event Settings',
			'slug' => 'settings',
		]);
		
		\App\EventTab::create([
			'title' => 'Schedule',
			'slug' => 'schedule',
		]);
		\App\EventTab::create([
			'title' => 'Contributors',
			'slug' => 'contributors',
		]);
		\App\EventTab::create([
			'title' => 'Venue',
			'slug' => 'venue',
		]);
		\App\EventTab::create([
			'title' => 'FAQs',
			'slug' => 'faqs',
		]);
		\App\EventTab::create([
			'title' => 'Event Materials',
			'slug' => 'event_materials',
		]);
		\App\EventTab::create([
			'title' => 'Feedback',
			'slug' => 'feedback',
		]);
		\App\EventTab::create([
			'title' => 'Sponsors',
			'slug' => 'sponsors',
		]);
        
        \App\EventTab::create([
            'title' => 'Gallery',
            'slug' => 'gallery',
        ]);
	}

}
