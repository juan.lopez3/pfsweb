<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class DietaryRequirementsSeeder extends Seeder {

	public function run()
	{
		\App\DietaryRequirement::create(['title' => 'No Dietary requirements']);
		\App\DietaryRequirement::create(['title' => 'Vegetarian']);
		\App\DietaryRequirement::create(['title' => 'Vegetarian - Eats Fish']);
		\App\DietaryRequirement::create(['title' => 'Vegetarian - No Fish']);
		\App\DietaryRequirement::create(['title' => 'Other']);
	}
}
