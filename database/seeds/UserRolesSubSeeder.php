<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class UserRolesSubSeeder extends Seeder {

	public function run()
	{
		\App\UserRoleSub::create([
			'title' => 'Speaker',
			'description' => '',
		]);

		\App\UserRoleSub::create([
			'title' => 'Chairperson',
			'description' => '',
		]);

		\App\UserRoleSub::create([
			'title' => 'Chartered Champion',
			'description' => '',
		]);

		\App\UserRoleSub::create([
			'title' => 'Sponsor attendee',
			'description' => '',
		]);

		\App\UserRoleSub::create([
			'title' => 'Exhibitor',
			'description' => '',
		]);
        
        \App\UserRoleSub::create([
            'title' => 'Committee',
            'description' => '',
        ]);
	}
}
