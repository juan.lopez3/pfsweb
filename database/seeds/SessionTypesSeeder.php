<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class SessionTypesSeeder extends Seeder {

	public function run()
	{

		\App\SessionType::create([
			'title' => 'Introduction',
			'color' => '#00CED1'
		]);

		\App\SessionType::create([
			'title' => 'Lecture',
			'color' => '#00BFFF'
		]);

		\App\SessionType::create([
			'title' => 'Lunch',
			'color' => '#FFFF66'
		]);

		\App\SessionType::create([
			'title' => 'Exhibition',
			'color' => '#FFccFF'
		]);
	}

}
