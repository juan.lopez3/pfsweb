<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class RegionsSeeder extends Seeder {

	public function run()
	{
		\App\Region::create(['title' => 'Birmingham']);
		\App\Region::create(['title' => 'Bristol and Cheltenham']);
		\App\Region::create(['title' => 'Central Scotland']);
		\App\Region::create(['title' => 'East Midlands']);
		\App\Region::create(['title' => 'Essex']);
		\App\Region::create(['title' => 'Exeter and North Devon']);
		\App\Region::create(['title' => 'Hants and Dorset']);
		\App\Region::create(['title' => 'Haydock']);
		\App\Region::create(['title' => 'Herts and Middlesex']);
		\App\Region::create(['title' => 'Isle Of Man']);
		\App\Region::create(['title' => 'Jersey']);
		\App\Region::create(['title' => 'Kent']);
		\App\Region::create(['title' => 'London']);
		\App\Region::create(['title' => 'Manchester']);
		\App\Region::create(['title' => 'Norfolk']);
		\App\Region::create(['title' => 'North Scotland']);
		\App\Region::create(['title' => 'Northern Ireland']);
		\App\Region::create(['title' => 'Plymouth and Cornwall']);
		\App\Region::create(['title' => 'South Wales']);
		\App\Region::create(['title' => 'Staffordshire & Shopshire']);
		\App\Region::create(['title' => 'Stamford']);
		\App\Region::create(['title' => 'Surrey']);
		\App\Region::create(['title' => 'Sussex']);
		\App\Region::create(['title' => 'Thames Valley']);
		\App\Region::create(['title' => 'Tyne Tees']);
		\App\Region::create(['title' => 'Yorkshire']);
	}
}
