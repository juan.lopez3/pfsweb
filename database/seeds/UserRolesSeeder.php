<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class UserRolesSeeder extends Seeder {

	public function run()
	{
		\App\UserRole::create([
			'title' => 'Super administrator',
			'description' => 'aElite',
		]);

		\App\UserRole::create([
			'title' => 'Event manager',
			'description' => 'Admin',
		]);

		\App\UserRole::create([
			'title' => 'Event coordinator',
			'description' => 'Event admin',
		]);

		\App\UserRole::create([
			'title' => 'Member',
			'description' => '',
		]);

		\App\UserRole::create([
			'title' => 'Non - Member',
			'description' => '',
		]);

		\App\UserRole::create([
			'title' => 'PFS Staff',
			'description' => '',
		]);

		\App\UserRole::create([
			'title' => 'Venue Staff',
			'description' => '',
		]);

		\App\UserRole::create([
			'title' => 'Host Person',
			'description' => 'Hostess',
		]);
        
        \App\UserRole::create([
            'title' => 'Uploaded Attendee',
            'description' => 'Imported attendee',
        ]);
        
        \App\UserRole::create([
            'title' => 'Regional Coordinator',
            'description' => '',
        ]);
	}
}
