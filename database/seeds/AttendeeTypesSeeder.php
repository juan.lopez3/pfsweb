<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class AttendeeTypesSeeder extends Seeder {

	public function run()
	{

		\App\AttendeeType::create(['title' => 'PFS Members']);
		\App\AttendeeType::create(['title' => 'Non PFS Members']);
		\App\AttendeeType::create(['title' => 'CertPFS']);
		\App\AttendeeType::create(['title' => 'DipPFS']);
		\App\AttendeeType::create(['title' => 'APFS Chartered Financial Planner']);
		\App\AttendeeType::create(['title' => 'APFS Non-Chartered Financial Planner']);
		\App\AttendeeType::create(['title' => 'FPFS Chartered Financial Planner']);
		\App\AttendeeType::create(['title' => 'FPFS Non-Chartered']);
		\App\AttendeeType::create(['title' => 'Financial Planner']);
		\App\AttendeeType::create(['title' => 'No PFS Designation']);
		\App\AttendeeType::create(['title' => 'Student']); //11
		\App\AttendeeType::create(['title' => 'Financial Planner']);
		\App\AttendeeType::create(['title' => 'ParaPlanner']);
		\App\AttendeeType::create(['title' => 'Other']);
	}

}
