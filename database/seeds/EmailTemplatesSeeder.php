<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class EmailTemplatesSeeder extends Seeder {

	public function run()
	{
		\App\EmailTemplate::create([
			'type' => 'Confirmation of new registration',
			'template' => '<p>Dear FIRST_NAME,</p>

<p><strong>Your reservation has been confirmed for EVENT_NAME.</strong></p>

<p>The packed programme will be delivered by subject specialists. Please&nbsp;<a href="EVENT_LINK" target="_blank">click here</a>&nbsp;for further information on the programme.</p>

<p>You have booked to attend the following sessions:&nbsp;<br />
<strong>BOOKED_SESSIONS</strong></p>

<p>For those staying for the full day, lunch will be provided.&nbsp;</p>

<p><strong>This event will qualify for up to SESSIONS_DURATION of structured CPD.</strong>&nbsp;For more agenda details, please visit&nbsp;<a href="http://www.thepfs.org/events" target="_blank">http://www.thepfs.org/events</a>.</p>

<p><em>Please note that the Morning and Afternoon sessions are different sessions and not a repeat. If you need to amend your registration as a result of this, please follow the instructions below.</em></p>

<p>If you would like to invite a non-PFS member to attend this event please send them&nbsp;this&nbsp;<a href="EVENT_LINK" target="_blank">link</a>.</p>

<p>We look forward to welcoming you at the event.</p>

<p>Kind Regards,</p>

<p><strong>Personal Finance Society Conference Office</strong><br />
<br />
0845 166 8415<br />
<a href="mailto:regionals@pfsevents.org" target="_blank">regionals@pfsevents.org</a></p>

<p><img src="http://demo.aelite.co.uk/Project_155/public/images/email_templates/image002.jpg" style="height:57px; width:269px" /></p>

<p><em>In association with our Partners in Professionalism</em></p>

<p>&nbsp;<em><img src="http://demo.aelite.co.uk/Project_155/public/images/email_templates/image003.jpg" style="height:24px; width:105px" /></em>&nbsp;&nbsp;&nbsp;&nbsp;<em><img src="http://demo.aelite.co.uk/Project_155/public/images/email_templates/image005.jpg" style="height:19px; width:128px" /></em>&nbsp;&nbsp;&nbsp;<em><img src="http://demo.aelite.co.uk/Project_155/public/images/email_templates/image007.jpg" style="height:37px; width:167px" /></em>&nbsp;&nbsp;&nbsp;&nbsp;<em><img src="http://demo.aelite.co.uk/Project_155/public/images/email_templates/image008.jpg" style="height:84px; width:125px" /></em>&nbsp;&nbsp;&nbsp;</p>

<hr />
<p><strong>Amending Your Reservation</strong><br />
Demand for places at Personal Finance Society regional events remains high. If for any reason you are no longer able to attend please let us know so that we can re-allocate your place.</p>

<p>If you have any dietary requirements or wish to change your booking please&nbsp;<a href="PROFILE_LINK" target="_blank">click here</a>.</p>

<p>If you have any further questions please take a look at our&nbsp;<a href="EVENT_LINK" target="_blank">commonly asked questions</a>. Alternatively please get in touch by emailing&nbsp;<a href="mailto:regionals@pfsevents.org" target="_blank">regionals@pfsevents.org</a>&nbsp;or calling us on 0845 166 8415.</p>

<p>As part of your registration you agreed with the terms and conditions, this includes agreeing to pay a&nbsp;<strong>&pound;50 charge</strong>&nbsp;if you fail to attend the Conference and provide us with&nbsp;<strong>2 working days notice</strong>. If you are no longer available to attend, please inform us of this as soon as possible. You can do this by amending your registration or replying to this email.</p>
'
		]);


		\App\EmailTemplate::create([
			'type' => 'Amendment of new registration',
			'template' => '<p>Dear FIRST_NAME,<br />
&nbsp;</p>

<p><strong>Your reservation has been confirmed for EVENT_NAME</strong></p>

<p>Thank you for making amendments for the forthcoming Personal Finance Society Regional Conference at the EVENT_DESCRIPTION <br />
&nbsp;</p>

<p>The packed programme will be delivered by subject specialists. Please&nbsp;<a href="EVENT_LINK" target="_blank">click here</a>&nbsp;for further information on the programme.</p>

<p>You have booked to attend the following sessions:&nbsp;<br />
<strong>BOOKED_SESSIONS</strong><br />
&nbsp;</p>

<p>For those staying for the full day, lunch will be provided.&nbsp;<br />
&nbsp;</p>

<p><strong>This event will qualify for up to SESSIONS_DURATION of structured CPD.</strong>&nbsp;For more agenda details, please visit&nbsp;<a href="http://www.thepfs.org/events" target="_blank">http://www.thepfs.org/events</a>.</p>

<p><br />
We look forward to welcoming you at the event.</p>

<p>Kind Regards,</p>

<p><strong>Personal Finance Society Conference Office</strong><br />
<br />
0845 166 8415<br />
<a href="mailto:regionals@pfsevents.org" target="_blank">regionals@pfsevents.org</a></p>

<p><img src="http://demo.aelite.co.uk/Project_155/public/images/email_templates/image001.jpg" style="height:57px; width:269px" /></p>

<p><em>In association with our Partners in Professionalism</em></p>

<p>&nbsp;<em><img src="http://demo.aelite.co.uk/Project_155/public/images/email_templates/image003.jpg" style="height:24px; width:105px" /></em>&nbsp;&nbsp;&nbsp;&nbsp;<em><img src="http://demo.aelite.co.uk/Project_155/public/images/email_templates/image005.jpg" style="height:19px; width:128px" /></em>&nbsp;&nbsp;&nbsp;<em><img src="http://demo.aelite.co.uk/Project_155/public/images/email_templates/image006.jpg" style="height:37px; width:167px" /></em>&nbsp;&nbsp;&nbsp;&nbsp;<em><img src="http://demo.aelite.co.uk/Project_155/public/images/email_templates/image008.jpg" style="height:84px; width:125px" /></em>&nbsp;&nbsp;&nbsp;<br />
&nbsp;</p>

<p><strong>Amending Your Reservation</strong><br />
Demand for places at Personal Finance Society regional events remains high. If for any reason you are no longer able to attend please let us know so that we can re-allocate your place.</p>

<p>If you have any dietary requirements or wish to change your booking please&nbsp;<a href="PROFILE_LINK" target="_blank">click here</a>.</p>

<p>If you have any further questions please take a look at our&nbsp;<a href="EVENT_LINK" target="_blank">commonly asked questions</a>. Alternatively please get in touch by emailing&nbsp;<a href="mailto:regionals@pfsevents.org" target="_blank">regionals@pfsevents.org</a>&nbsp;or calling us on 0845 166 8415.</p>

<p>As part of your registration you agreed with the terms and conditions, this includes agreeing to pay a&nbsp;<strong>&pound;50 charge</strong>&nbsp;if you fail to attend the Conference and provide us with&nbsp;<strong>2 working days notice</strong>. If you are no longer available to attend, please inform us of this as soon as possible. You can do this by amending your registration or replying to this email.</p>
'
		]);

		\App\EmailTemplate::create([
			'type' => 'Event reminder 14 days prior',
			'reminder' => 14,
			'template' => '<p>Dear FIRST_NAME,<br />
&nbsp;</p>

<p><strong>Your reservation has been confirmed for EVENT_NAME.</strong></p>

<p>This is just a reminder that this event will take place EVENT_START, at the EVENT_DESCRIPTION.<br />
&nbsp;</p>

<p>The packed programme will be delivered by subject specialists. Please&nbsp;<a href="EVENT_LINK"" target="_blank">click here</a>&nbsp;for further information on the programme.</p>

<p>You have booked to attend the following sessions:&nbsp;<br />
<strong>BOOKED_SESSIONS</strong><br />
&nbsp;</p>

<p>For those staying for the full day, lunch will be provided.</p>
<br />

<p><strong>This event will qualify for up to SESSIONS_DURATION of structured CPD.</strong>&nbsp;For more agenda details, please visit&nbsp;<a href="http://www.thepfs.org/events" target="_blank">http://www.thepfs.org/events</a>&nbsp;For directions to the venue please&nbsp;<a href="EVENT_LINK" target="_blank">click here</a>.</p>

<p>As part of your registration you agreed with the terms and conditions, this includes agreeing to pay a&nbsp;<strong>&pound;50 charge</strong>&nbsp;if you fail to attend the Conference and provide us with&nbsp;<strong>2 working days notice</strong>. If you are no longer available to attend, please inform us of this as soon as possible. You can do this by amending your registration or replying to this email.</p>

<p><br />
We look forward to welcoming you at the event.</p>

<p>Kind Regards,</p>

<p><strong>Personal Finance Society Conference Office</strong><br />
<br />
0845 166 8415<br />
<a href="mailto:regionals@pfsevents.org" target="_blank">regionals@pfsevents.org</a></p>

<p><img src="http://demo.aelite.co.uk/Project_155/public/images/email_templates/image001.jpg" style="height:57px; width:269px" /></p>

<p><em>In association with our Partners in Professionalism</em></p>

<p>&nbsp;<em><img src="http://demo.aelite.co.uk/Project_155/public/images/email_templates/image003.jpg" style="height:24px; width:105px" /></em>&nbsp;&nbsp;&nbsp;&nbsp;<em><img src="http://demo.aelite.co.uk/Project_155/public/images/email_templates/image005.jpg" style="height:19px; width:128px" /></em>&nbsp;&nbsp;&nbsp;<em><img src="http://demo.aelite.co.uk/Project_155/public/images/email_templates/image006.jpg" style="height:37px; width:167px" /></em>&nbsp;&nbsp;&nbsp;&nbsp;<em><img src="http://demo.aelite.co.uk/Project_155/public/images/email_templates/image008.jpg" style="height:84px; width:125px" /></em>&nbsp;&nbsp;&nbsp;<br />
&nbsp;</p>

<p><strong>Amending Your Reservation</strong><br />
Demand for places at Personal Finance Society regional events remains high. If for any reason you are no longer able to attend please let us know so that we can re-allocate your place.</p>

<p>If you have any dietary requirements or wish to change your booking please&nbsp;<a href="PROFILE_LINK" target="_blank">click here</a>.</p>

<p>If you have any further questions please take a look at our&nbsp;<a href="EVENT_LINK" target="_blank">commonly asked questions</a>. Alternatively please get in touch by emailing&nbsp;<a href="mailto:regionals@pfsevents.org" target="_blank">regionals@pfsevents.org</a>&nbsp;or calling us on 0845 166 8415.</p>

<p>As part of your registration you agreed with the terms and conditions, this includes agreeing to pay a&nbsp;<strong>&pound;50 charge</strong>&nbsp;if you fail to attend the Conference and provide us with&nbsp;<strong>2 working days notice</strong>. If you are no longer available to attend, please inform us of this as soon as possible. You can do this by amending your registration or replying to this email.</p>
'
		]);
		
		\App\EmailTemplate::create([
			'type' => 'Event reminder 5 days prior',
			'reminder' => 5,
			'template' => '<p>Dear FIRST_NAME,<br />
&nbsp;</p>

<p><strong>Your reservation has been confirmed for EVENT_NAME.</strong></p>

<p>This is just a reminder that this event will take place EVENT_START, at the EVENT_DESCRIPTION.<br />
&nbsp;</p>

<p>The packed programme will be delivered by subject specialists. Please&nbsp;<a href="EVENT_LINK" target="_blank">click here</a>&nbsp;for further information on the programme.</p>

<p>You have booked to attend the following sessions:&nbsp;<br />
<strong>BOOKED_SESSIONS</strong><br />
&nbsp;</p>

<p>For those staying for the full day, lunch will be provided.</p>

<p><strong>This event will qualify for up to SESSIONS_DURATION of structured CPD.</strong>&nbsp;For more agenda details, please visit&nbsp;<a href="http://www.thepfs.org/events" target="_blank">http://www.thepfs.org/events</a>&nbsp;For directions to the venue please&nbsp;<a href="EVENT_LINK" target="_blank">click here</a>.</p>

<p>As part of your registration you agreed with the terms and conditions, this includes agreeing to pay a&nbsp;<strong>&pound;50 charge</strong>&nbsp;if you fail to attend the Conference and provide us with&nbsp;<strong>2 working days notice</strong>. If you are no longer available to attend, please inform us of this as soon as possible. You can do this by amending your registration or replying to this email.</p>

<p><br />
We look forward to welcoming you at the event.</p>

<p>Kind Regards,</p>

<p><strong>Personal Finance Society Conference Office</strong><br />
<br />
0845 166 8415<br />
<a href="mailto:regionals@pfsevents.org" target="_blank">regionals@pfsevents.org</a></p>

<p><img src="http://demo.aelite.co.uk/Project_155/public/images/email_templates/image001.jpg" style="height:57px; width:269px" /></p>

<p><em>In association with our Partners in Professionalism</em></p>

<p>&nbsp;<em><img src="http://demo.aelite.co.uk/Project_155/public/images/email_templates/image003.jpg" style="height:24px; width:105px" /></em>&nbsp;&nbsp;&nbsp;&nbsp;<em><img src="http://demo.aelite.co.uk/Project_155/public/images/email_templates/image005.jpg" style="height:19px; width:128px" /></em>&nbsp;&nbsp;&nbsp;<em><img src="http://demo.aelite.co.uk/Project_155/public/images/email_templates/image006.jpg" style="height:37px; width:167px" /></em>&nbsp;&nbsp;&nbsp;&nbsp;<em><img src="http://demo.aelite.co.uk/Project_155/public/images/email_templates/image008.jpg" style="height:84px; width:125px" /></em>&nbsp;&nbsp;&nbsp;<br />
&nbsp;</p>

<p><strong>Amending Your Reservation</strong><br />
Demand for places at Personal Finance Society regional events remains high. If for any reason you are no longer able to attend please let us know so that we can re-allocate your place.</p>

<p>If you have any dietary requirements or wish to change your booking please&nbsp;<a href="PROFILE_LINK" target="_blank">click here</a>.</p>

<p>If you have any further questions please take a look at our&nbsp;<a href="EVENT_LINK" target="_blank">commonly asked questions</a>. Alternatively please get in touch by emailing&nbsp;<a href="mailto:regionals@pfsevents.org" target="_blank">regionals@pfsevents.org</a>&nbsp;or calling us on 0845 166 8415.</p>

<p>As part of your registration you agreed with the terms and conditions, this includes agreeing to pay a&nbsp;<strong>&pound;50 charge</strong>&nbsp;if you fail to attend the Conference and provide us with&nbsp;<strong>2 working days notice</strong>. If you are no longer available to attend, please inform us of this as soon as possible. You can do this by amending your registration or replying to this email.</p>
'
		]);
		
		
		\App\EmailTemplate::create([
			'type' => 'Thank you for attending event',
			'template' => '<p>Dear FIRST_NAME,</p>

<p>Thank you very much for attending EVENT_NAME on EVENT_START<br />
<br />
We hope that you found this event worthwhile, so if you did not complete your feedback form onsite, or if you have any additional comments that you would like to make, please email us with your thoughts at&nbsp;<a href="mailto:regionals@pfsevents.org" target="_self">regionals@pfsevents.org</a>.</p>

<p><strong>To print CPD Attendance Certificate</strong> you need to log in to system and find event in your previous events list. Enter inside the event and you will be able to print certificate or click <a target="_blank" href="EVENT_EDIT">here</a></p>

<p>Your booked sessions:</p>
<p>BOOKED_SESSIONS</p>
<p>Please find below contact details for our Partner in Professionalism:</p>

<p>EVENT_SPONSORS</p>

<p>We look forward to welcoming you to another Personal Finance Society Conference soon.</p>

<p>&nbsp;</p>

<p>Kind Regards,</p>

<p>&nbsp;</p>

<p><strong>Personal Finance Society Conference Office</strong></p>

<p>T: 0845 166 8415</p>

<p>E:&nbsp;<a href="mailto:regionals@pfsevents.org" target="_self">regionals@pfsevents.org</a></p>

<p>W:&nbsp;<a href="http://www.thepfs.org/" target="_self">www.thepfs.org</a></p>
'
		]);
		
		
		\App\EmailTemplate::create([
			'type' => 'Cancel registration',
			'template' => '<p>Dear FIRST_NAME,</p>

<p>This email confirms cancellation of your place at EVENT_NAME on EVENT_START.</p>
<br />
We are sorry that you cannot make this event. We are pleased to confirm that as you have provided us with sufficient notice,&nbsp;<strong>you will not be charged the &pound;50 non-attendance fee</strong>.&nbsp;<br />
&nbsp;</p>

<p>Please check the Personal Finance Society website at&nbsp;<a href="http://www.thepfs.org/events" target="_self">www.thepfs.org/events</a>, as there might be a conference taking place somewhere else close by that you could attend.</p>

<p>Kind Regards,</p>

<p><strong>Personal Finance Society Conference office</strong><br />
0845 166 8415<br />
<a href="mailto:regionals@pfsevents.org" target="_blank">regionals@pfsevents.org</a><br />
<a href="http://www.thepfs.org/" target="_blank">www.thepfs.org</a></p>
'
		]);

		\App\EmailTemplate::create([
			'type' => 'Chairman conference confirmation',
			'reminder' => 12,
			'template' => '<p>Dear FIRST_NAME,</p>

<p>I&rsquo;m&nbsp;getting in touch about the <strong>EVENT_NAME</strong><strong> on EVENT_START </strong><strong>and thank you very much indeed for agreeing to chair on this occasion</strong><strong>. </strong>Please take a moment to note the following important information.</p>

<p>&nbsp;</p>

<p>I have been asked by the Personal Finance Society to remind you to take the pop-up banner that you have been provided with. If you need a banner please contact Martin Wells via email at <a href="mailto:Martin.Wells@thepfs.org">Martin.Wells@thepfs.org</a> or phone at 020 7397 1115.</p>

<p>&nbsp;</p>

<table border="0" cellpadding="0" cellspacing="0" style="width:568px">
	<tbody>
		<tr>
			<td style="width:287px">
			<p><strong>Number delegates to date</strong></p>
			</td>
			<td style="width:282px">
			<p>145</p>
			</td>
		</tr>
		<tr>
			<td style="width:287px">
			<p><strong>Conference room</strong></p>
			</td>
			<td style="width:282px">
			<p>Dorchester Suite</p>
			</td>
		</tr>
		<tr>
			<td style="width:287px">
			<p><strong>Chairman and Committee arrival</strong></p>
			</td>
			<td style="width:282px">
			<p><strong>30 minutes prior to start of registration</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width:287px">
			<p><strong>Onsite registration starts</strong></p>
			</td>
			<td style="width:282px">
			<p>08:30 - 09:00</p>
			</td>
		</tr>
		<tr>
			<td style="width:287px">
			<p><strong>Conference starts</strong></p>
			</td>
			<td style="width:282px">
			<p>09:00 - 09:15</p>
			</td>
		</tr>
		<tr>
			<td style="width:287px">
			<p><strong>Mid-morning break</strong></p>
			</td>
			<td style="width:282px">
			<p>11:15 - 11:35</p>
			</td>
		</tr>
		<tr>
			<td style="width:287px">
			<p><strong>Video message from PFS CEO</strong></p>
			</td>
			<td style="width:282px">
			<p>11:35 &ndash; 11:40</p>
			</td>
		</tr>
		<tr>
			<td style="width:287px">
			<p><strong>Lunch</strong></p>
			</td>
			<td style="width:282px">
			<p>13:00 - 13:40</p>
			</td>
		</tr>
		<tr>
			<td style="width:287px">
			<p><strong>Afternoon session starts</strong></p>
			</td>
			<td style="width:282px">
			<p>13:40 - 14:55</p>
			</td>
		</tr>
		<tr>
			<td style="width:287px">
			<p><strong>Mid-afternoon break</strong></p>
			</td>
			<td style="width:282px">
			<p>14:55 -15:15</p>
			</td>
		</tr>
		<tr>
			<td style="width:287px">
			<p><strong>Conference ends</strong></p>
			</td>
			<td style="width:282px">
			<p>15:55 - 16:00</p>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<p>Please make sure you carry out the following tasks during the course of the Conference:</p>

<p>&nbsp;</p>

<table border="1" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="width:568px">
			<p><strong>CHAIRMAN&rsquo;S BRIEFING NOTES:</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width:568px">
			<p><strong>Delegate sign-in</strong></p>

			<p>Remind attendees to <strong>make sure they have signed in</strong>. Only those that do will receive the post event email enclosing CPD certificates and speakers&rsquo; presentations. This is also important because <strong>there is a non-attendance fee of &pound;50</strong>.</p>
			</td>
		</tr>
		<tr>
			<td style="width:568px">
			<p><strong>Sponsors</strong></p>

			<p>Please make sure that you say THANK YOU to the sponsors at the start of the conference &ndash; and again at the end of the conference.&nbsp; Without the sponsors the events would not happen.&nbsp; They are Blackrock, MetLife, Old Mutual Wealth and Royal London.</p>
			</td>
		</tr>
		<tr>
			<td style="width:568px">
			<p><strong>Announcement</strong></p>

			<p>It&rsquo;s important to make a fire evacuation procedural announcement at the start of the day, walk through the agenda and appropriately introduce all speakers.</p>
			</td>
		</tr>
		<tr>
			<td style="width:568px">
			<p><strong>Time keeping</strong></p>

			<p>Please make sure that speakers respect their presentation timings.</p>
			</td>
		</tr>
		<tr>
			<td style="width:568px">
			<p><strong>At mid-morning break</strong></p>

			<p>Move the previous speaker&rsquo;s final slide on to the &lsquo;Forthcoming PFS events&rsquo; holding slide</p>
			</td>
		</tr>
		<tr>
			<td style="width:568px">
			<p><strong>On site hostess </strong></p>

			<p>Our hostess <strong>(Helena Harper)</strong> will provide support throughout the day. She will arrive 1 hour before registration opens. Please note that you and the committee are required to assist with the set up for the Conference. Please find attached committee guideline document.</p>
			</td>
		</tr>
		<tr>
			<td style="width:568px">
			<p><strong>Additional Committee meetings</strong></p>

			<p>If you are holding a committee meeting on the same day as a regional conference please obtain prior approval from Head of Operations before booking or incurring additional expense.</p>
			</td>
		</tr>
		<tr>
			<td style="width:568px">
			<p><strong>Presentation</strong></p>

			<p>I am attaching a draft of the master presentation. This is only for your information so you can see how it looks.&nbsp; You don&rsquo;t need to fill in any information, as it will all be done by TFI Meeting Point for the actual day.</p>

			<p>On the day of the conference, there will be two USB memory sticks with the same content (one for back up) &ndash; the MASTER presentation of the day will be clearly identified once the USB folder is open.</p>

			<p>If you have any questions please let us know on <a href="mailto:admin@pfsevents.org">admin@pfsevents.org</a>.</p>
			</td>
		</tr>
		<tr>
			<td style="width:568px">
			<p><strong>Video message</strong></p>

			<p>There will be a video of Keith Richards embedded in the final master presentation. In the attached presentation, we have included a link to the video so that you can watch it beforehand if you wish. Please note the coffee break has been shortened 5 minutes for this purpose.</p>
			</td>
		</tr>
		<tr>
			<td style="width:568px">
			<p><strong>Hand out</strong></p>

			<p>Please note that there will be a Case study available as per attached.</p>
			</td>
		</tr>
		<tr>
			<td style="width:568px">
			<p><strong>Badges</strong></p>

			<p>There will be badges provided at the event for the delegates use.</p>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<table border="1" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="width:568px">
			<p><strong>Speaker Details:</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width:568px">
			<p><strong>Session 1: The Budget</strong></p>

			<p>09:15 - 09:55</p>

			<p>John Woolley, Director, Technical Connections</p>

			<p>John is a director of Technical Connection, a firm who have supplied tax and technical advice within the financial services sector for the last 27 years.</p>

			<p>John has many legal and tax qualifications and has been involved in financial planning in the financial services sector for the last 30 years.</p>

			<p>He regularly contributes articles on tax planning to various journals as well as making guest attendances as a speaker at financial services and tax conferences.</p>

			<p>John&rsquo;s experience and technical knowhow have made him an invaluable first point of contact for many clients of Technical Connection.</p>
			</td>
		</tr>
		<tr>
			<td style="width:568px">
			<p><strong>Session 2: </strong>Lifetime Cash Flow Modelling</p>

			<p>09:55 - 10:35</p>

			<p>Ben Goss, Chief Executive Officer, Distribution Technology</p>

			<p>Ben Goss is co-founder and CEO of Distribution Technology. Working with the team he has grown the company from start-up in 2003 to a leading provider of risk profiling, financial planning and wealth management technology for financial advice firms. Over 6,000 advisers from more than 500 firms use DT&rsquo;s Dynamic Planner&reg; each month to profile, plan and manage their clients. DT has the most widely used investment risk profiling process in the industry with over &pound;50bn of assets from more than 80 managers either run with reference to DT&rsquo;s asset model or profiled against its risk profiles.</p>

			<p>Ben has long believed that technology needs to play a critical role in helping advisers serve consumers and consumers access the advice they need. In 1998 he founded Sort, the UK&rsquo;s first regulated online advice firm, successfully selling the business to mPower in 2000, the US&rsquo;s largest provider of online 401k pension advice. Ben went on to become UK MD. Prior to Sort Ben was a financial services strategy consultant with PwC and Cooper &amp; Lybrand.</p>

			<p>Ben is a regular speaker at industry events and is widely quoted in the industry press and has published a number of articles on the topic of advice and technology. He is an Ernst &amp; Young Entrepreneur of the Year and winner of the Bank of Scotland Entrepreneur Challenge. DT was listed in the Sunday Times Tech Track 100 in 2007, 2008 and 2009 and won the Deloitte Fast 50, the fastest growing technology company over five years in the UK in 2009.</p>

			<p>Ben is an occasional triathlete, a Fellow of the Royal institute of Geography and a member of Bytenight which supports Action for Children.</p>
			</td>
		</tr>
		<tr>
			<td style="width:568px">
			<p><strong>Session 3: </strong>Income planning in early retirement</p>

			<p>10:35 - 11:15</p>

			<p>Kirsty Anderson, Business Development Manager, Royal London</p>

			<p>Kirsty has over 15 years of pensions experience and spends the majority of her time travelling around the UK presenting to professional advisers and employers alike; delivering almost 100 seminars in 2014 alone.</p>

			<p>Historically, Kirsty specialised in workplace pensions steering clients through the muddy waters of automatic enrolment.&nbsp; With the recent changes announced in the budget however, it&rsquo;s safe to say she couldn&rsquo;t resist the pull of navigating the unknown tides of proposed changes to pension rules at retirement.</p>

			<p>When she isn&rsquo;t travelling the country sharing her views, she has the alternative task of steering her daughter through the terror of being three which, she says, makes pensions feel like plain sailing!</p>
			</td>
		</tr>
		<tr>
			<td style="width:568px">
			<p><strong>Session 4: </strong>Diversification: more important than ever?</p>

			<p>11:40 - 12:20</p>

			<p>Adam Ryan, Portfolio Manager, Managing Director, Blackrock</p>

			<p>Adam Ryan, Managing Director and portfolio manager, is a member of BlackRock&#39;s Multi-Asset Strategies (MAS) group which is responsible for developing, assembling and managing investment strategies involving multiple asset classes. He is Head of the Diversified Strategies team and is lead portfolio manager of Dynamic Diversified Growth (DDG). Adam&#39;s service with the firm dates back to 1999, including his years with Merrill Lynch Investment Managers (MLIM), which merged with BlackRock in 2006. His background is in fixed income portfolio management where he was Head of Fixed Income for MLIM&#39;s Private Client business before developing and managing diversified multi-asset portfolios for both private and institutional clients.</p>

			<p>Mr Ryan earned a BA degree with honours in engineering from Cambridge University in 1991.</p>
			</td>
		</tr>
		<tr>
			<td style="width:568px">
			<p><strong>Session 5: </strong>Can you really have it all?</p>

			<p>12:20 -13:00</p>

			<p>Richard Sheppard, Intermediary Development Manager, MetLife</p>

			<p>Richard joined MetLife in 2009 to head up their Adviser Education Programme. He joined the financial services industry in 1984 and worked with product providers before moving to a national IFA practice where he held the role of Head of Pensions. Before joining MetLife, Richard was responsible for building the adviser engagement plan for the Personal Accounts Delivery Authority (now NEST).&nbsp;&nbsp;</p>

			<p>In his role at MetLife Richard is responsible for the design and delivery of a holistic education programme embracing the whole range of solutions available to advisers for use with their clients with a specific interest in the way that investors are influenced in their behaviour.</p>

			<p>Richard is married and has three young children. In his spare time, when he is not looking after his pigs and chickens, he helps to raise money for charity by engaging in sporting challenges and he is also the holder of a Guinness World Record!</p>
			</td>
		</tr>
		<tr>
			<td style="width:568px">
			<p><strong>Session 6: </strong>Creating highly personalised advice solutions. Planes, trains and automobiles &ndash; the multiple routes of a retirement planning journey</p>

			<p>13:40 - 14:55</p>

			<p>Tim Mason, Pension Specialist, Old Mutual Wealth</p>

			<p>Tim Mason is a Fellow of the Personal Finance Society, Certified Financial Planner, IMC &amp; Associate of the Pension Management Institute. Tim has more than 14 years&rsquo; experience in the financial service market, and has re-joined Old Mutual Wealth from Just Retirement, where he spent 3 years as Key Account Director.</p>
			</td>
		</tr>
		<tr>
			<td style="width:568px">
			<p><strong>Session 7: </strong>Crowdfunding, peer-to-peer lending, and micro funding - disentangling the issues</p>

			<p>15:15 - 15:55</p>

			<p>Kevin Caley/Malcolm Caley, TBC, ThinCats</p>

			<p>TBC</p>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<p>To access speaker photos please follow this link, where you can find all the speakers information for this Quarter: <a href="https://www.dropbox.com/sh/7rktif2fnacrwub/AADG1cDCc4HOrXuehVsCXqLBa?dl=0">https://www.dropbox.com/sh/7rktif2fnacrwub/AADG1cDCc4HOrXuehVsCXqLBa?dl=0</a></p>

<p>&nbsp;</p>

<p>If you have any queries about the above or need my help just let me know.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>
'
		]);

		\App\EmailTemplate::create([
			'type' => 'Speaker reminder',
			'reminder' => 12,
			'template' => '<p>Dear FIRST_NAME,</p>

<p>&nbsp;</p>

<p>I&rsquo;m&nbsp;getting in touch about the <strong>EVENT_NAME</strong> <strong>EVENT_REGION</strong> <strong> on </strong><strong>EVENT_START</strong></strong><strong>and thank you very much indeed for speaking on this occasion</strong><strong>. </strong></p>

<p>&nbsp;</p>

<p>Please take a moment to note the following important information.</p>

<p>&nbsp;</p>

<table border="0" cellpadding="0" cellspacing="0" style="width:652px">
	<tbody>
		<tr>
			<td style="width:338px">
			<p><strong>Number delegates to date </strong></p>
			</td>
			<td style="width:314px">
			<p><strong>132</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width:338px">
			<p><strong>Conference room</strong></p>
			</td>
			<td style="width:314px">
			<p>Dorchester Suite</p>
			</td>
		</tr>
		<tr>
			<td style="width:338px">
			<p><strong>Onsite registration starts</strong></p>
			</td>
			<td style="width:314px">
			<p>08:30 - 09:00</p>
			</td>
		</tr>
		<tr>
			<td style="width:338px">
			<p><strong>Speaker arrival</strong></p>
			</td>
			<td style="width:314px">
			<p>1 hour prior to start of speaker&rsquo;s session</p>
			</td>
		</tr>
		<tr>
			<td style="width:338px">
			<p><strong>Conference starts</strong></p>
			</td>
			<td style="width:314px">
			<p>09:00 - 09:15</p>
			</td>
		</tr>
		<tr>
			<td style="width:338px">
			<p><strong>Mid-morning break</strong></p>
			</td>
			<td style="width:314px">
			<p>11:15 - 11:35</p>
			</td>
		</tr>
		<tr>
			<td style="width:338px">
			<p><strong>Lunch</strong></p>
			</td>
			<td style="width:314px">
			<p>13:00 - 13:40</p>
			</td>
		</tr>
		<tr>
			<td style="width:338px">
			<p><strong>Afternoon session starts</strong></p>
			</td>
			<td style="width:314px">
			<p>13:40 - 14:55</p>
			</td>
		</tr>
		<tr>
			<td style="width:338px">
			<p><strong>Mid-afternoon break</strong></p>
			</td>
			<td style="width:314px">
			<p>14:55 -15:15</p>
			</td>
		</tr>
		<tr>
			<td style="width:338px">
			<p><strong>Chairman&rsquo;s closing remarks</strong></p>
			</td>
			<td style="width:314px">
			<p>15:55 - 16:00</p>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<p>Please note the following:</p>

<p>&nbsp;</p>

<table border="1" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="width:657px">
			<p><strong>SPEAKER&rsquo;S BRIEFING NOTES:</strong></p>
			</td>
		</tr>
		<tr>
			<td style="width:657px">
			<p><strong>Agenda &amp; Venue directions</strong><br />
			Please find attached agenda and venue directions for your reference.</p>
			</td>
		</tr>
		<tr>
			<td style="width:657px">
			<p><strong>Presentation</strong></p>

			<p>Although we will have your presentation installed on our laptop, ALWAYS bring a copy of the presentation with you on a memory stick, just in case of any technical glitches. Please note that you should not bring or use encrypted memory sticks.</p>

			<p>&nbsp;</p>

			<p>If you do have changes in your presentation, please send us the updated version to <a href="mailto:admin@pfsevents.org">admin@pfsevents.org</a> no later than <strong>2 days before the event</strong> and we will have this embedded in the master presentation that will be used on site.</p>
			</td>
		</tr>
		<tr>
			<td style="width:657px">
			<p><strong>Arrival</strong></p>

			<p>Please arrive one hour before your session starts.</p>
			</td>
		</tr>
		<tr>
			<td style="width:657px">
			<p><strong>Time keeping</strong></p>

			<p>Please ensure you keep a close eye on your timings and make sure your session concludes at the right time.</p>
			</td>
		</tr>
		<tr>
			<td style="width:657px">
			<p><strong>On site hostess </strong></p>

			<p>The name of the hostess supporting you on the day is <strong>Helena Harper. </strong>She will be with you 1 hour prior to registration opening and will support you should you need anything during the day.</p>
			</td>
		</tr>
		<tr>
			<td style="width:657px">
			<p><strong>Delegate Badges</strong></p>

			<p>There will be badges provided at the event for the delegates use only.</p>
			</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<p>I do hope it all goes well and look forward to receiving your feedback.</p>

<p>&nbsp;</p>

<p>Please do not hesitate to contact me for further information.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>
'
		]);
		
		\App\EmailTemplate::create([
			'type' => 'Waitinglist space available',
			'template' => '<p>Dear FIRST_NAME,</p><br/>

<p>Thank you for expressing an interest in attending the <b>EVENT_NAME</b>. A place has now become available for session <b>SESSION_NAME</b>. Your booking has been automatically confirmed.</p>
<p>If for any reason you are no longer able to attend please cancel your booking, so that we can re-allocate your place</p>
If there is anything else we can assist with, please do not hesitate to ask.

<p>Kind regards,</p>
Personal Finance Society Conference Office<br/>
P: 0845 166 8415<br/>
E: regionals@pfsevents.org<br/>
W: www.thepfs.org<br/>
'
		]);
		
		\App\EmailTemplate::create([
			'type' => 'Waitinglist cancellation',
			'template' => '<p>Dear FIRST_NAME,</p><br/>

<p>You have cancelled session <b>SESSION_NAME</b> for event <b>EVENT_NAME</b> on EVENT_START</p> 
<p>If there is anything else we can assist with, please do not hesitate to ask.</p>


<p>Kind regards,</p>
Personal Finance Society Conference Office<br/>
P: 0845 166 8415<br/>
E: regionals@pfsevents.org<br/>
W: www.thepfs.org<br/>
'
		]);
		
		\App\EmailTemplate::create([
			'type' => 'Waitinglist confirmation',
			'template' => '<p>Dear FIRST_NAME,</p><br/>

<p>This email is to confirm your booking for <b>SESSION_NAME</b> session in event <b>EVENT_NAME</b> on EVENT_START.</p>
<p>Thank you for expressing an interest in attending the EVENT_NAME</p>
<p>If there is anything else we can assist with, please do not hesitate to ask.</p>

<p>Kind regards,</p>
Personal Finance Society Conference Office<br/>
P: 0845 166 8415<br/>
E: regionals@pfsevents.org<br/>
W: www.thepfs.org<br/>
'
		]);
		
		\App\EmailTemplate::create([
			'type' => 'Public Invitation to attend event',
			'template' => '<p>Dear FIRST_NAME,</p><br/>

<p>we would like to invite you to join  <b>EVENT_NAME</b> event on EVENT_START.</p>
<p>You can see full details about the event by clicking <a href="EVENT_LINK">here</a></p>

<p>If there is anything else we can assist with, please do not hesitate to ask.</p>

<p>Kind regards,</p>
Personal Finance Society Conference Office<br/>
P: 0845 166 8415<br/>
E: regionals@pfsevents.org<br/>
W: www.thepfs.org<br/>
'
		]);
		
		\App\EmailTemplate::create([
			'type' => 'Confirmation of new registration WAITLIST',
			'template' => '<p>Dear FIRST_NAME,</p><br/>


<p>Kind regards,</p>
Personal Finance Society Conference Office<br/>
P: 0845 166 8415<br/>
E: regionals@pfsevents.org<br/>
W: www.thepfs.org<br/>
'
		]);
		
		\App\EmailTemplate::create([
			'type' => 'Late Cancel registration',
			'template' => '<p>Dear FIRST_NAME,</p>

<p>This email confirms cancellation of your place at EVENT_NAME on EVENT_START.</p>
<br />
We are sorry that you cannot make this event. We are pleased to confirm that as you have provided us with sufficient notice,&nbsp;<strong>you will not be charged the &pound;50 non-attendance fee</strong>.&nbsp;<br />
&nbsp;</p>

<p>Please check the Personal Finance Society website at&nbsp;<a href="http://www.thepfs.org/events" target="_self">www.thepfs.org/events</a>, as there might be a conference taking place somewhere else close by that you could attend.</p>

<p>Kind Regards,</p>

<p><strong>Personal Finance Society Conference office</strong><br />
0845 166 8415<br />
<a href="mailto:regionals@pfsevents.org" target="_blank">regionals@pfsevents.org</a><br />
<a href="http://www.thepfs.org/" target="_blank">www.thepfs.org</a></p>
'
		]);
		
		\App\EmailTemplate::create([
			'type' => 'Event Invitation',
			'template' => 'Hello,

We are just getting in touch because your colleague, FIRST_NAME LAST_NAME, has just registered for the Personal Finance Society’s EVENT_NAME on EVENT_START, and has suggested you might also be interested in attending.<br/>

Click here for further details and to register your place:<br/> 

Find out more » <br/>

We hope to see you at the event.<br/><br/>

Kind regards<br/>

Personal Finance Society Conference Office<br/>
0845 166 8415<br/>
regionals@pfsevents.org<br/>
http://www.thepfs.org/events<br/>

'
		]);
        
        \App\EmailTemplate::create([
            'type' => 'Admin Invitation to attend event',
            'subject' => 'Invitation to attend event',
            'template' => '<p>Dear FIRST_NAME,</p><br/>

<p>we would like to invite you to join  <b>EVENT_NAME</b> event on EVENT_START.</p>
<p>You can see full details about the event by clicking <a href="EVENT_LINK">here</a></p>

<p>To accept invitation please <a href="ACCEPT_LINK">click here</a></p>
<p>To decline invitation please <a href="DECLINE_LINK">click here</a></p>

<p>If there is anything else we can assist with, please do not hesitate to ask.</p>

<p>Kind regards,</p>
Personal Finance Society Conference Office<br/>
P: 0845 166 8415<br/>
E: regionals@pfsevents.org<br/>
W: www.thepfs.org<br/>
'
        ]);
        
        \App\EmailTemplate::create([
            'type' => 'Invitation declined',
            'subject' => 'Event invitation declined',
            'template' => '<p>Dear FIRST_NAME,</p><br/>

<p>you have successfully declined invitation for  <b>EVENT_NAME</b> event on EVENT_START.</p>

<p>If there is anything else we can assist with, please do not hesitate to ask.</p>

<p>Kind regards,</p>
Personal Finance Society Conference Office<br/>
P: 0845 166 8415<br/>
E: regionals@pfsevents.org<br/>
W: www.thepfs.org<br/>
'
        ]);
	}
}
