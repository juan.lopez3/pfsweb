<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class UsersSeeder extends Seeder {

	public function run()
	{
		\App\User::create([
			'title' => 'Mr',
			'first_name' => 'one',
			'last_name' => 'Mohen',
			'password' => bcrypt('one'),
			'email' => 'one@one.com',
			'cc_email' => 'noemail@noemail.com',
			'role_id' => '1',
			'phone' => '111111111',
			'mobile' => '99999999',
			'bio' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum at euismod nulla. Curabitur commodo molestie libero, ac vestibulum orci ultrices ac. Nunc efficitur purus sed efficitur venenatis.',
		]);

		\App\User::create([
			'title' => 'Mr',
			'first_name' => 'two',
			'last_name' => 'Martin',
			'email' => 'two@two.com',
			'cc_email' => 'noemail@noemail.com',
			'role_id' => '2',
			'phone' => '111111111',
			'mobile' => '99999999',
			'bio' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum at euismod nulla. Curabitur commodo molestie libero, ac vestibulum orci ultrices ac. Nunc efficitur purus sed efficitur venenatis.',
			'password' => bcrypt('two'),
		]);

		\App\User::create([
			'title' => 'Mr',
			'first_name' => 'three',
			'last_name' => 'Sand',
			'email' => 'three@three.com',
			'cc_email' => 'noemail@noemail.com',
			'role_id' => '3',
			'phone' => '111111111',
			'mobile' => '99999999',
			'bio' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum at euismod nulla. Curabitur commodo molestie libero, ac vestibulum orci ultrices ac. Nunc efficitur purus sed efficitur venenatis.',
			'password' => bcrypt('three'),
		]);

		\App\User::create([
			'title' => 'Mr',
			'first_name' => 'four',
			'last_name' => 'Robinson',
			'email' => 'four@four.com',
			'cc_email' => 'noemail@noemail.com',
			'role_id' => '4',
			'phone' => '111111111',
			'mobile' => '99999999',
			'bio' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum at euismod nulla. Curabitur commodo molestie libero, ac vestibulum orci ultrices ac. Nunc efficitur purus sed efficitur venenatis.',
			'password' => bcrypt('four'),
		]);

		\App\User::create([
			'title' => 'Mr',
			'first_name' => 'five',
			'last_name' => 'Pewpew',
			'email' => 'five@five.com',
			'cc_email' => 'noemail@noemail.com',
			'role_id' => '5',
			'phone' => '111111111',
			'mobile' => '99999999',
			'bio' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum at euismod nulla. Curabitur commodo molestie libero, ac vestibulum orci ultrices ac. Nunc efficitur purus sed efficitur venenatis.',
			'password' => bcrypt('five'),
		]);

		\App\User::create([
			'title' => 'Mr',
			'first_name' => 'six',
			'last_name' => 'Ban',
			'email' => 'six@six.com',
			'cc_email' => 'noemail@noemail.com',
			'role_id' => '6',
			'phone' => '111111111',
			'mobile' => '99999999',
			'bio' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum at euismod nulla. Curabitur commodo molestie libero, ac vestibulum orci ultrices ac. Nunc efficitur purus sed efficitur venenatis.',
			'password' => bcrypt('six'),
		]);

		\App\User::create([
			'title' => 'Mr',
			'first_name' => 'seven',
			'last_name' => 'Mimimi',
			'email' => 'seven@seven.com',
			'cc_email' => 'noemail@noemail.com',
			'role_id' => '7',
			'phone' => '111111111',
			'mobile' => '99999999',
			'bio' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum at euismod nulla. Curabitur commodo molestie libero, ac vestibulum orci ultrices ac. Nunc efficitur purus sed efficitur venenatis.',
			'password' => bcrypt('seven'),
		]);

		\App\User::create([
			'title' => 'Mr',
			'first_name' => 'eight',
			'last_name' => 'Reta',
			'email' => 'eight@eight.com',
			'cc_email' => 'noemail@noemail.com',
			'role_id' => '8',
			'phone' => '111111111',
			'mobile' => '99999999',
			'bio' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum at euismod nulla. Curabitur commodo molestie libero, ac vestibulum orci ultrices ac. Nunc efficitur purus sed efficitur venenatis.',
			'password' => bcrypt('eight'),
		]);

	}
}
