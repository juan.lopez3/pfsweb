<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class RegistrationStatusSeeder extends Seeder {

	public function run()
	{

		\App\RegistrationStatus::create([
			'title' => 'Booked',
		]);

		\App\RegistrationStatus::create([
			'title' => 'Cancelled',
		]);
		
		\App\RegistrationStatus::create([
			'title' => 'Invited',
		]);
		
		\App\RegistrationStatus::create([
			'title' => 'Waitlisted',
		]);
		
		\App\RegistrationStatus::create([
			'title' => 'Late Cancelled',
		]);
        
        \App\RegistrationStatus::create([
            'title' => 'Declined',
        ]);
	}

}
