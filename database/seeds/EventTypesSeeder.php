<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class EventTypesSeeder extends Seeder {

	public function run()
	{
		\App\EventType::create(['title'=>'Regional']);
		\App\EventType::create(['title'=>'Specialist Regional Roadshows']);
		\App\EventType::create(['title'=>'Mind-Stretcher Dinners']);
		\App\EventType::create(['title'=>'National Conference']);
	}
}
