<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventTypeStepForms extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('type_step_forms', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('form_title', 60);
			$table->string('form_description', 125);
			$table->text('form_json');
			$table->integer('event_type_step_id')->unsigned();
			$table->foreign('event_type_step_id')->references('id')->on('event_type_steps')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('type_step_forms');
	}

}
