<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventAttendeeScheduleSlotSessionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event_attendee_slot_session', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('event_attendee_id')->unsigned();
			$table->foreign('event_attendee_id')->references('id')->on('event_atendees')->onDelete('cascade');
			$table->integer('event_schedule_slot_id')->unsigned();
			$table->foreign('event_schedule_slot_id')->references('id')->on('event_schedule_slots')->onDelete('cascade');
			$table->integer('registration_status_id')->unsigned()->default(1);
			$table->foreign('registration_status_id')->references('id')->on('registration_status')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event_attendee_slot_session');
	}

}
