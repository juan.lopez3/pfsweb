<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CraeteEmailTemplateEventTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('email_template_event', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('event_id')->unsigned();
			$table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
			$table->integer('email_template_id')->unsigned();
			$table->foreign('email_template_id')->references('id')->on('email_templates')->onDelete('cascade');
			$table->tinyInteger('reminder')->unsigned()->nullable();
			$table->tinyInteger('active')->unsigned()->default(1);
			$table->string('subject', 150);
			$table->text('template')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('email_template_event');
	}

}
