<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIssuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('issues', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string('title');
            $table->string('description', 5000);
            $table->tinyInteger('status')->unsigned()->default(1);
            $table->tinyInteger('type')->unsigned()->default(1);
            $table->boolean('confirmed')->default(false);
            $table->smallInteger('priority')->default(1);
            $table->decimal('quote_min', 5, 2)->nullable();
            $table->decimal('quote_max', 5, 2)->nullable();
            $table->decimal('quote_final', 5, 2)->nullable();
            $table->date('due');
            $table->integer('assigned_to')->unsigned();
            $table->foreign('assigned_to')->references('id')->on('users')->onDelete('restrict');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('issues');
	}

}
