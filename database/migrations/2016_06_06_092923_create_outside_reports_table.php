<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutsideReportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('outside_reports', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name', 150);
            $table->string('email', 150);
            $table->string('filter_from');
            $table->string('filter_to');
            $table->tinyInteger('event_type_id')->nullable();
            $table->tinyInteger('interval')->unsigned();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('outside_reports');
	}

}
