<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventOnsiteDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event_onsite_details', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('event_id')->unsigned();
			$table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
			$table->string('duty_manager_name', 50)->nullable();
			$table->string('duty_manager_number', 15)->nullable();
			$table->string('av_technician_name', 50)->nullable();
			$table->string('av_technician_number', 15)->nullable();
			$table->string('venue_note', 1000)->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event_onsite_details');
	}

}
