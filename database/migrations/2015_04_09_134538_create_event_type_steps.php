<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventTypeSteps extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event_type_steps', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title', 60);
			$table->string('description', 125);
			$table->smallInteger('order')->default(1);
			$table->integer('event_type_id')->unsigned();
			$table->foreign('event_type_id')->references('id')->on('event_types')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event_type_steps');
	}

}
