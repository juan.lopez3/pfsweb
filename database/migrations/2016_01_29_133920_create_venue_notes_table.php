<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVenueNotesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('venue_notes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('note', 1000);
			$table->integer('venue_id')->unsigned();
			$table->foreign('venue_id')->references('id')->on('venues')->onDelete('cascade');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->tinyInteger('priority')->default(1);
			$table->string('tags', 60)->nullable();
			$table->string('comment', 400)->nullable();
			$table->tinyInteger('order')->unsigned()->default(1);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('venue_notes');
	}

}
