<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateThemesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('themes', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('title');
            $table->string('title_background_color', 10)->default('#FFFFFF');
            $table->string('title_text_color', 10)->default('#000000');
            $table->string('text_color', 10)->default('#000000');
            $table->string('link_color', 10)->default('#B3A3C7');
            $table->string('link_active_color', 10)->default('#4C52A3');
            $table->string('link_hover_color', 10)->default('#4C52A3');
            $table->string('link_visited_color', 10)->default('#4C52A3');
            $table->string('book_now_hover_color', 10)->default('#B3A3C7');
            $table->string('twitter_image', 200)->nullable();
            $table->string('background_image', 200)->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('themes');
	}

}
