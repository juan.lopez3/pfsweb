<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMobileAppTextTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mobile_app_text', function(Blueprint $table)
		{
			$table->increments('id');
            $table->text('sign_up')->nullable();
            $table->text('forgot_pin')->nullable();
            $table->text('exchange_business_cards')->nullable();
            $table->text('no_connection')->nullable();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mobile_app_text');
	}

}
