<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSavedReportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('saved_reports', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 100);
			$table->string('type', '40');
			$table->text('data');
            $table->boolean('client_report')->default(false);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('saved_reports');
	}

}
