<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventScheduleSlotQuestionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event_schedule_slot_questions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('event_schedule_slot_id')->unsigned();
			$table->foreign('event_schedule_slot_id')->references('id')->on('event_schedule_slots')->onDelete('cascade');
			$table->string('question', 300);
			$table->string('type', 15);
			$table->string('answers', 250)->nullable();
			$table->string('available', 250)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event_schedule_slot_questions');
	}

}
