<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVenuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('venues', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 50)->unique();
			$table->string('address', 170);
			$table->string('city', 40);
			$table->string('county', 50);
			$table->string('postcode', 15);
			$table->index('postcode');
			$table->string('details', 4000)->nullable();
			$table->smallInteger('bedrooms')->nullable();
			$table->string('email', 150)->nullable();
			$table->string('website', 200)->nullable();
			$table->string('venue_phone', 15)->nullable();
			$table->integer('venue_staff_id')->unsigned()->nullable();
			$table->foreign('venue_staff_id')->references('id')->on('users')->onDelete('set null');
			$table->smallInteger('min_guaranteed')->unsigned()->nullable();
			$table->smallInteger('commission')->unsigned()->nullable();
			$table->boolean('show_map')->default(false);
			$table->string('longitude', 20)->nullable();
			$table->string('latitude', 20)->nullable();
			$table->string('image_1', 150)->nullable();
			$table->string('image_2', 150)->nullable();
			$table->string('rating', 15)->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('venues');
	}

}
