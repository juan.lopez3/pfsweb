<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventVenueRoomTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event_venue_room', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('event_id')->unsigned();
			$table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
			$table->integer('venue_room_id')->unsigned();
			$table->foreign('venue_room_id')->references('id')->on('venue_rooms')->onDelete('cascade');
			$table->string('meeting_space_name', 50);
			$table->tinyInteger('layout_style')->unsigned();
			$table->smallInteger('num_of_tables')->unsigned()->nullable();
			$table->string('setup_note', 250)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event_venue_room');
	}

}
