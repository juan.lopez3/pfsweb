<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('password', 60)->nullable();
			$table->string('pin', 10)->nullable();
			$table->string('title', 5);
			$table->string('first_name', 40);
            $table->string('middle_name', 40)->nullable();
			$table->string('last_name', 40);
			$table->string('email')->unique();
			$table->string('cc_email')->nullable();
			$table->string('postcode', 15);
            $table->integer('country_id')->unsigned()->nullable();
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('set null');
			$table->string('company', 60)->nullable();
			$table->string('job_title', 60)->nullable();
			$table->string('phone', 15)->nullable();
			$table->string('mobile', 15)->nullable();
            $table->string('fca_number', 30)->nullable();
			
			$table->integer('role_id')->unsigned();
			$table->foreign('role_id')->references('id')->on('user_roles')->onDelete('restrict');
			
			$table->string('pfs_class', 25)->nullable();
			$table->string('job_role', 25)->nullable();
			$table->boolean('chartered')->default(false);
            $table->string('invited_by', 150)->nullable();
			
			$table->string('badge_name', 40)->nullable();
			$table->integer('dietary_requirement_id')->unsigned()->nullable()->default(1);
			$table->foreign('dietary_requirement_id')->references('id')->on('dietary_requirements')->onDelete('set null');
			$table->string('dietary_requirement_other', 200)->nullable();
			$table->string('special_requirements', 150)->nullable();
			$table->string('bio', 2500)->nullable();
			$table->string('profile_image', 60)->nullable();
			
            $table->boolean('available_in_chat')->default(false);

            $table->string('login_shortcode', 6)->nullable();
            $table->dateTime('login_shortcode_set')->nullable();
            
			$table->rememberToken();
			$table->timestamps();
			
			$table->index('pin');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
