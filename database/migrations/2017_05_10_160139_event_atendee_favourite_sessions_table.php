<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EventAtendeeFavouriteSessionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event_atendee_favourite_sessions', function(Blueprint $table)
		{
		    $table->increments('id');
            $table->integer('event_attendee_id')->unsigned();
            $table->foreign('event_attendee_id')->references('id')->on('event_atendees')->onDelete('cascade');
            $table->integer('slot_session_id')->unsigned();
            $table->foreign('slot_session_id')->references('id')->on('event_schedule_slot_sessions')->onDelete('cascade');
            $table->integer('event_schedule_slot_id')->unsigned();
            $table->foreign('event_schedule_slot_id')->references('id')->on('event_schedule_slots')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event_atendee_favourite_sessions');
	}

}
