<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVenueRoomsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('venue_rooms', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('venue_id')->unsigned();
			$table->foreign('venue_id')->references('id')->on('venues')->onDelete('cascade');
			$table->string('room_name', 50);
			$table->smallInteger('capacity')->unsigned()->nullable();
			$table->smallInteger('cabaret_capacity')->unsigned()->nullable();
			$table->smallInteger('theatre_capacity')->unsigned()->nullable();
			$table->smallInteger('max_tables')->unsigned()->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('venue_rooms');
	}

}
