<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventEquipmentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event_technology', function(Blueprint $table)
		{
			$table->integer('event_id')->unsigned();
			$table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
			$table->integer('technology_id')->unsigned();
			$table->foreign('technology_id')->references('id')->on('technologies')->onDelete('cascade');
			$table->smallInteger('quantity')->unsigned()->nullable();
			$table->string('provided_by', 25)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event_technology');
	}

}
