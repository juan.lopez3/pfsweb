<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('events', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title')->unique();
			$table->string('description', 10000)->nullable();
			$table->string('registration_text', 1000)->nullable();
			$table->string('slug')->unique();
			$table->index('slug');
			$table->integer('region_id')->unsigned()->nullable();
			$table->foreign('region_id')->references('id')->on('regions')->onDelete('set null');
			$table->string('county', 40)->nullable();
			$table->integer('event_type_id')->unsigned();
			$table->foreign('event_type_id')->references('id')->on('event_types')->onDelete('restrict');
			$table->integer('venue_id')->unsigned()->nullable();
			$table->foreign('venue_id')->references('id')->on('venues')->onDelete('cascade');
			$table->date('event_date_from')->nullable();
			$table->date('event_date_to')->nullable();
			$table->dateTime('cut_of_date')->nullable();
			$table->dateTime('amendment_date')->nullable();
            $table->dateTime('late_cencellation_date')->nullable();
			$table->string('browser_title');
			$table->string('outside_link')->nullable();
			$table->integer('tfi_contact_id')->unsigned()->nullable();
			$table->foreign('tfi_contact_id')->references('id')->on('users')->onDelete('set null');
			$table->boolean('invite_only')->default(false);
			$table->boolean('invitation_allowed')->default(true);
			$table->string('banner', 200)->nullable();
            $table->string('app_banner', 200)->nullable();
			$table->smallInteger('registration_type_id')->unsigned()->default(1);
			$table->tinyInteger('notification_level')->unsigned()->default(0);
			$table->boolean('main_schedule_visible_web')->default(0);
			$table->boolean('main_schedule_visible_reg')->default(0);
			$table->boolean('mandatory_slots')->default(0);
			$table->string('hashtag')->nullable();
			$table->boolean('publish')->default(false);
            $table->smallInteger('capacity')->unsigned()->nullable();
            $table->boolean('app_enabled')->default(false);
            $table->boolean('local_committee')->default(false);
			$table->boolean('cpd_download')->default(false);
			$table->string('agenda_pdf')->nullable();
            $table->string('floor_plan', 100)->nullable();
            $table->integer('theme_id')->unsigned()->default(1);
            $table->foreign('theme_id')->references('id')->on('themes')->onDelete('restrict');
            $table->tinyInteger('schedule_version')->default(0);
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('events');
	}

}
