<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventScheduleSlotSessions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event_schedule_slot_sessions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->time('session_start');
			$table->time('session_end');
			$table->boolean('show_start')->default(0);
			$table->boolean('show_end')->default(0);
            $table->boolean('use_api_start')->default(0);
            $table->integer('meeting_space_id')->unsigned()->nullable();
            $table->foreign('meeting_space_id')->references('id')->on('event_venue_room')->onDelete('cascade');
            $table->smallInteger('capacity')->unsigned()->nullable();
            $table->boolean('selectable')->default(false);
			$table->integer('session_type_id')->unsigned();
			$table->foreign('session_type_id')->references('id')->on('session_types')->onDelete('cascade');
			$table->integer('event_schedule_slot_id')->unsigned();
			$table->foreign('event_schedule_slot_id')->references('id')->on('event_schedule_slots')->onDelete('cascade');
			$table->string('title', 200);
			$table->string('description', 5000)->nullable();
			$table->string('learning_objectives', 5000)->nullable();
			$table->string('question', 200)->nullable();
			$table->boolean('include_cpd_log')->default(0);
			$table->tinyInteger('order_column')->unsigned()->default(1);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event_schedule_slot_sessions');
	}

}
