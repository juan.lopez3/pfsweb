<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventGalleriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event_galleries', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('event_id')->unsigned()->default(1);
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
            $table->string('image', 200);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event_galleries');
	}

}
