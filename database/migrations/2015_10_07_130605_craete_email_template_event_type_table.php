<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CraeteEmailTemplateEventTypeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('email_template_event_type', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('email_template_id')->unsigned();
			$table->foreign('email_template_id')->references('id')->on('email_templates')->onDelete('cascade');
			$table->integer('event_type_id')->unsigned();
			$table->foreign('event_type_id')->references('id')->on('event_types')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('email_template_event_type');
	}

}
