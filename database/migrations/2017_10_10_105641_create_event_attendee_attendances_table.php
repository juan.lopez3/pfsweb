<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventAttendeeAttendancesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event_attendee_attendances', function(Blueprint $table)
		{
			$table->integer('event_attendee_id')->unsigned()->nullable();
            $table->foreign('event_attendee_id')->references('id')->on('event_atendees')->onDelete('cascade');
            $table->integer('session_id')->unsigned()->nullable();
            $table->foreign('session_id')->references('id')->on('event_schedule_slot_sessions')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event_attendee_attendances');
	}

}
