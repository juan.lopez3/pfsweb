<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventSponsorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event_sponsor', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('event_id')->unsigned();
			$table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
			$table->integer('sponsor_id')->unsigned();
			$table->foreign('sponsor_id')->references('id')->on('sponsors')->onDelete('cascade');
			$table->tinyInteger('type')->default(0); // 0 - sponsor; 1 - exhibitor
			$table->string('stand_number', 25)->nullable();
			$table->string('category_1', 50)->nullable();
			$table->string('category_2', 50)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event_sponsor');
	}

}
