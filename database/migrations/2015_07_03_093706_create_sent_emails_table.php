<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSentEmailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sent_emails', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('to', 150)->index();
			$table->string('subject', 200)->nullable();
			$table->text('template')->nullable();
			$table->integer('user_id')->unsigned()->nullable();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
			$table->integer('event_id')->unsigned()->nullable();
			$table->foreign('event_id')->references('id')->on('events')->onDelete('set null');
			$table->datetime('opened')->nullable();
			$table->string('campaign', 50)->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sent_emails');
	}

}
