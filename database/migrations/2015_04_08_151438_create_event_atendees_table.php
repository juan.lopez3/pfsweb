<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventAtendeesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event_atendees', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->integer('event_id')->unsigned();
			$table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
			$table->integer('registration_status_id')->unsigned()->default(1);
			$table->foreign('registration_status_id')->references('id')->on('registration_status')->onDelete('restrict');
			$table->dateTime('checkin_time')->nullable();
			$table->dateTime('checkout_time')->nullable();
			$table->boolean('sponsors_contact')->default(false);
            $table->boolean('first_time_attendee')->default(false);
			$table->boolean('paid')->default(0);
			$table->boolean('manual_booking')->default(0);
            $table->boolean('fee_waived')->default(0);
			$table->string('cancel_reason', 400)->nullable();
            $table->boolean('onsite_booking')->default(false);
            $table->boolean('api_booking')->default(false);
            $table->boolean('comitee')->default(false);
            $table->string('notes', 500)->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event_atendees');
	}

}
