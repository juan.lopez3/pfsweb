<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventScheduleSlots extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event_schedule_slots', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title', 200)->nullable();
			$table->integer('event_id')->unsigned();
			$table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
			$table->integer('meeting_space_id')->unsigned()->nullable();
			$table->foreign('meeting_space_id')->references('id')->on('event_venue_room')->onDelete('cascade');
			$table->date('start_date');
			$table->time('start');
			$table->date('end_date');
			$table->time('end');
			$table->boolean('show_start')->default(false);
            $table->boolean('show_end')->default(false);
			$table->smallInteger('slot_capacity')->unsigned()->nullable();
			$table->boolean('bookable')->default(0);
			$table->boolean('stop_booking')->default(0);
			$table->tinyInteger('notification_level')->unsigned()->default(0);
			$table->tinyInteger('available_chartered')->default(0);
			$table->boolean('notified')->default(false);
			$table->tinyInteger('columns')->unsigned()->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event_schedule_slots');
	}

}
