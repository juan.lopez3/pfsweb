<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSessionTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('session_types', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title', 60);
			$table->string('description', 125);
			$table->string('color', 10);
			$table->boolean('include_in_cpd')->default(false);
			$table->boolean('include_in_feedback')->default(false);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('session_types');
	}

}
