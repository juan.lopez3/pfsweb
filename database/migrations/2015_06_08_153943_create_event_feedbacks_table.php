<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventFeedbacksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event_feedbacks', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('event_id')->unsigned();
			$table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
			$table->string('question', 2000)->nullable();
			$table->string('answer', 500)->nullable();
			$table->integer('session_id')->unsigned()->nullable();
			$table->foreign('session_id')->references('id')->on('event_schedule_slot_sessions')->onDelete('cascade');
			$table->smallInteger('type');
			$table->boolean('checkin_required')->default(false);
			$table->smallInteger('position')->unsigned()->default(1);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event_feedbacks');
	}

}
