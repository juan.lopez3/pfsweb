<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSponsorSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sponsor_settings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title', 50);
			$table->date('date_from');
			$table->date('date_to');
			$table->string('logo_1', 10)->nullable();
			$table->string('logo_2', 10)->nullable();
			$table->string('logo_3', 10)->nullable();
			$table->string('logo_4', 10)->nullable();
            $table->string('logo_5', 10)->nullable();
            $table->string('logo_6', 10)->nullable();
            $table->string('logo_7', 10)->nullable();
            $table->string('logo_8', 10)->nullable();
            $table->string('logo_9', 10)->nullable();
            $table->string('logo_10', 10)->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sponsor_settings');
	}

}
