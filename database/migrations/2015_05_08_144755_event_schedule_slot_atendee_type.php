<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EventScheduleSlotAtendeeType extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event_schedule_slot_attendee_type', function(Blueprint $table)
		{
			$table->integer('event_schedule_slot_id')->unsigned();
			$table->foreign('event_schedule_slot_id')->references('id')->on('event_schedule_slots')->onDelete('cascade');
			$table->integer('attendee_type_id')->unsigned();
			$table->foreign('attendee_type_id')->references('id')->on('attendee_types')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event_schedule_slot_attendee_type');
	}

}
