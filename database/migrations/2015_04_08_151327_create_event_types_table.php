<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event_types', function(Blueprint $table)
		{
			$table->increments('id');
            $table->boolean('exhibition')->default(FALSE);
			$table->string('title', 70);
			$table->text('terms_conditions')->nullable;
			$table->text('amendment_text')->nullable;
			$table->text('cancel_text')->nullable;
			$table->string('branding_image', 100)->nullable();
            $table->string('email_branding', 100)->nullable();
            $table->string('agenda_branding', 150)->nullable();
            
            $table->string('app_branding', 100)->nullable();
            $table->string('app_icon', 100)->nullable();
            $table->string('app_description', 2000)->nullable();
            $table->string('app_promotional_banner', 100)->nullable();
            $table->string('app_color', 10)->nullable();
            $table->boolean('app_sponsors_active')->default(FALSE);
            
            $table->boolean('app_additional_active')->default(FALSE);
            $table->string('app_additional_title', 150)->nullable();
            $table->text('app_additional_content')->nullable();
            $table->string('feedback_type', 20)->default('traditional');
            
			$table->boolean('has_sponsor')->default(0);
			$table->integer('email_address_id')->unsigned()->nullable();
			$table->foreign('email_address_id')->references('id')->on('email_addresses')->onDelete('set null');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('event_types');
	}

}
