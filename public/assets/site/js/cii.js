﻿/* Causes script errors to get posted back and logged in Elmah */
window.onerror = function(msg, url, line) {
    var error = "url: " + url + " line: " + line + " msg: " + msg;
    $.post("/handlers/LogJavascriptError.ashx", { Error: error });
};
var previousError = false;

$(function() {
    $("#slides").slides({
        play: 5000,
        pause: 2500,
        hoverPause: true,
        effect: 'fade'
    });
});


//must be set for add or remove from basket
var BookExamNowOrLater = '';
var InvalidQuantity = '';
var PleaseChoose = '';
var LimitedBooking = '';
var basketCaptions = { caption1: "", text1: "", caption2: "", text2: "" };


$(document).ready(function() {

    $("#login-link").click(function(event) {
        event.preventDefault();
        var href = $('a.loginframe').attr('href');
        $("a.loginframe").fancybox({
            'type': 'iframe',
            'href': href,
            'autoDimensions': true,
            'width': 720,
            'height': 470,
            'hideOnOverlayClick': false,
            'hideOnContentClick': false,
            'showNavArrows': false,
            'scrolling': 'no'
        }).trigger('click');
    });


//book overlay
    $("a.book").click(function(event) {
        event.preventDefault();

        var href = $(this).attr('href') + '?ol=1&source=8';


        $("a.iframe").fancybox({
            'href': href,
            'autoDimensions': true,
            'width': 720,
            'height': 900,
            'hideOnOverlayClick': false,
            'hideOnContentClick': false,
            'showNavArrows': false,
            'scrolling': 'yes'
        }).trigger('click');


    });

    // Remove from basket
    $("#products-list .remove-from-basket").click(function(event) {
        event.preventDefault();
        var container = $(this).parent().parent().parent();
        var productCode = $(container).find("input[name='ProductCode'], select[name='ProductCode'], .remove-training").val();

        RemoveFromBasket(productCode, function() {
            // Switch the remove to an add button (only if a remove div exists)
            if ($(container).find('div:.remove-from-basket').html() != null) {
                $(container).find('div:.add-to-basket').show();
                $(container).find('div:.remove-from-basket').hide();
            }
        });
    });

    //Add to basket
    $("#products-list input[type='image']:.add-to-basket, #products-list input[type='submit']:.add-to-basket").click(function(event) {

        event.preventDefault();
        var container = $(this).parent().parent();
        showError(container, "");

        // Prevent an exam being added when the drop down optin is select
        // "as Book exam now"
        if (event.target.id.indexOf("addToBasketForKitWithExam") == 0);
        {
            var pCode = event.target.id.replace("addToBasketForKitWithExam_", "");
            var id = "#bookNowOrLaterKitWithExam_" + pCode;
            if ($(id).val() == 'now') return false;
            if ($(id).val() == '') {
                showError(container, BookExamNowOrLater);
                return false;
            }
        }
        var qty = 1;
        if ($(container).find("input[name^='Quantity']").html() != null) {
            qty = $(container).find("input[name^='Quantity']").val();

            if (parseFloat(qty) != parseInt(qty) || qty != String(parseInt(qty)) || isNaN(qty) || qty < 1) {
                showError(container, InvalidQuantity);
                return false;
            }
        }


        var productCode = $(container).find("input[name^='ProductCode']:not([type='radio']), input[name^='ProductCode'][type='radio']:checked, select[name^='ProductCode']").val();
        var productType = $(container).find("input[name^='ProductType']").val();
        var detailedSession = '';
        var sessionCode = '';
        if (productType == 'TrainingCourse') {
            var prodCombo = productCode.split('|');
            productCode = prodCombo[0];
            sessionCode = prodCombo[1];
            detailedSession = prodCombo[2];
        }


        if (productCode == "-1")
            showError(container, PleaseChoose);
        else if (productCode == "-10")
            showError(container, LimitedBooking);
        else if (productType == 'TrainingCourse' && detailedSession != '') {
            location.href = detailedSession + '?ID=' + productCode + '&BackUrl=' + encodeURI(document.url) + '&';
        } else {
            var cb = function(basket) {
                // This code gets called after a successful add
                if ($(container).parent().find('div:.remove-from-basket').html() != null) {
                    $(container).parent().find('div:.add-to-basket').hide();
                    $(container).parent().find('div:.remove-from-basket').show();
                }
                // If they click the remove button in the popup, it should act the same
                // as the popup in the page
                $(".basketcontainer .remove-from-basket").unbind('click').click(function(event) {
                    event.preventDefault();
                    $(container).parent().find("input[type=image]:.remove-from-basket").first().click();
                });

                // wire up the remove button to remove the correct product code 
                $(container).parent().find(".remove-from-basket").unbind('click').click(function(event) {
                    event.preventDefault();
                    RemoveFromBasket(basket.RecentItem.ProductCode);
                    $(container).parent().find('div:.add-to-basket').show();
                    $(container).parent().find('div:.remove-from-basket').hide();
                });

                //populate hidden value for training course, so we know which has been removed
                $(container).parent().parent().parent().find('.remove-training').val(productCode);
            };

            AddToBasket(productType, productCode, qty, basketCaptions, cb, sessionCode);
        }


        $('.fancyClose').click(function(event) {
            event.preventDefault();
            $.fancybox.close();
        });

    });


    function showError(container, errorMessage) {
        if (errorMessage == "")
            $(container).parent().find('#errorMessageDiv').hide();
        else {
            $(container).parent().find('#errorMessageDiv').show();
            $(container).parent().find('#errorMessage').text(errorMessage);
        }

    }

    // GA Push for downloads
    $('a').click(function() {
        try {
            var file = $(this).attr('href');
            file = file.toLowerCase();
            var extension = file.substr((file.lastIndexOf('.') + 1));
            switch (extension) {
            case 'pdf':
            case 'doc':
            case 'docx':
            case 'xls':
            case 'xlsx':
            case 'epub':
            case 'mobi':
            case 'mp3':
                _gaq.push(['_trackEvent', 'Download', 'Click', file]);
                break;
            }
        } catch (err) {
        }
    });


    $(".ciidd li").click(function(event) {
        var val = $("a", this).attr('href');
        location.href = val;
        return false;
    });

    $(".ciidd > div").click(function(event) {
        var visible = $("ul", this).is(":visible");
        $(".ciidd ul").hide();

        if (!visible) $("ul", this).show();
        return false;
    });

    $(".ciidd ul li").hover(
        function() {
            $(this).toggleClass('lihover');
        },
        function() {
            $(this).toggleClass('lihover');
        }
    );

    $(":not(.ciidd):not(.ciidd *)").click(function(event) {
        $(".ciidd ul").hide();
    });

    $("#edit-address-save-button").click(function(event) {
        var retVal = true;
        $('.valAddress1').hide();
        $('.valPostCode').hide();
        $('.valCountry').hide();

        if ($('#editAddress1').val().length == 0) {
            $('.valAddress1').show();
            retVal = false;
        }

        if ($('#editPostCode').val().length > 10 || $('#editPostCode').val().length < 3) {
            $('.valPostCode').show();
            retVal = false;
        }

        if ($('#editCountry').val() == 'default') {
            $('.valCountry').show();
            retVal = false;
        }

        if (!retVal) {
            if (!event)
                event = window.event;
            if (event.preventDefault) {
                event.preventDefault();
            } else {
                window.event.returnValue = false;
            }
        } else {
            order_updateAddress();
        }
    });


    //js enabled so rollup styled form drop downs
    $('.frmdd input:radio').addClass('rdo_js');
    $('.frmdd label').addClass('lbl_js');
    $('.frmdd .list').addClass('list_js');

    $('.frmdd .list').hide();

    $('.frmdd .list label').click(ddSel);

    $(':not(.frmdd):not(.frmdd *)').click(
        function() {
            $('.frmdd .list').hide();
        }
    );

    //styled drop-down for form submit
    $('.frmdd .f').click(
        function() {
            var list = $(this).parent().find('.list');
            var visible = list.is(':visible');
            $('.frmdd .list').slideUp('fast');
            if (!visible) list.slideDown('fast');
            return false;
        }
    );

    $('input:checked').each(function(i) {
        if ($(this).parent().parent().hasClass('frmdd')) {
            //styled drop downs
            var lb = $(this).parent().find('label[for=' + $(this).attr('id') + ']');
            if (lb != null) ddSel(lb);
        } else {
            radio($(this).parent().find('div'));
        }
    });


});


function loginFormSubmit(event, url) {
    if (previousError)
        return;

    /* stop form from submitting normally */
    event.preventDefault();
    /* get some values from elements on the page: */
    var LoginEmail = $('#LoginEmail').val();
    var LoginPassword = $('#LoginPassword').val();
    $.fancybox.showActivity();

    /* Send the data using post and put the results in a div */
    $.ajax({
            url: url,
            dataType: 'jsonp',
            data: { LoginEmail: LoginEmail, LoginPassword: LoginPassword, AjaxPost: true, LoginPosted: true }
        })
        .done(function(data) {
            $.fancybox.hideActivity();
            if (data.success) {
                var redirectUrl = $('#login-pop-requestedUrl').val();
                $('#validationError').hide();
                $.fancybox.close();
                $('#login-div').hide();
                $('#logout-div').show();

                if (redirectUrl == '#') {
                    redirectUrl = "/";
                }

                window.top.location = redirectUrl;
            } else {
                $('#validationError').show().text(data.message);
            }
        })
        .fail(function() {
            $('form').attr('action', url);
            $('form').submit();
            $('#validationError').show().text('An error occured');
        })
        .always(function() {
            $.fancybox.hideActivity();
        });
    submitting = false;
};


function logActivity() {
    var activity = $('#selActivity').val();

    if (activity != "None") {
        var nodeId = $('#nodeId').val();
        var url = "/handlers/LogActivity.ashx";
        $.fancybox.showActivity();

        /* Send the data using post and put the results in a div */
        $.ajax({
                url: url,
                data: { selActivity: activity, AjaxPost: true, nodeId: nodeId }
            }).done(function(data) {
                $.fancybox.hideActivity();
                if (data.success) {
                    var text = $('#ActivityConfirm').html();
                    $.fancybox(text, { 'autoDimensions': true, 'scrolling': false, 'padding': 20 });

                    //todo Show Confirm box.
                } else
                    $.fancybox('An error occured<p/>' + data.message, { 'autoDimensions': true, 'scrolling': false, 'padding': 20 });
            })
            .fail(function(xhr, ajaxOptions, thrownError) {
                $.fancybox.hideActivity();
                alert(thrownError);
            })
            .always(function() {
                $.fancybox.hideActivity();
            });
        submitting = false;
    }
};

//validate textboxes
function TextBox_Validate(source, args) {

    //textbox to validate
    var textbox = $("#" + source.controltovalidate);

    //set status
    Validation_Status(textbox, !(args.Value.length == 0 || $.trim($(textbox).val()) == ""), args);
}

//validate dropdown
function Dropdown_Validate(source, args) {

    //dropdown to validate
    var dropdown = $("#" + source.controltovalidate);

    //set status
    Validation_Status(dropdown, !(dropdown.val() == -1 || dropdown.val().length == 0), args);
}

function Validation_Status(control, isValid, args) {

    //was validation successful?
    if (isValid) {
        control.removeClass("inputtext-error");
        args.IsValid = true;
    } else {
        control.addClass("inputtext-error");
        args.IsValid = false;
    }
}

//validate counties
function CountyDropdown_Validate(source, args) {

    //country dropdown
    var countryDropdown = $('#' + $("#" + source.id).attr('ddlCountryId'));

    //county dropdown
    var countyDropdown = $("#" + source.controltovalidate);

    //if united kingdom is selected, user must select a county
    if (countryDropdown.val() == 'GB' && countyDropdown.val() <= 0)
        Validation_Status(countyDropdown, false, args);
    else
        Validation_Status(countyDropdown, true, args);
}

function CountyPanel_Validate(source, args) {

    //country dropdown
    var countryPanel = $('#' + $("#" + source.id).attr('pnlCountry'));

    //county dropdown
    var countyDropdown = $("#" + source.controltovalidate);

    //if united kingdom is selected, user must select a county
    if (jQuery.trim(countryPanel.text()) == 'United Kingdom' && countyDropdown.val() <= 0)
        Validation_Status(countyDropdown, false, args);
    else
        Validation_Status(countyDropdown, true, args);
}


function Faculties_ValidateCheckBoxList(sender, args) {
    args.IsValid = false;

    //checkboxlist to validate
    var checkboxlist = $("#FacultiesContent").find("input:checkbox");

    $(checkboxlist).each(function() {
        if ($(this).attr("checked")) {
            args.IsValid = true;
            //return;
        }
    });

    //Requirement: At least 1 checkbox 'checked'
    CheckboxList_Validation_Error(checkboxlist, args.IsValid);
}

function CheckboxList_Validation_Error(control, IsValid) {

    $(control).each(function() {
        if (IsValid)
            $(this).removeClass("inputcheck-error");
        else
            $(this).addClass("inputcheck-error");
    });
}

function Validate_Declaration(sender, args) {
    var checkbox = $("#checkbankaccount");

    if ($(checkbox).attr("checked")) {
        args.IsValid = true;
        $(checkbox).removeClass("inputcheck-error");
    } else {
        args.IsValid = false;
        $(checkbox).addClass("inputcheck-error");
    }
}

function Validate_OrderConfirmation(sender, args) {
    var checkbox = $("#chkOrderConfirmation");

    if ($(checkbox).attr("checked")) {
        args.IsValid = true;
        $(checkbox).removeClass("inputcheck-error");
    } else {
        args.IsValid = false;
        $(checkbox).addClass("inputcheck-error");
    }
}

function Validate_EmploymentStatus(sender, args) {

    //check if user have selected an employment status
    if ($('.EmploymentStatus input').is(':checked'))
        args.IsValid = true;
    else
        args.IsValid = false;
}

function SubmitMembershipDetails(button) {

    //disable button
    $(button).attr("disabled", true);

    //validate page
    //    if (!Page_ClientValidate()) {
    //        
    //        //scroll to top of the page
    //        $('html, body').animate({ scrollTop: 0 }, 'fast');

    //        //display error message block
    //        $(".errormessageblock").show();
    //    }

    return true;
}

var homeAddress;
var workAddress;
var basketDisplayMode = "All";

function createAddressObj(Id, Company, Address1, Address2, Address3, PostCode, Country, County, City) {
    var address = { Id: Id, Company: Company, AddressLine1: Address1, AddressLine2: Address2, AddressLine3: Address3, PostCode: PostCode, Country: Country, County: County, City: City };
    return address;
}

function order_selectAddress(sender, addressPurpose) {
    var url = '/handlers/SelectAddress.ashx';
    var id = sender.selectedIndex;
    var addressType = sender[id].value;
    var address = workAddress;
    if (addressType == 0) address = homeAddress;
    $.fancybox.showActivity();

    var callback = function() { showAddress(addressPurpose, addressType, address); };
    refreshBasket(url, { addressId: address.Id, addressPurpose: addressPurpose, DisplayMode: basketDisplayMode }, callback);

    submitting = false;
}

function showAddresses(addressPurpose, addressType, address) {
    if ($('#Invoice').find('.currentAddressType').val() == addressType) {
        showAddress('Invoice', addressType, address);
    }
    if ($('#Delivery').find('.currentAddressType').val() == addressType) {
        showAddress('Delivery', addressType, address);
    }

    //reload current page
    location.href = ".";
}

function showAddress(addressPurpose, addressType, address) {

    if (address.Company == '') {
        $('#' + addressPurpose).find('.CompLabel').hide();
    } else {
        $('#' + addressPurpose).find('.CompLabel').show();
    }


    $('#' + addressPurpose).find('.currentAddressId').val(address.Id);
    $('#' + addressPurpose).find('.currentAddressType').val(addressType);

    $('#' + addressPurpose).find('.Company').html(address.Company);
    if (address.Company == '') $('#' + addressPurpose).find('.Company').html('&nbsp;');
    $('#' + addressPurpose).find('.Add1').html(address.AddressLine1);
    $('#' + addressPurpose).find('.Add2').html(address.AddressLine2);
    $('#' + addressPurpose).find('.Add3').html(address.AddressLine3);
    $('#' + addressPurpose).find('.City').html(address.City);
    $('#' + addressPurpose).find('.County').html(address.County);
    $('#' + addressPurpose).find('.PostCode').html(address.PostCode);
    $('#' + addressPurpose).find('.Country').html(address.Country);
}

function order_editAddress(addressPurpose) {
    //populate fields
    var addressType = $('#' + addressPurpose).find('.addressDD').val();
    var addressTypeText = $('#' + addressPurpose).find('.addressDD > option:selected').text();

    var address = homeAddress;
    $('#EditCompanyBlock').show();
    if (addressType == 1) {
        address = workAddress;
    } else {
        $('#EditCompanyBlock').hide(); //don't show company name
    }
    $('#editAddressPurpose').val(addressPurpose);
    $('#editAddressId').val(address.Id);
    $('#editAddressType').val(addressType);

    $('#ShowAddressType').html(addressTypeText);

    $('#editCompany').val(address.Company);

    $('#editAddress1').val(address.AddressLine1);
    $('#editCompany').val(address.Company);
    $('#editAddress2').val(address.AddressLine2);
    $('#editAddress3').val(address.AddressLine3);
    $('#editPostCode').val(address.PostCode);
    $('#editCounty').val(address.County);
    $('#editCity').val(address.City);

    $('#editCountry').val(address.Country);

    $.fancybox({ 'autoDimensions': true, 'scrolling': false, 'padding': 20, 'href': '#editAddress' });
}


function order_updateAddress() {
    var url = "/Handlers/EditAddress.ashx";

    var AddressPurpose = $('#editAddressPurpose').val();
    var AddressType = $('#editAddressType').val();

    var address = createAddressObj($('#editAddressId').val(), $('#editCompany').val(), $('#editAddress1').val(), $('#editAddress2').val(), $('#editAddress3').val(), $('#editPostCode').val(), $('#editCountry').val(), $('#editCounty').val(), $('#editCity').val());
    $.fancybox.showActivity();
    switch (AddressType) {
    case '0':
        homeAddress = address;
        break;
    case '1':
        workAddress = address;
        break;
    }


    var callback = function() { showAddresses(AddressPurpose, AddressType, address); };
    refreshBasket(url, { addressId: address.Id, DisplayMode: basketDisplayMode, company: address.Company, lineOne: address.AddressLine1, lineTwo: address.AddressLine2, lineThree: address.AddressLine3, postCode: address.PostCode, country: address.Country, city: address.City, county: address.County }, callback);
    submitting = false;
};

function order_remove(id) {
    var url = "/Handlers/BasketUpdateQuantity.ashx";
    var quantity = 0;
    $.fancybox.showActivity();

    refreshBasket(url, { item: id, DisplayMode: basketDisplayMode, quantity: quantity });
    submitting = false;

}

function order_updateQty(id) {
    var url = "/Handlers/BasketUpdateQuantity.ashx";
    var quantity = $('#Product-' + id).val();


    $.fancybox.showActivity();
    refreshBasket(url, { item: id, DisplayMode: basketDisplayMode, quantity: quantity }, null);
    submitting = false;
}

function refreshBasket(url, data, cb) {
    $.ajax({
            url: url,
            data: data
        })
        .done(function(data) {
            $('#order').html(data); //refresh basket
            if (cb != null) cb();
        })
        .fail(function(xhr, ajaxOptions, thrownError) {
            $.fancybox.hideActivity();
            $.fancybox('An error occured', { 'autoDimensions': true, 'scrolling': false, 'padding': 20 });
        })
        .always(function() {
            $.fancybox.hideActivity();
            $.fancybox.close();
            cb;
        });
}

function ValidateCreditCardNumber(sender, args) {
    args.IsValid = IsCreditCardNumberValid(args.Value, true);
}

function IsCreditCardNumberValid(cardNumber, allowSpaces) {
    if (allowSpaces) {
        cardNumber = cardNumber.replace(/ /g, '');
    }

    if (!cardNumber.match(/^\d+$/)) {
        return false;
    }

    var checksum = 0;

    for (var i = 0; i < cardNumber.length; i++) {
        var n = (cardNumber.charAt(cardNumber.length - i - 1) - '0') << (i & 1);

        checksum += n > 9 ? n - 9 : n;
    }

    return (checksum % 10) == 0 && checksum > 0;
}

$.extend({
    getUrlVars: function() {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    },
    getUrlVar: function(name) {
        return $.getUrlVars()[name];
    }
});

function PostWith(to, p) {
    var myForm = document.createElement("form");
    myForm.method = "post";
    myForm.action = to;
    for (var k in p) {
        var myInput = document.createElement("input");
        myInput.setAttribute("name", k);
        myInput.setAttribute("value", p[k]);
        myForm.appendChild(myInput);
    }
    document.body.appendChild(myForm);
    myForm.submit();
    document.body.removeChild(myForm);
}

function EnableAspNetValidatorsForElement(source) {
    for (i = 0; i < Page_Validators.length; i++) {
        if (Page_Validators[i].controltovalidate == $(source).attr('id'))
            ValidatorEnable(Page_Validators[i], true);
    }
}

function DisableAspNetValidatorsForElement(source) {
    for (i = 0; i < Page_Validators.length; i++) {
        if (Page_Validators[i].controltovalidate == $(source).attr('id'))
            ValidatorEnable(Page_Validators[i], false);
    }
}

//rich drop down form item
function radio(element) {
    var e = $(this);
    var vl = true;

    if (element.currentTarget == null) {
        e = element;
        vl = false;
    }

    var p = e.parent();
    var cb = p.find('input:radio');
    cb.attr('checked', 'checked');
    var div = p.find('div');

    if (!div.hasClass('on')) {
        $('.rdo div').removeClass('on');
        div.addClass('on');
    }

    if (vl) cb.valid();
    return false;
}

//rich drop down form item
function ddSel(element, parent, gfather) {
    var lb = $(this);
    var vl = true;

    if (element.currentTarget == null) {
        lb = element;
        vl = false;
    }

    var f = lb.attr == null ? lb.htmlFor : lb.attr('for');
    parent == null ? parent = lb.parent() : parent = parent;
    gfather == null ? gfather = lb.parent().parent() : gfather = gfather;

    var cb = parent.find('input#' + f);

    var newval = lb.innerHTML == null ? $('div:not(.st)', lb).text() : $('div:not(.st)', lb).innerHTML(); //lb.text() : lb.innerHTML;
    var bg = lb.attributes != null ? lb.attributes['css'] = 'background-image' : lb.css('background-image');

    gfather.find('span').text(newval);
    if (bg != null) gfather.find('span').css('background-image', bg);

    parent.hide();

    cb.attr('checked', 'checked');

    //TODO check to see if callbacks required... use parameter on div?
    var callback = gfather.attr('callback');


    if (callback != null) {
        var arr = callback.split('|');
        var fn = arr[0];
        var arg = arr[1];
        var val = cb.val();
        window[fn](arg, val);
    }
    if (vl) cb.valid();


    return false;
}

//another way to write "document.ready"
$(function() {

    // nav menu left menu, last item remove background
    $(".nav-menu > ul > li:last-child div").css({
        'border-top': 'none',
        'border-bottom': 'none',
        'background': 'none'
    });

    $(".nav-menu > ul > li").hover(function() {
        $(this).toggleClass("nav-menu-hover");
    });

    $(".nav-menu > ul > li.faa-nav-article").hover(function() {
        $(this).toggleClass("faa-nav-article-hover");
    });

    $(".nav-menu > ul > li.faa-nav-article").hover(function() {

            var previousMenuItem = $(this).prev();

            if (previousMenuItem.is("li.faa-nav-article")) {

                var innerDiv = $(previousMenuItem).children(":first");

                //remove background image from previous menu item
                innerDiv.addClass("no-background");
            }
        },
        function() {
            var previousMenuItem = $(this).prev();

            if (previousMenuItem.is("li.faa-nav-article")) {

                var innerDiv = $(previousMenuItem).children(":first");

                //remove background image from previous menu item
                innerDiv.removeClass("no-background");
            }
        }
    );

    //main nav menu hrz hover
    $(".nav-menu-hrz > ul > li").hover(
        function() { //appearing on hover
            var previousMenuItem = $(this).prev();

            //is li tag
            if (previousMenuItem.is("li")) {
                //remove background image from previous menu item
                previousMenuItem.addClass("no-background");
            }

            $('.nav-sub-menu', this).slideDown(400);
        },
        function() { //disappearing on hover

            //reset backgrounds. Remove 'no-background' css class from li tags except the last-child in list
            $(".nav-menu-hrz > ul").children('li:not(:last-child)').removeClass("no-background");
            $('.nav-sub-menu', this).slideUp(400);
        }
    );

    //sub menu hover
    $(".nav-sub-menu > ul").hover(
        function() { // Mouse Over
            $(this).closest("div").prev().addClass("nav-selected");
            $(this).closest("li").prev().addClass("no-background");
        },
        function() { // Mouse Out
            $(this).closest("div").prev().removeClass("nav-selected");
        }
    );
});

//soutron books
var soutronBooks = {
    isSechtionHidden: function() {

        return ($(".hideShow-section-container").css("display") == "none");
    },
    determineDisplayedLink: function() {

        // Depending on the display state of the advanced search section, set the text 
        // for the toggle link 
        var hideShowLink = $(".hideShow-section-line a");
        if (soutronBooks.isSechtionHidden()) {
            hideShowLink.html("Show full catalogue record").removeClass("hideShow-section-link-hide")
                .addClass("hideShow-section-link-show");
        } else {
            hideShowLink.html("Hide full catalogue record").removeClass("hideShow-section-link-show")
                .addClass("hideShow-section-link-hide");
        }
    },
    attachShowHideToLinkOnClick: function() {

        $(".hideShow-section-line a").click(function() {

            var durationMilliseconds = 450;
            $(".hideShow-section-container").toggle(durationMilliseconds, soutronBooks.determineDisplayedLink);
            return false;
        });
    }
};

//soutron books on ready
$(document).ready(function() {
    soutronBooks.determineDisplayedLink();
    soutronBooks.attachShowHideToLinkOnClick();
});