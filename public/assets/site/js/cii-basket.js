﻿var basketTimeout = 4000;
var basketTimeoutFunction = null;


function ShowBasket(throb) {
    $(".basketcontainer .errormessage").hide()
    var basketlink = $(".mybasket");
    var left = basketlink.offset().left;
    var top = basketlink.offset().top;

    $(".basketcontainer").show()
                    .css('top', $(document).scrollTop() == 0 ? top + 15 : $(document).scrollTop())
                    .css('left', left - 126);

    $(".basketcontainer .errormessage").hide();
    if (throb)
        $(".basketthrobber").show();
}


function ShowBasketText(basketCaptions, basketModel) {
    if (basketCaptions == null) {
        $("#basket-caption-panel").hide();
    } else {
        $("#basket-caption-panel").show();
        $("#basket-caption1").text(basketCaptions.caption1);
        $("#basket-text1").text(basketCaptions.text1);
        $("#basket-caption2").text(basketCaptions.caption2);
        $("#basket-text2").text(basketCaptions.text2);
    }
    if (basketModel.RecentItem == null) {
        $("#basket-product").hide();
    }
    else {
        $("#basket-product").show();
        $("#basket-category").text(basketModel.RecentCaption);

        if (basketModel.RecentItem.DisplayDetail == null || basketModel.RecentItem.DisplayDetail == "")
            $("#basket-product-description").text(basketModel.RecentItem.DisplayName);
        else
            $("#basket-product-description").text(basketModel.RecentItem.DisplayDetail);
        $("#basket-recent-cost").html(basketModel.RecentItemCost);
    }
    $("#basket-total-cost").html(basketModel.BasketCost);
}


function UpdateMyBasketText(ItemsCount) {
    if (ItemsCount == 0)
        $('#mybasket-caption').hide();
    else {
        $('#mybasket-caption').show();
        $('#mybasket-caption #basket-qty').text(ItemsCount);
    }
}


function AddToBasket(productType, productCode, quantity, basketCaptions, successFunction, additional) {
    var url = '/Handlers/BasketAdd.ashx';

        
    clearTimeout(basketTimeoutFunction);
    ShowBasket(true);
    var request = $.ajax(
        {
            cache: false,
            type: "post",
            url: url,
            data: { ProductType: productType, ProductCode: productCode, Quantity: quantity, Additional: additional },
            success: function (data) {
                
                if (data.BasketErrorMessage != null && data.BasketErrorMessage != "")
                    $(".basketcontainer .errormessage").show();
                else {

                    UpdateMyBasketText(data.ItemsCount);
                    ShowBasketText(basketCaptions, data);
                    if (successFunction != null)
                        successFunction(data);
                }
                $(".basketthrobber").hide();
                basketTimeoutFunction = setTimeout(function () {
                    $(".basketcontainer").stop(true, true).hide('fast');
                }, basketTimeout);
                $(window).scroll(function () {
                    $(".basketcontainer").stop(true, true).hide('fast');
                });



                //$(".basketcontainer").delay(basketTimeout).hide('fast');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $(".basketthrobber").hide();
                $(".basketcontainer").hide();
                alert(thrownError);
            },
            complete: function () {
            }
        });
}

function RemoveFromBasket(productCode, successFunction) {
    $(".basketthrobber").show();
    var request = $.ajax(
        {
            cache: false,
            type: "post",
            url: "/Handlers/BasketRemove.ashx",
            data: { ProductCode: productCode },
            success: function (data) {
                if (data.BasketErrorMessage != null && data.BasketErrorMessage != "")
                    $(".basketcontainer .errormessage").show()
                else {
                    ShowBasketText(null, data);
                    if (successFunction != null)
                        successFunction(data);
                }
                $(".basketthrobber").hide();
                UpdateMyBasketText(data.ItemsCount);

            },
            error: function (xhr, ajaxOptions, thrownError) {
                $(".basketthrobber").hide();
                $(".basketcontainer").hide();
                alert(thrownError);
            },
            complete: function () {
            }
        });
}
