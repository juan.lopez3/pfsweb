﻿    //=========map and setting markers

    // The following example creates complex markers to indicate events near
    // Sydney, NSW, Australia. Note that the anchor is set to
    // (0,32) to correspond to the base of the flagpole.
    
    var infowindow = null;
    
    function initialize() {
        
        var mypoint = new google.maps.LatLng(53.775, -1.50862);

        var mapOptions = {
            zoom : 6,
            center : mypoint 
        }
        
        var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
        
        infowindow = new google.maps.InfoWindow({
            content : "holding..",
            maxWidth: 250
        });
        
        setMarkers(map, events);
    }

    /**
     * Data for the markers consisting of a name, a LatLng and a zIndex for
     * the order in which these markers should display on top of each
     * other.
     * 
     * Calculate distance, if it's in range put to events array
     * var dist = distance(53.775, -1.50862, event[1], event[2]);
            
     */
    var events = [
        ['Bondi event', 52.875, -1.074856],
        ['Coogee event', 53.923036, -1.259052],
        ['Maroubra event', 53.950198, -1.959302]
    ];

    function setMarkers(map, locations) {
        // Marker sizes are expressed as a Size of X,Y
        // where the origin of the image (0,0) is located
        // in the top left of the image.
        // Origins, anchor positions and coordinates of the marker
        // increase in the X direction to the right and in
        // the Y direction down.
        var image = {
            url: '{{\Config::get('app.url')}}images/marker.png',
            // This marker is 20 pixels wide by 32 pixels tall.
            size: new google.maps.Size(20, 32),
            // The origin for this image is 0,0.
            origin: new google.maps.Point(0,0),
            // The anchor for this image is the base of the flagpole at 0,32.
            anchor:
            new google.maps.Point(0, 32)
        };
        // Shapes define the clickable region of the icon.
        // The type defines an HTML &lt;area&gt; element 'poly' which
        // traces out a polygon as a series of X,Y points. The final
        // coordinate closes the poly by connecting to the first
        // coordinate.
        var shape = {
            coords : [1, 1, 1, 20, 18, 20, 18, 1],
            type : 'poly'
        };
    
        for (var i = 0; i < locations.length; i++) {
            var event = locations[i];
            var myLatLng = new google.maps.LatLng(event[1], event[2]);
            var marker = new google.maps.Marker({
                position : myLatLng,
                map : map,
                icon : image,
                shape : shape,
                title : event[0],
                html : "test"+i
            });
    
            google.maps.event.addListener(marker, 'click', function() {
                
                infowindow.setContent(this.html);
                infowindow.open(map, this);
                find_closest_marker(53.775, -1.50862);
            });
        }
    }
    
    google.maps.event.addDomListener(window, 'load', initialize);


/**
 * Finding markers in radius 
 */
function distance(lat1, lng1, lat2, lng2) {
    var radlat1 = Math.PI * lat1 / 180;
    var radlat2 = Math.PI * lat2 / 180;
    var radlon1 = Math.PI * lng1 / 180;
    var radlon2 = Math.PI * lng2 / 180;
    var theta = lng1 - lng2;
    var radtheta = Math.PI * theta / 180;
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist);
    dist = dist * 180 / Math.PI;
    dist = dist * 60 * 1.1515;

    //Get in in kilometers
    dist = dist * 1.609344;

    return dist;
}