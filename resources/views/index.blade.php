@extends('layouts.default')

@section('title')Events list @endsection

@section('content')


<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyCFo1QWcA_GRpH7Q_jV14HF2PJ_3qzuiB8"></script>
<script src="{{asset('assets/site/js/libs/googlemaps/markerclusterer.js')}}"></script>
<div class="banner-one">
	<!-- <img class="banner" src="{{asset('assets/site/img/banerpfs.jpg')}}" alt="PFS" title="Personal Finance Society" /> -->
	<div class="wrapper">
	    <p class="banner-titulo">Out and about</p>
	    <p class="banner-texto">Events</p>
	</div>
</div>
<div class="wrapper">
    @include('partials.top')
</div>

@include('partials.validationErrors')
<!-- <div class="row">
	<div class="col-xs-12">
		<div class="homebox">
			<h1 class="event-title">Events and Conferences</h1>
			<div class="clearfix"></div>
		</div>
	</div>
</div> -->
<?php
	$distance = [
		'' => 'Select distance',
		'1' => '1 mile',
		'2' => '2 miles',
		'5' => '5 miles',
		'10' => '10 miles',
		'15' => '15 miles',
		'20' => '20 miles',
		'30' => '30 miles',
		'40' => '40 miles',
		'50' => '50 miles',
		'100' => '100 miles',
		'400' => '100+ miles',
	];
?>
<div class="row body-form">
	<div class="col-12">
		{!! Form::open(['route' => ['events'], 'class' => 'form', 'id'=>'filter', 'method'=>'get']) !!}
		<div class=" sidebar wrapper">
			<p class="search">{!! Form::label('address', 'Search events:') !!} <span><a class="btn boton btn-events" href="{{route('profile.myEvents')}}">My events </a></span> </p>
		</div>
        
		<div class="formulario">
		    <div class="wrapper">
				<div id="selector">
					<div class="col-md-6 col-12 form-label select ">
                	{!! Form::text('address', null, ['placeholder' => 'Enter a Postcode, Town or Region']) !!}
                    </div>
                    <div class="col-12 col-md-6 form-label  select-g">
                    	{!! Form::select('range', $distance, null, ['id'=>'ddlFrom']) !!}
		    	    </div>
		    	    
		    	    <div class="col-12 col-md-6 d-md-flex" style="margin-bottom: 0px; padding:0px;">
                        <div class="col-md-6 form-label select">
                        	<!-- {!! Form::label('from', 'From:') !!} -->
                        	{!! Form::select('from', ['placeholder'=>'From']+$filter_date_from, null, ['id'=>'ddlFrom', 'value'=> null]) !!}
                        </div>
		    	        <div class="col-md-6 nopad form-label select">
		    	        	<!-- {!! Form::label('to', 'To:') !!} -->
		    	        	{!! Form::select('to', ['placeholder'=>'To']+$filter_date_to, null, ['id'=>'ddlFrom']) !!}
		    	        </div>
		    	    </div>
        
		    	    <div class="clearfix col-12 col-md-6  form-label keywords select">
		    	        <!-- {!! Form::label('keywords', 'Keywords') !!} -->
		    	        {!! Form::text('keywords', null, ['placeholder' => 'Keywords', 'id'=>'keywords']) !!}
		    	    </div>
				</div>
		    	<div class="col-md-12 col-xs-6 d-inline-flex botones">
		    		{!! Form::submit('Search', ['class' => 'btn boton btn-form']) !!}
					<a href="{{route('events')}}"><input class="btn boton btn-form" id="btnClear" name="btnClear" type="button" value="Clear" /></a>
		    	    <span id="boton" class="btn boton btn-form" >View map</span>
		    	</div>
		    	
		    </div>
		    {!! Form::close() !!}	
		</div>
		
	</div>
</div>
<div class="wrapper">
    <div class="row body-listado">
		<div class="listado">
       @if(!$events->isEmpty())
    		@foreach($events as $i => $e)
    		<div class="col-12 col-sm-4 list-box">
    		    <a href="@if(!empty($e->outside_link)){{$e->outside_link}} @else{{route('events.view', $e->slug)}}@endif" title="{{$e->title}}" class="ancla-lista">
                    <div class="">
    	            	@if ($e->event_date_from == $e->event_date_to)
    	            		<p class="fecha arriba"><span class=""> {{ date('l',strtotime($e->event_date_from)) }} {{ date('d F Y',strtotime($e->event_date_from)) }}</span></p>
							<br>
							<p></p>
    	            	@else
    	            	    <p class="fecha">{{ date('d',strtotime($e->event_date_from)) }}-{{ date('d F Y',strtotime($e->event_date_to)) }} </p>
    	            	@endif
    	            	
                        <p class="nombre" >{{$e->title}}</p>
    		    		<p class="espera" >@if($e->checkAvailability()) Waiting List @endif</p>
						<!-- <p class="espera" >Waiting List</p> -->
						<br>
    	            	<p class="locacion"></p>
						<p class="locacion">@if(sizeof($e->venue)){{$e->venue->city}} @endif</p>
    		        </div>
    		    </a>
    		</div>
			@endforeach
			<!-- <p class="locacion abajo">Menber fee: @if($e->price > 0) £{{number_format($e->price, 2)}} @else £0 @endif</p> -->
			</div>
        <div class="row paginador-principal">
	        <div class="centered col-md-12">
				 @include("pagination.default",["paginator" => $events]) 
    	    </div>
            @else
    		    <h1><i>No events found by selected criteria.</i></h1>
    	    @endif
	   </div>


    
    </div>
</div>


	<!-- <div class="col-12">
		@if(!$events->isEmpty())
			<div class="row results home-row-header heading hidden-sm hidden-xs">
				<div class="col-md-3 date"><b>Date</b></div>
				<div class="col-md-5 title"><b>Title</b></div>
				<div class="col-md-3 location"><b>Location</b></div>
				<div class="col-md-1 fee"><b>Member Fee</b></div>
			</div>
			<div class="row">
			    <div class="col-12 col-sm-6 col-lg-4"></div>
			</div>
			@foreach($events as $i => $e)
			<div class="row results item full @if ($i % 2 == 0)odd @else even @endif">
		    	<div class="col-xs-12 col-sm-2 col-md-3 date">
					@if ($e->event_date_from == $e->event_date_to)
						{{ date('l',strtotime($e->event_date_from)) }}<br/>
						{{ date('d F Y',strtotime($e->event_date_from)) }}
					@else
						{{ date('d',strtotime($e->event_date_from)) }}-{{ date('d F Y',strtotime($e->event_date_to)) }}
					@endif
				</div>
		    	<div class="col-xs-12 col-sm-4 col-md-5 title"><a href="@if(!empty($e->outside_link)){{$e->outside_link}} @else{{route('events.view', $e->slug)}}@endif" title="{{$e->title}}">{{$e->title}}</a>
		    	<br>@if($e->checkAvailability() && $e->id != 680)<span class="wait-list">Waiting List</span>@endif</div>
		    	<div class="col-xs-12 col-sm-2 col-md-3 location">@if(sizeof($e->venue)){{$e->venue->city}} @endif</div>
		    	<div class="col-md-1 col-sm-1 hidden-sm hidden-xs fee">@if($e->price > 0) £{{number_format($e->price, 2)}} @else £0 @endif </div>
	    	</div>
			@endforeach
			<div class="centered">
				<?php
				//  echo $events->render();
				
				?>
			</div>
	</div
		@else
			<h1><i>No events found by selected criteria.</i></h1>
		@endif
	</div> -->

	<!-- mapa -->
<div class="wrapper">
    <div id="map" class="body-mapa">
	    <div class="clearfix"></div>
	    <div id="map-canvas" class="side-map"></div>
	    <button class="btn boton boton-mapa" type="button" id="expand-map"><span class="e-m-t">View larger map</span><span style="display: none;" class="e-m-t">View smaller map</span></button>
	</div>

	<!-- mapa grande, que sale al dar clic en el boton -->
	<div class="row" id="top-map">
	    <div class="col-xs-12">
	    	<div id="wide-map" class="wide-map"></div>
	    </div>
    </div>
</div>


</div>

<script type="text/javascript">
   $("#boton").click(function(){
	    $("html, body").animate({
            scrollTop: $("#map").offset().top}, 2000);
   })

	$( "#expand-map" ).click(function() {
		$( "#top-map" ).toggle( "slow", function() {
			$("#map-canvas").toggle();
			initialize();
			$(".e-m-t").toggle();
		});
	});

	//=========map and setting markers
	// The following example creates complex markers to indicate events near
	// Sydney, NSW, Australia. Note that the anchor is set to
	// (0,32) to correspond to the base of the flagpole.

	var infowindow = null;

	function initialize() {
		
		//starting map focus location
		var mypoint = new google.maps.LatLng({{$center_point['lat']}}, {{$center_point['long']}});

		var mapOptions = {
			zoom : {{$zoom_level}},
			center : mypoint,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		}
		
		var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
		var wide_map = new google.maps.Map(document.getElementById('wide-map'), mapOptions);
		
		infowindow = new google.maps.InfoWindow({
			content : "holding..",
			maxWidth: 250
		});
		
		// get filtered events
		/**
		 * Data for the markers consisting of a name, a LatLng and a zIndex for
		 * the order in which these markers should display on top of each
		 * other.
		 */
		
	    var events =JSON.parse({!!$events_json!!});
	    setMarkers(map, events);
	    setMarkers(wide_map, events);
	}

	function setMarkers(map, locations) {
		// Marker sizes are expressed as a Size of X,Y
		// where the origin of the image (0,0) is located
		// in the top left of the image.
		// Origins, anchor positions and coordinates of the marker
		// increase in the X direction to the right and in
		// the Y direction down.
		var image = {
			url: '{{\Config::get('app.url')}}images/marker.png',
			size: new google.maps.Size(20, 20),
			origin: new google.maps.Point(0,0),
			anchor:
			new google.maps.Point(10, 20)
		};
		// Shapes define the clickable region of the icon.
		// The type defines an HTML &lt;area&gt; element 'poly' which
		// traces out a polygon as a series of X,Y points. The final
		// coordinate closes the poly by connecting to the first
		// coordinate.
		var shape = {
			coords : [1, 1, 1, 20, 18, 20, 18, 1],
			type : 'poly'
		};
		
		var markers = [];
		var existingMarker = [];
		
		for (var i = 0; i < locations.length; i++) {
			var event = locations[i];
			
			var myLatLng = new google.maps.LatLng(event.lat, event.lng);
			
			//check if marker exists for overlay
			var overlay = false;
			for (var z = 0; z < existingMarker.length; z++) {
				if(event.lat == existingMarker[z].lat && event.lng == existingMarker[z].lng) overlay = true;
			}
			
			if(overlay) {
				var newLat = parseFloat(event.lat) + Math.random() / 60000;
				var newLng = parseFloat(event.lng) + Math.random() / 60000;
				var myLatLng = new google.maps.LatLng(newLat, newLng);
			}
			
			existingMarker.push(locations[i]);
			
			var marker = new google.maps.Marker({
				position : myLatLng,
				map : map,
				icon : image,
				shape : shape,
				title : event.title,
				description : event.description
			});
			
			google.maps.event.addListener(marker, 'click', function() {
				
				infowindow.setContent(this.description);
				infowindow.open(map, this);
			});
			markers.push(marker);
		}
		
		mcOptions = {styles: [{
			height: 25,
			width: 25,
			textColor: 'white',
			url: "{{\Config::get('app.url')}}images/marker2.png",
			}],
			gridSize: 2, maxZoom: 15
		};
		var markerCluster = new MarkerClusterer(map, markers, mcOptions);
	}

	google.maps.event.addDomListener(window, 'load', initialize);
</script>

@endsection
