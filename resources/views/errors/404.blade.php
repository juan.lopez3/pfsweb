@extends('layouts.default')
@section('content')
<link type="text/css" rel="stylesheet" href="{{asset('assets/site/css/theme-default/materialadmin.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/site/css/theme-default/font-awesome.min.css')}}" />
<div id="base">
	<!-- BEGIN CONTENT-->
	<div id="content">
		<!-- BEGIN 404 MESSAGE -->
		<section>
			<div class="section-body contain-lg">
				<div class="row">
					<div class="col-lg-12 text-center">
						<h1><span class="text-xxxl text-light">404 <i class="fa fa-search-minus text-primary"></i></span></h1>
						<h2 class="text-light">This page does not exist</h2>
					</div><!--end .col -->
				</div><!--end .row -->
			</div><!--end .section-body -->
		</section>
		<!-- END 404 MESSAGE -->

		<!-- BEGIN SEARCH SECTION -->
		<section>
			<div class="section-body contain-sm">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-search"></i></span>
					<input type="text" class="form-control" placeholder="You're searching for...">
					<span class="input-group-btn"><button class="btn boton" type="submit">Find</button></span>
				</div>
			</div><!--end .section-body -->
		</section>
		<!-- END SEARCH SECTION -->
	</div><!--end #content-->
	<!-- END CONTENT -->
</div>
@endsection