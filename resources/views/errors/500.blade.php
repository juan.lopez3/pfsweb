<!DOCTYPE html>
<html lang="en">
<head>
	<title>PFS EVENTS Unauthorized!</title>

	<!-- BEGIN META -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- END META -->
	
	<!-- BEGIN STYLESHEETS -->
	<link type="text/css" rel="stylesheet" href="{{asset('assets/site/css/bootstrap.css')}}" />
	<link type="text/css" rel="stylesheet" href="{{asset('assets/site/css/estilos.css')}}" />
	<link type="text/css" rel="stylesheet" href="{{asset('assets/site/css/theme-default/materialadmin.css')}}" />
	<link type="text/css" rel="stylesheet" href="{{asset('assets/site/css/theme-default/font-awesome.min.css')}}" />
	<!-- END STYLESHEETS -->
</head>
	
<body>
	<!-- BEGIN BASE-->
	<div id="base">
		<!-- BEGIN CONTENT-->
		<div id="content">
			<!-- BEGIN 500 MESSAGE -->
			<section>
				<div class="section-body contain-lg">
					<div class="row">
						<div class="col-lg-12 text-center">
							<h1><span class="text-xxxl text-light">500 <i class="fa fa-exclamation-circle text-danger"></i></span></h1>
							<h2 class="text-light">Unauthorized!</h2>
						</div><!--end .col -->
					</div><!--end .row -->
				</div><!--end .section-body -->
			</section>
			<!-- END 500 MESSAGE -->
		</div><!--end #content-->
		<!-- END CONTENT -->
	</div><!--end #base-->
	<!-- END BASE -->
</body>
</html>