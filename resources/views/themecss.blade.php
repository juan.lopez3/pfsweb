<style>

.gradient h3 {
    background: {{$event->theme->title_background_color}};
    color: {{$event->theme->title_text_color}}
}

.sidebar-inner a:link {
    color: {{$event->theme->link_color}};
}

a:visited {
    color: {{$event->theme->link_visited_color}};
}

a:hover {
    color: {{$event->theme->link_hover_color}};
}

a:active {
    color: {{$event->theme->link_active_color}};
}


body {
    color: {{$event->theme->text_color}};
    @if (!empty($event->theme->background_image))
    background: url({{asset('uploads/branding/'.$event->theme->background_image)}})
    @endif
}


.twitter-hash {
    color: {{$event->theme->title_text_color}}
}

.purplebox a:link{
    background: {{$event->theme->title_background_color}};
}

.btn {
    background: {{$event->theme->title_background_color}};
}


.venue-more-details-btn a:link{
    background: {{$event->theme->title_background_color}};
}

.accordion a {
    background: {{$event->theme->title_background_color}};
}
.accordion .panel-body .panel-title a {
    background: {{$event->theme->link_color}};
}

.accordion .panel-title a:hover {
    background: {{$event->theme->link_hover_color}};
}

.purplebox a:hover {
    background: {{$event->theme->book_now_hover_color}};
}

a.btn {
    background: {{$event->theme->title_background_color}};
}

.btn:hover {
    background: {{$event->theme->book_now_hover_color}};
}

.a:hover {
    background: {{$event->theme->book_now_hover_color}};
}

.accordion a.collapsed {
    background: {{$event->theme->title_background_color}};
    
}

.accordion a.collapsed:hover {
    background: {{$event->theme->link_hover_color}}
}

.nav-pills>li>a {
    background: {{$event->theme->title_background_color}};
    color: #FFF !important;
}

.nav-pills>li.active>a,
.nav-pills>li.active>a:focus,
.nav-pills>li.active>a:hover,
.nav-pills>li>a:hover {
    background-color: {{$event->theme->link_hover_color}}
}

</style>