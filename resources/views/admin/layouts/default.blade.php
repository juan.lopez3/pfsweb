<!DOCTYPE html>
<html lang="en">
	<head>
		<title>PFS EVENTS @section('title') @show</title>

		<!-- BEGIN META -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<meta name="keywords" content="@section('meta_keywords')DEFAULT KEYWORDS @show"/>
		<meta name="description" content="@section('meta_description')DEFAULT DESCRIPTION @show"/>
	    
		<!-- END META -->
		<script src="{{asset('assets/admin/js/libs/jquery/jquery-1.11.2.min.js')}}"></script>
		<script src="{{asset('assets/admin/js/libs/jquery-ui/jquery-ui.min.js')}}"></script>
		<script src="{{asset('assets/admin/js/libs/bootstrap/bootstrap.min.js')}}"></script>
		<script type="text/javascript" src="{{asset('assets/admin/js/libs/DataTables/jquery.dataTables.js')}}"></script>
		<script type="text/javascript" src="{{asset('assets/admin/js/libs/DataTables/dataTables.bootstrap.js')}}"></script>
		<script src="{{asset('assets/admin/js/core/demo/DemoFormEditors.js')}}?md={{\Config::get('cache.buster')}}"></script>
		<script src="{{asset('assets/admin/js/core/demo/DemoFormComponents.js')}}?md={{\Config::get('cache.buster')}}"></script>
		<script src="{{asset('assets/admin/js/core/demo/Demo.js')}}?md={{\Config::get('cache.buster')}}"></script>
        
        <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js" ></script>
        <script src="//cdn.datatables.net/plug-ins/1.10.11/sorting/datetime-moment.js"></script>

		<!-- BEGIN STYLESHEETS -->
		<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/bootstrap.css')}}" />
		<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/bootstrap-colorpicker/bootstrap-colorpicker.css')}}" />
		<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/materialadmin.css')}}?md={{\Config::get('cache.buster')}}" />
		<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/font-awesome.min.css')}}?md={{\Config::get('cache.buster')}}" />
		<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/material-design-iconic-font.min.css')}}?md={{\Config::get('cache.buster')}}" />
		<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/DataTables/dataTables.bootstrap.css')}}?md={{\Config::get('cache.buster')}}" />
		<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/app.css')}}?md={{\Config::get('cache.buster')}}" />
		<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/bootstrap-tagsinput/bootstrap-tagsinput.css')}}?md={{\Config::get('cache.buster')}}" />
		<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/select2/select2.css')}}?md={{\Config::get('cache.buster')}}" />
		<!-- END STYLESHEETS -->
		
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script type="text/javascript" src="{{asset('assets/admin/js/libs/utils/html5shiv.js?1403934957')}}"></script>
		<script type="text/javascript" src="{{asset('assets/admin/js/libs/utils/respond.min.js?1403934956')}}"></script>
		<![endif]-->
	</head>
	<body class="menubar-hoverable header-fixed menubar-pin ">

		<!-- BEGIN HEADER-->
		<header id="header" >
			@include('admin.partials.header')
		</header>
		<!-- END HEADER-->

		<!-- BEGIN BASE-->
		<div id="base">

			<!-- BEGIN OFFCANVAS LEFT -->
			<div class="offcanvas">
			</div>
			<!-- END OFFCANVAS LEFT -->

			<!-- BEGIN CONTENT-->
			<div id="content">
				@yield('content')
			</div>
			<!-- END CONTENT -->

			<!-- BEGIN MENUBAR-->
			<div id="menubar" class="menubar-inverse ">
				@include('admin.partials.navigation')
			</div>
			<!-- END MENUBAR -->

			<!-- BEGIN OFFCANVAS RIGHT -->
			<div class="offcanvas"></div>
			<!-- END OFFCANVAS RIGHT -->

		</div>
		<!-- END BASE -->
		<!-- BEGIN JAVASCRIPT -->
		<script src="{{asset('assets/admin/js/libs/ckeditor/ckeditor.js')}}?md={{\Config::get('cache.buster')}}"></script>
		<script src="{{asset('assets/admin/js/libs/ckeditor/adapters/jquery.js')}}"></script>
		<script src="{{asset('assets/admin/js/libs/ckeditor/adapters/jquery.js')}}"></script>
		<script src="{{asset('assets/admin/js/libs/jquery/jquery-migrate-1.2.1.min.js')}}"></script>
		<script src="{{asset('assets/admin/js/libs/spin.js/spin.min.js')}}?md={{\Config::get('cache.buster')}}"></script>
		<script src="{{asset('assets/admin/js/libs/autosize/jquery.autosize.min.js')}}?md={{\Config::get('cache.buster')}}"></script>
		<script src="{{asset('assets/admin/js/libs/nanoscroller/jquery.nanoscroller.min.js')}}?md={{\Config::get('cache.buster')}}"></script>
		<script src="{{asset('assets/admin/js/libs/jquery-validation/dist/jquery.validate.min.js')}}?md={{\Config::get('cache.buster')}}"></script>
		<script src="{{asset('assets/admin/js/libs/jquery-validation/dist/additional-methods.min.js')}}?md={{\Config::get('cache.buster')}}"></script>
		<script src="{{asset('assets/admin/js/core/source/App.js')}}?md={{\Config::get('cache.buster')}}"></script>
		<script src="{{asset('assets/admin/js/core/source/AppNavigation.js')}}?md={{\Config::get('cache.buster')}}"></script>
		<script src="{{asset('assets/admin/js/core/source/AppOffcanvas.js')}}?md={{\Config::get('cache.buster')}}"></script>
		<script src="{{asset('assets/admin/js/core/source/AppCard.js')}}?md={{\Config::get('cache.buster')}}"></script>
		<script src="{{asset('assets/admin/js/core/source/AppForm.js')}}?md={{\Config::get('cache.buster')}}"></script>
		<script src="{{asset('assets/admin/js/core/source/AppNavSearch.js')}}?md={{\Config::get('cache.buster')}}"></script>
		<script src="{{asset('assets/admin/js/core/source/AppVendor.js')}}?md={{\Config::get('cache.buster')}}"></script>
		<script src="{{asset('assets/admin/js/libs/inputmask/jquery.inputmask.bundle.min.js')}}"></script>
		<script type="text/javascript" src="{{asset('assets/admin/js/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js')}}"></script>
		<script type="text/javascript" src="{{asset('assets/admin/js/core/demo/DemoFormComponents.js')}}"></script>
		<script src="{{asset('assets/admin/js/libs/select2/select2.min.js')}}"></script>
		<script src="{{asset('assets/admin/js/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js')}}"></script>
		
		<!-- Drag&drop report table libs. May cause conflicts-->
		<!--
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.min.js"></script>
		-->
		<!-- END JAVASCRIPT -->

	</body>
</html>
