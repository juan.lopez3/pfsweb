@extends('admin.layouts.default')

@section('title')
Edit Venue
@endsection

@section('content')
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/js/libs/jquery-fileupload/uploadfile.css')}}" />
<script type="text/javascript" src="{{asset('assets/admin/js/libs/jquery-fileupload/jquery.uploadfile.min.js')}}"></script>
<section>
<div class="section-body">
	<div class="col-md-12">
		<div class="card">
			<div class="card-head style-primary">
				<header>Edit Venue</header>
			</div>
			
			@include('admin.partials.validationErrors')
			
			<div class="card-body">
				{!! Form::model($venue, ['route' => ['venues.update', $venue->id], 'method' => 'PATCH', 'id'=>'venue_edit_form', 'class' => 'form-horizontal form-validate', 'files' => true]) !!}
				 @include('admin.venues.form')
				 
				 <div class="card-actionbar">
					<div class="card-actionbar-row">
						<a href="{{ URL::previous() }}"><button type="button" class="btn btn-flat">Cancel</button></a>
						{!! Form::submit('UPDATE VENUE', ['class' => 'btn btn-flat btn-primary']) !!}
					</div>
				</div>
				
				{!! Form::close() !!}
				
				@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator'), role('regional_coordinator')]))
				<hr class="ruler-xxl">
				<h3 class="opacity-75">Venue notes</h3>
				<div class="row">
					<p><button type="button" class="btn ink-reaction btn-raised btn-sm btn-success" data-toggle="modal" data-target="#add_note">Add Venue Note</button></p>
					<div class="modal fade" id="add_note" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="section-body">
								<div class="col-md-12">
									<div class="card">
										<div class="card-head style-primary">
											<header>Add Venue Note</header>
										</div>
										
										{!! Form::open(['route' => ['venues.addNote', $venue->id], 'class' => 'form form-validate']) !!}
										
										<div class="card-body">
										    <div class="form-group">
										        {!! Form::label('priority', 'Priority', ['class' => 'control-label']) !!}
										        {!! Form::select('priority', config('const.priorities'), null, ['class'=>'form-control']) !!}
										    </div>
										    <div class="form-group">
                                                {!! Form::label('tags', 'Tags', ['class' => 'control-label']) !!}
                                                    {!! Form::text('tags', null, ['class'=>'form-control', 'data-role'=>'tagsinput']) !!}
                                                    <p class="help-block">Push enter to separate tags</p>
                                            </div>
										    <div class="form-group">
											     {!! Form::textarea('note', null, ['class'=>'form-control', 'rows' => 3, 'placeholder' => 'Please enter venue note']) !!}
											 </div>
										</div>
										<div class="card-actionbar">
											<div class="card-actionbar-row">
												<br/><br/>
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												{!! Form::submit('Add note', ['class' => 'btn btn-flat btn-primary']) !!}
											</div>
										</div>
										{!! Form::close() !!}
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<script>
                    $( function() {
                        $( "#sortable-high" ).sortable({
                            'containment': 'parent',
                            'handle': '.handle',
                            update: function(event, ui) {
                                $.post('{{ route('venues.notes.reorder') }}',
                                    $("#list-high").serialize(), function(data) {
                                        
                                        if(!data.success) {
                                            alert('Whoops, something went wrong :/');
                                        }
                                    }, 'json');
                            }
                        });
                        
                        $( "#sortable-medium" ).sortable({
                            'containment': 'parent',
                            'handle': '.handle',
                            update: function(event, ui) {
                                $.post('{{ route('venues.notes.reorder') }}',
                                    $("#list-medium").serialize(), function(data) {
                                        
                                        if(!data.success) {
                                            alert('Whoops, something went wrong :/');
                                        }
                                    }, 'json');
                            }
                        });
                        
                        $( "#sortable-low" ).sortable({
                            'containment': 'parent',
                            'handle': '.handle',
                            update: function(event, ui) {
                                $.post('{{ route('venues.notes.reorder') }}',
                                    $("#list-low").serialize(), function(data) {
                                        
                                        if(!data.success) {
                                            alert('Whoops, something went wrong :/');
                                        }
                                    }, 'json');
                            }
                        });
                    });
                    </script>
                
					<div class="col-md-12">
					    <form id="list-high">
						<table class="table table-striped venue-notes">
						    <thead>
						        <tr><th>High Priority Notes</th></tr>
						    </thead>
							<tbody id="sortable-high">
								@foreach($venue->notes()->where('priority', 3)->orderBy('order')->get() as $note)
									@include('admin.venues.venue_note_line')
								@endforeach
								</div>
							</tbody>
						</table>
						</form>
						
                        
                        <!-- Modal -->
                        @foreach($venue->notes()->where('priority', 3)->orderBy('order')->get() as $note)
                        <div id="comment-{{$note->id}}" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                {!! Form::open(['route'=> ['venues.notes.addComment', $note->id], 'method'=>'post']) !!}
                                <div class="modal-content">
                                    <div class="card-head style-primary">
                                        <header>Add Note Comment</header>
                                    </div>
                                  
                                  <div class="card-body">
                                    {!! Form::textarea('comment', null, ['class'=>'form-control', 'rows'=>2, 'required', 'placeholder'=>'Add your commend']) !!}
                                  </div>
                                  
                                    <div class="card-actionbar">
                                        <div class="card-actionbar-row">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            {!! Form::submit('Save Comment', ['class' => 'btn btn-flat btn-primary']) !!}
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
						@endforeach
						
						<form id="list-medium">
                        <table class="table table-striped venue-notes">
                            <thead>
                                <tr><th>Medium Priority Notes</th></tr>
                            </thead>
                            <tbody id="sortable-medium">
                                @foreach($venue->notes()->where('priority', 2)->orderBy('order')->get() as $note)
                                    @include('admin.venues.venue_note_line')
                                @endforeach
                                </div>
                            </tbody>
                        </table>
                        </form>
                        
                        <!-- Modal -->
                        @foreach($venue->notes()->where('priority', 2)->orderBy('order')->get() as $note)
                        <div id="comment-{{$note->id}}" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                {!! Form::open(['route'=> ['venues.notes.addComment', $note->id], 'method'=>'post']) !!}
                                <div class="modal-content">
                                    <div class="card-head style-primary">
                                        <header>Add Note Comment</header>
                                    </div>
                                  
                                  <div class="card-body">
                                    {!! Form::textarea('comment', null, ['class'=>'form-control', 'rows'=>2, 'required', 'placeholder'=>'Add your commend']) !!}
                                  </div>
                                  
                                    <div class="card-actionbar">
                                        <div class="card-actionbar-row">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            {!! Form::submit('Save Comment', ['class' => 'btn btn-flat btn-primary']) !!}
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        @endforeach
                        
                        
                        <form id="list-low">
                        <table class="table table-striped venue-notes">
                            <thead>
                                <tr><th>Low Priority Notes</th></tr>
                            </thead>
                            <tbody id="sortable-low">
                                @foreach($venue->notes()->where('priority', 1)->orderBy('order')->get() as $note)
                                    @include('admin.venues.venue_note_line')
                                @endforeach
                                </div>
                            </tbody>
                        </table>
                        </form>
						
						<!-- Modal -->
                        @foreach($venue->notes()->where('priority', 1)->orderBy('order')->get() as $note)
                        <div id="comment-{{$note->id}}" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                {!! Form::open(['route'=> ['venues.notes.addComment', $note->id], 'method'=>'post']) !!}
                                <div class="modal-content">
                                    <div class="card-head style-primary">
                                        <header>Add Note Comment</header>
                                    </div>
                                  
                                  <div class="card-body">
                                    {!! Form::textarea('comment', null, ['class'=>'form-control', 'rows'=>2, 'required', 'placeholder'=>'Add your commend']) !!}
                                  </div>
                                  
                                    <div class="card-actionbar">
                                        <div class="card-actionbar-row">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            {!! Form::submit('Save Comment', ['class' => 'btn btn-flat btn-primary']) !!}
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        @endforeach
						
						<script type="text/javascript">
                            $(document).ready(function() {
                                $('.venue-notes').dataTable({
                                    "ordering" : false,
                                    "pageLength" : 50,
                                    "info" : false,
                                    "paging" : false
                                });
                            } );
                        </script>
					</div>
				</div>
				@endif
                
                <hr class="ruler-xxl">
                <h3 class="opacity-75">Venue files</h3>
                <div class="row">
                    <div class="form-group">
                        <div id="venue_files_upload">Upload</div>
                        <div id="status"></div>
                    </div>
                </div>
                
                <div class="row">
                    
                    <table id="documents_list" class="table table-striped table-responsive" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Document Name</th>
                                <th>Display Name</th>
                                <th>Type</th>
                                <th>Size(Mb)</th>
                                <th>Date Uploaded</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                    
                        <tbody>
                            @foreach ($venue->files as $document)
                            <tr>
                                <td>{{ $document->document_name }}</td>
                                <td>{{ $document->display_name }}</td>
                                <td>{{ $document->type}}</td>
                                <td>{{ $document->size / 1000000 }}</td>
                                <td>{{ date("d m Y H:i:s",strtotime($document->created_at)) }}</td>
                                <td>
                                    @if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
                                        <a href="{{config('app.url')}}uploads/venue_files/{{$document->document_name}}" target="_new"><button type="button" class="btn btn-info btn-xs" title="Download"><span class="glyphicon glyphicon-download"></span></button></a>
                                        <button data-href="{{ route('venues.removeFile', $document->id) }}" type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-trash"></span></button>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                    
                </div>
                
			</div>
			
			
		</div>
	</div>
</div>
</section>


<script type="text/javascript">

    $(".note_status").change(function(){
        $.ajax({
            url: "{!! route('venues.notes.changeStatus') !!}",
            type: "POST",
            data: {
                 "_token": "{{ csrf_token() }}",
                   "venue_id": this.id, 
                   "priority": this.value,
            },
            success: function () {
                location.reload();
            },
            error: function() {
                alert('Error updating note status.');
            }
        });
    });

    $(document).ready(function() {
        $('#documents_list').dataTable({
            
        });
    } );

    $(document).ready(function()
    {
    var settings = {
        url: "{!! route('venues.uploadFile') !!}",
        formData: {  
           "_token": "{{ csrf_token() }}",
           "venue_id": "{{ $venue->id }}" 
        },
        dragDrop:true,
        maxFileSize: 20000000,
        fileName: "myfile",
        allowedTypes:"jpg,png,gif,bmp,jpeg,pdf,doc,docx,xls,xlsx,docx,ppt,pptx",    
        returnType:"json",
         onSuccess:function(files,data,xhr)
        {
           // alert((data));
           location.reload();
        },
        showDelete:false
    }
    var uploadObj = $("#venue_files_upload").uploadFile(settings);
    
    });
</script>

@include('admin.partials.readonly', ['form_id' => 'venue_edit_form'])

@include('admin.partials.deleteConfirmation')

@endsection
