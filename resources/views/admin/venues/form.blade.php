<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			{!! Form::label('name', 'Venue Name', ['class' => 'col-sm-3 col-md-2 control-label']) !!}
			<div class="col-sm-9 col-md-10">
				{!! Form::text('name', null, ['class'=>'form-control', 'required']) !!}
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group">
			{!! Form::label('email', 'Venue Email', ['class' => 'col-sm-3 col-md-2 control-label']) !!}
			<div class="col-sm-9 col-md-10">
				{!! Form::text('email', null, ['class'=>'form-control']) !!}
			</div>
		</div>
	</div>	
</div>

<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			{!! Form::label('address', 'Venue Address', ['class' => 'col-sm-3 col-md-2 control-label']) !!}
			<div class="col-sm-9 col-md-10">
				{!! Form::text('address', null, ['class'=>'form-control', 'required']) !!}
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group">
			{!! Form::label('website', 'Venue Website', ['class' => 'col-sm-3 col-md-2 control-label']) !!}
			<div class="col-sm-9 col-md-10">
				{!! Form::text('website', null, ['class'=>'form-control']) !!}
				<p class="help-block">Starts with http://</p>
			</div>
		</div>
	</div>	
</div>

<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			{!! Form::label('city', 'City / Town', ['class' => 'col-sm-3 col-md-2 control-label']) !!}
			<div class="col-sm-9 col-md-10">
				{!! Form::text('city', null, ['class'=>'form-control', 'required']) !!}
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group">
			{!! Form::label('venue_phone', 'Venue Phone', ['class' => 'col-sm-3 col-md-2 control-label']) !!}
			<div class="col-sm-9 col-md-10">
				{!! Form::text('venue_phone', null, ['class'=>'form-control']) !!}
				<p class="help-block">Digits only</p>
			</div>
		</div>
	</div>	
</div>

<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			{!! Form::label('county', 'County', ['class' => 'col-sm-3 col-md-2 control-label']) !!}
			<div class="col-sm-9 col-md-10">
				{!! Form::text('county', null, ['class'=>'form-control', 'required']) !!}
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group">
			{!! Form::label('venue_staff_id', 'Contact Person', ['class' => 'col-sm-3 col-md-2 control-label']) !!}
			<div class="col-sm-9 col-md-10">
				{!! Form::select('venue_staff_id', [''=>''] + $tfi_staff, null, ['class'=>'form-control select2-list', 'required']) !!}
			</div>
		</div>
	</div>	
</div>

<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			{!! Form::label('postcode', 'Postcode', ['class' => 'col-sm-3 col-md-2 control-label']) !!}
			<div class="col-sm-9 col-md-10">
				{!! Form::text('postcode', null, ['class'=>'form-control', 'required']) !!}
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-3">
		<div class="form-group">
			{!! Form::label('bedrooms', 'Bedrooms', ['class' => 'col-sm-4 col-md-4 control-label']) !!}
			<div class="col-sm-8 col-md-8">
				{!! Form::input('number', 'bedrooms', null, ['class'=>'form-control', 'min' => '0']) !!}
			</div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
			{!! Form::label('rating', 'Rating', ['class' => 'col-sm-4 col-md-4 control-label']) !!}
			<div class="col-sm-8 col-md-8">
				{!! Form::select('rating', [''=>'', 'Bad'=>'Bad', 'OK'=>'OK', 'Good' => 'Good'], null, ['class'=>'form-control']) !!}
			</div>
		</div>
	</div>
	
	<div class="col-sm-6">
		<div class="form-group">
			{!! Form::label('show_map', 'Show map', ['class' => 'col-sm-3 col-md-2 control-label']) !!}
			<div class="col-sm-9 col-md-10">
				<div class="checkbox checkbox-styled">
					<label>
						{!! Form::checkbox('show_map', true) !!}
					</label>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-3">
		<div class="form-group">
			{!! Form::label('min_guaranteed', 'Min guaranteed numbers', ['class' => 'col-sm-7 col-md-7 control-label']) !!}
			<div class="col-sm-5 col-md-5">
				{!! Form::input('number', 'min_guaranteed', null, ['class'=>'form-control']) !!}
			</div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group text-right">
			{!! Form::label('Commission', 'Commission(%)', ['class' => 'col-sm-6 col-md-6 control-label']) !!}
			<div class="col-sm-6 col-md-6">
				{!! Form::input('number', 'commission', null, ['class'=>'form-control']) !!}
			</div>
		</div>
	</div>
	
	<div class="col-sm-3">
		<div class="form-group">
			{!! Form::label('latitude', 'Latitude', ['class' => 'col-sm-4 col-md-4 control-label']) !!}
			<div class="col-sm-8 col-md-8">
				{!! Form::text('latitude', null, ['class'=>'form-control']) !!}
			</div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
			{!! Form::label('longitude', 'Longitude', ['class' => 'col-sm-4 col-md-4 control-label']) !!}
			<div class="col-sm-8 col-md-8">
				{!! Form::text('longitude', null, ['class'=>'form-control']) !!}
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		{!! Form::label('details', 'Venue details', ['class' => 'control-label']) !!}
		{!! Form::textarea('details', null, ['row'=>'3', 'style'=>'height: 200px;', 'id'=>'details']) !!}
	</div>
</div>

<header>
	<h3 class="opacity-75">Meeting Rooms</h3>
</header>


<div class="row">
	<p>{!! Form::button('Add Meeting Room', ['class' => 'btn ink-reaction btn-raised btn-sm btn-success', 'id'=>'add']) !!}</p>
	<br/>
	<div class="row">
		<div class="col-xs-3">Venue Room Name</div>
		<div class="col-xs-2">Capacity</div>
		<div class="col-xs-2">Cabaret Capacity</div>
		<div class="col-xs-2">Theatre Capacity</div>
		<div class="col-xs-2">Max No of tables</div>
	</div>
	<div id="items">
		@if(isset($venue))
		
		@foreach($venue->rooms as $room)
		
			<div class="row">
				<div class="col-xs-3"><input type="hidden" name="room_id[]" value="{{$room->id}}" /><input type="text" value="{{$room->room_name}}" name="room_name[]" class="form-control" required /></div>
				<div class="col-xs-2"><input type="number" value="{{$room->capacity}}" name="capacity[]" class="form-control" /> </div>
				<div class="col-xs-2"><input type="number" value="{{$room->cabaret_capacity}}" name="cabaret_capacity[]" class="form-control" /> </div>
				<div class="col-xs-2"><input type="number" value="{{$room->theatre_capacity}}" name="theatre_capacity[]" class="form-control" /> </div>
				<div class="col-xs-2"><input type="number" value="{{$room->max_tables}}" name="max_tables[]" class="form-control" /> </div>
				<div class="col-xs-1"><button type="button" class="delete_room_check btn btn-danger btn-icon-toggle" id="{{$room->id}}" title="Delete"><i class="md md-delete"></i></button></div>
			</div>
		@endforeach
		@endif
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$('#details').ckeditor();
});
//when the Add Field button is clicked
$("#add").click(function () {
	
	var html = '<div class="row">'+
					'<div class="col-xs-3"><input type="text" value="" name="room_name[]" class="form-control" required /></div>'+
					'<div class="col-xs-2"><input type="number" value="" name="capacity[]" class="form-control" /> </div>'+
					'<div class="col-xs-2"><input type="number" value="7" name="cabaret_capacity[]" class="form-control" /> </div>'+
					'<div class="col-xs-2"><input type="number" value="" name="theatre_capacity[]" class="form-control" /> </div>'+
					'<div class="col-xs-2"><input type="number" value="" name="max_tables[]" class="form-control" /> </div>'+
					'<div class="col-xs-1"><button type="button" class="delete btn btn-danger btn-icon-toggle" title="Delete"><i class="md md-delete"></i></button></div>'+
				'</div>';
	
	$("#items").append(html);
});

// for previously saved rooms perform check if any sessions are assigned to room
$("body").on("click", ".delete_room_check", function (e) {
	var localThis = this;
	
	$.ajax({
		url: "{!! route('events.venue.chechRoom') !!}",
		data: {venue_room_id: localThis.id},
		success: function(response){
			if(response) {
				alert("At least one session is assigned to this meeting room. Deleting is not allowed.")
			} else {
				$(localThis).parent("div").parent("div").remove();
			}
		},
		error: function(){
			alert('Error checking if meeting room is assigned to sessions');
		}
	});
});

$("body").on("click", ".delete", function (e) {
	$(this).parent("div").parent("div").remove();
});
</script>