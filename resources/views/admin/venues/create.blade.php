@extends('admin.layouts.default')

@section('title')
Create Venue
@endsection

@section('content')

<section>
<div class="section-body">
	<div class="col-sm-12">
		<div class="card">
			<div class="card-head style-primary">
				<header>Add Venue</header>
			</div>
			
			@include('admin.partials.validationErrors')
			
			{!! Form::open(['route' => ['venues.store'], 'class' => 'form-horizontal form-validate']) !!}
			<div class="card-body">
				 @include('admin.venues.form')
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					<a href="{{ URL::previous() }}"><button type="button" class="btn btn-default">Cancel</button></a>
					{!! Form::submit('CREATE Venue', ['class' => 'btn btn-flat btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
</section>
@endsection
