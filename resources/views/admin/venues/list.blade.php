@extends('admin.layouts.default')

@section('title')
	Venues
@endsection

@section('content')

<script type="text/javascript">
	$(document).ready(function() {
	    $.fn.dataTable.moment( 'D/M/YYYY HH:mm:ss' );
		$('#venues_list').dataTable({
			"order": [[ 4, "desc" ]],
			"iDisplayLength": 50,
		});
	} );
</script>

<section>
<div class="section-header">
	<ol class="breadcrumb">
		<li class="active">Venues List</li>
	</ol>
</div>
@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
<p><a href="{{ route('venues.create') }}"><button type="button" class="btn btn-success">Add New</button></a></p>
@endif
<div class="section-body">	
	<div class="card">
		<div class="card-body">
			<table id="venues_list" class="table table-striped table-responsive" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Name</th>
						<th>City</th>
						<th>County</th>
						<th>Postcode</th>
						<th>Updated</th>
						<th>Actions</th>
					</tr>
				</thead>
			
				<tfoot>
					<tr>
						<th>Name</th>
						<th>City</th>
						<th>County</th>
						<th>Postcode</th>
						<th>Updated</th>
						<th>Actions</th>
					</tr>
				</tfoot>
			
				<tbody>
					@foreach ($venues as $venue)
					<tr>
						<td>{{ $venue->name }}</td>
						<td>{{ $venue->city}}</td>
						<td>{{ $venue->county}}</td>
						<td>{{ $venue->postcode}}</td>
						<td>{{ date("d/m/Y H:i:s",strtotime($venue->updated_at)) }}</td>
						<td>
							<a href="{{ route('venues.edit', $venue->id) }}"><button type="button" class="btn btn-warning btn-xs" title="Edit/View"><span class="glyphicon glyphicon-pencil"></span></button></a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
</section>

@include('admin.partials.deleteConfirmation')

@endsection
