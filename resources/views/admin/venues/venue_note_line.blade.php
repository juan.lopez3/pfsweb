<tr style="background-color: {{config('const.priority_colour.'.$note->priority)}}">
    <td>
        <div class="handle"><input type="hidden" name="item[]" value="{{$note->id}}" /></div>
        {!! Form::select('note_status', config('const.priorities'), $note->priority, ['class'=>'form-select note_status', 'id'=>$note->id]) !!}
        @if (!empty($note->tags))
        @foreach (explode(",", $note->tags) as $tag)
        <button class="btn btn-xs btn-success btn-rounded">{{$tag}}</button>
        @endforeach 
        @endif
        <b>{{ date("d/m/Y H:i:s",strtotime($note->created_at)) }} {{$note->user->first_name}} {{$note->user->last_name}}</b> - {{$note->note}}
        
        @if (!empty($note->comment))
        <br/><b>Comments: {{$note->comment}}</b>
        @else
        <br/>
            <a data-toggle="modal" data-toggle="modal" data-target="#comment-{{$note->id}}">Add Comment</a>
        @endif
    </td>
</tr>