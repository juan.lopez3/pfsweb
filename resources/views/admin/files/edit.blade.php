@extends('admin.layouts.default')

@section('title')
Edit File
@endsection

@section('content')

<section>
<div class="section-body">
	<div class="col-lg-offset-1 col-md-12 col-lg-10">
		<div class="card">
			<div class="card-head style-primary">
				<header>Edit File</header>
			</div>
			
			@include('admin.partials.validationErrors')
			
			{!! Form::model($file, ['route' => ['files.update', $file->id], 'method' => 'PATCH', 'class' => 'form form-validate']) !!}
			<div class="card-body">
				 @include('admin.files.form')
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					<a href="{{ URL::previous() }}"><button type="button" class="btn btn-flat">Cancel</button></a>
					{!! Form::submit('UPDATE FILE', ['class' => 'btn btn-flat btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
</section>
@endsection
