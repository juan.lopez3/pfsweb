<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/js/libs/jquery-fileupload/uploadfile.css')}}" />
<script type="text/javascript" src="{{asset('assets/admin/js/libs/jquery-fileupload/jquery.uploadfile.min.js')}}"></script>


<div class="row">
	<div class="col-md-4">
		<div class="form-group">
			<div id="event_materials_upload">Upload</div>
			<div id="status"></div>
		</div>
	</div>
</div>

<div class="col-sm-6"> 
	<div class="form-group floating-label">
		{!! Form::text('document_name', null, ['class'=>'form-control', 'readonly']) !!}
		{!! Form::label('document_name', 'Document Name') !!}
	</div>
</div>


<div class="col-sm-6">
	<div class="form-group floating-label">
		{!! Form::text('display_name', null, ['class'=>'form-control', 'required']) !!}
		{!! Form::label('display_name', 'Display Name') !!}
	</div>
</div>

<div class="row">
	<div class="col-sm-4">
		<div class="form-group">
			{!! Form::text('type', null, ['class'=>'form-control']) !!}
			{!! Form::label('type', 'Type') !!}
		</div>
	</div>
	
	<div class="col-sm-2">
		<div class="form-group">
			{!! Form::text('size', null, ['class'=>'form-control']) !!}
			{!! Form::label('size', 'Size') !!}
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function()
	{
	var settings = {
	    url: "{!! route('files.updateFile', $file->id) !!}",
	    formData: {  
		   "_token": "{{ csrf_token() }}",
		},
	    dragDrop:true,
	    maxFileSize: 10000000,
	    fileName: "myfile",
	    allowedTypes:"jpg,png,gif,bmp,jpeg,pdf,doc,docx,xls,xlsx,docx,ppt,pptx",	
	    returnType:"json",
		 onSuccess:function(files,data,xhr)
	    {
	       // alert((data));
	       location.reload();
	    },
	    showDelete:false
	}
	var uploadObj = $("#event_materials_upload").uploadFile(settings);
	
	});
</script>