@extends('admin.layouts.default')

@section('title')
	Files
@endsection

@section('content')
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/js/libs/jquery-fileupload/uploadfile.css')}}" />
<script type="text/javascript" src="{{asset('assets/admin/js/libs/jquery-fileupload/jquery.uploadfile.min.js')}}"></script>

<script type="text/javascript">
	$(document).ready(function() {
	    $.fn.dataTable.moment( 'D/M/YYYY HH:mm:ss' );
		$('#files').dataTable({
			"iDisplayLength": 50,
		});
	} );
</script>

<section>
<div class="section-header">
	<ol class="breadcrumb">
		<li class="active">Files</li>
	</ol>
</div>
<div class="section-body">	
	@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<div id="event_materials_upload">Upload</div>
					<div id="status"></div>
				</div>
			</div>
		</div>
	@endif
	<div class="card">
		<div class="card-body">
			
			@include('admin.partials.validationErrors')
			
			<table id="files" class="table table-striped table-responsive" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Document Name</th>
						<th>Display Name</th>
						<th>Type</th>
						<th>Size(Mb)</th>
						<th>Date Uploaded</th>
						<th>Actions</th>
					</tr>
				</thead>
			
				<tbody>
					@foreach ($files as $f)
					<tr>
						<td>{{ $f->document_name }}</td>
						<td>{{ $f->display_name }}</td>
						<td>{{ $f->type}}</td>
						<td>{{ $f->size / 1000000 }}</td>
						<td>{{ date("d m Y H:i:s",strtotime($f->created_at)) }}</td>
						<td>
							@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
							<a href="{{ route('files.edit', $f->id) }}"><button type="button" class="btn btn-warning btn-xs" title="Edit"><span class="glyphicon glyphicon-pencil"></span></button></a>
							<a href="{{config('app.url')}}uploads/files/{{$f->document_name}}" target="_new"><button type="button" class="btn btn-info btn-xs" title="Download"><span class="glyphicon glyphicon-download"></span></button></a>
							<button data-href="{{ route('files.delete', $f->id) }}" type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-trash"></span></button>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
</section>

@include('admin.partials.deleteConfirmation')

<script type="text/javascript">
	$(document).ready(function()
	{
	var settings = {
	    url: "{!! route('files.store') !!}",
	    formData: {  
		   "_token": "{{ csrf_token() }}",
		},
	    dragDrop:true,
	    maxFileSize: 16000000,
	    fileName: "myfile",
	    allowedTypes:"jpg,png,gif,bmp,jpeg,pdf,doc,docx,xls,xlsx,docx,ppt,pptx",	
	    returnType:"json",
		 onSuccess:function(files,data,xhr)
	    {
	       // alert((data));
	       location.reload();
	    },
	    showDelete:false
	}
	var uploadObj = $("#event_materials_upload").uploadFile(settings);
	
	});
</script>

@endsection
