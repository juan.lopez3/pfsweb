@extends('admin.layouts.default')

@section('page-title')Edit Support Ticket @endsection

@section('content')

    <div class="section-body">
        <div class="col-md-12">
            <div class="card">
                {!! Form::model($issue, ['route' => ['issues.update', $issue->id], 'method' => 'PUT', 'class' => 'form form-validate', 'files'=>true]) !!}
                <div class="card-head style-primary">
                    <header>View ticket</header>
                </div>

                <div class="card-body">

                    @include('admin.partials.validationErrors')
                    @include('admin.issues.form')

                    <div class="row pt-md">
                        <div class="col-md-12 text-right">
                            <a href="{{ URL::previous() }}"><button type="button" class="btn btn-default">Cancel</button></a>
                            {!! Form::submit('UPDATE ISSUE', ['class' => 'btn btn-flat btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <hr />

                    <h3>Actions history</h3>

                    <table class="table table-striped table-responsive" cellspacing="0" width="100%">
                        <tr>
                            <td>Date</td>
                            <td>Author</td>
                            <td>Change</td>
                        </tr>
                        @foreach($issue->changes()->with('user')->orderBy('created_at', 'desc')->get() as $change)
                        <tr>
                            <td>{{date("d/m/Y H:i", strtotime($change->created_at))}}</td>
                            <td>{{$change->user->first_name}} {{$change->user->last_name}}</td>
                            <td>{!!$change->change!!}</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
