<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/bootstrap-datepicker/datepicker3.css?1424887858')}}" />
<script type="text/javascript" src="{{asset('assets/admin/js/libs/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>

<div class="row">
    <div class="form-group floating-label">
        {!! Form::label('title', 'Title', ['class'=>'control-label required']) !!}
        {!! Form::text('title', null, ['class'=>'form-control', 'required']) !!}
    </div>

    <div class="form-group floating-label">
        {!! Form::label('assigned_to', 'Raised By', ['class'=>'control-label required']) !!}
        @if (isset($issue))
            {!! Form::select('assigned_to', $users, null, ['class'=>'form-control']) !!}
        @else
            {!! Form::select('assigned_to', $users, auth()->user()->id, ['class'=>'form-control']) !!}
        @endif
    </div>

    <div class="form-group">
        {!! Form::label('due', 'Due date', ['class'=>'control-label required']) !!}
        <div class="col-md-3">
            <div class="input-group date" id="amendment-date">
                <div class="input-group-content">
                    @if(isset($issue))
                        {!! Form::text('due', date("d/m/Y",strtotime($issue->due)), ['class' => 'form-control', 'required']) !!}
                    @else
                        {!! Form::text('due', null, ['class'=>'form-control', 'required']) !!}
                    @endif
                </div>
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-xs-12 col-sm-4">
        <div class="form-group">
            {!! Form::label('priority', 'Priority') !!}
            {!! Form::select('priority', config('issues.priority'), null, ['class'=>'form-control']) !!}
        </div>
    </div>
    @if(isset($issue))
    <div class="col-xs-12 col-sm-4">
        <div class="form-group">
            {!! Form::label('status', 'Status') !!}
            {!! Form::select('status', config('issues.options'), null, ['class'=>'form-control']) !!}
        </div>
    </div>
    @endif
    <div class="col-xs-12 col-sm-4">
        <div class="form-group">
            {!! Form::label('type', 'Issue type') !!}
            {!! Form::select('type', config('issues.type'), null, ['class'=>'form-control']) !!}
        </div>
    </div>
</div>

@if(isset($issue))
<div class="row">
    <div class="col-xs-12 col-sm-4">
        <div class="form-group">
            {!! Form::label('quote_min', 'Quote min(h)') !!}
            {!! Form::text('quote_min', null, ['class'=>'form-control']) !!}
        </div>
    </div>

    <div class="col-xs-12 col-sm-4">
        <div class="form-group">
            {!! Form::label('quote_max', 'Quote max(h)') !!}
            {!! Form::text('quote_max', null, ['class'=>'form-control']) !!}
        </div>
    </div>

    <div class="col-xs-12 col-sm-4">
        <div class="form-group">
            {!! Form::label('quote_final', 'Quote final(h)') !!}
            {!! Form::text('quote_final', null, ['class'=>'form-control']) !!}
        </div>
    </div>
</div>
@endif
<br/>

<div class="form-group">
    {!! Form::label('description', 'Description', ['class'=>'control-label required']) !!}
    {!! Form::textarea('description', null, ['class'=>'form-control', 'rows'=>3]) !!}
</div>

<div class="form-group">
    {!! Form::label('attachments', 'Attachments', ['class'=>'control-label']) !!}
    @if (isset($issue))
        @foreach ($issue->attachments as $file)
            <a href="{{asset('ticket_attachments/'.$file->saved_name)}}">{{$file->name}}</a>
        @endforeach
    @endif
    <input type="file" name="file" class="dropify" data-max-file-size="2M" /> <br/>
</div>



@if(isset($issue))
<div class="form-group floating-label">
    {!! Form::label('message', 'Message') !!}
    {!! Form::textarea('message', null, ['class'=>'form-control', 'rows' => 3]) !!}
</div>
@endif