@extends('admin.layouts.default')
@section('page-title')Support Ticket tracking @endsection

@section('content')
<section>
	<div class="section-header">
		<ol class="breadcrumb">
			<li class="active" ​style="color:#FF0000">Support</li>
		</ol>
	</div>
	<div class="section-body">
		<div class="card">
			<div class="card-body">

				<div class="row">
​
<p style="color:#FF0000">You have 0 hours on your account as of 12th Feb 2018.</p> 
<p  style="color:#FF0000">The team will do their best to answer tickets as soon as possible. Please be aware that this could take up to 3 working days for a response as all developers are dealing with other tickets. </p>
					
<div class="col-md-6">
						<a href="{{ route('issues.create') }}"><button class="btn ink-reaction btn-raised btn-success" type="button">New Ticket</button></a>
					</div>
					<div class="col-md-6 text-right">
						<a href="{{ route('issues') }}?show-closed=1"><button class="btn ink-reaction btn-raised btn-info" type="button">Show Closed</button></a>
					</div>
				</div>
				<br/>

				@include('admin.partials.validationErrors')

				<table id="list" class="table table-hover">
					<thead>
						<tr>
							<th>#ID</th>
							<th>Created</th>
							<th>Priority</th>
							<th>Due Date</th>
							<th>Title</th>
							<th>Raised By</th>
							<th>Status</th>
							<th>Type</th>
							<th>Actions</th>
						</tr>
					</thead>

					<tbody>
						@foreach ($issues as $issue)
						<tr>
							<td>{{$issue->id}}</td>
							<td>{{date("d/m/Y H:i", strtotime($issue->created_at))}}</td>
							<td>{{config('issues.priority.'.$issue->priority)}}</td>
							<td>{{date("d/m/Y", strtotime($issue->due))}}</td>
							<td>{{$issue->title}}</td>
							<td>{{$issue->assignedTo->first_name}} {{$issue->assignedTo->last_name}}</td>
							<td>{{config('issues.options.'.$issue->status)}}</td>
							<td>{{config('issues.type.'.$issue->type)}}</td>
							<td>
								<a href="{{ route('issues.edit', $issue->id) }}"><button type="button" class="btn btn-warning btn-xs" title="Edit"><span class="glyphicon glyphicon-pencil"></span></button></a>
								@if(!$issue->confirmed && $issue->status == config('issues.need-confirmation')) <a href="{{ route('issues.confirm', $issue->id) }}"><button type="button" class="btn btn-success btn-xs" title="Confirm to be done as maintenance"><span class="glyphicon glyphicon-ok"></span></button></a> @endif
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('#list').dataTable({
            "pageLength": 50,
            "order": [[ 3, 'asc' ]]
		});
	} );
</script>
</section>
@endsection