@extends('admin.layouts.default')
@section('page-title')Create New Ticket @endsection

@section('content')
<div class="section-body">
	<div class="col-md-12">
		<div class="card">
			{!! Form::open(['route' => ['issues.store'], 'class' => 'form form-validate', 'files'=>true]) !!}
			<div class="card-head style-primary">
				<header>Add ticket</header>
			</div>

			<div class="card-body">
				@include('admin.partials.validationErrors')
				@include('admin.issues.form')
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					<a href="{{ URL::previous() }}"><button type="button" class="btn btn-default">Cancel</button></a>
					{!! Form::submit('SAVE', ['class' => 'btn btn-flat btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection
