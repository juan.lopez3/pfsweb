@extends('admin.layouts.default')

@section('title')
	Sponsors
@endsection

@section('content')

<script type="text/javascript">
	$(document).ready(function() {
	    $.fn.dataTable.moment( 'D/M/YYYY HH:mm:ss' );
		$('#sponsors').dataTable({
			"iDisplayLength": 50,
		});
	} );
</script>

<section>
<div class="section-header">
	<ol class="breadcrumb">
		<li class="active">Sponsors</li>
	</ol>
</div>
<div class="section-body">	
	@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
	<p><a href="{{ route('sponsors.create') }}"><button class="btn ink-reaction btn-raised btn-success" type="button">ADD SPONSOR</button></a></p>
	@endif
	<div class="card">
		<div class="card-body">
			
			@include('admin.partials.validationErrors')
			
			<table id="sponsors" class="table table-striped table-responsive" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Sponsor Name</th>
						<th>Website URL</th>
						<th>Profile</th>
						<th>Logo</th>
						<th>Created</th>
						<th>Modified</th>
						<th>Actions</th>
					</tr>
				</thead>
			
				<tfoot>
					<tr>
						<th>Sponsor Name</th>
						<th>Website URL</th>
						<th>Profile</th>
						<th>Logo</th>
						<th>Created</th>
						<th>Modified</th>
						<th>Actions</th>
					</tr>
				</tfoot>
			
				<tbody>
					@foreach ($sponsors as $sponsor)
					<tr>
						<td>{{ $sponsor->name }}</td>
						<td>{{ $sponsor->website}}</td>
						<td>{{ $sponsor->info}}</td>
						<td> @if(!empty($sponsor->logo)){!! HTML::image('uploads/sponsors/'.$sponsor->logo,'', ['class'=>'img-responsive height-1']) !!} @else &nbsp; @endif</td>
						<td>{{ date("d/m/Y H:i:s",strtotime($sponsor->created_at)) }}</td>
						<td>{{ date("d/m/Y H:i:s",strtotime($sponsor->updated_at)) }}</td>
						<td>
							@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
							<a href="{{ route('sponsors.edit', $sponsor->id) }}"><button type="button" class="btn btn-warning btn-xs" title="Edit"><span class="glyphicon glyphicon-pencil"></span></button></a>
							<button data-href="{{ route('sponsors.delete', $sponsor->id) }}" type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-trash"></span></button>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
</section>

@include('admin.partials.deleteConfirmation')

@endsection
