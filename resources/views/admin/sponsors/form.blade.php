<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/js/libs/jquery-fileupload/uploadfile.css')}}" />
<script type="text/javascript" src="{{asset('assets/admin/js/libs/jquery-fileupload/jquery.uploadfile.min.js')}}"></script>

<div class="form-group floating-label">
	{!! Form::text('name', null, ['class'=>'form-control', 'required']) !!}
	{!! Form::label('name', 'Sponsor name') !!}
</div>
<div class="form-group floating-label">
	{!! Form::text('website', null, ['class'=>'form-control']) !!}
	{!! Form::label('website', 'Website URL') !!}
	<p class="help-block">With http://</p>
</div>
<div class="form-group floating-label">
	{!! Form::textarea('info', null, ['class'=>'form-control', 'maxlength'=>'1000', 'rows'=>2, 'onkeyup' => 'countChar(this)']) !!}
	{!! Form::label('info', 'Profile') !!}
	<div id="charNum"></div>
</div>

<div class="form-group floating-label">
	{!! Form::label('thank_you', 'Thank you message') !!}
	<br/>
	{!! Form::textarea('thank_you', null, ['row'=>'3', 'style'=>'height: 200px;', 'id'=>'ckeditor']) !!}
</div>

<div class="form-group floating-label col-xs-12 col-sm-6">
	{!! Form::label('logo', 'Logo') !!}
	{!! Form::hidden('logo', null, ['id'=>'logo']) !!}
	<br/>
	<span id="logo_img">
	@if(isset($sponsor) && !empty($sponsor->logo))
		{!! HTML::image('uploads/sponsors/'.$sponsor->logo,'', ['class'=>'img-responsive']) !!}
	@endif
	</span>
	<div id="logo_upload">Upload</div>
	<div id="status"></div>
</div>

<div class="form-group floating-label col-xs-12 col-sm-6">
    {!! Form::label('app_logo', 'Sponsor advert') !!}
    {!! Form::hidden('app_logo', null, ['id'=>'app_logo']) !!}
    <br/>
    <span id="app_logo_img">
	@if(isset($sponsor) && empty($sponsor->app_logo))
		<div id="app_logo_upload">Upload</div>
		<div id="status"></div>
	@endif
	@if(isset($sponsor) && !empty($sponsor->app_logo))
        {!! HTML::image('uploads/sponsors/'.$sponsor->app_logo,'', ['class'=>'img-responsive']) !!}
	<button type="button" class="btn btn-danger btn-xs" id="delete_logo_app" title="Delete">Remove</button>
	@endif
    </span>
</div>

<script type="text/javascript">
function countChar(val) {
	
	var len = val.value.length;
	if (len >= 1000) {
		val.value = val.value.substring(0, 1000);
	} else {
		$('#charNum').text(1000 - len);
	}
};

$(document).ready(function()
{
	var settings = {
	    url: "{!! route('sponsors.uploadLogo') !!}",
	    formData: {  
		   "_token": "{{ csrf_token() }}"
		},
	    dragDrop:true,
	    maxFileSize: 600000,
	    fileName: "myfile",
	    allowedTypes:"jpg,png,gif,bmp,jpeg",	
	    returnType:"json",
		 onSuccess:function(files,data,xhr)
	    {
	       //alert((data));
	       $('#logo').val(data);
	       $('#logo_img').html('<img src="{{\Config::get("app.url")}}uploads/sponsors/'+data+'" class="img-responsive" />');
	       
	       //location.reload();
	    },
	    showDelete:false
	}
	var uploadObj = $("#logo_upload").uploadFile(settings);
	
	var app_settings = {
        url: "{!! route('sponsors.uploadLogo') !!}",
        formData: {  
           "_token": "{{ csrf_token() }}"
        },
        dragDrop:true,
        maxFileSize: 600000,
        fileName: "myfile",
        allowedTypes:"jpg,png,gif,bmp,jpeg",    
        returnType:"json",
         onSuccess:function(files,data,xhr)
        {
           //alert((data));
           $('#app_logo').val(data);
           $('#app_logo_img').html('<img src="{{\Config::get("app.url")}}uploads/sponsors/'+data+'" class="img-responsive" />');
           
           //location.reload();
        },
        showDelete:false
    }
    var uploadObj = $("#app_logo_upload").uploadFile(app_settings);
	@if(isset($sponsor) && !empty($sponsor->id))
	 $('#delete_logo_app').bind('click', function () {
        $.ajax({
            url: "{{ route('sponsors.deleteLogo', $sponsor->id) }}",
            success: function () {
                location.reload();
            }
        });
    });
	@endif;
});
</script>