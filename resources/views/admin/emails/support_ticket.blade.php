<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <style>

    </style>
</head>
<body>
<h4>Support Ticket: {{$issue->title}}</h4>
<b>Priority: </b>{{config('issues.priority.'.$issue->priority)}} <br/>
<b>Due Date: </b>{{date("d/m/Y", strtotime($issue->due))}} <br/>
<b>Type: </b>{{config('issues.type.'.$issue->type)}} <br/>
{!! $issue->description !!} <br/>

<hr />
<h4>Last changes: </h4>
{!! $issue_change->change !!}

</body>
</html>