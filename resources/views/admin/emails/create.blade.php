@extends('admin.layouts.default')

@section('title')
{{$event->title}} Emails
@endsection

@section('content')

<section>
<div class="section-header">
	<div class="col-xs-9"><span class="event-head">{{$event->title}}</span> - {{ date("j F Y",strtotime($event->event_date_from)) }}<br/>
		<span class="glyphicon glyphicon-envelope text-primary"></span><span class="event-head"> Emails</span></div>
	<div class="col-xs-3 text-right">@include('admin.partials.eventActions')</div>
</div>
<br/>
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/summernote/summernote.css')}}" />
<script src="{{asset('assets/admin/js/libs/summernote/summernote.min.js')}}"></script>

<div class="section-body">	
	
	<div class="card">
		<div class="card-body">
			@include('admin.partials.validationErrors')
			
			@if(Session::has('success'))
			<div class="alert alert-success">{{Session::get('success')}}</div>
			@endif
			
			<div class="section-header">
				<ol class="breadcrumb">
					<li class="active">Filter Attendees</li>
				</ol>
			</div>
			{!! Form::open(['route' => ['events.emails.create', $event->id], 'class' => 'form form-validate']) !!}
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						{!! Form::label('booking_status', 'Booking status', ['class'=>'control-label']) !!}
						{!! Form::select('booking_status', [''=>'Please select', 1=>'Booked', 2=>'Cancelled', 3=>'Invited', 4=>'Waitlisted'], null, ['class'=>'form-control']) !!}
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						{!! Form::label('user_roles', 'User roles', ['class'=>'control-label']) !!}
						{!! Form::select('user_roles[]', $user_roles, null, ['class'=>'form-control select2-list', 'multiple']) !!}
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						{!! Form::label('user_roles_sub', 'User sub roles', ['class'=>'control-label']) !!}
						{!! Form::select('user_roles_sub[]', $user_roles_sub, null, ['class'=>'form-control select2-list', 'multiple']) !!}
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						{!! Form::label('attended', 'Attendance', ['class'=>'control-label']) !!}
						{!! Form::select('attended', [''=>'Please select', '0'=>'Not attended', 1=>'Attended'], null, ['class'=>'form-control']) !!}
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						{!! Form::label('sessions', 'Schedule slots', ['class'=>'control-label']) !!}
						{!! Form::select('sessions[]', $sessions, null, ['class'=>'form-control select2-list', 'multiple']) !!}
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						{!! Form::label('name', 'Name', ['class'=>'control-label']) !!}
						{!! Form::text('name', null, ['class'=>'form-control', 'placeholder' => 'Name']) !!}
					</div>
				</div>
				<div class="col-md-7">
					<div class="form-group">
						{!! Form::label('question', 'Slot question', ['class'=>'control-label']) !!}
						{!! Form::select('question', [''=>'Please select']+$questions, null, ['class'=>'form-control']) !!}
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						{!! Form::text('answer', null, ['class'=>'form-control', 'placeholder' => 'Answer']) !!}
					</div>
				</div>
			</div>
			
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					{!! Form::submit('Filter', ['class' => 'btn ink-reaction btn-raised btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
	
	<div class="card">
		<div class="card-body">
			<div class="section-header">
				<ol class="breadcrumb">
					<li class="active">Email Configuration</li>
				</ol>
			</div>
			
			@if(isset($filtered_attendees) && sizeof($filtered_attendees) == 0)
				<div class="alert alert-info" role="alert">
					<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> No attendees were found by your chosen filter parameters.
				</div>
			@endif
			
			{!! Form::open(['route' => ['events.emails.send', $event->id], 'class' => 'form form-validate']) !!}
			
			@if(isset($filtered_attendees) && sizeof($filtered_attendees) > 0)
			
			<h3>Filterred list of attendees who will receive the message</h3>
			<table class="table table-striped table-responsive" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Send</th>
						<th>Date Registered</th>
						<th>Name</th>
						<th>Email Address</th>
						<th>Status</th>
						<th>Member</th>
						<th>Contributor role</th>
						<th>Chartered</th>
					</tr>
				</thead>
			
				<tbody>
					@foreach ($filtered_attendees as $attendee)
					<tr>
						<td>
							
							<label>
								{!! Form::checkbox('receipients[]', $attendee->id, true) !!}
							</label>
                            
						</td>
						<td>{{ date("d/m/Y H:i:s",strtotime($attendee->pivot->created_at)) }}</td>
						<td>{{$attendee->first_name}} {{$attendee->last_name}}</td>
						<td><a href="mailto:{{$attendee->email}}">{{$attendee->email}}</a></td>
						<td>{{\Config::get('registrationstatus.'.$attendee->pivot->registration_status_id)}}</td>
						<td>
							@if($attendee->role_id == role('member')) Y @else N @endif
						</td>
						<td>{{implode(", ", $attendee->subRoles->lists('title'))}}</td>
						<td>@if($attendee->chartered) Y @else N @endif</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			@endif
			
			<div class="row">
			    <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('from', 'From', ['class' => 'control-label']) !!}
                        {!! Form::select('from', [''=>'Default by event type']+$senders, null, ['class'=>'form-control']) !!}
                    </div>
                </div>
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('to', 'To', ['class' => 'control-label']) !!}
						{!! Form::text('to', null, ['class'=>'form-control']) !!}
						<p class="help-block">(Optional) Custom receipients. ";" to separate</p>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('subject', 'Email Subject', ['class' => 'control-label']) !!}
						{!! Form::text('subject', null, ['class'=>'form-control', 'id'=>'subject', 'required']) !!}
					</div>
				</div>
				<div class="col-md-6">
                    {!! Form::label('email_templates', 'Load email template', ['class'=>'control-label']) !!}
                    {!! Form::select('email_templates', [''=>'Select template']+$email_templates, null, ['class'=>'form-control select2-list', 'id'=>'email_templates']) !!}
                </div>
			</div>
			
			
			@include('admin.partials.availableTags')
			
			<div class="form-group floating-label">
				{!! Form::textarea('message', 'Enter your message', ['id' => 'ckeditor']) !!}
			</div>
			
			<div class="row">
                {!! Form::label('file_name', 'Insert file link') !!}
			    {!! Form::text('file_name', null, ['class'=>'form-control', 'id' => 'file_name', 'placeholder' => 'File name']) !!}
			    <br/>
			</div>
			
			{!! Form::submit('Send email', ['class' => 'btn ink-reaction btn-raised btn-primary']) !!}
			
			{!! Form::close() !!}
			<br/><br/><br/>
			
		</div>
	</div>
	
	<!-- Email template configuration Event special -->
	@if(sizeof($event->eventType->emailTemplates))
	<div class="card">
		<div class="card-body">
			<div class="section-header">
				<ol class="breadcrumb">
					<li class="active">Event email template configuration</li>
				</ol>
			</div>
			
			{!! Form::open(['route' => ['events.emails.saveTemplates', $event->id], 'class' => 'form form-validate']) !!}
			
			@foreach($event->eventType->emailTemplates as $et)
				<h3>{{$et->type}}</h3>
				<?php 
					$custom_template = $event->emailTemplates()->where('email_template_id', $et->id)->first();
					$active = 1;
					
					$subject = (sizeof($custom_template) != 0) ? $custom_template->pivot->subject : $et->subject;
					
					if(sizeof($custom_template) && $custom_template->pivot->active == 0) $active = 0; ?>
					
				{!! Form::hidden('template_id[]', $et->id) !!}
				{!! Form::hidden('custom_template_id[]', $et->id) !!}
				
				<div class="row">
					<div class="checkbox checkbox-styled">
						<label>
							{!! Form::checkbox('active'.$et->id, '1', $active) !!}
							{!! Form::label('active', 'Active') !!}
						</label>
					</div>
				</div>
				
				<div class="row">
					<div class="form-group floating-label col-xs-2">
						{!! Form::label('subject', 'Subject') !!}
						{!! Form::text('subject'.$et->id, $subject, ['class'=>'form-control']) !!}
					</div>
				</div>
				
				@if(in_array($et->id, array(3,4,7,8)))
				
				<?php $reminder = (!empty($custom_template)) ? $custom_template->pivot->reminder : $et->reminder; ?>
				
				<div class="row">
					<div class="form-group floating-label col-xs-2">
						{!! Form::label('reminder', 'Remind before (days)') !!}
						{!! Form::input('number', 'reminder'.$et->id, $reminder, ['class'=>'form-control', 'min'=>0, 'max'=>99]) !!}
					</div>
				</div>
				@endif
				
				<?php $template = (!empty($custom_template)) ? $custom_template->pivot->template : $et->template; ?>
				<div class="row">
					<div class="form-group">
						{!! Form::label('Template', 'Tamplate') !!}<br/>
						{!! Form::textarea('template'.$et->id, $template, ['id' => 'ckeditor'.$et->id]) !!}
					</div>
				</div>
			@endforeach
			
			<div class="pull-right">
				{!! Form::submit('Update template configuration', ['class' => 'btn ink-reaction btn-raised btn-primary']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
	@endif
</div>
</section>

<script type="text/javascript">
	$(document).ready(function(){
		
		@foreach($event->eventType->emailTemplates as $et)
			$('#ckeditor{{$et->id}}').ckeditor();
		@endforeach
		
		$("#email_templates").bind('change', function(){
			//load tempplate
			$.ajax({
				url: "{!! route('settings.emailTemplates.loadtemplate') !!}",
				type: "GET",
				data: {"template_id" : this.value},
				success: function(res){
					$("#subject").val(res.subject);
					$("#ckeditor").val(res.template);
				}
			});
		})
		
		// autocomplete for central files
		$("#file_name").autocomplete({
        source: function( request, response ) {
        $.ajax({
            url: "{!! route('files.list') !!}",
            dataType: "json",
            data: {term: request.term},
            success: function(data) {
                        response($.map(data, function(item) {
                            
                        return {
                            label: item.display_name,
                            id: item.document_name,
                            };
                    }));
                }
            });
        },
        minLength: 2,
        select: function(event, ui) {
            var html = '<a target="_new" href="{{config("app.url")}}/uploads/files/'+ui.item.id+'">'+ui.item.value+'</a>';
            
            //$("#ckeditor").ckeditor().editor.insertText(html);
            $("#ckeditor").val($("#ckeditor").val() + html);
        }
    });
	});
</script>
@endsection
