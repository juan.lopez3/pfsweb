@extends('admin.layouts.default')

@section('title')
Create User
@endsection

@section('content')
<section>
<div class="section-body">
	<div class="col-md-12">
		<div class="card">
			<div class="card-head style-primary">
				<header>Add User</header>
			</div>
			
			@include('admin.partials.validationErrors')
			
			{!! Form::open(['route' => ['users.store'], 'class' => 'form-horizontal form-validate', 'autocomplete'=>'off', 'files' => true]) !!}
			<div class="card-body">
				 @include('admin.users.form')
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					{!! Form::submit('CREATE User', ['class' => 'btn btn-flat btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
</section>
@endsection
