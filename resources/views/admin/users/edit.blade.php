@extends('admin.layouts.default')

@section('title')
Edit User
@endsection

@section('content')

<section>
<div class="section-body">
	<div class="col-md-12">
		<div class="card">
			<div class="card-head style-primary">
				<header>Edit User</header>
			</div>
			
			@include('admin.partials.validationErrors')
			
			{!! Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'PATCH', 'id'=>'user_edit_form', 'autocomplete'=>'off', 'class' => 'form-horizontal form-validate', 'files' => true]) !!}
			<div class="card-body">
				 @include('admin.users.form')
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					<a href="{{ URL::previous() }}"><button type="button" class="btn btn-flat">Cancel</button></a>
					{!! Form::submit('UPDATE User', ['class' => 'btn btn-flat btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
		
		<div class="card">
            <div class="card-head style-primary">
                <header>Registered Events</header>
            </div>
            
            <div class="card-body">
                
                <div class="section-header">
                    <ol class="breadcrumb">
                        <li class="active">Forthcoming Events</li>
                    </ol>
                </div>
                
                <table id="forthcoming_events" class="table table-striped table-responsive" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Venue Name</th>
                            <th>Postcode</th>
                            <th>Region</th>
                            <th>City</th>
                            <th>Event Date</th>
                            <th>Registration Status</th>
                            <th>Registration Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach($user->events()->where('event_date_from', '>=', date("Y-m-d"))->get() as $event)
                        <tr>
                            <td>{{$event->title}}</td>
                            @if(empty($event->venue_id))
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        @else
                            <td>{{ $event->venue->name}}</td>
                            <td>{{ $event->venue->postcode}}</td>
                            <td>@if(!empty($event->region)){{ $event->region->title}}@endif</td>
                            <td>{{ $event->venue->city}}</td>
                        @endif
                            <td>{{date("d/m/Y", strtotime($event->event_date_from))}}</td>
                            <td>{{config('registrationstatus.'.$event->pivot->registration_status_id)}} @if(!empty($event->pivot->cancel_reason)) ({{$event->pivot->cancel_reason}}) @endif</td>
                            <td>{{date("d/m/Y H:i", strtotime($event->pivot->created_at))}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
                
                <div class="section-header">
                    <ol class="breadcrumb">
                        <li class="active">Previous Events</li>
                    </ol>
                </div>
                
                <table id="previous_events" class="table table-striped table-responsive" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Venue Name</th>
                            <th>Postcode</th>
                            <th>Region</th>
                            <th>City</th>
                            <th>Event Date</th>
                            <th>Registration Status</th>
                            <th>Registration Date</th>
                            <th>Checkin</th>
                            <th>Checkout</th>
                            <th>Paid</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach($user->events()->where('event_date_from', '<', date("Y-m-d"))->get() as $event)
                        <tr>
                            <td>{{$event->title}}</td>
                            @if(empty($event->venue_id))
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        @else
                            <td>{{ $event->venue->name}}</td>
                            <td>{{ $event->venue->postcode}}</td>
                            <td>@if(!empty($event->region)){{ $event->region->title}}@endif</td>
                            <td>{{ $event->venue->city}}</td>
                        @endif
                            <td>{{date("d/m/Y", strtotime($event->event_date_from))}}</td>
                            <td>{{config('registrationstatus.'.$event->pivot->registration_status_id)}} @if(!empty($event->pivot->cancel_reason)) ({{$event->pivot->cancel_reason}}) @endif</td>
                            <td>{{date("d/m/Y H:i", strtotime($event->pivot->created_at))}}</td>
                            <td>@if(!empty($event->pivot->checkin_time)) {{date("d/m/Y H:i", strtotime($event->pivot->checkin_time))}} @endif</td>
                            <td>@if(!empty($event->pivot->checkout_time)) {{date("d/m/Y H:i", strtotime($event->pivot->checkout_time))}} @endif</td>
                            <td>@if($event->pivot->paid)Y @else N @endif</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div>
        </div>
        
        <div class="card">
            <div class="card-head style-primary">
                <header>Sent Emails Record</header>
            </div>
            
            <div class="card-body">
		        @foreach($emails as $email)
                <b>{{date("d/m/Y", strtotime($email->created_at))}}</b> {{$email->to}} - {{$email->subject}}<br/>
                @endforeach
             </div>
        </div>
        
	</div>
</div>
</section>


@include('admin.partials.readonly', ['form_id' => 'user_edit_form'])

@endsection
