@extends('admin.layouts.default')

@section('title')
My Profile
@endsection

@section('content')

<section>
<div class="section-body">
	<div class="col-md-12">
		<div class="card">
			<div class="card-head style-primary">
				<header>My Profile</header>
			</div>
			
			@include('admin.partials.validationErrors')
			
			{!! Form::model($user, ['route' => ['users.updateProfile', $user->id], 'method' => 'PATCH', 'autocomplete'=>'off', 'class' => 'form-horizontal form-validate', 'files' => true]) !!}
			<div class="card-body">
				 @include('admin.users.form')
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					<a href="{{ URL::previous() }}"><button type="button" class="btn btn-flat">Cancel</button></a>
					{!! Form::submit('UPDATE PROFILE', ['class' => 'btn btn-flat btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
</section>
@endsection
