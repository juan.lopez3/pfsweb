@extends('admin.layouts.default')

@section('title')
Users
@endsection

@section('content')


<script type="text/javascript">
$(document).ready(function() {
	$('#users_list').dataTable({
		"processing": true,
        "serverSide": true,
        "iDisplayLength": 50,
        "ajax": "{{route('users.dataTable')}}"
	});
} );
</script>

<section>
<div class="section-header">
	<ol class="breadcrumb">
		<li class="active">Users List</li>
	</ol>
</div>
@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
<p><a href="{{ route('users.create') }}"><button type="button" class="btn btn-success">Add New</button></a></p>
@endif
<div class="section-body">
	<div class="card">
		<div class="card-body">	
			<table id="users_list" class="table table-striped table-responsive" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Membership no.</th>
						<th>First name</th>
						<th>Last name</th>
						<th>Email</th>
						<th>Postcode</th>
						<th>Company</th>
						<th>Role</th>
						<th>Actions</th>
					</tr>
				</thead>
			
				<tfoot>
					<tr>
						<th>Membership no.</th>
						<th>First name</th>
						<th>Last name</th>
						<th>Email</th>
						<th>Postcode</th>
						<th>Company</th>
						<th>Role</th>
						<th>Actions</th>
					</tr>
				</tfoot>
				<!--
				<tbody>
					@foreach (array() as $user)
					<tr>
						<td>{{ $user->pin}}</td>
						<td>{{ $user->first_name }}</td>
						<td>{{ $user->last_name }}</td>
						<td>{{ $user->email }}</td>
						<td>{{ $user->postcode }}</td>
						<td>{{ $user->company }}</td>
						<td>{{ $user->role->title}}</td>
						<td>
							<a href="{{ route('users.edit', $user->id) }}"><button type="button" class="btn btn-warning btn-xs" title="Edit"><span class="glyphicon glyphicon-pencil"></span></button></a>
							<a href="{{ route('users.listActions', $user->id) }}"><button type="button" class="btn btn-info btn-xs" title="Actions history"><span class="md md-history"></span></button></a>
							@if(hasRoles(Auth::user()->role_id, [role('super_administrator')]))
							<a href="{{ route('users.login', $user->id) }}"><button type="button" class="btn btn-info btn-xs" title="Log in as user"><span class="md md-shield-check"></span></button></a>
							<button data-href="{{ route('users.delete', $user->id) }}" type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-trash"></span></button>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
				-->
			</table>
		</div>
	</div>
</div>
</section>

@include('admin.partials.deleteConfirmation')

@endsection
