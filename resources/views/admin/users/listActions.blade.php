@extends('admin.layouts.default')

@section('title')
{{$user->first_name}} {{$user->last_name}} actions history
@endsection

@section('content')
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/bootstrap-datepicker/datepicker3.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/bootstrap-datepicker/datepicker3.css?1424887858')}}" />
<script type="text/javascript" src="{{asset('assets/admin/js/libs/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
<section>
	
	<div class="section-body">
		<div class="container">
			<h2 class="text-light text-center">{{$user->first_name}} {{$user->last_name}} actions history</h2>
			<br/>
			{!! Form::open(['route' => ['users.listActions', $user->id], 'method' => 'get']) !!}
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<div class="input-daterange input-group" id="demo-date-range">
							{!! Form::label('date_from', 'Action Date', ['class' => 'control-label']) !!}
							<div class="input-group-content">
								{!! Form::text('date_from', Input::old('date_from'), ['class' => 'form-control']) !!}
							</div>
							<span class="input-group-addon">to</span>
							<div class="input-group-content">
								{!! Form::text('date_to', Input::old('date_to'), ['class' => 'form-control']) !!}
								<div class="form-control-line"></div>
							</div>
						</div>
					</div>
				</div>
				<div 
				<div class="col-sm-3 col-xs-9">
					<div class="form-group">
						{!! Form::select('action_select', $actions_select, Input::old('action'), ['class'=>'form-control']) !!}
					</div>
				</div>	
				<div class="col-sm-3 col-xs-3">{!! Form::submit('Filter', ['class' => 'btn ink-reaction btn-raised btn-primary']) !!}</div>
			</div>
			
			{!! Form::close() !!}
			
			<!-- BEGIN FIXED TIMELINE -->
			<ul class="timeline collapse-lg timeline-hairline">
				
				@foreach($actions as $action)
				
				<li>
					@if($action->action == 'actionCreate')
					<div class="timeline-circ circ-xl style-success"><span class="md md-input "></span></div>
					@elseif($action->action == 'actionUpdate')
					<div class="timeline-circ circ-xl style-warning"><span class="md md-mode-edit"></span></div>
					@elseif($action->action == 'actionDelete')
					<div class="timeline-circ circ-xl style-danger"><span class="md md-delete"></span></div>
					@else
					<div class="timeline-circ circ-xl style-primary"><span class="md md-accessibility "></span></div>
					@endif
					<div class="timeline-entry">
						<div class="card style-default-bright">
							<div class="card-body small-padding">
								<span class="text-medium">{{$action->details}}<br/></span>
								<span class="opacity-50">
									{{ date("j F, Y H:i:s",strtotime($action->created_at)) }}
								</span>
							</div>
						</div>
					</div>
				</li>
				@endforeach

				@if($actions->isEmpty())
					<li>
						<div class="timeline-circ circ-xl style-info"><span class="md md-info "></span></div>
						<div class="timeline-entry">
							<div class="card style-default-bright">
								<div class="card-body small-padding">
									<span class="text-medium">No user actions by selected filtering parameters</span>
								</div>
							</div>
						</div>
					</li>
				@endif
			</ul>
		</div>
	</div>
</section>

@endsection
