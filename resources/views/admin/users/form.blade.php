<div class="row">
    @if (isset($user) && !in_array($user->role_id, [role('member'), role('non_member')]))
    
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('pin', 'Pin', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-9">
                {!! Form::text('pin', null, ['class'=>'form-control']) !!}
            </div>
        </div>
    </div>
    <div class="col-sm-6">
    </div>
    
    @endif
    
	<div class="col-sm-6">
		<div class="form-group">
			{!! Form::label('title', 'Title', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				{!! Form::select('title', ['Mr'=>'Mr', 'Ms' => 'Ms', 'Mrs'=>'Mrs', 'Miss'=>'Miss', 'Dr'=>'Dr'], null, ['class'=>'form-control']) !!}
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group">
			{!! Form::label('badge_name', 'Badge name', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				{!! Form::text('badge_name', null, ['class'=>'form-control']) !!}
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			{!! Form::label('first_name', 'First name', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				{!! Form::text('first_name', null, ['class'=>'form-control', 'required']) !!}
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group">
			{!! Form::label('dietary_requirement_id', 'Dietary Requirements', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				{!! Form::select('dietary_requirement_id', $dietary_requirements, null, ['class'=>'form-control select2-list', 'id'=>'dietary_requirement_id']) !!}
			</div>
		</div>
		
		<div class="form-group" id="dietary_other">
			{!! Form::label('dietary_requirement_other', 'Other', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				{!! Form::text('dietary_requirement_other', null, ['class'=>'form-control']) !!}
			</div>
		</div>
		
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			{!! Form::label('last_name', 'Last name', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				{!! Form::text('last_name', null, ['class'=>'form-control', 'required']) !!}
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group">
			{!! Form::label('special_requirements', 'Special Requirements', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				{!! Form::text('special_requirements', null, ['class'=>'form-control']) !!}
			</div>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			{!! Form::label('password', 'Password', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				{!! Form::password('password', ['class'=>'form-control', 'id'=>'password']) !!}
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group">
			{!! Form::label('attendee_types', 'Attendee Types', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				@if(isset($user))
					{!! Form::select('attendee_types[]', $attendee_types, $user->attendeeTypes->lists('id'), ['class'=>'form-control select2-list', 'multiple', 'data-placeholder'=>'Select attendee types']) !!}
				@else
					{!! Form::select('attendee_types[]', $attendee_types, null, ['class'=>'form-control select2-list', 'multiple', 'data-placeholder'=>'Select attendee types']) !!}
				@endif
				
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			{!! Form::label('email', 'Email', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				{!! Form::input('email', 'email', null, ['class'=>'form-control', 'required']) !!}
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group">
			{!! Form::label('bio', 'Bio', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				{!! Form::textarea('bio', null, ['class'=>'form-control control-2-rows']) !!}
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			{!! Form::label('cc_email', 'CC Email', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				{!! Form::input('email', 'cc_email', null, ['class'=>'form-control']) !!}
			</div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
			{!! Form::label('phone', 'Phone', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				{!! Form::text('phone', null, ['class'=>'form-control']) !!}
				<p class="help-block">Digits only</p>
			</div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="form-group">
			{!! Form::label('mobile', 'Mobile', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				{!! Form::text('mobile', null, ['class'=>'form-control']) !!}
				<p class="help-block">Digits only</p>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			{!! Form::label('fca_number', 'FCA Number', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				{!! Form::text('fca_number', null, ['class'=>'form-control']) !!}
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			{!! Form::label('postcode', 'Postcode', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				{!! Form::text('postcode', null, ['class'=>'form-control', 'required']) !!}
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			{!! Form::label('country_id', 'Country', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				{!! Form::select('country_id', [''=>'Select country']+$countries, null, ['class'=>'form-control', 'required']) !!}
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			{!! Form::label('company', 'Company', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				{!! Form::text('company', null, ['class'=>'form-control']) !!}
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group">
			{!! Form::label('profile_image', 'Profile image', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				{!! form::file('profile_image') !!}
				@if(isset($user) && !empty($user->profile_image))
					{!! HTML::image('uploads/profile_images/'.$user->profile_image,'', ['class'=>'img-responsive img-circle height-2']) !!}
				@endif
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			{!! Form::label('job_title', 'Job Title', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				{!! Form::text('job_title', null, ['class'=>'form-control']) !!}
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			{!! Form::label('role_id', 'Role', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				@if(isActiveRoute('users.profile'))
				{!! Form::select('role_id', $roles, null, ['class'=>'form-control select2-list', 'data-placeholder'=>'Select role', 'disabled', 'id' => 'role_id']) !!}
				@else
				{!! Form::select('role_id', $roles, null, ['class'=>'form-control select2-list', 'data-placeholder'=>'Select role', 'id' => 'role_id']) !!}
				@endif
			</div>
		</div>
	</div>
</div>

<div class="row" id="sub_roles" @if(isset($user) && $user->hasRoles(['Member', 'Non - Member'])) 45 @else style="display:none;"  @endif>
	<div class="col-sm-6">
		<div class="form-group">
			{!! Form::label('user_roles_sub_id', 'Sub Roles', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				@if(isset($user))
					{!! Form::select('user_roles_sub_id[]', $user_roles_sub, $user->subRoles->lists('id'), ['class'=>'form-control select2-list', 'multiple', 'data-placeholder'=>'Select role']) !!}
				@else
					{!! Form::select('user_roles_sub_id[]', $user_roles_sub, null, ['class'=>'form-control select2-list', 'multiple', 'data-placeholder'=>'Select role']) !!}
				@endif
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
    $("#password").val('');
});
	$('#role_id').bind('change', function (event) {
        if (this.value == 4 || this.value == 5) {
        	$('#sub_roles').show();
        } else {
        	$('#sub_roles').hide();
        }
    });

    if($('#dietary_requirement_id').val() != 13) $('#dietary_other').hide();

    $('#dietary_requirement_id').bind('change', function(event){
    	if(this.value == 13) {
    		$('#dietary_other').show();
    	} else {
    		$('#dietary_other').hide();
    	}
    });
</script>

@if(isset($user))
<hr class="ruler">
<div class="row">
	<div class="col-col-sm-6">
		<div class="col-xs-4 col-md-3 text-right">PFS Membership Number</div><div class="col-xs-8">{{$user->pin}}</div>
	</div>
</div>
<div class="row">
	<div class="col-col-sm-6">
		<div class="col-xs-4 col-md-3 text-right">PFS Status</div><div class="col-xs-8">{{$user->pfs_class}}</div>
	</div>
</div>
<div class="row">
	<div class="col-col-sm-6">
		<div class="col-xs-4 col-md-3 text-right">Chartered Status</div><div class="col-xs-8">@if ($user->chartered) Yes @else No @endif</div>
	</div>
</div>
@endif
