<!-- @if ($event->schedule_version == 1) -->
			<br/>
				<div class="tabs">
					<ul class="nav nav-tabs">
                        <?php $first = true; ?>
						@foreach ($event_days as $seq => $day)
							<li class="@if($first) active @endif"><a href="#day{{$day}}" data-toggle="tab">DAY {{$seq+1}} - {{date("d/m/Y", strtotime($day))}}</a></li>
                            <?php $first = false; ?>
						@endforeach

					</ul>
					<div class="tab-content">
                        <?php $first = true; ?>
						@foreach ($event_days as $day)
							<div id="day{{$day}}" class="tab-pane @if($first)active @endif">

								@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
									<div class="card-actionbar">
										<div class="card-actionbar-row">
											<button type="button" class="btn btn-sm ink-reaction btn-raised btn-success" data-toggle="modal" data-target="#add_time_slot{{$day}}">ADD TIME SLOT</button>
										</div>
									</div>

									<div class="modal fade" id="add_time_slot{{$day}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										<div class="modal-dialog" style="width: 60%;">
											<div class="card">
												<div class="card-head style-primary">
													<header>Add Time Slot</header>
												</div>

												{!! Form::open(['route' => ['events.scheduleBuilder.storeTimeSlot', $event->id], 'class' => 'form-horizontal form-validate']) !!}

												<div class="card-body">
													<div class="row">
														<div class="col-xs-4 col-sm-offset-1">
															<div class="form-group">
																{!! Form::hidden('start_date', $day) !!}
																{!! Form::label('start', 'Start Time', ['class' => 'control-label']) !!}
																{!! Form::text('start', null, ['class'=>'form-control time-mask', 'required']) !!}
																<p class="help-block">Time: 24h</p>
															</div>
															<div class="checkbox checkbox-styled">
																<label>
																	{!! Form::checkbox('show_start', 1) !!}
																	Show start time
																</label>
															</div>
														</div>
														<div class="col-xs-4 col-xs-offset-2">
															<div class="form-group">
																{!! Form::hidden('end_date', $day) !!}
																{!! Form::label('end', 'End Time', ['class' => 'control-label']) !!}
																{!! Form::text('end', null, ['class'=>'form-control time-mask', 'required']) !!}
																<p class="help-block">Time: 24h</p>
															</div>
															<div class="checkbox checkbox-styled">
																<label>
																	{!! Form::checkbox('show_end', 1) !!}
																	Show end time
																</label>
															</div>
														</div>
													</div>
													<div class="col-xs-offset-1 col-sm-4">
														<div class="form-group">
															{!! Form::label('columns', 'Number of columns', ['class' => 'control-label required']) !!}
															{!! Form::input('number', 'columns', 1, ['class'=>'form-control', 'required']) !!}
														</div>
													</div>
												</div>

												<div class="card-actionbar">
													<div class="card-actionbar-row">
														<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
														{!! Form::submit('CREATE TIME SLOT', ['class' => 'btn btn-flat btn-primary']) !!}
													</div>
												</div>
												{!! Form::close() !!}
											</div>
										</div>
									</div>
								@endif

								@if (isset($schedule_header[$day]))
								<?php
                                if (sizeof($schedule_header[$day]) >= 5) {
                                    $column_width = 2;
                                } elseif (sizeof($schedule_header[$day]) == 0) {
                                    $column_width = 12;
                                } else {
                                    $column_width = 12 / sizeof($schedule_header[$day]);
                                }
                                ?>
								<div class="row">
									<div class="col-xs-offset-1 col-xs-11 text-center">
										@foreach ($schedule_header[$day] as $title)
											<div class="hidden-sm hidden-xs col-md-{{$column_width}}"><b>{{$title}}</b></div>
										@endforeach
									</div>
								</div>
								@endif

								@foreach ($day_slots[$day] as $slot)
								<?php
                                $start = strtotime($slot->start);
                                $end = strtotime($slot->end);
                                $slot_height = (int) round(abs($end - $start) / 60) * 3;
                                ?>
								<div class="row" style="">
									<div class="col-xs-1 text-center">
										@if ($slot->show_start) {{date("H:i", strtotime($slot->start))}} <br/> @endif

										@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
											<button data-href="#" data-days_range="{{date("H:i", strtotime($slot->start))}} and {{date("H:i", strtotime($slot->end))}}" id="{{$slot->id}}" type="button" class="btn new-session btn-success btn-xs" data-toggle="modal" data-target="#add_session"><i class="glyphicon glyphicon-plus "></i></button> <br/>
											<a href="{{ route('events.scheduleBuilder.editTimeSlot', [$event->id, $slot->id]) }}"><button type="button" class="btn btn-warning btn-xs" title="Edit Time Slot"><i class="glyphicon glyphicon-pencil"></i></button></a>
											<button data-href="{{ route('events.scheduleBuilder.deleteTimeSlot', [$event->id, $slot->id]) }}" type="button" class="btn btn-danger btn-xs" title="Remove Time Slot" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-trash"></span></button>
										@endif

										@if ($slot->show_end)<br/> {{date("H:i", strtotime($slot->end))}} @endif
									</div>

									<div class="col-xs-11">
										<?php
                                        if ($slot->columns >= 5) {
                                            $column_width = 2;
                                        } elseif ($slot->columns == 0) {
                                            $column_width = 12;
                                        } else {
                                            $column_width = 12 / $slot->columns;
                                        }
                                        ?>
										@for($i=1; $i <=$slot->columns; $i++)
											<?php
                                                // for each slot clean top start time for correct gap calculation
                                                $start = strtotime($slot->start);
                                            ?>
											<form id="list{{$slot->id.$i}}">
											{!! Form::hidden('order_column', $i) !!}
											{!! Form::hidden('slot_id', $slot->id) !!}

											<div class="col-md-{{$column_width}} sortable-slot" id="slot{{$slot->id.$i}}" style="min-height: 200px;">

												@foreach ($slot->sessions()->where('order_column', $i)->orderBy('session_start')->with('type')->get() as $session)
                                                    <?php
                                                    $session_start = strtotime($session->session_start);
                                                    $session_end = strtotime($session->session_end);
                                                    $session_gap = (int) round(abs($start - $session_start) / 60) * 3;
                                                    $session_height = (int) round(abs($session_end - $session_start) / 60) * 3;
                                                    $start = $session_end;
                                                    ?>
													<div class="session col-md-12" style="min-height: 80px; background: {{$session->type->color}}; height: {{$session_height}}px; margin-top: {{$session_gap}}px;">
														<div class="col-xs-10">
															<b>{{date("H:i", strtotime($session->session_start))}} - {{date("H:i", strtotime($session->session_end))}}</b><br/>
															<div class="visible-xs visible-sm">{{$session->meetingSpace->meeting_space_name}}<br/></div>
															{{$session->title}}<br/>
														</div>
														<div class="col-xs-2 text-right">
															@if($slot->columns > 1)<div class="handle-horizontal"><input type="hidden" name="item[]" value="{{$session->id}}" /></div> @endif
															<a href="{{ route('events.scheduleBuilder.editSession', [$event->id, $session->id]) }}"><button type="button" class="btn btn-warning btn-xs" title="Edit Session"><i class="glyphicon glyphicon-pencil"></i></button></a>
															<button data-href="{{ route('events.scheduleBuilder.deleteSession', [$event->id, $session->id]) }}" type="button" class="btn btn-danger btn-xs" title="Remove Session" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-trash"></span></button>
														</div>
													</div>
												@endforeach
											</div>
											</form>

											<script>
												$( function() {
													$( "#slot{{$slot->id.$i}}" ).sortable({
														connectWith: ".sortable-slot",
                                                        'handle': '.handle-horizontal',
                                                        dropOnEmpty: true,
                                                        update: function(event, ui) {
                                                            $.post('{{ route('events.scheduleBuilder.updateSessionsOrder') }}',
                                                                $("#list{{$slot->id.$i}}").serialize(), function(data) {
                                                                    location.reload(); // reload for correct ordering by time
                                                                }, 'json');
                                                        }
													});
												} );
											</script>
										@endfor
									</div>
								</div>
								@endforeach

								<br /><br />
								<hr />
								<div class="row">
									<div class="col-md-12">
										{!! Form::model($event->scheduleExplanationTexts()->where('day', $day)->first(), ['route' => ['events.scheduleBuilder.updateExplanationText', $event->id], 'method' => 'PUT', 'class' => 'form form-validate']) !!}
										{!! Form::hidden('day', $day) !!}
										{!! Form::textarea('text', null, ['row'=>'3', 'style'=>'height: 200px;', 'id'=>'schedule_editor'.$day]) !!}
										<script type="text/javascript">
											$(document).ready(function(){$( '#schedule_editor{{$day}}' ).ckeditor();});
										</script>
										{!! Form::submit('Update explanation text', ['class' => 'btn btn-flat btn-primary']) !!}
										{!! Form::close() !!}
									</div>
								</div>
							</div>
                            <?php $first = false; ?>
						@endforeach
					</div>
				</div>
			<!--@else  Schedule version #0 -->