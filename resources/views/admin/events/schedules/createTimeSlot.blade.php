<div class="section-body">
	<div class="col-md-12">
		<div class="card">
			<div class="card-head style-primary">
				<header>Add Time Slot</header>
			</div>
			
			{!! Form::open(['route' => ['events.scheduleBuilder.storeTimeSlot', $event->id], 'class' => 'form-horizontal form-validate']) !!}
				@include('admin.events.schedules.timeSlotForm')
				
				<div class="card-actionbar">
					<div class="card-actionbar-row">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						{!! Form::submit('CREATE TIME SLOT', ['class' => 'btn btn-flat btn-primary']) !!}
					</div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>