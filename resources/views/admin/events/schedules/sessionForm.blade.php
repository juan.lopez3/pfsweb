{!! Form::hidden('event_schedule_slot_id', null, ['id'=>'event_schedule_slot_id']) !!}

<div class="card-body">
	@if(isset($slot_session))
	<i>Session start and end times should be in range of {{ date("H:i", strtotime($slot_session->slot->start)) }} - {{ date("H:i", strtotime($slot_session->slot->end)) }}</i>
	@endif
	<div class="row">
		<div class="col-xs-6">
			<div class="form-group">
				{!! Form::label('session_start', 'Start Time', ['class' => 'control-label']) !!}
				{!! Form::text('session_start', null, ['class'=>'form-control time-mask', 'required']) !!}
				<p class="help-block">Time: 24h</p>
			</div>

			<div class="checkbox checkbox-styled">
				<label>
					{!! Form::checkbox('show_start', 1) !!}
					Show start time
				</label>
			</div>
		</div>
		<div class="col-xs-6">
			<div class="form-group">
				{!! Form::label('session_end', 'End Time', ['class' => 'control-label']) !!}
				{!! Form::text('session_end', null, ['class'=>'form-control time-mask', 'required']) !!}
				<p class="help-block">Time: 24h</p>
			</div>
			<div class="checkbox checkbox-styled">
				<label>
					{!! Form::checkbox('show_end', 1) !!}
					Show end time
				</label>
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-xs-6">
			<div class="checkbox checkbox-styled">
				<label>
					{!! Form::checkbox('visible', 1) !!}
					Visible on Programme
				</label>
			</div>
		</div>
	</div>
</div>

@if ($event->schedule_version == 1)
<div class="row">
	<div class="col-xs-offset-1 col-xs-11">
		<div class="col-md-6">
			<div class="form-group">
				{!! Form::label('meeting_space_id', 'Meeting Space', ['class'=>'control-label']) !!}
				{!! Form::select('meeting_space_id', [''=>'']+$meeting_room_spaces, null, ['class'=>'form-control required']) !!}
			</div>
		</div>

		<div class="col-md-3">
			<div class="form-group">
				{!! Form::label('capacity', 'Capacity', ['class' => 'control-label']) !!}
				{!! Form::input('number', 'capacity', null, ['class'=>'form-control', 'min' => '0', 'required']) !!}
			</div>
		</div>

		<div class="col-md-3">
			<br/>
			<div class="checkbox checkbox-styled">
				<label>
					{!! Form::checkbox('selectable', 1) !!}
					Selectable
				</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<div class="form-group">
			{!! Form::label('order_column', 'Assigned Column', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-4">
				{!! Form::input('number', 'order_column', null, ['class'=>'form-control', 'required', 'placeholder'=>'1']) !!}
			</div>
		</div>
	</div>
</div>

@else
<div class="row">
    <div class="col-xs-offset-1 col-xs-11">
        <div class="form-group">
            <div class="checkbox checkbox-styled">
                <label>
                    {!! Form::checkbox('use_api_start', 1) !!}
                    Use as event start time
                </label>
            </div>
        </div>
    </div>
</div>
@endif

<div class="row">
	<div class="col-xs-12">
		<div class="form-group">
			{!! Form::label('session_type_id', 'Session Type', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-8">
				{!! Form::select('session_type_id', $session_types, null, ['class'=>'form-control select2-list']) !!}
			</div>
		</div>
	</div>
</div>

 <div class="row">
	<div class="col-xs-12">
		<div class="form-group">
			{!! Form::label('title', 'Title', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-8">
				{!! Form::text('title', null, ['class'=>'form-control', 'required']) !!}
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<div class="form-group">
			{!! Form::label('description', 'Description', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-8">
				{!! Form::text('description', null, ['class'=>'form-control']) !!}
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<div class="form-group">
			{!! Form::label('leaning_objectives', 'Learning Objectives', ['class' => 'col-sm-3 control-label']) !!}<br/>
			<div class="col-sm-12">
				{!! Form::textarea('learning_objectives', null, ['row'=>'2', 'style'=>'height: 100px;', 'id'=>'learning_objectives']) !!}
			</div>
		</div>
	</div>
</div>

@if ($event->schedule_version == 0)
<div class="row">
	<div class="col-xs-12">
		<div class="form-group">
			{!! Form::label('question', 'Question', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-8">
				{!! Form::text('question', null, ['class'=>'form-control']) !!}
			</div>
		</div>
	</div>
</div>
@endif

<div class="row">
	<div class="col-xs-6 col-xs-offset-1">{!! Form::select('user_id', ['' => 'Please select contributor'] + $contributors_select, null, ['class'=>'form-control select2-list', 'id'=>'contributor_id']) !!}</div>
	<div class="col-xs-3">{!! Form::select('type', $contributor_types, null, ['class'=>'form-control', 'id'=>'contributor_type']) !!}</div>
	<div class="col-xs-1">{!! Form::button('+', ['class' => 'btn ink-reaction btn-raised btn-sm btn-success', 'id'=>'add_contributor', 'title'=>'Add selected contributor']) !!}</div>
</div>

<div class="col-xs-11 col-xs-offset-1">
	<div id="session_contributors">
		@if(isset($slot_session))
		@foreach($slot_session->contributors as $c)
		<div class="row">
			<div class="col-xs-6"><input type="hidden" value="{{$c->id}}" name="contributor_id[]" /><input type="text" name="name[]" value="{{$c->badge_name}}" readonly class="form-control" /></div>
			<div class="col-xs-3"><input type="text" name="contributor_type[]" value="{{$c->pivot->type}}" readonly class="form-control" /></div>
			<div class="col-xs-1"><button type="button" class="delete btn btn-danger btn-icon-toggle" title="Delete"><i class="md md-delete"></i></button></div>
		</div>
		@endforeach
		@endif
	</div>
</div>
<br/><br/>

<script type="text/javascript">
$(document).ready(function(){
	$('#learning_objectives').ckeditor({
		height: 100
	});
});

$("#add_contributor").click(function () {
	
	var html = '<div class="row">'+
					'<div class="col-xs-6"><input type="hidden" name="contributor_id[]" value="'+$('#contributor_id').val()+'" /><input type="text" name="name[]" value="'+$('#contributor_id option:selected').text()+'" readonly class="form-control" /></div>'+
					'<div class="col-xs-3"><input type="text" name="contributor_type[]" value="'+$('#contributor_type').val()+'" readonly class="form-control" /></div>'+
					'<div class="col-xs-1"><button type="button" class="delete btn btn-danger btn-icon-toggle" title="Delete"><i class="md md-delete"></i></button></div>'+
				'</div>';
				
	$("#session_contributors").append(html);
});

$("body").on("click", ".delete", function (e) {
	$(this).parent("div").parent("div").remove();
});
</script>