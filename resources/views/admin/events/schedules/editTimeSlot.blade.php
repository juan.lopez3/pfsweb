@extends('admin.layouts.default')

@section('title')
Edit Schedule Time Slot
@endsection

@section('content')

<section>
<div class="section-body">
	<div class="col-sm-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
		<div class="card">
			<div class="card-head style-primary">
				<header>Edit Schedule Time Slot</header>
			</div>
			
			@include('admin.partials.validationErrors')
			
			{!! Form::model($time_slot, ['route' => ['events.scheduleBuilder.updateTimeSlot', $event->id, $time_slot->id], 'method' => 'PATCH', 'class' => 'form form-validate']) !!}
			<div class="card-body">
				 @include('admin.events.schedules.timeSlotForm')
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					<a href="{{ URL::previous() }}"><button type="button" class="btn btn-flat">Cancel</button></a>
					{!! Form::submit('UPDATE TIME SLOT', ['class' => 'btn btn-flat btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
</section>
@endsection
