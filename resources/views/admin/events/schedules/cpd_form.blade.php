<!DOCTYPE html>
<html>
<head>
    <title>Attendance Certificate</title>
<style type="text/css">
    @font-face {
      font-family: 'MetaPlusBold';
      src: url('{{config("app.root")}}storage/fonts/MetaPlusBold.ttf') format('truetype');
    }
    
    @font-face {
      font-family: 'MetaPlusBookRoman';
      src: url('{{config("app.root")}}storage/fonts/MetaPlusBookRoman.otf') format('truetype');
    }
    
    @font-face {
      font-family: 'MyriadPro-Regular';
      src: url('{{config("app.root")}}storage/fonts/MyriadPro-Regular.otf') format('truetype');
    }
    @font-face {
      font-family: 'SwissBoldCondensed';
      src: url('{{config("app.root")}}storage/fonts/SwissBoldCondensed.ttf') format('truetype');
    }
    
    @font-face {
      font-family: 'SwissCondensed';
      src: url('{{config("app.root")}}storage/fonts/SwissCondensed.ttf') format('truetype');
    }
    
    @font-face {
      font-family: 'SwissLightCondensed';
      src: url('{{config("app.root")}}storage/fonts/SwissLightCondensed.ttf') format('truetype');
    }

     body {font-family: 'SwissLightCondensed'; font-size: 12pt; color: #192387}
     .small {font-size: 8pt}
     .medium {font-size: 10pt}
     h2 {font-size: 14pt; margin-bottom: 0px; font-family: 'SwissBoldCondensed'}
     .heading{font-size: 14pt; margin-bottom: 0px; font-family: 'SwissBoldCondensed'}
     hr.original{ background-color: #192387; height: 3px}
     hr.custom { background-color: #C6C8E3; border: #C6C8E3; height: 3px;}
     thead {background-color: #192387; color: #FFF; font-size: 8pt; margin-bottom: 4px; font-family: 'SwissCondensed'}
     table {width: 100%; font-size: 8pt; border-spacing: 0;}
     table.feedback tr td {border-bottom: 1px solid #192387;}
     table.feedback thead tr td {padding: 5px;}
     .rating{word-spacing: 18px; padding-left: 10px;}
     .bb {border-bottom: 1px solid #192387;}
     .br {border-right: 1px solid #192387;}
     .br-white {border-right: 1px solid #FFF;}
     
     .bold-consensed {font-family: 'SwissBoldCondensed'}
     .meta-plus {font-family: 'MetaPlusBookRoman'}
     .no-break{page-break-inside: avoid;}
     p {margin-top: 0px; margin-bottom: 5px;}

 ul li {
    line-height: 7px;
 }




</style>
</head>
<body>

<table>
    <tr>
        <td width="50%">
            <span class="bold-consensed" style="font-size: 12pt">Important notes:</span><br />
            <span class="small">This form is intended to help you reflect on the value of CPD you gained from attending this
event whilst it is fresh in your memory. You should take it away with you and use it to update
your CPD records after the event.</span>
        </td>
        <td align="right">
            @if($event->eventType->id == 58)
                <img src="images/SMP-logo.png" width="250" height="150" alt="SMP Accredited">
            @else
                <img height="110px" src="images/cii_logo.png" />
            @endif
        </td>
    </tr>
    <tr>
        <td class="custom">You will also have access to a CPD Attendance Certificate through the MyEvents Portal at
<span class="bold-consensed">www.thepfs.org/events</span>. For more information about the Personal Finance Society’s CPD
scheme and your ongoing commitment to CPD please visit <span class="bold-consensed">www.thepfs.org/CPD</span>.</td>
        <td align="right"><span class="bold-consensed" style="font-size: 22pt;">CPD Log</span></td>
    </tr>
</table>


<hr class="original"/>

<p class="heading">Section A - Event details</p>
<span class="medium" style="color: black; font-family: 'MetaPlusBold'">{{$event->title}}, {{date("l d M Y", strtotime($event->event_date_from))}}@if(sizeof($event->venue)), {{$event->venue->name}}@endif</span>

<p class="heading">Section B - CPD Log</p>

<table class="feedback">
    <thead>
        <tr>
            <td width="20%" class="br-white">Name</td>
            <td width="45%" class="br-white">Learning objectives</td>
            <td width="35%">Reflective statement</td>
        </tr>
    </thead>
    
    <tbody style="color: black; font-family: 'MetaPlusBookRoman'">
    @foreach ($slots as $slot)
    @foreach($slot->sessions as $session)
    <?php if (!$session->type->include_in_cpd && !in_array($session->session_type_id, [1, 10])) continue; ?>
        <tr>
            <td valign="top" class="br" height="@if(in_array($session->session_type_id, [1, 10])) 60px @else 120px @endif">
                <span style="font-family: 'MetaPlusBold'">
                    {!!$session->title!!} <br/>
                    {{date("H:i", strtotime($session->session_start))}} - {{date("H:i", strtotime($session->session_end))}}
                </span><br/>
                    
                @foreach($session->contributors as $contributor)
                {{$contributor->first_name}} {{$contributor->last_name}}, {{$contributor->company}} <br/>
                @endforeach
            </td>
            <td class="br" style="font-size: 7pt">{!!$session->learning_objectives!!}</td>
            <td>&nbsp;</td>
        </tr>
    @endforeach
    @endforeach
    </tbody>
</table>

<span class="small meta-plus" style="color: black">The content in each session has been carefully selected and can be considered for both structured and unstructured CPD hours, depending how this activity
addressed your individual personal development needs.</span><br/><br/>

<span class="small meta-plus" style="color: black">Structured CPD is the undertaking of any formal learning activity designed to meet a specific learning outcome (this is what an individual is expected to know,
understand or do as a result of his or her learning).</span><br/><br/>

<span class="small meta-plus" style="color: black">Unstructured CPD is any activity an individual considers has met a learning outcome, but which may not have been specifically designed to meet their
development needs.</span><br/><br/>

<table>
    <tr>
        <td width="65%"><span class="small meta-plus">
            The Personal Finance Society<br/>
42–48 High Road, South Woodford,<br/>
London E18 2JP<br/>
tel: +44 (0)20 8530 0852<br/>
fax: +44 (0)20 8530 3052<br/>
customer.serv@thepfs.org<br/>
www.thepfs.org
</span>
        </td>
        <td align="right"><br/><br/> 
            <img src="images/inlogo.png" width="15px" height="15px" /> 
                PFS President'sThinks <br>
            @if(!is_null($event->hashtag))
                <img src="images/Twitter-bird-50x40.png" width="15px" height="15px" />  
                #{{$event->hashtag}} <br>
            @else
                <img src="images/Twitter-bird-50x40.png" width="15px" height="15px" /> 
                @Pfsregiones
            @endif
                www.thepfs.org
        </td>
    </tr>
</table>

</body>
</html>