@extends('admin.layouts.default')

@section('title')
{{$event->title}} Schedule Builder
@endsection

@section('content')
<section>
<div class="section-header">
	<div class="row">
		<div class="col-xs-9"><span class="event-head">{{$event->title}}</span> - {{ date("j F Y",strtotime($event->event_date_from)) }}<br/>
			<span class="glyphicon glyphicon-time text-accent-dark "></span><span class="event-head"> Schedule Builder</span>
		</div>
		<div class="col-xs-3 text-right">@include('admin.partials.eventActions')</div>
	</div>
</div>
<br/>
<div class="section-body">	
	
	<div class="card">
		<div class="card-body">

			@include('admin.partials.validationErrors')

			{!! Form::open(['route' => ['events.scheduleBuilder.copySchedule', $event->id], 'method'=>'post', 'class' => 'form form-validate', 'onsubmit'=>'return confirm("Event has '.$event->atendees()->where('registration_status_id', config('registrationstatus.booked'))->count().' active bookings. Copying schedule will remove slot bookings, are you sure you want to continue!");']) !!}
			<div class="row">
				<div class="col-sm-3">
					<div class="form-group floating-label">
						{!! Form::text('old_event_name', null, ['class'=>'form-control', 'id' => 'event_name', 'required']) !!}
						{!! Form::label('old_event_name', 'Event Name') !!}
						{!! Form::hidden('event_name_id', null, array('id' => 'event_name_id')) !!}
					</div>
				</div>
				<div class="col-sm-1">
					{!! Form::submit('Copy schedule', ['class' => 'btn ink-reaction btn-sm btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
			
			<div class="checkbox checkbox-styled">
				<label>
					{!! Form::checkbox('visible_on_web', 1, $event->main_schedule_visible_web, ['class'=>'schedule_visibility', 'id'=>'visible_on_web']) !!}
					{!! Form::label('visible_on_web', 'Visible on Website') !!}
				</label>
			</div>

			<div class="checkbox checkbox-styled">
				<label>
					{!! Form::checkbox('visible_on_app', 1, $event->main_schedule_visible_app, ['class'=>'schedule_visibility_app', 'id'=>'visible_on_app']) !!}
					{!! Form::label('visible_on_app', 'Visible on App') !!}
				</label>
			</div>
			
			<div class="checkbox checkbox-styled">
				<label>
					{!! Form::checkbox('visible_on_reg', 1, $event->main_schedule_visible_reg, ['class'=>'schedule_visibility', 'id'=>'visible_on_reg']) !!}
					{!! Form::label('visible_on_reg', 'Visible on Registration') !!}
				</label>
			</div>

			<div class="checkbox checkbox-styled">
				<label>
					{!! Form::checkbox('mandatory_slots', 1, $event->mandatory_slots, ['class'=>'schedule_visibility', 'id'=>'mandatory_slots']) !!}
					{!! Form::label('mandatory_slots', 'Make all sessions mandatory during booking') !!}
				</label>
			</div>
			<br>
			<br>

			<div class="checkbox checkbox-styled">
				<label><b>Schedule Type</b></label>
					{!! Form::select('schedule_version', ['Bookable Time Slots', 'Selectable Sessions (New)'], $event->schedule_version, ['class'=>'form-control', 'id'=>'schedule_version']) !!}
			</div>

			<!-- Aqui va la informacion de schedule version  == 1 -->
			
			@if ($event->schedule_version == 1 || $event->schedule_version == 0)
				@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
				
				<div class="tabs">
					<ul class="nav nav-tabs">
                        <?php $first = true; ?>
						@foreach ($event_days as $seq => $day)
							<li class="@if($first) active @endif"><a href="#day{{$day}}" data-toggle="tab">DAY {{$seq+1}} - {{date("d/m/Y", strtotime($day))}}</a></li>
                            <?php $first = false; ?>
						@endforeach
					</ul>				
					<div class="tab-content">

						<?php $first = true; ?>
						@foreach ($event_days as $day)
							<div id="day{{$day}}" class="tab-pane @if($first)active @endif">
				
								<div class="card-actionbar">
									<div class="card-actionbar-row">
										<a href="{{route('events.scheduleBuilder.generateCPDLog', $event->id)}}" class="btn btn-success btn-sm">GENERATE CPD LOG</a>
										<button  onclick="setDateOnTimeSlot('{{$day}}')" data-href="#" type="button" class="btn btn-sm ink-reaction btn-raised btn-success" data-toggle="modal" data-target="#add_time_slot">ADD TIME SLOT</button>
									</div>
								</div> <!-- @END card-actionbar -->
<!----- INIT DAY SCHEDULE -->

				@if(!$slots->isEmpty())

<!-- display schedule if exists
	* 1st - group slots by time and loop time slots
	* 2nd - loop grouped time slot for sessions and display sessions in one slot
	-->
@foreach($slots as $slot_key => $slot)
<?php if ($day != $slot[0]->start_date){
    continue;
}?>
<!-- [START] time slot row loop -->
<div class="row row-eq-height">{{$day}}{{$slot[0]->start_date}}
	<div class="text-center slot-time"><br/>{{date("H:i",strtotime($slot[0]->start))}}</div>

	<?php //calculate columns width: 95%/ num_of_col 5% for time
		$column_width = 95/sizeof($slot);
	?>

	@foreach($slot as $s)
	<div class="text-center" style="width:{{$column_width}}%;">
		<div class="session-header">
			@if(!empty($s->meeting_space_id)){{$s->meetingSpace->meeting_space_name}}@endif
			{{$s->title}}
			@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
			<button data-href="#" id="{{$s->id}}" type="button" class="btn new-session btn-success btn-xs" data-toggle="modal" data-target="#add_session"><i class="glyphicon glyphicon-plus "></i></button>
			<a href="{{ route('events.scheduleBuilder.editTimeSlot', [$event->id, $s->id]) }}"><button type="button" class="btn btn-warning btn-xs" title="Edit Time Slot"><i class="glyphicon glyphicon-pencil"></i></button></a>
			<button data-href="{{ route('events.scheduleBuilder.deleteTimeSlot', [$event->id, $s->id]) }}" type="button" class="btn btn-danger btn-xs" title="Remove Time Slot" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-trash"></span></button>
			@endif
		</div>

		<!-- [START] slot sessions loop -->
		@foreach($s->sessions()->orderBy('session_start')->get() as $session)
		<?php 
				//calculate session height: 100%/ num of session
				$session_height = 50;
				$contributors =  $session->contributors()->orderBy('type')->get();
				if ($contributors && count($contributors)) {
					$session_height += (count($contributors)-1)*15;
				}
		?>
		<div class="session" style="background: {{$session->type->color}}; height:{{$session_height}}px;">
			<div class="col-xs-1">
				@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
				<a href="{{ route('events.scheduleBuilder.editSession', [$event->id, $session->id]) }}"><button type="button" class="btn btn-warning btn-xs" title="Edit Session"><i class="glyphicon glyphicon-pencil"></i></button></a>
				<button data-href="{{ route('events.scheduleBuilder.deleteSession', [$event->id, $session->id]) }}" type="button" class="btn btn-danger btn-xs" title="Remove Session" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-trash"></span></button>
				@endif
			</div>

			<div class="col-xs-11">

				<div class="col-xs-8"><p>
				<b>{{$session->title}}</b>
				@foreach($session->contributors()->orderBy('type')->get() as $c)
					<br/><b>{{$c->pivot->type}}</b> - {{$c->badge_name}}
				@endforeach</p></div>

				<div class="col-xs-2"><p>@if($session->show_start)
					{{ date("H:i",strtotime($session->session_start)) }}
				@endif</p></div>

				<div class="col-xs-2"><p>@if($session->show_end)
					{{ date("H:i",strtotime($session->session_end)) }}
				@endif</p></div>

			</div>
		</div>
		@endforeach <!-- [END] slot sessions loop -->
	</div>
	@endforeach <!-- [END] slots  -->
</div>
<br/>
@endforeach <!-- [END] time slot row loop  -->
@endif


<!----- END DAY SCHEDULE -->


							</div>
                            <?php $first = false; ?>
						@endforeach	
					</div> <!-- @END tab-content -->
				</div><!-- @END TABS -->
				
				@endif
			@endif
	      <br/>
	</div>
</div>
</section>


<div class="modal fade" id="add_session" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		@include('admin.events.schedules.createSession')
	</div>
</div>

@include('admin.partials.deleteConfirmation')

@if ($event->schedule_version == 0)
<div     class="modal fade" id="add_time_slot" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="width: 60%;">
		@include('admin.events.schedules.createTimeSlot')
	</div>
</div>
@endif


<script type="text/javascript">
/*
Set slot-day inside the creation-form based on current selected day
*/
function setDateOnTimeSlot(day)
{	
	$("#add_time_slot #timeslotdatelabel").text(day);
	$("#add_time_slot #timeslotdatevalue").val(day);
	
}
</script>

<script type="text/javascript">
$(function() {
	$("#event_name").autocomplete({
        source: function( request, response ) {
        $.ajax({
            url: "{!! route('events.geteventslist') !!}",
            dataType: "json",
            data: {term: request.term},
            success: function(data) {
                        response($.map(data, function(item) {
                        return {
                            label: item.title,
                            id: item.id,
                            };
                    }));
                }
            });
        },
        minLength: 2,
        select: function(event, ui) {
            $('#event_name_id').val(ui.item.id);
        }
    });
});
</script>

<script type="text/javascript">
$(document).ready(function () {
    $('.schedule_visibility').bind('change', function () {
        $.ajax({
        	url: "{!! route('events.scheduleBuilder.updateVisibility', $event->id) !!}",
        	type: "POST",
        	data: {"visible_on_web": $('#visible_on_web').is(':checked'),
        	"visible_on_reg": $('#visible_on_reg').is(':checked'),
			"mandatory_slots": $('#mandatory_slots').is(':checked'),
        	"_token": "{{ csrf_token() }}"},
        	success: function(){
	        	// success
	        	//location.reload();
	    }});
	});
});

$(document).ready(function () {
    $('.schedule_visibility_app').bind('change', function () {
        $.ajax({
        	url: "{!! route('events.scheduleBuilder.updateVisibilityApp', $event->id) !!}",
        	type: "POST",
        	data: {"visible_on_app": $('#visible_on_app').is(':checked'),
        	"_token": "{{ csrf_token() }}"},
        	success: function(){
	        	// success
	        	//location.reload();
	    }});
	});
});



$('.new-session').click(function(){
	$('#event_schedule_slot_id').val(this.id);
	@if ($event->schedule_version == 1)
	$("#slot_range").html("Time must be between "+$(this).data('days_range'));
	@endif
});

$(function() {
    // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        // save the latest tab; use cookies if you like 'em better:
        localStorage.setItem('lastTab', $(this).attr('href'));
    });

    // go to the latest tab, if it exists:
    var lastTab = localStorage.getItem('lastTab');
    if (lastTab) {
        $('[href="' + lastTab + '"]').tab('show');
    }
});

// update schedule version
$("#schedule_version").bind('change', function() {
    $.ajax({
		type: "post",
		data: {schedule_version: this.value},
		url: "{{route('events.scheduleBuilder.updateScheduleVersion', $event->id)}}",
		success: function(resp) {
		    location.reload();
		}
	});
});
</script>

@endsection
