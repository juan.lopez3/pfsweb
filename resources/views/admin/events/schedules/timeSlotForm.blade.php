<?php 

//echo "<pre>";var_dump(array_keys(get_defined_vars()));die;?>

@if ($event->schedule_version == 10)
	<div class="row">
		<div class="col-xs-4 col-sm-offset-1">
			<div class="form-group">
				{!! Form::hidden('start_date', null) !!}
				{!! Form::label('start', 'Start Time', ['class' => 'control-label']) !!}
				{!! Form::text('start', null, ['class'=>'form-control time-mask', 'required']) !!}
				<p class="help-block">Time: 24h</p>
			</div>
			<div class="checkbox checkbox-styled">
				<label>
					{!! Form::checkbox('show_start', 1) !!}
					Show start time
				</label>
			</div>
		</div>
		<div class="col-xs-4 col-xs-offset-2">
			<div class="form-group">
				{!! Form::hidden('end_date', null) !!}
				{!! Form::label('end', 'End Time', ['class' => 'control-label']) !!}
				{!! Form::text('end', null, ['class'=>'form-control time-mask', 'required']) !!}
				<p class="help-block">Time: 24h</p>
			</div>
			<div class="checkbox checkbox-styled">
				<label>
					{!! Form::checkbox('show_end', 1) !!}
					Show end time
				</label>
			</div>
		</div>

		<div class="col-xs-offset-1 col-sm-4">
			<div class="form-group">
				{!! Form::label('columns', 'Number of columns', ['class' => 'control-label required']) !!}
				{!! Form::input('number', 'columns', null, ['class'=>'form-control', 'required']) !!}
			</div>
		</div>
	</div>
@else
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/jquery-ui/jquery-ui-theme.css')}}" />
<script type="text/javascript" src="{{asset('assets/admin/js/libs/bootstrap-slider/jquery.js')}}"></script>

<div class="card-body">
	<div class="row">
		<div class="col-sm-12 col-sm-12">
			<div class="form-group">
				{!! Form::label('title', 'Block title', ['class' => 'control-label']) !!}
					{!! Form::text('title', null, ['class'=>'form-control', 'required']) !!}
			</div>
		</div>
	</div>
</div>

<div class="card-body">

	<div class="row">
		<div class="col-sm-12">
		
		{!! Form::hidden('start_date', (isset($time_slot) && $time_slot->start_date)?$time_slot->start_date:$event->event_date_from,['id'=>'timeslotdatevalue']) !!}
		{!! Form::hidden('end_date',   (isset($time_slot) && $time_slot->end_date)?$time_slot->end_date:$event->event_date_to) !!}
		<p>Date: <span id="timeslotdatelabel">{{$time_slot->start_date or '' }}</span></p>
		</div>
	</row>

	<div class="row">
		<div class="col-xs-4 col-sm-offset-1">
			<div class="form-group">
				
				{!! Form::label('start', 'Start Time', ['class' => 'control-label']) !!}
				{!! Form::text('start', null, ['class'=>'form-control time-mask', 'required']) !!}
				<p class="help-block">Time: 24h</p>
			</div>
		</div>
		<div class="col-xs-4 col-xs-offset-2">
			<div class="form-group">
				
				{!! Form::label('end', 'End Time', ['class' => 'control-label']) !!}
				{!! Form::text('end', null, ['class'=>'form-control time-mask', 'required']) !!}
				<p class="help-block">Time: 24h</p>
			</div>
		</div>
		  
	</div>
</div>

<div class="row">
	<div class="col-xs-offset-1 col-xs-5">
		<div class="form-group">
			{!! Form::label('meeting_space_id', 'Meeting Space', ['class'=>'control-label']) !!}
			{!! Form::select('meeting_space_id', [''=>'']+$meeting_room_spaces, null, ['class'=>'form-control select2-list', 'required']) !!}
		</div>
	</div>
	<div class="col-xs-offset-1 col-xs-4">
		<div class="form-group">
			{!! Form::label('slot_capacity', 'Capacity', ['class' => 'control-label']) !!}
			{!! Form::input('number', 'slot_capacity', null, ['class'=>'form-control', 'min' => '0', 'required']) !!}
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-3 text-center">
		<div class="checkbox checkbox-styled">
			<label>
				{!! Form::checkbox('bookable', null, 1) !!}
				{!! Form::label('bookable', 'Bookable') !!}
			</label>
		</div>
	</div>
	<div class="col-xs-4 text-center">
		<div class="checkbox checkbox-styled">
			<label>
				{!! Form::checkbox('stop_booking', 1) !!}
				{!! Form::label('stop_booking', 'Stop Booking Now') !!}
			</label>
		</div>
	</div>
	<div class="col-xs-2">
		Notification
	</div>
	<div class="col-xs-2">
		<input type="text" name="notification_level" id="notification_level" readonly style="border:0; color:#f6931f; font-weight:bold;">
		<div id="slider-range-min"></div> 
	</div>
</div>

<br/>

<div class="row">
	<div class="form-group">
		<label class="col-xs-offset-1 col-sm-offset-0 col-xs-2 col-sm-3 control-label">Available to:</label>
		<div class="col-xs-5">
			@if (isset($at_checked))
			@foreach($at_checked as $at)
			<div class="checkbox checkbox-styled">
				<label>
					{!! Form::checkbox('attendee_types[]', $at['id'], $at['checked']) !!}
					{!! Form::label($at['title'], $at['title']) !!}
				</label>
			</div>
			@endforeach
			@else
			@foreach($attendee_types as $at)
			<div class="checkbox checkbox-styled">
				<label>
					{!! Form::checkbox('attendee_types[]', $at->id, 1) !!}
					{!! Form::label($at->title, $at->title) !!}
				</label>
			</div>
			@endforeach
			@endif
			
			<div class="checkbox checkbox-styled">
				<label>
					{!! Form::checkbox('available_chartered', null, 1) !!}
					{!! Form::label('available_chartered', 'Chartered') !!}
				</label>
			</div>
		</div>
		
	</div>
</div>

<br/>

{!! Form::button('Add question', ['class' => 'btn ink-reaction btn-raised btn-sm btn-success', 'id'=>'add_question', 'title'=>'Add question']) !!}
<?php $types = ['text' => 'Text', 'option'=>'Option', 'checkbox'=>'Checkbox']; ?>

<div class="row" id="slot_questions">
	@if(isset($time_slot))
	@foreach($time_slot->questions as $q)
	
	<div class="row">
		<div class="col-lg-5">{!! Form::text('questions[]', $q->question, ['class'=>'form-control', 'placeholder'=>'Question']) !!}</div>
		<div class="col-lg-2">{!! Form::select('types[]', $types, $q->type, ['class'=>'form-control']) !!}</div>
		<div class="col-lg-2">{!! Form::text('answers[]', $q->answers, ['class'=>'form-control', 'placeholder'=>'Answers', 'title'=>'Comma to separate answers']) !!}</div>
		<div class="col-lg-2">{!! Form::text('available[]', $q->available, ['class'=>'form-control', 'placeholder'=>'Registration answ.', 'title'=>'Answer that allows registration']) !!}</div>
		<div class="col-lg-1"><button type="button" class="delete btn btn-danger btn-icon-toggle" title="Delete"><i class="md md-delete"></i></button></div>
	</div>
	@endforeach
	@endif
</div>
<br/>

<script type="text/javascript">

$("#add_question").click(function () {
	
	var html = '<div class="row" style="padding-left: 20px;">'+
					'<div class="col-lg-5"><input type="text" name="questions[]" value="" class="form-control" placeholder="Question" /></div>'+
					'<div class="col-lg-2"><select name="types[]" class="form-control"><option value="text">Text</option><option value="option">Option</option><option value="checkbox">Checkbox</option></select></div>'+
					'<div class="col-lg-2"><input type="text" name="answers[]" value="" class="form-control" placeholder="Answers" title="Comma to separate answers" /><span class="help-block">Coma to separate</span></div>'+
					'<div class="col-lg-2"><input type="text" name="available[]" value="" class="form-control" placeholder="Registration answ." title="Answer that allows registration" /></div>'+
					'<div class="col-lg-1"><button type="button" class="delete btn btn-danger btn-icon-toggle" title="Delete"><i class="md md-delete"></i></button></div>'+
				'</div></div>';
				
	$("#slot_questions").append(html);
});

$("body").on("click", ".delete", function (e) {
	$(this).parent("div").parent("div").remove();
});

$(function() {
	$( "#slider-range-min" ).slider({
		range: "min",
		value: @if(isset($time_slot)) {{$time_slot->notification_level}} @else 0 @endif,
		min: 0,
		max: 100,
		step: 5,
		slide: function( event, ui ) {
			$( "#notification_level" ).val( ui.value + "%" );
		}
	});
	
	$( "#notification_level" ).val( $( "#slider-range-min" ).slider( "value" ) + "%" );
});
</script>
@endif