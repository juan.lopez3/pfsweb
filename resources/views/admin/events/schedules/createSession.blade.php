<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/jquery-ui/jquery-ui-theme.css')}}" />
<script type="text/javascript" src="{{asset('assets/admin/js/libs/bootstrap-slider/jquery.js')}}"></script>

<div class="section-body">
	<div class="col-md-12">
		<div class="card">
			<div class="card-head style-primary">
				<header>Add Session</header>
			</div>
			
			@include('admin.partials.validationErrors')
			
			{!! Form::open(['route' => ['events.scheduleBuilder.storeSession', $event->id], 'class' => 'form-horizontal form-validate']) !!}
			<div id="slot_range"></div>
			@include('admin.events.schedules.sessionForm')
			
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					{!! Form::submit('CREATE SESSION', ['class' => 'btn btn-flat btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>