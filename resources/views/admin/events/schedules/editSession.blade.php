@extends('admin.layouts.default')

@section('title')
Edit Schedule Session
@endsection

@section('content')

<section>
<div class="section-body">
	<div class="col-lg-offset-1 col-sm-12 col-md-9">
		<div class="card">
			<div class="card-head style-primary">
				<header>Edit Schedule Session</header>
			</div>
			
			{!! Form::model($slot_session, ['route' => ['events.scheduleBuilder.updateSession', $event->id, $slot_session->id], 'method' => 'PATCH', 'class' => 'form form-validate']) !!}
			<div class="card-body">
				 @include('admin.events.schedules.sessionForm')
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					<a href="{{ URL::previous() }}"><button type="button" class="btn btn-flat">Cancel</button></a>
					{!! Form::submit('UPDATE SESSION', ['class' => 'btn btn-flat btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
</section>
@endsection
