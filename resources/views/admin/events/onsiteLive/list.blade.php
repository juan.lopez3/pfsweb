@extends('admin.layouts.default')

@section('title')
{{$event->title}} On Site Live
@endsection

@section('content')
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/jquery-ui/jquery-ui-theme.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/bootstrap-datepicker/datepicker3.css?1424887858')}}" />
<script type="text/javascript" src="{{asset('assets/admin/js/libs/moment/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/libs/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/libs/bootstrap-datepicker/bootstrap-datetimepicker.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/combodate-1.0.7/combodate.js')}}"></script>


<section>
<div class="section-header">
	<div class="row">
		<div class="col-xs-9"><span class="event-head">{{$event->title}}</span> - {{ date("j F Y",strtotime($event->event_date_from)) }}
			<br/><span class="glyphicon glyphicon-play text-success "></span><span class="event-head"> On Site Live</span>
		</div>
		<div class="col-xs-3 text-right">@include('admin.partials.eventActions')</div>
	</div>
</div>
<br/>
<div class="section-body">
	<div class="card">
		<div class="card-body">
			
			@include('admin.partials.validationErrors')
			@include('admin.partials.alerts')

            <!--Show only if event is live or past -->
            @if ($event->event_date_from <= date("Y-m-d"))
            @endif
            <div class="row">
                <div class="col-xs-12 text-right">
                    <!-- Trigger the modal with a button -->
                    <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#import-attendance">Import recorded attendance</button>
                </div>
            </div>
            <!-- Modal -->
            <div id="import-attendance" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    {!! Form::open(['route' => ['events.onsite.importRecordedAttendance', $event->id], 'class' => 'form form-validate', 'files'=>true]) !!}
                    <div class="modal-content">
                        <div class="modal-header bg-primary">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">IMPORT RECORDED ATTENDANCE</h4>
                        </div>
                        <div class="modal-body">
                            <p>This section allows importing attendance.</p>
                            <b>Accepted file types:</b> csv <br />
                            <b>First line is for headers</b> <br />
                            <b>File format:</b> first column registration id, second column session id / event attendance (yes/no)

                            <div class="form-group">
                                <label>Select file</label>
                                {!! Form::file('file', ['class'=>'form-control']) !!}
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            {!! Form::submit('Import', ['class' => 'btn ink-reaction btn-raised btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

			<div class="row">
			    <div class="col-xs-8">
				    <button class="btn btn-success btn-raised" id="new_registration-button">NEW / EDIT REGISTRATION</button>
				</div>
				<div class="col-xs-4 text-right"><b>Running attendee(s) live: </b>{{$event->atendees()->where('registration_status_id', config('registrationstatus.booked'))->whereNotNull('checkin_time')->whereNull('checkout_time')->count()}}</div>
			</div>
			
			<div class="row" id="new_registration" style="display:none;">
				<br/>
				{!! Form::label('new_reg_name', 'Attendee Name') !!}
				{!! Form::text('new_reg_name', null, ['class'=>'form-control', 'id' => 'new_reg_name', 'placeholder'=>'Enter name, surname or PIN']) !!}
			</div>
			<br/>
			
			<div class="row">
				<div class="col-md-6">
					<div class="card">
						<div class="card-head style-primary">
							<header>CHECK-IN</header>
						</div>
						<div class="card-body">
							{!! Form::label('checkin_name', 'Attendee Name') !!}
							{!! Form::text('checkin_name', null, ['class'=>'form-control', 'id' => 'checkin', 'placeholder'=>'Enter name, surname or PIN']) !!}
							
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="card">
						<div class="card-head style-primary">
							<header>CHECK-OUT</header>
						</div>
						<div class="card-body">
							{!! Form::label('checkout_name', 'Attendee Name') !!}
							{!! Form::text('checkout_name', null, ['class'=>'form-control', 'id' => 'checkout', 'placeholder'=>'Enter name, surname or PIN']) !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
    <script type="text/javascript">
        $(document).ready(function() {
            $('#atendees_list').dataTable({
                "pageLength": 10
            });
        } );
    </script>
	
	<div class="card">
        <div class="card-body">
            <div class="section-header"><h3>Attendees list</h3></div>
           
            {!! Form::open(['route' => ['events.onsite.updateListAttendance', $event->id], 'class' => 'form form-validate', 'method'=>'put']) !!}
            <div class="row text-right" style="margin-bottom: 15px;">
                <button type="button" class="btn btn-sm btn-success btn-raised" data-toggle="modal" data-target="#checkin-all">CHECK-IN ALL</button>
            </div>
            <div class="row text-right" style="margin-bottom: 15px;">
                <button type="button" class="btn btn-sm btn-success btn-raised" data-toggle="modal" data-target="#checkout-all">CHECK-OUT ALL</button>
            </div>
            
    <div id="checkin-all" class="fade modal" role="dialog">
        <div class="modal-dialog">
            <div class="section-body">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-head style-primary">
                            <header><span id="checkin-header"></span> CHECK-IN ALL</header>
                        </div>
                        
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <label for="checkin_all">Check in time</label>
                                    <input id="checkin_all" data-format="DD-MM-YYYY HH:mm" data-template="DD / MM / YYYY HH : mm" name="checkin_all" value="" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="card-actionbar">
                            <div class="card-actionbar-row">
                                <div class="text-right col-xs-12">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    {!! Form::submit('CHECK-IN ALL', ['class' => 'btn btn-success btn-raised','name' => 'command','value' => 'check-in']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>            

    <div id="checkout-all" class="fade modal" role="dialog">
        <div class="modal-dialog">
            <div class="section-body">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-head style-primary">
                            <header><span id="checkin-header"></span> CHECK-OUT ALL</header>
                        </div>
                        
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <label for="checkout_all">Check out time</label>
                                    <input id="checkout_all" data-format="DD-MM-YYYY HH:mm" data-template="DD / MM / YYYY HH : mm" name="checkout_all" value="" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="card-actionbar">
                            <div class="card-actionbar-row">
                                <div class="text-right col-xs-12">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    {!! Form::submit('CHECK-OUT ALL', ['class' => 'btn btn-success btn-raised','name' => 'command','value' => 'check-out']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

            <table id="atendees_list" class="table table-striped table-responsive" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Date Registered</th>
                        <th>PIN</th>
                        <th>Name</th>
                        <th>Email Address</th>
                        <th>Member</th>
                        <th>Role</th>
                        <th>Feedback</th>
                        <th>CHECK-IN</th>
                        <th>CHECK-OUT</th>
                    </tr>
                </thead>

                <tfoot>
                    <tr>
                        <th>Date Registered</th>
                        <th>PIN</th>
                        <th>Name</th>
                        <th>Email Address</th>
                        <th>Member</th>
                        <th>Role</th>
                        <th>Feedback</th>
                        <th>CHECK-IN</th>
                        <th>CHECK-OUT</th>
                    </tr>
                </tfoot>

                <tbody>
                    @foreach ($attendees as $attendee)
                    <tr>
                        <td>{{ date("d/m/Y H:i:s",strtotime($attendee->pivot->created_at)) }}</td>
                        <td>{{$attendee->pin}}</td>
                        <td>{{$attendee->first_name}} {{$attendee->last_name}}</td>
                        <td><a href="mailto:{{$attendee->email}}">{{$attendee->email}}</a></td>
                        <td>
                            @if($attendee->role_id == role('member')) Y @else N @endif
                        </td>
                        <td>{{$attendee->role->title}}</td>
                        <td>
                            @if(!sizeof($attendee->feedbackAnswers()->where('event_id', $event->id)->get()))
                                <a href="{{route('events.feedbacks.leave', [$event->id, $attendee->id])}}">Submit</a>
                            @else
                                <a href="{{route('events.feedbacks.leave', [$event->id, $attendee->id])}}">Edit</a>
                            @endif</td>
                        </td>
                        <td id="ch-in-{{$attendee->pivot->id}}">
                            @if(!empty($attendee->pivot->checkin_time))CH-IN {{ date("d/m/Y H:i",strtotime($attendee->pivot->checkin_time)) }}
                            @else
                                <label>
                                    <input type="checkbox" name="check_in_attendee" class="checkin_attendee" value="{{$attendee->pivot->id}}" />
                                </label>
                            @endif
                        </td>
                        <td id="ch-out-{{$attendee->pivot->id}}">
                            @if(!empty($attendee->pivot->checkout_time))CH-OUT {{ date("d/m/Y H:i",strtotime($attendee->pivot->checkout_time)) }}
                            @else
                                <label>
                                    <input type="checkbox" name="check_out_attendee" class="checkout_attendee" value="{{$attendee->pivot->id}}" />
                                </label>
                            @endif

                            <input type="hidden" name="check_out_list[]" value="{{$attendee->pivot->id}}" />
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {!! Form::close() !!}
        </div>
   </div>
</div>
</section>

<!-- checkin -->
@include('admin.events.onsiteLive.checkin')
<!-- checkout -->
@include('admin.events.onsiteLive.checkout')

<script type="text/javascript">
$(function() {
    $("#checkin_all").val(moment().format('DD-MM-YYYY HH:mm'));
    $('#checkin_all').combodate({
            minYear: 2016,
            maxYear: <?=date("Y");?>,
            minuteStep: 1
    });
    $("#checkout_all").val(moment().format('DD-MM-YYYY HH:mm'));
    $('#checkout_all').combodate({
            minYear: 2016,
            maxYear: <?=date("Y");?>,
            minuteStep: 1
    });
    
	// CHECKIN IF CLICKED IN THE LIST
    $('#atendees_list').on('click', 'input[class="checkin_attendee"]', function() {
	    var attendee_id = this.value;
	    $.ajax({
            url: "{{ route('events.onsite.updateAttendeeCheckin') }}",
            data: {"attendee_id": attendee_id, "_token": "{{ csrf_token() }}"},
            method: "PUT",
            success: function (resp) {
                if (resp != false) {
                    $("#ch-in-"+attendee_id).html("CH-IN "+resp);
                } else {
                    alert("Error updating check in time. Check in time was not saved");
                }
                
            },
            error: function() {
                alert("Error updating check in time. Check in time was not saved");
            }
        });
	});
	
	// CHECKOUT IF CLICKED IN THE LIST
    $('#atendees_list').on('click', 'input[class="checkout_attendee"]', function() {
        var attendee_id = this.value;
        $.ajax({
            url: "{{ route('events.onsite.updateAttendeeCheckout') }}",
            data: {"attendee_id": attendee_id, "_token": "{{ csrf_token() }}"},
            method: "PUT",
            success: function (resp) {
                if (resp != false) {
                    $("#ch-out-"+attendee_id).html("CH-OUT "+resp);
                } else {
                    alert("Error updating check OUT time. Check out time was not saved");
                }
                
            },
            error: function() {
                alert("Error updating check OUT time. Check out time was not saved");
            }
        });
    });
    
	// CHECK IN
	$("#checkin").autocomplete({
        source: function( request, response ) {
        $.ajax({
            url: "{!! route('events.onsite.attendees', $event->id) !!}",
            dataType: "json",
            data: {term: request.term},
            success: function(data) {
                        response($.map(data, function(item) {
                        return {
                            label: item.pin + ' ' + item.title + ' ' + item.first_name + ' ' + item.last_name,
                            id: item.pivot.id,
                            };
                    }));
                }
            });
        },
        minLength: 3,
        select: function(event, ui) {
            
            // get info about the user
            $.ajax({
	            url: "{!! route('users.getAttendeeProfile') !!}",
	            dataType: "json",
	            data: {event_attendee_id: ui.item.id},
	            success: function(data) {
	            	$("#checkin-time").val(moment().format('DD-MM-YYYY HH:mm'));
	            	$('#checkin-time').combodate({
	            		    minYear: 2016,
						    maxYear: <?=date("Y");?>,
						    minuteStep: 1
	            	});
	            	
	            	$("#checkin-attendee_id").val(ui.item.id);
	            	$("#checkin-header").html(data.title+' '+data.first_name+' '+data.last_name)
	            	$("#checkin-pin").html(data.pin)
	            	$("#checkin-badge_name").html(data.badge_name)
	            	$("#checkin-title").html(data.title)
	            	$("#checkin-first_name").html(data.first_name)
	            	$("#checkin-last_name").html(data.last_name)
	            	$("#checkin-email").html(data.email)
	            	$("#checkin-company").html(data.company)
	            	
	            	//set link to clear checkin
	            	$("#clear-checkin").attr("href", "{{route('events.onsite.clearcheckin')}}?attendee_id="+ui.item.id);
	            	
	            	$('#checkin-modal').modal('show');
				},
				error: function(resp) {
					alert('Error getting attendee profile details');
				}
            });
        }
    });
    
    // CHECK OUT
    $("#checkout").autocomplete({
        source: function( request, response ) {
        $.ajax({
            url: "{!! route('events.onsite.attendees', $event->id) !!}",
            dataType: "json",
            data: {term: request.term},
            success: function(data) {
                        response($.map(data, function(item) {
                        return {
                            label: item.pin + ' ' + item.title + ' ' + item.first_name + ' ' + item.last_name,
                            id: item.pivot.id,
                            };
                    }));
                }
            });
        },
        minLength: 3,
        select: function(event, ui) {
            
            // get info about the user
            $.ajax({
	            url: "{!! route('users.getAttendeeProfile') !!}",
	            dataType: "json",
	            data: {event_attendee_id: ui.item.id},
	            success: function(data) {
	            	$("#checkout-time").val(moment().format('DD-MM-YYYY HH:mm'));
	            	$('#checkout-time').combodate({
	            		    minYear: 2016,
						    maxYear: <?=date("Y");?>,
						    minuteStep: 1
	            	});
	            	
	            	$("#checkout-attendee_id").val(ui.item.id);
	            	$("#checkout-header").html(data.title+' '+data.first_name+' '+data.last_name)
	            	$("#checkout-pin").html(data.pin)
	            	$("#checkout-badge_name").html(data.badge_name)
	            	$("#checkout-title").html(data.title)
	            	$("#checkout-first_name").html(data.first_name)
	            	$("#checkout-last_name").html(data.last_name)
	            	$("#checkout-email").html(data.email)
	            	$("#checkout-company").html(data.company)
	            	
	            	$("#clear-checkout").attr("href", "{{route('events.onsite.clearcheckout')}}?attendee_id="+ui.item.id);
	            	
	            	$('#checkout-modal').modal('show');
				},
				error: function(resp) {
					alert('Error getting attendee profile details');
				}
            });
        }
    });

	// NEW EDIT REGISTRATION
	$( "#new_registration-button" ).click(function() {
		$( "#new_registration" ).toggle( "slow", function() {});
	});
	
	$("#new_reg_name").autocomplete({
        source: function( request, response ) {
        $.ajax({
            url: "{!! route('users.getList') !!}",
            dataType: "json",
            data: {term: request.term},
            success: function(data) {
                        response($.map(data, function(item) {
                        return {
                            label: item.pin + ' ' + item.title + ' ' + item.first_name + ' ' + item.last_name,
                            id: item.id,
                            };
                    }));
                }
            });
        },
        minLength: 3,
        select: function(event, ui) {
        	window.location.href = "{{route('events.onsite.registration', $event->id)}}?user_id="+ui.item.id;
        }
	});
});
</script>

@endsection
