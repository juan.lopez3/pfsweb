@extends('admin.layouts.default')

@section('title')
{{$event->title}} On Site Live Registration
@endsection

@section('content')
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/jquery-ui/jquery-ui-theme.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/bootstrap-datepicker/datepicker3.css?1424887858')}}" />
<script type="text/javascript" src="{{asset('assets/admin/js/libs/moment/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/libs/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/libs/bootstrap-datepicker/bootstrap-datetimepicker.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/combodate-1.0.7/combodate.js')}}"></script>


<section>
<div class="section-header">
	<div class="row">
		<div class="col-xs-9"><span class="event-head">{{$event->title}}</span> - {{ date("j F Y",strtotime($event->event_date_from)) }}
			<br/><span class="glyphicon glyphicon-play text-success "></span><span class="event-head"> On Site Live Registration</span>
		</div>
		<div class="col-xs-3 text-right">@include('admin.partials.eventActions')</div>
	</div>
</div>
<br/>
<div class="section-body">
	<div class="card">
		<div class="card-body">

			@include('admin.partials.validationErrors')
			@include('admin.partials.alerts')

			<div class="col-md-12">
				<div class="card">
					<div class="card-head style-primary">
						<header>@if($attended)Registration found! Edit {{$attendee->title}} {{$attendee->first_name}} {{$attendee->last_name}} registration @else Registration not found! Create new {{$attendee->title}} {{$attendee->first_name}} {{$attendee->last_name}} registration @endif</header>
					</div>
					<div class="card-body">
						{!! Form::model($attendee, ['route' => ['events.onsite.store', $event->id, $attendee->id], 'method' => 'POST', 'class' => 'form form-validation']) !!}

						<div class="row">
							<div class="col-sm-6">
								<h3>Contact details</h3><br/>
								<dl>
									<dt>Badge Name</dt>
									<dd>{{$attendee->badge_name}}</dd>

									<dt>Title</dt>
									<dd>{{$attendee->title}}</dd>

									<dt>First Name</dt>
									<dd>{{$attendee->first_name}}</dd>

									<dt>Last Name</dt>
									<dd>{{$attendee->last_name}}</dd>

									<dt>Email</dt>
									<dd>{{$attendee->email}}</dd>

									<dt>Phone</dt>
									<dd>{{$attendee->phone}}</dd>

									<dt>Mobile</dt>
									<dd>{{$attendee->mobile}}</dd>

									<dt>Postcode</dt>
									<dd>{{$attendee->postcode}}</dd>

									@if(!empty($attendee->company))
									<dt>Company</dt>
									<dd>{{$attendee->company}}</dd>
									@endif

									<dt>Membership PIN</dt>
									<dd>{{$attendee->pin}}</dd>

									<dt>PFS Designation</dt>
									<dd>{{$attendee->pfs_class}}</dd>
								</dl>
							</div>
						</div>

                        <?php
                        $user_attendee_types = $attendee->attendeeTypes->lists('id');
                        $bookable_slots_count = 0;
                        ?>

						<div class="row">
							<h3>Select Sessions</h3><br/>

							@if($event->main_schedule_visible_reg)

								<table id="sessions" class="table table-striped table-responsive head" cellspacing="0" width="100%">
									<thead class="events">
										<tr>
											<th>Time</th>
											<th>Session</th>
											<th>Book</th>
											@if($attended)
											<th>Status</th>
											@endif
										</tr>
									</thead>

									<tbody>
										@foreach($event->scheduleTimeSlots()->where('bookable', 1)->orderBy('start')->get() as $s)
										<?php

											if($s->stop_booking) continue;

											$available = false;
											$slot_attendee_types = $s->attendeeTypes->lists('id');

											foreach($user_attendee_types as $uat) {

												// if attendee has at least 1 attende type assigned to slot attendee types
												if(in_array($uat, $slot_attendee_types)) {
													$available = true;
													break;
												}
											}

											//check chartered status if attendee type not found
											if(!$available)
												if($s->available_chartered && $attendee->chartered) $available = true;

											// user doesn't have attendee type that is related with slots then don't give option to register for that slot
											if(!$available) continue;

											$bookable_slots_count++;

											$full = ($s->slot_capacity <= $s->attendees->count() || $s->stop_booking) ? true : false;

											$checked = "";
											if(isset($booked_slots) && in_array($s->id, $booked_slots)) $checked = "checked";
										?>

										<tr id="slot_{{$s->id}}">
											<td>
												{{date("H:i", strtotime($s->start))}} - {{date("H:i", strtotime($s->end))}}
												@if($full)
												<span class="full-slot-book">Waiting list</span>
												@endif
											</td>
											<td>{{$s->title}}</td>
											<td>
												<div class="form-group">
												<div class="checkbox checkbox-styled">
													<label>
														<input type="checkbox" {{$checked}} value="{{$s->id}}" name="sessions[]">
													</label>
												</div>
												</div>
											</td>
											@if($attended)

												<td>
												@if(sizeof($booked_slots_full->where('event_schedule_slot_id', $s->id)->first()) > 0)
												{{$booked_slots_full->where('event_schedule_slot_id', $s->id)->first()->status->title}}
												@else
												Not booked
												@endif
												</td>
											@endif
										</tr>
										@endforeach
									</tbody>
								</table>
								@if($bookable_slots_count == 0)
									<div class="alert alert-warning">

                                        This event is available just for selected attendee types. This attendee is not eligible to book any slots!
                                    </div>
									<div style="display: none;">
										<input type="text" name="empty-sessions" required />
									</div>
								@endif
							@endif

							<div class="card-actionbar">
								<div class="card-actionbar-row">
									<a href="{{ URL::previous() }}"><button type="button" class="btn">Close</button></a>
									@if($bookable_slots_count)
    									@if($attended)
    										{!! Form::submit('UPDATE REGISTRATION', ['class' => 'btn btn-primary btn-raised']) !!}
    									@else
    										{!! Form::submit('CREATE REGISTRATION', ['class' => 'btn btn-primary btn-raised']) !!}
    									@endif
    							    @endif
								</div>
							</div>
						</div>

						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</section>

@endsection
