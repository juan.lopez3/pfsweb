<div id="checkout-modal" class="fade modal" role="dialog">
	<div class="modal-dialog">
		<div class="section-body">
			<div class="col-md-12">
				<div class="card">
					<div class="card-head style-primary">
						<header><span id="checkout-header"></span> CHECK-OUT</header>
					</div>
					{!! Form::open(['route' => ['events.onsite.checkout', $event->id], 'class' => 'form form-validate']) !!}
					{!! Form::hidden('checkout-attendee_id', null, ['id'=>'checkout-attendee_id']) !!}
					<div class="card-body">
						 <div class="row">
						 	<div class="col-xs-12">
							 	<dl>
							 		<dt>PFS Membership Number</dt>
									<dd><span id="checkout-pin"></span></dd>
									<dt>Badge Name</dt>
									<dd><span id="checkout-badge_name"></span></dd>
									<dt>Title</dt>
									<dd><span id="checkout-title"></span></dd>
									<dt>First Name</dt>
									<dd><span id="checkout-first_name"></span></dd>
									<dt>Surname</dt>
									<dd><span id="checkout-last_name"></span></dd>
									<dt>Email</dt>
									<dd><span id="checkout-email"></span></dd>
									<dt>Company</dt>
									<dd><span id="checkout-company"></span></dd>
									
									<dt>CHECK-OUT TIME</dt>
									<dd>
										<input id="checkout-time" data-format="DD-MM-YYYY HH:mm" data-template="DD / MM / YYYY HH : mm" name="checkout-time" value="" type="text">
									</dd>
								</dl>
							</div>
						 </div>
					</div>
					<div class="card-actionbar">
						<div class="card-actionbar-row">
                            <div class="text-left col-xs-4">
                                <a id="clear-checkout" href=""><button type="button" class="btn btn-flat btn-warning">Clear Check-out</button></a>
                            </div>
                            
                            <div class="text-right col-xs-8">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                {!! Form::submit('CHECK-OUT', ['class' => 'btn btn-success btn-raised']) !!}
                            </div>
                        </div>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>