<div id="checkin-modal" class="fade modal" role="dialog">
	<div class="modal-dialog">
		<div class="section-body">
			<div class="col-md-12">
				<div class="card">
					<div class="card-head style-primary">
						<header><span id="checkin-header"></span> CHECK-IN</header>
					</div>
					{!! Form::open(['route' => ['events.onsite.checkin', $event->id], 'class' => 'form form-validate']) !!}
					{!! Form::hidden('checkin-attendee_id', null, ['id'=>'checkin-attendee_id']) !!}
					<div class="card-body">
						 <div class="row">
						 	<div class="col-xs-12">
							 	<dl>
							 		<dt>PFS Membership Number</dt>
									<dd><span id="checkin-pin"></span></dd>
									<dt>Badge Name</dt>
									<dd><span id="checkin-badge_name"></span></dd>
									<dt>Title</dt>
									<dd><span id="checkin-title"></span></dd>
									<dt>First Name</dt>
									<dd><span id="checkin-first_name"></span></dd>
									<dt>Surname</dt>
									<dd><span id="checkin-last_name"></span></dd>
									<dt>Email</dt>
									<dd><span id="checkin-email"></span></dd>
									<dt>Company</dt>
									<dd><span id="checkin-company"></span></dd>
									
									<dt>CHECK-IN TIME</dt>
									<dd>
										<input id="checkin-time" data-format="DD-MM-YYYY HH:mm" data-template="DD / MM / YYYY HH : mm" name="checkin-time" value="" type="text">
									</dd>
								</dl>
							</div>
						 </div>
					</div>
					<div class="card-actionbar">
						<div class="card-actionbar-row">
						    <div class="text-left col-xs-4">
						        <a id="clear-checkin" href=""><button type="button" class="btn btn-flat btn-warning">Clear Check-in</button></a>
						    </div>
							
							<div class="text-right col-xs-8">
							    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							    {!! Form::submit('CHECK-IN', ['class' => 'btn btn-success btn-raised']) !!}
							</div>
						</div>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>