@if (isset($site_tabs[4]) && $site_tabs[4]['checked'])
<script>
$(function() {
	
	$('#sortable').sortable({
		'containment': 'parent',
		'revert': true,
		'handle': '.handle',
		update: function(event, ui) {
			$.post('{{ route('events.faqs.reposition') }}',
				$("#list").serialize(), function(data) {
					
					if(!data.success) {
						alert('Whoops, something went wrong :/');
					}
				}, 'json');
		}
	});
});
</script>

<div class="tab-pane" id="faqs">
	<header>
		<h3 class="opacity-75">FAQs</h3>
	</header>
	
	@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
	{!! Form::open(['route' => ['events.faqs.import', $event->id], 'class' => 'form form-validate']) !!}
	<div class="row">
		<div class="col-sm-3">
			<div class="form-group floating-label">
				{!! Form::text('faq_old_event_name', null, ['class'=>'form-control', 'id' => 'faq_previous_events', 'required']) !!}
				{!! Form::label('faq_old_event_name', 'Old Event Name') !!}
				{!! Form::hidden('old_event_id', null, array('id' => 'old_event_id')) !!}
			</div>
		</div>
		<div class="col-sm-1">
			{!! Form::submit('Import Now', ['class' => 'btn ink-reaction btn-sm btn-primary']) !!}
		</div>
	</div>
	<hr class="ruler">
	<div class="row">
		<button data-href="#" type="button" class="btn ink-reaction btn-raised btn-sm btn-success" data-toggle="modal" data-target="#add_faq">Add New FAQ</button>
		<button data-href="{{ route('events.faqs.deleteAll', $event->id) }}" type="button" class="btn ink-reaction btn-raised btn-sm btn-danger" title="Delete" data-toggle="modal" data-target="#confirm-delete">Delete All</button>
	</div>
	{!! Form::close() !!}
	@endif
	<br/>
	<form id="list">
		<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
		<div id="sortable">
		@foreach($event->eventFaqs()->orderBy('position')->get() as $index => $faq)
		<div class="card panel">
			<div class="card-head collapsed style-primary-bright" data-toggle="collapse" data-parent="#accordion{{$index}}" data-target="#accordion{{$index}}-{{$index}}">
				<header><div class="handle"><input type="hidden" name="item[]" value="{{$faq->id}}" /></div> {{ $faq->question }}</header>
				<div class="tools">
					<button data-href="{{route('events.faqs.delete', ['id'=>$event->id, 'id2'=>$faq->id])}}" type="button" class="btn btn-icon-toggle"" title="Delete" data-toggle="modal" data-target="#confirm-delete"><i class="md md-delete"></i></button>
					<a href="{{ route('events.faqs.edit', $faq->id) }}"><button type="button" class="btn btn-icon-toggle" title="Edit"><span class="glyphicon glyphicon-pencil"></span></button></a>
					<a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a>
				</div>
			</div>
			<div id="accordion{{$index}}-{{$index}}" class="collapse">
				<div class="card-body"><p>{!!$faq->answer!!}</p>
				</div>
			</div>
		</div>
		@endforeach
		</div>
	</form>
	@include('admin.events._tabNotes', array('tab_id' => 5, 'notes' => $event->tabs()->where('tab_id', 5)->first()->notes()->where('event_id', $event->id)->orderby('created_at', 'DESC')->get()))

<div class="modal fade" id="add_faq" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		@include('admin.events.faqs.create')
	</div>
</div>

<script type="text/javascript">
$(function() {
	$("#faq_previous_events").autocomplete({
        source: function( request, response ) {
        $.ajax({
            url: "{!! route('events.geteventslist') !!}",
            dataType: "json",
            data: {term: request.term},
            success: function(data) {
                        response($.map(data, function(item) {
                        return {
                            label: item.title,
                            id: item.id,
                            };
                    }));
                }
            });
        },
        minLength: 2,
        select: function(event, ui) {
            $('#old_event_id').val(ui.item.id);
        }
    });
});
</script>

</div>
@endif