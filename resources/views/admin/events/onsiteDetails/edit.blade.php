@extends('admin.layouts.default')

@section('title')
{{$event->title}} On Site Details
@endsection

@section('content')

<section>
<div class="section-header">
	<div class="row">
		<div class="col-xs-9"><span class="event-head">{{$event->title}}</span> - {{ date("j F, Y",strtotime($event->event_date_from)) }}<br/>
			<span class="glyphicon glyphicon-th text-accent-light "></span><span class="event-head"> On-Site Details</span></div>
		<div class="col-xs-3 text-right">@include('admin.partials.eventActions')</div>
	</div>
</div>
<br/>
<div class="section-body">	
	
	<div class="card">
		<div class="card-body">
			{!! Form::model($event->onSiteDetails, ['route' => ['events.onsiteDetails.update', $event->id], 'id'=>'onsite_form', 'class' => 'form-horizontal form-validate']) !!}
			@include('admin.partials.validationErrors')
			<header>
				<h3 class="opacity-75">Venue Details</h3>
			</header>
			@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					{!! Form::submit('Update', ['class' => 'btn ink-reaction btn-raised btn-primary']) !!}
				</div>
			</div>
			@endif
			@if(!empty($event->venue_id))
				<div class="row">
					<div class="col-sm-6">Venue Name: {{$event->venue->name}}</div>
					<div class="col-sm-6">Venue Email: {{$event->venue->email}}</div>
				</div>
				
				<div class="row">
					<div class="col-sm-6">Venue Address: {{$event->venue->address}}</div>
					<div class="col-sm-6">Venue Contact Tel.: {{$event->venue->venue_phone}}</div>
				</div>
				
				@if(sizeof($event->venue->contactPerson))
				<div class="row">
					<div class="col-sm-6">Contact person: {{$event->venue->contactPerson->first_name}} {{$event->venue->contactPerson->last_name}}</div>
					<div class="col-sm-6">Contact person phone: {{$event->venue->contactPerson->mobile}}</div>
				</div>
				@endif
			@else
				<i>Venue is not assigned for this event.</i>
			@endif
			
			<hr class="ruler">
			
			<header>
				<h3 class="opacity-75">Venue Person</h3>
			</header>
			
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						{!! Form::label('duty_manager_name', 'Duty manager on the day', ['class' => 'col-sm-5 control-label']) !!}
						<div class="col-sm-7">
							{!! Form::text('duty_manager_name', null, ['class'=>'form-control']) !!}
						</div>
					</div>
				</div>
				
				<div class="col-sm-6">
					<div class="form-group">
						{!! Form::label('duty_manager_number', 'Duty manager contact number', ['class' => 'col-sm-5 control-label']) !!}
						<div class="col-sm-7">
							{!! Form::text('duty_manager_number', null, ['class'=>'form-control']) !!}
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						{!! Form::label('av_technician_name', 'AV Technician name (or company name)', ['class' => 'col-sm-5 control-label']) !!}
						<div class="col-sm-7">
							{!! Form::text('av_technician_name', null, ['class'=>'form-control']) !!}
						</div>
					</div>
				</div>
				
				<div class="col-sm-6">
					<div class="form-group">
						{!! Form::label('av_technician_number', 'AV Technician phone number (or number of supplier) ', ['class' => 'col-sm-5 control-label']) !!}
						<div class="col-sm-7">
							{!! Form::text('av_technician_number', null, ['class'=>'form-control']) !!}
						</div>
					</div>
				</div>
			</div>
			
			<hr class="ruler">
			
			<header>
				<h3 class="opacity-75">Event Staff</h3>
			</header>
			
			<div class="row">
				<div class="col-xs-6">
					<h4>Host Person</h4>
					<div class="col-xs-11 col-sm-11 col-md-8">{!! Form::select('host_id', [''=>'']+$hosts, null, ['class'=>'form-control select2-list', 'id'=>'host_id']) !!}</div>
					<div class="col-xs-1 col-md-1">{!! Form::button('+', ['class' => 'btn ink-reaction btn-raised btn-sm btn-success', 'id'=>'add_host']) !!}</div>
				</div>
				
				<div class="col-xs-6">
					<h4>TFI Contact</h4>
					<div class="col-xs-11 col-sm-11 col-md-8">{!! Form::select('tfi_contact_id', [''=>'']+$tfi_staff, null, ['class'=>'form-control select2-list', 'id'=>'tfi_contact_id']) !!}</div>
					<div class="col-xs-1 col-md-1">{!! Form::button('+', ['class' => 'btn ink-reaction btn-raised btn-sm btn-success', 'id'=>'add_tfi_contact']) !!}</div>
				</div>
			</div>
			<br/>
			<div class="row">
				<div class="col-xs-6">
					<div id="hosts">
						@foreach($event->atendeesByRole(role('host')) as $host)
						<div class="row">
							<div class="col-xs-4"><input type="hidden" name="hosts_id[]" value="{{$host->id}}" />{{$host->first_name}} {{$host->last_name}}</div>
							<div class="col-xs-4">{{$host->email}}</div>
							<div class="col-xs-1"><button type="button" class="delete btn btn-danger btn-icon-toggle" title="Delete"><i class="md md-delete"></i></button></div>
						</div>
						@endforeach
					</div>
				</div>
				
				<div class="col-xs-6">
					<div id="tfi_contacts">
						@foreach($event->atendeesByRole(role('event_manager'), role('event_coordinator')) as $tfi)
						<div class="row">
							<div class="col-xs-4"><input type="hidden" name="tfi_contacts_id[]" value="{{$tfi->id}}" />{{$tfi->first_name}} {{$tfi->last_name}}</div>
							<div class="col-xs-4">{{$tfi->email}}</div>
							<div class="col-xs-1"><button type="button" class="delete btn btn-danger btn-icon-toggle" title="Delete"><i class="md md-delete"></i></button></div>
						</div>
						@endforeach
					</div>
				</div>
			</div>

			<hr class="ruler">
			
			<header>
				<h3 class="opacity-75">Meeting Space Set-up</h3>
			</header>
			<?php $event_attendees = $event->atendees()->where('registration_status_id', config('registrationstatus.booked'))->count(); ?>
			
			@if(!empty($event->venue_id))
			<?php
				$layout_styles = array(0 => 'N/a', 1=>'Cabaret', 2=>'Theatre');
			?>
			
			<div class="row">
				<div class="col-xs-8 col-sm-3">{!! Form::select('meeting_room_id', [''=>'Select meeting room']+$event->venue->rooms->lists('room_name', 'id'), null, ['class'=>'form-control', 'id'=>'meeting_room_id']) !!}</div>
				<div class="col-xs-3 col-sm-2">{!! Form::select('layout_style_id', $layout_styles, null, ['class'=>'form-control', 'id'=>'layout_style_id']) !!}</div>
				<div class="col-xs-1">{!! Form::button('+', ['class' => 'btn ink-reaction btn-raised btn-sm btn-success', 'id'=>'add_meeting_space']) !!}</div>
			</div>
			<br/>
			<div class="row">
				<div class="col-xs-2">Meeting Room Name</div>
				<div class="col-xs-2">Meeting Space Name</div>
				<div class="col-xs-1">Layout</div>
				<div class="col-xs-1">Max Capacity for layout style</div>
				<div class="col-xs-1">Attendees</div>
				<div class="col-xs-1">Max num. of tables in room</div>
				<div class="col-xs-1">Number of Tables</div>
				<div class="col-xs-2">Set up notes</div>
			</div>
			
			<div class="row">
				<div id="meeting_spaces">
					
					@foreach($event->venueSpaces as $space)
					
					<div class="row">
						<div class="col-xs-2"><input type="hidden" name="venue_space_id[]" value="{{$space->pivot->id}}" /><input type="hidden" name="venue_room_id[]" value="{{$space->id}}" /><input type="text" value="{{$space->room_name}}" readonly name="meeting_room_name[]" class="form-control" /></div>
						<div class="col-xs-2"><input type="text" value="{{$space->pivot->meeting_space_name}}" name="meeting_space_name[]" class="form-control" required /></div>
						<div class="col-xs-1">{!! Form::select('space_layout_style[]', $layout_styles, $space->pivot->layout_style, ['class'=>'form-control']) !!}</div>
						<div class="col-xs-1 text-center">@if($space->pivot->layout_style == 1) {{$space->cabaret_capacity}} @elseif($space->pivot->layout_style == 2) {{$space->theatre_capacity}}  @endif</div>
						<div class="col-xs-1 text-center">{{$event_attendees}}</div>
						<div class="col-xs-1 text-center">{{$space->max_tables}}</div>
						<div class="col-xs-1"><input type="number" value="{{$space->pivot->num_of_tables}}" name="num_of_tables[]" class="form-control" min="0"/></div>
						<div class="col-xs-2"><input type="text" value="{{$space->pivot->setup_note}}" name="setup_note[]" class="form-control" /></div>
						<div class="col-xs-1"><button type="button" class="btn btn-danger btn-icon-toggle delete_meeting_space" id="{{$space->pivot->id}}" title="Delete"><i class="md md-delete"></i></button></div>
					</div>
					@endforeach
				</div>
			</div>
			
			@else
				<i>Venue is not assigned for this event.</i>
			@endif
			
			<hr class="ruler">
			
			<header>
				<h3 class="opacity-75">Equipment Required</h3>
			</header>
			<div class="row">
				<div class="col-xs-11 col-sm-6 col-md-4">{!! Form::select('technology_id', [''=>'']+$technologies, null, ['class'=>'form-control select2-list', 'id'=>'technology_id']) !!}</div>
				<div class="col-xs-1 col-md-1">{!! Form::button('+', ['class' => 'btn ink-reaction btn-raised btn-sm btn-success', 'id'=>'add_equipment']) !!}</div>
			</div>
			<br/>
			
			<div class="row">
				<div class="col-sm-8">
					<div class="col-xs-2">Quantity</div>
					<div class="col-xs-4">Name</div>
					<div class="col-xs-4">Provided by</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-sm-8">
					<div id="equipment">
						@foreach($event->technologies as $technology)
						<div class="row">
							<div class="col-xs-2"><input type="number" value="{{$technology->pivot->quantity}}" name="quantity[]" class="form-control" min="0"/> </div>
							<div class="col-xs-4"><input type="hidden" name="technology_id[]" value="{{$technology->id}}" /><input type="text" value="{{$technology->title}}" readonly name="technology_title[]" class="form-control" required /></div>
							<div class="col-xs-4"><input type="text" value="{{$technology->pivot->provided_by}}" name="provided_by[]" class="form-control" /></div>
							<div class="col-xs-1"><button type="button" class="delete btn btn-danger btn-icon-toggle" title="Delete"><i class="md md-delete"></i></button></div>
						</div>
						@endforeach
					</div>
				</div>
				<div class="col-sm-4">
					{!! Form::label('venue_note', 'Notes for Venue (Appears on confirmation)', ['class'=>'control-label']) !!}
					{!! Form::textarea('venue_note', null, ['class' => 'form-control control-3-rows', 'maxlength'=>1000]) !!}
				</div>
			</div>
			
			@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					{!! Form::submit('Update', ['class' => 'btn ink-reaction btn-raised btn-primary']) !!}
				</div>
			</div>
			@endif
			{!! Form::close() !!}
		</div>
	</div>
</div>
</section>

@include('admin.partials.readonly', ['form_id' => 'onsite_form'])

<script type="text/javascript">

$("#add_meeting_space").click(function () {
	
	$.ajax({
        url: "{!! route('events.onsiteDetails.getMeetingRoomInfo') !!}",
        method: "POST",
        data: {  
		   "_token": "{{ csrf_token() }}",
		   "meeting_room_id": $('#meeting_room_id').val(),
		   "layout_style_id": $('#layout_style_id').val()
		},
        
        success: function(data) {
        	// @TODO atendees for room from attendees registered to session
			var html = '<div class="row">'+
					'<div class="col-xs-2"><input type="hidden" name="venue_room_id[]" value="'+data.id+'" /><input type="text" value="'+data.room_name+'" readonly name="meeting_room_name[]" class="form-control" /></div>'+
					'<div class="col-xs-2"><input type="text" value="" name="meeting_space_name[]" class="form-control" required /></div>'+
					'<div class="col-xs-1"><select name="space_layout_style[]" class="form-control" readonly><option value="'+$('#layout_style_id').val()+'">'+$('#layout_style_id option:selected').text()+'</option></select></div>'+
					'<div class="col-xs-1 text-center">'+data.layout_capacity+'</div>'+
					'<div class="col-xs-1 text-center">{{$event_attendees}}</div>'+
					'<div class="col-xs-1 text-center">'+data.max_tables+'</div>'+
					'<div class="col-xs-1"><input type="number" value="" name="num_of_tables[]" class="form-control" min="0"/></div>'+
					'<div class="col-xs-2"><input type="text" value="" name="setup_note[]" class="form-control" /></div>'+
					'<div class="col-xs-1"><button type="button" class="delete btn btn-danger btn-icon-toggle" title="Delete"><i class="md md-delete"></i></button></div>'+
				'</div>';
	
			$("#meeting_spaces").append(html);
		}
	});
});


$("#add_equipment").click(function () {
	
	var html = '<div class="row">'+
					'<div class="col-xs-2"><input type="number" value="" name="quantity[]" class="form-control" min="0"/> </div>'+
					'<div class="col-xs-4"><input type="hidden" name="technology_id[]" value="'+$('#technology_id').val()+'" /><input type="text" value="'+$('#technology_id option:selected').text()+'" readonly name="technology_title[]" class="form-control" required /></div>'+
					'<div class="col-xs-4"><input type="text" value="" name="provided_by[]" class="form-control" /></div>'+
					'<div class="col-xs-1"><button type="button" class="delete btn btn-danger btn-icon-toggle" title="Delete"><i class="md md-delete"></i></button></div>'+
				'</div>';
	
	$("#equipment").append(html);
});

$("#add_host").click(function () {
	
	$.ajax({
        url: "{!! route('events.onsiteDetails.getPersonInfo') !!}",
        method: "POST",
        data: {  
		   "_token": "{{ csrf_token() }}",
		   "user_id": $('#host_id').val(),
		},
        
        success: function(data) {
        	
			var html = '<div class="row">'+
								'<div class="col-xs-4"><input type="hidden" name="hosts_id[]" value="'+data.id+'" />'+data.first_name+' '+data.last_name+'</div>'+
								'<div class="col-xs-4">'+data.email+'</div>'+
								'<div class="col-xs-1"><button type="button" class="delete btn btn-danger btn-icon-toggle" title="Delete"><i class="md md-delete"></i></button></div>'+
							'</div>';
				
			$("#hosts").append(html);
		}
	});
});

$("#add_tfi_contact").click(function () {
	
	$.ajax({
        url: "{!! route('events.onsiteDetails.getPersonInfo') !!}",
        method: "POST",
        data: {  
		   "_token": "{{ csrf_token() }}",
		   "user_id": $('#tfi_contact_id').val(),
		},
        
        success: function(data) {
        	
			var html = '<div class="row">'+
								'<div class="col-xs-4"><input type="hidden" name="tfi_contacts_id[]" value="'+data.id+'" />'+data.first_name+' '+data.last_name+'</div>'+
								'<div class="col-xs-4">'+data.email+'</div>'+
								'<div class="col-xs-1"><button type="button" class="delete btn btn-danger btn-icon-toggle" title="Delete"><i class="md md-delete"></i></button></div>'+
							'</div>';
				
			$("#tfi_contacts").append(html);
		}
	});
});

$("body").on("click", ".delete", function (e) {
	
	$(this).parent("div").parent("div").remove();
});

$("body").on("click", ".delete_meeting_space", function (e) {
	var localThis = this;
	
	$.ajax({
		url: "{!! route('events.onsiteDetails.checkSessions', $event->id) !!}",
		data: {meeting_space_id: localThis.id},
		success: function(response){
			if(response) {
				alert("At least one session is assigned to this meeting space. Deleting is not allowed.")
			} else {
				$(localThis).parent("div").parent("div").remove();
			}
		},
		error: function(){
			alert('Error checking if meeting space is assigned to sessions');
		}
	});
});
</script>

@endsection
