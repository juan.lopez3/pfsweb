@extends('admin.layouts.default')

@section('title')
Edit Event
@endsection

@section('content')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/jquery-ui/jquery-ui-theme.css')}}" />

<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/js/libs/jquery-fileupload/uploadfile.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/wizard/wizard.css')}}" />

<script type="text/javascript" src="{{asset('assets/admin/js/libs/moment/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/libs/bootstrap-slider/bootstrap-slider.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/libs/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/core/demo/Demo.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/core/demo/DemoFormComponents.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/libs/jquery-fileupload/jquery.uploadfile.min.js')}}"></script>

<script src="{{asset('assets/admin/js/jquery.cookie.js')}}"></script>

<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/bootstrap.css?1422792965')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/materialadmin.css?1425466319')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/font-awesome.min.css?1422529194')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/material-design-iconic-font.min.css?1421434286')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/select2/select2.css?1424887856')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/multi-select/multi-select.css?1424887857')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/bootstrap-datepicker/datepicker3.css?1424887858')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/jquery-ui/jquery-ui-theme.css?1423393666')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/bootstrap-tagsinput/bootstrap-tagsinput.css?1424887862')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/typeahead/typeahead.css?1424887863')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/dropzone/dropzone-theme.css?1424887864')}}" />

<script type="text/javascript">
	$(function(){
        $('#color').colorpicker();
    });
</script>

<script src="{{asset('assets/admin/js/core/demo/DemoFormComponents.js')}}"></script>

<section>
	<div class="section-header">
		<div class="row">
			<div class="col-xs-9"><span class="event-head">{{$event->title}}</span> - {{ date("j F, Y",strtotime($event->event_date_from)) }}<br/>
				<span class="glyphicon glyphicon-cog text-warning"></span> <span class="event-head"> Edit Event</span>
			</div>
			<div class="col-xs-3 text-right">@include('admin.partials.eventActions')
				<br/><a target="_blank" href="{{route('events.view', $event->slug)}}"><button class="btn ink-reaction btn-sm btn-raised btn-info">Preview</button></a>
				
			</div>
		</div>
	</div>
	<br/>
	<div class="section-body">
		<div class="card">
			<div class="card-head">
				<ul class="nav nav-tabs" data-toggle="tabs" id="tabs">
					@foreach($event->tabs()->orderBy('id')->get() as $tab)
						<li id="lid_{{$tab->slug}}"><a id="_{{$tab->slug}}" class="tab" href="#{{$tab->slug}}">{{$tab->title}}</a></li>
					@endforeach
				</ul>
			</div>
			
			<div class="card-body tab-content">
				@include('admin.events._tab1')
				@include('admin.events._tab2')
				@include('admin.events._tab3')
				@include('admin.events._tab4')
				@include('admin.events._tab5')
				@include('admin.events._tab6')
				@include('admin.events._tab7')
				@include('admin.events._tab8')
				@include('admin.events._tab9')
			</div>
			<!--
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					<a href="{{ URL::previous() }}"><button type="button" class="btn btn-flat">Cancel</button></a>
				</div>
			</div>
			-->
		</div>
	</div>	
</section>

@include('admin.partials.deleteConfirmation')

<script type="text/javascript">
	
$(document).ready(function(){
	
	$('.add_note').bind('click', function (event) {
        
        if ($('#note_' + this.id).val()) {
        	
        	var id = this.id;
        	
        	$.ajax({
				url: "{{ route('events.notes.addNote', $event->id) }}",
				type: "POST",
				data: {
					"_token": "{{ csrf_token() }}",
					"note": $('#note_' + id).val(),
					"tab_id": parseInt(id)
				},
				success: function () {
					$('#note_' + id).val('');
					location.reload();
				}
			});
        } else {
        	alert('Please enter the note!');
        }
    });
    
    var event_id = {{$event->id}};
    
    // tab handler. Set last_event_id, last_active_tab cookies. show same tab after submission. If no cookie on that event, show event_settings_tab
    $('.tab').bind('click', function(){
    	$.cookie('last_event_id', event_id, { expires: 0.01 });
    	$.cookie('last_active_tab', this.id.substring(1), { expires: 0.01 });
    });
    
    
    // cookie is of last event is active show it else show event_settigns
    
    var last_event_id = $.cookie('last_event_id');
    var last_tab_id = $.cookie('last_active_tab'); 
    
    if(last_tab_id !== 'undefined' && event_id == last_event_id) {
    	$('#lid_'+last_tab_id).addClass('active');
    	$('#'+last_tab_id).addClass('active');
    } else {
    	$('#lid_settings').addClass('active');
    	$('#settings').addClass('active');
    }
});

</script>
@endsection
