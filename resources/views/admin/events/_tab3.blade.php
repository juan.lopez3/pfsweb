@if (isset($site_tabs[2]) && $site_tabs[2]['checked'])
<div class="tab-pane" id="contributors">
	<header>
		<h3 class="opacity-75">Contributors</h3>
	</header>
	@include('admin.partials.validationErrors')
	
	@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
	{!! Form::open(['route' => ['events.contributors.store', $event->id], 'class' => 'form-horizontal form-validate']) !!}
	{!! Form::hidden('contributor_id', null, ['id'=>'contributor_id']) !!}
	
	<div class="row">
		<div class="col-xs-4 col-md-2">
			<div class="form-group text-center">
				{!! Form::label('contributor_name', 'Contributor Name') !!}
			</div>
		</div>
		<div class="col-xs-4 col-md-3">
			<div class="form-group">
				{!! Form::text('contributor_name', null, ['class'=>'form-control', 'id'=>'contributor_name', 'required']) !!}
			</div>
		</div>
		<div class="col-xs-2 text-center">
            {!! Form::submit('Add Contributor', ['class' => 'btn ink-reaction btn-sm btn-raised btn-primary']) !!}
        </div>
	</div>
	{!! Form::close() !!}
	@endif
	<br/>
	<div class="row">
		
		@foreach($event->atendeesByRoles(role('member'), role('non_member')) as $atendee)

		@if(isset($atendee->bio) && $atendee->bio !==  "" && !is_null($atendee->bio))
		
			<div class="col-md-4 col-sm-6 col-lg-3">
				<div class="card">
					<div class="card-head card-head-xs style-info">
						<header>{{$atendee->badge_name}}</header>
					</div>
					
					<div class="card-body">
						<div class="holder">
							@if (empty($atendee->profile_image))
								{!! HTML::image('uploads/no_image.png','', ['class'=>'img-responsive']) !!}
							@else
								{!! HTML::image('uploads/profile_images/'.$atendee->profile_image,'', ['class'=>'img-responsive']) !!}
							@endif
						</div>
						
						<b>Role:</b> {{$atendee->role->title}}<br/>
						@foreach($atendee->subRoles as $sr)
							<i>{{ $sr->title }}</i><br/>
						@endforeach
						<b>Email:</b> {{$atendee->email}}
						
						{!! Form::select('comitee', ['Non committee', 'Comittee'], $atendee->pivot->comitee, ['class' => 'form-control comitee', 'id' => 'attendee-'.$atendee->pivot->id]) !!}
						
						<blockquote><small>{{$atendee->bio}}</small></blockquote>
					</div>
					
					@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
					<div class="card-actionbar-row">
						<button data-href="{{ route('events.contributors.delete', [$event->id, $atendee->id]) }}" id="{{$atendee->id}}" type="button" class="btn ink-reaction btn-raised btn-xs btn-danger check_delete" title="Delete" data-toggle="modal" data-target="#confirm-delete">Delete Contributor</button>
					</div>
					@endif
				</div>
			</div>
		@endif
		@endforeach
	</div>
	
	
	@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
	<hr class="ruler-xxl">
	<header>
		<h3 class="opacity-75">Import Contributors</h3>
	</header>
	{!! Form::open(['route' => ['events.contributors.copy', $event->id], 'class' => 'form form-validate']) !!}
	<div class="row">
		<div class="col-xs-4 col-md-2">
			<div class="form-group text-right">
				{!! Form::label('contributors_old_event_name', 'Previous Event Name') !!}
			</div>
		</div>
		<div class="col-xs-4 col-md-2">
			<div class="form-group">
				{!! Form::text('contributors_old_event_name', null, ['class'=>'form-control', 'id' => 'contributors_previous_events', 'required']) !!}
				{!! Form::hidden('old_c_event_id', null, array('id' => 'old_c_event_id')) !!}
			</div>
		</div>
		<div class="col-xs-1">
			{!! Form::submit('Import Now', ['class' => 'btn ink-reaction btn-sm btn-primary']) !!}
		</div>
	</div>
	{!! Form::close() !!}
	@endif
	
	@include('admin.events._tabNotes', array('tab_id' => 3, 'notes' => $event->tabs()->where('tab_id', 3)->first()->notes()->where('event_id', $event->id)->orderby('created_at', 'DESC')->get()))

<script type="text/javascript">
$(".comitee").bind("change", function(){
    ids = this.id.split("-");
    
    $.ajax({
        url: "{!! route('events.contributors.toggleComitee') !!}",
        type: "POST",
        data: {
             "_token": "{{ csrf_token() }}",
               "event_attendee_id": ids[1], 
               "comitee": $("#"+this.id).val(),
        },
        success: function () {
            //location.reload();
        }
    });
});

//before delete check if contributor is assigned to any sessions
$('.check_delete').click(function(){
	
	$.ajax({
		url: "{!!route('events.contributors.checkContributor', $event->id) !!}",
		dataType: "json",
		data: {user_id: this.id},
		success: function(response){
			
			if(response) {
				alert('This contributor is assigned to schedule session(s). Deleting contributor will delete contributor in session(s)')
			}
		},
		error: function(){
			
			alert('Error checking if contributor is assigned to any sessions.');
		}
	});
});

$('.delete_all').click(function(){
	
	$.ajax({
		url: "{!!route('events.contributors.checkContributors', $event->id) !!}",
		dataType: "json",
		success: function(response){
			
			if(response) {
				if(response) {
					alert('At least 1 contributor is assigned to schedule session(s). Deleting contributor will delete contributor in session(s)')
				}
			}
		},
		error: function(){
			
			alert('Error checking if contributor is assigned to any sessions.');
		}
	});
});


$(function() {
	$("#contributors_previous_events").autocomplete({
        source: function( request, response ) {
        $.ajax({
            url: "{!! route('events.geteventslist') !!}",
            dataType: "json",
            data: {term: request.term},
            success: function(data) {
                        response($.map(data, function(item) {
                        return {
                            label: item.title,
                            id: item.id,
                            };
                    }));
                }
            });
        },
        minLength: 2,
        select: function(event, ui) {
            $('#old_c_event_id').val(ui.item.id);
        }
    });
});
</script>

<script type="text/javascript">
$(function() {
	$("#contributor_name").autocomplete({
        source: function( request, response ) {
        $.ajax({
            url: "{!! route('events.contributors.getlist') !!}",
            dataType: "json",
            data: {term: request.term},
            success: function(data) {
                        response($.map(data, function(item) {
                        return {
                            label: item.badge_name,
                            id: item.id,
                            };
                    }));
                }
            });
        },
        minLength: 2,
        select: function(event, ui) {
            $('#contributor_id').val(ui.item.id);
        }
    });
});
</script>

</div>

@endif