@extends('admin.layouts.default')

@section('title')
{{$event->title}} Costs
@endsection

@section('content')

<section>
<div class="section-header">
	<div class="row">
		<div class="col-xs-9"><span class="event-head">{{$event->title}}</span> - {{ date("j F Y",strtotime($event->event_date_from)) }}
			<br/><span class="glyphicon glyphicon-gbp text-info "></span><span class="event-head"> Costs</span>
		</div>
		<div class="col-xs-3 text-right">@include('admin.partials.eventActions')</div>
	</div>
</div>
<br/>
<div class="section-body">
	<div class="card">
		<div class="card-body">
			<div class="row">
			{!! Form::model($event, ['route' => ['events.costs.update', $event->id], 'class' => 'form form-validate']) !!}
			@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
			<p>{!! Form::button('Add Cost', ['class' => 'btn ink-reaction btn-raised btn-sm btn-success', 'id'=>'add']) !!}</p>
			@endif
				<div class="form-group">
					<div class="row">	
						<div class="col-sm-3 col-xs-3">Name</div>
						<div class="col-sm-1 col-xs-2">Venue Cost</div>
						<div class="col-sm-2 col-xs-2">Amount (£)</div>
						<div class="col-sm-1 col-xs-2">Per Person</div>
						<div class="col-sm-1 col-xs-2">Atendees</div>
						<div class="col-sm-1 col-xs-2">Contingency no.</div>
						<div class="col-sm-2 col-xs-2">Total (£)</div>
					</div>
					
					<div id="items">
						<?php
							$total_venue_cost = 0.00;
							$total_cost = 0.00;
						?>
						@foreach($event->costs as $key => $cost)
						<?php 
							$line_total = $cost->amount;
							if($cost->per_person == 1) $line_total = ($atendees + $cost->contingency_no) * $cost->amount;
							
							if($cost->venue_cost == 1) $total_venue_cost += $line_total;
							$total_cost += $line_total;
						?>
						
						<div class="row">
							<div class="col-sm-3 col-xs-3"><input type="text" value="{{$cost->title}}" name="title[]" class="form-control" required /></div>
							<div class="col-sm-1 col-xs-2"><div class="checkbox"><label><input type="checkbox" valuea="1" @if($cost->venue_cost == 1) checked  @endif name="venue_cost[{{$key}}]"></label></div></div>
							<div class="col-sm-2 col-xs-2">
								<input type="text" value="{{number_format($cost->amount, 2)}}" name="amount[]" class="form-control" required data-rule-number="true" />
							</div>
							<div class="col-sm-1 col-xs-2"><div class="checkbox"><label><input type="checkbox" value="1" @if($cost->per_person == 1) checked  @endif name="per_person[{{$key}}]"></label></div></div>
							<div class="col-sm-1 col-xs-2">@if($cost->per_person == 1) {{$atendees}}  @endif</div>
							<div class="col-sm-1 col-xs-2"><input type="number" value="{{$cost->contingency_no}}" name="contingency_no[]" class="form-control" min="0"/> </div>
							<div class="col-sm-2 col-xs-2"><input type="text" value="{{number_format($line_total, 2)}}" name="total[]" readonly class="form-control" /></div>
							<div class="col-xs-1"><button type="button" class="delete btn btn-danger btn-icon-toggle" title="Delete"><i class="md md-delete"></i></button></div>
						</div>
						{!! Form::hidden('counter', $key+1, ['id'=>'counter']) !!}
						@endforeach
						
					</div>
				</div>
			</div>

			<div class="card-actionbar">
				<div class="card-actionbar-row">
					@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
					{!! Form::submit('Calculate Total Costs', ['class' => 'btn ink-reaction btn-raised btn-primary']) !!}
					@endif
				</div>
			</div>
			{!! Form::close() !!}
			<hr />
			<div class="section-header">
				<ol class="breadcrumb">
					<li class="active">Totals</li>
				</ol>
			</div>
			
			
			<div class="row">
				<div class="col-xs-4 col-md-2">
					<div class="form-group text-center">
						{!! Form::label('event_venue_total', 'Total Venue Costs(£):') !!}
					</div>
				</div>
				<div class="col-xs-4 col-md-3">
					<div class="form-group">
						{!! Form::text('event_venue_total', number_format($total_venue_cost, 2), ['class'=>'form-control', 'readonly', 'id'=>'event_venue_total']) !!}
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-xs-4 col-md-2">
					<div class="form-group text-center">
						{!! Form::label('event_total', 'Total Costs(£):') !!}
					</div>
				</div>
				<div class="col-xs-4 col-md-3">
					<div class="form-group">
						{!! Form::text('event_total', number_format($total_cost, 2), ['class'=>'form-control', 'readonly', 'id'=>'event_total']) !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</section>



<script type="text/javascript">
$("#add").click(function () {
	
	var html = '<div class="row">'+
					'<div class="col-sm-3 col-xs-3"><input type="text" value="" name="title[]" class="form-control" required /></div>'+
					'<div class="col-sm-1 col-xs-2"><div class="checkbox"><label><input type="checkbox" value="1" name="venue_cost['+$('#counter').val()+']"></label></div></div>'+
					'<div class="col-sm-2 col-xs-2"><input type="text" value="" name="amount[]" class="form-control" required /></div>'+
					'<div class="col-sm-1 col-xs-2"><div class="checkbox"><label><input type="checkbox" value="1" name="per_person['+$('#counter').val()+']"></label></div></div>'+
					'<div class="col-sm-1 col-xs-2">1</div>'+
					'<div class="col-sm-1 col-xs-2"><input type="number" value="" name="contingency_no[]" class="form-control" min="0"/> </div>'+
					'<div class="col-sm-2 col-xs-2"><input type="text" value="" name="total[]" readonly class="form-control" /></div>'+
					'<div class="col-xs-1"><button type="button" class="delete btn btn-danger btn-icon-toggle" title="Delete"><i class="md md-delete"></i></button></div>'+
				'</div>';
	
	$("#items").append(html);
	
	$('#counter').val(parseInt($('#counter').val())+1);
	
	//$(".form-control.gbp-mask").inputmask('£ 999.999.999,99', {numericInput: true, rightAlignNumerics: false});
});

$("body").on("click", ".delete", function (e) {
	$(this).parent("div").parent("div").remove();
});
</script>

@endsection
