@if (isset($site_tabs[8]) && $site_tabs[8]['checked'])
<div class="tab-pane" id="gallery">
    <header>
        <h3 class="opacity-75">Gallery</h3>
    </header>
    
    <div class="row">
        <div class="col-md-6">
            {!! Form::label('image', 'Upload gallery image') !!}
            <br/>
            
            <div id="gallery_image_upload">Upload</div>
            <div id="status"></div>
        </div>
    </div>
    
    <div class="row" style="margin-top: 15px;">
        @foreach ($event->gallery as $image)
        <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="holder">
                <div class="overlay overlay-default">
                    
                    <button data-href="{{ route('events.deleteGallery', [$event->id, $image->id]) }}" type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#confirm-delete">Delete</button>
                </div>
                {!! HTML::image('uploads/gallery/'.$image->image,'', ['class'=>'img-responsive']) !!}
            </div>
        </div>
        @endforeach
    </div>
    
    <script type="text/javascript">
    // background image upload
    var settings = {
        url: "{!! route('events.uploadGallery') !!}",
        formData: {  
           "_token": "{{ csrf_token() }}",
           "event_id": "{{ $event->id }}"
        },
        dragDrop:true,
        maxFileSize: 500000,
        fileName: "myfile",
        allowedTypes:"jpg,png,gif,bmp,jpeg",    
        returnType:"json",
         onSuccess:function(files,data,xhr)
        {
           //alert((data));
           location.reload();
        },
        showDelete:false
    }
    var uploadObj = $("#gallery_image_upload").uploadFile(settings);
    
    </script>
    
</div>
@endif