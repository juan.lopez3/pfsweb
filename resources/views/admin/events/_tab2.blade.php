@if (isset($site_tabs[1]) && $site_tabs[1]['checked'])
<div class="tab-pane" id="schedule">
	
	<header>
		<h3 class="opacity-75">Schedule</h3>
	</header>
	
	{!! Form::model($event->schedule, ['route' => ['events.schedule.update', $event->id], 'class' => 'form form-validate']) !!}
	
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<div class="checkbox checkbox-styled">
					<label>
						{!! Form::checkbox('visible_website', true) !!}
						{!! Form::label('visible_website', 'Visible on Website') !!}
					</label>
				</div>
				<div class="checkbox checkbox-styled">
					<label>
						{!! Form::checkbox('visible_registration', true) !!}
						{!! Form::label('visible_registration', 'Visible for Registration') !!}
					</label>
				</div>
			</div>
		</div>
	</div>
	
	<p style="font-style: italic">This is an override schedule which will display instead of that created in the schedule builder.  To show this override schedule you must select 'visible on website' and ensure the Schedule builder schedule is not also ticked as visible on website.</p>
	
	@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
	<div class="card-actionbar">
		<div class="card-actionbar-row">
			{!! Form::submit('Update Schedule', ['class' => 'btn ink-reaction btn-raised btn-primary']) !!}
		</div>
	</div>
	@endif
	
	<!-- BEGIN SUMMERNOTE -->
	<div class="row">
		<div class="col-lg-12">
			<h2 class="text-primary">Schedule Editor</h2>
		</div>
	</div>
	
	<div class="card">
		<div class="card-body no-padding">
			{!! Form::textarea('schedule', null, ['row'=>'3', 'style'=>'height: 200px;', 'id'=>'schedule_editor']) !!}
		</div>
	</div>
	<!-- END SUMMERNOTE -->
	
	{!! Form::close() !!}
	
	@include('admin.events._tabNotes', array('tab_id' => 2, 'notes' => $event->tabs()->where('tab_id', 2)->first()->notes()->where('event_id', $event->id)->orderby('created_at', 'DESC')->get()))
	
</div>
<script type="text/javascript">
$(document).ready(function(){
	$( '#schedule_editor' ).ckeditor();
});
</script>
@endif