<hr class="ruler-xxl">

<div class="row">
	@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
	<div class="col-sm-6">
		<div class="form-group">
			{!! Form::label('note', 'Note', ['class'=>'col-sm-3 control-label']) !!}
			<div class="col-md-10">
				{!! Form::textarea('note', '', ['class' => 'form-control control-5-rows', 'id' => 'note_'.$tab_id, 'maxlength' => 1000]) !!}
			</div>
		</div>
		<button type="button" title="Add Note" class="btn btn-default add_note" id="{{$tab_id}}"><span class="glyphicon glyphicon-chevron-right"></span></button>
	</div>
	@endif
	
	<div class="col-sm-5 notes_history">
		<table class="table table-striped">
			<tbody>
				@foreach($notes as $note)
					<tr><td><b>{{ date("d/m/Y H:i:s",strtotime($note->created_at)) }} {{$note->user->first_name}} {{$note->user->last_name}}</b> - {{$note->note}}</td></tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>