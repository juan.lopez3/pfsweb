@if (isset($site_tabs[6]) && $site_tabs[6]['checked'])
<script>
	$(function() {
		$('#feedback-sortable').sortable({
			'containment': 'parent',
			'revert': true,
			'handle': '.handle',
			update: function(event, ui) {
				$.post('{{ route('events.feedback.reposition') }}',
					$("#feedback-list").serialize(), function(data) {
						if(!data.success) {
							alert('Whoops, something went wrong :/');
						}
					}, 'json');
			}
		});
	});
</script>
<div class="tab-pane" id="feedback">
	<header>
		<h3 class="opacity-75">Feedback</h3>
	</header>
	@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
	{!! Form::open(['route' => ['events.feedback.storeFromTab', $event->id], 'class' => 'form']) !!}
	
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
					{!! Form::text('question', null, ['class'=>'form-control', 'required']) !!}
					{!! Form::label('question', 'Question') !!}
				</div>
			</div>
			<div class="col-sm-2">
				<div class="form-group">
					{!! Form::select('type', [1=>'Rating', 2=>'Checkbox', 3=>'Option box', 4=>'Select box', 5=>'Text'], null, ['class'=>'form-control']) !!}
					{!! Form::label('type', 'Question type') !!}
				</div>
			</div>	
		</div>
		
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
					{!! Form::text('answer', null, ['class'=>'form-control', 'required', 'placeholder' => 'Please separate answers by semicolon']) !!}
					{!! Form::label('answer', 'Answers') !!}
					<p class="help-block">Example: Poor;Average;Excellent</p>
				</div>

                <div class="form-group">
                    {!! Form::label('checkin_required', 'Check-in Required') !!}
                    {!! Form::checkbox('checkin_required', 1, null) !!}
                </div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
					{!! Form::select('session_id', [''=>'Not linked to session']+$sessions, null, ['class'=>'form-control select2-list']) !!}
					{!! Form::label('session_id', 'Link session') !!}
				</div>
			</div>
		</div>
	
	<div class="row">
		{!! Form::submit('Add question', ['class' => 'btn ink-reaction btn-raised btn-success']) !!}
	</div>
	{!! Form::close() !!}
	
	<!-- If has any feedbacks confirmation fot generation required -->
	<div class="row">
	@if (sizeof($event->feedbacks))
        <br/>
        <a data-href="#" type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#feedback_confirmation">Generate feedback from schedule</a>
        <div class="modal fade" id="feedback_confirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="section-body">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-head style-primary">
                                <header>Generate feedback from schedule confirmation</header>
                            </div>
                            
                            <div class="card-body">
                                 This event already has feedback questions. Generating feedback form from schedule may duplicate some questions. Are you sure you want to generate feedback form?
                            </div>
                            <div class="card-actionbar">
                                <div class="card-actionbar-row">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <a href="{{ route('events.feedback.generate', $event->id) }}"><button type="button" class="btn btn-success"><i>Confirm & generate</i></button></a> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    	@else
            <br/>
            <a href="{{ route('events.feedback.generate', $event->id) }}" class="btn btn-sm btn-primary"><i>Generate feedback from schedule</i></a>
    	@endif
    	
    	<a href="{{ route('events.feedback.generatePDF', $event->id) }}" class="btn btn-sm btn-info"><i>Generate PDF form</i></a>
    </div>
	<hr class="ruler-xxl">
	
	@endif
	
	<header>
		<h3 class="opacity-75">Event Feedback</h3>
	</header>
	<form id="feedback-list">
	<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
		<div id="feedback-sortable">
			@foreach ($event->feedbacks()->with('session')->get() as $index => $fb)

				<div class="card panel">
				<div class="card-head collapsed style-primary-bright" data-toggle="collapse" data-parent="#accordion{{$index}}" data-target="#accordion{{$index}}-{{$index}}">
					<header><div class="handle"><input type="hidden" name="item[]" value="{{$fb->id}}" /></div>@if(sizeof($fb->session)){{ date("H:i",strtotime($fb->session->session_start))}} - {{date("H:i",strtotime($fb->session->session_end))}} {{$fb->session->title}} - @endif {{$fb->question}} @if($fb->checkin_required)<i class="text-info fa fa-check" title="Check-in required"></i> @endif</header>
					<div class="tools">
						<button data-href="{{route('events.feedback.delete', ['id'=>$event->id, 'id2'=>$fb->id])}}" type="button" class="btn btn-icon-toggle" title="Delete" data-toggle="modal" data-target="#confirm-delete"><i class="md md-delete"></i></button>
						<a href="{{ route('events.feedback.edit', [$event->id, $fb->id]) }}"><button type="button" class="btn btn-icon-toggle" title="Edit"><span class="glyphicon glyphicon-pencil"></span></button></a>
						<a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a>
					</div>
				</div>
				<div id="accordion{{$index}}-{{$index}}" class="collapse">

					<div class="card-body">
						<!-- rating -->
						@if($fb->type == 1)
							<div id="rootwizard1" class="form-wizard form-wizard-horizontal">
								<div class="form-wizard-nav">
									<div class="progress"><div class="progress-bar progress-bar-primary"></div></div>
									<ul class="nav nav-justified">
									@foreach(explode(";", $fb->answer) as $i => $answ)
										<li><a href="#"><span class="step">{{$i+1}}</span> <span class="title">{{$answ}}</span></a></li>
									@endforeach
									</ul>
								</div>
							</div>
						<!-- checkbox -->
						@elseif($fb->type == 2)
							@foreach(explode(";", $fb->answer) as $answ)
								<div class="checkbox checkbox-styled">
									<label>
										{!! Form::checkbox($answ, true) !!}
										{!! Form::label($answ, $answ) !!}
									</label>
								</div>
							@endforeach
						<!-- optionbox -->
						@elseif($fb->type == 3)
							@foreach(explode(";", $fb->answer) as $answ)

								<div class="radio radio-styled">
									<label>
										<input type="radio" checked="" value="{{$answ}}" name="{{'group'.$fb->id}}">
										<span>{{$answ}}</span>
									</label>
								</div>

							@endforeach
						<!-- selectbox -->
						@elseif($fb->type == 4)
							{!! Form::select('select'.$fb->id, explode(";", $fb->answer), null, ['class'=>'form-control']) !!}
						<!-- textbox -->
						@elseif($fb->type == 5)
							{!! Form::text('text'.$fb->id, null, ['class'=>'form-control', 'required', 'placeholder' => $fb->answer]) !!}
						@endif
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</form>

	@include('admin.events._tabNotes', array('tab_id' => 7, 'notes' => $event->tabs()->where('tab_id', 7)->first()->notes()->where('event_id', $event->id)->orderby('created_at', 'DESC')->get()))
	
</div>

@endif