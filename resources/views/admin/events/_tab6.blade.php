@if (isset($site_tabs[5]) && $site_tabs[5]['checked'])
<script type="text/javascript">
$(document).ready(function() {
	$('#documents_list').dataTable();
} );
</script>

<div class="tab-pane" id="event_materials">
	<header>
		<h3 class="opacity-75">Event Materials</h3>
	</header>
	
	@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
	<div class="row">
		<div class="col-xs-4">
			<div class="form-group">
				<div id="event_materials_upload">Upload</div>
				<div id="status"></div>
			</div>
		</div>
		<div class="col-xs-1">
			<button data-href="{{ route('events.materials.deleteAll', $event->id) }}" type="button" class="btn  btn-flat btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#confirm-delete">Remove All Documents</button>
		</div>
	</div>
	
	<div class="row">
		{!! Form::open(['route'=> ['events.attachFile', $event->id], 'class'=>'form']) !!}
		<div class="col-sm-3">Attach file from central files:</div>
		<div class="col-sm-8">{!! Form::select('file_id', [''=>'Select file to assign']+$files, null, ['class'=>'form-control select2-list', 'required']) !!}</div>
		<div class="col-sm-1">{!! Form::submit('+', ['class'=>'btn btn-sm btn-success']) !!}</div>
		{!! Form::close() !!}
	</div>
	
	<hr class="ruler">
	@endif
	<header>
		<h3 class="opacity-75">Current Documents</h3>
	</header>
	
	<table id="documents_list" class="table table-striped table-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Document Name</th>
				<th>Display Name</th>
				<th>Type</th>
				<th>Size(Mb)</th>
				<th>Date Uploaded</th>
				<th>Date Live</th>
				<th>Actions</th>
			</tr>
		</thead>
	
		<tbody>
			@foreach ($event_materials as $document)
			<tr>
				<td><a href="{{config('app.url')}}uploads/files/{{$document->document_name}}">{{ $document->document_name }}</a></td>
				<td><a href="{{config('app.url')}}uploads/files/{{$document->document_name}}">{{ $document->display_name }}</a></td>
				<td>{{ $document->type}}</td>
				<td>{{ $document->size / 1000000 }}</td>
				<td>{{ date("d m Y H:i:s",strtotime($document->created_at)) }}</td>
				<td>@if(!empty($document->date_live)){{ date("d m Y",strtotime($document->date_live)) }}@endif</td>
				<td>
					@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
						@if($document->getTable() == 'files')
						<button data-href="{{ route('events.detachFile', ['id' => $event->id, 'id2'=> $document->id]) }}" type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-trash"></span></button>
						@else
						<a href="{{ route('events.materials.edit', $document->id) }}"><button type="button" class="btn btn-warning btn-xs" title="Edit"><span class="glyphicon glyphicon-pencil"></span></button></a>
						<button data-href="{{ route('events.materials.delete', ['id' => $event->id, 'id2'=> $document->id]) }}" type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-trash"></span></button>
						@endif
					@endif
				</td>
			</tr>
			@endforeach
			
		</tbody>
	</table>
	
	@include('admin.events._tabNotes', array('tab_id' => 6, 'notes' => $event->tabs()->where('tab_id', 6)->first()->notes()->where('event_id', $event->id)->orderby('created_at', 'DESC')->get()))

<script type="text/javascript">
	$(document).ready(function()
	{
	var settings = {
	    url: "{!! route('events.materials.store') !!}",
	    formData: {  
		   "_token": "{{ csrf_token() }}",
		   "event_id": "{{ $event->id }}" 
		},
	    dragDrop:true,
	    // maxFileSize: 9000000000,
	    fileName: "myfile",
	    allowedTypes:"jpg,png,gif,bmp,jpeg,pdf,doc,docx,xls,xlsx,docx,ppt,pptx",	
	    returnType:"json",
		 onSuccess:function(files,data,xhr)
	    {
		//    alert((data));
		//    console.log(files, data);
	       location.reload();
	    },
	    showDelete:false
	}
	var uploadObj = $("#event_materials_upload").uploadFile(settings);
	
	});
</script>

</div>
@endif