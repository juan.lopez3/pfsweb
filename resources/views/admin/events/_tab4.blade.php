@if (isset($site_tabs[3]) && $site_tabs[3]['checked'])
<div class="tab-pane" id="venue">
	<header>
		<h3 class="opacity-75">Venue</h3>
		@if(empty($event->venue_id))
			<i>No venue is assigned to this event. Please create a new venue or assign previously used venue.</i>
		@endif
	</header>
	<br/>
	
	@include('admin.partials.validationErrors')

@if(empty($event->venue_id))
<!--[START] show create/assign venue form -->
	<div class="col-md-offset-1 col-md-10">
		<div class="card">
			<div class="card-head style-primary">
				<header>Create New Venue</header>
			</div>
			
			{!! Form::open(['route' => ['events.venue.store'], 'id'=>'venue_form', 'class' => 'form-horizontal form-validate']) !!}
			{!! Form::hidden('event_id', $event->id) !!}
			<div class="card-body">
				 <div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							{!! Form::label('name', 'Venue Name', ['class' => 'col-sm-5 col-md-3 control-label']) !!}
							<div class="col-sm-7 col-md-8">
								{!! Form::text('name', null, ['class'=>'form-control', 'required']) !!}
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							{!! Form::label('address', 'Address', ['class' => 'col-sm-5 col-md-3 control-label']) !!}
							<div class="col-sm-7 col-md-8">
								{!! Form::text('address', null, ['class'=>'form-control', 'required']) !!}
							</div>
						</div>
					</div>	
				</div>
				
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							{!! Form::label('city', 'City / Town', ['class' => 'col-sm-5 col-md-3 control-label']) !!}
							<div class="col-sm-7 col-md-8">
								{!! Form::text('city', null, ['class'=>'form-control', 'required']) !!}
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							{!! Form::label('county', 'County', ['class' => 'col-sm-5 col-md-3 control-label']) !!}
							<div class="col-sm-7 col-md-8">
								{!! Form::text('county', null, ['class'=>'form-control', 'required']) !!}
							</div>
						</div>
					</div>	
				</div>
				
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							{!! Form::label('postcode', 'Postcode', ['class' => 'col-sm-5 col-md-3 control-label']) !!}
							<div class="col-sm-7 col-md-8">
								{!! Form::text('postcode', null, ['class'=>'form-control', 'required']) !!}
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						
					</div>	
				</div>
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					{!! Form::submit('Create Venue', ['class' => 'btn btn-flat btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>

	<div class="col-md-offset-1 col-md-10">
		<div class="card">
			<div class="card-head style-primary">
				<header>Assign Previously Used Venue</header>
			</div>
			
			@if (!empty($event_not_found))
				<div class="alert alert-danger" role="alert">
					<div>{{$event_not_found}}</div>
				</div>
			@endif
			
			{!! Form::open(['route' => ['events.venue.copy'], 'class' => 'form form-validate']) !!}
			{!! Form::hidden('event_id', $event->id) !!}
			<div class="card-body">
				 <div class="row">
					<div class="col-md-5">
					 	<div class="form-group floating-label">
							{!! Form::text('old_venue_name', null, ['class'=>'form-control', 'id' => 'previous_venues', 'required']) !!}
							{!! Form::label('old_venue_name', 'Previously used venue name') !!}
							{!! Form::hidden('old_venue_id', null, array('id' => 'old_venue_id')) !!}
						</div>
					</div>
				</div>
			</div>
			
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					{!! Form::submit('Assign Venue', ['class' => 'btn btn-flat btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>


<script type="text/javascript">
	$(function() {
		$("#previous_venues").autocomplete({
	        source: function( request, response ) {
	        $.ajax({
	            url: "{!! route('venues.getvenueslist') !!}",
	            dataType: "json",
	            data: {term: request.term},
	            success: function(data) {
	                        response($.map(data, function(item) {
	                        return {
	                            label: item.name,
	                            id: item.id,
	                            };
	                    }));
	                }
	            });
	        },
	        minLength: 2,
	        select: function(event, ui) {
	            $('#old_venue_id').val(ui.item.id);
	        }
	    });
	});
</script>
<!--[END] show create/assign venue form -->

@else

<!--[START] Venue is assigned. Edit venue details -->
	<div class="pull-right">
		<button data-href="{{ route('events.detachVenue', $event->id) }}" type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#confirm-delete">Detach Venue</button>
	</div>
	{!! Form::model($event->venue, ['route' => ['events.venue.update', $event->id, $event->venue_id], 'id'=>'venue_form', 'method' => 'PATCH', 'class' => 'form-vertical form-validate']) !!}
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				{!! Form::label('name', 'Venue Name', ['class' => 'col-sm-3 col-md-2 control-label']) !!}
				<div class="col-sm-9 col-md-10">
					{!! Form::text('name', null, ['class'=>'form-control', 'required']) !!}
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				{!! Form::label('email', 'Venue Email', ['class' => 'col-sm-3 col-md-2 control-label']) !!}
				<div class="col-sm-9 col-md-10">
					{!! Form::text('email', null, ['class'=>'form-control']) !!}
				</div>
			</div>
		</div>	
	</div>
	
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				{!! Form::label('address', 'Venue Address', ['class' => 'col-sm-3 col-md-2 control-label']) !!}
				<div class="col-sm-9 col-md-10">
					{!! Form::text('address', null, ['class'=>'form-control', 'required']) !!}
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				{!! Form::label('website', 'Venue Website', ['class' => 'col-sm-3 col-md-2 control-label']) !!}
				<div class="col-sm-9 col-md-10">
					{!! Form::text('website', null, ['class'=>'form-control']) !!}
					<p class="help-block">Starts with http://</p>
				</div>
			</div>
		</div>	
	</div>

	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				{!! Form::label('city', 'City / Town', ['class' => 'col-sm-3 col-md-2 control-label']) !!}
				<div class="col-sm-9 col-md-10">
					{!! Form::text('city', null, ['class'=>'form-control', 'required']) !!}
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				{!! Form::label('venue_phone', 'Venue Phone', ['class' => 'col-sm-3 col-md-2 control-label']) !!}
				<div class="col-sm-9 col-md-10">
					{!! Form::text('venue_phone', null, ['class'=>'form-control']) !!}
					<p class="help-block">Digits only</p>
				</div>
			</div>
		</div>	
	</div>
	
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				{!! Form::label('county', 'County', ['class' => 'col-sm-3 col-md-2 control-label']) !!}
				<div class="col-sm-9 col-md-10">
					{!! Form::text('county', null, ['class'=>'form-control', 'required']) !!}
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				{!! Form::label('venue_staff_id', 'Contact Person', ['class' => 'col-sm-3 col-md-2 control-label']) !!}
				<div class="col-sm-9 col-md-10">
					{!! Form::select('venue_staff_id', [''=>''] + $tfi_staff, null, ['class'=>'form-control select2-list', 'required']) !!}
				</div>
			</div>
		</div>	
	</div>
	
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				{!! Form::label('postcode', 'Postcode', ['class' => 'col-sm-3 col-md-2 control-label']) !!}
				<div class="col-sm-9 col-md-10">
					{!! Form::text('postcode', null, ['class'=>'form-control', 'required']) !!}
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-sm-3">
			<div class="form-group">
				{!! Form::label('bedrooms', 'Bedrooms', ['class' => 'col-sm-4 col-md-4 control-label']) !!}
				<div class="col-sm-8 col-md-8">
					{!! Form::input('number', 'bedrooms', null, ['class'=>'form-control', 'min' => '0']) !!}
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="form-group">
				{!! Form::label('rating', 'Rating', ['class' => 'col-sm-4 col-md-4 control-label']) !!}
				<div class="col-sm-8 col-md-8">
					{!! Form::select('rating', [''=>'', 'Bad'=>'Bad', 'OK'=>'OK', 'Good' => 'Good'], null, ['class'=>'form-control']) !!}
				</div>
			</div>
		</div>
		
		<div class="col-sm-6">
			<div class="form-group">
				{!! Form::label('show_map', 'Show map', ['class' => 'col-sm-3 col-md-2 control-label']) !!}
				<div class="col-sm-9 col-md-10">
					<div class="checkbox checkbox-styled">
						<label>
							{!! Form::checkbox('show_map', true) !!}
						</label>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-sm-3">
			<div class="form-group">
				{!! Form::label('min_guaranteed', 'Min guaranteed numbers', ['class' => 'col-sm-7 col-md-7 control-label']) !!}
				<div class="col-sm-5 col-md-5">
					{!! Form::input('number', 'min_guaranteed', null, ['class'=>'form-control']) !!}
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="form-group text-right">
				{!! Form::label('Commission', 'Commission(%)', ['class' => 'col-sm-6 col-md-6 control-label']) !!}
				<div class="col-sm-6 col-md-6">
					{!! Form::input('number', 'commission', null, ['class'=>'form-control']) !!}
				</div>
			</div>
		</div>
		
		<div class="col-sm-3">
			<div class="form-group">
				{!! Form::label('latitude', 'Latitude', ['class' => 'col-sm-4 col-md-4 control-label']) !!}
				<div class="col-sm-8 col-md-8">
					{!! Form::text('latitude', null, ['class'=>'form-control']) !!}
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="form-group">
				{!! Form::label('longitude', 'Longitude', ['class' => 'col-sm-4 col-md-4 control-label']) !!}
				<div class="col-sm-8 col-md-8">
					{!! Form::text('longitude', null, ['class'=>'form-control']) !!}
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-sm-12">
			{!! Form::label('details', 'Venue details', ['class' => 'control-label']) !!}
			{!! Form::textarea('details', null, ['row'=>'2', 'style'=>'height: 150px;', 'id'=>'details']) !!}
		</div>
	</div>

	<header>
		<h3 class="opacity-75">Meeting Rooms</h3>
	</header>
	
	
	<div class="row">
		@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
		<p>{!! Form::button('Add Meeting Room', ['class' => 'btn ink-reaction btn-raised btn-sm btn-success', 'id'=>'add']) !!}</p>
		@endif
		<br/>
		<div class="row">
			<div class="col-xs-3">Venue Room Name</div>
			<div class="col-xs-2">Capacity</div>
			<div class="col-xs-2">Cabaret Capacity</div>
			<div class="col-xs-2">Theatre Capacity</div>
			<div class="col-xs-2">Max No of tables</div>
		</div>
		<div id="items">
			@foreach($event->venue->rooms as $room)
				<div class="row">
					<div class="col-xs-3"><input type="hidden" name="room_id[]" value="{{$room->id}}" /><input type="text" value="{{$room->room_name}}" name="room_name[]" class="form-control" required /></div>
					<div class="col-xs-2"><input type="number" value="{{$room->capacity}}" name="capacity[]" class="form-control" /> </div>
					<div class="col-xs-2"><input type="number" value="{{$room->cabaret_capacity}}" name="cabaret_capacity[]" class="form-control" /> </div>
					<div class="col-xs-2"><input type="number" value="{{$room->theatre_capacity}}" name="theatre_capacity[]" class="form-control" /> </div>
					<div class="col-xs-2"><input type="number" value="{{$room->max_tables}}" name="max_tables[]" class="form-control" /> </div>
					@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
					<div class="col-xs-1"><button type="button" class="delete_room_check btn btn-danger btn-icon-toggle" id="{{$room->id}}" title="Delete"><i class="md md-delete"></i></button></div>
					@endif
				</div>
			@endforeach
		</div>
	</div>
	
	@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
	<div class="card-actionbar">
		<div class="card-actionbar-row">
			{!! Form::submit('Update Venue', ['class' => 'btn ink-reaction btn-raised btn-primary']) !!}
		</div>
	</div>
	@endif
	{!! Form::close() !!}
	
	@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
	<hr class="ruler-xxl">
	<h4>Venue notes</h4>
	<div class="row">
		<p><button type="button" class="btn ink-reaction btn-raised btn-sm btn-success" data-toggle="modal" data-target="#add_note">Add Venue Note</button></p>
		<div class="modal fade" id="add_note" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="section-body">
					<div class="col-md-12">
						<div class="card">
							<div class="card-head style-primary">
								<header>Add Venue Note</header>
							</div>
							
							{!! Form::open(['route' => ['venues.addNoteTab', $event->id], 'class' => 'form form-validate']) !!}
							<div class="card-body">
                                <div class="form-group">
                                    {!! Form::label('priority', 'Priority', ['class' => 'control-label']) !!}
                                    {!! Form::select('priority', config('const.priorities'), null, ['class'=>'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('tags', 'Tags', ['class' => 'control-label']) !!}
                                        {!! Form::text('tags', null, ['class'=>'form-control', 'data-role'=>'tagsinput']) !!}
                                        <p class="help-block">Push enter to separate tags</p>
                                </div>
                                <div class="form-group">
                                     {!! Form::textarea('note', null, ['class'=>'form-control', 'rows' => 3, 'placeholder' => 'Please enter venue note']) !!}
                                 </div>
                            </div>
							<div class="card-actionbar">
								<div class="card-actionbar-row">
									<br/><br/>
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									{!! Form::submit('Add note', ['class' => 'btn btn-flat btn-primary']) !!}
								</div>
							</div>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<script>
            $( function() {
                $( "#sortable-high" ).sortable({
                    'containment': 'parent',
                    'handle': '.handle',
                    update: function(event, ui) {
                        $.post('{{ route('venues.notes.reorder') }}',
                            $("#list-high").serialize(), function(data) {
                                
                                if(!data.success) {
                                    alert('Whoops, something went wrong :/');
                                }
                            }, 'json');
                    }
                });
                
                $( "#sortable-medium" ).sortable({
                    'containment': 'parent',
                    'handle': '.handle',
                    update: function(event, ui) {
                        $.post('{{ route('venues.notes.reorder') }}',
                            $("#list-medium").serialize(), function(data) {
                                
                                if(!data.success) {
                                    alert('Whoops, something went wrong :/');
                                }
                            }, 'json');
                    }
                });
                
                $( "#sortable-low" ).sortable({
                    'containment': 'parent',
                    'handle': '.handle',
                    update: function(event, ui) {
                        $.post('{{ route('venues.notes.reorder') }}',
                            $("#list-low").serialize(), function(data) {
                                
                                if(!data.success) {
                                    alert('Whoops, something went wrong :/');
                                }
                            }, 'json');
                    }
                });
            });
            </script>
        
            <div class="col-md-12">
                <form id="list-high">
                <table class="table table-striped venue-notes">
                    <thead>
                        <tr><th>High Priority Notes</th></tr>
                    </thead>
                    <tbody id="sortable-high">
                        @foreach($event->venue->notes()->where('priority', 3)->orderBy('order')->get() as $note)
                            @include('admin.venues.venue_note_line')
                        @endforeach
                        </div>
                    </tbody>
                </table>
                </form>
                
                <!-- Modal -->
                @foreach($event->venue->notes()->where('priority', 3)->orderBy('order')->get() as $note)
                <div id="comment-{{$note->id}}" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        {!! Form::open(['route'=> ['venues.notes.addComment', $note->id], 'method'=>'post']) !!}
                        <div class="modal-content">
                            <div class="card-head style-primary">
                                <header>Add Note Comment</header>
                            </div>
                          
                          <div class="card-body">
                            {!! Form::textarea('comment', null, ['class'=>'form-control', 'rows'=>2, 'required', 'placeholder'=>'Add your commend']) !!}
                          </div>
                          
                            <div class="card-actionbar">
                                <div class="card-actionbar-row">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    {!! Form::submit('Save Comment', ['class' => 'btn btn-flat btn-primary']) !!}
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                @endforeach
                
                <form id="list-medium">
                <table class="table table-striped venue-notes">
                    <thead>
                        <tr><th>Medium Priority Notes</th></tr>
                    </thead>
                    <tbody id="sortable-medium">
                        @foreach($event->venue->notes()->where('priority', 2)->orderBy('order')->get() as $note)
                            @include('admin.venues.venue_note_line')
                        @endforeach
                        </div>
                    </tbody>
                </table>
                </form>
                
                <!-- Modal -->
                @foreach($event->venue->notes()->where('priority', 2)->orderBy('order')->get() as $note)
                <div id="comment-{{$note->id}}" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        {!! Form::open(['route'=> ['venues.notes.addComment', $note->id], 'method'=>'post']) !!}
                        <div class="modal-content">
                            <div class="card-head style-primary">
                                <header>Add Note Comment</header>
                            </div>
                          
                          <div class="card-body">
                            {!! Form::textarea('comment', null, ['class'=>'form-control', 'rows'=>2, 'required', 'placeholder'=>'Add your commend']) !!}
                          </div>
                          
                            <div class="card-actionbar">
                                <div class="card-actionbar-row">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    {!! Form::submit('Save Comment', ['class' => 'btn btn-flat btn-primary']) !!}
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                @endforeach
                
                
                <form id="list-low">
                <table class="table table-striped venue-notes">
                    <thead>
                        <tr><th>Low Priority Notes</th></tr>
                    </thead>
                    <tbody id="sortable-low">
                        @foreach($event->venue->notes()->where('priority', 1)->orderBy('order')->get() as $note)
                            @include('admin.venues.venue_note_line')
                        @endforeach
                        </div>
                    </tbody>
                </table>
                </form>
                
                <!-- Modal -->
                @foreach($event->venue->notes()->where('priority', 1)->orderBy('order')->get() as $note)
                <div id="comment-{{$note->id}}" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        {!! Form::open(['route'=> ['venues.notes.addComment', $note->id], 'method'=>'post']) !!}
                        <div class="modal-content">
                            <div class="card-head style-primary">
                                <header>Add Note Comment</header>
                            </div>
                          
                          <div class="card-body">
                            {!! Form::textarea('comment', null, ['class'=>'form-control', 'rows'=>2, 'required', 'placeholder'=>'Add your commend']) !!}
                          </div>
                          
                            <div class="card-actionbar">
                                <div class="card-actionbar-row">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    {!! Form::submit('Save Comment', ['class' => 'btn btn-flat btn-primary']) !!}
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                @endforeach
                
                
                <script type="text/javascript">
                    $(document).ready(function() {
                        $('.venue-notes').dataTable({
                            "ordering" : false,
                            "pageLength" : 50,
                            "info" : false,
                            "paging" : false
                        });
                    } );
                </script>
            </div>
	</div>
	@endif
	
	<hr class="ruler-xxl">
	<header>
		<h3 class="opacity-75">Venue Images</h3>
	</header>
	
	<div class="row">
		<div class="col-lg-3">
			@if(!empty($event->venue->image_1))
				<div class="holder">
					<div class="overlay overlay-default">
						<button data-href="{{ route('events.venue.deleteImage', ['id' => $event->id, 'id2' => 1]) }}" type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#confirm-delete">Remove</button>
					</div>
					{!! HTML::image('uploads/images/'.$event->venue->image_1,'', ['class'=>'img-responsive']) !!}
				</div>
			@else
				@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
				<div id="venue_image_1">Upload</div>
				<div id="status"></div>
				@endif
			@endif
		</div>
		
		<div class="col-lg-3">
			@if(!empty($event->venue->image_2))
				<div class="holder">
					<div class="overlay overlay-default">
						<button data-href="{{ route('events.venue.deleteImage', ['id' => $event->id, 'id2' => 2]) }}" type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#confirm-delete">Remove</button>
					</div>
					{!! HTML::image('uploads/images/'.$event->venue->image_2,'', ['class'=>'img-responsive']) !!}
				</div>
			@else
				@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
				<div id="venue_image_2">Upload</div>
				<div id="status"></div>
				@endif
			@endif
		</div>
	</div>
	
	
	
	<hr class="ruler-xxl">
    <h3 class="opacity-75">Venue files</h3>
    <div class="row">
        <div class="form-group">
            <div id="venue_files_upload">Upload</div>
            <div id="status"></div>
        </div>
    </div>
    
    <div class="row">
        
        <table id="documents_list" class="table table-striped table-responsive" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Document Name</th>
                    <th>Display Name</th>
                    <th>Type</th>
                    <th>Size(Mb)</th>
                    <th>Date Uploaded</th>
                    <th>Date Live</th>
                    <th>Actions</th>
                </tr>
            </thead>
        
            <tbody>
                @foreach ($event->venue->files as $document)
                <tr>
                    <td>{{ $document->document_name }}</td>
                    <td>{{ $document->display_name }}</td>
                    <td>{{ $document->type}}</td>
                    <td>{{ $document->size / 1000000 }}</td>
                    <td>{{ date("d m Y H:i:s",strtotime($document->created_at)) }}</td>
                    <td>@if(!empty($document->date_live)){{ date("d m Y",strtotime($document->date_live)) }}@endif</td>
                    <td>
                        @if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
                            <a href="{{config('app.url')}}uploads/venue_files/{{$document->document_name}}" target="_new"><button type="button" class="btn btn-info btn-xs" title="Download"><span class="glyphicon glyphicon-download"></span></button></a>
                            <button data-href="{{ route('events.venues.removeFile', [$event->id, $document->id]) }}" type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-trash"></span></button>
                        @endif
                    </td>
                </tr>
                @endforeach
                
            </tbody>
        </table>
        
    </div>
	
	@include('admin.partials.readonly', ['form_id'=>'venue_form'])
	
	@include('admin.events._tabNotes', array('tab_id' => 4, 'notes' => $event->tabs()->where('tab_id', 4)->first()->notes()->where('event_id', $event->id)->orderby('created_at', 'DESC')->get()))

<script type="text/javascript">
    $(document).ready(function() {
        $('#documents_list').dataTable();
    } );

    $(document).ready(function()
    {
    var vf_settings = {
        url: "{!! route('venues.uploadFile') !!}",
        formData: {  
           "_token": "{{ csrf_token() }}",
           "venue_id": "{{ $event->venue->id }}" 
        },
        dragDrop:true,
        maxFileSize: 20000000,
        fileName: "myfile",
        allowedTypes:"jpg,png,gif,bmp,jpeg,pdf,doc,docx,xls,xlsx,docx,ppt,pptx",    
        returnType:"json",
         onSuccess:function(files,data,xhr)
        {
           // alert((data));
           location.reload();
        },
        showDelete:false
    }
    var uploadObj = $("#venue_files_upload").uploadFile(vf_settings);
    
    });
</script>

<script type="text/javascript">
$(document).ready(function(){
	$( '#details' ).ckeditor();
});
//when the Add Field button is clicked
$("#add").click(function () {
	
	var html = '<div class="row">'+
					'<div class="col-xs-3"><input type="text" value="" name="room_name[]" class="form-control" required /></div>'+
					'<div class="col-xs-2"><input type="number" value="" name="capacity[]" class="form-control" /> </div>'+
					'<div class="col-xs-2"><input type="number" value="7" name="cabaret_capacity[]" class="form-control" /> </div>'+
					'<div class="col-xs-2"><input type="number" value="" name="theatre_capacity[]" class="form-control" /> </div>'+
					'<div class="col-xs-2"><input type="number" value="" name="max_tables[]" class="form-control" /> </div>'+
					'<div class="col-xs-1"><button type="button" class="delete btn btn-danger btn-icon-toggle" title="Delete"><i class="md md-delete"></i></button></div>'+
				'</div>';
	
	$("#items").append(html);
});

$("body").on("click", ".delete", function (e) {
	$(this).parent("div").parent("div").remove();
});

// for previously saved rooms perform check if any sessions are assigned to room
$("body").on("click", ".delete_room_check", function (e) {
	var localThis = this;
	
	$.ajax({
		url: "{!! route('events.venue.chechRoom') !!}",
		data: {venue_room_id: localThis.id},
		success: function(response){
			if(response) {
				alert("At least one session is assigned to this meeting room. Deleting is not allowed.")
			} else {
				$(localThis).parent("div").parent("div").remove();
			}
		},
		error: function(){
			alert('Error checking if meeting room is assigned to sessions');
		}
	});
});
	

$(document).ready(function(){
	var settings = {
	    url: "{!! route('events.venue.uploadImage') !!}",
	    formData: {  
		   "_token": "{{ csrf_token() }}",
		   "venue_id": "{{ $event->venue_id }}"
		},
	    dragDrop:true,
	    maxFileSize: 1000000,
	    fileName: "myfile",
	    allowedTypes:"jpg,png,gif,bmp,jpeg",	
	    returnType:"json",
		 onSuccess:function(files,data,xhr)
	    {
	       // alert((data));
	       location.reload();
	    },
	    showDelete:false
	}
	var uploadObj = $("#venue_image_1").uploadFile(settings);
	var uploadObj = $("#venue_image_2").uploadFile(settings);
	
});

$(".note_status").change(function(){
    $.ajax({
        url: "{!! route('venues.notes.changeStatus') !!}",
        type: "POST",
        data: {
            "_token": "{{ csrf_token() }}",
            "venue_id": this.id,
            "priority": this.value,
        },
        success: function () {
            location.reload();
        },
        error: function() {
            alert('Error updating note status.');
        }
    });
});
</script>

<!--[END] Venue is assigned. Edit venue details -->
@endif

</div>
@endif