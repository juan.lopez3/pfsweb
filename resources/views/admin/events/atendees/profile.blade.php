<script src="{{asset('assets/admin/js/libs/jquery/jquery-1.11.2.min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/jquery-ui/jquery-ui.min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/bootstrap/bootstrap.min.js')}}"></script>

<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/materialadmin.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/bootstrap.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/app.css')}}" />


<div class="row">
	<div class="col-xs-5">
		<div class="row">
			<div class="col-xs-4">
			@if(!empty(Auth::user()->profile_image))
				{!! HTML::image('uploads/profile_images/'.Auth::user()->profile_image, '', ['class' => 'img-circle height-2']) !!}
			@else	
				{!! HTML::image('images/no_image.png', '', ['class' => 'img-circle height-2']) !!}
			@endif
			</div>
			<div class="col-xs-8">
				<span class="attendee">{{$user->title}} {{$user->first_name}} {{$user->last_name}}</span>
			</div>
		</div>
		<br/>
		<div class="row">
			<div class="col-xs-5 title">Email:</div>
			<div class="col-xs-7"><a href="mailto:{{$user->email}}">{{$user->email}}</a></div>
			
			@if(!empty($user->cc_email))
			<div class="col-xs-5 title">CC Email:</div>
			<div class="col-xs-7"><a href="mailto:{{$user->cc_email}}">{{$user->cc_email}}</a></div>
			@endif
			
			@if(!empty($user->phone))
			<div class="col-xs-5 title">Phone:</div>
			<div class="col-xs-7">{{$user->phone}}</div>
			@endif
			
			@if(!empty($user->mobile))
			<div class="col-xs-5 title">Mobile:</div>
			<div class="col-xs-7">{{$user->mobile}}</div>
			@endif
			
			@if(!empty($user->postcode))
			<div class="col-xs-5 title">Postcode:</div>
			<div class="col-xs-7">{{$user->postcode}}</div>
			@endif
			
			@if(!empty($user->company))
			<div class="col-xs-5 title">Company:</div>
			<div class="col-xs-7">{{$user->company}}</div>
			@endif
			
			@if(!empty($user->badge_name))
			<div class="col-xs-5 title">Badge name:</div>
			<div class="col-xs-7">{{$user->badge_name}}</div>
			@endif
			
			@if(!empty($user->dietary_requirement_id))
			<div class="col-xs-5 title">Dietary requirements:</div>
			<div class="col-xs-7">{{$user->dietaryRequirement->title}}</div>
			@endif
			
			@if(!empty($user->special_requirements))
			<div class="col-xs-5 title">Special requirements:</div>
			<div class="col-xs-7">{{$user->special_requirements}}</div>
			@endif
		</div>
		
		<br/>
		<h4><b>PFS Details</b></h4>
		<div class="row">
			<div class="col-xs-7">PFS Member:</div>
			<div class="col-xs-5">@if($user->role_id == \Config::get('roles.member')) Y @else N  @endif</div>
			
			<div class="col-xs-7">Chartered Status:</div>
			<div class="col-xs-5">@if($user->chartered) Y @else N @endif</div>
		</div>
		
		<br/>
		<h4><b>Email Record</b></h4>
		
		@foreach($emails as $email)
		<b>{{date("d/m/Y H:s", strtotime($email->created_at))}}</b> {{$email->to}} - {{$email->subject}}<br/>
		@endforeach
		
	</div>
	<div class="col-xs-7 profile-middle">
		
		<h4><b>Booking Status</b></h4>
		
		<div class="row">
			<div class="col-xs-5"><b>Registration Date:</b></div>
			<div class="col-xs-7">{{ date("H:i j F, Y",strtotime($event_attendee->created_at))}}</div>
		</div>
		
		
		<?php
		$reg_status = $event_attendee->pivot->registration_status_id;
		$attended = empty($event_attendee->pivot->checkin_time) ? false : true;
		?>
		
		<div class="row">
			<div class="col-xs-5"><b>Event booking status:</b></div>
			<div class="col-xs-7">{!! Form::select('booking_status', $registration_status, $reg_status, ['class'=>'form-control booking_status']) !!}</div>
		</div>
		
		@if(in_array($reg_status, [\Config::get('registrationstatus.cancelled'), \Config::get('registrationstatus.late_cancelled')]))
		<br/>
        <div class="row">
            <div class="col-xs-4"><b>Cancellation reason:</b></div>
            <div class="col-xs-8"><textarea name="cancel_reason" id="cancel_reason" class="form-control">{{$event_attendee->pivot->cancel_reason}}</textarea></div>
        </div>
        <div class="row pull-right">
            <div class="col-xs-12" style="padding-top: 5px">
            {!! Form::submit('Update cancellation reason', ['class' =>'btn btn-xs btn-success', 'id'=>'update_cancel_reason']) !!}
            </div>
        </div>
        @endif
        
		@if(!$attended || in_array($reg_status, [\Config::get('registrationstatus.cancelled'), \Config::get('registrationstatus.late_cancelled')]))
		<br/>
		
		<div class="checkbox checkbox-styled">
			<label>
				<input type="checkbox" id="paid" value="1" name="paid" @if($event_attendee->pivot->paid) checked @endif />
				<span>Cancellation / non attendance fee paid</span>
			</label>
		</div>
		
		<div class="checkbox checkbox-styled">
            <label>
                <input type="checkbox" id="fee_waived" value="1" name="fee_waived" @if($event_attendee->pivot->fee_waived) checked @endif />
                <span>Cancellation fee waived</span>
            </label>
        </div>
		@endif
		
		<div class="checkbox checkbox-styled">
            <label>
                <input type="checkbox" id="manual_booking" value="1" name="manual_booking" @if($event_attendee->pivot->manual_booking) checked @endif />
                <span>Manual booking</span>
            </label>
        </div>
        
        <div class="checkbox checkbox-styled">
            <label>
                <input type="checkbox" id="onsite_booking" value="1" name="manual_booking" @if($event_attendee->pivot->onsite_booking) checked @endif />
                <span>ON-SITE booking</span>
            </label>
		</div>
		
		<br/>
		
		<!--<h4><b>Post Event Details</b></h4>-->
		<h4><b>Event </b></h4>
		
		<b>Booked sessions:</b>
		
		@foreach($event->scheduleTimeSlots()->where('bookable', 1)->orderBy('start')->get() as $s)
		<?php
			$booking =  $booked_sessions->where('event_schedule_slot_id', $s->id)
				->first();
			
			$status = isset($booking) ? $booking->registration_status_id : '';
		?>
		
		<div class="row">
			<div class="col-xs-8">{{date("H:i", strtotime($s->start))}} - {{date("H:i", strtotime($s->end))}} - {{$s->title}} @if(($s->slot_capacity <= $s->attendees()->whereIn('registration_status_id', [config('registrationstatus.booked'), config('registrationstatus.waitlisted')])->count()) || $s->stop_booking)</span><img src="{{asset('assets/site/img/fully_booked.png')}}" width="70" height="22" alt="fully booked">@endif</div>
			<div class="col-xs-4">{!! Form::select($s->id, [''=>'Not Booked']+$registration_status, $status, ['class'=>'sessions_booked form-control']) !!}</div>
		</div>
		@endforeach
		
		<br/>
		<h4><b>Contribution Status</b></h4>
		
		<div class="row">
			@foreach($user->subRoles as $sr)
			<div class="col-xs-12"><i>{{$sr->title}}</i></div>
			@endforeach
		</div>
		
		<div class="row">
            <div class="col-xs-1">Notes:</div>
            <div class="col-xs-11">{!! Form::textarea('notes', $event_attendee->pivot->notes, ['class'=>'form-control', 'id'=>'notes', 'placeholder'=>'User event specific notes', 'rows'=>3]) !!}</div>
        </div>
		
		<div class="row">
			<div class="col-xs-1">Bio:</div>
			<div class="col-xs-10">{{$user->bio}}</div>
		</div>
		<br/>
		<h4><b>Payments</b></h4>
		@foreach($payments as $payment)
				<div class="row">
					<div class="col-xs-3"><b>Amount:</b></div>
					<div class="col-xs-8">{{$payment->amount}}</div>
					<div class="col-xs-3"><b>Currency:</b></div>
					<div class="col-xs-8">{{$payment->currency}}</div>
					<div class="col-xs-3"><b>Status:</b></div>
					<div class="col-xs-8">{{$registration_status[$payment->status_result]}}</div>
					<div class="col-xs-3"><b>Date:</b></div>
					<div class="col-xs-8">{{ date("d/m/Y H:i:s",strtotime($event_attendee->pivot->created_at)) }}</div>
					<div class="col-xs-3"><b>Brand:</b></div>
					<div class="col-xs-8">{{$payment->paymentBrand}}</div>
					<div class="col-xs-3"><b>Card Holder:</b></div>
					<div class="col-xs-8">{{$payment->holder}}</div>
					<div class="col-xs-3"><b>Checkout Id:</b></div>
					<div class="col-xs-8">{{$payment->checkout_id}}</div>
				</div>
				<hr>
		@endforeach
	</div>
</div>

<script type="text/javascript">

$(document).ready(function () {
	
	$("#notes").bind('change', function(){
	    
	    $.ajax({
            url: "{!! route('events.atendees.updateNotes', $event_attendee->pivot->id) !!}",
            type: "POST",
            data: {"notes":$("textarea#notes").val(), "_token": "{{ csrf_token() }}"},
            success: function(){
            // success
            //location.reload();
        }});
	})


	$("#manually_cpd").bind('change', function(){
	    console.log("hi entre acá updateCPD")
	    $.ajax({
            url: "{!! route('events.atendees.updateManuallyCpd', $event_attendee->pivot->id) !!}",
            type: "POST",
            data: {"manually_cpd":$("#manually_cpd").val(), "_token": "{{ csrf_token() }}"},
            success: function(){
            // success
            //location.reload();
        }});
	})
	
	$('#paid').bind('change', function(){
		
		$.ajax({
			url: "{!! route('events.atendees.updatePaidStatus', $event_attendee->pivot->id) !!}",
			type: "POST",
			data: {"paid":$('#paid').is(":checked"), "_token": "{{ csrf_token() }}"},
			success: function(){
			// success
			//location.reload();
		}});
	});
	
	$('#manual_booking').bind('change', function(){
        
        $.ajax({
            url: "{!! route('events.atendees.updateManualBookingStatus', $event_attendee->pivot->id) !!}",
            type: "POST",
            data: {"manual_booking":$('#manual_booking').is(":checked"), "_token": "{{ csrf_token() }}"},
            success: function(){
            // success
            //location.reload();
        }});
    });
    
    $('#fee_waived').bind('change', function(){
        
        $.ajax({
            url: "{!! route('events.atendees.updatefeeWaived', $event_attendee->pivot->id) !!}",
            type: "POST",
            data: {"fee_waived":$('#fee_waived').is(":checked"), "_token": "{{ csrf_token() }}"},
            success: function(){
            // success
            //location.reload();
        }});
    });
	
	$('#onsite_booking').bind('change', function(){
        
        $.ajax({
            url: "{!! route('events.atendees.updateOnsiteBookingStatus', $event_attendee->pivot->id) !!}",
            type: "POST",
            data: {"onsite_booking":$('#onsite_booking').is(":checked"), "_token": "{{ csrf_token() }}"},
            success: function(){
            // success
            //location.reload();
        }});
    });
    
    $('#update_cancel_reason').bind('click', function(){
        
        $.ajax({
            url: "{!! route('events.atendees.updateCancellationReason', $event_attendee->pivot->id) !!}",
            type: "POST",
            data: {"cancel_reason":$('#cancel_reason').val(), "_token": "{{ csrf_token() }}"},
            success: function(){
            // success
            //location.reload();
        }});
    });
    
	$('.sessions_booked').bind('change', function(){
		
		var c = confirm("Are you sure you want to change booking status?");
		
		if(c) {
			$.ajax({
				url: "{!! route('events.atendees.updateBookingStatus', $event_attendee->pivot->id) !!}",
				type: "POST",
				data: {"schedule_slot_id":this.name, "status":this.value, "_token": "{{ csrf_token() }}"},
				success: function(){
				// success
				//location.reload();
			}});
		}
	});
	
	var selected = $("input[type='radio'][class='booking_status']:checked").attr("id");
	$('.booking_status').bind('change', function () {
		
		var c = confirm("Are you sure you want to change booking status?");
		
		if(c) {
			$.ajax({
				url: "{!! route('events.atendees.updateStatus', [$event->id, $user->id]) !!}",
				type: "POST",
				data: {"value":this.value, "_token": "{{ csrf_token() }}"},
				success: function(){
				// success
				location.reload();
			}});
		} else {
			// set previous option
			$('#'+selected).prop('checked', true);
		}
	});
});
</script>