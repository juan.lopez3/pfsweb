<div class="row" style="display: none;" id="upload_attendee">
    <div class="col-xs-12">
        <div class="card">
            <div class="card-head style-primary">
                <header>Upload Attendees</header>
            </div>
            
            {!! Form::open(['route' => ['event.atendees.importAttendees', $event->id], 'class' => 'form form-validate', 'files'=>true]) !!}
            
            <div class="card-body">
                <div class="form-group">
                    {!! Form::label('file_upload', 'Select file (Supported file types: xls, xlsx)') !!}
                    {!! Form::file('file_upload', ['class' => 'form-control']) !!}
                </div>
                <a style="color:green" href="{!! route('event.atendees.downloadExcel') !!}" class="btn btn-large pull-left">
                    <i class="glyphicon glyphicon-download-alt"></i> 
                    Download Template 
                </a>
            </div>
            
            <div class="card-actionbar">
                <div class="card-actionbar-row">
                    {!! Form::submit('Import', ['class' => 'btn btn-primary']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>


<script type="text/javascript">
$(document).ready(function(){

    $( "#upload_attendee_button" ).click(function() {
        $( "#upload_attendee" ).toggle( "slow", function() {});
    });

});
</script>