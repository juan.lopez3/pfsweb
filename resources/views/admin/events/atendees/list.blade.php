@extends('admin.layouts.default')

@section('title')
{{$event->title}} Atendees List
@endsection

@section('content')

<section>
<div class="section-header">
	<div class="row">
		<div class="col-xs-9"><span class="event-head">{{$event->title}}</span> - {{ date("j F Y",strtotime($event->event_date_from)) }}<br/>
			<span class="glyphicon glyphicon-user text-success"></span> <span class="event-head">Attendees List</span>
		</div>
		<div class="col-xs-3 text-right">@include('admin.partials.eventActions')</div>
	</div>
</div>
<br/>
@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator'), role('regional_coordinator')]))
<div class="row pull">
	<p class="pull-right">
	    <button type="button" class="btn ink-reaction btn-raised btn-success" id="upload_attendee_button">Upload Attendees</button>
	    <button type="button" class="btn ink-reaction btn-raised btn-success" id="add_attendee_button">ADD Attendee</button>
	</p>
</div>

@include('admin.partials.validationErrors')
@include('admin.events.atendees.addAttendee')
@include('admin.events.atendees.uploadAttendee')

@endif


<script src="{{asset('assets/admin/fancybox/source/jquery.fancybox.js')}}"></script>
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/fancybox/source/jquery.fancybox.css')}}" />

<script type="text/javascript">
$(document).ready(function() {
	$('.fancybox').fancybox();
});
</script>

<script type="text/javascript">
$(document).ready(function() {
    $.fn.dataTable.moment( 'D/M/YYYY HH:mm:ss' );
	$('#atendees_list').dataTable();
} );
</script>

<div class="section-body">	
	<?php
		$show_attendance = false;
		if($event->event_date_from <= date("Y-m-d") && hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator'), role('host')])) $show_attendance = true;
	?>
	<div class="card">
		<div class="card-body" style="overflow-y:scroll;">
			<table id="atendees_list" class="table table-striped table-responsive" cellspacing="0" width="100%">
				<thead>
					<tr>
						@if($show_attendance)
							<th>CHECK-IN</th>
							<th>CHECK-OUT</th>
						@endif
						<th>PIN</th>
						<th>Name</th>
						<th>Email Address</th>
						<th>Status</th>
						<th>Member</th>
						<th>Contributor role</th>
						<th>Role</th>
						<th>Date Registered</th>
						<th>Update</th>
						<th>Actions</th>
					</tr>
				</thead>
			
				<tfoot>
					<tr>
						@if($show_attendance)
							<th>CHECK-IN</th>
							<th>CHECK-OUT</th>
						@endif
						<th>Date Registered</th>
						<th>PIN</th>
						<th>Name</th>
						<th>Email Address</th>
						<th>Status</th>
						<th>Member</th>
						<th>Contributor role</th>
						<th>Role</th>
						<th>Actions</th>
					</tr>
				</tfoot>
			
				<tbody>
					@foreach ($event->atendees()->with('subRoles', 'role')->get() as $attendee)
					<tr>
						@if($show_attendance)
							<td>@if(!empty($attendee->pivot->checkin_time)){{ date("d/m/Y H:i",strtotime($attendee->pivot->checkin_time)) }} @else - @endif</td>
							<td>@if(!empty($attendee->pivot->checkout_time)){{ date("d/m/Y H:i",strtotime($attendee->pivot->checkout_time)) }} @else - @endif</td>
						@endif
						<td>{{$attendee->pin}}</td>
						<td>{{$attendee->first_name}} {{$attendee->last_name}}</td>
						<td><a href="mailto:{{$attendee->email}}">{{$attendee->email}}</a></td>
						<td>{{$registration_status[$attendee->pivot->registration_status_id]}}</td>
						<td>
							@if($attendee->role_id == role('member')) Y @else N @endif
						</td>
						<td>{{implode(", ", $attendee->subRoles->lists('title'))}}</td>
						<td>{{$attendee->role->title}}</td>
						<td>{{ date("d/m/Y H:i:s",strtotime($attendee->pivot->created_at)) }}</td>
						<td>{{ date("d/m/Y H:i:s",strtotime($attendee->pivot->updated_at)) }}</td>
						<td>
							<a class="fancybox fancybox.iframe" href="{{ route('events.atendees.show', [$event->id, $attendee->id]) }}"><button type="button" class="btn btn-warning btn-xs" title="View/Edit"><span class="glyphicon glyphicon-pencil"></span></button></a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="section-body">	
	<div class="card">
		<div class="card-body">
			<div class="section-header">
				<ol class="breadcrumb">
					<li class="active">Invite members</li>
				</ol>
			</div>
			@include('admin.partials.validationErrors')
			@if(Session::has('success'))
			<div class="alert alert-success">{{Session::get('success')}}</div>
			@endif
			
			{!! Form::open(['route' => ['event.atendees.invite', $event->id], 'class' => 'form form-validate']) !!}
				
				{!! Form::textarea('pins', null, ['class' => 'form-control control-2-rows', 'required', 'placeholder' =>'Enter member PIN numbers separated by comma']) !!}
				
				<div class="card-actionbar">
					<div class="card-actionbar-row">
						{!! Form::submit('Send invitation', ['class' => 'btn ink-reaction btn-raised btn-primary']) !!}
					</div>
				</div>
			{!! Form::close() !!}
			
		</div>
	</div>
</div>


</section>

@endsection
