<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/jquery-ui/jquery-ui-theme.css')}}" />
<div class="row" style="display: none;" id="add_attendee">
	<div class="col-xs-12">
		<div class="card">
			<div class="card-head style-primary">
				<header>Add Attendee</header>
			</div>
			
			{!! Form::open(['route' => ['event.atendees.attachattendee', $event->id], 'class' => 'form form-validate']) !!}
			<div class="card-body">
				{!! Form::label('attendee_id', 'Attendee') !!}
				{!! Form::text('attendee', null, ['class'=>'form-control', 'placeholder'=>'Pin, name or surname', 'id'=>'attendee']) !!}
				{!! Form::hidden('attendee_id', null, ['id'=>'attendee_id']) !!}
			
                <!-- a sesssion slot is required for full booking -->
                <div>
                    <h3>Please select a session time slot:</h3>
                    <?php 
                        $tss = $event->scheduleTimeSlots()->get();    
                    ?>
                    @foreach($tss as $key=>$ts)
                    <div class="checkbox">
                        <label for="scales[{{$key}}]">
                            <input type="checkbox" name="timeslot_ids[{{$key}}]" value="{{$ts->id}}" checked />
                            <span>{{$ts->title}}</span>
                        </label>
                    </div>
                    @endforeach            
                 </div>           
            
            </div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					{!! Form::submit('Add Attendee', ['class' => 'btn btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>


<script type="text/javascript">
$(document).ready(function(){

$( "#add_attendee_button" ).click(function() {
	$( "#add_attendee" ).toggle( "slow", function() {});
});

$("#attendee").autocomplete({
        source: function( request, response ) {
        $.ajax({
            url: "{!! route('events.atendees.getAttendeesList', $event->id) !!}",
            dataType: "json",
            data: {term: request.term},
            success: function(data) {
                        response($.map(data, function(item) {
                        return {
                            label: item.pin + ' ' + item.title + ' ' + item.first_name + ' ' + item.last_name,
                            id: item.id,
                            };
                    }));
                }
            });
        },
        minLength: 3,
        select: function(event, ui) {
            $('#attendee_id').val(ui.item.id);
            
        }
    });
});
</script>