@extends('admin.layouts.default')

@section('title')
	Event List
@endsection

@section('content')

<script type="text/javascript">
	$(document).ready(function() {
	    $.fn.dataTable.moment( 'D/M/YYYY' );
		$('#event_list').dataTable({
			"order": [[ 6, "asc" ]],
			"iDisplayLength": 50,
			"columnDefs": [
			    { "width": "28%", "targets": 8,  "searchable": false }
			  ]
		});
	} );
</script>

<section>
<div class="section-header">
	<ol class="breadcrumb">
		<li class="active">Archive List</li>
	</ol>
</div>
@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
<!-- <p><a href="{{route('events.create') }}"<button type="button" class="btn ink-reaction btn-raised btn-success">ADD Event</button></a></p> -->
@endif
<div class="section-body">	
	<div class="card">
		<div class="card-body">
			<table id="event_list" class="table table-striped table-responsive" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Venue Name</th>
						<th>Postcode</th>
						<th>Region</th>
						<th>City</th>
						<th>Event Date</th>
						<th>Attending</th>
						<th>Actions</th>
					</tr>
				</thead>
			
				<tfoot>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Venue Name</th>
						<th>Postcode</th>
						<th>Region</th>
						<th>City</th>
						<th>Event Date</th>
						<th>Attending</th>
						<th>Actions</th>
					</tr>
				</tfoot>
			
				<tbody>
					@foreach ($events as $event)
					<tr>
						<td>{{ $event->id }}</td>
						<td>(@if($event->publish)<span class="green" title="Active">A</span>@else<span class="red" title="Not Active">N</span>@endif) {{ $event->title }}</td>
						@if(empty($event->venue_id))
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						@else
							<td>{{ $event->venue->name}}</td>
							<td>{{ $event->venue->postcode}}</td>
							<td>@if(!empty($event->region)){{ $event->region->title}}@endif</td>
							<td>{{ $event->venue->city}}</td>
						@endif
						
						<td>{{ date("d/m/Y",strtotime($event->event_date_from))}}</td>
						<th>{{$event->atendees()->where('registration_status_id', config('registrationstatus.booked'))->whereIn('role_id', [role('member'), role('non_member'), role('uploaded_attendee')])->count()}}</th>
						<td>
						   
							@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator'), role('host'), role('venue_staff'), role('regional_coordinator')]))
								<a href="{{ route('events.delete', $event->id) }}"><button type="button" class="btn btn-flat btn-success btn-sm" title="Delete"><span class="glyphicon glyphicon-trash"></span></button></a>
							@endif
							@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
								<a href="{{ route('events.restore', $event->id) }}"><button type="button" class="btn btn-flat btn-info btn-sm" title="Restore"><span class="glyphicon glyphicon-repeat"></span></button></a>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
</section>

@include('admin.partials.deleteConfirmation')

@endsection
