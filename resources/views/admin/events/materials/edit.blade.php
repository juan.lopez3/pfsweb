@extends('admin.layouts.default')

@section('title')
Edit Event Material
@endsection

@section('content')

<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/bootstrap-datepicker/datepicker3.css')}}" />

<script type="text/javascript" src="{{asset('assets/admin/js/libs/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/core/demo/DemoFormComponents.js')}}"></script>

<section>
<div class="section-body">
	<div class="col-lg-offset-2 col-md-7">
		<div class="card">
			<div class="card-head style-primary">
				<header>Edit Event Material</header>
			</div>
			
			@include('admin.partials.validationErrors')
			
			{!! Form::model($event_material, ['route' => ['events.materials.update', $event_material->id], 'method' => 'PATCH', 'class' => 'form form-validate']) !!}
			<div class="card-body">
				<div class="col-sm-6"> 
					<div class="form-group floating-label">
						{!! Form::text('document_name', null, ['class'=>'form-control', 'readonly']) !!}
						{!! Form::label('document_name', 'Document Name') !!}
					</div>
				</div>
				
				
				<div class="col-sm-6">
					<div class="form-group floating-label">
						{!! Form::text('display_name', null, ['class'=>'form-control', 'required']) !!}
						{!! Form::label('display_name', 'Display Name') !!}
					</div>
				</div>
				
				<div class="row">
					<div class="col-sm-4">
						<div class="form-group">
							{!! Form::text('type', null, ['class'=>'form-control']) !!}
							{!! Form::label('type', 'Type') !!}
						</div>
					</div>
					
					<div class="col-sm-2">
						<div class="form-group">
							{!! Form::text('size', null, ['class'=>'form-control']) !!}
							{!! Form::label('size', 'Size') !!}
						</div>
					</div>
					
					<div class="col-sm-4">
						<div class="form-group">
							<div class="input-group date" id="demo-date-format">
								<div class="input-group-content">
									{!! Form::text('date_live', date("d/m/Y",strtotime($event_material->date_live)), ['class' => 'form-control']) !!}
									{!! Form::label('date_live', 'Date Live') !!}
								</div>
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							{!! Form::label('role_visibility', 'Role visibility') !!}
							{!! Form::select('role_visibility[]', $roles, explode(",", $event_material->role_visibility), ['class'=>'form-control select2-list', 'multiple', 'data-placeholder'=>'Select roles for doc visibility']) !!}
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							{!! Form::label('subrole_visibility', 'Sub Role visibility') !!}
							{!! Form::select('subrole_visibility[]', $sub_roles, explode(",", $event_material->subrole_visibility), ['class'=>'form-control select2-list', 'multiple', 'data-placeholder'=>'Select sub roles for doc visibility']) !!}
						</div>
					</div>
				</div>
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					<a href="{{ URL::previous() }}"><button type="button" class="btn btn-flat">Cancel</button></a>
					{!! Form::submit('UPDATE EVENT MATERIAL', ['class' => 'btn btn-flat btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
</section>
@endsection
