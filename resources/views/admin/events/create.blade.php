@extends('admin.layouts.default')

@section('title')
Add Event
@endsection

@section('content')
<link type="text/css" rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/jquery-ui/jquery-ui-theme.css')}}" />

<section>

<div class="section-header">
	<ol class="breadcrumb">
		<li class="active">New Event</li>
	</ol>
</div>
<div class="section-body">
	
	@include('admin.partials.validationErrors')
	
	<div class="col-md-offset-1 col-md-10">
		<div class="card">
			<div class="card-head style-primary">
				<header>Create New</header>
			</div>
			
			{!! Form::open(['route' => ['events.store'], 'class' => 'form form-validate']) !!}
			<div class="card-body">
				 <div class="row">
				 	<div class="col-md-5">
					 	<div class="form-group floating-label">
							{!! Form::text('title', null, ['class'=>'form-control', 'required']) !!}
							{!! Form::label('title', 'New Event Name') !!}
						</div>
					</div>
						
					<div class="col-md-4">
						<div class="form-group floating-label">
							{!! Form::select('event_type_id', $event_types, null, ['class'=>'form-control']) !!}
							{!! Form::label('event_type_id', 'Event Type') !!}
						</div>							
					</div>					
				 </div>
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					<a href="{{ URL::previous() }}"><button type="button" class="btn btn-flat">Cancel</button></a>
					{!! Form::submit('Add Event', ['class' => 'btn btn-flat btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
	
	<div class="col-md-offset-1 col-md-10">
		<div class="card">
			<div class="card-head style-primary">
				<header>Previous Events</header>
			</div>
			
			@if (!empty($event_not_found))
				<div class="alert alert-danger" role="alert">
					<div>{{$event_not_found}}</div>
				</div>
			@endif
			
			{!! Form::open(['route' => ['events.copy'], 'class' => 'form form-validate', 'required']) !!}
			<div class="card-body">
				 <div class="row">
				 	<div class="col-md-5">
					 	<div class="form-group floating-label">
							{!! Form::text('title', null, ['class'=>'form-control', 'required']) !!}
							{!! Form::label('title', 'New Event Name') !!}
						</div>
					</div>
					
					<div class="col-md-5">
					 	<div class="form-group floating-label">
							{!! Form::text('old_event_name', null, ['class'=>'form-control', 'id' => 'previous_events', 'required']) !!}
							{!! Form::label('old_event_name', 'Old Event Name') !!}
							{!! Form::hidden('old_event_id', null, array('id' => 'old_event_id')) !!}
							<p class="help-block">Type previous event name, that data has to copied</p>
						</div>
					</div>
				</div>
				
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					<a href="{{ URL::previous() }}"><button type="button" class="btn btn-flat">Cancel</button></a>
					{!! Form::submit('Copy Event', ['class' => 'btn btn-flat btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
</section>

<script type="text/javascript">
$(function() {
	$("#previous_events").autocomplete({
        source: function( request, response ) {
        $.ajax({
            url: "{!! route('events.geteventslist') !!}",
            dataType: "json",
            data: {term: request.term},
            success: function(data) {
                        response($.map(data, function(item) {
                        return {
                            label: item.title,
                            id: item.id,
                            };
                    }));
                }
            });
        },
        minLength: 2,
        select: function(event, ui) {
            $('#old_event_id').val(ui.item.id);
        }
    });
});
</script>
@endsection
