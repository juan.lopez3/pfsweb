<div class="form-group floating-label">
    {!! Form::label('visibility', 'Visibility') !!}
    {!! Form::select('visibility', [1=>'Web and Mobile App', 2=>'Web only', 3=>'Mobile App only'], null, ['class' => 'form-control', 'required']) !!}
</div>

<div class="form-group floating-label">
	{!! Form::textarea('question', null, ['class' => 'form-control control-2-rows', 'required']) !!}
	{!! Form::label('question', 'Question') !!}
</div>
<div class="form-group floating-label">
	{!! Form::label('answer', 'Answer') !!}<br/>
	@if(empty($event_id))
	{!! Form::textarea('answer', 'Enter your answer', ['class' => 'form-control control-2-rows', 'id'=>'ckeditor', 'required', 'placeholder'=>'']) !!}
	@else
	{!! Form::textarea('answer', null, ['class' => 'form-control control-2-rows', 'id'=>'ckeditor', 'required', 'placeholder'=>'']) !!}
	@endif
</div>

@if(empty($event_id))
	{!! Form::hidden('event_id', $event->id) !!}
@else
	{!! Form::hidden('event_id', $event_id) !!}
@endif
