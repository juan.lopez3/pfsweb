@extends('admin.layouts.default')

@section('title')
{{$event->title}} Statistics
@endsection

@section('content')

<section>
<div class="section-header">
	<div class="row">
		<div class="col-xs-9"><span class="event-head">{{$event->title}}</span> - {{ date("j F Y",strtotime($event->event_date_from)) }}<br/>
			<span class="glyphicon glyphicon-signal text-danger"></span><span class="event-head"> Statistics</span>
		</div>
		<div class="col-xs-3 text-right">@include('admin.partials.eventActions')</div>
	</div>
</div>
<br/>
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/morris/morris.core.css')}}" />

<div class="section-body">	
	<div class="card">
		<div class="card-body">
			
			<?php
			$datetime1 = new DateTime(date("Y-m-d"));
			$datetime2 = new DateTime($event->event_date_from);
			$interval = $datetime1->diff($datetime2);
			?>

			<b>Event title:</b> {{$event->title}}<br/>
			<b>Event date:</b> {{ date("j F, Y",strtotime($event->event_date_from)) }}<br/>
			@if(date("Y-m-d") <= $event->event_date_from)
			<b>Days to event: </b><?php echo $interval->format('%a days'); ?><br/>
			@endif
			<b>Event region:</b> {{$event->region->title}}<br/>
			
			<h4><b>Event attendees: </b> {{$event->atendees()->where('registration_status_id', \Config::get('registrationstatus.booked'))->count()}}</h4>
			<h4><b>Cancelled registrations: </b> {{$event->atendees()->where('registration_status_id', \Config::get('registrationstatus.cancelled'))->count()}}<br/><br/></h4>
			
			
			<div class="text-center"><h3>Event registrations per day</h3></div>
			@if(sizeof($event_registrations_data))
			<!-- BEGIN MORRIS - LINE CHART -->
			<section class="style-default">
				<div class="section-body">
					<div id="morris-line-graph" class="height-7" data-colors="#9C27B0"></div>
				</div><!--end .section-body -->
			</section>
			<!-- END MORRIS - LINE CHART -->
			@else
			<div class="text-center"><b>There are no existing registrations for this event</b></div>
			@endif
			
			<!-- BEGIN MORRIS - STACKED BAR CHART -->
			
			@if(strlen($sessions_chart_data) > 2)
			<div class="text-center"><h3>Sessions capacities and registrations</h3></div>
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-body">
							<div id="morris-stacked-bar-graph" class="height-7" data-colors="#FF0000,#00CC00,#0066FF"></div>
						</div>
					</div>
				</div>
			</div>
			@endif
			<!-- END MORRIS - STACKED BAR CHART -->
		</div>
	</div>
</div>

</section>

<script src="{{asset('assets/admin/js/libs/raphael/raphael-min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/morris.js/morris.min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/d3/d3.v3.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/rickshaw/rickshaw.min.js')}}"></script>

<script type="text/javascript">

	// Morris line demo
	if ($('#morris-line-graph').length > 0) {
		window.m = Morris.Line({
			element: 'morris-line-graph',
			data: {!!$event_registrations_data!!},
			xkey: 'date',
			ykeys: ['registrations'],
			labels: ['Registrations'],
			parseTime: false,
			resize: true,
			lineColors: $('#morris-line-graph').data('colors').split(','),
			xLabelMargin: 10,
			integerYLabels: true
		});
	}


	// Morris stacked bar demo
	if ($('#morris-stacked-bar-graph').length > 0) {
		Morris.Bar({
			element: 'morris-stacked-bar-graph',
			data: {!!$sessions_chart_data!!},
			xkey: 'x',
			ykeys: ['booked', 'remaining', 'waitlisted'],
			labels: ['Booked', 'Remaining', 'Waitlisted'],
			stacked: true,
			barColors: $('#morris-stacked-bar-graph').data('colors').split(',')
		});
	}
</script>

@endsection
