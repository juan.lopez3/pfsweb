@extends('admin.layouts.default')

@section('title')
	Event List
@endsection

@section('content')

<script type="text/javascript">
	$(document).ready(function() {
	    $.fn.dataTable.moment( 'D/M/YYYY' );
		$('#event_list').dataTable({
			"order": [[ 6, "asc" ]],
			"iDisplayLength": 50,
			"columnDefs": [
			    { "width": "28%", "targets": 8,  "searchable": false }
			  ]
		});
	} );
</script>

<section>
<div class="section-header">
	<ol class="breadcrumb">
		<li class="active">Event List</li>
	</ol>
</div>
@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
<p><a href="{{route('events.create') }}"<button type="button" class="btn ink-reaction btn-raised btn-success">ADD Event</button></a></p>
@endif
<div class="section-body">	
	<div class="card">
		<div class="card-body">
			<table id="event_list" class="table table-striped table-responsive" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Venue Name</th>
						<th>Postcode</th>
						<th>Region</th>
						<th>City</th>
						<th>Event Date</th>
						<th>Attending</th>
						<th>Actions</th>
					</tr>
				</thead>
			
				<tfoot>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Venue Name</th>
						<th>Postcode</th>
						<th>Region</th>
						<th>City</th>
						<th>Event Date</th>
						<th>Attending</th>
						<th>Actions</th>
					</tr>
				</tfoot>
			
				<tbody>
					@foreach ($events as $event)
					<tr>
						<td>{{ $event->id }}</td>
						<td>(@if($event->publish)<span class="green" title="Active">A</span>@else<span class="red" title="Not Active">N</span>@endif) {{ $event->title }}</td>
						@if(!isset($event->venue))
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						@else
							<td>{{ $event->venue->name}}</td>
							<td>{{ $event->venue->postcode}}</td>
							<td>@if(!empty($event->region)){{ $event->region->title}}@endif</td>
							<td>{{ $event->venue->city}}</td>
						@endif
						
						<td>{{ date("d/m/Y",strtotime($event->event_date_from))}}</td>
						<th>{{$event->atendees()->where('registration_status_id', config('registrationstatus.booked'))->whereIn('role_id', [role('member'), role('non_member'), role('uploaded_attendee'), role('event_manager')])->count()}}</th>
						<td>
						    @if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator'), role('host')]))
								<a href="{{ route('events.edit', $event->id) }}"><button type="button" class="btn btn-flat btn-warning btn-sm" title="Settings"><span class="glyphicon glyphicon-cog"></span></button></a>
							@endif
							@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator'), role('host'), role('regional_coordinator')]))
								<a href="{{ route('events.atendees', $event->id) }}"><button type="button" class="btn btn-flat btn-success btn-sm" title="Attendees"><span class="glyphicon glyphicon-user"></span></button></a>
							@endif
							@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
							<a href="{{ route('events.costs', $event->id) }}"><button type="button" class="btn btn-flat btn-info btn-sm" title="Costs"><span class="glyphicon glyphicon-gbp"></span></button></a>
							<a href="{{ route('events.scheduleBuilder', $event->id) }}"><button type="button" class="btn btn-flat btn-accent-dark btn-sm" title="Schedule"><span class="glyphicon glyphicon-time"></span></button></a>
							@endif
							@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator'), role('host')]))
								<a href="{{ route('events.onsiteDetails', $event->id) }}"><button type="button" class="btn btn-flat btn-accent-light btn-sm" title="On site Details"><span class="glyphicon glyphicon-th"></span></button></a>
								<a href="{{ route('events.emails.filter', $event->id) }}"><button type="button" class="btn btn-flat btn-primary btn-sm" title="Email attendees"><span class="glyphicon glyphicon-envelope"></span></button></a>
								<a href="{{ route('events.statistics', $event->id) }}"><button type="button" class="btn btn-flat btn-danger btn-sm" title="Statistics"><span class="glyphicon glyphicon-signal"></span></button></a>
							@endif
							@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator'), role('host'), role('venue_staff'), role('regional_coordinator')]))
								<a href="{{ route('events.onsite', $event->id) }}"><button type="button" class="btn btn-flat btn-success btn-sm" title="On site LIVE"><span class="glyphicon glyphicon-play"></span></button></a>
							@endif
							@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
								<a href="{{ route('events.archive', $event->id) }}"><button type="button" class="btn btn-flat btn-info btn-sm" title="Costs">Archive</button></a>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
</section>

@include('admin.partials.deleteConfirmation')

@endsection
