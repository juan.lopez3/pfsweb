@if (isset($site_tabs[0]) && $site_tabs[0]['checked'])

<div class="tab-pane" id="settings">
<script type="text/javascript">
$(function() {
	$( "#slider-range-min" ).slider({
		range: "min",
		value: {{ $event->notification_level }},
		min: 0,
		max: 100,
		step: 5,
		slide: function( event, ui ) {
			$( "#notification_level" ).val( ui.value + "%" );
		}
	});
	
	$( "#notification_level" ).val( $( "#slider-range-min" ).slider( "value" ) + "%" );
});
</script>
<script type="text/javascript" src="{{asset('assets/admin/js/libs/bootstrap-datepicker/bootstrap-datetimepicker.js')}}"></script>

	<header>
		<h3 class="opacity-75">Event Details</h3>
	</header>
	@include('admin.partials.validationErrors')
	
	{!! Form::model($event, ['route' => ['events.update', $event->id], 'id'=>'event_settings_form', 'class' => 'form-horizontal form-validate', 'files' => true]) !!}
	
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				{!! Form::label('title', 'Event Name', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-9">
					{!! Form::text('title', null, ['class'=>'form-control', 'required']) !!}
				</div>
			</div>
			
			<div class="form-group">
                {!! Form::label('theme_id', 'Theme', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-9">
                    {!! Form::select('theme_id', $themes, null, ['class'=>'form-control', 'required']) !!}
                </div>
            </div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<div class="input-daterange input-group" id="demo-date-range">
					{!! Form::label('event_data_from', 'Event Date(s)', ['class' => 'control-label']) !!}
					<div class="input-group-content">
						@if (empty($event->event_date_from))
							{!! Form::text('event_date_from', null, ['class' => 'form-control', 'required']) !!}
						@else
							{!! Form::text('event_date_from', date("d/m/Y",strtotime($event->event_date_from)), ['class' => 'form-control', 'required']) !!}
						@endif
					</div>
					<span class="input-group-addon">to</span>
					<div class="input-group-content">
						@if (empty($event->event_date_to))
							{!! Form::text('event_date_to', null, ['class' => 'form-control']) !!}
						@else
							{!! Form::text('event_date_to', date("d/m/Y",strtotime($event->event_date_to)), ['class' => 'form-control']) !!}
						@endif
						<div class="form-control-line"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-sm-4">
			<div class="form-group">
				{!! Form::label('event_type_id', 'Event Type', ['class'=>'col-sm-4 control-label']) !!}
				<div class="col-sm-8">
					{!! Form::select('event_type_id', $event_types, null, ['class'=>'form-control', 'required']) !!}
				</div>							
			</div>
		</div>
		<div class="col-sm-2">
			<div class="input-group date" id="demo-date">
				<div title="This will disable Booking Now button after this date." class="input-group-content">
					@if(!empty($event->cut_of_date))
					{!! Form::text('cut_of_date', date("d/m/Y",strtotime($event->cut_of_date)), ['class' => 'form-control']) !!}
					@else
					{!! Form::text('cut_of_date', null, ['class'=>'form-control']) !!}
					@endif
					<p title="This will disable Booking Now button after this date." class="help-block">Cut off Date</p>
				</div>
				<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
			</div>
		</div>
		<div class="col-sm-2">
            <div class="form-group">
                <div class="input-group date" id="amendment-date">
                    <div class="input-group-content">
                        @if(!empty($event->amendment_date))
                        {!! Form::text('amendment_date', date("d/m/Y",strtotime($event->amendment_date)), ['class' => 'form-control']) !!}
                        @else
                        {!! Form::text('amendment_date', null, ['class'=>'form-control']) !!}
                        @endif
                        <p class="help-block">Amendment Date</p>
                    </div>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </div>
        </div>
        
        <div class="col-sm-2">
            <div class="form-group">
                <div class="input-group date" id="amendment-date">
                    <div class="input-group-content">
                        @if(!empty($event->late_cancellation_date))
                        {!! Form::text('late_cancellation_date', date("d/m/Y H:i",strtotime($event->late_cancellation_date)), ['class' => 'form-control', 'id' => 'datetimepicker5']) !!}
                        @else
                        {!! Form::text('late_cancellation_date', null, ['class'=>'form-control', 'id'=>'datetimepicker5']) !!}
                        @endif
                        <p class="help-block">Late Cancellation Date</p>
                    </div>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </div>
        </div>
	</div>

	<div class="row" style="display:none;">
		<div class="col-sm-4">
			<div class="form-group">
				{!! Form::label('capacity', 'Event Capacity', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-8">
					{!! Form::input('number', 'capacity', null, ['class'=>'form-control']) !!}
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-4">
			<div class="form-group">
				{!! Form::label('price', 'Price (£)', ['class' => 'col-sm-4 control-label']) !!}
				<div class="col-sm-8">
					{!! Form::text('price', null, ['class'=>'form-control']) !!}
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="form-group">
				<div class="col-sm-8">
					{!! Form::text('price_nonmember', null, ['class'=>'form-control']) !!}
                    <p class="help-block">Price Non-Members (£)</p>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-4">
			<div class="form-group">
				{!! Form::label('payment_enabled', 'Payment Gateway', ['class' => 'col-sm-4 control-label']) !!}
					<div class="checkbox checkbox-styled">
							{!! Form::checkbox('payment_enabled', true) !!}
					</div>
			</div>
		</div>
		<div class="col-sm-8">
			<div class="form-group">
				<div class="input-group-content">
					@if(!empty($event->manually_cpd))
					{!! Form::text('manually_cpd', $event->manually_cpd, ['class' => 'form-control']) !!}
					@else
					{!! Form::text('manually_cpd', null, ['class'=>'form-control']) !!}
					@endif
                    <p class="help-block">CPD Hours</p>
				</div>
			</div>
		</div>
	</div>

    <script type="text/javascript">
        $(function () {
            $('#datetimepicker5').datetimepicker({
                format : 'DD/MM/YYYY HH:mm',
                stepping: 5
            });
        });
    </script>

	<br/>
	<div class="row">
		<div class="col-sm-12">
			<div class="form-group">
				{!! Form::label('description', 'Event Description', ['class'=>'col-sm-2 control-label']) !!}
				<div class="col-sm-10">
					{!! Form::textarea('description', null, ['row'=>'3', 'style'=>'height: 200px;', 'id'=>'event_description']) !!}
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-sm-12">
			<div class="form-group">
				{!! Form::label('registration_text', 'Registration Text', ['class'=>'col-sm-2 control-label']) !!}
				<div class="col-sm-10">
					{!! Form::textarea('registration_text', null, ['row'=>'2', 'style'=>'height: 100px;', 'id'=>'registration_text']) !!}
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-sm-5">
			<div class="form-group">
				{!! Form::label('region_id', 'Region', ['class'=>'col-sm-3 control-label']) !!}
				<div class="col-sm-9">
					{!! Form::select('region_id', $regions, null, ['class'=>'form-control select2-list']) !!}
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="form-group">
				{!! Form::label('county', 'County', ['class' => 'col-sm-3 control-label']) !!}
				<div class="col-sm-9">
					{!! Form::text('county', null, ['class'=>'form-control']) !!}
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				{!! Form::label('hashtag', 'Hashtags', ['class' => 'col-xs-3 control-label']) !!}
				<div class="col-xs-9">
					{!! Form::text('hashtag', null, ['class'=>'form-control', 'data-role'=>'tagsinput']) !!}
					<p class="help-block">Push enter to separate tags. For example: #PFS, @PFS</p>
				</div>
			</div>
		</div>
	</div>
	
	<header>
		<h3 class="opacity-75">Event Site Functions</h3>
	</header>
	
	<div class="row">
		<div class="col-md-6 col-sm-5">
			<div class="form-group">
				<label class="col-sm-3 control-label">Site</label>
				<div class="col-sm-9">
					@foreach($site_tabs as $tab)
						
						<div class="checkbox checkbox-styled">
							<label>
								{!! Form::checkbox('site_functions[]', $tab['id'], $tab['checked']) !!}
								{!! Form::label($tab['title'], $tab['title']) !!}
							</label>
						</div>
					@endforeach
				</div>
			</div>
		</div>
		<div class="col-md-6 col-sm-6">
			<div class="form-group">
				{!! Form::label('publish', 'Publish Event', ['class' => 'col-sm-2 control-label']) !!}
				<div class="col-sm-10">
					<div class="checkbox checkbox-styled">
						<label>
							{!! Form::checkbox('publish', true) !!}
						</label>
					</div>
				</div>
			</div>
			
			<div class="form-group">
                {!! Form::label('invite_only', 'Invite only', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    <div class="checkbox checkbox-styled">
                        <label>
                            {!! Form::checkbox('invite_only', true) !!}
                        </label>
                    </div>
                </div>
            </div>
			
			<div class="form-group">
                {!! Form::label('app_enabled', 'Mobile App', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    <div class="checkbox checkbox-styled">
                        <label>
                            {!! Form::checkbox('app_enabled', true) !!}
                        </label>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                {!! Form::label('local_committee', 'Local Committee', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    <div class="checkbox checkbox-styled">
                        <label>
                            {!! Form::checkbox('local_committee', true) !!}
                        </label>
                    </div>
                </div>
            </div>
            
			<div class="form-group">
				{!! Form::label('cpd_download', 'Agenda download', ['class' => 'col-sm-2 control-label']) !!}
				<div class="col-sm-10">
					<div class="checkbox checkbox-styled">
						<label>
							{!! Form::checkbox('cpd_download', true) !!}
						</label>
					</div>
				</div>
			</div>

			<div class="form-group">
				{!! Form::label('agenda_pdf', 'Upload agenda PDF', ['class' => 'col-sm-2 control-label']) !!}
				<div class="col-sm-10">
					@if (!empty($event->agenda_pdf))
                        <div>
						{!! Form::hidden('agenda_pdf', null, ['id' => 'agenda_pdf']) !!} <button type="button" id="delete_agenda_pdf" class="btn btn-danger btn-xs fa fa-trash" title="Delete Uploaded pdf agenda"></button>
						<a href="{{asset('uploads/agendas/'.$event->agenda_pdf)}}">{{$event->agenda_pdf}}</a>
                        </div>
					@endif
					{!! Form::file('agenda_pdf_upload', ['class' => 'form-control']) !!}
				</div>
			</div>
			
			<div class="form-group">
				{!! Form::label('invitation_allowed', 'Invitation allowed', ['class' => 'col-sm-2 control-label']) !!}
				<div class="col-sm-10">
					<div class="checkbox checkbox-styled">
						<label>
							{!! Form::checkbox('invitation_allowed', true) !!}
						</label>
					</div>
				</div>
			</div>
			
			<div class="form-group">
				{!! Form::label('registration_type_id', 'Registration type', ['class' => 'col-sm-2 control-label']) !!}
				<div class="col-sm-4">
					{!! Form::select('registration_type_id', [1=> 'Add to waiting list', 2 => 'Accept all'], null, ['class'=>'form-control']) !!}
				</div>
				<div class="col-sm-2">
					Notification level
				</div>
				<div class="col-sm-3">
					<input type="text" name="notification_level" id="notification_level" readonly style="border:0; color:#f6931f; font-weight:bold;">
					<div id="slider-range-min"></div> 
				</div>
			</div>
			
			<div class="form-group">
				{!! Form::label('browser_title', 'Browser Title', ['class' => 'col-sm-2 control-label']) !!}
				<div class="col-sm-10">
					{!! Form::text('browser_title', null, ['class'=>'form-control', 'required']) !!}
				</div>
			</div>
			
			<div class="form-group">
				{!! Form::label('outside_link', 'External Link', ['class' => 'col-sm-2 control-label']) !!}
				<div class="col-sm-10">
					{!! Form::text('outside_link', null, ['class'=>'form-control']) !!}
					<p class="help-block">Starting with http://...</p>
				</div>
			</div>
			
			<div class="form-group">
				{!! Form::label('tfi_contact_id', 'Event Contact', ['class' => 'col-sm-2 control-label']) !!}
				<div class="col-sm-10">
					{!! Form::select('tfi_contact_id', [''=>'Select TFI contact'] + $tfi_staff, null, ['class'=>'form-control select2-list', 'required']) !!}
				</div>
			</div>
			
		</div>
	</div>
	
	<hr class="ruler">
	
	<header>
		<h3 class="opacity-75">Event Branding</h3>
	</header>
	
	<div class="row">
		@if(empty($event->banner))
		<div class="col-md-5">
			<div class="form-group">
				{!! Form::label('banner', 'Event Banner', ['class' => 'col-md-3 control-label']) !!}
				<div class="col-md-2">
					<div id="event_banner_upload" style="cursor: pointer;">Upload</div>
					<div id="status"></div>
				</div>
			</div>
		</div>
		@endif
		<div class="col-sm-4 col-xs-12">
			@if(!empty($event->banner))
				<div class="holder">
					<div class="overlay overlay-default">
						<button type="button" class="btn btn-danger btn-xs" id="delete_banner" title="Delete">Remove</button>
					</div>
					{!! HTML::image('uploads/images/'.$event->banner,'', ['class'=>'img-responsive']) !!}
				</div>
			@endif
		</div>
	</div>
	
	
	<header>
        <h3 class="opacity-75">Mobile Application Branding</h3>
    </header>
    
    <div class="row">
        @if(empty($event->app_banner))
        <div class="col-md-5">
            <div class="form-group">
                {!! Form::label('app_banner', 'Mobile Application Banner', ['class' => 'col-md-5 control-label']) !!}
                <div class="col-md-2">
                    <div id="event_app_banner_upload" style="cursor: pointer;">Upload</div>
                    <div id="status1"></div>
                </div>
            </div>
        </div>
        @endif
        <div class="col-sm-4 col-xs-12">
            @if(!empty($event->app_banner))
                <div class="holder" style="min-height: 50px;">
                    <div class="overlay overlay-default">
                        <button type="button" class="btn btn-danger btn-xs" id="delete_app_banner" title="Delete">Remove</button>
                    </div>
                    {!! HTML::image('uploads/app/'.$event->app_banner,'', ['class'=>'img-responsive']) !!}
                </div>
            @endif
        </div>
    </div>

	<header>
		<h3 class="opacity-75">Floor Plan</h3>
	</header>
	
	<div class="row">
		@if(empty($event->floor_plan))
		<div class="col-md-5">
			<div class="form-group">
				{!! Form::label('banner', 'Floor Plan', ['class' => 'col-md-3 control-label']) !!}
				<div class="col-md-2">
					<div id="event_flor_plan_upload" style="cursor: pointer;">Upload</div>
					<div id="status"></div>
				</div>
			</div>
		</div>
		@endif
		<div class="col-sm-4 col-xs-12">
			@if(!empty($event->floor_plan))
				<div class="holder" style="min-height: 50px;">
					<div class="overlay overlay-default">
						<button type="button" class="btn btn-danger btn-xs" id="delete_for_plan" title="Delete">Remove</button>
					</div>
					{!! HTML::image('uploads/files/'.$event->floor_plan,'', ['class'=>'img-responsive']) !!}
				</div>
			@endif
		</div>
	</div>
	
	
	@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
	<div class="card-actionbar">
		<div class="card-actionbar-row">
			{!! Form::submit('Save Settings', ['class' => 'btn ink-reaction btn-raised btn-primary']) !!}
		</div>
	</div>
	@endif
	
	{!! Form::close() !!}
	
	@include('admin.partials.readonly', ['form_id'=>'event_settings_form'])
	
	@include('admin.events._tabNotes', array('tab_id' => 1, 'notes' => $event->tabs()->where('tab_id', 1)->first()->notes()->where('event_id', $event->id)->orderby('created_at', 'DESC')->get()))
	
<script type="text/javascript">

$(document).ready(function(){
$('#event_description').ckeditor({});
$('#registration_text').ckeditor({
	'height': 80
});

    // event branding
    var settings = {
        url: "{!! route('events.uploadBanner') !!}",
        formData: {  
    	   "_token": "{{ csrf_token() }}",
    	   "event_id": "{{ $event->id }}"
    	},
        dragDrop:true,
        maxFileSize: 800000,
        fileName: "myfile",
        allowedTypes:"jpg,png,gif,bmp,jpeg",	
        returnType:"json",
    	 onSuccess:function(files,data,xhr)
        {
           // alert((data));
           location.reload();
        },
        showDelete:false
    }
	
	$("#event_banner_upload").click(function(){
		var uploadObj = $("#event_banner_upload").uploadFile(settings);
	})


	$('#delete_banner').bind('click', function (event) {
        $.ajax({
            url: "{{ route('events.deleteBanner', $event->id) }}",
            success: function () {
                location.reload();
            }
        });
    });

    // mobile app branding
    var settings_app = {
        url: "{!! route('events.uploadAppBanner') !!}",
        formData: {  
           "_token": "{{ csrf_token() }}",
           "event_id": "{{ $event->id }}" 
        },
        dragDrop:true,
        maxFileSize: 1000000,
        fileName: "myfile",
        allowedTypes:"jpg,png,gif,bmp,jpeg",    
        returnType:"json",
        onSuccess:function(files,data,xhr)
        {
           // alert((data));
           location.reload();
        },
        showDelete:false
    }
    
    $("#event_app_banner_upload").click(function(){
        var uploadObj = $("#event_app_banner_upload").uploadFile(settings_app);
    })


    $('#delete_app_banner').bind('click', function (event) {
        $.ajax({
            url: "{{ route('events.deleteAppBanner', $event->id) }}",
            success: function () {
                location.reload();
            }
        });
    });

	 // mobile app Floor plan
	 var settings_app_floor = {
        url: "{!! route('events.uploadFloorPlan') !!}",
        formData: {  
           "_token": "{{ csrf_token() }}",
           "event_id": "{{ $event->id }}" 
        },
        dragDrop:true,
        maxFileSize: 3500000,
        fileName: "myfile",
        allowedTypes:"jpg,png,gif,bmp,jpeg",    
        returnType:"json",
        onSuccess:function(files,data,xhr)
        {
           // alert((data));
           location.reload();
        },
        showDelete:false
    }

	 $("#event_flor_plan_upload").click(function(){
        var uploadObj = $("#event_flor_plan_upload").uploadFile(settings_app_floor);
    })


    $('#delete_for_plan').bind('click', function (event) {
        $.ajax({
            url: "{{ route('events.removeFloorPlan', $event->id) }}",
            success: function () {
                location.reload();
            }
        });
    });

    $("#delete_agenda_pdf").bind('click', function() {
        $("#agenda_pdf").val('');
        this.parentNode.remove();
    })
});
</script>
</div>
@endif
