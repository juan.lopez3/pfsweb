@if (isset($site_tabs[7]) && $site_tabs[7]['checked'])
<div class="tab-pane" id="sponsors">
	<header>
		<h3 class="opacity-75">Sponsors</h3>
	</header>
	
	@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
	<i>Add sponsors for this event. Sponsor companies and logos must be created in the sponsors section.<br/>
	If you would like this event and other events in this event type to automatically show the pre-fixed quartely sponsors, please add this event type to the Quartely sponsor settings</i>
	<br/>
	
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-lg-4">
			<h4>Add Sponsor</h4>
			{!! Form::open(['route' => ['events.addSponsor', $event->id], 'class' => 'form form-validate']) !!}
			<div class="col-xs-10 col-sm-11 col-md-8">{!! Form::select('sponsor_id', $sponsors, null, ['class'=>'form-control select2-list']) !!}</div>
			<div class="col-xs-1 col-md-1">{!! Form::submit('+', ['class' => 'btn ink-reaction btn-raised btn-sm btn-success']) !!}</div>
			{!! Form::close() !!}
		</div>
	</div>
	@endif
	<div class="row">
		@foreach($event->sponsors as $sponsor)
			<div class="col-xs-5 col-sm-4 col-md-3">
				<div class="card">
					<div class="card-body text-center">
						<div class="holder">
							@if (empty($sponsor->logo))
								{!! HTML::image('uploads/no_image.png','', ['class'=>'img-responsive']) !!}
							@else
								{!! HTML::image('uploads/sponsors/'.$sponsor->logo,'', ['class'=>'img-responsive']) !!}
							@endif
						</div>
						@if(!empty($sponsor->website))
						<h4><a targer="_blank" href="{{$sponsor->website}}">{{$sponsor->name}}</a></h4>
						@else
						<h4>{{$sponsor->name}}</h4>
						@endif
						{!! Form::select('type', ['Sponsor', 'Exhibitor'], $sponsor->pivot->type, ['class'=>'form-control event-sponsor', 'id'=>'es-'.$sponsor->pivot->id]) !!}
						
						{!! Form::text('stand_number', $sponsor->pivot->stand_number, ['class'=>'form-control stand-no', 'placeholder' => 'Stand no.', 'id'=>'esstand-'.$sponsor->pivot->id]) !!}
						{!! Form::text('category_1', $sponsor->pivot->category_1, ['class'=>'form-control category_1', 'placeholder' => 'Category 1', 'id'=>'cat1-'.$sponsor->pivot->id]) !!}
						{!! Form::text('category_2', $sponsor->pivot->category_2, ['class'=>'form-control category_2', 'placeholder' => 'Category 2', 'id'=>'cat2-'.$sponsor->pivot->id]) !!}
						{!! Form::text('order',      $sponsor->order,      ['class'=>'form-control order',      'placeholder' => 'Order',      'id'=>'ord-'.$sponsor->pivot->id]) !!}
                    </div>
                    <div class="card-actionbar-row">
						<button data-href="{{ route('events.destroySponsor', [$event->id, $sponsor->id]) }}" type="button" class="btn ink-reaction btn-raised btn-xs btn-danger" title="Delete" data-toggle="modal" data-target="#confirm-delete">Delete Sponsor</button>
					</div>
				</div>
			</div>
		@endforeach
	</div>
	
	@if ($event->eventType->exhibition)
	<header>
        <h3 class="opacity-75">Exhibition settings</h3>
    </header>
    
    <div class="form-group floating-label col-xs-12 col-md-4">
        {!! Form::label('floor_plan', 'Floor plan') !!}
        <br/>
        <span id="floor_plan_p">
        @if(!empty($event->floor_plan))
            {!! HTML::image('uploads/files/'.$event->floor_plan,'', ['class'=>'img-responsive']) !!}
            <a href="{{route('events.removeFloorPlan', $event->id)}}" class="btn btn-xs btn-danger">Remove FLOOR PLAN</a>
        @endif
        </span>
        <div id="floor_plan_upload">Upload</div>
        <div id="status"></div>
    </div>
    
<script type="text/javascript">
$(document).ready(function(){
    
    $(".stand-no").change(function(){
        ids = this.id.split("-");
        
        $.ajax({
            url: "{!! route('events.sponsors.updateStandNumber') !!}",
            type: "POST",
            data: {
                 "_token": "{{ csrf_token() }}",
                   "sponsor_id": ids[1], 
                   "stand_number": $("#"+this.id).val(),
            },
            success: function () {
                //location.reload();
            }
        });
    });

    /** categorires
     *
     */
    $(".category_1").change(function(){
        ids = this.id.split("-");

        $.ajax({
            url: "{!! route('events.sponsors.updateCategory', 'category_1') !!}",
            type: "POST",
            data: {
                "_token": "{{ csrf_token() }}",
                "sponsor_id": ids[1],
                "category": $("#"+this.id).val(),
            },
            success: function () {
                //location.reload();
            }
        });
    });

    $(".category_2").change(function(){
        ids = this.id.split("-");

        $.ajax({
            url: "{!! route('events.sponsors.updateCategory', 'category_2') !!}",
            type: "POST",
            data: {
                "_token": "{{ csrf_token() }}",
                "sponsor_id": ids[1],
                "category": $("#"+this.id).val(),
            },
            success: function () {
                //location.reload();
            }
        });
    });
               
    $(".order").change(function(){
        ids = this.id.split("-");

        $.ajax({
            url: "{!! route('events.sponsors.updateCategory', 'order') !!}",
            type: "POST",
            data: {
                "_token": "{{ csrf_token() }}",
                "sponsor_id": ids[1],
                "category": $("#"+this.id).val(),
            },
            success: function () {
                //location.reload();
            }
        });
    });

    $(".event-sponsor").change(function(){
        ids = this.id.split("-");
        
        $.ajax({
            url: "{!! route('events.sponsors.updateType') !!}",
            type: "POST",
            data: {
                 "_token": "{{ csrf_token() }}",
                   "sponsor_id": ids[1], 
                   "type": $("#"+this.id).val(),
            },
            success: function () {
                //location.reload();
            }
        });
    });
    
    //event branding
    var settings_floor = {
        url: "{!! route('events.uploadFloorPlan', $event->id) !!}",
        formData: {  
           "_token": "{{ csrf_token() }}"
        },
        dragDrop:true,
        maxFileSize: 3500000,
        fileName: "myfile",
        allowedTypes:"jpg,png,gif,bmp,jpeg,pdf",    
        returnType:"json",
         onSuccess:function(files,data,xhr)
        {
           //alert((data));
           $('#floor_plan_p').html('<img src="{{\Config::get("app.url")}}uploads/files/'+data+'" class="img-responsive" />');
           
           //location.reload();
        },
        showDelete:false
    }
    var uploadObj = $("#floor_plan_upload").uploadFile(settings_floor);
});
</script>

    @endif
</div>
@endif