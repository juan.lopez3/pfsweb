<!DOCTYPE html>
<html>
<head>
    <title>Attendance Certificate</title>
<style type="text/css">
    @font-face {
      font-family: 'arial';
      src: url('{{config("app.root")}}storage/fonts/Arial.ttf') format('truetype'),
            url('{{config("app.root")}}storage/fonts/Arial.woff') format('woff');
    }

    html{margin:15px 40px 10px}

     body {font-family: 'arial', 'Helvetica'; font-size: 12pt; color: #192387;}
     .small {font-size: 8pt}
     .footer .small {line-height:9px}
     br{margin-top:2px;}
     .medium {font-size: 10pt;}
     h2 {font-size: 14pt; margin-bottom: 0px; font-family: 'arial', 'Helvetica'}
     .heading{
         font-size: 14pt; margin-bottom: 0px; padding-top:0px; 
         margin-top:2px; font-family: 'arial', 'Helvetica'}
     hr.original{ background-color: #192387; height: 3px}
     hr.custom { background-color: #C6C8E3; border: #C6C8E3; height: 3px;}
     thead {background-color: #192387; color: #FFF; font-size: 8pt; margin-bottom: 4px; font-family: 'arial', 'Helvetica'}
     table {width: 100%; font-size: 8pt; border-spacing: 0;}
     table.feedback tr td {border-bottom: 1px solid #192387;}
     table.feedback thead tr td {padding: 5px;}
     table.sessions{border-top: 1px solid #192387;}
     table.feedback tbody {color: black; font-family: 'arial', 'Helvetica'}
     .rating{word-spacing: 18px; padding-left: 10px;}
     .bb {border-bottom: 1px solid #192387;}
     .br {border-right: 1px solid #192387;}
     .br-white {border-right: 1px solid #FFF;}
     .bold-condensed {font-family: 'arial', 'Helvetica'}
     .meta-plus {font-family: 'arial', 'Helvetica'}
     .no-break{page-break-inside: avoid;}
     .withoutmargin{margin-top:0;padding:0;line-height:8px;}
     .contributor{margin-top:0px;}
     .font_bold{
        font-weight:bold;
        font-size: 13.5px;
     }
     
     p {margin-top: 0px; margin-bottom: 5px;}
</style>
</head>
<body>

<table>
    <tr>
        <td width="50%">
            <span class="bold-condensed" style="font-size: 12pt">Important notes:</span><br />
            <span class="small">Please complete all sections of this form in BLOCK CAPITALS and leave it on your chair or handto one of the event organisers or Local Committee Members.
            </span>
        </td>
        <td align="right">
            <img height="110px" src="images/cii_logo.png" />
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td align="right"><span class="bold-condensed" style="font-size: 22pt;">Feedback form</span></td>
    </tr>
</table>
<hr class="original" />
<p class="heading">Section A - Event details</p>
<span class="medium" style="color: black; font-family: 'arial', 'Helvetica'">{{$event->title}}, {{date("l d M Y", strtotime($event->event_date_from))}}@if(sizeof($event->venue)), {{$event->venue->name}}@endif</span>

<p class="heading">Section B - Personal details</p>
<span class="small" style="font-family: 'arial', 'Helvetica'">By providing your contact details you are giving permission for them to be shared with our Partners in Professionalism where requested by you in the feedback form below.</span>
<br/>
<img height="130px" src="images/profile_details.jpg" />


<p class="heading">Section C - Feedback on today's sessions</p>

<table class="feedback">
    <thead>
        <tr>
            <td width="32%" class="br-white">Session</td>
            <td width="18%" class="br-white">Request contact from speaker/company</td>
            <td colspan="2">Please rate the following session between 1 (poor) and 10 (excellent) by speaker and content:</td>
        </tr>
    </thead>
</table>

    @foreach ($slots as $slot)
    @foreach($slot->sessions as $session)
    <?php if (!$session->type->include_in_feedback) continue; ?>
    <div class="no-break">
    <table class="feedback sessions">
    <tbody>
        <tr>
            <td rowspan="3" valign="top" class="br medium" width="32%">
                <span class="font_bold" style="font-family: 'arial', 'Helvetica'">
                {{date("H:i", strtotime($session->session_start))}} - {{date("H:i", strtotime($session->session_end))}} {!!$session->title!!} <br/>
                </span> <br>
                <div style="line-height: 9px; font-size:11px"> 
                @foreach($session->contributors as $contributor)
                <div class="contributor">
                {{$contributor->first_name}} {{$contributor->last_name}}, {{$contributor->company}}
                    @if($contributor != end($session->contributors))
                        
                    @endif
                    </div>
                    @endforeach
     
                </div>
            </td>
            <td rowspan="2" width="18%" align="center" valign="middle" class="br"><img src="images/checkbox.jpg" /></td>
            <td width="12%" class="br">Speaker rating</td>
            <td class="rating">1 2 3 4 5 6 7 8 9 10</td>
        </tr>
        <tr>
            <td class="br">Content rating</td>
            <td class="rating">1 2 3 4 5 6 7 8 9 10</td>
        </tr>
        <tr>
            <td colspan="3" height="50px" valign="top" style="line-height: 8px;">
                @if (empty($session->question))
                Feedback comments/reason for contact request:
                @else
                {{$session->question}}
                @endif
            </td>
        </tr>
    </tbody>
    </table>
    </div>
    @endforeach
    @endforeach
    



<div class="no-break">
<p class="heading">Section D</p>
<table class="feedback">
    <!-- <thead>
        <tr>
            <td colspan="2">Please rate the committee between 1 (poor) and 10 (excellent)</td>
        </tr>
    </thead> -->
    <tbody>
        <tr>
            <td width="82%" class="br">Have you recently renewed your PII?</td>
            <td class="rating">Yes    &nbsp;&nbsp;&nbsp;No</td>
        </tr>
        <tr>
            <td width="82%" class="br">If you have, have your PII premiums increased due to pension transfers? <br> If yes, please specify the percentage increase</td>
            <td class="rating">Yes    &nbsp;&nbsp;&nbsp;No<br>&nbsp;&nbsp;&nbsp;&nbsp;%</td>
            
        </tr>
        <tr>
            <td width="82%" class="br">Have you had pension transfers specifically excluded at PII renewal?</td>
            <td class="rating">Yes    &nbsp;&nbsp;&nbsp;No</td>
            
        </tr>
        <tr>
            <td width="82%" class="br">Have you been refused PII because of transfers</td>
            <td class="rating">Yes    &nbsp;&nbsp;&nbsp;No</td>
            
        </tr>
    </tbody>
</table>
</div>


<div class="no-break">
<p class="heading">Section E</p>
<span class="small" style="color:black; font-family: 'arial', 'Helvetica'">Recent widespread publicity surrounding high profile pension problems such as British Steel, have driven some advisers to stop giving DB pension advice.</small>
<table class="feedback">
    <tbody>
        <tr>
            <td width="82%" class="br">
                Have you recently decided against offering DB pension tranfer advice?</td>
            <td class="rating">Yes   &nbsp;&nbsp;&nbsp;No</td>
        </tr>
        <tr>
            <td width="82%" class="br">If yes, is this because of <br> New FCA rules?</td>
            <td class="rating">Yes   &nbsp;&nbsp;&nbsp;No</td>
            
        </tr>
        <tr>
            <td width="82%" class="br">Concerns over maintaining PII cover?</td>
            <td class="rating">Yes  &nbsp;&nbsp;&nbsp;No</td>
            
        </tr>
        <tr>
            <td width="82%" class="br">PFS guidance on insistent client?</td>
            <td class="rating">Yes  &nbsp;&nbsp;&nbsp;No</td>
            
        </tr>
    </tbody>
</table>
</div>









<div class="no-break">
<p class="heading">Section F - Feedback on today's committee</p>
<table class="feedback">
    <thead>
        <tr>
            <td colspan="2">Please rate the committee between 1 (poor) and 10 (excellent)</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td width="62%" class="br">Chair</td>
            <td class="rating">1 2 3 4 5 6 7 8 9 10</td>
        </tr>
        <tr>
            <td width="62%" class="br">Education Officer</td>
            <td class="rating">1 2 3 4 5 6 7 8 9 10</td>
        </tr>
        <tr>
            <td width="62%" class="br">Membership Officer</td>
            <td class="rating">1 2 3 4 5 6 7 8 9 10</td>
        </tr>
        <tr>
            <td width="62%" class="br">Chartered Champion</td>
            <td class="rating">1 2 3 4 5 6 7 8 9 10</td>
        </tr>
    </tbody>
</table>
</div>


<div class="no-break">
    
<p class="heading">Section G - Overall feedback on the event</p>
<table class="feedback">
    <thead>
        <tr>
            <td colspan="2">Please rate the event in general between 1 (poor) and 10 (excellent)</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td width="62%" class="br">Content</td>
            <td class="rating">1 2 3 4 5 6 7 8 9 10</td>
        </tr>
        <tr>
            <td width="62%" class="br">Venue</td>
            <td class="rating">1 2 3 4 5 6 7 8 9 10</td>
        </tr>
        <tr>
            <td width="62%" class="br">Catering</td>
            <td class="rating">1 2 3 4 5 6 7 8 9 10</td>
        </tr>
        <tr>
            <td width="62%" class="br">Organisation</td>
            <td class="rating">1 2 3 4 5 6 7 8 9 10</td>
        </tr>
        <tr>
            <td colspan="2" valign="top" height="50px">Any other comments:</td>
        </tr>
        <tr>
            <td colspan="2" valign="top" height="50px">Are there any topics you would like to see covered at future events? If so, please let us know below:</td>
        </tr>
    </tbody>
</table>
</div>
<br/>

<table class="footer">
    <tr>
        <td width="50%">
            <p class="small" style="color: black;">Thank you for completing this form – your feedback helps us to continually improve the quality of our events.</p>

            <p class="small" style="font-family: 'arial', 'Helvetica'">Please leave this feedback form on your chair or hand it to one of the organisers or local Committee members.
            <br>
            @if((sizeof($quarterly_sponsors) || sizeof($event->sponsors)) && in_array(\Config::get('tabs.tab_8'), $event->tabs()->lists('id')))
                
                
                <p style="color:black">In association with our 
                    <?php  echo  date("Y"); ?>
                Partners in Professionalism:
                </p>
                </p>
               
                
                @foreach($event->sponsors()->where('type', 0)->get() as $sponsor)
                
                    @if(!empty($sponsor->logo))
                        <img style="margin-right: 10px;width:50px" src="{{\Config::get('app.url')}}uploads/sponsors/{{$sponsor->logo}}" />
                    @else
                        {{$sponsor->name}}
                    @endif
                @endforeach
                
                <!-- display quarterly sponsors -->
                @foreach($quarterly_sponsors as $sponsor)
                <?php
                for ($i=1; $i <= 10; $i++) {
                    $logo_id = "logo_".$i;
                    if (empty($sponsor->{$logo_id})) continue;
                    $sp1 = $sponsors->find($sponsor->{$logo_id});
                ?>
                    @if(!empty($sp1->logo))
                        <img  style="width:90%" src="{{\Config::get('app.url')}}uploads/sponsors/{{$sp1->logo}}" />
                    @else
                        {{$sp1->name}}
                    @endif
                
                <?php } ?>
                @endforeach
            @endif
        </td>
        <td width="50%">
            <img src="images/footer_right.jpg" />  
            <div class="small meta-plus withoutmargin">
            The Personal Finance Society, 42–48 High Road, South Woodford, London E18 2JP</div>
            <div class="small meta-plus withoutmargin">tel: +44 (0)20 8530 0852  &nbsp; &nbsp; &nbsp; &nbsp; fax: +44 (0)20 8530 3052
customer.serv@thepfs.org &nbsp; &nbsp;&nbsp; www.thepfs.org</div> 
        </td>
    </tr>
</table>

</body>
</html>