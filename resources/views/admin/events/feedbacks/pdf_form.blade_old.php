<!DOCTYPE html>
<html>
<head>
    <title>Attendance Certificate</title>
<style type="text/css">
     @font-face {
      font-family: 'MetaPlusBold';
      src: url('{{config("app.root")}}storage/fonts/MetaPlusBold.ttf') format('truetype');
    }
    
    @font-face {
      font-family: 'MetaPlusBookRoman';
      src: url('{{config("app.root")}}storage/fonts/MetaPlusBookRoman.otf') format('truetype');
    }
    
    @font-face {
      font-family: 'MyriadPro-Regular';
      src: url('{{config("app.root")}}storage/fonts/MyriadPro-Regular.otf') format('truetype');
    }
    @font-face {
      font-family: 'SwissBoldCondensed';
      src: url('{{config("app.root")}}storage/fonts/SwissBoldCondensed.ttf') format('truetype');
    }
    
    @font-face {
      font-family: 'SwissCondensed';
      src: url('{{config("app.root")}}storage/fonts/SwissCondensed.ttf') format('truetype');
    }
    
    @font-face {
      font-family: 'SwissLightCondensed';
      src: url('{{config("app.root")}}storage/fonts/SwissLightCondensed.ttf') format('truetype');
    }

    html{margin:15px 40px 10px}

     body {font-family: 'SwissLightCondensed'; font-size: 12pt; color: #192387;}
     .small {font-size: 8pt}
     .medium {font-size: 10pt;}
     h2 {font-size: 14pt; margin-bottom: 0px; font-family: 'SwissBoldCondensed'}
     .heading{font-size: 14pt; margin-bottom: 0px; font-family: 'SwissBoldCondensed'}
     hr.original{ background-color: #192387; height: 3px}
     hr.custom { background-color: #C6C8E3; border: #C6C8E3; height: 3px;}
     thead {background-color: #192387; color: #FFF; font-size: 8pt; margin-bottom: 4px; font-family: 'SwissCondensed'}
     table {width: 100%; font-size: 8pt; border-spacing: 0;}
     table.feedback tr td {border-bottom: 1px solid #192387;}
     table.feedback thead tr td {padding: 5px;}
     table.sessions{border-top: 1px solid #192387;}
     table.feedback tbody {color: black; font-family: 'MetaPlusBookRoman'}
     .rating{word-spacing: 18px; padding-left: 10px;}
     .bb {border-bottom: 1px solid #192387;}
     .br {border-right: 1px solid #192387;}
     .br-white {border-right: 1px solid #FFF;}
     .bold-condensed {font-family: 'SwissBoldCondensed'}
     .meta-plus {font-family: 'MetaPlusBookRoman'}
     .no-break{page-break-inside: avoid;}

     p {margin-top: 0px; margin-bottom: 5px;}
</style>
</head>
<body>

<table>
    <tr>
        <td width="50%">
            <span class="bold-condensed" style="font-size: 12pt">Important notes:</span><br />
            <span class="small">Please complete all sections of this form in BLOCK CAPITALS and leave it on your chair or hand
to one of the event organisers or Local Committee Members.</span>
        </td>
        <td align="right">
            <img height="110px" src="images/cii_logo.png" />
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td align="right"><span class="bold-condensed" style="font-size: 22pt;">Feedback form</span></td>
    </tr>
</table>
<hr class="original" />
<p class="heading">Section A - Event details</p>
<span class="medium" style="color: black; font-family: 'MetaPlusBold'">{{$event->title}}, {{date("l d M Y", strtotime($event->event_date_from))}}@if(sizeof($event->venue)), {{$event->venue->name}}@endif</span>

<p class="heading">Section B - Personal details</p>
<span class="small" style="font-family: 'SwissLightCondensed'">By providing your contact details you are giving permission for them to be shared with our Partners in Professionalism where requested by you in the feedback form below.</span>
<br/>
<img height="130px" src="images/profile_details.jpg" />


<p class="heading">Section C - Feedback on today's sessions</p>

<table class="feedback">
    <thead>
        <tr>
            <td width="32%" class="br-white">Session</td>
            <td width="18%" class="br-white">Request contact from speaker/company</td>
            <td colspan="2">Please rate the following session between 1 (poor) and 10 (excellent) by speaker and content:</td>
        </tr>
    </thead>
</table>

    @foreach ($slots as $slot)
    @foreach($slot->sessions as $session)
    <?php if (!$session->type->include_in_feedback) continue; ?>
    <div class="no-break">
    <table class="feedback sessions">
    <tbody>
        <tr>
            <td rowspan="3" valign="top" class="br medium" width="32%">
                <span style="font-family: 'MetaPlusBold'">
                {{date("H:i", strtotime($session->session_start))}} - {{date("H:i", strtotime($session->session_end))}} {!!$session->title!!} <br/>
                </span>
                <div style="line-height: 11px;"> 
                @foreach($session->contributors as $contributor)
                {{$contributor->first_name}} {{$contributor->last_name}}, {{$contributor->company}} <br/>
                @endforeach
                </div>
            </td>
            <td rowspan="2" width="18%" align="center" valign="middle" class="br"><img src="images/checkbox.jpg" /></td>
            <td width="12%" class="br">Speaker rating</td>
            <td class="rating">1 2 3 4 5 6 7 8 9 10</td>
        </tr>
        <tr>
            <td class="br">Content rating</td>
            <td class="rating">1 2 3 4 5 6 7 8 9 10</td>
        </tr>
        <tr>
            <td colspan="3" height="60px" valign="top" style="line-height: 8px;">
                @if (empty($session->question))
                Feedback comments/reason for contact request:
                @else
                {{$session->question}}
                @endif
            </td>
        </tr>
    </tbody>
    </table>
    </div>
    @endforeach
    @endforeach
    
<br/>

<div class="no-break">
<p class="heading">Section D - Feedback on today's committee</p>
<table class="feedback">
    <thead>
        <tr>
            <td colspan="2">Please rate the committee between 1 (poor) and 10 (excellent)</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td width="62%" class="br">Chair</td>
            <td class="rating">1 2 3 4 5 6 7 8 9 10</td>
        </tr>
        <tr>
            <td width="62%" class="br">Education Officer</td>
            <td class="rating">1 2 3 4 5 6 7 8 9 10</td>
        </tr>
        <tr>
            <td width="62%" class="br">Membership Officer</td>
            <td class="rating">1 2 3 4 5 6 7 8 9 10</td>
        </tr>
        <tr>
            <td width="62%" class="br">Chartered Champion</td>
            <td class="rating">1 2 3 4 5 6 7 8 9 10</td>
        </tr>
    </tbody>
</table>
</div>
<br/>

<div class="no-break">
    
<p class="heading">Section E - Overall feedback on the event</p>
<table class="feedback">
    <thead>
        <tr>
            <td colspan="2">Please rate the event in general between 1 (poor) and 10 (excellent)</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td width="62%" class="br">Content</td>
            <td class="rating">1 2 3 4 5 6 7 8 9 10</td>
        </tr>
        <tr>
            <td width="62%" class="br">Venue</td>
            <td class="rating">1 2 3 4 5 6 7 8 9 10</td>
        </tr>
        <tr>
            <td width="62%" class="br">Catering</td>
            <td class="rating">1 2 3 4 5 6 7 8 9 10</td>
        </tr>
        <tr>
            <td width="62%" class="br">Organisation</td>
            <td class="rating">1 2 3 4 5 6 7 8 9 10</td>
        </tr>
        <tr>
            <td colspan="2" valign="top" height="50px">Any other comments:</td>
        </tr>
        <tr>
            <td colspan="2" valign="top" height="50px">Are there any topics you would like to see covered at future events? If so, please let us know below:</td>
        </tr>
    </tbody>
</table>
</div>
<br/>

<p class="small" style="color: black;">Thank you for completing this form – your feedback helps us to continually improve the quality of our events.</p>

<span class="small" style="font-family: 'SwissLightCondensed'">Please leave this feedback form on your chair or hand it to one of the organisers or local Committee members.</span>
<br/>

@if((sizeof($quarterly_sponsors) || sizeof($event->sponsors)) && in_array(\Config::get('tabs.tab_8'), $event->tabs()->lists('id')))
    
    
    <span class="small" style="color: black;">In association with our 
        <?php
            $year = date("Y");
            echo $year;
        ?>
    Partners in Professionalism:
    </span><br/>
    
    @foreach($event->sponsors()->where('type', 0)->get() as $sponsor)
    
        @if(!empty($sponsor->logo))
            <img style="margin-right: 10px" height="50px" src="{{\Config::get('app.url')}}uploads/sponsors/{{$sponsor->logo}}" />
        @else
            {{$sponsor->name}}
        @endif
    @endforeach
    
    <!-- display quarterly sponsors -->
    @foreach($quarterly_sponsors as $sponsor)
    <?php
    for ($i=1; $i <= 10; $i++) {
        $logo_id = "logo_".$i;
        if (empty($sponsor->{$logo_id})) continue;
        
        $sp1 = $sponsors->find($sponsor->{$logo_id});
    ?>
        @if(!empty($sp1->logo))
            <img style="margin-right: 15px" height="50px" src="{{\Config::get('app.url')}}uploads/sponsors/{{$sp1->logo}}" />
        @else
            {{$sp1->name}}
        @endif
    
    <?php } ?>
    @endforeach
@endif
<br/>
<table>
    <tr>
        <td width="80%"><span class="small meta-plus">
            The Personal Finance Society, 42–48 High Road, South Woodford, London E18 2JP<br/>
tel: +44 (0)20 8530 0852 fax: +44 (0)20 8530 3052<br/>
customer.serv@thepfs.org www.thepfs.org</span>
        </td>
        <td>
            <br/><br/>
            <img src="images/footer_right.jpg" />
        </td>
    </tr>
</table>

</body>
</html>