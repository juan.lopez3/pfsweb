<div class="form-group">
	{!! Form::textarea('question', null, ['class' => 'form-control control-2-rows', 'required']) !!}
	{!! Form::label('question', 'Question') !!}
</div>
<div class="form-group">
	{!! Form::text('answer', null, ['class'=>'form-control', 'required', 'placeholder' => 'Please separate answers by semicolon']) !!}
	{!! Form::label('answer', 'Answer') !!}
	<p class="help-block">Example: Poor;Average;Excellent</p>
</div>
<div class="form-group">
	{!! Form::select('type', [1=>'Rating', 2=>'Checkbox', 3=>'Option box', 4=>'Select box', 5=>'Text'], null, ['class'=>'form-control']) !!}
	{!! Form::label('type', 'Question type') !!}
</div>
<div class="form-group">
	{!! Form::checkbox('checkin_required', 1, null) !!}
	{!! Form::label('checkin_required', 'Check-in Required') !!}
</div>
<div class="form-group">
	{!! Form::select('session_id', [''=>'']+$sessions, null, ['class'=>'form-control select2-list']) !!}
	{!! Form::label('session_id', 'Link session') !!}
</div>