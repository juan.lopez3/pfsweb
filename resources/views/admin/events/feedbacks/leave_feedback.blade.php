@extends('admin.layouts.default')

@section('title')
Leave feedback
@endsection

@section('content')

<section>
<div class="section-body">
	<div class="col-md-12">
		<div class="card">
			<div class="card-head style-primary">
			    @if (sizeof($user->feedbackAnswers))
				<header>Edit Feedback for {{$user->first_name}} {{$user->last_name}}</header>
				@else
				<header>Submit Feedback for {{$user->first_name}} {{$user->last_name}}</header>
				@endif
			</div>
			
			@include('admin.partials.validationErrors')
			
			<div class="card-body">
				 
				<h3>{{$event->title}} Feedback form</h3><br>
                <b>Please rate between 1 (poor) and 10 (excellent)</b><br/><br/>
                {!! Form::model(null, ['route' => ['events.feedbacks.submit', $event->id, $user->id], 'method' => 'PUT', 'class' => 'form form-validate']) !!}
                <div class="row">
                    <div class="col-xs-12">
                        <div class="feedback gradient gr_sm gr-pad">
                            Feedback on today's sessions
                        </div>
                    </div>
                </div>
                @foreach($event->scheduleSessions()->orderBy('session_start')->has('feedbacks')->with('feedbacks', 'contributors')->get() as $session)
                    
                    <div class="row">
                        <div class="col-md-3">
                            {{date("H:i", strtotime($session->session_start))}} - {{date("H:i", strtotime($session->session_end))}} {!!$session->title!!} <br/>
                            <div style="line-height: 11px;"> 
                            @foreach($session->contributors as $contributor)
                            {{$contributor->first_name}} {{$contributor->last_name}}, {{$contributor->company}} <br/>
                            @endforeach
                            </div>
                        </div>
                        
                        <div class="col-md-9">
                            @foreach ($session->feedbacks as $index => $fb)
                                <div class="row">
                                    <div class="col-sm-3">{{$fb->question}}</div>
                                    <div class="col-sm-9">
                                        @if($fb->type == 1)
                                        <!-- Rating -->
                                            @foreach(explode(";", $fb->answer) as $answer)
                                                <input type="radio" name="{{$fb->id}}" value="{{$answer}}" @if(isset($feedback_answers[$fb->id]) && $feedback_answers[$fb->id] == $answer) checked @endif /> {{$answer}} &nbsp;&nbsp;&nbsp;
                                            @endforeach
                                            <input type="radio" name="{{$fb->id}}" value="" /> n/a
                                        @elseif($fb->type == 2)
                                        <!-- ch/box -->
                                            @foreach(explode(";", $fb->answer) as $answer)
                                                <input type="checkbox" name="{{$fb->id}}[]" value="{{$answer}}" @if(isset($feedback_answers[$fb->id]) && in_array($feedback_answers[$fb->id], array_map('trim', explode(",", $answer)))) checked @endif /> {{$answer}}
                                            @endforeach
                                        @elseif($fb->type == 3)
                                        <!--  option -->
                                            @foreach(explode(";", $fb->answer) as $answer)
                                                <input type="radio" name="{{$fb->id}}" value="{{$answer}}" @if(isset($feedback_answers[$fb->id]) && $feedback_answers[$fb->id] == $answer) checked @endif /> {{$answer}} &nbsp;&nbsp;&nbsp;
                                            @endforeach
                                        @elseif($fb->type == 4)
                                        <!-- select -->
                                            @if(isset($feedback_answers[$fb->id]))
                                                {!! Form::select($fb->id, explode(";", $fb->answer), $feedback_answers[$fb->id], ['class'=>'form-control']) !!}
                                            @else
                                                {!! Form::select($fb->id, explode(";", $fb->answer), null, ['class'=>'form-control']) !!}
                                            @endif
                                        @elseif($fb->type == 5)
                                        <!-- text -->
                                            @if(isset($feedback_answers[$fb->id]))
                                                {!! Form::textarea($fb->id, $feedback_answers[$fb->id], ['rows'=>3, 'class'=>'form-control']) !!}
                                            @else
                                                {!! Form::textarea($fb->id, null, ['rows'=>3, 'class'=>'form-control']) !!}
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <hr />
                @endforeach
                
                @if(sizeof($event_feedback))
                <div class="row">
                    <div class="col-xs-12">
                        <div class="feedback gradient gr_sm gr-pad">
                            Feedback on today’s committee and the overall event
                        </div>
                    </div>
                </div>
                @endif
                
                @foreach ($event_feedback as $index => $fb)
                <div class="row feedback-row">
                    <div class="col-md-5">
                       <b>{{$fb->question}}</b>
                    </div>
                    <div class="col-md-7">
                        @if($fb->type == 1)
                        <!-- Rating -->
                            @foreach(explode(";", $fb->answer) as $answer)
                                <input type="radio" name="{{$fb->id}}" value="{{$answer}}" @if(isset($feedback_answers[$fb->id]) && $feedback_answers[$fb->id] == $answer) checked @endif /> {{$answer}} &nbsp;&nbsp;&nbsp;
                            @endforeach
                            <input type="radio" name="{{$fb->id}}" value="" /> n/a
                        @elseif($fb->type == 2)
                        <!-- ch/box -->
                            @foreach(explode(";", $fb->answer) as $answer)
                                <input type="checkbox" name="{{$fb->id}}[]" value="{{$answer}}" @if(isset($feedback_answers[$fb->id]) && in_array($feedback_answers[$fb->id], array_map('trim', explode(",", $answer)))) checked @endif /> {{$answer}}
                            @endforeach
                        @elseif($fb->type == 3)
                        <!--  option -->
                            @foreach(explode(";", $fb->answer) as $answer)
                                <input type="radio" name="{{$fb->id}}" value="{{$answer}}" @if(isset($feedback_answers[$fb->id]) && $feedback_answers[$fb->id] == $answer) checked @endif /> {{$answer}} &nbsp;&nbsp;&nbsp;
                            @endforeach
                        @elseif($fb->type == 4)
                        <!-- select -->
                            @if(isset($feedback_answers[$fb->id]))
                                {!! Form::select($fb->id, explode(";", $fb->answer), $feedback_answers[$fb->id], ['class'=>'form-control']) !!}
                            @else
                                {!! Form::select($fb->id, explode(";", $fb->answer), null, ['class'=>'form-control']) !!}
                            @endif
                        @elseif($fb->type == 5)
                        <!-- text -->
                            @if(isset($feedback_answers[$fb->id]))
                                {!! Form::textarea($fb->id, $feedback_answers[$fb->id], ['rows'=>3, 'class'=>'form-control']) !!}
                            @else
                                {!! Form::textarea($fb->id, null, ['rows'=>3, 'class'=>'form-control']) !!}
                            @endif
                        @elseif($fb->type == 6)
                        <!--rad/bton -->
                            @foreach(explode(";", $fb->answer) as $answer)
                                <input type="radio" name="{{$fb->id}}" value="{{$answer}}" @if(isset($feedback_answers[$fb->id]) && $feedback_answers[$fb->id] == $answer) checked @endif /> {{$answer}} &nbsp;&nbsp;&nbsp;
                            @endforeach
                        @endif
                    </div>
                </div>
                <hr />
                @endforeach
                
                <div class="card-actionbar">
                    <div class="card-actionbar-row">
                        <a href="{{ URL::previous() }}"><button type="button" class="btn btn-flat">Cancel</button></a>
                        {!! Form::submit('SAVE', ['class' => 'btn btn-primary']) !!}
                    </div>
                </div>
                
                <style type="text/css">
                     div.feedback {padding: 5px; font-size: 19px;}
                </style>
				 
			</div>
			
			{!! Form::close() !!}
		</div>
	</div>
</div>
</section>
@endsection
