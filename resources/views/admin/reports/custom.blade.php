@extends('admin.layouts.default')

@section('title')
	Create Custom Reports
@endsection

@section('content')

<!-- @TODO DRAG AND DROP conflicts with radio. moved to default layout
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.min.js"></script>
-->

<section>
<div class="section-header">
	<ol class="breadcrumb">
		<li class="active">Create Custom Reports</li>
	</ol>
</div>
<div class="section-body">	
	<div class="card">
		<div class="card-body">
			<div class="row text-center">
				<div id="tab" class="btn-group report-nav" data-toggle="buttons-radio">
					<?php $i = 0;?>
					
					@foreach($models as $key => $m)
						<?php $active = ($i==0) ? "active" : ""; $i++;?>
						<a href="#{{$key}}" class="btn {{$active}}" data-toggle="tab">{{$key}}</a>
					@endforeach
	            </div>
            </div>
             
            <div class="tab-content">
            	<?php $i = 0;?>
            	@foreach($models as $key => $m)
            	
					<?php
					   $active = ($i==0) ? "active" : ""; $i++;
                       
                       if (sizeof($saved_report)) {
                           // set active tab from saved reports
                           $c = 0;
                           $find_active = true;
                           while($find_active) {
                               
                               if(!empty($saved_report[$c])) {
                                    $first_field = explode(".", $saved_report[$c]);
                                    $active = ($key == $first_field[0]) ? "active" : "";
                                    $find_active = false;
                               }
                               $c++;
                           }
                       }
                       
					?>
					<div class="tab-pane {{$active}}" id="{{$key}}">
						
						{!! Form::open(['route' => ['reports.custom.generate'], 'class' => 'form form-validate']) !!}
            			
            			<!-- Modal for report generation -->
						<div id="generate_report{{$key}}" class="modal fade" role="dialog">
						  <div class="modal-dialog">
						
						    <!-- Modal content-->
						    <div class="modal-content">
							      <div class="card-head style-primary">
							        <header>Generate Report</header>
							      </div>
						      <div class="modal-body">
						      
						      <div class="row">
						      	<div class="form-group">
						      		<div class="col-sm-3 text-right">
										{!! Form::label('report_title', 'Report Title', ['class' => 'control-label']) !!}
									</div>
									<div class="col-sm-9">
										{!! Form::text('report_title', null, ['class'=>'form-control', 'required']) !!}
									</div>
								</div>
						      </div>
						      <div class="row" style="padding-top: 5px;">
						      	<div class="col-sm-3 text-right">
						      		{!! Form::label('report_type', 'Report Type', ['class' => 'control-label']) !!}
						      	</div>
						      	<div class="col-sm-9">
						      		<div class="input-group">
					    				<div id="radioBtn" class="btn-group">
					    					<a class="btn btn-primary btn-sm active" data-toggle="report_type-{{$key}}" data-title="HTML">HTML</a>
					                        <a class="btn btn-primary btn-sm notActive" data-toggle="report_type-{{$key}}" data-title="XLSX">XLSX</a>
					    					<a class="btn btn-primary btn-sm notActive" data-toggle="report_type-{{$key}}" data-title="PDF">PDF</a>
					    				</div>
					    				<input type="hidden" name="report_type" id="report_type-{{$key}}" value="HTML">
					    			</div>
						      	</div>
						      </div>
						      
						      <div class="row">
						      	<div class="col-sm-3 text-right">
						      		{!! Form::label('report_save', 'Save Report', ['class' => 'control-label']) !!}
						      	</div>
						      	<div class="col-sm-9">
							      	<div class="checkbox checkbox-styled">
							      		<label>
										{!! Form::checkbox('report_save', 1, 0) !!}
										</label>
									</div>
								</div>
						      </div>
						      
						      <div class="row">
                                <div class="col-sm-3 text-right">
                                    {!! Form::label('client_report', 'Save as Client Report', ['class' => 'control-label']) !!}
                                </div>
                                <div class="col-sm-9">
                                    <div class="checkbox checkbox-styled">
                                        <label>
                                        {!! Form::checkbox('client_report', 1, 0) !!}
                                        </label>
                                    </div>
                                </div>
                              </div>
						      
						      </div>
						      <div class="modal-footer">
						        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						        {!! Form::submit('Generate', ['class'=>'btn btn-success btn-md btn-raised pull-right']) !!}
						      </div>
						    </div>
						
						  </div>
						</div>
						<!-- END MODAL -->
            			
		            	<div class="row"><button type="button" class="btn btn-success btn-md btn-raised pull-right" data-toggle="modal" data-target="#generate_report{{$key}}">GENERATE</button></div>
            	         
						<table class="table table-hover" id="sort{{$key}}">
						    <thead>
						      <tr>
						        <th>Model.Column (default label)</th>
						        <th width="200px">Custom Label</th>
						        <th title="Add to report">
						        	Add<br/>
						        	<button type="button" class="btn btn-xs btn-info deselect_all">Deselect</button>
						        </th>
						        <th title="Order by field">Order</th>
						        <th width="250px">Conditions
						        	<div class="input-group">
										<div id="radioBtn" class="btn-group">
											<a class="btn btn-primary btn-sm active" data-toggle="condition" data-title="AND">AND</a>
											<a class="btn btn-primary btn-sm notActive" data-toggle="condition" data-title="OR">OR</a>
										</div>
										<input type="hidden" name="condition" value="AND" id="condition">
									</div>
						        </th>
						      </tr>
						    </thead>
						    <tbody>
						      
							<?php
							$model = "App\\".$key;
			            	$table = with(new $model)->getTable();
							$fields = \DB::connection()->getSchemaBuilder()->getColumnListing($table);
							
							$add_counter = 1;
			            	?>
			            	
			            	<!-- Output model fields -->
			            	@foreach($fields as $f)
			            	<?php
			            		if(in_array($f, $m["IgnoreFields"])) continue;
								$label = (isset($m["FieldLabels"][$f])) ? $m["FieldLabels"][$f] : $f;
			            	?>
							<tr>
								<td><span class="drag-marker"><i></i></span><b>{{$key}}.{{$label}}</b></td>
								<td>{!! Form::text('custom_label_'.$key.'_'.$f, null, ['class' => 'form-control']) !!}</td>
								<td>
									<div class="input-group">
										<div id="radioBtn" class="btn-group">
											<a class="btn bba btn-primary btn-sm @if(in_array($key.'.'.$f, $saved_report))active @else notActive @endif" data-toggle="b_{{$key.$f.$add_counter}}" data-title="{{$key.'.'.$f}}">YES</a>
											<a class="btn bbna btn-primary btn-sm @if(in_array($key.'.'.$f, $saved_report)) notActive @else active @endif" data-toggle="b_{{$key.$f.$add_counter}}" data-title="">NO</a>
										</div>
										
										<input type="hidden" class="add_field" name="report_fields[]" value="@if(in_array($key.'.'.$f, $saved_report)){{$key.'.'.$f}}@endif" id="b_{{$key.$f.$add_counter}}">
									</div>
								</td>
								<td>{!! Form::select('order_'.$key.'.'.$f, [''=>'','ASC'=>'ASCENDING', 'DESC'=>'DESCENDING'], null, ['class'=>'form-control']) !!}</td>
								<td class="text-center condition_{{$key}}_{{$f}}">
									@if(isset($m["Conditions"][$f]) && !empty($m["Conditions"][$f]))
										<button id="b_condition_{{$key}}_{{$f}}" type="button" onclick="createCondition('condition_{{$key}}_{{$f}}', 2, '{{$m['Conditions'][$f][0]}}', '{{$m['Conditions'][$f][1]}}', '{{$m['Conditions'][$f][2]}}');" class="btn btn-xs btn-info">Create</button>
									@elseif(isset($m["Conditions"][$f]))
										<button id="b_condition_{{$key}}_{{$f}}" type="button" onclick="createCondition('condition_{{$key}}_{{$f}}', 0);" class="btn btn-xs btn-info">Create</button>
									@else
										<button id="b_condition_{{$key}}_{{$f}}" type="button" onclick="createCondition('condition_{{$key}}_{{$f}}', 1);" class="btn btn-xs btn-info">Create</button>
									@endif
								</td>
							</tr>
							
							<?php $add_counter++;?>
							
							@endforeach
							
							<!-- Show related models -->
							@foreach($m["AssocciatedModels"] as $am => $method)
								<?php
								$model = "App\\".$am;
				            	$table = with(new $model)->getTable();
								$fields = \DB::connection()->getSchemaBuilder()->getColumnListing($table);
								?>
								
								@foreach($fields as $f)
				            	<?php
				            		if(in_array($f, $m["IgnoreFields"])) continue;
									$label = (isset($m["FieldLabels"][$f])) ? $m["FieldLabels"][$f] : $f;  
				            	?>
								<tr>
									<td><span class="drag-marker"><i></i></span>{{$am}}.{{$label}}</td>
									<td>{!! Form::text('custom_label_'.$am.'_'.$f, null, ['class' => 'form-control']) !!}</td>
									<td>
										<div class="input-group">
											<div id="radioBtn" class="btn-group">
												<a class="btn bba btn-primary btn-sm @if(in_array($am.'.'.$f, $saved_report))active @else notActive @endif" data-toggle="{{$am.$f.$add_counter}}" data-title="{{$am.'.'.$f}}">YES</a>
                                                <a class="btn bbna btn-primary btn-sm @if(in_array($am.'.'.$f, $saved_report)) notActive @else active @endif" data-toggle="{{$am.$f.$add_counter}}" data-title="">NO</a>
											</div>
											<input type="hidden" class="add_field" name="report_fields[]" value="@if(in_array($am.'.'.$f, $saved_report)){{$am.'.'.$f}}@endif" id="{{$am.$f.$add_counter}}">
										</div>
									</td>
									<td>{!! Form::select('order_'.$am.'.'.$f, [''=>'','ASC'=>'ASCENDING', 'DESC'=>'DESCENDING'], null, ['class'=>'form-control']) !!}</td>
									<td class="text-center condition_{{$am}}_{{$f}}">
    								    @if(isset($m["Conditions"][$f]) && !empty($m["Conditions"][$f]))
    										<button id="b_condition_{{$am}}_{{$f}}" type="button" onclick="createCondition('condition_{{$am}}_{{$f}}', 2, '{{$m['Conditions'][$f][0]}}', '{{$m['Conditions'][$f][1]}}', '{{$m['Conditions'][$f][2]}}');" class="btn btn-xs btn-info">Create</button>
    									@elseif(isset($m["Conditions"][$f]))
    										<button id="b_condition_{{$am}}_{{$f}}" type="button" onclick="createCondition('condition_{{$am}}_{{$f}}', 0);" class="btn btn-xs btn-info">Create</button>
    									@else
    										<button id="b_condition_{{$am}}_{{$f}}" type="button" onclick="createCondition('condition_{{$am}}_{{$f}}', 1);" class="btn btn-xs btn-info">Create</button>
    									@endif
									</td>
								</tr>
								@endforeach
								
								<?php $add_counter++; ?>
								
							@endforeach
						    </tbody>
						</table>
					{!! Form::close() !!}
					</div>
					
					
					
					<script type="text/javascript">
					// Return a helper with preserved width of cells
					
						var fixHelper = function(e, ui) {
							ui.children().each(function() {
								$(this).width($(this).width());
							});
							return ui;
						};
						
						$("#sort{{$key}} tbody").sortable({
							handle: '.drag-marker',
							helper: fixHelper
						});
					</script>
					
				@endforeach
            </div>
		</div>
	</div>
</div>
</section>

<script type="text/javascript">

	$(".deselect_all").click(function(){
		$(".add_field").val("");
		$('.bba').removeClass('active').addClass('notActive');
	    $('.bbna').removeClass('notActive').addClass('active');
	})
	// types: 1 - text, 2-select
	function createCondition(replace_id, type, model, title_field, id_field) {
		
		var b_id = "#b_"+replace_id; 
		$(b_id).hide();
		//$("#b_"+replace_id).show();
		
		var html = '<div><button type="button" class="btn btn-danger btn-xs" onclick="this.parentNode.remove(); $(\''+b_id+'\').show();"><span class="glyphicon glyphicon-trash"></span></button><select class="form-control" name="operator_'+replace_id+'"><option value="=">Equal</option><option value="!=">Not equal</option><option value=">">More than</option><option value="<">Less than</option><option value="LIKE">Like</option></select>';
		
		if(type == "2") {
			
			if(model != 'undefined' && title_field != 'undefined' && id_field != 'undefined'){
				
				$.ajax({
					url: '{{route("reports.getConditionSelect")}}',
					data: {"model" : model, "title_field" : title_field, "id_field" : id_field, "select_name" : replace_id},
					type: "GET",
					success: function(success){
						
						html += success + '</div>';
						$("."+replace_id).append(html);
					}
				});
				
			} else {
				alert("Error! No enough parameters");
			}
		} else if(type == "0") {
			html += '<select class="form-control" name="'+replace_id+'"><option value="0">No</option><option value="1">Yes</option></select></div>';
			$("."+replace_id).append(html);
		} else {
			html += '<input class="form-control" type="text" name="'+replace_id+'" /></div>';
			$("."+replace_id).append(html);
		}
		
	}
	
	$('#radioBtn a').on('click', function(){
	    var sel = $(this).data('title');
	    var tog = $(this).data('toggle');
	    $('#'+tog).prop('value', sel);
	    
	    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
	    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
	})
</script>

@include('admin.partials.deleteConfirmation')

@endsection
