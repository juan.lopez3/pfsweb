<br/>
{!! Form::open(['route' => ['reports.standard.eventSlotByRegion'], 'class' => 'form form-validate']) !!}
	<div class="card">
		<div class="card-head style-primary">
			<header>Report Filter</header>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="form-group">
					<div class="col-xs-5">
						<div class="form-group">
							<select class="form-control" name="year" required>
								<option value="#">Select</option>
								<option value="2016">2016</option>
								<option value="2017">2017</option>
								<option value="2018">2018</option>
								</select>
							<label>Years</label>
						</div>
						<div class="input-group-content">
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
			    <div class="col-xs-5">
                    <div class="form-group">
                        {!! Form::select('event_type_id[]', [''=>'All event types']+$event_types, null, ['class'=>'form-control', 'multiple']) !!}
                        <label>Event type</label>
                    </div>
                </div>
			</div>
			
			<div class="row">
			    <div class="col-xs-12">
			        <div class="checkbox checkbox-styled">
                        <label>
                            {!! Form::checkbox('save_pfs_report', 1) !!}
                            Save to PFS reports
                        </label>
                    </div>
			    </div>
			</div>
			
			<div class="row">
                <div class="col-md-4">
                    {!! Form::label('saved_report_title', 'Saved report title') !!}
                    {!! Form::text('saved_report_title', null, ['class'=>'form-control', 'placeholder' => 'Saved report title']) !!}
                </div>
            </div>
			
		</div><!--end .card-body -->
		<div class="card-actionbar">
			<div class="card-actionbar-row">
				{!! Form::submit('Generate', ['class'=>'btn btn-flat btn-primary ink-reaction']) !!}
			</div>
		</div>
	</div>
	<em class="text-caption">This report generates the list of events in your chosen date range with all bookable schedule slots and a count of attendees for each slot</em>
{!! Form::close() !!}

<script type="text/javascript">
    $('#demo-date-range-1').datepicker({todayHighlight: true, format: "dd/mm/yyyy", weekStart: 1});
</script>