{!! Form::open(['route' => ['reports.standard.emailCampaigns'], 'class' => 'form form-validate']) !!}
<div class="card">
    <div class="card-head style-primary">
        <header>Report Filter</header>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="form-group">
                {!! Form::select('campaign', [''=>'Please select campaign']+$campaigns, null, ['class'=>'form-control select2-list', 'required']) !!}
                <label>Campaign name</label>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="checkbox checkbox-styled">
                    <label>
                        {!! Form::checkbox('save_pfs_report', 1) !!}
                        Save to PFS reports
                    </label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                {!! Form::label('saved_report_title', 'Saved report title') !!}
                {!! Form::text('saved_report_title', null, ['class'=>'form-control', 'placeholder' => 'Saved report title']) !!}
            </div>
        </div>

    </div><!--end .card-body -->
    <div class="card-actionbar">
        <div class="card-actionbar-row">
            {!! Form::submit('Generate', ['class'=>'btn btn-flat btn-primary ink-reaction']) !!}
        </div>
    </div>
</div>
<em class="text-caption">Reports on email campaigns. Sent emails, opened emails, links clickd.</em>
{!! Form::close() !!}
