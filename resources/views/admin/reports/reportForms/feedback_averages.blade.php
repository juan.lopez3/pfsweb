<br/>
{!! Form::open(['route' => ['reports.standard.feedbackAverages'], 'class' => 'form form-validate']) !!}
    <div class="card">
        <div class="card-head style-primary">
            <header>Report Filter</header>
        </div>
        <div class="card-body">
            
            <div class="row">
                <div class="form-group">
                    <div class="input-daterange input-group" id="demo-date-range-3">
                        <div class="input-group-content">
                            <input type="text" class="form-control" name="from" required />
                            <label>Date range</label>
                        </div>
                        <span class="input-group-addon">to</span>
                        <div class="input-group-content">
                            <input type="text" class="form-control" name="to" required />
                            <div class="form-control-line"></div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-xs-5">
                    <div class="form-group">
                        {!! Form::select('event_type_id[]', [''=>'Select event type']+$event_types, null, ['class'=>'form-control', 'multiple']) !!}
                        <label>Event type</label>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-xs-12">
                    <div class="checkbox checkbox-styled">
                        <label>
                            {!! Form::checkbox('save_pfs_report', 1) !!}
                            Save to PFS reports
                        </label>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-4">
                    {!! Form::label('saved_report_title', 'Saved report title') !!}
                    {!! Form::text('saved_report_title', null, ['class'=>'form-control', 'placeholder' => 'Saved report title']) !!}
                </div>
            </div>
            
        </div><!--end .card-body -->
        <div class="card-actionbar">
            <div class="card-actionbar-row">
                {!! Form::submit('Generate', ['class'=>'btn btn-flat btn-primary ink-reaction']) !!}
            </div>
        </div>
    </div>
    <em class="text-caption">This report generates event's feedback answers averages for selected event type</em>
{!! Form::close() !!}
<script type="text/javascript">
    $('#demo-date-range-3').datepicker({todayHighlight: true, format: "dd/mm/yyyy", weekStart: 1});
</script>