<br/>
{!! Form::open(['route' => ['reports.standard.regionalBookings'], 'class' => 'form form-validate']) !!}
	<div class="card">
		<div class="card-head style-primary">
			<header>Report Filter</header>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-xs-4">
					<div class="form-group">
						<input type="number" name="year" value="{{date("Y")}}" class="form-control" required />
						<label>Year</label>
					</div>
				</div>
				<div class="col-xs-4">
					<div class="form-group">
						<select name="quater" class="form-control" required>
							<option value="1">Quarter 1</option>
							<option value="2">Quarter 2</option>
							<option value="3">Quarter 3</option>
							<option value="4">Quarter 4</option>
						</select>
						<label>Quarter</label>
					</div>
				</div>
				<div class="col-xs-4">
				    <div class="form-group">
				        {!! Form::select('event_type_id[]', [''=>'Select event type']+$event_types, null, ['class'=>'form-control', 'multiple']) !!}
				        <label>Event type</label>
				    </div>
				</div>
			</div>
			
			<div class="row">
                <div class="col-xs-12">
                    <div class="checkbox checkbox-styled">
                        <label>
                            {!! Form::checkbox('save_pfs_report', 1) !!}
                            Save to PFS reports
                        </label>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-4">
                    {!! Form::label('saved_report_title', 'Saved report title') !!}
                    {!! Form::text('saved_report_title', null, ['class'=>'form-control', 'placeholder' => 'Saved report title']) !!}
                </div>
            </div>
			
		</div><!--end .card-body -->
		<div class="card-actionbar">
			<div class="card-actionbar-row">
				{!! Form::submit('Generate', ['class'=>'btn btn-flat btn-primary ink-reaction']) !!}
			</div>
		</div>
	</div>
	<em class="text-caption">This report generates regional events morning and afternoon sessions attendance for selected quarter</em>
{!! Form::close() !!}
