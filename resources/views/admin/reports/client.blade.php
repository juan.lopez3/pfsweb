@extends('admin.layouts.default')

@section('title')
    PFS Reports
@endsection

@section('content')

<script type="text/javascript">
    $(document).ready(function() {
        $('#client_reports').dataTable({
            "iDisplayLength": 50,
        });
    } );
</script>

<section>
<div class="section-header">
    <ol class="breadcrumb">
        <li class="active">PFS Reports</li>
    </ol>
</div>
<div class="section-body">  
    <div class="card">
        <div class="card-body">
            <table id="client_reports" class="table table-striped table-responsive" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                </tfoot>
            
                <tbody>
                    @foreach ($client_reports as $r)
                    <tr>
                        <td>{{$r->id}}</td>
                        <td>{{$r->name}}</td>
                        <td>
                            <div class="col-xs-1">
                                @if (in_array($r->type, ['HTML', 'XLSX', 'PDF']))
                                {!! Form::open(['route' => ['reports.custom.generate'], 'class' => 'form form-validate']) !!}
                                @else
                                {!! Form::open(['route' => ['reports.standard.'.$r->type], 'class' => 'form form-validate']) !!}
                                @endif
                                {!! Form::hidden('client_report_id', $r->id) !!}
                                <button type="submit" class="btn btn-info btn-xs" title="Generate Report"><span class="md-file-download"></span></button>
                                {!! Form::close() !!}
                            </div>
                            @if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
                            <div class="col-xs-1">
                                <button data-href="{{ route('reports.delete', $r->id) }}" type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-trash"></span></button>
                            </div>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</section>

@include('admin.partials.deleteConfirmation')

@endsection
