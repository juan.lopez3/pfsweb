@extends('admin.layouts.default')

@section('title')
Create Report
@endsection

@section('content')

<section>
<div class="section-body">
	<div class="col-lg-offset-1 col-md-12 col-lg-10">
		<div class="card">
			<div class="card-head style-primary">
				<header>Create Custom Report</header>
			</div>
			
			<div class="card-body">
				<div class="row">
					<a href="{{route('reports.custom')}}"><button type="button" class="btn btn-success button-raised">Create New</button></a>
				</div>
				<hr/>
				
				{!! Form::open(['route' => ['reports.custom.regenerate'], 'class' => 'form form-validate']) !!}
				<div class="row">
					<div class="col-xs-6">
						{!! Form::select('saved_report_id', ['-1'=>'Select saved report']+$saved_reports, null, ['class' => 'form-control']) !!}
					</div>
					
					<div class="col-xs-2">
						{!! Form::submit('Regenerate report', ['class'=>'btn btn-flat btn-primary']) !!}
					</div>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
</section>
@endsection
