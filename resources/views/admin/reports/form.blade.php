<div class="col-md-6">
	{!! Form::label('report_type', 'Report Type:') !!}
	{!! Form::select('report_type', $report_types, null, ['class'=>'form-control', 'id' => 'report_type']) !!}
</div>
<div class="col-md-6 f-dates">
	<div class="form-group">
		<div class="input-daterange input-group" id="demo-date-range">
			{!! Form::label('from', 'Data dates', ['class' => 'control-label']) !!}
			<div class="input-group-content">
				{!! Form::text('from', null, ['class' => 'form-control']) !!}
			</div>
			<span class="input-group-addon">to</span>
			<div class="input-group-content">
				{!! Form::text('to', null, ['class' => 'form-control']) !!}
			</div>
		</div>
	</div>
</div>


<div class="col-md-6 f-event_title">
	{!! Form::label('event_title', 'Old Event Name') !!}
	{!! Form::text('event_title', null, ['class'=>'form-control', 'id' => 'event_title']) !!}
	{!! Form::hidden('event_id', null, array('id' => 'event_id')) !!}
</div>




<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/bootstrap-datepicker/datepicker3.css')}}" />
<script type="text/javascript" src="{{asset('assets/admin/js/libs/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/jquery-ui/jquery-ui-theme.css')}}" />


<style>
	.f-event_title, .f-dates{ display:none; }
</style>

<script type="text/javascript">

$(function() {
	$("#event_title").autocomplete({
        source: function( request, response ) {
        $.ajax({
            url: "{!! route('events.geteventslist') !!}",
            dataType: "json",
            data: {term: request.term},
            success: function(data) {
                        response($.map(data, function(item) {
                        return {
                            label: item.title,
                            id: item.id,
                            };
                    }));
                }
            });
        },
        minLength: 3,
        select: function(event, ui) {
            $('#event_id').val(ui.item.id);
        }
    });
});

$(document).ready(function(){
	$('#report_type').bind('change', function(){
		
		switch(this.value) {
			
			case '1':
				$('.f-dates').show();
				$('.f-event_title').hide();
			break;
			
			case '2':
				$('.f-dates').hide();
				$('.f-event_title').show();
			break;
			
			case '3':
				$('.f-dates').hide();
				$('.f-event_title').show();
			break;
			
		}
	});
});
</script>