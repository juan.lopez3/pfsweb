<div class="section-body">
	<div class="col-md-12">
		<div class="card">
			<div class="card-head style-primary">
				<header>Add Reporting Campaign</header>
			</div>
			
			@include('admin.partials.validationErrors')
			
			{!! Form::open(['route' => ['reports.outside.store'], 'class' => 'form form-validate']) !!}
			<div class="card-body">
				 @include('admin.reports.outside.form')
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					{!! Form::submit('CREATE Reporting Campaign', ['class' => 'btn btn-flat btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>