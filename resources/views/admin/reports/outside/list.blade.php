@extends('admin.layouts.default')

@section('title')
	Outside Reporting
@endsection

@section('content')

<script type="text/javascript">
	$(document).ready(function() {
		$('#regions').dataTable({
			"iDisplayLength": 50,
		});
	} );
</script>

<section>
<div class="section-header">
	<ol class="breadcrumb">
		<li class="active">Outside Reporting</li>
	</ol>
</div>
<div class="section-body">	
	<p><button data-href="#" type="button" class="btn ink-reaction btn-raised btn-success" data-toggle="modal" data-target="#add_campaign">ADD Reporting Campaign</button></p>
	<div class="card">
		<div class="card-body">
			<table id="regions" class="table table-striped table-responsive" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Client</th>
						<th>Email</th>
						<th>Interval</th>
						<th>Actions</th>
					</tr>
				</thead>
			
				<tbody>
					@foreach ($reports as $r)
					<tr>
						<td>{{ $r->name }}</td>
						<td>{{ $r->email }}</td>
						<td>{{ $r->interval }}</td>
						<td>
							<a href="{{ route('reports.outside.edit', $r->id) }}"><button type="button" class="btn btn-warning btn-xs" title="Edit"><span class="glyphicon glyphicon-pencil"></span></button></a>
							<button data-href="{{ route('reports.outside.delete', $r->id) }}" type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-trash"></span></button>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
</section>

@include('admin.partials.deleteConfirmation')
<div class="modal fade" id="add_campaign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		@include('admin.reports.outside.create')
	</div>
</div>
@endsection
