<script type="text/javascript" src="{{asset('assets/admin/js/libs/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('assets/admin/fancybox/source/jquery.fancybox.js')}}"></script>
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/bootstrap-datepicker/datepicker3.css?1424887858')}}" />

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('name', 'Client Name') !!}
            {!! Form::text('name', null, ['class'=>'form-control', 'required']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('email', 'Email') !!}
            {!! Form::email('email', null, ['class'=>'form-control', 'required']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group">
        <div class="input-daterange input-group" id="demo-date-range-1">
            <div class="input-group-content">
                @if (isset($report))
                {!! Form::text('filter_from', date("d/m/Y", strtotime($report->filter_from)), ['class'=>'form-control', 'required']) !!}
                @else
                {!! Form::text('filter_from', null, ['class'=>'form-control', 'required']) !!}
                @endif
                <label>Date range</label>
            </div>
            <span class="input-group-addon">to</span>
            <div class="input-group-content">
                @if (isset($report))
                {!! Form::text('filter_to', date("d/m/Y", strtotime($report->filter_to)), ['class'=>'form-control', 'required']) !!}
                @else
                {!! Form::text('filter_to', null, ['class'=>'form-control', 'required']) !!}
                @endif
                <div class="form-control-line"></div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::select('event_type_id', [''=>'Select event type']+$event_types, null, ['class'=>'form-control', 'id'=>'event_type_id', 'required']) !!}
            <label>Event type</label>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::select('interval', ['1'=>'Daily', '7'=>'Weekly', '30'=> 'Monthly'], null, ['class'=>'form-control']) !!}
            <label>Interval</label>
        </div>
    </div>
    <div class="row" id="type_event">
        <div class="col-sm-6 col-xs-3 text-center">
            <label style="color:red;" name="event_type" class="control-label">Please select an event type to continue</label>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#demo-date-range-1').datepicker({todayHighlight: true, format: "dd/mm/yyyy", weekStart: 1});
    
	// event types input
	if($('#event_type_id').val() == 0) $('#type_event').show();
    
    $('#event_type_id').bind('change', function(event){
        if(this.value == 0) {
    		$('#type_event').show();
    		$("#event_type").show();
    	} else {
    		$('#type_event').hide();
    		$("#event_type").hide();
    	}
    });

</script>

<script type="text/javascript" src="{{asset('assets/site/js/libs/wizard/jquery.bootstrap.wizard.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/site/js/libs/jquery-validation/dist/jquery.validate.min.js')}}"></script>
<script src="{{asset('assets/site/js/libs/wizard/jquery.bootstrap.wizard.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/site/js/core/source/AppForm.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/site/js/core/demo/DemoFormWizard.js')}}"></script>