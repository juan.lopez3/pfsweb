<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<h2>{{$report_title}}</h2>

<table border="1">
	<thead>
	    <tr>
		@foreach($columns as $c)
		<th>{{$c}}</th>
		@endforeach
		</tr>
	</thead>
	<tbody>
		
		@foreach($data as $row)
		<tr>
			@foreach($row as $data)
			<td>{{$data}}</td>
			@endforeach
		</tr>
		@endforeach
	</tbody>
</table>