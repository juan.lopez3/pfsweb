@extends('admin.layouts.default')

@section('title')
Create Report Template
@endsection

@section('content')

<section>
<div class="section-body">
	<div class="col-lg-offset-1 col-md-12 col-lg-10">
		<div class="card">
			<div class="card-head style-primary">
				<header>Add Report Template</header>
			</div>
			
			@include('admin.partials.validationErrors')
			
			{!! Form::open(['route' => ['reports.template.store'], 'class' => 'form form-validate']) !!}
			<div class="card-body">
				 @include('admin.reports.template.form')
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					<a href="{{ URL::previous() }}"><button type="button" class="btn btn-default">Cancel</button></a>
					{!! Form::submit('CREATE REPORT TEMPLATE', ['class' => 'btn btn-flat btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
</section>
@endsection
