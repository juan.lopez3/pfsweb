@extends('admin.layouts.default')

@section('title')
Edit Report Template
@endsection

@section('content')

<section>
<div class="section-body">
	<div class="col-lg-offset-1 col-md-12 col-lg-10">
		<div class="card">
			<div class="card-head style-primary">
				<header>Edit Report Template</header>
			</div>
			
			@include('admin.partials.validationErrors')
			
			{!! Form::model($report_template, ['route' => ['reports.template.update', $report_template->id], 'method' => 'PATCH', 'class' => 'form form-validate']) !!}
			<div class="card-body">
				 @include('admin.reports.template.form')
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					<a href="{{ URL::previous() }}"><button type="button" class="btn btn-flat">Cancel</button></a>
					{!! Form::submit('UPDATE REPORT TEMPLATE', ['class' => 'btn btn-flat btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
</section>
@endsection
