<div class="form-group floating-label">
	{!! Form::text('title', null, ['class'=>'form-control', 'required']) !!}
	{!! Form::label('title', 'Template Title') !!}
</div>

<div class="form-group floating-label">
	{!! Form::label('template', 'Report template') !!}
	@include('admin.partials.templateReportShortTags')
	<br/>
	{!! Form::textarea('template', null, ['row'=>'3', 'style'=>'height: 200px;', 'id'=>'ckeditor']) !!}
</div>