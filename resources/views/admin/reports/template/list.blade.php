@extends('admin.layouts.default')

@section('title')
	Report Templates
@endsection

@section('content')

<script type="text/javascript">
	$(document).ready(function() {
		$('#templates').dataTable();
	} );
</script>

<section>
<div class="section-header">
	<ol class="breadcrumb">
		<li class="active">Report Templates</li>
	</ol>
</div>
<div class="section-body">	
	@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
	<p><a href="{{ route('reports.template.create') }}"><button class="btn ink-reaction btn-raised btn-success" type="button">CREATE TEMPLATE</button></a></p>
	@endif
	<div class="card">
		<div class="card-body">
			
			@include('admin.partials.validationErrors')
			
			<table id="templates" class="table table-striped table-responsive" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Title</th>
						<th>Created</th>
						<th>Modified</th>
						<th>Actions</th>
					</tr>
				</thead>
			
				<tfoot>
					<tr>
						<th>Title</th>
						<th>Created</th>
						<th>Modified</th>
						<th>Actions</th>
					</tr>
				</tfoot>
			
				<tbody>
					@foreach($report_templates as $rt)
					<tr>
						<td>{{$rt->title}}</td>
						<td>{{ date("d/m/Y H:i:s",strtotime($rt->created_at)) }}</td>
						<td>{{ date("d/m/Y H:i:s",strtotime($rt->updated_at)) }}</td>
						<td>
							<a href="{{ route('reports.template.edit', $rt->id) }}"><button type="button" class="btn btn-warning btn-xs" title="Edit"><span class="glyphicon glyphicon-pencil"></span></button></a>
							<button title="Generate Report" type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#generate{{$rt->id}}"><span class="glyphicon glyphicon-download-alt"></span></button>
							<button data-href="{{ route('reports.template.delete', $rt->id) }}" type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-trash"></span></button>
						</td>
					</tr>
					
					<!-- Modal -->
					<div id="generate{{$rt->id}}" class="modal fade" role="dialog">
						<div class="modal-dialog">
						{!! Form::open(['route' => ['reports.template.generate'], 'class' => 'form form-validate']) !!}
						<!-- Modal content-->
						<div class="modal-content">
							<div class="card-head style-primary">
								<header>Generate Template Report</header>
							</div>
							<div class="modal-body">
								{!! Form::hidden('report_template_id', $rt->id) !!}
								{!! Form::select('event_id', [''=>'Select Event'] + $events, null, ['class'=>'form-control select2-list', 'required']) !!}
								
							</div>
							<div class="card-actionbar">
								<div class="card-actionbar-row">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									{!! Form::submit('GENERATE REPORT', ['class' => 'btn btn-flat btn-primary']) !!}
								</div>
							</div>
						</div>
						{!! Form::close() !!}
						</div>
					</div>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
</section>

@include('admin.partials.deleteConfirmation')

@endsection
