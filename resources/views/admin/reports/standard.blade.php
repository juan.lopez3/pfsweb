@extends('admin.layouts.default')

@section('title')
	Create Standard Reports
@endsection

@section('content')
<script type="text/javascript" src="{{asset('assets/admin/js/libs/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/bootstrap-datepicker/datepicker3.css?1424887858')}}" />

<div class="section-header">
	<ol class="breadcrumb">
		<li class="active">Create Standard Reports </li>
	</ol>
</div>
<div class="section-body">	
	<div class="card">
		<div class="card-body">
			@include('admin.partials.validationErrors')
			
			<ul class="nav nav-tabs">
				<li><a data-toggle="tab" href="#event-slot">Booking summary</a></li>
				<li><a data-toggle="tab" href="#event-slot-by-region">Booking summary by region by Quarters</a></li>
				<li><a data-toggle="tab" href="#regional-bookings">Regional Bookings</a></li>
				<li><a data-toggle="tab" href="#total-attendees">Total Attendees</a></li>
				<li><a data-toggle="tab" href="#quater-bookings">Bookings Statistics</a></li>
				<li><a data-toggle="tab" href="#designation">Attendance Designation Statistics</a></li>
				<li><a data-toggle="tab" href="#registration-questions">Registration Questions</a></li>
				<li><a data-toggle="tab" href="#slot-waitinglist">Slot Waiting List</a></li>
				<li><a data-toggle="tab" href="#slot-bookings">Slot Bookings</a></li>
				<li><a data-toggle="tab" href="#selected-sessions">Selected Sessions Capacity</a></li>
				<li><a data-toggle="tab" href="#attendee-list">Attendee list</a></li>
				<li><a data-toggle="tab" href="#attendee-list-by-Date">Attendee list by Dates</a></li>
				<li><a data-toggle="tab" href="#attendee-list-status">Attendee list status</a></li>
				<li><a data-toggle="tab" href="#attendee-list-sponsors">Attendee list Sponsors</a></li>
				<li><a data-toggle="tab" href="#feedback">Event Feedback</a></li>
				<li><a data-toggle="tab" href="#feedback-averages">Feedback answers averages</a></li>
				<li><a data-toggle="tab" href="#opened-emails">Sent Emails vs Opened</a></li>
				<li><a data-toggle="tab" href="#clicked-links">Clicked links</a></li>
				<li><a data-toggle="tab" href="#pfs-master">PFS Master</a></li>
			</ul>
	
			<div class="tab-content">
				 <div id="event-slot" class="tab-pane fade">
					@include('admin.reports.reportForms.event_slot')
				</div>
				<div id="event-slot-by-region" class="tab-pane fade">
					@include('admin.reports.reportForms.event_slot_by_region')
				</div>
				<div id="regional-bookings" class="tab-pane fade">
					@include('admin.reports.reportForms.regional_bookings')
				</div>
				<div id="total-attendees" class="tab-pane fade">
                    @include('admin.reports.reportForms.total_attendees')
                </div>
				<div id="quater-bookings" class="tab-pane fade">
					@include('admin.reports.reportForms.quater_bookings')
				</div>
				<div id="designation" class="tab-pane fade">
					@include('admin.reports.reportForms.designation')
				</div>
				<div id="registration-questions" class="tab-pane fade">
					@include('admin.reports.reportForms.registration_questions')
				</div>
				<div id="slot-waitinglist" class="tab-pane fade">
					@include('admin.reports.reportForms.slot_waiting_list')
				</div>
				<div id="slot-bookings" class="tab-pane fade">
					@include('admin.reports.reportForms.slot_bookings')
				</div>
				<div id="selected-sessions" class="tab-pane fade">
					@include('admin.reports.reportForms.selected_sessions_capacity')
				</div> 
				<div id="attendee-list" class="tab-pane fade">
                    @include('admin.reports.reportForms.attendee_list')
                </div>  
				<div id="attendee-list-by-Date" class="tab-pane fade">
                    @include('admin.reports.reportForms.attendee_list_by_date')
                </div> 
				<div id="attendee-list-status" class="tab-pane fade">
                    @include('admin.reports.reportForms.attendee_list_status')
                </div>
                <div id="attendee-list-sponsors" class="tab-pane fade">
                    @include('admin.reports.reportForms.attendee_list_sponsors')
                </div>
                <div id="feedback" class="tab-pane fade">
                    @include('admin.reports.reportForms.feedback')
                </div>
                <div id="feedback-averages" class="tab-pane fade">
                    @include('admin.reports.reportForms.feedback_averages')
                </div>
				<div id="opened-emails" class="tab-pane fade">
					@include('admin.reports.reportForms.opened_emails')
				</div>
				<div id="clicked-links" class="tab-pane fade">
					@include('admin.reports.reportForms.clicked_links')
				</div>
				<div id="pfs-master" class="tab-pane fade">
					@include('admin.reports.reportForms.pfs_master')
				</div>
			</div>
			
		</div>
	</div>
</div>

@endsection