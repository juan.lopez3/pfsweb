@extends('admin.layouts.default')

@section('title')
	Reports
@endsection

@section('content')

<script type="text/javascript">
	$(document).ready(function() {
		$('#reports').dataTable({
			"iDisplayLength": 50,
		});
	} );
</script>

<section>
<div class="section-header">
	<ol class="breadcrumb">
		<li class="active">Reports</li>
	</ol>
</div>
<div class="section-body">	
	<p><a href="{{ route('reports.create') }}"><button class="btn ink-reaction btn-raised btn-success" type="button">Create Report</button></a></p>
	<div class="card">
		<div class="card-body">
			<table id="reports" class="table table-striped table-responsive" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Report Type</th>
						<th>File name</th>
						<th>Generated By</th>
						<th>Created at</th>
						<th>Actions</th>
					</tr>
				</thead>
			
				<tfoot>
					<tr>
						<th>Report Type</th>
						<th>File name</th>
						<th>Generated By</th>
						<th>Created at</th>
						<th>Actions</th>
					</tr>
				</tfoot>
			
				<tbody>
					@foreach ($reports as $r)
					<tr>
						<td>{{$r->report_type}}</td>
						<td><a href="{{\Config::get('app.root')}}storage/reports/{{$r->filename}}">{{ $r->filename}}</a></td>
						<td>{{ $r->user->first_name}} {{ $r->user->last_name}}</td>
						<td>{{ date("d/m/Y H:i:s",strtotime($r->created_at)) }}</td>
						<td>
							<a href="{{\Config::get('app.root')}}storage/reports/{{$r->filename}}"><button type="button" class="btn btn-info btn-xs" title="Download"><span class="md-file-download"></span></button></a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
</section>

@include('admin.partials.deleteConfirmation')

@endsection
