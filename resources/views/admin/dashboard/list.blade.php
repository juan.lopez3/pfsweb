@extends('admin.layouts.default')

@section('title')
Dashboard
@endsection

@section('content')
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/morris/morris.core.css')}}" />

<!-- BEGIN MORRIS - LINE CHART -->
<section class="style-default">
	<div class="section-body">
		<h2 class="text-primary">Registrations per day</h2>
		<div id="morris-line-graph" class="height-8" data-colors="#9C27B0"></div>
	</div><!--end .section-body -->
</section>
<!-- END MORRIS - LINE CHART -->
<script src="{{asset('assets/admin/js/libs/raphael/raphael-min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/morris.js/morris.min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/d3/d3.v3.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/rickshaw/rickshaw.min.js')}}"></script>

<script type="text/javascript">
$(".loader").hide();
	// Morris line demo
	if ($('#morris-line-graph').length > 0) {
		window.m = Morris.Line({
			element: 'morris-line-graph',
			data: {!! $events_registrations !!},
			xkey: 'date',
			ykeys: ['registrations'],
			labels: ['Registrations'],
			parseTime: false,
			resize: true,
			lineColors: $('#morris-line-graph').data('colors').split(','),
			xLabelMargin: 10,
			integerYLabels: true
		});
	}
</script>
@endsection