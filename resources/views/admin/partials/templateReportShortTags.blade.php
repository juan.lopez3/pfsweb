<div class="row pull-right">
	<button type="button" class="btn btn-xs btn-ink" id="opener">Available tags</button>
</div>
<br/>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script type="text/javascript">
$(function() {
	$( "#opener" ).click(function() {
		$( "#tags" ).toggle( "slow", function() {
			// Animation complete.
		});
	});
});

function aT(tag){

	$("#ckeditor").ckeditor().editor.insertText(tag);
}
</script>

<style type="text/css">
	.tag{cursor: pointer;}
</style>


<div class="row" id="tags" style="font-size: 11px; display:none;">
	<div class="col-sm-6">
		<span class="tag" onclick="aT('EVENT_TYPE');">EVENT_TYPE</span> = Event type<br/>
		<span class="tag" onclick="aT('EVENT_REGION');">EVENT_REGION</span> = Event region<br/>
		<span class="tag" onclick="aT('EVENT_DESCRIPTION');">EVENT_DESCRIPTION</span> = Event description<br/>
		<span class="tag" onclick="aT('EVENT_LINK');">EVENT_LINK</span> = Event page link<br/>
		<span class="tag" onclick="aT('EVENT_START');">EVENT_START</span> = Event Start date<br/>
		<span class="tag" onclick="aT('EVENT_END');">EVENT_END</span> = Event End date<br/>
		<span class="tag" onclick="aT('VENUE_COSTS');">VENUE_COSTS</span> = Venue costs<br/>
		<span class="tag" onclick="aT('EVENT_VENUE_NAME');">EVENT_VENUE_NAME</span> = Venue name<br/>
		<span class="tag" onclick="aT('EVENT_VENUE_COUNTY');">EVENT_VENUE_COUNTY</span> = Venue county<br/>
		<span class="tag" onclick="aT('EVENT_VENUE_POSTCODE');">EVENT_VENUE_POSTCODE</span> = Venue postcode<br/>
		<span class="tag" onclick="aT('EVENT_VENUE_ADDRESS');">EVENT_VENUE_ADDRESS</span> = Venue address<br/>
		<span class="tag" onclick="aT('EVENT_VENUE_EMAIL');">EVENT_VENUE_EMAIL</span> = Venue email<br/>
		<span class="tag" onclick="aT('EVENT_VENUE_PHONE');">EVENT_VENUE_PHONE</span> = Venue phone<br/>
		<span class="tag" onclick="aT('EVENT_VENUE_ROOMS');">EVENT_VENUE_ROOMS</span> = All rooms in venue with room information<br/>
		<span class="tag" onclick="aT('DELEGATES_NUMBER');">DELEGATES_NUMBER</span> = Number of Delegates<br/>
		<span class="tag" onclick="aT('EVENT_SPONSORS');">EVENT_SPONSORS</span> = Event sponsors<br/>
		<span class="tag" onclick="aT('EVENT_HASHTAGS');">EVENT_HASHTAGS</span> = Event hashtags<br/>
		<span class="tag" onclick="aT('ALL_EVENT_SESSIONS');">ALL_EVENT_SESSIONS</span> = All sessions with speakers<br/>
		<span class="tag" onclick="aT('EVENT_SESSIONS_TIMES');">EVENT_SESSIONS_TIMES</span> = All sessions with times<br/>
		<span class="tag" onclick="aT('SESSIONS_DURATION');">SESSIONS_DURATION</span> = Count of hours that will be included to CPD<br/>
		<span class="tag" onclick="aT('SESSION_CONTRIBUTORS');">SESSION_CONTRIBUTORS</span> = All contributors assigned to sessions<br/>
		<span class="tag" onclick="aT('SESSION_CONTRIBUTORS_BIO');">SESSION_CONTRIBUTORS_BIO</span> = All contributors assigned to sessions with bio<br/>
		<span class="tag" onclick="aT('DIETARY_SPECIAL_REQUIREMENTS');">DIETARY_SPECIAL_REQUIREMENTS</span> = List of attendees with dietary and special requirements requirements<br/>
	</div>
	<div class="col-sm-6">
	<!--
		<span class="tag" onclick="aT('LUNCH_TIMING');">LUNCH_TIMING</span> = Scheduled time of session type 'Lunch' - Start - End<br/>
		<span class="tag" onclick="aT('REGISTRATION_TIMING');">REGISTRATION_TIMING</span> = Scheduled time of session type 'Registration' - Start - End<br/>
		<span class="tag" onclick="aT('CHAIRMANS_INTRODUCTION_TIMING');">CHAIRMANS_INTRODUCTION_TIMING</span> = Scheduled time of session type 'Introduction' - Start - End<br/>
		<span class="tag" onclick="aT('MORNING_BREAK');">MORNING_BREAK</span> = Scheduled time of session type 'Morning Break' - Start - End<br/>
		<span class="tag" onclick="aT('AFTERNOON_BREAK');">AFTERNOON_BREAK</span> = Scheduled time of session type 'Afternoon Break' - Start - End<br/>
		//<span class="tag" onclick="aT('AV_TOTAL');">AV_TOTAL</span> = NOT SURE IF THIS IS POSSIBLE<br/>
	-->
		<span class="tag" onclick="aT('MEETING_ROOMS');">MEETING_ROOMS</span> = Meeting spaces<br/>
		<span class="tag" onclick="aT('MEETING_ROOMS_SETUP');">MEETING_ROOMS_SETUP</span> = Meeting spaces with layout and number of tables<br/>
		<span class="tag" onclick="aT('SESSION_TYPE_ID');">SESSION_TYPE_ID</span> = Start and end times of all sessions with selected session type, where ID - session type id<br/>
		<span class="tag" onclick="aT('ATTENDEES_FIRST_SLOT');">ATTENDEES_FIRST_SLOT</span> = Regional event, first slot attendees count<br/>
		<span class="tag" onclick="aT('ATTENDEES_SECOND_SLOT');">ATTENDEES_SECOND_SLOT</span> = Regional event, second slot attendees count<br/>
		<span class="tag" onclick="aT('ATTENDEES_BOTH_SLOT');">ATTENDEES_BOTH_SLOT</span> = Regional event, both slots attendees count<br/>
		<span class="tag" onclick="aT('AV_EQUIPMENT');">AV_EQUIPMENT</span> = List of AV equpment assigned to the event<br/>
		<span class="tag" onclick="aT('TFI_CONTACTS');">TFI_CONTACTS</span> = All contacts assigned to event with Telephone Number<br/>
		<span class="tag" onclick="aT('VENUE_CONTACTS');">VENUE_CONTACT</span> = First Name Last Name of venue contact<br/>
		<span class="tag" onclick="aT('DUTY_MANAGER');">DUTY_MANAGER</span> = CONTACT Duty manager name and contact number<br/>
		<span class="tag" onclick="aT('AV_CONTACT');">AV_CONTACT</span> = AV technichan Name and phone number <br/>
		<span class="tag" onclick="aT('HOSTESSES');">HOSTESSES</span> = All hostesses name and phone<br/>
		<span class="tag" onclick="aT('EVENT_SCHEDULE_START');">EVENT_SCHEDULE_START</span> = First time on the Schedule<br/>
		<span class="tag" onclick="aT('EVENT_SCHEDULE_END');">EVENT_SCHEDULE_END</span> = Last time on the Schedule<br/>
		<span class="tag" onclick="aT('MIN_GUARANTEED_NUMBERS');">MIN_GUARANTEED_NUMBERS</span> = Venue minimum numbers <br/>
		<span class="tag" onclick="aT('DATE_TODAY');">DATE_TODAY</span> = The date<br/>
	</div>
</div>