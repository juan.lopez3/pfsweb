<div class="menubar-fixed-panel">
	<div>
		<a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
			<i class="fa fa-bars"></i>
		</a>
	</div>
	<div class="expanded">
		<a href="{{ route('dashboard') }}">
			<span class="text-lg text-bold text-primary ">MATERIAL&nbsp;ADMIN</span>
		</a>
	</div>
</div>
<div class="menubar-scroll-panel">

	<!-- BEGIN MAIN MENU -->
	<ul id="main-menu" class="gui-controls">

		<!-- BEGIN DASHBOARD -->
		<li>
			<a href="{{ route('dashboard') }}" class="{{ isActiveRoute('dashboard') }}" >
				<div class="gui-icon"><i class="md md-home"></i></div>
				<span class="title">Dashboard</span>
			</a>
		</li>
		<!-- END DASHBOARD -->

		
		<!-- BEGIN EVENTS -->
		@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'),role('regional_coordinator'), role('pfs_staff'), role('host'), role('venue_staff'), role('event_coordinator')]))
		<li class="gui-folder">
			<a class="{{ areActiveRoutes(['events.scheduleBuilder', 'events.atendees', 'events.costs', 'events.onsiteDetails']) }}">
				<div class="gui-icon"><i class="glyphicon glyphicon-globe"></i></div>
				<span class="title">Events</span>
			</a>
			<!--start submenu -->
			<ul>
				<li class="gui-folder">
					<a href="javascript:void(0);">
						<span class="title">Forthcoming</span>
					</a>
					<ul>
					    @if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
						<li><a href="{{ route('events.create') }}" class="{{ isActiveRoute('events.create') }}"><span class="title">Add new</span></a></li>
						@endif
						<li><a href="{{ route('events.forthcoming') }}" class="{{ areActiveRoutes(['events.forthcoming', 'events.edit', 'events.statistics', 'events.emails.filter']) }}"><span class="title">View / Edit</span></a></li>
					</ul>
				</li> 
				<li><a href="{{ route('events.previous') }}" class="{{ isActiveRoute('events.previous') }}"><span class="title">Previous</span></a></li>		
				<li><a href="{{ route('events.archived') }}" class="{{ isActiveRoute('events.archived') }}"><span class="title">Archive</span></a></li>				
			</ul>
		</li>
		@endif
		<!-- END EVENTS -->
		
		<!-- BEGIN VENUES -->
		@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator'), role('regional_coordinator')]))
		<li class="gui-folder">
			<a>
				<div class="gui-icon"><i class="glyphicon glyphicon-tower"></i></div>
				<span class="title">Venues</span>
			</a>
			<!--start submenu -->
			<ul>
			    @if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
				<li><a href="{{ route('venues.create') }}" class="{{ areActiveRoutes(['venues.create']) }}"><span class="title">Add New</span></a></li>
				@endif
				<li><a href="{{ route('venues.list') }}" class="{{ areActiveRoutes(['venues.list', 'venues.edit']) }}"><span class="title">View / Edit</span></a></li>
			</ul>
		</li>
		@endif
		
		<!-- END VENUES -->
		
		<!-- BEGIN USERS -->
		@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
		<li class="gui-folder">
			<a>
				<div class="gui-icon"><i class="glyphicon glyphicon-user"></i></div>
				<span class="title">Users</span>
			</a>
			<!--start submenu -->
			<ul>
				<li><a href="{{ route('users.create') }}" class="{{ isActiveRoute('users.create') }}"><span class="title">Add new</span></a></li>
				<li><a href="{{ route('users.list') }}" class="{{ areActiveRoutes(['users.list', 'users.listActions', 'users.edit']) }}"><span class="title">View / Edit</span></a></li>
				<li><a href="{{ route('users.profile', Auth::user()->id) }}" class="{{ isActiveRoute('users.profile') }}"><span class="title">My Profile</span></a></li>
			</ul>
		</li>
		@endif
		<!-- END USERS -->
		
		<!-- SPONSORS -->
		@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
		<li>
			<a href="{{ route('sponsors') }}" class="{{ areActiveRoutes(['sponsors', 'sponsors.create', 'sponsors.edit']) }}">
				<div class="gui-icon"><i class="glyphicon glyphicon-briefcase"></i></div>
				<span class="title">Sponsors</span>
			</a>
		</li>
		@endif
		<!-- SPONSORS -->
		
		<!-- BEGIN REPORTS -->
		@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('member'), role('non_member'), role('event_coordinator'), role('regional_coordinator'), role('pfs_staff')]))
		<li class="gui-folder">
			<a>
				<div class="gui-icon"><i class="glyphicon glyphicon-file"></i></div>
				<span class="title">Reports</span>
			</a>
			<!--start submenu -->
			<ul>
			    @if(hasRoles(Auth::user()->role_id, [role('super_administrator'),  role('event_manager'), role('pfs_staff'), role('regional_coordinator'), role('regional_coordinator')]))
			    <li><a href="{{ route('reports.standard') }}" class="{{ areActiveRoutes(['reports.standard']) }}"><span class="title">Create Standard Report</span></a></li>
				@endif
				@if(hasRoles(Auth::user()->role_id, [role('super_administrator'),  role('event_manager'), role('regional_coordinator'), role('regional_coordinator')]))
				<li><a href="{{ route('reports.outside') }}" class="{{ areActiveRoutes(['reports.outside']) }}"><span class="title">Outside Reporting</span></a></li>
				<li><a href="{{ route('reports.create') }}" class="{{ areActiveRoutes(['reports.create', 'reports.custom']) }}"><span class="title">Create Custom Report</span></a></li>
				<li><a href="{{ route('reports.template') }}" class="{{ isActiveRoute('reports.template') }}"><span class="title">Create Template Report</span></a></li>
				<li><a href="{{ route('reports') }}" class="{{ areActiveRoutes(['reports', 'reports.edit']) }}"><span class="title">View Reports</span></a></li>
				@endif
				<li><a href="{{ route('reports.client') }}" class="{{ areActiveRoutes(['reports.client']) }}"><span class="title">PFS Reports</span></a></li>
			</ul>
		</li>
		@endif
		<!-- END REPORTS -->
		
		<!-- SPONSORS -->
		@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
		<li>
			<a href="{{ route('files') }}" class="{{ areActiveRoutes(['files', 'files.create', 'files.edit']) }}">
				<div class="gui-icon"><i class="glyphicon glyphicon-file"></i></div>
				<span class="title">Files</span>
			</a>
		</li>
		@endif
		<!-- SPONSORS -->

		<!-- SENT EMAILS -->
		@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
		<li>
			<a href="{{ route('emails') }}" class="{{ areActiveRoutes(['emails']) }}">
				<div class="gui-icon"><i class="glyphicon glyphicon-envelope"></i></div>
				<span class="title">Sent Emails Log</span>
			</a>
		</li>
		@endif
		<!-- SENT EMAILS -->
		
		<!-- SETTING -->
		@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
		<li>
			<a href="{{ route('settings') }}" class="{{ areActiveRoutes(['settings', 'settings.technologies', 'settings.regions', 'settings.attendeeTypes', 'settings.sessionTypes', 'settings.sponsors', 'settings.eventTypes']) }}">
				<div class="gui-icon"><i class="glyphicon glyphicon-cog"></i></div>
				<span class="title">Settings</span>
			</a>
		</li>
		@endif
		<!-- SETTING -->
        
        
        <!-- ISSUES -->
        @if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
        <li>
            <a href="{{ route('issues') }}" class="{{ areActiveRoutes(['issues']) }}">
                <div class="gui-icon"><i class="glyphicon glyphicon-wrench"></i></div>
                <span class="title">Support</span>
            </a>
        </li>
        @endif
        <!-- ISSUES -->
        
		<!-- LOGOUT -->
		<li>
			<a href="{{ url('/auth/logout') }}">
				<div class="gui-icon"><i class="fa fa-power-off"></i></div>
				<span class="title">Logout</span>
			</a>
		</li>
		<!-- LOGOUT -->

	</ul>
	<!-- END MAIN MENU -->

	<div class="menubar-foot-panel">
		<small class="no-linebreak hidden-folded">
			<span class="opacity-75">Copyright &copy; 2016</span> <strong>TFI</strong>
		</small>
	</div>
</div>