<div class="row pull-right">
	<button type="button" class="btn btn-xs btn-ink" id="opener">Available tags</button>
</div>
<br/>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script type="text/javascript">
$(function() {
	
	$( "#opener" ).click(function() {
		$( "#tags" ).toggle( "slow", function() {
			// Animation complete.
		});
	});
});

function aT(tag){

	$("#ckeditor").ckeditor().editor.insertText(tag);
}
</script>

<style type="text/css">
	.tag{cursor: pointer;}
</style>


<div class="row" id="tags" style="font-size: 11px; display: none;">
	
	<div class="col-sm-6">
		<span class="tag" onclick="aT('TITLE');">TITLE</span> = Title<br/>
		<span class="tag" onclick="aT('FIRST_NAME');">FIRST_NAME</span> = First name<br/>
		<span class="tag" onclick="aT('LAST_NAME');">LAST_NAME</span> = Last name<br/>
		<span class="tag" onclick="aT('EVENT_NAME');">EVENT_NAME</span> = Event name<br/>
		<span class="tag" onclick="aT('EVENT_REGION');">EVENT_REGION</span> = Event region<br/>
		<span class="tag" onclick="aT('EVENT_DESCRIPTION');">EVENT_DESCRIPTION</span> = Event description<br/>
		<span class="tag" onclick="aT('EVENT_LINK');">EVENT_LINK</span> = Event page link<br/>
		<span class="tag" onclick="aT('EVENT_AGENDA');">EVENT_AGENDA</span> = Event agenda link<br/>
		<span class="tag" onclick="aT('EVENT_CONTRIBUTORS');">EVENT_CONTRIBUTORS</span> = Link to event contributors document<br/>
		<span class="tag" onclick="aT('EDIT_MY_EVENT');">EDIT_MY_EVENT</span> = Link to registered event view page<br/>
		<span class="tag" onclick="aT('EVENT_START');">EVENT_START</span> = Event Start date<br/>
		<span class="tag" onclick="aT('EVENT_END');">EVENT_END</span> = Event End date<br/>
		<span class="tag" onclick="aT('MEETING_ROOMS');">MEETING_ROOMS</span> = Meeting spaces<br/>
		<span class="tag" onclick="aT('MEETING_ROOMS_SETUP');">MEETING_ROOMS_SETUP</span> = Meeting spaces with layout and number of tables<br/>
		<span class="tag" onclick="aT('DELEGATES_NUMBER');">DELEGATES_NUMBER</span> = Number of Delegates<br/>
		<span class="tag" onclick="aT('EVENT_SPONSORS');">EVENT_SPONSORS</span> = Event sponsors<br/>
		<span class="tag" onclick="aT('EVENT_HASHTAGS');">EVENT_HASHTAGS</span> = Event hashtags<br/>
		<span class="tag" onclick="aT('BOOKED_SESSIONS');">BOOKED_SESSIONS</span> = Booked slots<br/>
		<span class="tag" onclick="aT('WAITLISTED_SESSIONS');">WAITLISTED_SESSIONS</span> = Waitlisted slots<br/>
		<span class="tag" onclick="aT('ALL_EVENT_SESSIONS');">ALL_EVENT_SESSIONS</span> = All event sessions with speakers<br/>
		<span class="tag" onclick="aT('EVENT_SESSIONS_TIMES');">EVENT_SESSIONS_TIMES</span> = All event sessions with times<br/>
		<span class="tag" onclick="aT('SESSIONS_DURATION');">SESSIONS_DURATION</span> = Event sessions duration<br/>
		<span class="tag" onclick="aT('FEEDBACK');">FEEDBACK</span> = Link to event's feedback page<br/>
		<span class="tag" onclick="aT('CERTIFICATE');">CERTIFICATE</span> = Link to download CPD certificate<br/>
		<span class="tag" onclick="aT('USER_PIN');">USER_PIN</span> = User PIN code<br/>
		<span class="tag" onclick="aT('PROFILE_LINK');">PROFILE_LINK</span> = User profile link<br/>
		<span class="tag" onclick="aT('EDIT_PROFILE');">EDIT_PROFILE</span> = Edit User profile link<br/>
		<span class="tag" onclick="aT('ACCEPT_INVITATION');">ACCEPT_INVITATION</span> = Link to accept event invitation<br/>
		<span class="tag" onclick="aT('DECLINE_INVITATION');">DECLINE_INVITATION</span> = Link to decline event invitation<br/>
		
		<span class="tag" onclick="aT('MY_EVENTS');">MY_EVENTS</span> = Link to my events page<br/>
		<span class="tag" onclick="aT('NON_MEMBER_CONFIRMATION_TEXT');">NON_MEMBER_CONFIRMATION_TEXT</span> = Statis piece of text for non members in registration confirmation email template<br/>
		<span class="tag" onclick="aT('NON_MEMBER_THANKYOU_TEXT');">NON_MEMBER_THANKYOU_TEXT</span> = Statis piece of text for non members in thank you email template<br/>
	</div>
	<div class="col-sm-6">
		<span class="tag" onclick="aT('SESSION_TYPE_ID');">SESSION_TYPE_ID</span> = Start and end times of all sessions with selected session type, where ID - session type id<br/>
		<span class="tag" onclick="aT('ATTENDEES_FIRST_SLOT');">ATTENDEES_FIRST_SLOT</span> = Regional event, first slot attendees count<br/>
		<span class="tag" onclick="aT('ATTENDEES_SECOND_SLOT');">ATTENDEES_SECOND_SLOT</span> = Regional event, second slot attendees count<br/>
		<span class="tag" onclick="aT('ATTENDEES_BOTH_SLOT');">ATTENDEES_BOTH_SLOT</span> = Regional event, both slots attendees count<br/>
		<span class="tag" onclick="aT('EVENT_VENUE_NAME');">EVENT_VENUE_NAME</span> = Venue name<br/>
		<span class="tag" onclick="aT('VENUE_COSTS');">VENUE_COSTS</span> = Venue costs<br/>
		<span class="tag" onclick="aT('VENUE_ATTENDEE_LIST');">VENUE_ATTENDEE_LIST</span> = Event attendee list for venue<br/>
		<span class="tag" onclick="aT('EVENT_VENUE_COUNTY');">EVENT_VENUE_COUNTY</span> = Venue county<br/>
		<span class="tag" onclick="aT('EVENT_VENUE_POSTCODE');">EVENT_VENUE_POSTCODE</span> = Venue postcode<br/>
		<span class="tag" onclick="aT('EVENT_VENUE_ADDRESS');">EVENT_VENUE_ADDRESS</span> = Venue address<br/>
		<span class="tag" onclick="aT('EVENT_VENUE_EMAIL');">EVENT_VENUE_EMAIL</span> = Venue email<br/>
		<span class="tag" onclick="aT('EVENT_VENUE_PHONE');">EVENT_VENUE_PHONE</span> = Venue phone<br/>
		<span class="tag" onclick="aT('AV_EQUIPMENT');">AV_EQUIPMENT</span> = List of AV equpment assigned to the event<br/>
		<span class="tag" onclick="aT('TFI_CONTACTS');">TFI_CONTACTS</span> = All contacts assigned to event with Telephone Number<br/>
		<span class="tag" onclick="aT('VENUE_CONTACTS');">VENUE_CONTACT</span> = First Name Last Name of venue contact<br/>
		<span class="tag" onclick="aT('DUTY_MANAGER');">DUTY_MANAGER</span> = CONTACT Duty manager name and contact number<br/>
		<span class="tag" onclick="aT('AV_CONTACT');">AV_CONTACT</span> = AV technichan Name and phone number <br/>
		<span class="tag" onclick="aT('HOSTESSES');">HOSTESSES</span> = All hostesses name and phone<br/>
		<span class="tag" onclick="aT('EVENT_SCHEDULE_START');">EVENT_SCHEDULE_START</span> = First time on the Schedule<br/>
		<span class="tag" onclick="aT('EVENT_SCHEDULE_END');">EVENT_SCHEDULE_END</span> = Last time on the Schedule<br/>
		<span class="tag" onclick="aT('MIN_GUARANTEED_NUMBERS');">MIN_GUARANTEED_NUMBERS</span> = Venue minimum numbers <br/>
		<span class="tag" onclick="aT('DATE_TODAY');">DATE_TODAY</span> = The date<br/>
	</div>
</div>
