@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator'), role('host')]))
<a href="{{ route('events.edit', $event->id) }}"><button type="button" class="btn btn-flat btn-warning btn-sm" title="Settings"><span class="glyphicon glyphicon-cog"></span></button></a>
@endif
@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator'), role('host'), role('regional_coordinator')]))
<a href="{{ route('events.atendees', $event->id) }}"><button type="button" class="btn btn-flat btn-success btn-sm" title="Attendees"><span class="glyphicon glyphicon-user"></span></button></a>
@endif
@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
<a href="{{ route('events.costs', $event->id) }}"><button type="button" class="btn btn-flat btn-info btn-sm" title="Costs"><span class="glyphicon glyphicon-gbp"></span></button></a>
<a href="{{ route('events.scheduleBuilder', $event->id) }}"><button type="button" class="btn btn-flat btn-accent-dark btn-sm" title="Schedule"><span class="glyphicon glyphicon-time"></span></button></a>
@endif
@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator'), role('host')]))
<a href="{{ route('events.onsiteDetails', $event->id) }}"><button type="button" class="btn btn-flat btn-accent-light btn-sm" title="On site Details"><span class="glyphicon glyphicon-th"></span></button></a>
<a href="{{ route('events.emails.filter', $event->id) }}"><button type="button" class="btn btn-flat btn-primary btn-sm" title="Email attendees"><span class="glyphicon glyphicon-envelope"></span></button></a>
<a href="{{ route('events.statistics', $event->id) }}"><button type="button" class="btn btn-flat btn-danger btn-sm" title="Statistics"><span class="glyphicon glyphicon-signal"></span></button></a>
@endif
@if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator'), role('host'), role('venue_staff'), role('regional_coordinator')]))
<a href="{{ route('events.onsite', $event->id) }}"><button type="button" class="btn btn-flat btn-success btn-sm" title="On site LIVE"><span class="glyphicon glyphicon-play"></span></button></a>
@endif