<div class="headerbar">
	<div class="headerbar-left">
		<ul class="header-nav header-nav-options">
			<li class="header-nav-brand" >
				<div class="brand-holder">
					<a href="{{ route('dashboard') }}">
						<span class="text-lg text-bold text-primary">{!! HTML::image('images/logo.png', '', ['class'=>'img-responsive']) !!}</span>
					</a>
				</div>
			</li>
			<li>
				<a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
					<i class="fa fa-bars"></i>
				</a>
			</li>
		</ul>
	</div>
	<!-- Collect the nav links, forms, and other content for toggling -->
	<div class="headerbar-right">
		<!--
		<ul class="header-nav header-nav-options">
			<li>
				<!-- Search form -->
			<!--	<form class="navbar-search" role="search">
					<div class="form-group">
						<input type="text" class="form-control" name="headerSearch" placeholder="Enter your keyword">
					</div>
					<button type="submit" class="btn btn-icon-toggle ink-reaction"><i class="fa fa-search"></i></button>
				</form>
			</li>
			<!--
			<li class="dropdown hidden-xs">
				<a href="javascript:void(0);" class="btn btn-icon-toggle btn-default" data-toggle="dropdown">
					<i class="fa fa-bell"></i><sup class="badge style-danger">4</sup>
				</a>
				<ul class="dropdown-menu animation-expand">
					<li class="dropdown-header">Today's messages</li>
					<li>
						<a class="alert alert-callout alert-warning" href="javascript:void(0);">
							<img class="pull-right img-circle dropdown-avatar" src="../../assets/img/avatar2.jpg?1404026449" alt="" />
							<strong>Alex Anistor</strong><br/>
							<small>Testing functionality...</small>
						</a>
					</li>
					<li>
						<a class="alert alert-callout alert-info" href="javascript:void(0);">
							<img class="pull-right img-circle dropdown-avatar" src="../../assets/img/avatar3.jpg?1404026799" alt="" />
							<strong>Alicia Adell</strong><br/>
							<small>Reviewing last changes...</small>
						</a>
					</li>
					<li class="dropdown-header">Options</li>
					<li><a href="../../html/pages/login.html">View all messages <span class="pull-right"><i class="fa fa-arrow-right"></i></span></a></li>
					<li><a href="../../html/pages/login.html">Mark as read <span class="pull-right"><i class="fa fa-arrow-right"></i></span></a></li>
				</ul>
			</li>
			
		</ul><!--end .header-nav-options -->
		
		<ul class="header-nav header-nav-profile">
			<li class="dropdown">
				<a href="javascript:void(0);" class="dropdown-toggle ink-reaction" data-toggle="dropdown">
					@if(!empty(Auth::user()->profile_image))
						{!! HTML::image('uploads/profile_images/'.Auth::user()->profile_image) !!}
					@else	
						{!! HTML::image('uploads/no_image.png') !!}
					@endif
					
					<span class="profile-info">
						{{Auth::user()->first_name}} {{Auth::user()->last_name}}
						<small>{{Auth::user()->role->title}}</small>
					</span>
				</a>
				<ul class="dropdown-menu animation-dock">
					<li><a href="{{ route('users.profile', Auth::user()->id) }}">My profile</a></li>
					<li class="divider"></li>
					<li><a href="{{ url('/auth/logout') }}"><i class="fa fa-fw fa-power-off text-danger"></i> Logout</a></li>
				</ul>
			</li>
		</ul>
	</div>
</div>