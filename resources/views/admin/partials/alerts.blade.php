@if(Session::has('success'))
<div class="alert alert-success">
	<a class="close" title="close" aria-label="close" data-dismiss="alert" href="#">×</a>
	<strong>Success!</strong> {!!Session::get('success')!!}
</div>
@endif

@if(Session::has('warning'))
<div class="alert alert-warning">
	<a class="close" title="close" aria-label="close" data-dismiss="alert" href="#">×</a>
	<strong>Warning!</strong> {!!Session::get('warning')!!}
</div>
@endif

@if(Session::has('error'))
<div class="alert alert-danger">
	<a class="close" title="close" aria-label="close" data-dismiss="alert" href="#">×</a>
	<strong>Error!</strong> {!!Session::get('error')!!}
</div>
@endif