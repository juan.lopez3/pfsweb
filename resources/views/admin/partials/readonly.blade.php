@if(!hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator'), role('regional_coordinator')]))
<script type="text/javascript">	
	$("#{{$form_id}} :input").attr("disabled", true);
</script>
@endif