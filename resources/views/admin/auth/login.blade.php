<!DOCTYPE html>
<html lang="en">
	<head>
		<title>PFS Admin - Login</title>
		<!-- BEGIN META -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- END META -->

		<!-- BEGIN STYLESHEETS -->
		<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/bootstrap.css')}}" />
		<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/materialadmin.css')}}" />
		<!-- END STYLESHEETS -->

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script type="text/javascript" src="../../assets/js/libs/utils/html5shiv.js?1403934957"></script>
		<script type="text/javascript" src="../../assets/js/libs/utils/respond.min.js?1403934956"></script>
		<![endif]-->
	</head>
<body class="menubar-hoverable header-fixed ">

<!-- BEGIN LOGIN SECTION -->
<section class="section-account">
	<div class="spacer"></div>
	<div class="card contain-sm style-transparent">
		<div class="card-body">
			
			<div class="row">
				<div class="col-sm-offset-2 col-sm-8">
					<br/>
					<span class="text-lg text-bold text-primary">PFS ADMIN</span>
					<br/><br/>
					{!! Form::open(['url' => '/auth/login', 'class' => 'form form-validate']) !!}
						<div class="form-group">
							{!! Form::label('email', 'Email') !!}
							{!! Form::input('email', 'email',  null, ['class'=>'form-control', 'required']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('password', 'Password') !!}
							{!! Form::password('password', ['class'=>'form-control', 'required']) !!}
							<p class="help-block"><a href="{{ url('/password/email') }}" target="_parent">Forgotten password</a></p>
						</div>
						<br/>
						<div class="row">
							<div class="col-xs-6 text-left">
								<div class="checkbox checkbox-inline checkbox-styled">
									<label>
										<input type="checkbox" name="remember"> <span>Remember me</span>
									</label>
								</div>
							</div>
							<div class="col-xs-6 text-right">
								{!! Form::submit('Login', ['class' => 'btn btn-primary btn-raised', 'style'=>'width:225px']) !!}
							</div>
						</div>
					{!! Form::close() !!}
				</div>
			
			</div>
		</div>
	</div>
</section>
<!-- BEGIN JAVASCRIPT -->
<script src="{{asset('assets/admin/js/libs/jquery/jquery-1.11.2.min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/jquery-ui/jquery-ui.min.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/jquery-validation/dist/jquery.validate.min.js')}}"></script>
<script src="{{asset('assets/admin/js/core/source/AppForm.js')}}"></script>
<!-- END JAVASCRIPT -->

</body>
</html>
