@extends('admin.layouts.default')

@section('title')
Edit Technology
@endsection

@section('content')

<section>
<div class="section-body">
	<div class="col-lg-offset-2 col-md-7">
		<div class="card">
			<div class="card-head style-primary">
				<header>Edit Technology</header>
			</div>
			
			@include('admin.partials.validationErrors')
			
			{!! Form::model($technology, ['route' => ['settings.technologies.update', $technology->id], 'method' => 'PATCH', 'class' => 'form form-validate']) !!}
			<div class="card-body">
				 @include('admin.settings.technologies.form')
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					<a href="{{ URL::previous() }}"><button type="button" class="btn btn-flat">Cancel</button></a>
					{!! Form::submit('UPDATE Technology', ['class' => 'btn btn-flat btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
</section>
@endsection
