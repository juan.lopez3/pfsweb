<div class="form-group floating-label">
	{!! Form::text('title', null, ['class'=>'form-control', 'required']) !!}
	{!! Form::label('title', 'Technology name') !!}
</div>
<div class="form-group floating-label">
	{!! Form::text('description', null, ['class'=>'form-control']) !!}
	{!! Form::label('description', 'Description') !!}
</div>