@extends('admin.layouts.default')

@section('title')
	Technology List
@endsection

@section('content')

<script type="text/javascript">
	$(document).ready(function() {
		$('#technique').dataTable({
			"iDisplayLength": 50,
		});
	} );
</script>

<section>
<div class="section-header">
	<ol class="breadcrumb">
		<li class="active">Technology List</li>
	</ol>
</div>
<div class="section-body">	
	<p><button data-href="#" type="button" class="btn ink-reaction btn-raised btn-success" data-toggle="modal" data-target="#add_technology">ADD Technology</button></p>
	<div class="card">
		<div class="card-body">
			<table id="technique" class="table table-striped table-responsive" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Created</th>
						<th>Modified</th>
						<th>Actions</th>
					</tr>
				</thead>
			
				<tfoot>
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Created</th>
						<th>Modified</th>
						<th>Actions</th>
					</tr>
				</tfoot>
			
				<tbody>
					@foreach ($technologies as $t)
					<tr>
						<td>{{ $t->title }}</td>
						<td>{{ $t->description}}</td>
						<td>{{ date("d/m/Y H:i:s",strtotime($t->created_at)) }}</td>
						<td>{{ date("d/m/Y H:i:s",strtotime($t->updated_at)) }}</td>
						<td>
							<a href="{{ route('settings.technologies.edit', $t->id) }}"><button type="button" class="btn btn-warning btn-xs" title="Edit"><span class="glyphicon glyphicon-pencil"></span></button></a>
							<button data-href="{{ route('settings.technologies.delete', $t->id) }}" type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-trash"></span></button>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
</section>

@include('admin.partials.deleteConfirmation')
<div class="modal fade" id="add_technology" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		@include('admin.settings.technologies.create')
	</div>
</div>
@endsection
