@extends('admin.layouts.default')

@section('title')
Create Session Type
@endsection

@section('content')

<section>
<div class="section-body">
	<div class="col-lg-offset-2 col-md-7">
		<div class="card">
			<div class="card-head style-primary">
				<header>Add Session Type</header>
			</div>
			
			@include('admin.partials.validationErrors')
			
			{!! Form::open(['route' => ['settings.sessionTypes.store'], 'class' => 'form form-validate']) !!}
			<div class="card-body">
				 @include('admin.settings.sessionTypes.form')
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					<a href="{{ URL::previous() }}"><button type="button" class="btn btn-default">Cancel</button></a>
					{!! Form::submit('CREATE SESSION TYPE', ['class' => 'btn btn-flat btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
</section>
@endsection
