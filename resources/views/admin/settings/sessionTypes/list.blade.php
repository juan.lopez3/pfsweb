@extends('admin.layouts.default')

@section('title')
	Session Types
@endsection

@section('content')

<script type="text/javascript">
	$(document).ready(function() {
		$('#session_types').dataTable({
			"iDisplayLength": 50,
		});
	} );
</script>

<section>
<div class="section-header">
	<ol class="breadcrumb">
		<li class="active">Session Types</li>
	</ol>
</div>
<div class="section-body">	
	<p><a href="{{ route('settings.sessionTypes.create') }}"><button class="btn ink-reaction btn-raised btn-success" type="button">ADD SESSION TYPE</button></a></p>
	<div class="card">
		<div class="card-body">
			<table id="session_types" class="table table-striped table-responsive" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>ID</th>
						<th>Session Type Name</th>
						<th>Description</th>
						<th>Color</th>
						<th>Include hours in CPD</th>
						<th>Include generating feedback</th>
						<th>Created</th>
						<th>Modified</th>
						<th>Actions</th>
					</tr>
				</thead>
			
				<tfoot>
					<tr>
						<th>ID</th>
						<th>Session Type Name</th>
						<th>Description</th>
						<th>Color</th>
						<th>Include hours in CPD</th>
						<th>Include generating feedback</th>
						<th>Created</th>
						<th>Modified</th>
						<th>Actions</th>
					</tr>
				</tfoot>
			
				<tbody>
					@foreach ($session_types as $type)
					<tr>
						<td>{{ $type->id }}</td>
						<td>{{ $type->title }}</td>
						<td>{{ $type->description}}</td>
						<td style="background: {{ $type->color}};"></td>
						<td>@if($type->include_in_cpd)Y @else N @endif</td>
						<td>@if($type->include_in_feedback)Y @else N @endif</td>
						<td>{{ date("d/m/Y H:i:s",strtotime($type->created_at)) }}</td>
						<td>{{ date("d/m/Y H:i:s",strtotime($type->updated_at)) }}</td>
						<td>
							<a href="{{ route('settings.sessionTypes.edit', $type->id) }}"><button type="button" class="btn btn-warning btn-xs" title="Edit"><span class="glyphicon glyphicon-pencil"></span></button></a>
							<button data-href="{{ route('settings.sessionTypes.delete', $type->id) }}" type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-trash"></span></button>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
</section>

@include('admin.partials.deleteConfirmation')

@endsection
