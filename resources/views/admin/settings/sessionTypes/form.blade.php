<script type="text/javascript">
	$(function(){
        $('#color').colorpicker();
    });
</script>

<div class="form-group floating-label">
	{!! Form::text('title', null, ['class'=>'form-control', 'required']) !!}
	{!! Form::label('title', 'Session type name') !!}
</div>
<div class="form-group floating-label">
	{!! Form::text('description', null, ['class'=>'form-control']) !!}
	{!! Form::label('description', 'Description') !!}
</div>
<div class="form-group">
	{!! Form::text('color', null, ['class'=>'form-control', 'id' => 'color']) !!}
	{!! Form::label('color', 'Color') !!}
</div>
<div class="form-group">
	<div class="checkbox checkbox-styled">
		<label>
			{!! Form::checkbox('include_in_cpd', true) !!}
		</label>
		{!! Form::label('include_in_cpd', 'Include hours in CPD', ['class' => 'control-label']) !!}
	</div>
</div>
<div class="form-group">
    <div class="checkbox checkbox-styled">
        <label>
            {!! Form::checkbox('include_in_feedback', true) !!}
        </label>
        {!! Form::label('include_in_feedback', 'Include generating feedback', ['class' => 'control-label']) !!}
    </div>
</div>