@extends('admin.layouts.default')

@section('title')
	About
@endsection

@section('content')

<section>

<div class="section-header">
	<ol class="breadcrumb">
		<li class="active">About</li>
	</ol>
</div>

<div class="section-body">	


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin elementum augue quis tellus auctor blandit. Phasellus imperdiet magna non luctus viverra. Phasellus dapibus, neque id posuere bibendum, massa nulla aliquam metus, at varius sapien eros eu metus. Quisque vel gravida ex. Fusce sapien nisl, elementum in augue ac, pharetra consectetur odio. Vestibulum vel lorem ac massa posuere congue vel ut ante. Fusce sapien diam, sagittis vitae aliquet a, varius vitae lacus.

Cras sodales purus massa, nec posuere ligula suscipit id. Curabitur faucibus pharetra nisi et interdum. Aliquam laoreet, mauris id laoreet pretium, ante odio convallis nunc, non tincidunt nibh justo id erat. Integer eget accumsan magna. Maecenas aliquet orci placerat, vehicula turpis quis, imperdiet ligula. Vivamus malesuada arcu varius odio placerat interdum. Nulla rutrum, velit ac mollis aliquam, augue massa hendrerit lorem, commodo vestibulum purus tellus ut sem.

Nunc sed maximus diam. Sed venenatis erat eu leo gravida, euismod maximus mi molestie. Aenean pulvinar scelerisque est nec efficitur. Aenean ut metus vestibulum, lacinia odio a, tincidunt dolor. Nam non lectus tempus, sodales diam vitae, porta mauris. Nunc euismod aliquet sollicitudin. Phasellus sollicitudin ipsum consequat ultrices condimentum.

Maecenas at facilisis nisi, sed malesuada mauris. Morbi gravida lobortis urna. Nullam in mi magna. Praesent velit nunc, sollicitudin sit amet sagittis eu, fringilla vel orci. Morbi odio dui, dapibus nec faucibus id, dapibus ac mi. Sed elementum eu dui ac faucibus. Suspendisse et consequat justo. Maecenas pellentesque semper justo, non porta est vestibulum tristique. Etiam laoreet magna quam, sed venenatis diam pellentesque euismod. Sed neque augue, tincidunt a tellus non, auctor efficitur mauris. Aliquam eu lectus euismod, aliquam odio ac, ullamcorper sapien. Donec scelerisque maximus justo, ac semper ex tincidunt at. Pellentesque sed nisi nisi. Curabitur dui mauris, pellentesque non finibus in, fringilla non nibh. Duis lacinia viverra lectus vitae iaculis.

Phasellus ut ultrices libero. Curabitur pharetra aliquet viverra. Etiam laoreet erat suscipit, scelerisque erat eu, fermentum nulla. Vivamus nec dictum velit. Praesent commodo erat id velit feugiat posuere. Integer bibendum ex et magna tempor rhoncus. Curabitur ipsum nunc, tristique vitae vulputate sit amet, tincidunt id nisl. Fusce sodales purus condimentum varius sagittis. Curabitur id ante diam. In et nibh at augue tempus dictum eget non elit. Pellentesque placerat risus diam, consequat cursus elit iaculis eu. Etiam faucibus ultricies metus, et fermentum mauris lobortis quis. Nam id urna ac odio egestas ullamcorper. Nam vel lacus vulputate, lobortis lectus at, porta purus. 	
</div>
</section>

@endsection
