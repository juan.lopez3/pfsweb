@extends('admin.layouts.default')

@section('title')
	Attendee Types List
@endsection

@section('content')

<script type="text/javascript">
	$(document).ready(function() {
		$('#attendeeTypes').dataTable({
			"iDisplayLength": 50,
		});
	} );
</script>

<section>
<div class="section-header">
	<ol class="breadcrumb">
		<li class="active">Attendee Types List</li>
	</ol>
</div>
<div class="section-body">
	<p><button data-href="#" type="button" class="btn ink-reaction btn-raised btn-success" data-toggle="modal" data-target="#add_attendee_type">ADD Attendee TYPE</button></p>
	<div class="card">
		<div class="card-body">
			<table id="attendeeTypes" class="table table-striped table-responsive" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Name</th>
						<th>Created</th>
						<th>Modified</th>
						<th>Actions</th>
					</tr>
				</thead>
			
				<tfoot>
					<tr>
						<th>Name</th>
						<th>Created</th>
						<th>Modified</th>
						<th>Actions</th>
					</tr>
				</tfoot>
			
				<tbody>
					@foreach ($attendee_type as $t)
					<tr>
						<td>{{ $t->title }}</td>
						<td>{{ date("d/m/Y H:i:s",strtotime($t->created_at)) }}</td>
						<td>{{ date("d/m/Y H:i:s",strtotime($t->updated_at)) }}</td>
						<td>
							<a href="{{ route('settings.attendeeTypes.edit', $t->id) }}"><button type="button" class="btn btn-warning btn-xs" title="Edit"><span class="glyphicon glyphicon-pencil"></span></button></a>
							<button data-href="{{ route('settings.attendeeTypes.delete', $t->id) }}" type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-trash"></span></button>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
</section>

@include('admin.partials.deleteConfirmation')
<div class="modal fade" id="add_attendee_type" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		@include('admin.settings.attendeeTypes.create')
	</div>
</div>
@endsection
