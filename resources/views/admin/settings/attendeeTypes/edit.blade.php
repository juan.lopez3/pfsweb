@extends('admin.layouts.default')

@section('title')
Edit Attendee Type
@endsection

@section('content')

<section>
<div class="section-body">
	<div class="col-lg-offset-2 col-md-7">
		<div class="card">
			<div class="card-head style-primary">
				<header>Edit Attendee Type</header>
			</div>
			
			@include('admin.partials.validationErrors')
			
			{!! Form::model($attendee_type, ['route' => ['settings.attendeeTypes.update', $attendee_type->id], 'method' => 'PATCH', 'class' => 'form form-validate']) !!}
			<div class="card-body">
				 @include('admin.settings.attendeeTypes.form')
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					<a href="{{ URL::previous() }}"><button type="button" class="btn btn-flat">Cancel</button></a>
					{!! Form::submit('UPDATE Attendee Type', ['class' => 'btn btn-flat btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
</section>
@endsection
