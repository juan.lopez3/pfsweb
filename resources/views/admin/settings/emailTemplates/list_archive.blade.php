@extends('admin.layouts.default')

@section('title')
    Email Templates List
@endsection

@section('content')

<script type="text/javascript">
    $(document).ready(function() {
        $('#email_templates').dataTable({
            "iDisplayLength": 50,
        });
    } );
</script>

<section>
<div class="section-header">
    <ol class="breadcrumb">
        <li class="active">Email Templates List</li>
    </ol>
</div>
<div class="section-body">  
    <p><a href="{{ route('settings.emailTemplates.create') }}"><button class="btn ink-reaction btn-raised btn-success" type="button">ADD EMAIL TEMPLATE</button></a></p>
    <div class="card">
        <div class="card-body">
            <table id="email_templates" class="table table-striped table-responsive" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Email Type</th>
                        <th>Subject</th>
                        <th>Created</th>
                        <th>Modified</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            
                <tfoot>
                    <tr>
                        <th>Email Type</th>
                        <th>Subject</th>
                        <th>Created</th>
                        <th>Modified</th>
                        <th>Actions</th>
                    </tr>
                </tfoot>
            
                <tbody>
                    @foreach ($email_templates as $t)
                    <tr>
                        <td>
                            
                            @if($t->id <= 16)
                                <span title="System template" class="glyphicon glyphicon-exclamation-sign text-info"></span>
                            @else
                                <span title="Custom template" class="glyphicon glyphicon-wrench text-primary"></span>
                            @endif
                            {{ $t->type }}
                        </td>
                        <td>{{$t->subject}}</td>
                        <td>{{ date("d/m/Y H:i:s",strtotime($t->created_at)) }}</td>
                        <td>{{ date("d/m/Y H:i:s",strtotime($t->updated_at)) }}</td>
                        <td>
                            <a href="{{ route('settings.emailTemplates.unarchive', $t->id) }}" class="btn btn-success btn-xs" title="Archive"><span class="glyphicon glyphicon-floppy-open"></span></button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</section>
@endsection
