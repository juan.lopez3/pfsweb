@extends('admin.layouts.default')

@section('title')
Edit Email Template
@endsection

@section('content')

<section>
<div class="section-body">
	<div class="col-lg-offset-1 col-lg-10">
		<div class="card">
			<div class="card-head style-primary">
				<header>Edit Email Template</header>
			</div>
			
			@include('admin.partials.validationErrors')
			
			{!! Form::model($email_template, ['route' => ['settings.emailTemplates.update', $email_template->id], 'method' => 'PATCH', 'class' => 'form form-validate']) !!}
			<div class="card-body">
				 @include('admin.settings.emailTemplates.form')
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					<a href="{{ URL::previous() }}"><button type="button" class="btn btn-flat">Cancel</button></a>
					{!! Form::submit('UPDATE Email Template', ['class' => 'btn btn-flat btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
</section>
@endsection
