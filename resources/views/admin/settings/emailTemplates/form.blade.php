<script src="{{asset('assets/admin/js/libs/ckeditor/adapters/jquery.js')}}"></script>
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/summernote/summernote.css')}}" />
<script src="{{asset('assets/admin/js/libs/summernote/summernote.min.js')}}"></script>

<script type="text/javascript">
	$( '#ckeditor' ).ckeditor();
</script>

<div class="form-group floating-label">
	@if(isset($template_id) && $template_id <= 12)
		{!! Form::text('type', null, ['class'=>'form-control', 'readonly']) !!}
	@else
		{!! Form::text('type', null, ['class'=>'form-control']) !!}
	@endif
	
	{!! Form::label('type', 'Email Type') !!}
</div>

<div class="form-group floating-label">
	{!! Form::text('subject', null, ['class'=>'form-control']) !!}
	{!! Form::label('subject', 'Subject') !!}
</div>

<div class="form-group floating-label">
	{!! Form::input('number', 'reminder', null, ['class'=>'form-control', 'min'=>0, 'max'=>99]) !!}
	{!! Form::label('number', 'Reminder before days') !!}
</div>

@include('admin.partials.availableTags')
<div class="form-group floating-label">
	{!! Form::label('Template', 'Template') !!}<br/>
	{!! Form::textarea('template', null, ['id' => 'ckeditor']) !!}
</div>
