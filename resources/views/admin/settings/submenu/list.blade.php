@extends('admin.layouts.default')

@section('title')
	Submenu Manager
@endsection

@section('content')

<script type="text/javascript">
	$(document).ready(function() {
		$('#list').dataTable({
			"iDisplayLength": 50,
		});
	} );
</script>

<section>
<div class="section-header">
	<ol class="breadcrumb">
		<li class="active">Submenu Manager</li>
	</ol>
</div>
<div class="section-body">	
	<p><a href="{{ route('settings.submenu.create') }}"><button class="btn ink-reaction btn-raised btn-success" type="button">Add New</button></a></p>
	<div class="card">
		<div class="card-body">
			<table id="list" class="table table-striped table-responsive" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Title</th>
						<th>URL</th>
						<th>Actions</th>
						<th>Created At</th>
						<th>Updated At</th>
					</tr>
				</thead>
			
				<tbody>
					@foreach ($submenu as $item)
					<tr>
						<td>{{ $item->title }}</td>
						<td>{{ $item->url}}</td>
						<td>{{ date("d/m/Y H:i:s",strtotime($item->created_at)) }}</td>
						<td>{{ date("d/m/Y H:i:s",strtotime($item->updated_at)) }}</td>
						<td>
							<a href="{{ route('settings.submenu.edit', $item->id) }}"><button type="button" class="btn btn-warning btn-xs" title="Edit"><span class="glyphicon glyphicon-pencil"></span></button></a>
							<button data-href="{{ route('settings.submenu.delete', $item->id) }}" type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-trash"></span></button>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
</section>

@include('admin.partials.deleteConfirmation')

@endsection
