<div class="form-group floating-label">
	{!! Form::text('title', null, ['class'=>'form-control', 'required']) !!}
	{!! Form::label('title', 'Title') !!}
</div>

<div class="form-group floating-label">
    {!! Form::text('url', null, ['class'=>'form-control', 'required', 'id'=>'url']) !!}
    {!! Form::label('url', 'URL') !!}
</div>

<script type="text/javascript">
    $('#url').change(function(){
        var prefix = 'http://';
        var s = $("#url").val();
        if (s.substr(0, prefix.length) !== prefix)
        {
            s = prefix + s;
            $("#url").val(s);
        }
    });
</script>