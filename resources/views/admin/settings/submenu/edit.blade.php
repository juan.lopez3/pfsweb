@extends('admin.layouts.default')

@section('title')
Edit Submenu
@endsection

@section('content')

<section>
<div class="section-body">
	<div class="col-lg-offset-2 col-md-7">
		<div class="card">
			<div class="card-head style-primary">
				<header>Edit Submenu</header>
			</div>
			
			@include('admin.partials.validationErrors')
			
			{!! Form::model($submenu, ['route' => ['settings.submenu.update', $submenu->id], 'method' => 'PUT', 'class' => 'form form-validate']) !!}
			<div class="card-body">
				 @include('admin.settings.submenu.form')
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					<a href="{{ URL::previous() }}"><button type="button" class="btn btn-flat">Cancel</button></a>
					{!! Form::submit('UPDATE', ['class' => 'btn btn-flat btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
</section>
@endsection
