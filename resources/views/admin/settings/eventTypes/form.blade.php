<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/js/libs/jquery-fileupload/uploadfile.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/multi-select/multi-select.css')}}" />

<script type="text/javascript" src="{{asset('assets/admin/js/libs/jquery-fileupload/jquery.uploadfile.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/libs/multi-select/jquery.multi-select.js')}}"></script>

<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/summernote/summernote.css')}}" />
<script src="{{asset('assets/admin/js/libs/summernote/summernote.min.js')}}"></script>

<script type="text/javascript">
    $(function(){
        $('#color').colorpicker();
    });
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$("#summernote1").summernote();
		$("#summernote2").summernote();
		$("#summernote3").summernote();
	})
	
function removeImage(field) {
    
    $("#"+field).val('');
    $("#"+field+"_p").html('');
}
</script>

<div class="checkbox checkbox-styled">
    <label>
        {!! Form::checkbox('exhibition', 1, 1) !!}
        Exhibition
    </label>
</div>

<div class="form-group floating-label">
	{!! Form::text('title', null, ['class'=>'form-control', 'required']) !!}
	{!! Form::label('title', 'Event type name') !!}
</div>
<div class="form-group">
	{!! Form::select('email_address_id', [''=>'Please select email address']+$email_addresses, null, ['class'=>'form-control', 'required']) !!}
	{!! Form::label('email_address_id', 'Email From Address') !!}
</div>
<div class="form-group">
    {!! Form::select('feedback_type', ['traditional'=>'Traditional (old)', 'split' => 'Split (new)'], null, ['class'=>'form-control', 'required']) !!}
    {!! Form::label('feedback_type', 'Feedback survey type') !!}
</div>
<div class="form-group floating-label">
	{!! Form::label('terms_conditions', 'Terms and conditions') !!}<br/>
	{!! Form::textarea('terms_conditions', null, ['row'=>'3', 'style'=>'height: 200px;', 'id'=>'summernote']) !!}
</div>
<div class="form-group floating-label">
	{!! Form::label('amendment_text', 'Amendment text') !!}<br/>
	{!! Form::textarea('amendment_text', null, ['row'=>'3', 'style'=>'height: 200px;', 'id'=>'summernote1']) !!}
</div>
<div class="form-group floating-label">
	{!! Form::label('cancel_text', 'Non-attendance text') !!}<br/>
	{!! Form::textarea('cancel_text', null, ['row'=>'3', 'style'=>'height: 200px;', 'id'=>'summernote2']) !!}
</div>
<div class="form-group floating-label">
	{!! Form::label('type_template', 'Assigned templates') !!}
	<br/>
	<select id="optgroup" name="type_template[]" multiple="multiple">
		@foreach($email_templates as $key=>$et)
			<?php $selected="";  if(isset($event_type)){$selected = in_array($key, $event_type->emailTemplates->lists('id')) ? "selected" : "";} ?>
			<option {{$selected}} value="{{$key}}">{{$et}}</option>
		@endforeach
	</select>
</div>
<div class="row">
    <div class="form-group floating-label col-xs-12 col-sm-3">
    	{!! Form::label('branding_image', 'Branding image') !!}
    	{!! Form::hidden('branding_image', null, ['id'=>'branding_image']) !!}
    	<br/>
    	<span id="branding_image_p">
    	@if(!empty($event_type->branding_image))
    		{!! HTML::image('uploads/branding/'.$event_type->branding_image,'', ['class'=>'img-responsive']) !!}
    	@endif
    	</span>
    	<div id="logo_upload">Upload</div>
    	<div id="status"></div>
    </div>
</div>

<div class="row">
    <div class="form-group floating-label col-xs-12 col-sm-3">
        {!! Form::label('email_branding', 'Email branding image') !!}
        {!! Form::hidden('email_branding', null, ['id'=>'email_branding']) !!}
        <br/>
        <span id="email_branding_p">
        @if(!empty($event_type->email_branding))
            {!! HTML::image('uploads/branding/'.$event_type->email_branding,'', ['class'=>'img-responsive']) !!}
        @endif
        </span>
        <div id="email_branding_upload">Upload</div>
        <div id="status_1"></div>
    </div>
</div>

<div class="row">
    <div class="form-group floating-label col-xs-12 col-sm-3">
        {!! Form::label('agenda_branding', 'Agenda branding image') !!}
        {!! Form::hidden('agenda_branding', null, ['id'=>'agenda_branding']) !!}
        <br/>
        <span id="agenda_branding_p">
        @if(!empty($event_type->agenda_branding))
            {!! HTML::image('uploads/branding/'.$event_type->agenda_branding,'', ['class'=>'img-responsive']) !!}
        @endif
        </span>
        <div id="agenda_branding_upload">Upload</div>
        <div id="status_1"></div>
    </div>
</div>

<hr/>
<h2 class="text-primary">Mobile API configuration</h2>

<div class="row">
    <div class="checkbox checkbox-styled">
        <label>
            {!! Form::checkbox('app_sponsors_active', 1, 1) !!}
            Show app sponsor advert
        </label>
    </div>
    
    <div class="form-group">
        {!! Form::text('app_color', null, ['class'=>'form-control', 'id' => 'color']) !!}
        {!! Form::label('app_color', 'Event type color') !!}
    </div>
        
    <div class="form-group floating-label">
        {!! Form::label('app_description', 'Short topic description') !!}
        {!! Form::textarea('app_description', null, ['class'=>'form-control', 'rows'=>2]) !!}
        
    </div>
    
    <div class="form-group floating-label col-xs-12 col-md-4">
        {!! Form::label('app_branding', 'Mobile Application branding image') !!}
        {!! Form::hidden('app_branding', null, ['id'=>'app_branding']) !!}
        <br/>
        <span id="app_branding_p">
        @if(!empty($event_type->app_branding))
            <button type="button" onclick="removeImage('app_branding');" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
            {!! HTML::image('uploads/app/'.$event_type->app_branding,'', ['class'=>'img-responsive']) !!}
        @endif
        </span>
        <div id="app_branding_upload">Upload</div>
        <div id="status_2"></div>
    </div>
    
    
    <div class="form-group floating-label col-xs-12 col-md-4">
        {!! Form::label('app_icon', 'Mobile Application ICON') !!}
        {!! Form::hidden('app_icon', null, ['id'=>'app_icon']) !!}
        <br/>
        <span id="app_icon_p">
        @if(!empty($event_type->app_icon))
        <button type="button" onclick="removeImage('app_icon');" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
            {!! HTML::image('uploads/app/'.$event_type->app_icon,'', ['class'=>'img-responsive']) !!}
        @endif
        </span>
        <div id="app_icon_upload">Upload</div>
        <div id="status_3"></div>
    </div>
    
    <div class="form-group floating-label col-xs-12 col-md-4">
        {!! Form::label('app_promotional_banner', 'Mobile Application promotional banner') !!}
        {!! Form::hidden('app_promotional_banner', null, ['id'=>'app_promotional_banner']) !!}
        <br/>
        <span id="app_promotional_banner_p">
        @if(!empty($event_type->app_promotional_banner))
        <button type="button" onclick="removeImage('app_promotional_banner');" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
            {!! HTML::image('uploads/app/'.$event_type->app_promotional_banner,'', ['class'=>'img-responsive']) !!}
        @endif
        </span>
        <div id="app_promotional_banner_upload">Upload</div>
        <div id="status_4"></div>
    </div>
</div>

<h2 class="text-primary">Additional App Section</h2>
<div class="row">
    
    <div class="col-md-3">
        <div class="form-group">
            <div class="checkbox checkbox-styled">
                <label>
                    {!! Form::checkbox('app_additional_active', 1, 1) !!}
                    Additional Section Active
                </label>
            </div>
        </div>
    </div>
    
    <div class="col-md-9">
        <div class="form-group">
            {!! Form::text('app_additional_title', null, ['class'=>'form-control']) !!}
            {!! Form::label('app_additional_title', 'Title') !!}
        </div>
    </div>
    
    <div class="col-md-12">
        {!! Form::label('app_additional_content', 'Content') !!}<br/>
        {!! Form::textarea('app_additional_content', null, ['row'=>'3', 'style'=>'height: 200px;', 'id'=>'summernote3']) !!}
    </div>
    
</div>

<script type="text/javascript">
$(document).ready(function(){
    
    //event branding
	var settings = {
	    url: "{!! route('settings.eventTypes.uploadBranding') !!}",
	    formData: {  
		   "_token": "{{ csrf_token() }}"
		},
	    dragDrop:true,
	    maxFileSize: 1000000,
	    fileName: "myfile",
	    allowedTypes:"jpg,png,gif,bmp,jpeg",	
	    returnType:"json",
		 onSuccess:function(files,data,xhr)
	    {
	       //alert((data));
	       $('#branding_image').val(data);
	       $('#branding_image_p').html('<img src="{{\Config::get("app.url")}}uploads/branding/'+data+'" class="img-responsive" />');
	       
	       //location.reload();
	    },
	    showDelete:false
	}
	var uploadObj = $("#logo_upload").uploadFile(settings);
    
    // email branding
    var settings_email = {
        url: "{!! route('settings.eventTypes.uploadEmailBranding') !!}",
        formData: {  
           "_token": "{{ csrf_token() }}"
        },
        dragDrop:true,
        maxFileSize: 1000000,
        fileName: "myfile",
        allowedTypes:"jpg,png,gif,bmp,jpeg",    
        returnType:"json",
         onSuccess:function(files,data,xhr)
        {
           //alert((data));
           $('#email_branding').val(data);
           $('#email_branding_p').html('<img src="{{\Config::get("app.url")}}uploads/branding/'+data+'" class="img-responsive" />');
           
           //location.reload();
        },
        showDelete:false
    }
    var uploadObj_email = $("#email_branding_upload").uploadFile(settings_email);
    
    // agenda branding
    var settings_agenda = {
        url: "{!! route('settings.eventTypes.uploadEmailBranding') !!}",
        formData: {  
           "_token": "{{ csrf_token() }}"
        },
        dragDrop:true,
        maxFileSize: 1000000,
        fileName: "myfile",
        allowedTypes:"jpg,png,gif,bmp,jpeg",    
        returnType:"json",
         onSuccess:function(files,data,xhr)
        {
           //alert((data));
           $('#agenda_branding').val(data);
           $('#agenda_branding_p').html('<img src="{{\Config::get("app.url")}}uploads/branding/'+data+'" class="img-responsive" />');
           
           //location.reload();
        },
        showDelete:false
    }
    var uploadObj_agenda = $("#agenda_branding_upload").uploadFile(settings_agenda);
    
    // mobile app branding
    var settings_app = {
        url: "{!! route('settings.eventTypes.uploadAppBranding') !!}",
        formData: {  
           "_token": "{{ csrf_token() }}"
        },
        dragDrop:true,
        maxFileSize: 1000000,
        fileName: "myfile",
        allowedTypes:"jpg,png,gif,bmp,jpeg",    
        returnType:"json",
         onSuccess:function(files,data,xhr)
        {
           //alert((data));
           $('#app_branding').val(data);
           $('#app_branding_p').html('<img src="{{\Config::get("app.url")}}uploads/app/'+data+'" class="img-responsive" />');
           
           //location.reload();
        },
        showDelete:false
    }
    var uploadObj_email = $("#app_branding_upload").uploadFile(settings_app);
    
    // mobile app icon
    var settings_app_icon = {
        url: "{!! route('settings.eventTypes.uploadAppBranding') !!}",
        formData: {  
           "_token": "{{ csrf_token() }}"
        },
        dragDrop:true,
        maxFileSize: 1000000,
        fileName: "myfile",
        allowedTypes:"jpg,png,gif,bmp,jpeg",    
        returnType:"json",
         onSuccess:function(files,data,xhr)
        {
           //alert((data));
           $('#app_icon').val(data);
           $('#app_icon_p').html('<img src="{{\Config::get("app.url")}}uploads/app/'+data+'" class="img-responsive" />');
           
           //location.reload();
        },
        showDelete:false
    }
    var uploadObj_email = $("#app_icon_upload").uploadFile(settings_app_icon);
    
    // mobile app branding
    var settings_app_promotional_banner = {
        url: "{!! route('settings.eventTypes.uploadAppBranding') !!}",
        formData: {  
           "_token": "{{ csrf_token() }}"
        },
        dragDrop:true,
        maxFileSize: 1000000,
        fileName: "myfile",
        allowedTypes:"jpg,png,gif,bmp,jpeg",    
        returnType:"json",
         onSuccess:function(files,data,xhr)
        {
           //alert((data));
           $('#app_promotional_banner').val(data);
           $('#app_promotional_banner_p').html('<img src="{{\Config::get("app.url")}}uploads/app/'+data+'" class="img-responsive" />');
           
           //location.reload();
        },
        showDelete:false
    }
    var uploadObj_email = $("#app_promotional_banner_upload").uploadFile(settings_app_promotional_banner);
});
</script>