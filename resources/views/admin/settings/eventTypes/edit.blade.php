@extends('admin.layouts.default')

@section('title')
Edit Event Type
@endsection

@section('content')

<section>
<div class="section-body">
	<div class="col-lg-offset-1 col-lg-10 col-md-12">
		<div class="card">
			<div class="card-head style-primary">
				<header>Edit Event Type</header>
			</div>
			
			@include('admin.partials.validationErrors')
			
			{!! Form::model($event_type, ['route' => ['settings.eventTypes.update', $event_type->id], 'method' => 'PATCH', 'class' => 'form form-validate']) !!}
			<div class="card-body">
				 @include('admin.settings.eventTypes.form')
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					<a href="{{ URL::previous() }}"><button type="button" class="btn btn-flat">Cancel</button></a>
					{!! Form::submit('UPDATE EVENT TYPE', ['class' => 'btn btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
			
			<div class="col-xs-12">
                <h2 class="text-primary">Event type files</h2>
                
                <div class="row">
                    {!! Form::open(['route'=> ['settings.eventTypes.attachFile', $event_type->id], 'class'=>'form']) !!}
                    <div class="col-sm-3">Attach file from central files:</div>
                    <div class="col-sm-8">{!! Form::select('file_id', [''=>'Select file to assign']+$files, null, ['class'=>'form-control select2-list', 'required']) !!}</div>
                    <div class="col-sm-1">{!! Form::submit('+', ['class'=>'btn btn-sm btn-success']) !!}</div>
                    {!! Form::close() !!}
                </div>
                
                
                <table id="documents_list" class="table table-striped table-responsive" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Document Name</th>
                            <th>Display Name</th>
                            <th>Type</th>
                            <th>Size(Mb)</th>
                            <th>Date Uploaded</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                
                    <tbody>
                        @foreach ($event_type->files as $document)
                        <tr>
                            <td>{{ $document->document_name }}</td>
                            <td>{{ $document->display_name }}</td>
                            <td>{{ $document->type}}</td>
                            <td>{{ $document->size / 1000000 }}</td>
                            <td>{{ date("d m Y H:i:s",strtotime($document->created_at)) }}</td>
                            <td>
                                @if(hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
                                    <button data-href="{{ route('settings.eventTypes.detachFile', [$event_type->id, $document->id]) }}" type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-trash"></span></button>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        
                    </tbody>
                </table>
                
            </div>
            
		</div>
	</div>
</div>
</section>
@include('admin.partials.deleteConfirmation')
@endsection
