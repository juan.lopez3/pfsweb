@extends('admin.layouts.default')

@section('title')
	Event Types
@endsection

@section('content')

<script type="text/javascript">
	$(document).ready(function() {
		$('#event_types').dataTable({
			"iDisplayLength": 50,
		});
	} );
</script>

<section>
<div class="section-header">
	<ol class="breadcrumb">
		<li class="active">Event Types</li>
	</ol>
</div>
<div class="section-body">	
	@include('admin.partials.validationErrors')
	<p><button data-href="#" type="button" class="btn ink-reaction btn-raised btn-success" data-toggle="modal" data-target="#add_event_type">ADD EVENT TYPE</button></p>
	<div class="card">
		<div class="card-body">
			<table id="event_types" class="table table-striped table-responsive" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Event Type Name</th>
						<th>Created</th>
						<th>Modified</th>
						<th>Actions</th>
					</tr>
				</thead>
			
				<tfoot>
					<tr>
						<th>Event Type Name</th>
						<th>Created</th>
						<th>Modified</th>
						<th>Actions</th>
					</tr>
				</tfoot>
			
				<tbody>
					@foreach ($event_types as $type)
					<tr>
						<td>{{ $type->title }}</td>
						<td>{{ date("d/m/Y H:i:s",strtotime($type->created_at)) }}</td>
						<td>{{ date("d/m/Y H:i:s",strtotime($type->updated_at)) }}</td>
						<td>
							<a href="{{ route('settings.eventTypes.edit', $type->id) }}"><button type="button" class="btn btn-warning btn-xs" title="Edit"><span class="glyphicon glyphicon-pencil"></span></button></a>
							<button data-href="{{ route('settings.eventTypes.delete', $type->id) }}" type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-trash"></span></button>
							<a href="{{ route('settings.eventTypeSteps', $type->id) }}"><button type="button" class="btn btn-success btn-xs" title="Edit Steps">Edit Steps</button></a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
</section>

@include('admin.partials.deleteConfirmation')

<div class="modal fade" id="add_event_type" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		@include('admin.settings.eventTypes.create')
	</div>
</div>
@endsection
