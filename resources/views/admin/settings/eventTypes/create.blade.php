<div class="section-body">
	<div class="col-md-12">
		<div class="card">
			<div class="card-head style-primary">
				<header>Add Event Type</header>
			</div>
			
			{!! Form::open(['route' => ['settings.eventTypes.store'], 'class' => 'form form-validate']) !!}
			<div class="card-body">
				 @include('admin.settings.eventTypes.form')
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					{!! Form::submit('CREATE EVENT TYPE', ['class' => 'btn btn-flat btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>