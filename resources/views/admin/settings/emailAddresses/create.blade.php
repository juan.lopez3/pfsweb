<div class="section-body">
	<div class="col-md-12">
		<div class="card">
			<div class="card-head style-primary">
				<header>Add Email Address</header>
			</div>
			
			@include('admin.partials.validationErrors')
			
			{!! Form::open(['route' => ['settings.emailAddresses.store'], 'class' => 'form form-validate']) !!}
			<div class="card-body">
				 @include('admin.settings.emailAddresses.form')
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					{!! Form::submit('CREATE Email Address', ['class' => 'btn btn-flat btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>