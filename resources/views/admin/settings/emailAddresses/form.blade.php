<div class="form-group floating-label">
	{!! Form::text('email', null, ['class'=>'form-control', 'required']) !!}
	{!! Form::label('email', 'Email Address') !!}
</div>