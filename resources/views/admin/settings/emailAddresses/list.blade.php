@extends('admin.layouts.default')

@section('title')
	Email Addresses List
@endsection

@section('content')

<script type="text/javascript">
	$(document).ready(function() {
		$('#email_addresses').dataTable({
			"iDisplayLength": 50,
		});
	} );
</script>

<section>
<div class="section-header">
	<ol class="breadcrumb">
		<li class="active">Email Addresses List</li>
	</ol>
</div>
<div class="section-body">	
	@include('admin.partials.validationErrors')
	
	<p><button data-href="#" type="button" class="btn ink-reaction btn-raised btn-success" data-toggle="modal" data-target="#add_email_address">ADD Email Address</button></p>
	<div class="card">
		<div class="card-body">
			<table id="email_addresses" class="table table-striped table-responsive" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Email Address</th>
						<th>Created</th>
						<th>Modified</th>
						<th>Actions</th>
					</tr>
				</thead>
			
				<tfoot>
					<tr>
						<th>Email Address</th>
						<th>Created</th>
						<th>Modified</th>
						<th>Actions</th>
					</tr>
				</tfoot>
			
				<tbody>
					@foreach ($email_addresses as $t)
					<tr>
						<td>{{ $t->email }}</td>
						<td>{{ date("d/m/Y H:i:s",strtotime($t->created_at)) }}</td>
						<td>{{ date("d/m/Y H:i:s",strtotime($t->updated_at)) }}</td>
						<td>
							<a href="{{ route('settings.emailAddresses.edit', $t->id) }}"><button type="button" class="btn btn-warning btn-xs" title="Edit"><span class="glyphicon glyphicon-pencil"></span></button></a>
							<button data-href="{{ route('settings.emailAddresses.delete', $t->id) }}" type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-trash"></span></button>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
</section>

@include('admin.partials.deleteConfirmation')
<div class="modal fade" id="add_email_address" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		@include('admin.settings.emailAddresses.create')
	</div>
</div>
@endsection
