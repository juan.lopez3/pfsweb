<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/js/libs/jquery-fileupload/uploadfile.css')}}" />
<script type="text/javascript" src="{{asset('assets/admin/js/libs/jquery-fileupload/jquery.uploadfile.min.js')}}"></script>

<div class="row">
    <div class="col-md-12">
        <div class="form-group floating-label">
        	{!! Form::text('title', null, ['class'=>'form-control', 'required']) !!}
        	{!! Form::label('title', 'Theme title') !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-3 col-md-4 col-sm-6">
        <div class="form-group floating-label">
            {!! Form::text('title_background_color', null, ['class'=>'form-control', 'id' => 'title_background_color']) !!}
            {!! Form::label('title_background_color', 'Title background colour') !!}
        </div>
    </div>
    
    <div class="col-lg-3 col-md-4 col-sm-6">
        <div class="form-group floating-label">
            {!! Form::text('title_text_color', null, ['class'=>'form-control', 'id' => 'title_text_color']) !!}
            {!! Form::label('title_text_color', 'Title text colour') !!}
        </div>
    </div>
    
    <div class="col-lg-3 col-md-4 col-sm-6">
        <div class="form-group floating-label">
            {!! Form::text('text_color', null, ['class'=>'form-control', 'id' => 'text_color']) !!}
            {!! Form::label('text_color', 'Text colour') !!}
        </div>
    </div>
    
    <div class="col-lg-3 col-md-4 col-sm-6">
        <div class="form-group floating-label">
            {!! Form::text('link_color', null, ['class'=>'form-control', 'id' => 'link_color']) !!}
            {!! Form::label('link_color', 'Link colour') !!}
        </div>
    </div>
    
    <div class="col-lg-3 col-md-4 col-sm-6">
        <div class="form-group floating-label">
            {!! Form::text('link_active_color', null, ['class'=>'form-control', 'id' => 'link_active_color']) !!}
            {!! Form::label('link_active_color', 'Link active colour') !!}
        </div>
    </div>
    
    <div class="col-lg-3 col-md-4 col-sm-6">
        <div class="form-group floating-label">
            {!! Form::text('link_hover_color', null, ['class'=>'form-control', 'id' => 'link_hover_color']) !!}
            {!! Form::label('link_hover_color', 'Link hover colour') !!}
        </div>
    </div>
    
    <div class="col-lg-3 col-md-4 col-sm-6">
        <div class="form-group floating-label">
            {!! Form::text('link_visited_color', null, ['class'=>'form-control', 'id' => 'link_visited_color']) !!}
            {!! Form::label('link_visited_color', 'Link visited colour') !!}
        </div>
    </div>
    
    <div class="col-lg-3 col-md-4 col-sm-6">
        <div class="form-group floating-label">
            {!! Form::text('book_now_hover_color', null, ['class'=>'form-control', 'id' => 'book_now_hover_color']) !!}
            {!! Form::label('book_now_hover_color', 'Book now hover colour') !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        {!! Form::label('twitter_image', 'Twitter image') !!}
        {!! Form::hidden('twitter_image', null, ['id'=>'twitter_image']) !!}
        <br/>
        <span id="twitter_image_p">
        @if(!empty($theme->twitter_image))
            {!! HTML::image('uploads/branding/'.$theme->twitter_image,'', ['class'=>'img-responsive']) !!}
        @endif
        </span>
        <div id="twitter_image_upload">Upload</div>
        <div id="status1"></div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        {!! Form::label('background_image', 'Background image') !!}
        {!! Form::hidden('background_image', null, ['id'=>'background_image']) !!}
        <br/>
        <span id="background_image_p">
        @if(!empty($theme->background_image))
            {!! HTML::image('uploads/branding/'.$theme->background_image,'', ['class'=>'img-responsive']) !!}
        @endif
        </span>
        <div id="bg_image_upload">Upload</div>
        <div id="status"></div>
    </div>
</div>


<script type="text/javascript">
    // background image upload
    var settings = {
        url: "{!! route('settings.themes.upload') !!}",
        formData: {  
           "_token": "{{ csrf_token() }}"
        },
        dragDrop:true,
        maxFileSize: 1500000,
        fileName: "myfile",
        allowedTypes:"jpg,png,gif,bmp,jpeg",    
        returnType:"json",
         onSuccess:function(files,data,xhr)
        {
           //alert((data));
           $('#background_image').val(data);
           $('#background_image_p').html('<img src="{{\Config::get("app.url")}}uploads/branding/'+data+'" class="img-responsive" />');
           
           //location.reload();
        },
        showDelete:false
    }
    var uploadObj = $("#bg_image_upload").uploadFile(settings);
    
    
    var twitter = {
        url: "{!! route('settings.themes.upload') !!}",
        formData: {  
           "_token": "{{ csrf_token() }}"
        },
        dragDrop:true,
        maxFileSize: 500000,
        fileName: "myfile",
        allowedTypes:"jpg,png,gif,bmp,jpeg",    
        returnType:"json",
         onSuccess:function(files,data,xhr)
        {
           //alert((data));
           
           $('#twitter_image').val(data);
           $('#twitter_image_p').html('<img src="{{\Config::get("app.url")}}uploads/branding/'+data+'" class="img-responsive" />');
           
           //location.reload();
        },
        showDelete:false
    }
    var uploadObj = $("#twitter_image_upload").uploadFile(twitter);

    $(function(){
        $('#title_background_color').colorpicker();
        $('#title_text_color').colorpicker();
        $('#text_color').colorpicker();
        $('#link_color').colorpicker();
        $('#link_active_color').colorpicker();
        $('#link_hover_color').colorpicker();
        $('#link_visited_color').colorpicker();
        $('#book_now_hover_color').colorpicker();
    });
</script>