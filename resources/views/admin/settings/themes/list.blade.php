@extends('admin.layouts.default')

@section('title')
	Themes List
@endsection

@section('content')

<script type="text/javascript">
	$(document).ready(function() {
		$('#themes').dataTable({
			"iDisplayLength": 50,
		});
	} );
</script>

<section>
<div class="section-header">
	<ol class="breadcrumb">
		<li class="active">Themes List</li>
	</ol>
</div>
<div class="section-body">	
	<p><a href="{{ route('settings.themes.create') }}"><button class="btn ink-reaction btn-raised btn-success" type="button">NEW THEME</button></a></p>
	<div class="card">
		<div class="card-body">
			<table id="regions" class="table table-striped table-responsive" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Theme</th>
						<th>Created</th>
						<th>Modified</th>
						<th>Actions</th>
					</tr>
				</thead>
			
				<tfoot>
					<tr>
						<th>Theme</th>
						<th>Created</th>
						<th>Modified</th>
						<th>Actions</th>
					</tr>
				</tfoot>
			
				<tbody>
					@foreach ($themes as $theme)
					<tr>
						<td>{{ $theme->title }}</td>
						<td>{{ date("d/m/Y H:i:s",strtotime($theme->created_at)) }}</td>
						<td>{{ date("d/m/Y H:i:s",strtotime($theme->updated_at)) }}</td>
						<td>
						    @if ($theme->id > 1)
							<a href="{{ route('settings.themes.edit', $theme->id) }}"><button type="button" class="btn btn-warning btn-xs" title="Edit"><span class="glyphicon glyphicon-pencil"></span></button></a>
							<button data-href="{{ route('settings.themes.delete', $theme->id) }}" type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-trash"></span></button>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
</section>

@include('admin.partials.deleteConfirmation')
@endsection
