<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/summernote/summernote.css')}}" />
<script src="{{asset('assets/admin/js/libs/summernote/summernote.min.js')}}"></script>

<div class="form-group floating-label">
    {!! Form::label('sign_up', 'Sign up text') !!}<br/>
    {!! Form::textarea('sign_up', null, ['row'=>'3', 'style'=>'height: 200px;', 'id'=>'summernote1']) !!}
</div>

<div class="form-group floating-label">
    {!! Form::label('forgot_pin', 'Forgot pin text') !!}<br/>
    {!! Form::textarea('forgot_pin', null, ['row'=>'3', 'style'=>'height: 200px;', 'id'=>'summernote2']) !!}
</div>

<div class="form-group floating-label">
    {!! Form::label('exchange_business_cards', 'How to exchange business cards text') !!}<br/>
    {!! Form::textarea('exchange_business_cards', null, ['row'=>'3', 'style'=>'height: 200px;', 'id'=>'summernote3']) !!}
</div>

<div class="form-group floating-label">
    {!! Form::label('no_connection', 'No internet connection text') !!}<br/>
    {!! Form::textarea('no_connection', null, ['row'=>'3', 'style'=>'height: 200px;', 'id'=>'summernote4']) !!}
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $("#summernote1").summernote();
        $("#summernote2").summernote();
        $("#summernote3").summernote();
        $("#summernote4").summernote();
    })
</script>