@extends('admin.layouts.default')

@section('title')
Generic app text
@endsection

@section('content')

<section>
<div class="section-body">
	<div class="col-md-12">
		<div class="card">
			<div class="card-head style-primary">
				<header>Generic app text</header>
			</div>
			
			@include('admin.partials.validationErrors')
			
			{!! Form::model($settings, ['route' => ['settings.apiText.update'], 'method' => 'PUT', 'class' => 'form form-validate']) !!}
			<div class="card-body">
				 @include('admin.settings.appText.form')
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					<a href="{{ URL::previous() }}"><button type="button" class="btn btn-flat">Cancel</button></a>
					{!! Form::submit('UPDATE settings', ['class' => 'btn btn-flat btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
</section>
@endsection
