@extends('admin.layouts.default')

@section('title')
Highlights Manager
@endsection

@section('content')
<section>
<div class="section-header">
	<ol class="breadcrumb">
		<li class="active">Highlights Manager</li>
	</ol>
</div>
{!! Form::open(['route' => ['settings.highlights.store'], 'class' => 'form form-validate']) !!}
<div class="section-body">
	<div class="row">
			<div class="col-xs-6 col-xs-offset-1">
			{!! Form::select('event_id', ['' => 'Please select event']+$events_select, null, ['class'=>'form-control select2-list', 'id'=>'events_id']) !!}
			</div>
			<div class="col-xs-1">
			{!! Form::button('+', ['class' => 'btn ink-reaction btn-raised btn-sm btn-success', 'id'=>'add_events', 'title'=>'Add selected event']) !!}
			</div>
	</div>

	<div class="col-xs-11 col-xs-offset-1">
		<div id="session_events">
			@if(isset($highlights))
				@foreach($highlights as $h)
					<div class="row">
						<div class="col-xs-6">
							<p style = "color: black; font-size:17px">{{$h->title}}</p>
						</div>
						<div class="col-xs-1">
							<button data-href="{{ route('settings.highlights.delete', [$h->id]) }}" type="button" class="btn btn-danger btn-icon-toggle" title="Remove Highlights" data-toggle="modal" data-target="#confirm-delete"><i class="md md-delete"></i></span></button>
						</div>
					</div>
				@endforeach
			@endif
		</div>
	</div>
<br/><br/>
</div>
		<div class="card-actionbar-row">
			<a href="{{ route('settings') }}"><button type="button" class="btn btn-flat">Cancel</button></a>
			{!! Form::submit('SAVE HIGHLIGHTS', ['class' => 'btn btn-flat btn-primary']) !!}
		</div>
{!! Form::close() !!}

@include('admin.partials.deleteConfirmation')

</section>

<script type="text/javascript">
$("#add_events").click(function () {
	
	var html = '<div class="row">'+
					'<div class="col-xs-6"><input type="hidden" name="events_id[]" value="'+$('#events_id').val()+'" /><input style = "color: black" type="text" name="name[]" value="'+$('#events_id option:selected').text()+'" readonly class="form-control" /></div>'+
					'<div class="col-xs-1"><button type="button" class="delete btn btn-danger btn-icon-toggle" title="Remove Highlights"><i class="md md-delete"></i></button></div>'+
				'</div>';
				
	$("#session_events").append(html);
});

	
$("body").on("click", ".delete", function (e) {
	$(this).parent("div").parent("div").remove();
});
</script>
@endsection
