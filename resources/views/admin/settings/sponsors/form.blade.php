<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/bootstrap-datepicker/datepicker3.css')}}" />
<script type="text/javascript" src="{{asset('assets/admin/js/libs/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
<div class="form-group floating-label">
	{!! Form::text('title', null, ['class'=>'form-control', 'required']) !!}
	{!! Form::label('title', 'Quarter name') !!}
</div>
<div class="form-group">
	<div class="input-daterange input-group" id="demo-date-range">
		{!! Form::label('date_to', 'Active Dates', ['class' => 'control-label']) !!}
			
		@if(empty($sponsor_settings))
			<div class="input-group-content">
				{!! Form::text('date_from', null, ['class' => 'form-control', 'required']) !!}
			</div>
			<span class="input-group-addon">to</span>
			<div class="input-group-content">
				{!! Form::text('date_to', null, ['class' => 'form-control', 'required']) !!}
				<div class="form-control-line"></div>
			</div>
		@else
		
			<div class="input-group-content">
				{!! Form::text('date_from', date("d/m/Y",strtotime($sponsor_settings->date_from)), ['class' => 'form-control', 'required']) !!}
			</div>
			<span class="input-group-addon">to</span>
			<div class="input-group-content">
				{!! Form::text('date_to', date("d/m/Y",strtotime($sponsor_settings->date_to)), ['class' => 'form-control', 'required']) !!}
				<div class="form-control-line"></div>
			</div>
		@endif
	</div>
</div>