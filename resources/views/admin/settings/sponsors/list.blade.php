@extends('admin.layouts.default')

@section('title')
	Quarterly Sponsor Setup
@endsection

@section('content')
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/jquery-ui/jquery-ui-theme.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/js/libs/jquery-fileupload/uploadfile.css')}}" />
<script type="text/javascript" src="{{asset('assets/admin/js/core/demo/DemoFormComponents.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/libs/jquery-fileupload/jquery.uploadfile.min.js')}}"></script>

<section>

<div class="section-header">
	<ol class="breadcrumb">
		<li class="active">Quarterly Sponsor Setup</li>
	</ol>
</div>

<div class="section-body">
	<div class="card">
		<div class="card-body">
			<div class="row">
				<div class="col-md-offset-1 col-md-3">Select on which event types should have these quarterly sponsors</div>
				<div class="col-md-4">
					
					@foreach($event_types as $type)
						<div class="checkbox checkbox-styled">
							<label>
								{!! Form::checkbox($type->id, true, $type->has_sponsor, ['class' => 'sponsors_types']) !!}
								{!! Form::label($type->id, $type->title) !!}
							</label>
						</div>
					@endforeach
					
				</div>
			</div>
			<p><button data-href="#" type="button" class="btn ink-reaction btn-sm btn-raised btn-success" data-toggle="modal" data-target="#add_quarter">ADD QUARTER</button></p>
			
			@foreach ($sponsors as $sponsor)
			<div class="row sponsor-row">
			    <div class="row">
    			    <div class="col-xs-1">
    			        <a href="{{ route('settings.sponsors.edit', $sponsor->id) }}"><button type="button" class="btn btn-warning btn-xs" title="Edit"><span class="glyphicon glyphicon-pencil"></span></button></a>
                        <button data-href="{{ route('settings.sponsors.deleteRow', $sponsor->id) }}" type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-trash"></span></button>
    			    </div>
    			    <div class="col-xs-1">{{ $sponsor->title }}</div>
    			    <div class="col-xs-2">{{ date("d/m/Y",strtotime($sponsor->date_from)) }} - {{ date("d/m/Y",strtotime($sponsor->date_to)) }}</div>
    			    
    			    <!-- logo 1 -->
    			    <div class="col-xs-2">
    			        @if(empty($sponsor->logo_1))
                            <div class="col-xs-9">{!! Form::select('sponsor_select_'.$sponsor->id.'-1', [''=>'Select']+$sponsors_select, null, ['class'=>'form-control select2-list', 'id' => 'sponsor_select_'.$sponsor->id.'-1']) !!}</div>
                            <div class="col-xs-1">{!! Form::button('+', ['class' => 'btn add_sponsor ink-reaction btn-raised btn-sm btn-success', 'id'=>$sponsor->id.'-1']) !!}</div>
                        @else
                            @if(!empty($sponsor->sponsor($sponsor->logo_1)->logo))
                            
                                {!! HTML::image('uploads/sponsors/'.$sponsor->sponsor($sponsor->logo_1)->logo, '', ['class'=>'img-responsive height-2']) !!}
                                <button type="button" class="btn btn-danger btn-xs delete_sponsor" id="{{$sponsor->id}}-1" title="Delete">Remove Sponsor</button>
                            
                            @endif
                            <h4>{{$sponsor->sponsor($sponsor->logo_1)->name}}</h4>
                            {{$sponsor->sponsor($sponsor->logo_1)->website}}
                        @endif
    			    </div>
    			    
    			    <!-- logo 2 -->
                    <div class="col-xs-2">
                        @if(empty($sponsor->logo_2))
                            <div class="col-xs-9">{!! Form::select('sponsor_select_'.$sponsor->id.'-2', [''=>'Select']+$sponsors_select, null, ['class'=>'form-control select2-list', 'id' => 'sponsor_select_'.$sponsor->id.'-2']) !!}</div>
                            <div class="col-xs-1">{!! Form::button('+', ['class' => 'btn add_sponsor ink-reaction btn-raised btn-sm btn-success', 'id'=>$sponsor->id.'-2']) !!}</div>
                        @else
                            @if(!empty($sponsor->sponsor($sponsor->logo_2)->logo))
                            
                                {!! HTML::image('uploads/sponsors/'.$sponsor->sponsor($sponsor->logo_2)->logo, '', ['class'=>'img-responsive height-2']) !!}
                                <button type="button" class="btn btn-danger btn-xs delete_sponsor" id="{{$sponsor->id}}-2" title="Delete">Remove Sponsor</button>
                            
                            @endif
                            <h4>{{$sponsor->sponsor($sponsor->logo_2)->name}}</h4>
                            {{$sponsor->sponsor($sponsor->logo_2)->website}}
                        @endif
                    </div>
    			    
    			    <!-- logo 3 -->
                    <div class="col-xs-2">
                        @if(empty($sponsor->logo_3))
                            <div class="col-xs-9">{!! Form::select('sponsor_select_'.$sponsor->id.'-3', [''=>'Select']+$sponsors_select, null, ['class'=>'form-control select2-list', 'id' => 'sponsor_select_'.$sponsor->id.'-3']) !!}</div>
                            <div class="col-xs-1">{!! Form::button('+', ['class' => 'btn add_sponsor ink-reaction btn-raised btn-sm btn-success', 'id'=>$sponsor->id.'-3']) !!}</div>
                        @else
                            @if(!empty($sponsor->sponsor($sponsor->logo_3)->logo))
                            
                                {!! HTML::image('uploads/sponsors/'.$sponsor->sponsor($sponsor->logo_3)->logo, '', ['class'=>'img-responsive height-2']) !!}
                                <button type="button" class="btn btn-danger btn-xs delete_sponsor" id="{{$sponsor->id}}-3" title="Delete">Remove Sponsor</button>
                            
                            @endif
                            <h4>{{$sponsor->sponsor($sponsor->logo_3)->name}}</h4>
                            {{$sponsor->sponsor($sponsor->logo_3)->website}}
                        @endif
                    </div>
                
                    <!-- logo 4 -->
                    <div class="col-xs-2">
                        @if(empty($sponsor->logo_4))
                            <div class="col-xs-9">{!! Form::select('sponsor_select_'.$sponsor->id.'-4', [''=>'Select']+$sponsors_select, null, ['class'=>'form-control select2-list', 'id' => 'sponsor_select_'.$sponsor->id.'-4']) !!}</div>
                            <div class="col-xs-1">{!! Form::button('+', ['class' => 'btn add_sponsor ink-reaction btn-raised btn-sm btn-success', 'id'=>$sponsor->id.'-4']) !!}</div>
                        @else
                            @if(!empty($sponsor->sponsor($sponsor->logo_4)->logo))
                            
                                {!! HTML::image('uploads/sponsors/'.$sponsor->sponsor($sponsor->logo_4)->logo, '', ['class'=>'img-responsive height-2']) !!}
                                <button type="button" class="btn btn-danger btn-xs delete_sponsor" id="{{$sponsor->id}}-4" title="Delete">Remove Sponsor</button>
                            
                            @endif
                            <h4>{{$sponsor->sponsor($sponsor->logo_4)->name}}</h4>
                            {{$sponsor->sponsor($sponsor->logo_4)->website}}
                        @endif
                    </div>
                    
                </div>
                <div class="row">
                    
                    <!-- logo 5 -->
                    <div class="col-xs-2">
                        @if(empty($sponsor->logo_5))
                            <div class="col-xs-9">{!! Form::select('sponsor_select_'.$sponsor->id.'-5', [''=>'Select']+$sponsors_select, null, ['class'=>'form-control select2-list', 'id' => 'sponsor_select_'.$sponsor->id.'-5']) !!}</div>
                            <div class="col-xs-1">{!! Form::button('+', ['class' => 'btn add_sponsor ink-reaction btn-raised btn-sm btn-success', 'id'=>$sponsor->id.'-5']) !!}</div>
                        @else
                            @if(!empty($sponsor->sponsor($sponsor->logo_5)->logo))
                            
                                {!! HTML::image('uploads/sponsors/'.$sponsor->sponsor($sponsor->logo_5)->logo, '', ['class'=>'img-responsive height-2']) !!}
                                <button type="button" class="btn btn-danger btn-xs delete_sponsor" id="{{$sponsor->id}}-5" title="Delete">Remove Sponsor</button>
                            
                            @endif
                            <h4>{{$sponsor->sponsor($sponsor->logo_5)->name}}</h4>
                            {{$sponsor->sponsor($sponsor->logo_5)->website}}
                        @endif
                    </div>
                    
                    
                    <!-- logo 6 -->
                    <div class="col-xs-2">
                        @if(empty($sponsor->logo_6))
                            <div class="col-xs-9">{!! Form::select('sponsor_select_'.$sponsor->id.'-6', [''=>'Select']+$sponsors_select, null, ['class'=>'form-control select2-list', 'id' => 'sponsor_select_'.$sponsor->id.'-6']) !!}</div>
                            <div class="col-xs-1">{!! Form::button('+', ['class' => 'btn add_sponsor ink-reaction btn-raised btn-sm btn-success', 'id'=>$sponsor->id.'-6']) !!}</div>
                        @else
                            @if(!empty($sponsor->sponsor($sponsor->logo_6)->logo))
                            
                                {!! HTML::image('uploads/sponsors/'.$sponsor->sponsor($sponsor->logo_6)->logo, '', ['class'=>'img-responsive height-2']) !!}
                                <button type="button" class="btn btn-danger btn-xs delete_sponsor" id="{{$sponsor->id}}-6" title="Delete">Remove Sponsor</button>
                            
                            @endif
                            <h4>{{$sponsor->sponsor($sponsor->logo_6)->name}}</h4>
                            {{$sponsor->sponsor($sponsor->logo_6)->website}}
                        @endif
                    </div>
                    
                    
                    <!-- logo 7 -->
                    <div class="col-xs-2">
                        @if(empty($sponsor->logo_7))
                            <div class="col-xs-9">{!! Form::select('sponsor_select_'.$sponsor->id.'-7', [''=>'Select']+$sponsors_select, null, ['class'=>'form-control select2-list', 'id' => 'sponsor_select_'.$sponsor->id.'-7']) !!}</div>
                            <div class="col-xs-1">{!! Form::button('+', ['class' => 'btn add_sponsor ink-reaction btn-raised btn-sm btn-success', 'id'=>$sponsor->id.'-7']) !!}</div>
                        @else
                            @if(!empty($sponsor->sponsor($sponsor->logo_7)->logo))
                            
                                {!! HTML::image('uploads/sponsors/'.$sponsor->sponsor($sponsor->logo_7)->logo, '', ['class'=>'img-responsive height-2']) !!}
                                <button type="button" class="btn btn-danger btn-xs delete_sponsor" id="{{$sponsor->id}}-7" title="Delete">Remove Sponsor</button>
                            
                            @endif
                            <h4>{{$sponsor->sponsor($sponsor->logo_7)->name}}</h4>
                            {{$sponsor->sponsor($sponsor->logo_7)->website}}
                        @endif
                    </div>
                    
                    <!-- logo 8 -->
                    <div class="col-xs-2">
                        @if(empty($sponsor->logo_8))
                            <div class="col-xs-9">{!! Form::select('sponsor_select_'.$sponsor->id.'-8', [''=>'Select']+$sponsors_select, null, ['class'=>'form-control select2-list', 'id' => 'sponsor_select_'.$sponsor->id.'-8']) !!}</div>
                            <div class="col-xs-1">{!! Form::button('+', ['class' => 'btn add_sponsor ink-reaction btn-raised btn-sm btn-success', 'id'=>$sponsor->id.'-8']) !!}</div>
                        @else
                            @if(!empty($sponsor->sponsor($sponsor->logo_8)->logo))
                            
                                {!! HTML::image('uploads/sponsors/'.$sponsor->sponsor($sponsor->logo_8)->logo, '', ['class'=>'img-responsive height-2']) !!}
                                <button type="button" class="btn btn-danger btn-xs delete_sponsor" id="{{$sponsor->id}}-8" title="Delete">Remove Sponsor</button>
                            
                            @endif
                            <h4>{{$sponsor->sponsor($sponsor->logo_8)->name}}</h4>
                            {{$sponsor->sponsor($sponsor->logo_8)->website}}
                        @endif
                    </div>
                    
                    <!-- logo 9 -->
                    <div class="col-xs-2">
                        @if(empty($sponsor->logo_9))
                            <div class="col-xs-9">{!! Form::select('sponsor_select_'.$sponsor->id.'-9', [''=>'Select']+$sponsors_select, null, ['class'=>'form-control select2-list', 'id' => 'sponsor_select_'.$sponsor->id.'-9']) !!}</div>
                            <div class="col-xs-1">{!! Form::button('+', ['class' => 'btn add_sponsor ink-reaction btn-raised btn-sm btn-success', 'id'=>$sponsor->id.'-9']) !!}</div>
                        @else
                            @if(!empty($sponsor->sponsor($sponsor->logo_9)->logo))
                            
                                {!! HTML::image('uploads/sponsors/'.$sponsor->sponsor($sponsor->logo_9)->logo, '', ['class'=>'img-responsive height-2']) !!}
                                <button type="button" class="btn btn-danger btn-xs delete_sponsor" id="{{$sponsor->id}}-9" title="Delete">Remove Sponsor</button>
                            
                            @endif
                            <h4>{{$sponsor->sponsor($sponsor->logo_9)->name}}</h4>
                            {{$sponsor->sponsor($sponsor->logo_9)->website}}
                        @endif
                    </div>
                    
                    
                    <!-- logo 10 -->
                    <div class="col-xs-2">
                        @if(empty($sponsor->logo_10))
                            <div class="col-xs-9">{!! Form::select('sponsor_select_'.$sponsor->id.'-10', [''=>'Select']+$sponsors_select, null, ['class'=>'form-control select2-list', 'id' => 'sponsor_select_'.$sponsor->id.'-10']) !!}</div>
                            <div class="col-xs-1">{!! Form::button('+', ['class' => 'btn add_sponsor ink-reaction btn-raised btn-sm btn-success', 'id'=>$sponsor->id.'-10']) !!}</div>
                        @else
                            @if(!empty($sponsor->sponsor($sponsor->logo_10)->logo))
                            
                                {!! HTML::image('uploads/sponsors/'.$sponsor->sponsor($sponsor->logo_10)->logo, '', ['class'=>'img-responsive height-2']) !!}
                                <button type="button" class="btn btn-danger btn-xs delete_sponsor" id="{{$sponsor->id}}-10" title="Delete">Remove Sponsor</button>
                            
                            @endif
                            <h4>{{$sponsor->sponsor($sponsor->logo_10)->name}}</h4>
                            {{$sponsor->sponsor($sponsor->logo_10)->website}}
                        @endif
                    </div>
                </div>
			</div>
			@endforeach
		</div>
	</div>
</div>
</section>

<div class="modal fade" id="add_quarter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		@include('admin.settings.sponsors.create')
	</div>
</div>

<script type="text/javascript">

// ajax to save changed checkbox sponsor type
$(document).ready(function () {
    
    $('.sponsors_types').bind('change', function () {
    	
        $.ajax({
        	url: "{!! route('settings.sponsors.typeUpdate') !!}",
        	type: "POST",
        	data: {"id":this.name, "value":this.checked, "_token": "{{ csrf_token() }}"},
        	success: function(){
	        	// success
	        	//location.reload();
	    }});
    });
    
    $('.add_sponsor').bind('click', function(event){
    	
    	var ids = this.id;
    	ids = ids.split("-");
		
		$.ajax({
            url: "{!! route('settings.sponsors.addSponsor') !!}",
            type: "POST",
            data: {
            	 "_token": "{{ csrf_token() }}",
				   "sponsor_setting_id": ids[0], 
				   "logo_id": ids[1],
				   "sponsor_select_id": $('#sponsor_select_' + this.id).val()
            },
            success: function () {
                location.reload();
            }
        });
    });
    
    $('.delete_sponsor').bind('click', function (event) {
    	
		var ids = this.id;
    	ids = ids.split("-");
    	
        $.ajax({
            url: "{{ route('settings.sponsors.delete') }}",
            type: "POST",
            data: {
            	"_token": "{{ csrf_token() }}",
            	"id": ids[0], 
				"logo_id": ids[1]
            },
            success: function () {
                location.reload();
            }
        });
    });
});

</script>

@include('admin.partials.deleteConfirmation')

@endsection
