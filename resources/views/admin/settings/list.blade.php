@extends('admin.layouts.default')

@section('title')
	Settings
@endsection

@section('content')

<section>

<div class="section-header">
	<ol class="breadcrumb">
		<li class="active">System Overview</li>
	</ol>
</div>

<div class="section-body">	
	<div class="row">
		<div class="col-sm-3">
			<div class="card">
				<div class="card-head style-primary"><header>Actions Logs</header></div>
				<div class="card-body">
					<p><a href="{{ route('users.list') }}">All Action Logs by User</a></p>
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="card">
				<div class="card-head style-primary"><header>Mobile APP settings</header></div>
				<div class="card-body">
				    <p><a href="{{ route('settings.apiText') }}">Generic app text</a></p>
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="card">
				<div class="card-head style-primary"><header>General Settings</header></div>
				<div class="card-body">
					<p><a href="{{ route('settings.flushCache') }}">Flush all cache</a></p>
					<p><a href="{{ route('settings.submenu') }}">Submenu Manager</a></p>
					<p><a href="{{ route('settings.highlights') }}">Highlights</a></p>
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="card">
				<div class="card-head style-primary"><header>Configuration</header></div>
				<div class="card-body">
					<p><a href="{{ route('settings.attendeeTypes') }}">Attendee Types</a></p>
					<p><a href="{{ route('settings.emailAddresses') }}">Email Addresses</a></p>
					<p><a href="{{ route('settings.emailTemplates') }}">Email Templates</a></p>
					<p><a href="{{ route('settings.eventTypes') }}">Event Types</a></p>
					<p><a href="{{ route('settings.regions') }}">Regions</a></p>
					<p><a href="{{ route('settings.sponsors') }}">Sponsors Setup</a></p>
					<p><a href="{{ route('settings.sessionTypes') }}">Session Types</a></p>
					<p><a href="{{ route('settings.technologies') }}">Technology List</a></p>
					<p><a href="{{ route('settings.themes') }}">Themes</a></p>
				</div>
			</div>
		</div>
	</div>
</div>
</section>

@endsection
