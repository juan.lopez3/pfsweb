@extends('admin.layouts.default')

@section('title')
	Event Type Step Form Editor - {{ $step->title }}
@endsection

@section('content')

<script src="{{asset('assets/admin/js/libs/form-builder-gh/vendor/js/vendor.js')}}"></script>
<script src="{{asset('assets/admin/js/libs/form-builder-gh/dist/formbuilder.js')}}"></script>

<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/js/libs/form-builder-gh/vendor/css/vendor.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/js/libs/form-builder-gh/dist/formbuilder.css')}}" />

<!--<script src="{{asset('assets/admin/js/libs/form-builder/formBuilder.js')}}"></script>
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/libs/form-builder/formBuilder.css')}}" />-->

<section>
<div class="section-header">
	<ol class="breadcrumb">
		<li class="active">{{ $step->title }} Step Form Builder</li>
	</ol>
</div>

<div class="section-body">	
	<div class="card">
		<div class="card-body">
			<br />
			{!! Form::model($step->form, ['route' => ['settings.eventTypeStep.form.update', $step->id], 'method' => 'PATCH', 'class' => 'form-vertical form-validate']) !!}
			<div class="row">
				<div class="col-sm-5 col-lg-4">
					<div class="form-group text-right">
						{!! Form::label('form_title', 'Name', ['class' => 'col-sm-5 col-md-3 control-label']) !!}
						<div class="col-sm-7 col-md-8">
							{!! Form::text('form_title', null, ['class'=>'form-control', 'required']) !!}
						</div>
					</div>
				</div>
				<div class="col-sm-5 col-lg-4">
					<div class="form-group text-right">
						{!! Form::label('form_description', 'Description', ['class' => 'col-sm-5 col-md-3 control-label']) !!}
						<div class="col-sm-7 col-md-8">
							{!! Form::text('form_description', null, ['class'=>'form-control']) !!}
						</div>
					</div>
				</div>
				<div class="col-sm-2 col-lg-2">
					{!! Form::submit('Save', ['class' => 'btn ink-reaction btn-raised btn-primary']) !!}
					<a href="{{ URL::previous() }}"><button type="button" class="btn btn-flat btn-sm btn-danger">Cancel</button></a>
				</div>
			</div>
			<br/>
			
			<div class='fb-main' id="formBuilder"></div>
			{!! Form::hidden('form_json', null, ['id' => 'form_json']) !!}
			
			{!! Form::close() !!}
		</div>
	</div>
</div>
</section>

<script>
    $(function(){
    	
      fb = new Formbuilder({
        selector: '#formBuilder',
        
        @if (!empty($step->form->form_json))
        bootstrapData: {!! $step->form->form_json !!}
        @endif
        
      });

      fb.on('save', function(payload){
        $('#form_json').val(payload);
        //console.log(payload);
      })
    });


  </script>

@endsection
