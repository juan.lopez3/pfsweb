@extends('admin.layouts.default')

@section('title')
	Event Type Steps - {{ $event_type->title }}
@endsection

@section('content')

<script type="text/javascript">
	$(document).ready(function() {
		$('#event_type_steps').dataTable({
			"iDisplayLength": 50,
		});
	} );
</script>

<section>
<div class="section-header">
	<ol class="breadcrumb">
		<li class="active">Edit {{ $event_type->title }} Steps</li>
	</ol>
</div>

<div class="section-body">	
	<p><a href="{{ route('settings.eventTypeSteps.create', $event_type->id) }}"><button class="btn ink-reaction btn-raised btn-success" type="button">ADD EVENT TYPE STEP</button></a></p>
	<div class="card">
		<div class="card-body">
			<table id="event_type_steps" class="table table-striped table-responsive" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Step Name</th>
						<th>Order</th>
						<th>Description</th>
						<th>Created</th>
						<th>Modified</th>
						<th>Actions</th>
					</tr>
				</thead>
			
				<tfoot>
					<tr>
						<th>Step Name</th>
						<th>Order</th>
						<th>Description</th>
						<th>Created</th>
						<th>Modified</th>
						<th>Actions</th>
					</tr>
				</tfoot>
			
				<tbody>
					@foreach ($event_type_steps as $step)
					<tr>
						<td>{{ $step->title }}</td>
						<td>{{ $step->order}}</td>
						<td>{{ $step->description}}</td>
						<td>{{ date("d/m/Y H:i:s",strtotime($step->created_at)) }}</td>
						<td>{{ date("d/m/Y H:i:s",strtotime($step->updated_at)) }}</td>
						<td>
							<a href="{{ route('settings.eventTypeSteps.edit', ['id' => $event_type->id, 'id2'=> $step->id]) }}"><button type="button" class="btn btn-warning btn-xs" title="Edit"><span class="glyphicon glyphicon-pencil"></span></button></a>
							<button data-href="{{ route('settings.eventTypeSteps.delete', ['id' => $event_type->id, 'id2'=> $step->id]) }}" type="button" class="btn btn-danger btn-xs" title="Delete" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-trash"></span></button>
							<a href="{{ route('settings.eventTypeStep.form.edit', $step->id) }}"><button type="button" class="btn btn-success btn-xs" title="Edit Steps">Edit Form</button></a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
</section>

@include('admin.partials.deleteConfirmation')

@endsection
