<div class="form-group floating-label">
	{!! Form::text('title', null, ['class'=>'form-control', 'required']) !!}
	{!! Form::label('title', 'Event type name') !!}
</div>
<div class="form-group floating-label">
	{!! Form::text('description', null, ['class'=>'form-control']) !!}
	{!! Form::label('description', 'Description') !!}
</div>
<div class="form-group floating-label">
	@if (!empty($next_order))
		{!! Form::input('number', 'order', $next_order, ['class'=>'form-control', 'required']) !!}
	@else
		{!! Form::input('number', 'order', null, ['class'=>'form-control', 'required']) !!}
	@endif
	
	{!! Form::label('order', 'Order') !!}
</div>