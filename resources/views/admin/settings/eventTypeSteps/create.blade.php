@extends('admin.layouts.default')

@section('title')
Add Event Type
@endsection

@section('content')

<section>
<div class="section-body">
	<div class="col-lg-offset-2 col-md-7">
		<div class="card">
			<div class="card-head style-primary">
				<header>Add Event Type Step</header>
			</div>
			
			@include('admin.partials.validationErrors')
			
			{!! Form::open(['route' => ['settings.eventTypeSteps.store', $id], 'class' => 'form form-validate']) !!}
			<div class="card-body">
				{!! Form::hidden('event_type_id', $id) !!}
				 @include('admin.settings.eventTypeSteps.form')
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					<a href="{{ URL::previous() }}"><button type="button" class="btn btn-flat">Cancel</button></a>
					{!! Form::submit('CREATE EVENT TYPE STEP', ['class' => 'btn btn-flat btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
</section>
@endsection
