@extends('admin.layouts.default')

@section('title')
Edit Event Type Step Edit
@endsection

@section('content')

<section>
<div class="section-body">
	<div class="col-lg-offset-2 col-md-7">
		<div class="card">
			<div class="card-head style-primary">
				<header>Edit Event Type</header>
			</div>
			
			@include('admin.partials.validationErrors')
			
			{!! Form::model($event_type_step, ['route' => ['settings.eventTypeSteps.update', $event_type_step->id], 'method' => 'PATCH', 'class' => 'form form-validate']) !!}
			<div class="card-body">
				 @include('admin.settings.eventTypeSteps.form')
			</div>
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					<a href="{{ URL::previous() }}"><button type="button" class="btn btn-flat">Cancel</button></a>
					{!! Form::submit('UPDATE EVENT TYPE', ['class' => 'btn btn-flat btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
</section>
@endsection
