@extends('admin.layouts.default')

@section('title')
View Message
@endsection

@section('content')

<section>
<div class="section-body">
	<div class="col-lg-offset-1 col-md-12 col-lg-10">
		<div class="card">
			<div class="card-head style-primary">
				<header>View Message</header>
			</div>
			
			<div class="col-md-2 text-right"><b>Name</b></div>
			<div class="col-md-10">@if(sizeof($sent_email->user)){{$sent_email->user->first_name}}@else - @endif</div>
			
			<div class="col-md-2 text-right"><b>Surname</b></div>
			<div class="col-md-10">@if(sizeof($sent_email->user)){{$sent_email->user->last_name}}@else - @endif</div>
			
			<div class="col-md-2 text-right"><b>Event</b></div>
			<div class="col-md-10">@if(sizeof($sent_email->event)){{$sent_email->event->title}}@else - @endif</div>
			
			<div class="col-md-2 text-right"><b>To</b></div>
			<div class="col-md-10">{{$sent_email->to}}</div>
			
			<div class="col-md-2 text-right"><b>Subject</b></div>
			<div class="col-md-10">{{$sent_email->subject}}</div>
			
			<div class="col-md-2 text-right"><b>Date</b></div>
			<div class="col-md-10">{{date("m/d/Y H:i:s", strtotime($sent_email->created_at))}}</div>
			
			<div class="col-md-2 text-right"><b>Message</b></div>
			<div class="col-md-10">{!!$sent_email->template!!}</div>
			
			<div class="card-actionbar">
				<div class="card-actionbar-row">
					<a href="{{ URL::previous() }}"><button type="button" class="btn btn-default">Back to the List</button></a>
				</div>
			</div>
		</div>
	</div>
</div>
</section>
@endsection
