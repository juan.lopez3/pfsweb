@extends('admin.layouts.default')

@section('title')
Sent Emails Log
@endsection

@section('content')


<script type="text/javascript">
$(document).ready(function() {
    $.fn.dataTable.moment( 'D/M/YYYY HH:mm:ss' );
	$('#users_list').dataTable({
		"processing": true,
		"order": [[ 0, 'desc' ]],
        "serverSide": true,
        "iDisplayLength": 50,
        "ajax": "{{route('emails.dataTable')}}"
	});
} );
</script>

<section>
<div class="section-header">
	<ol class="breadcrumb">
		<li class="active">Sent Emails Log</li>
	</ol>
</div>

<div class="section-body">
	<div class="card">
		<div class="card-body">	
			<table id="users_list" class="table table-striped table-responsive" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>ID</th>
						<th>Date</th>
						<th>Event</th>
						<th>Name</th>
						<th>Surname</th>
						<th>To</th>
						<th>Subject</th>
						<th>Opened</th>
						<th>Actions</th>
					</tr>
				</thead>
			
				<tfoot>
					<tr>
						<th>ID</th>
						<th>Date</th>
						<th>Event</th>
						<th>Name</th>
						<th>Surname</th>
						<th>To</th>
						<th>Subject</th>
						<th>Opened</th>
						<th>Actions</th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>
</section>

@endsection
