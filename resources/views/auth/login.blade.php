@extends('layouts.default')

@section('title')Non-member Log in @endsection

@section('content')
@include('partials.validationErrors')
@if (Session::has('credentials_error'))
    <div class="alert alert-danger" role="alert">
        {!! Session::pull('credentials_error') !!}
    </div>
@endif
<div class="wrapper">
    <div class="row login-options login-form">
        <div class="col-12" style="margin-bottom:6% ">
            <h3  class="titulo-login titulo-uno">Non-member log in for events</h3>
        </div>
        <div class="col-sm-5">
            {!! Form::open(['url' => '/auth/login', 'class' => 'form form-validate']) !!}
            <div class="form-group">
                {!! Form::label('email', 'Email') !!}
                {!! Form::input('email', 'email',  null, ['class'=>'form-control form-control-input', 'required']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('password', 'Events password') !!}
                {!! Form::password('password', ['class'=>'form-control form-control-input', 'required']) !!}
                <p class="help-block"><a href="{{ url('/password/email') }}" target="_parent">Forgotten password</a></p>
            </div>
            <br/>
            <div class="row">
    
                <div class="col-xs-6">
                    {!! Form::submit('Login', ['class' => 'btn boton', 'style'=>'width:170px']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        <div class="col-sm-1"></div>
        <div class="col-sm-6 sdo-form">
            <h3 class="titulo-login">Non-member sign up for events</h3>
            <br/>
            <p>We are happy for non-PFS members to attend up to two PFS events, after which we require that everyone signs up for membership. If this is your first event and you are not sure yet if you want to sign up for membership, please click ‘sign up’ below and you will then be able to book onto events as a non-PFS member.</p>
    
            <p>If you would like to become a PFS member please <a href="http://www.thepfs.org/membership">click here</a></p>
    
            <a href="{{route('auth.signUp')}}" class="btn boton" style="width:170px">Sign up</a>
        </div>
    </div>
</div>


@endsection
