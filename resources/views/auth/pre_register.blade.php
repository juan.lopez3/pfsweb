@extends('layouts.default')

@section('title')Non-member Sign up @endsection

@section('content')
@include('partials.validationErrors')
<div class="wrapper"  style="padding:8% 0 10% 0">
    <div class="row login-options" style="margin-left:0px; margin-right:0px;">
        <div class="col-md-12">
            <h3 class="titulo-login">Non-member sign up for events</h3>
    
            <br/>
    
            {!! Form::open(['url' => route('auth.signUp.check'), 'class' => 'form form-validate']) !!}
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('email', 'Email') !!}
                        {!! Form::input('email', 'email',  null, ['class'=>'form-control form-control-input', 'required']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    {!! Form::submit('Next', ['class' => 'btn boton', 'style'=>'width:180px']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>


@endsection
