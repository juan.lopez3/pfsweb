@extends('layouts.default')

@section('content')

<div class="row">
	<div class="col-md-8 col-md-offset-2">

		<h3>Set or reset your password for events</h3>
		<br/>

		<p>Please enter the email you have used in the past to book and attend PFS events as a non-member. You will be sent an email to set/reset your password. Please note, as a non-PFS member, your are required to login for events using an Events Password, and this is not related to any password you may have used to sign in to your general PFS / CII account (although you are free to use the same password if you wish).</p>
		<p>If you do not receive this please check your junk / spam folders</p>

		<div class="panel-body">

			@include('partials.validationErrors')
			@if (session('status'))
				<div class="alert alert-success">
					{{ session('status') }}
				</div>
			@endif
			<form class="form-horizontal form-validate" role="form" method="POST" action="{{ url('/password/email') }}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="form-group">
					<label class="col-md-4 control-label required">E-Mail Address</label>
					<div class="col-md-6">
						<input type="email" class="form-control" required name="email" value="{{ old('email') }}">
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-6 col-md-offset-4">
						<button type="submit" class="btn btn-primary">
							Send Password Reset Link
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
