@extends('layouts.default')

@section('title')Non-member Sign up @endsection

@section('content')
<div class="wrapper">
    <div class="row" style="padding:6% 0">
        <div class="col-md-12">
            <div class="panel panel-default">
                <h3 style="text-align:center" class="titulo-login">Non-member sign up</h3><br>
                <div class="panel-body cuerpo-registro">
    
                @include('partials.validationErrors')
    
    				{!! Form::open(['url' => '/auth/register', 'class'=>'form-hotizontal form-validate']) !!}
    
                    <div class="row">
                        <div class="col-md-10">
                            <div class="form-group">
                                {!! Form::label('email', 'Email *', ['class' => 'col-sm-3 control-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::input('email', 'email', Session::pull('email'), ['class'=>'form-control form-control-input', 'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div class="row">
                        <div class="col-md-10">
                            <div class="form-group">
                                {!! Form::label('title', 'Title *', ['class' => 'col-sm-3 control-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::select('title', ['Mr'=>'Mr', 'Ms' => 'Ms', 'Mrs'=>'Mrs', 'Miss'=>'Miss', 'Dr'=>'Dr'], null, ['class'=>'form-control form-control-input', 'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div class="row">
                        <div class="col-md-10">
                            <div class="form-group">
                                {!! Form::label('first_name', 'First name *', ['class' => 'col-sm-3 control-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('first_name', null, ['class'=>'form-control form-control-input', 'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div class="row">
                        <div class="col-md-10">
                            <div class="form-group">
                                {!! Form::label('last_name', 'Last name *', ['class' => 'col-sm-3 control-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('last_name', null, ['class'=>'form-control form-control-input', 'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div class="row">
                        <div class="col-md-10">
                            <div class="form-group">
                                {!! Form::label('phone', 'Telephone *', ['class' => 'col-sm-3 control-label']) !!}
                                <div class="col-sm-4">
                                    {!! Form::text('phone', null, ['class'=>'form-control form-control-input', 'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-10">
                            <div class="form-group">
                                {!! Form::label('mobile', 'Mobile', ['class' => 'col-sm-3 control-label']) !!}
                                <div class="col-sm-4">
                                    {!! Form::text('mobile', null, ['class'=>'form-control form-control-input']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div class="row">
                        <div class="col-md-10">
                            <div class="form-group">
                                {!! Form::label('fca_number', 'FCA number', ['class' => 'col-sm-3 control-label']) !!}
                                <div class="col-sm-4">
                                    {!! Form::text('fca_number', null, ['class'=>'form-control form-control-input']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div class="row">
                        <div class="col-md-10">
                            <div class="form-group">
                                {!! Form::label('company', 'Company *', ['class' => 'col-sm-3 control-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('company', null, ['class'=>'form-control form-control-input', 'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div class="row">
                        <div class="col-md-10">
                            <div class="form-group">
                                {!! Form::label('job_title', 'Job title *', ['class' => 'col-sm-3 control-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('job_title', null, ['class'=>'form-control form-control-input', 'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div class="row">
                        <div class="col-md-10">
                            <div class="form-group">
                                {!! Form::label('postcode', 'Postcode *', ['class' => 'col-sm-3 control-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::text('postcode', null, ['class'=>'form-control form-control-input', 'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div class="row">
                        <div class="col-md-10">
                            <div class="form-group">
                                {!! Form::label('country_id', 'Country *', ['class' => 'col-sm-3 control-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::select('country_id', $countries, null, ['class'=>'form-control form-control-input', 'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div class="row">
                        <div class="col-md-10">
                            <div class="form-group">
                                {!! Form::label('password', 'Password *', ['class' => 'col-sm-3 control-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::password('password', ['class'=>'form-control form-control-input', 'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div class="row">
                        <div class="col-md-10">
                            <div class="form-group">
                                {!! Form::label('password_confirmation', 'Confirm password *', ['class' => 'col-sm-3 control-label']) !!}
                                <div class="col-sm-9">
                                    {!! Form::password('password_confirmation', ['class'=>'form-control form-control-input', 'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div class="row">
                        <div class="col-md-10">
                            <p>* required fields</p>
                        </div>
                    </div>
    
                    <div class="row>">
                        <div class="form-group" style="text-align:center;">
                            {!! Form::submit('Sign up', ['class' => 'btn boton']) !!}
                        </div>
                    </div>
    
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
