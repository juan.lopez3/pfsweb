@extends('layouts.default')

@section('title')Log in @endsection

@section('content')
@include('partials.validationErrors')
<div class="wrapper"  style="padding: 10% 0">
    <h4 style="margin-left:15px; margin-right:15px; text-align:center" class="titulo-login">You need to sign in to access events, please select one of the following options:</h4>
    <div class="row login-options" style="margin-right:0px;">
        <div class="col-sm-6 text-center login-l">
            <h4>I am a member of the PFS</h4>
            <a href="http://www.thepfs.org/auth/events?returnUrl={{Request::url()}}" class="btn boton" style="width: 225px;">Member Log in</a>
        </div>
        <div class="col-sm-6 text-center login-r">
            <h4>I am not a PFS member</h4>
            <a href="{{url('/auth/login')}}" class="btn boton" style="width: 225px;">Non-member Log in</a>
        </div>
    </div>
</div>


@endsection
