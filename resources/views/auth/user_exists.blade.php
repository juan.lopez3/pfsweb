@extends('layouts.default')

@section('title')Non-member Log in @endsection

@section('content')
    @include('partials.validationErrors')
<div class="wrapper maximo" style="padding: 9% 0;">
    <div class="row">
        <div class="col-sm-12">
            <h3 class="titulo-login">Non-member sign up for events</h3>
            <br/>

            {!! Form::open(['url' => '/auth/login', 'class' => 'form form-validate']) !!}
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('email', 'Email') !!}
                        {!! Form::input('email', 'email',  $email, ['class'=>'form-control-input form-control', 'required']) !!}
                        <span style="color: red"><b>There is already an events profile associated with this email. Please enter your password to access the events portal</b></span>
                    </div>
                    <div class="form-group">
                        {!! Form::label('password', 'Password') !!}
                        {!! Form::password('password', ['class'=>'form-control-input form-control', 'required']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 text-right">
                    {!! Form::submit('Login', ['class' => 'btn boton', 'style'=>'width:180px']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        <div class="col-md-7">
            <p>Alternatively, if you need to set your events portal password for the first time, or reset a forgotten events portal password, please click the below link.</p>

            <p>Please note, this password is used exclusively to access events whilst you are a non-PFS member, and is not related to any password used to access PFS / CII accounts (although if you have a PFS / CII account, you are free to use the same password if you wish).</p>

            <p><a href="{{ url('/password/email') }}">I would like to set or reset my password</a></p>
        </div>
    </div>
</div>
@endsection
