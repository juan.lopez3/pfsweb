<!DOCTYPE html>
<html>
	<head>
		<title>{{$event->title}} Agenda</title>
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
		<style>
			@font-face {
				font-family: 'Gotham-Light';
				src: url("{{ asset('fonts/Gotham-Light.otf') }}");
				font-weight: 300;
			}

			@font-face {
				font-family: 'Gotham';
				src: url("{{ asset('fonts/Gotham-Book.ttf') }}");
				font-weight: 400;
			}

			@font-face {
				font-family: 'Gotham-Medium';
				src: url("{{ asset('fonts/Gotham-Medium.ttf') }}");
				font-weight: 600;
			}

			@font-face {
				font-family: 'Gotham-Bold';
				src: url("{{ asset('fonts/Gotham-Bold.ttf') }}");
				font-weight: 700;
			}

			@font-face {
				font-family: 'Gotham-Black';
				src: url("{{ asset('fonts/Gotham-Black.ttf') }}");
				font-weight: 900;
			}

			html, body {
				background-color: #e5e1e5;
				font-family: 'Gotham', sans-serif;
				font-size: 16px;
				margin: 0 0 0 0;
				padding: 100px 0 0 0;
			}

			.pdf-page-break {
				page-break-after: always;
			}

			.pdf-single-page {
				margin: -100px 0 0 0;
				max-width: 100%;
				width: 100%;
				z-index: 2;
			}

			.pdf-single-page-logo-top img {
				height: 180px;
				width: auto;
			}

			.pdf-single-page-logo-bottom.last {
				margin: 0 25px;
			}

			.pdf-single-page-logo-bottom img {
				height: 100px;
				width: auto;
			}

			.pdf-single-page-text {
				font-size: 0.9rem;
			}

			.pdf-logo-number {
				height: 100px;
				margin: 1em 0;
				position: relative;
				width: auto;
			}

			.pdf-logo-number img {
				height: auto;
				width: 75px;
			}

			.pdf-logo-number span {
				color: #e5e1e5;
				font-family: 'Gotham-Medium', sans-serif;
				font-size: 1.7em;
				position: absolute;
				bottom: 90px;
				left: 0;
				right: 0;
				text-align: center;
				width: 75px;
			}

			.pdf-logo-number span small {
				font-size: 0.5rem;
			}

			.pdf-single-page-content {
				/*background-image: url("{{asset('images/agenda_template/bg.png')}}");*/
				background-position: top right;
				background-repeat: no-repeat;
				max-width: 100%;
				padding: 2em 4em;
				width: 100%;
			}

			.pdf-single-page-title {
				color: #685569;
				font-family: 'Gotham-Medium', sans-serif;
				font-size: 32px;
				line-height: 0.9;
				letter-spacing: -2px;
				margin: 0.5em 0;
				width: 50%;
			}

			.pdf-single-page-title p {
				color: #3e2d54;
				font-family: 'Gotham-Bold', sans-serif;
				font-size: 2rem;
				margin: 5px 0;
			}

			.pdf-single-page-text {
				font-family: 'Gotham-Medium', sans-serif;
				font-size: 1em;
				width: 70%;
			}

			.pdf-single-page-text b {
				font-family: 'Gotham-Bold', sans-serif;
				font-size: 1.05em;
			}

			.pdf-single-page-subtitle {
				width: 75%;
			}
			.pdf-single-page-subtitle div {
				color: #685569;
				font-family: 'Gotham-Medium', sans-serif;
				font-size: 24px;
				line-height: 1.1;
				letter-spacing: -1px;
				margin-left: 0;
			}

			.pdf-single-page-subtitle div p {
				color: #3e2d54;
				font-size: 26px;
				font-family: 'Gotham-Black', sans-serif;
				margin: 10px 0;
			}

			.pdf-content-page {
				display: block;
				margin: -50px 0 0 0;
				padding: 2em;
				width: 100%;
			}

			.pdf-content-page-small-text {
				color: #333;
				font-size: 14px;
				font-family: 'Gotham-Medium', sans-serif;
				margin: 10px 0;
			}

			.pdf-content-header {
    		border-bottom: 1px solid #cbafb9;
				height: 70px;
				margin: 0 2em;
    		padding: 10px 0;
				position: fixed;
				width: 100%;
				z-index: 1;
			}
			header .pagenum:before {
				content: counter(page);
			}

			.pdf-content-header-left {
				display: inline-block;
				height: 70px;
				margin: 0;
				padding: 0;
				position: relative;
				width: 49%;
			}

			.pdf-content-header-left img {
				height: 70px;
				width: auto;
				float: left;
				margin: 0;
				padding: 0;
				text-align: left;
			}

			.pdf-content-header-right {
				display: inline-block;
				height: 70px;
				margin: 0;
				padding: 0;
				position: relative;
				width: 50%;
			}

			.pdf-content-header-right span {
				color: #3e2d54;
				display: block;
				font-family: 'Gotham-Medium', sans-serif;
				font-size: 15px;
				float: right;
				line-height: 50px;
				height: 50px;
				margin: 0;
				padding: 0;
				position: relative;
				text-align: right;
			}
			
			.pdf-content-partners {
				border-bottom: 1px solid #cbafb9;
				margin: 0 auto 2em auto;
				padding: 0.5em 0 2em 0;
				position: relative;
				text-align: center;
				white-space: normal;
				width: 100%;
			}

			.pdf-content-partners table {
				width: 100%;
				text-align: center; 
    		    vertical-align: middle;
			}

			.pdf-partner-img {
				height: 50px;
				margin: 10px;
				width: auto;
			}

			.pdf-content-table {
				background-color: #e5e1e5;
				padding: 0;
				width: 100%;
			}

			.pdf-content-table-title-wrapper {
				background-color: #c9aab4;
				font-family: 'Gotham-Bold', sans-serif;
				padding: 5px;
				text-align: center;
			}

			.pdf-content-table-title {
				background-color: #c9aab4;
				color: #4b1856;
				font-family: 'Gotham-Bold', sans-serif;
				padding: 10px;
				text-align: center;
			}

			.pdf-content-table-row {
				background-color: transparent;
				padding: 15px 10px 10px 10px;
			}

			.pdf-content-table-hour {
				background-color: transparent;
				/* border: 0; */
				color: #333;
				font-size: 1em;
				font-family: 'Gotham-Bold', sans-serif;
				padding: 10px;
				text-align: center;
				width: 20%;
			}

			.pdf-content-table-desc {
				background-color: transparent;
				/* border: 1px solid #c9aab4; */
				color: #333;
			/* 	font-size: 1.1em; */
				font-family: 'Gotham-Medium', sans-serif;
				padding: 10px;
				width: 80%;
			}

			.pdf-content-table-desc b {
				font-size: 1.3em;
				font-family: 'Gotham-Medium', sans-serif;
			}

			.last .pdf-clearfix {
				background-color: #e5e1e5;
				height: 100px;
				width: 100%;
				z-index:-1;
				position: fixed;
				top:0cm;
				left:0cm;
			}

			
            /**  .pdf-single top
            * Define the width, height, margins and position of the watermark.
            **/
            #watermark {
                position: fixed; 
                bottom:   0cm;
                right:    0cm;
                /** Change image dimensions**/
                /*width:    10cm;*/
				width:10cm;
                height:   21cm;
                /** Your watermark should be behind every content**/
                z-index:  0;
            }	
			.col-3{
				width:30%;
			}
			.col-9{
				width:70%;
			}		
		</style>
	</head>

	<body>
		<div class="pdf-single-page">

			<div id="watermark">
				<img src="images/agenda_template/bg.png" height="100%" width="100%" />
			</div>
			<div class="pdf-single-page-content">
				<div class="pdf-single-page-logo-top">
					@if($event->eventType->id == 58)
						<img src="{{asset('images/agenda_template/smp_logo.png')}}" width="300" height="180" alt="SMP Accredited">
					@else
						<img src="{{asset('images/agenda_template/pfs_logo_2.png')}}"/>
					@endif
				</div>
				<div class="pdf-single-page-title">
					<p>Agenda</p>
					<span>{{$event->title}}</span>
				</div>

				<div class="pdf-single-page-subtitle">
					<div>
						<p>Venue:</p>
						<span>
							@if(!is_null($event->venue))
								{{$event->venue->name}}, {{$event->venue->address}}, {{$event->venue->city}}
								@if(!empty($event->venue->country)), {{$event->venue->country}}@endif, {{$event->venue->postcode}}
							@endif 
						</span>
					</div>
					<div>
						<p>Date:</p>
						<span>
							@if ($event->event_date_from == $event->event_date_to)
								{{ date('l d M Y',strtotime($event->event_date_from)) }}
							@else
								{{ date('d',strtotime($event->event_date_from)) }}-{{ date('d M Y',strtotime($event->event_date_to)) }}
							@endif
						</span>
					</div>
				</div>
				<div class="pdf-single-page-logo-bottom">
						<div class="pdf-logo-number">
						<img src="{{asset('images/agenda_template/cpd_logo_num.png')}}"/>
						<span>{{$cpd_time}}</span>
						</div>
				</div>
			</div>
		</div>

		<div class="pdf-page-break"></div>

		<?php $previus_session =false; ?>

		<header class="pdf-content-header">
			<div class="pdf-content-header-left">

					@if($event->eventType->id == 58)
						<img src="{{asset('images/agenda_template/smp_logo.png')}}" width="150" height="90" alt="SMP Accredited">
					@else
						<img src="{{asset('images/agenda_template/pfs_logo_1.png')}}"/>
					@endif
			</div>
			<div class="pdf-content-header-right">
				<span class="pdf-content-page-small-text">{{$event->title}}</span>
			</div>
		</header>

		<div class="pdf-content-page">
			<div class="pdf-content-page-small-text">
				<p>In association with our Partners in Professionalism:</p>
			</div>
			<div class="pdf-content-partners">

				@if ($format == 'pdf')
					@if((sizeof($quarterly_sponsors) || sizeof($event->sponsors)) && in_array(\Config::get('tabs.tab_8'), $event->tabs()->lists('id')))

						@if($event->id == 442)
							<?php $imgs = [];$names = []; ?>
							@foreach($event->sponsors()->where('type', 0)->where('event_sponsor.order', '>', 0)->orderBy('event_sponsor.order', 'ASC')->limit(19)->get() as $sponsor)
								@if(!empty($sponsor->logo))
								<?php $imgs[] = $sponsor->logo; ?>
								@else
									<?php $names[] = $sponsor->name ?>
								@endif
							@endforeach 
						@else
							@foreach($event->sponsors()->where('type', 0)->get() as $sponsor)
								@if(!empty($sponsor->logo))
								<?php $imgs[] = $sponsor->logo; ?>
								@else
									<?php $names[] = $sponsor->name ?>
								@endif
							@endforeach
						@endif
						
						@if (strpos($event->eventType->title,'National Financial' )!==FALSE)
						@foreach($quarterly_sponsors as $sponsor)
							<?php
							for ($i=1; $i <= 10; $i++) {
								$logo_id = "logo_".$i;
								if (empty($sponsor->{$logo_id})) continue;
								
								$sp1 = $sponsors->find($sponsor->{$logo_id});
							?>
								@if(!empty($sp1->logo))
								<?php $imgs[] = $sp1->logo; ?>
								@else
								<?php $names[] = $sp1->name ?>
								@endif
							<?php } ?>
						@endforeach
						@endif

								
						<?php $total_columns = 4; $indice=0;?>
						<table>
						@foreach($imgs as $img)
						   @if((($indice++)%$total_columns)==0) <tr> @endif
							<td> 
								<img class="pdf-partner-img" src="{{\Config::get('app.url')}}/uploads/sponsors/{{$img}}" />
							</td>
						   @if((($indice)%$total_columns)==0) </tr> @endif	
						@endforeach
						<!-- serrando el <tr> si las imagenes no son multiplo de $total_columns -->
						@if((($indice)%$total_columns)!=0) </tr> @endif
						</table>

					@endif
				@endif
			</div>
			
			<div class="pdf-content-table">
				<!-- <table cellpadding="20" cellspacing="5" cellmargin="20"> -->
					<?php $color_strong = false;?>
					@foreach($event->scheduleSessions()->with('contributors')->with('slot')->orderBy('start_date')->orderBy('session_start')->get() as $session)
						<?php $color_strong = !$color_strong;?>				
						@if(!($previus_session) || $previus_session->slot->start_date != $session->slot->start_date)
							<div class="pdf-content-table-title-wrapper">
								<span colspan="2" class="pdf-content-table-title">
									{{date("j-M-Y", strtotime($session->slot->start_date))}} 
								</span>
							</div>
						@endif
						<div class="pdf-content-table-row row" style="padding:10px;padding-top:15px;">
							<span class="pdf-content-table-hour col-3" valign="middle"> 
							    <span class="time">{{date("H:i", strtotime($session->session_start))}} - {{date("H:i", strtotime($session->session_end))}}</span>
							</span>
							<span class="pdf-content-table-desc col-9">
								<!-- SESSION CONTENT -->
								<!-- TITLE -->
								<b>{{$session->title}}</b><br/>
								<!-- OBJECTIVES -->
								@if(!empty($session->learning_objectives))
									{!!$session->learning_objectives!!}
								@endif
								<!-- CONTRIBUTORS -->
								@foreach($session->contributors as $contr)
									{{$contr->badge_name}}, {{$contr->company}} {{$contr->job_role}} 
								@endforeach
								<!-- END SESSION CONTENT -->
							</span>
						</div>
						<?php $previus_session =$session; ?>
					@endforeach
				<!-- </table> -->
			</div>
		</div>

		<div class="pdf-page-break"></div>

		<div class="pdf-single-page last">
		<div id="watermark">
				<img src="images/agenda_template/bg.png" height="100%" width="100%" />
		</div>		
		<div class="pdf-clearfix"></div>
			<div class="pdf-single-page-content">
				<div class="pdf-single-page-logo-top">
					@if($event->eventType->id == 58)
						<img src="{{asset('images/agenda_template/smp_logo.png')}}" width="300" height="180" alt="SMP Accredited">
					@else
						<img src="{{asset('images/agenda_template/pfs_logo_2.png')}}"/>
					@endif
				</div>

				<div class="pdf-single-page-logo-bottom last">
						<img src="{{asset('images/cpd-logo.png')}}"/>
				</div>

				<!-- <div class="pdf-single-page-title">
					<h2>Total CPD provided: {{$cpd_time}} h</h2>
				</div> -->

				<div class="pdf-single-page-text">
					<p>The content in each session has been carefully selected and can be considered for both structured and unstructured CPD hours, depending how this activity addressed each individual's personal development needs.</p>
					<p><b>Structured CPD</b> is the undertaking of any formal learning activity designed to meet a specific learning outcome (this is what an individual is expected to know, understand or do as a result of his or her learning).</p>
					<p><b>Unstructured CPD</b> is any activity an individual considers has met a learning outcome, but which may not have been specifically designed to meet their development needs.</p>
					<p><br></p>
					<p>Attendance at this event can be included as part of your CPD requirements should you consider it relevant to your professional development needs.</p>
				</div>
			</div>
		</div>
	</body>
</html>
