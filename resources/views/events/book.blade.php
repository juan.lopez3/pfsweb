@extends('layouts.default')
@section('title'){{$event->title}} @endsection
@section('description'){{$event->description}} @endsection

@section('content')
<link type="text/css" rel="stylesheet" href="{{asset('assets/site/css/theme-default/libs/select2/select2.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/site/css/theme-default/libs/wizard/wizard.css')}}" />
<script src="{{asset('assets/admin/fancybox/source/jquery.fancybox.js')}}"></script>
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/fancybox/source/jquery.fancybox.css')}}" />
<script type="text/javascript">
$(document).ready(function() {
	$('.fancybox').fancybox();
});
</script>
<?php
	$member_type = (Auth::user()->role_id == role('member')) ? true : false;
	if ($member_type == true) {
		$user_member_type = 1;
	} else {
		$user_member_type = 0;
	}
?>

<div class="wrapper">
@include('partials.top')

<!-- <div class="row">
		@if(!empty($event->eventType->branding_image))
	    <div class="col-md-12">
			<div class="branding-cover" style="background: url('{{Config::get("app.url")."uploads/branding/".$event->eventType->branding_image}}') #F7F7F7; background-size: cover;">
	    @else
	    <div class="col-md-12">
			<div class="branding-cover" style="background: #F7F7F7">
	    @endif -->
	    <div class="row" style="margin-left:0px; margin-right:-15px;">
			<h1 class="col-md-11 event-title evento-titulo borde" >{{$event->title}}</h1>
		</div>
		<!-- <p class="borde"></p> -->
	    <!-- <div class="clearfix"></div>
	    </div>
	</div>
</div> -->

@include('partials.validationErrors')

<!-- BEGIN VALIDATION FORM WIZARD -->
<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-body ">
				{!! $event->registration_text !!}
			
				<div id="rootwizard2" class="form-wizard form-wizard-horizontal">
					<?php $step_id = 1; ?>
					@if(isset($amendment))
						{!! Form::model(Auth::user(), ['route' => ['events.amendRegistration', $event->slug], 'method' => 'POST', 'class' => 'form form-validation', 'id'=>'reg-form']) !!}
					@else
						{!! Form::model(auth()->user(), ['route' => ['events.submitBooking', $event->slug], 'class' => 'form form-validation', 'id'=>'reg-form']) !!}
					@endif
						<div class="form-wizard-nav">
							<div class="progress"><div class="progress-bar progress-bar-primary"></div></div>
							<ul class="nav nav-justified">
								
								<li class="active"><a href="#step{{$step_id}}" class="barra-progreso" data-toggle="tab"><span class="step">{{$step_id}}</span> <span class="title">Confirm details</span></a></li>
								<?php $step_id++;?>
								
								@if(sizeof($slot_questions))
								<li class=""><a href="#step{{$step_id}}" class="barra-progreso" data-toggle="tab"><span class="step">{{$step_id}}</span> <span class="title">Additional questions</span></a></li>
								
								<?php $step_id++;?>
								@endif
								
								@if($event->main_schedule_visible_reg && $event->scheduleTimeSlots()->where('bookable', 1)->count())
								<li class=""><a href="#step{{$step_id}}" class="barra-progreso" data-toggle="tab"><span class="step">{{$step_id}}</span> <span class="title">Select sessions</span></a></li>
								<?php $step_id++;?>
								@endif
								
								@if($event->id == 680 && $user_member_type == 0)
									@foreach($event->eventType->typeSteps as $step)
									<li class=""><a href="#step{{$step_id}}" data-toggle="tab"><span class="step">{{$step_id}}</span> <span class="title">{{$step->title}}</span></a></li>
									<?php $step_id++; ?>
									@endforeach
								@endif
								
								<li class=""><a href="#step{{$step_id}}" data-toggle="tab"><span class="step">{{$step_id}}</span> <span class="title">Confirm & Register</span></a></li>
								
								@if($event->price > 0.00 && $user_member_type == 1)
								<?php $step_id++; ?>

								<li class=""><a href="#" data-toggle="tab"><span class="step">{{$step_id}}</span> <span class="title">Payment</span></a></li>
								@elseif($event->price_nonmember > 0.00 && $user_member_type == 0)
								<?php $step_id++; ?>

								<li class=""><a href="#" data-toggle="tab"><span class="step">{{$step_id}}</span> <span class="title">Payment</span></a></li>

								@endif
							</ul>
						</div><!--end .form-wizard-nav -->
						
						<?php $step_id = 1;?>
						
						<div class="tab-content clearfix">
							<div class="tab-pane active" id="step{{$step_id}}">
								<?php $step_id++;?>
								<br/><br/>
								
								<p><i>For CPD purposes, please ensure your details are correct</i></p>
								<p><b><i>If any of your details are incorrect please edit these before proceeding.</i></b></p>
								<!-- <p><b><i>If any of your details are incorrect please edit these in the '<a href="https://www.thepfs.org/my-pfs/edit-profile?returnUrl={{route('events.updateProfile', [Auth::user()->pin, Auth::user()->email])}}?redirect={{route('events.book', $event->slug)}}">My PFS</a>' section before proceeding.</i></b></p> -->
								<br/>
								<div class="row">
									<div class="col-sm-6">

										<h3 class="titulo-about">Contact details @if(auth()->user()->role_id == config('roles.member')) <a class="btn btn-sm" href="https://www.thepfs.org/my-pfs/edit-profile?returnUrl={{route('events.updateProfile', [Auth::user()->pin, Auth::user()->email])}}?redirect={{route('events.book', $event->slug)}}">Edit details </a> @endif </h3>

										@if (auth()->user()->role_id == config('roles.non_member'))
											<div class="row">
												<div class="col-md-12">
													<div class="form-group">
														{!! Form::label('title', 'Title', ['class' => 'col-sm-3 control-label label-formulario']) !!}
														<div class="col-sm-9 padding-rl">
															{!! Form::select('title', ['Mr'=>'Mr', 'Ms' => 'Ms', 'Mrs'=>'Mrs', 'Miss'=>'Miss', 'Dr'=>'Dr'], null, ['class'=>'form-control form-control-input', 'required']) !!}
														</div>
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<div class="form-group">
														{!! Form::label('first_name', 'First name', ['class' => 'col-sm-3 control-label label-formulario']) !!}
														<div class="col-sm-9 padding-rl">
															{!! Form::text('first_name', null, ['class'=>'form-control form-control-input', 'required']) !!}
														</div>
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<div class="form-group">
														{!! Form::label('last_name', 'Last name', ['class' => 'col-sm-3 control-label label-formulario']) !!}
														<div class="col-sm-9 padding-rl">
															{!! Form::text('last_name', null, ['class'=>'form-control form-control-input', 'required']) !!}
														</div>
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<div class="form-group">
														{!! Form::label('phone', 'Telephone', ['class' => 'col-sm-3 control-label label-formulario']) !!}
														<div class="col-sm-9 padding-rl">
															{!! Form::text('phone', null, ['class'=>'form-control form-control-input']) !!}
														</div>
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<div class="form-group">
														{!! Form::label('mobile', 'Mobile', ['class' => 'col-sm-3 control-label label-formulario']) !!}
														<div class="col-sm-9 padding-rl">
															{!! Form::text('mobile', null, ['class'=>'form-control form-control-input']) !!}
														</div>
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<div class="form-group">
														{!! Form::label('company', 'Company', ['class' => 'col-sm-3 control-label label-formulario']) !!}
														<div class="col-sm-9 padding-rl">
															{!! Form::text('company', null, ['class'=>'form-control form-control-input']) !!}
														</div>
													</div>
												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<div class="form-group">
														{!! Form::label('postcode', 'Postcode', ['class' => 'col-sm-3 control-label label-formulario']) !!}
														<div class="col-sm-9 padding-rl">
															{!! Form::text('postcode', null, ['class'=>'form-control form-control-input', 'required']) !!}
														</div>
													</div>
												</div>
											</div>
										@else
										<dl>
											<dt>Title</dt>
											<dd>{{Auth::user()->title}}</dd>
											
											<dt>First name</dt>
											<dd>{{Auth::user()->first_name}}</dd>
											
											<dt>Last name</dt>
											<dd>{{Auth::user()->last_name}}</dd>
											
											<dt>Email</dt>
											<dd>{{Auth::user()->email}}</dd>
											
											<dt>Phone</dt>
											<dd>{{\Auth::user()->phone}}</dd>
											
											<dt>Mobile</dt>
											<dd>{{\Auth::user()->mobile}}</dd>
											
											<dt>Postcode</dt>
											<dd>{{Auth::user()->postcode}}</dd>
											
											@if(!empty(Auth::user()->company))
											<dt>Company</dt>
											<dd>{{Auth::user()->company}}</dd>
											@endif
											
											<dt>Membership PIN</dt>
											<dd>{{Auth::user()->pin}}</dd>
											
											<dt>PFS Designation</dt>
											<dd>{{\Auth::user()->pfs_class}}</dd>
										</dl>
										@endif
									</div>
									
									<div class="col-sm-6">
										<h3 class="titulo-about">Event details</h3>
										
										<div class="row">
                                            <div class="col-sm-3">
                                                {!! Form::label('badge_name', 'Badge name', ['class' => 'control-label label-formulario']) !!}
                                            </div>
                                            <div class="col-sm-9">
                                                {!! Form::text('badge_name', Auth::user()->badge_name, ['class'=>'form-control form-control-input']) !!}
                                            </div>
                                        </div>
                                        
										<div class="row">
											<div class="col-sm-3 ">
												{!! Form::label('dietary_requirement_id', 'Dietary Requirements *', ['class' => 'control-label label-formulario']) !!}
											</div>
											<div class="col-sm-9">
												{!! Form::select('dietary_requirement_id', $dietary_requirements, Auth::user()->dietary_requirement_id, ['class'=>'form-control select2-list form-control-input', 'id'=>'dietary_requirement_id']) !!}
											</div>
										</div>
										
										<div class="row" id="dietary_other">
											<div class="col-sm-12 col-xs-3 text-right form-group">
												{!! Form::label('dietary_requirement_kosher', 'Please specify below:', ['class' => 'control-label']) !!}
											</div>
											<br>
											<div class="col-sm-3 col-xs-3 ">
												{!! Form::label('dietary_requirement_other', 'Other', ['class' => 'control-label label-formulario']) !!}
											</div>
											<div class="col-sm-9 col-xs-9">
											    <div class="form-group">
											    @if (Auth::user()->dietary_requirement_id == 13)
											    {!! Form::text('dietary_requirement_other', null, ['class'=>'form-control form-control-input', 'id'=>'dietary_requirement_other', 'required']) !!}
											    @else
												{!! Form::text('dietary_requirement_other', null, ['class'=>'form-control form-control-input', 'id'=>'dietary_requirement_other']) !!}
												@endif
												</div>
											</div>
										</div>
											<div class="row" id="dietary_kosher">
												<div class="col-sm-12 col-xs-3 text-right">
													{!! Form::label('dietary_requirement_kosher', 'NOTE: Due to venue/time constraints this is not guaranteed, please email regionals@pfsevents.org to confirm if we can cater for this.', ['class' => 'control-label', 'style'=>'color:red']) !!}
												</div>
											</div>
										
										<div class="row">
											<div class="col-sm-3 ">
												{!! Form::label('special_requirements', 'Special Requirements', ['class' => 'control-label label-formulario']) !!}
											</div>
											<div class="col-sm-9">
												{!! Form::text('special_requirements', Auth::user()->special_requirements, ['class'=>'form-control form-control-input', 'id'=>'special_requirements']) !!}
											</div>
										</div>
										
										<div class="row">
											<div class="col-sm-3">
												{!! Form::label('cc_email', 'CC Email', ['class' => 'control-label label-formulario']) !!}
											</div>
											<div class="col-sm-9">
												{!! Form::input('email', 'cc_email', Auth::user()->cc_email, ['class'=>'form-control form-control-input']) !!}
											</div>
										</div>
										
										<div class="row">
											<div class="form-group">
												<div class="col-sm-3 col-xs-3 text-right">
													{!! Form::label('attendee_types', 'My role is mainly', ['class' => 'control-label label-formulario-dos']) !!}
												</div>
												<div class="col-sm-9 col-xs-9">
													
													@foreach($attendee_types as $key => $at)
														<?php
															$checked = (in_array($key, Auth::user()->attendeeTypes->lists('id'))) ? true : false;
															$hided = ($key <= 11) ? 'style="display:block;"' : '';
														?>
														@if($key <= 11)
														@else
														<div {!!$hided!!} class="checkbox checkbox-styled">
															<label>
																{!! Form::checkbox('attendee_types[]', $key, $checked, ['class'=>'attendee_types check_field', 'required' => 'required']) !!} <span class="label-formulario-dos">{{$at}}</span>
															</label>
														</div>
														@endif
													@endforeach
												</div>
											</div>
										</div>
										
									</div>
								</div>
								
							</div><!--end #step1 -->
							
							@if(sizeof($slot_questions))
							
							<?php $question_id = 0; ?>
							
							<div class="tab-pane" id="step{{$step_id}}">
								<br/><br/>
								
								@foreach($slot_questions as $sq)
									
									@if( (!empty($sq->available) && !isset($amendment)) ||
										(isset($amendment) && ( !empty($sq->available) && isset($slot_questions_answers[$sq->question]) && $sq->available != $slot_questions_answers[$sq->question]) )
										 
									)
									<script type="text/javascript">
										$(document).ready(function(){
											$('#slot_{{$sq->event_schedule_slot_id}}').hide();
										});
									</script>
									@endif
								
									<?php
									$options = array();
									foreach(explode(",", $sq->answers) as $a) {
										$options[trim($a)] = trim($a);
									}
									
									$value = isset($slot_questions_answers[$sq->question]) ? $slot_questions_answers[$sq->question] : "";
									?>
								
								<div class="row">
									<div class="col-xs-5 text-right"><b>{{$sq->question}}</b></div>
									<div class="col-xs-4">
										
										@if($sq->type == 'text')
											{!! Form::hidden('question_'.$question_id, $sq->question) !!}
											{!! Form::text('registration_answer_'.$question_id, $value, ['class'=>'form-control']) !!}
											<?php $question_id++;?>
										@elseif($sq->type == 'option')
											{!! Form::hidden('question_'.$question_id, $sq->question) !!}
											{!! Form::select('registration_answer_'.$question_id, [''=>'Please select answer']+$options, $value, ['class'=>'form-control', 'onChange'=>'if(this.value == "'.$sq->available.'") $("#slot_'.$sq->event_schedule_slot_id.'").show(); else $("#slot_'.$sq->event_schedule_slot_id.'").hide()']) !!}
											<?php $question_id++;?>
										@elseif($sq->type == 'checkbox')
											@foreach($options as $chb)
											{!! Form::hidden('question_'.$question_id, $sq->question) !!}
											<?php $checked = ($value == $chb) ? "checked" : ""; ?>
											<div class="checkbox checkbox-styled">
												<label>
													<input type="checkbox" {{$checked}} onchange="if(this.value == '{{$sq->available}}') $('#slot_{{$sq->event_schedule_slot_id}}').show();" value="{{$chb}}" name="registration_answer_{{$question_id}}">
													{{$chb}}
												</label>
											</div>
											<?php $question_id++;?>
											@endforeach 
										@endif
									</div>
								</div>
								
								@endforeach
							</div>
							
							<?php $step_id++;?>
							@endif
							
							
							@if($event->scheduleTimeSlots()->where('bookable', 1)->count())
							
							<div class="tab-pane" id="step{{$step_id}}">
								<br/><br/>
								
								@if($event->main_schedule_visible_reg)
								@if ($event->id != 680)
								<b>Please select the session(s) you would like to attend:</b>
								<br><br>
								@else
								<b>Please select the session(s) you would like to attend:</b>
								<br>
								<b><span style="color:red">NOTE:</span> You can only select ONE breakfast session per person</b>
								<br><br>
								@endif
								<?php 
$event_days = [];
$date = $event->event_date_from;
while (strtotime($date) <= strtotime($event->event_date_to)) {
	$day = date("Y-m-d", strtotime($date));
	$event_days[] = $day;
	$date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
}
?>

<?php
$user_attendee_types = Auth::user()->attendeeTypes->lists('id');
$bookable_slots_count = 0;
?>

<!-- slots -->
<style>
.tabs .checkbox-styled:not(ie8) input:checked~span:before {
	border-color: #0aa89e !important;
}
</style>

<div class="tabs">
	<ul class="nav nav-tabs dias">
		<?php $first = true; ?>
		@foreach ($event_days as $seq => $day)
			<li class="@if($first) active @endif"><a href="#day{{$day}}" class="dia" data-toggle="tab">DAY {{$seq+1}} - {{date("d/m/Y", strtotime($day))}}</a></li>
			<?php $first = false; ?>
		@endforeach
	</ul>				
	<div class="tab-content">
	<?php $first = true; ?>
		@foreach ($event_days as $day)
			<!--- DAY CONTENT -->
			<div id="day{{$day}}" class="tab-pane @if($first)active @endif">
				
				<table id="sessions" class="table table-striped table-responsive head" cellspacing="0" width="100%">
					<thead class="events">
						<tr>
							<th class="centrar">Time</th>
							<th class="centrar">Name</th>
							<th class="centrar">Book</th>
							@if(isset($amendment))
							<th class="centrar">Status</th>
							@endif
						</tr>
					</thead>
									
					<tbody>
						@foreach($event->scheduleTimeSlots()->where('bookable', 1)->where('stop_booking', 0)->orderBy('start')->get() as $s)
						<?php if ($day != $s->start_date)continue;?>
						<?php
							$available = false;
							$slot_attendee_types = $s->attendeeTypes->lists('id');
							
							foreach($user_attendee_types as $uat) {
								
								// if attendee has at least 1 attende type assigned to slot attendee types
								if(in_array($uat, $slot_attendee_types)) {
									$available = true;
									break;
								}
							}
							
							//check chartered status if attendee type not found
							if(!$available)
								if($s->available_chartered && Auth::user()->chartered) $available = true;
							
							// user doesn't have attendee type that is related with slots then don't give option to register for that slot
							if(!$available) continue;
							
							$bookable_slots_count++;
							
							$s_capacity = $s->slot_capacity;

							$s_attendees = $s->attendees()->whereIn('registration_status_id', [config('registrationstatus.booked'), config('registrationstatus.waitlisted')])->count();
						
							$full = ($s->slot_capacity <= $s->attendees()->whereIn('registration_status_id', [config('registrationstatus.booked'), config('registrationstatus.waitlisted')])->count() || $s->stop_booking) ? true : false;
							
							$checked = "";
							if(isset($booked_slots) && in_array($s->id, $booked_slots)) $checked = "checked";

						?>
						<tr id="slot_{{$s->id}}">
							<td class="centrar">
								{{date("H:i", strtotime($s->start))}} - {{date("H:i", strtotime($s->end))}}
								@if($full)
									@if($event->id == 680)
									<img src="{{asset('assets/site/img/fully_booked.png')}}" width="70" height="22" alt="fully booked">
									@elseif($event->registration_type_id == 1)
									 | <span class="wait-list">Waiting List</span>
									@endif
								@endif
							</td>
							<td class="centrar">{{$s->title}}
								@if($full && $event->id == 680)
									     |   <span class="full-slot-book">Please, choose another session</span>
								@endif</td>
							<td class="centrar"> 
								@if(!$full)
									<div class="form-group">

										@if($event->id == 680 && $s->id == 1193)
											<div class="checkbox checkbox-styled">
												<label>
													<input  type="checkbox" checked="checked" disabled value="{{$s->id}}" required name="sessions[]">
												</label>
											</div>
											<input  type="hidden"   value="{{$s->id}}" required name="sessions[]">
										@elseif($event->id == 680 && $s->id != 1193)
											<div class="checkbox checkbox-styled">
												<label>
													<input  type="checkbox" {{$checked}} value="{{$s->id}}" required name="sessions[]">
												</label>
											</div>
										@endif
										@if(!$event->mandatory_slots && $event->id != 680)
										<div class="checkbox checkbox-styled">
										<label>
											<input  type="checkbox" {{$checked}} value="{{$s->id}}" required name="sessions[]">
										</label>
										</div>
										@elseif($event->mandatory_slots && $event->id != 680)
										<div class="checkbox checkbox-styled">
										<label>
											<input  type="checkbox" checked="checked" disabled value="{{$s->id}}" required name="sessions[]">
										</label>
										</div>
										<input  type="hidden"   value="{{$s->id}}" required name="sessions[]">
										
										
										@endif
									
									</div>
								@elseif($full && $event->id == 680)
									<div class="form-group">
										<div class="checkbox checkbox-styled">
										<label>
											<input  type="checkbox" {{$checked}} value="{{$s->id}}" required name="sessions[]" disabled>
										</label>
										</div>
									</div>
								@elseif($full && $event->registration_type_id == 1)
									<div class="form-group">
										<div class="checkbox checkbox-styled">
										<label>
											<input  type="checkbox" {{$checked}} value="{{$s->id}}" required name="sessions[]" >
										</label>
										</div>
									</div>
										
								@endif
							</td>
							@if(isset($amendment))
								<td>
								@if(sizeof($booked_slots_full->where('event_schedule_slot_id', $s->id)->first()) > 0)
								{{$booked_slots_full->where('event_schedule_slot_id', $s->id)->first()->status->title}}
								@else
								Not booked
								@endif
								</td>
							@endif
						</tr>
						@endforeach										
					</tbody>
				</table>
			</div><!---- ENDDAY CONTENT -->
			<?php $first = false; ?>		
		@endforeach <!-- ($event_days as $day) -->
	</div> <!-- class="tab-content"-->
</div>	<!-- class="tabs" -->
<!-- FIN SLTOS -->

								@if($bookable_slots_count == 0)
									<br/><i>This event is available just for selected attendee types.</i>
									<div style="display: none;">
										<input type="text" name="empty-sessions" required />
									</div>
								@endif
								@endif
							</div>
							<?php $step_id++; ?>
							@elseif($event->schedule()->count() && $event->schedule->visible_registration)
								<div class="tab-pane" id="step{{$step_id}}">
								{!! $event->schedule->schedule !!}
								</div>
								<?php $step_id++; ?>
							@endif
							
							<!--Show all steps that are assigned for event type -->
							
							<?php $dyn_question_id = 0; ?>
							@if($event->id == 680 && $user_member_type == 0)
								@foreach($event->eventType->typeSteps as $step)
								
								<div class="tab-pane" id="step{{$step_id}}">
									<br/><br/>
									@if(!empty($step->form->form_json))
									<div class="row">
										<div class="col-xs-12 text-center"><h3>{{$step->form->form_title}}</h3></div>
									</div>

									@foreach(json_decode($step->form->form_json) as $field)
									
									<?php
										$required = ($field->required) ? "required" : "";
										$checked = isset($slot_questions_answers[$field->label]) ? "checked": "";
										$value = isset($slot_questions_answers[$field->label]) ? $slot_questions_answers[$field->label] : "";
									?>
									
									<div class="row">
										<div class="col-sm-4 text-right">{{$field->label}}</div>
										
										<div class="col-sm-6">
											<div class="form-group">
												@if($field->field_type == "checkboxes")
												<?php
													$options = array();
													foreach($field->field_options->options as $opt) {
														$options[$opt->label] = $opt->label;
													}
												?>
												
												@foreach($options as $o)
													<?php $checked = (isset($slot_questions_answers[$field->label]) && ($slot_questions_answers[$field->label] == $o)) ? "checked" : ""; ?>
													<input type="hidden" name="dyn_question_{{$dyn_question_id}}" value="{{$field->label}}" />
													<div class="checkbox checkbox-styled">
														<label>
															<input type="checkbox" value="{{$o}}" {{$checked}} name="dyn_answer_{{$dyn_question_id}}">
															{{$o}}
														</label>
													</div>

													<?php $dyn_question_id++;?>
												@endforeach
												@elseif($field->field_type == "dropdown")
													<input type="hidden" name="dyn_question_{{$dyn_question_id}}" value="{{$field->label}}" />
													<?php
														$options = array();
														foreach($field->field_options->options as $opt) {
															$options[$opt->label] = $opt->label;
														}
													?>
													@if($event->id == 680 && $user_member_type == 0)
														{!! Form::select('dyn_answer_'.$dyn_question_id, $options, $value, ['class'=>'form-control', 'required' => 'required']) !!}
													@else
														{!! Form::select('dyn_answer_'.$dyn_question_id, [''=>'']+$options, $value, ['class'=>'form-control', $required]) !!}
													@endif


													<?php $dyn_question_id++;?>
												@elseif($field->field_type == "radio")
													<input type="hidden" name="dyn_question_{{$dyn_question_id}}" value="{{$field->label}}" />
													<?php
														$options = array();
														foreach($field->field_options->options as $opt) {
															$options[$opt->label] = $opt->label;
														}
													?>
													@foreach($options as $o)
														<?php $checked = (isset($slot_questions_answers[$field->label]) && ($slot_questions_answers[$field->label] == $o)) ? "checked" : ""; ?>
													
														<input {{$checked}} {{$required}} type="radio" name="dyn_answer_{{$dyn_question_id}}" value="{{$o}}" /> {{$o}}<br/>
													@endforeach
														<?php $dyn_question_id++;?>
												@else
													<input type="hidden" name="dyn_question_{{$dyn_question_id}}" value="{{$field->label}}" />
													{!! Form::text('dyn_answer_'.$dyn_question_id, $value, ['class'=>'form-control', $required]) !!}
													<?php $dyn_question_id++;?>
												@endif
											</div>
										</div>
									</div>
									@endforeach
									@endif
									
								</div><!--end #step 4 -->
								<?php $step_id++; ?>
								@endforeach
								@endif

							<ul class="pager wizard border-t" id="estep<?=$step_id ?>">
							    <li class="next"><a class="btn boton" href="javascript:void(0);">Next</a></li>
								<li class="previous"><a class="btn boton" href="javascript:void(0);">Previous</a></li>
							</ul>
							
							<div class="tab-pane" id="step<?=$step_id ?>">
							    <!-- <ul class="pager wizard">
							        <li class="next"><a class="btn boton derecho" href="javascript:void(0);">Next</a></li>
							    	<li class="previous"><a class="btn boton izquierdo" href="javascript:void(0);">Previous</a></li>
							    </ul> -->
								<br/><br/>
								<div class="row margen-b">
									<div class="col-sm-3"><b>Event name:</b></div>
									<div class="col-sm-7">{{$event->title}}</div>
								</div>
								
								<div class="row margen-b">
									<div class="col-sm-3"><b>Location:</b></div>
									<div class="col-sm-7">@if(sizeof($event->venue)){{$event->venue->name}}, {{$event->venue->address}}, {{$event->venue->city}}, {{$event->venue->county}}, {{$event->venue->postcode}}@endif</div>
								</div>
								
								<div class="row margen-b">
									<div class="col-sm-3"><b>Event date:</b></div>
									<div class="col-sm-7">
										@if ($event->event_date_from == $event->event_date_to)
											{{ date('l d M Y',strtotime($event->event_date_from)) }}
										@else
											{{ date('d',strtotime($event->event_date_from)) }}-{{ date('d M Y',strtotime($event->event_date_to)) }}
										@endif
									</div>
								</div>
								
								<div class="row margen-b">
									<div class="col-sm-3"><b>Dietary requirements:</b></div>
									<div class="col-sm-7" id="summary_dietary_requirements"></div>
								</div>
								
								<div class="row margen-b">
									<div class="col-sm-3"><b>Special requirements:</b></div>
									<div class="col-sm-7" id="summary_special_requirements"></div>
								</div>
								
								<div class="row">
									<div class="col-sm-3"><b>Booked sessions:</b></div>
									<div class="col-sm-7" id="summary_booked_sessions"></div>
								</div>
								
								<div class="row">
									<div class="col-sm-3"><b>Partner communications</b></div>
									<div class="col-xs-3 col-sm-1">
										<div class="form-group">
											<input type="radio" name="sponsors_contact" value="1" required @if (isset($event_attendee) && $event_attendee->pivot->sponsors_contact) checked @endif /> Yes <br>
											<input type="radio" name="sponsors_contact" value="0" @if (isset($event_attendee) && !$event_attendee->pivot->sponsors_contact) checked @endif /> No
										</div>
									</div>
									<div class="col-xs-9 col-sm-7">
										These CPD events are made possible by working with the support of ‘Partners in Professionalism’. 
										Please confirm if you are happy to be contacted by the participating Partners in relation to business relevant support arising from the subject matter of this event
									</div>
								</div>
								
								<div class="row">
                                    <div class="col-sm-3"><b>First time attendee?</b></div>
                                    <div class="col-xs-3 col-sm-1">
                                        <div class="form-group">
                                            <input type="radio" name="first_time_attendee" value="1" required @if (isset($event_attendee) && $event_attendee->pivot->first_time_attendee) checked @endif /> Yes <br>
                                            <input type="radio" name="first_time_attendee" value="0" @if (isset($event_attendee) && !$event_attendee->pivot->first_time_attendee) checked @endif /> No
                                        </div>
                                    </div>
                                    <div class="col-xs-9 col-sm-7">
                                        Is this the first time you have attended a PFS event?
                                    </div>
                                </div>
								
								@if($event->invitation_allowed)
								<div class="row">
									<div class="col-sm-3"><b>Invite a colleague</b></div>
									<div class="col-sm-7">
										If you would like to send details of this event to a colleague please enter their email address <input style="margin: 15px 0px;" type="text" name="invitees" placeholder="Email" class="form-control form-control-input" />
										<p class="help-block">To enter multiple email addresses, please separate each address with a comma</p> 
										<!--<button type="button" class="btn btn-xs btn-success" id="add_invitee">+</button>-->
									</div>
									<!--<div class="col-sm-offset-4 col-sm-4 col-xs-8 col-xs-offset-2" id="invites"></div>-->
								</div>
								@endif
								
								<div class="row">
									<div class="col-sm-3"><b>Terms and conditions</b></div>
									<div class="col-sm-7">
										@if(!empty($event->eventType->terms_conditions))
											@if($event->price > 0.00 && $user_member_type == 1)
												{!!$event->eventType->terms_conditions!!}
												@include('events.termAndConditonsAxcess')
											@elseif($event->price_nonmember > 0.00 && $user_member_type == 0 && $event->id == 680)
												@include('events.termAndConditonsFutureproof')
												{!!$event->eventType->terms_conditions!!}
											@elseif($user_member_type == 1 && $event->id == 680)
												@include('events.memberTermAndConditonsFutureproof')
											@elseif($event->price_nonmember > 0.00 && $user_member_type == 0 && $event->id != 680)
												@include('events.termAndConditonsAxcess')
												{!!$event->eventType->terms_conditions!!}
											@else
												{!!$event->eventType->terms_conditions!!}
											@endif											
										@else
											@if($event->price > 0.00 && $user_member_type == 1)
												{!!$event->eventType->terms_conditions!!}
												@include('events.termAndConditonsAxcess')
											@elseif($event->price_nonmember > 0.00 && $user_member_type == 0 && $event->id == 680)
												@include('events.memberTermAndConditonsFutureproof')
												{!!$event->eventType->terms_conditions!!}
											@elseif($user_member_type == 1 && $event->id == 680)
												@include('events.memberTermAndConditonsFutureproof')
											@elseif($event->price_nonmember > 0.00 && $user_member_type == 0 && $event->id != 680)
												@include('events.termAndConditonsAxcess')
												{!!$event->eventType->terms_conditions!!}
											@else
												{!!$event->eventType->terms_conditions!!}
											@endif			
										@endif
									</div>
								</div>
									
									
								<div class="cols-xs-12 text-right">
									<div class="checkbox checkbox-styled">
										<label>
											{!! Form::checkbox('policy', 1, false, ['id' => 'policy']) !!}
											I agree with terms & conditions
										</label>
									</div>
									<span id="condition_error" class="condition_error"></span>
									<br/><br/>
									
									<ul class="pager wizard">
									    <li class="next pull-right">
											@if(isset($amendment))
												@if($event->price > 0.00 && $user_member_type == 1)
													{!! Form::submit('Payment »', ['class' => 'btn', 'id'=>'complete_registration']) !!}
												@elseif($event->price_nonmember > 0.00 && $user_member_type == 0)
													{!! Form::submit('Payment »', ['class' => 'btn', 'id'=>'complete_registration']) !!}
												@else
													{!! Form::submit('Amend registration »', ['class' => 'btn', 'id'=>'complete_registration']) !!}
												@endif			
											@else
												@if($event->price > 0.00 && $user_member_type == 1)
													{!! Form::submit('Payment »', ['class' => 'btn', 'id'=>'complete_registration']) !!}
												@elseif($event->price_nonmember > 0.00 && $user_member_type == 0)
													{!! Form::submit('Payment »', ['class' => 'btn', 'id'=>'complete_registration']) !!}
												@else
													{!! Form::submit('Register »', ['class' => 'btn', 'id'=>'complete_registration']) !!}
												@endif		
											@endif
											<h4 id="message-submit" style="color: #821e3f; display:none">Please wait...</h4>
										</li>
										<li class="previous"><a class="btn boton" href="javascript:void(0);">Previous</a></li>
									</ul>
								</div>
							</div><!--end #step4 -->
						</div><!--end .tab-content -->
						{!! Form::close() !!}
				</div><!--end #rootwizard -->
			</div><!--end .card-body -->
		</div><!--end .card -->
	</div><!--end .col -->
</div><!--end .row -->
<!-- END VALIDATION FORM WIZARD -->
</div>

<script type="text/javascript">
var check_select = 0;
$("#add_invitee").click(function(){
	var html = '<input type="text" name="invitees[]" placeholder="Invitee email" class="form-control" />';
	
$("#invites").append(html);
});

$('.attendee_types').change(function(){
	
	$.ajax({
		url: "{{ route('events.book.updateAttendeeTypes') }}",
		type: "POST",
		data: $("#reg-form").serialize(),
		success: function () {
			location.reload();
		}
	});
	
});
jQuery(function($) {
jQuery('#rootwizard2').bootstrapWizard({
	'nextSelector':'.next',
	'previousSelector':'.previous',
	'onTabShow':function(tab, navigation, index) {
		var $total = navigation.find('li').length;
		var $current = index+0.5;
		var $percent = ($current/($total-0.06)) * 100;
        var tabs = $(".tab-pane");
        var currentTab = tabs[index - 1];
        var inputs = $(":input, select");
        var stepElements = $(currentTab).find(inputs);
        var count = stepElements.length;
		console.log(count);
		if (count <= 0) {
			$('input[type=checkbox].check_field').each(function( index ) {
			if($( this ).prop('checked') == true) { 
				check_select = 1;
				// console.log( "hay seleccionado" + check_select);  
				}
			});
        }
		$('#rootwizard2').find('.progress-bar').css({width:$percent+'%'});
	},
	'onNext': function (tab, navigation, index) {
        var isValid = false;
        var hadError = false;
        var tabs = $(".tab-pane");
        var currentTab = tabs[index - 1];
        var inputs = $(":input, select");
        var stepElements = $(currentTab).find(inputs);
        var count = stepElements.length;
        if (check_select != 1) {
			alert('You must select what is your role to continue');
        }
        if (count <= 0) {
            return true;
        }
        $(stepElements).each(function (idx) {
            isValid = $(document.forms[0]).validate({
        ignore: [],
        // any other options and/or rules
    	}).element($(this));
            if (!isValid) { hadError = true; }
        });

        if (hadError == false) {


            if (index == navigation.find('li').length) {
                $("form").submit();
            }

        }
		
        return !hadError;
    },
	'onTabClick':function(tab, navigation, index) {return false;},'class':'tabs'});
});
</script>

<script type="text/javascript">

	// $(document).ready(function() {
	// 	$('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
 //        var $target = $(e.target);    
 //        if ($target.parent().hasClass('disabled')) {
 //            return false;
 //        }
 //    });

	$(".btn-raised").click(function(){
		
		$("#summary_booked_sessions").html("");
		
		$("input[name='sessions[]']").each(function(){
			
			 if($(this).is(":checked")) {
			 	
			 	// get information about each selected session and display in the summary
			 	$.ajax({
			 		url: "../getsessiondetails/"+$(this).val(),
			 		type: "GET",
			 		success: function(response){
			 			$("#summary_booked_sessions").append(response +'<br/>');
			 		}
			 	});
			 	
			 }
		});
		
		$("#summary_dietary_requirements").html($("#dietary_requirements option:selected").text());
		$("#summary_special_requirements").html($("#special_requirements").val());
	});

	$('#complete_registration').bind('click', function(){

		if($("#policy").is(':checked')) {
			$('#condition_error').html('');
			return true;
		} else {
			$('#condition_error').html('To complete your registration please tick to confirm you understand and accept the terms and conditions.');
			return false;
		}
	});
	$(function()
	{
		$('#reg-form').submit(function(){
			$("input[type='submit']", this)
			.val("Please Wait...")
			.attr('disabled', 'disabled')
			.hide()
			$("#message-submit").show()
			return true;
		});
	});
	
	// dietary requirements other additional input
	if($('#dietary_requirement_id').val() != 13) $('#dietary_other').hide();
    
    $('#dietary_requirement_id').bind('change', function(event){
    	if(this.value == 13) {
    		$('#dietary_other').show();
    		$("#dietary_requirement_other").prop("required", true);
    	} else {
    		$('#dietary_other').hide();
    		$("#dietary_requirement_other").prop("required", false);
    	}
    });

	// dietary requirements other additional input
	if($('#dietary_requirement_id').val() != 12) $('#dietary_kosher').hide();
    
    $('#dietary_requirement_id').bind('change', function(event){
    	if(this.value == 12) {
    		$('#dietary_kosher').show();
    		$("#dietary_requirement_kosher").show();;
    	} else {
    		$('#dietary_kosher').hide();
    		$("#dietary_requirement_kosher").hide();;
    	}
    });
</script>

<script type="text/javascript" src="{{asset('assets/site/js/libs/wizard/jquery.bootstrap.wizard.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/site/js/libs/jquery-validation/dist/jquery.validate.min.js')}}"></script>
<script src="{{asset('assets/site/js/libs/wizard/jquery.bootstrap.wizard.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/site/js/core/source/AppForm.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/site/js/core/demo/DemoFormWizard.js')}}"></script>

@endsection