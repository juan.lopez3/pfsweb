@extends('layouts.default')
@section('title'){{$event->title}} @endsection
@section('description'){{$event->description}} @endsection

@section('content')
<link type="text/css" rel="stylesheet" href="{{asset('assets/site/css/theme-default/libs/select2/select2.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/site/css/theme-default/libs/wizard/wizard.css')}}" />
<script src="{{asset('assets/admin/fancybox/source/jquery.fancybox.js')}}"></script>
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/fancybox/source/jquery.fancybox.css')}}" />
<script src="{{\Config::get('paymentgateway.url')}}/v1/paymentWidgets.js?checkoutId={{$axcess_parameters->id}}"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('.fancybox').fancybox();
});
</script>
<?php
	$member = (Auth::user()->role_id == role('member')) ? true : false;
?>



<div class="row">
		@if(!empty($event->eventType->branding_image))
	    <div class="col-md-12">
			<div class="branding-cover" style="background: url('{{Config::get("app.url")."uploads/branding/".$event->eventType->branding_image}}') #F7F7F7; background-size: cover;">
	    @else
	    <div class="col-md-12">
			<div class="branding-cover" style="background: #F7F7F7">
	    @endif
	    
	    <h1 class="event-title">{{$event->title}}<br><!--<span><b>{{ date("l j F Y",strtotime($event->event_date_from)) }}</b></span>--></h1>
	    <div class="clearfix"></div>
	    </div>
	</div>
</div>
@include('partials.validationErrors')

<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-body ">
				{!! $event->registration_text !!}
			
				<div id="rootwizard2" class="form-wizard form-wizard-horizontal">
					<div class="form-wizard-nav">
						<div class="progress"><div class="progress-bar progress-bar-primary"></div></div>
						<ul class="nav nav-justified">
							<?php $step_id = 1; ?>
							
							<li><a href="{{route('events.editRegistration', $event->slug)}}" data-togglex="tab"><span class="step">{{$step_id}}</span> <span class="title">CONFIRM DETAILS</span></a></li>
							<?php $step_id++;?>
							
							<!--  -->
							
							@if($event->main_schedule_visible_reg && $event->scheduleTimeSlots()->where('bookable', 1)->count())
							<li class=""><a href="{{route('events.editRegistration', $event->slug)}}" data-togglex="tab"><span class="step">{{$step_id}}</span> <span class="title">SELECT SESSIONS</span></a></li>
							<?php $step_id++;?>
							@endif
							
							@foreach($event->eventType->typeSteps as $step)
							<li class=""><a href="{{route('events.editRegistration', $event->slug)}}" data-togglex="tab"><span class="step">{{$step_id}}</span> <span class="title">{{$step->title}}</span></a></li>
							<?php $step_id++; ?>
							@endforeach
							
							<li class=""><a href="{{route('events.editRegistration', $event->slug)}}" data-togglex="tab"><span class="step">{{$step_id}}</span> <span class="title">CONFIRM & REGISTER</span></a></li>
							<?php $step_id++; ?>

							<li class="active"><a href="#" data-togglex="tab"><span class="step">{{$step_id}}</span> <span class="title">PAYMENT</span></a></li>
						</ul>
					</div><!--end .form-wizard-nav -->
					<!-- 
					<div class="checkbox checkbox-styled">
						<label>
								{!! Form::checkbox('policy', 1, false, ['id' => 'policy']) !!}
								I agree with <a href="http://www.thepfs.org/about/terms-and-conditions/">Terms &amp; conditions</a>
						</label>
					</div> -->
					<br>
					<div class="row">
					<div class="tab-pane" id="step{{$step_id}}">
						<div class="col-xs-9">
						<b>Payment details:</b>
						<br>
							<label>
								<i>Payment for the event <b>{{$event->title}}</b>, price £<b>{{$price}}</b></i>
							</label>	
						</div>
						</div>
					</div>
					<br>

					<!-- BEGIN VALIDATION FORM WIZARD -->

						<div class="tab-pane" id="step1">
							<b>Axcess Payment Gateway</b>
							<form action="{{ route('payment.checkout') }}" class="paymentWidgets" data-brands="VISA MASTER AMEX" id="pay_axcess"></form>
						</div><!--end #step5 -->
					<br>
					<!-- END VALIDATION FORM WIZARD -->
						@if($event->id != 680)
							<div class="col-sm-12 text-right">	
								<a href="{{route('profile.myEvents')}}">Back to my profile »</a>
							</div>
						@endif
					<br>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="{{asset('assets/site/js/libs/wizard/jquery.bootstrap.wizard.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/site/js/libs/jquery-validation/dist/jquery.validate.min.js')}}"></script>
<script src="{{asset('assets/site/js/libs/wizard/jquery.bootstrap.wizard.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/site/js/core/source/AppForm.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/site/js/core/demo/DemoFormWizard.js')}}"></script>


<script type="text/javascript">
$('#wpwl-button').bind('click', function(){
		
		if($("#policy").is(':checked')) {
			$('#condition_error').html('');
			return true;
		} else {
			$('#condition_error').html('To complete your registration please tick to confirm you understand and accept the terms and conditions.');
			return false;
		}
	});
	var wpwlOptions = {
    billingAddress: {},
    mandatoryBillingFields:{
        country: true,
        state: true,
        city: true,
        postcode: true,
        street1: true,
        street2: false
    }
}
</script>

@endsection