<style>
	.content {
		font: 13px/1.231 arial, helvetica, clean, sans-serif;*
	}
</style>
<div class="content">
	<p><strong>Who will take payments:</strong></p>
	<p>Transactions for this event will be collected and managed by TFI Group Limited.&nbsp;<br /><br /></p>
	<p>Your payment will appear on your bank statement as TFI Group Ltd. TFI Group Ltd is a UK registered entity.&nbsp;<br />Companies House number 01845643. Address: 192 Vauxhall Bridge Road,&nbsp;SW1V 1DX London, UK</p>
	<p>E:&nbsp;<span style="color: #ff0000;"><a style="color: #ff0000;" href="mailto:events@thepfs.org">events@thepfs.org</a></span>&nbsp;| T: +44(0) 207 808 5616<br /><br /></p>
	<p><strong>Amending Your Reservation:</strong></p>
	<p>Demand for places at Power Live events remains high. If for any reason you are no longer able to attend please let us know so that we can re-allocate your place.</p>
	<p>If you have any further questions please take a look at our&nbsp;<span style="color: #ff0000;"><a style="color: #ff0000;" href="https://events.thepfs.org/public/emails/192887/shanur.islam@tfigroup.com/redirect?url=https://events.thepfs.org/public/event/power-live-the-future-of-financial-planning/view" data-saferedirecturl="https://www.google.com/url?q=https://events.thepfs.org/public/emails/192887/shanur.islam@tfigroup.com/redirect?url%3Dhttps://events.thepfs.org/public/event/power-live-the-future-of-financial-planning/view&amp;source=gmail&amp;ust=1562679308588000&amp;usg=AFQjCNGlkbil_9a7Gi2L3VMptFEbLyp4QA">frequently&nbsp;asked questions</a></span>.<br /><br /></p>
	<p><strong>Cancelling your reservation:</strong></p>
	<p>All cancellations must be received in writing at&nbsp;<span style="color: #ff0000;"><a style="color: #ff0000;" href="mailto:power@pfsevents.org">power@pfsevents.org</a></span></p>
	<p>All cancellations will receive a &pound;20 administration charge.</p>
	<p>If the cancellation is received 30-14 days&nbsp;prior to the event, then&nbsp;25% cancellation fee&nbsp;will be incurred.</p>
	<p>If the cancellation is received less than 14 days&nbsp;notice then no refunds&nbsp;will be provided&nbsp;</p>
	<p>If you fail to attend the conference please note that&nbsp;no refunds&nbsp;will be considered.<br /><br /></p>
	<p><strong>Substitutions</strong>&nbsp;can be made at any time prior to the conference without charge.</p>
	<p>If you wish to amend/cancel your booking please&nbsp;click here.&nbsp;</p>
	<p>&nbsp;</p>
</div>