<style>
.tabs .checkbox-styled:not(ie8) input:checked~span:before {
	border-color: #0aa89e !important;
}
</style>

<div class="tabs">
	<ul class="nav nav-tabs">
		<?php $first = true; ?>
		@foreach ($event_days as $seq => $day)
			<li class="@if($first) active @endif"><a href="#day{{$day}}" data-toggle="tab">DAY {{$seq+1}} - {{date("d/m/Y", strtotime($day))}}</a></li>
			<?php $first = false; ?>
		@endforeach
	</ul>				
	<div class="tab-content">
	<?php $first = true; ?>
		@foreach ($event_days as $day)
			<!--- DAY CONTENT -->
			<div id="day{{$day}}" class="tab-pane @if($first)active @endif">
				
				<table id="sessions" class="table table-striped table-responsive head" cellspacing="0" width="100%">
					<thead class="events">
						<tr>
							<th>Time</th>
							<th>Name</th>
							<th>Book</th>
							@if(isset($amendment))
							<th>Status</th>
							@endif
						</tr>
					</thead>
									
					<tbody>
						@foreach($event->scheduleTimeSlots()->where('bookable', 1)->where('stop_booking', 0)->orderBy('start')->get() as $s)
						<?php if ($day != $s->start_date)continue;?>
						<?php
							$available = false;
							$slot_attendee_types = $s->attendeeTypes->lists('id');
							
							foreach($user_attendee_types as $uat) {
								
								// if attendee has at least 1 attende type assigned to slot attendee types
								if(in_array($uat, $slot_attendee_types)) {
									$available = true;
									break;
								}
							}
							
							//check chartered status if attendee type not found
							if(!$available)
								if($s->available_chartered && Auth::user()->chartered) $available = true;
							
							// user doesn't have attendee type that is related with slots then don't give option to register for that slot
							if(!$available) continue;
							
							$bookable_slots_count++;
							
							$full = ($s->slot_capacity <= $s->attendees()->whereIn('registration_status_id', [config('registrationstatus.booked'), config('registrationstatus.waitlisted')])->count() || $s->stop_booking) ? true : false;
							
							$checked = "";
							if(isset($booked_slots) && in_array($s->id, $booked_slots)) $checked = "checked";

						?>
						
						<tr id="slot_{{$s->id}}">
							<td>
								{{date("H:i", strtotime($s->start))}} - {{date("H:i", strtotime($s->end))}}
								@if($full)
								<span class="full-slot-book">Waiting list</span>
								@endif
							</td>
							<td>{{$s->title}}</td>
							<td>
								<div class="form-group">

								
									@if(!$event->mandatory_slots)
									<div class="checkbox checkbox-styled">
									<label>
										<input  type="checkbox" {{$checked}} value="{{$s->id}}" required name="sessions[]">
									</label>
									</div>
									@else
									<div class="checkbox checkbox-styled">
									<label>
										<input  type="checkbox" checked="checked" disabled value="{{$s->id}}" required name="sessions[]">
									</label>
									</div>
									<input  type="hidden"   value="{{$s->id}}" required name="sessions[]">
									
									
									@endif
								
								</div>
							</td>
							@if(isset($amendment))
								<td>
								@if(sizeof($booked_slots_full->where('event_schedule_slot_id', $s->id)->first()) > 0)
								{{$booked_slots_full->where('event_schedule_slot_id', $s->id)->first()->status->title}}
								@else
								Not booked
								@endif
								</td>
							@endif
						</tr>
						@endforeach										
					</tbody>
				</table>
			</div><!---- ENDDAY CONTENT -->
			<?php $first = false; ?>		
		@endforeach <!-- ($event_days as $day) -->
	</div> <!-- class="tab-content"-->
</div>	<!-- class="tabs" -->