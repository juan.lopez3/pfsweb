@extends('layouts.default')
@section('title'){{$event->title}} @endsection
@section('description'){{$event->description}} @endsection

@section('content')
<link type="text/css" rel="stylesheet" href="{{asset('assets/site/css/theme-default/libs/select2/select2.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/site/css/theme-default/libs/wizard/wizard.css')}}" />

<?php
	$member = (Auth::user()->role_id == role('member')) ? true : false;
?>
<div class="wrapper">
@include('partials.top')
    <div class="row">
    		@if(!empty($event->eventType->branding_image))
    	    <div class="col-md-11">
    			<div class="branding-cover" >
    	    @else
    	    <div class="col-md-11">
    			<div class="branding-cover">
    	    @endif
    
    	    <h1 class="event-title evento-titulo borde">{{$event->title}}</h1>
    	    <div class="clearfix"></div>
    	    </div>
    	</div>
    </div>
    
    @include('partials.validationErrors')
    
    <h3 class="titulo-login">Feedback form</h3><br>
    	{!! Form::open(['route' => ['events.feedback.store', $event->slug], 'class' => 'form form-validate']) !!}
    @if(sizeof($event_feedback))
    	<div class="row">
    		<div class="col-xs-12">
    			<div class="gradient gr_sm gr-pad titulo-Programme">
    				General feedback on the event
    			</div>
    		</div>
    	</div>
    @endif
    
    @foreach ($event_feedback as $index => $fb)
    	<?php
    	$user_answer = $user_feedback->where('feedback_id', $fb->id)->first();
    	$user_answer = sizeof($user_answer) ? $user_answer->answer : null;
    	?>
    	<div class="row feedback-row">
    		<div class="col-md-5">
    			<b>{!!$fb->question!!}</b>
    		</div>
    		<div class="col-md-7">
    		@if($fb->type == 1)
    			<!-- Rating -->
    				@foreach(explode(";", $fb->answer) as $answer)
    					<input type="radio" name="{{$fb->id}}" value="{{$answer}}" @if ($user_answer == $answer)checked @endif /> {{$answer}} &nbsp;&nbsp;&nbsp;
    				@endforeach
    				<input type="radio" name="{{$fb->id}}" value="" /> n/a
    			@elseif($fb->type == 2)
    			<!-- ch/box -->
    				@foreach(explode(";", $fb->answer) as $answer)
    					{!! Form::checkbox($fb->id."[]", $answer, ($user_answer == $answer)) !!} {{$answer}}
    				@endforeach
    			@elseif($fb->type == 3)
    			<!--  option -->
    				@foreach(explode(";", $fb->answer) as $answer)
    					<input type="radio" name="{{$fb->id}}" value="{{$answer}}" @if ($user_answer == $answer)checked @endif /> {{$answer}} &nbsp;&nbsp;&nbsp;
    				@endforeach
    			@elseif($fb->type == 4)
    			<!-- select -->
    				{!! Form::select($fb->id, explode(";", $fb->answer), $user_answer, ['class'=>'form-control']) !!}
    			@elseif($fb->type == 5)
    			<!-- text -->
    				{!! Form::textarea($fb->id, $user_answer, ['rows'=>3, 'class'=>'form-control form-control-input']) !!}
    			@endif
    		</div>
    	</div>
    	<hr />
    @endforeach
    
    <br/><br/>
    
    <div class="row">
        <div class="col-xs-12">
            <div class="gradient gr_sm gr-pad titulo-Programme">
    			Feedback on the Festival sessions
            </div>
        </div>
    </div>
    @foreach($event->scheduleSessions()->orderBy('session_start')->has('feedbacks')->with('feedbacks', 'contributors')->get() as $session)
    
    	<div class="row">
    	    <div class="col-md-3">
    	        {{date("H:i", strtotime($session->session_start))}} - {{date("H:i", strtotime($session->session_end))}} {!!$session->title!!} <br/>
                <div style="line-height: 17px;">
                @foreach($session->contributors as $contributor)
                {{$contributor->first_name}} {{$contributor->last_name}}, {{$contributor->company}} <br/>
                @endforeach
                </div>
    	    </div>
    
    	    <div class="col-md-9">
    	        @foreach ($session->feedbacks as $index => $fb)
    				<?php
    					$user_answer = $user_feedback->where('feedback_id', $fb->id)->first();
                    	$user_answer = sizeof($user_answer) ? $user_answer->answer : null;
    				?>
                    <div class="row">
                        <div class="col-sm-3">{!!$fb->question!!}</div>
                        <div class="col-sm-9">
                            @if($fb->type == 1)
                            <!-- Rating -->
                                @foreach(explode(";", $fb->answer) as $answer)
                                <input type="radio" name="{{$fb->id}}" value="{{$answer}}" @if ($user_answer == $answer)checked @endif /> {{$answer}} &nbsp;&nbsp;&nbsp;
                                @endforeach
                                <input type="radio" name="{{$fb->id}}" value="" /> n/a
                            @elseif($fb->type == 2)
                            <!-- ch/box -->
                                @foreach(explode(";", $fb->answer) as $answer)
                                    {!! Form::checkbox($fb->id."[]", $answer, ($user_answer == $answer)) !!} {{$answer}}
                                @endforeach
                            @elseif($fb->type == 3)
                            <!--  option -->
                                @foreach(explode(";", $fb->answer) as $answer)
                                    <input type="radio" name="{{$fb->id}}" value="{{$answer}}" @if ($user_answer == $answer)checked @endif /> {{$answer}} &nbsp;&nbsp;&nbsp;
                                @endforeach
                            @elseif($fb->type == 4)
                            <!-- select -->
                                {!! Form::select($fb->id, explode(";", $fb->answer), $user_answer, ['class'=>'form-control']) !!}
                            @elseif($fb->type == 5)
                            <!-- text -->
                                {!! Form::textarea($fb->id, $user_answer, ['rows'=>3, 'class'=>'form-control form-control-input']) !!}
                            @endif
                        </div>
                    </div>
                @endforeach
    	    </div>
    	</div>
    	<hr />
    @endforeach
    
    <div class="row" style="margin-left:0px;">
        <p>Thank You for completing this form – your feedback helps us to continually improve the quality of our events.</p>
    </div>
    
    <div class="row>">
    	<div class="form-group pull-right">
    		<br/>
    		{!! Form::submit('Leave feedback ', ['class' => 'btn boton btn-feedback']) !!}
    	</div>
    </div>
    
    <div class="row">
    	<div class="col-md-12 col-sm-12 col-xs-12">
        </div>
    </div>
    
    {!! Form::close() !!}
</div>

<style type="text/css">
     div.feedback {padding: 5px; font-size: 19px;}
</style>
@endsection