<style>
	.content {
		font: 13px/1.231 arial, helvetica, clean, sans-serif;*
	}
</style>
<div class="content">
	<H4><b>Who will take payments:</b></H4>
		<p>Transactions for this event will be collected and managed by TFI Group Limited. Your payment will appear on your bank statement as TFI Group Ltd, 020 7233 5644.</p>
		<p>TFI Group Ltd is a UK registered entity. Companies House number 01845643.</p>
		<p>192 Vauxhall Bridge Road, SW1V 1DX London, UK</p>
		<p>E: events@thepfs.org | T: +44(0) 207 808 5616</p>

	<H4><b>Return / Cancellation Policy</b></H4>
		<!-- <p>All cancellations must be received in writing at regionals@pfsevents.org.</p>
		<p>All cancellations will receive a £20 administration charge.</p>
		<p>Before 15th May at 23:59 UK Time, a full refund will be given minus the administration charge;</p>
		<p>Between 16th May and 16th June at 23:59 UK Time, a 50% refund will be given minus the administration charge;</p>
		<p>From 17th June, no refund will be given. </p>
		<p>Name changes can be made at any point without charge.</p> -->

	<!-- <H4><b>Late Bookings</b></H4>
		<p>If you register within 2 working days of an event your name may not appear on the delegate list / signing in sheet. Please print off and bring your confirmation email with you to the Conference as proof of your booking.</p>
		<p>There is high demand for places at all conferences, so if you are no longer able to attend, please cancel your registration as soon as possible so that we have the opportunity to let another delegate attend. If we do not receive your cancellation 2 working days before the event, you will incur a £50.00 non-attendance fee.</p> -->
</div>