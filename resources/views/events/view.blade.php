@extends('layouts.default')
@section('title'){{$event->title}} @endsection
@section('description'){{$event->description}} @endsection

@section('content')
@if (isset($event) && $event->theme_id > 1)
@include('themecss')
@endif
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyCFo1QWcA_GRpH7Q_jV14HF2PJ_3qzuiB8"></script>
<script src="{{asset('assets/admin/fancybox/source/jquery.fancybox.js')}}"></script>
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/fancybox/source/jquery.fancybox.css')}}" />

<script type="text/javascript">
$(document).ready(function() {
	/*
	 *  Simple image gallery. Uses default settings
	 */

	$('.fancybox').fancybox();
	/*
	$('a[href^="#"]').on('click',function (e) {
	    e.preventDefault();

	    var target = this.hash;
	    var $target = $(target);

	    $('html, body').stop().animate({
	        'scrollTop': $target.offset().top
	    }, 900, 'swing', function () {
	        window.location.hash = target;
	    });
	});
*/
});
</script>
<div class="wrapper">
   <div class="row">
   		@if(!empty($event->eventType->branding_image))
   	    <div class="col-md-12">
   			<div class="branding-cover">
   	    @else
   	    <div class="col-md-12">
   			<div class="branding-cover">
   	    @endif
		<h1 class="event-title evento-titulo" style="margin-left:0px">{{$event->title}}</h1>
   	    <div class="clearfix"></div>
   	    </div>
	   </div>
	   
	</div>
		<div>
			@include('partials.top')
		</div>
   
   @if(!empty($event->banner))
   <div class="panel-group" align="center">
   	{!! HTML::image('uploads/images/'.$event->banner,'', ['class'=>'img-responsive full-width']) !!}
   </div>
   @endif
   
   <div class="row">
           <div class="gradient gr_sm gr-pad">  
   			<!-- <h3 class="titulo-about" style="margin-left:15px">About the event</h3> -->
   		</div>
   	<div class="col-md-9 col-sm-10 col-xs-12">
       	<div class="small-lp subtitulo-evento">
			{!! $event->description !!}
		</div>
       </div>
   
   	<div class="col-md-3 col-sm-10 col-xs-12 booknow">
   		<div class="gradient sidebar cuadro"><h3 class="titulo-Programme">Event details</h3>
   			<div class="sidebar-inner">
   				<b>
   					@if ($event->event_date_from == $event->event_date_to)
   						{{ date('l d M Y',strtotime($event->event_date_from)) }}
   					@else
   						{{ date('d',strtotime($event->event_date_from)) }}-{{ date('d M Y',strtotime($event->event_date_to)) }}
   					@endif
   				</b>
   				<br>
   				<b>Members <span class="fee">@if($event->price > 0) £{{number_format($event->price, 2)}} @else Free @endif</span></b>
   				<br>
   				<b>Non-Members <span class="fee">@if($event->price_nonmember > 0) £{{number_format($event->price_nonmember, 2)}} @else Free @endif</span></b>
   				<br><br>
   
   				@if($event->schedule_version == 0 && $event->main_schedule_visible_web || (sizeof($event->schedule) && $event->schedule->visible_website))
   				<a href="#Programme" class="link">Programme</a> | 
   				@endif
   				
   				@if(sizeof($event->venue) > 0 && in_array(\Config::get('tabs.tab_4'), $event_tabs))
   				<a href="#Venue" class="link">Venue</a> | 
   				@endif
   				
   				@if(sizeof($event->eventFaqs) && in_array(\Config::get('tabs.tab_5'), $event_tabs))
   				<a href="#FAQ" class="link">FAQs</a>
   				@endif
   
   				@if (!empty($event->agenda_pdf))
   					<br/><a class="link" href="{{asset('uploads/agendas/'.$event->agenda_pdf)}}">Download agenda </a>
   				@elseif($event->cpd_download)
   
   				<br/><a href="{{route('events.agenda', [$event->slug,'pdf'])}}" target="_blank" class="link">Download agenda pdf</a>
   
   				@endif
   				
   				@if (!is_null(Auth::user()) &&  hasRoles(Auth::user()->role_id, [role('super_administrator'), role('event_manager'), role('event_coordinator')]))
   					<br/><a href="{{route('events.agenda', [$event->slug,'pdf2'])}}" target="_blank">Download agenda old pdf</a>
   					<br/><a href="{{route('events.agenda', [$event->slug,'docx'])}}">Download agenda docx </a>
   
   				@endif
   				<br>
   				<br>
   		
   		@if(is_null(Auth::user()) || (!is_null(Auth::user()) && sizeof(Auth::user()->events->where('id', $event->id)) == 0))
   		
       		@if(empty($event->cut_of_date) || !empty($event->cut_of_date) && $event->cut_of_date > date("Y-m-d"))
           		<div class="purplebox small"><a class="btn boton" href="{{route('events.book', $event->slug)}}">
           		@if($event->checkAvailability())Join waiting list  @else Book now @endif</a>
           		</div>
       		@endif
   		@elseif(!is_null(Auth::user()) 
   			&& Auth::user()->events->where('id', $event->id)->count()
   			&& (in_array(Auth::user()->events->where('id', $event->id)->first()->pivot->registration_status_id, [config('registrationstatus.cancelled'), config('registrationstatus.declined')])))
   			<div class="purplebox small"><a class="btn btn-sm" href="{{route('events.renewBooking', $event->slug)}}">Renew booking </a></div>
   		@else
               @if(Auth::user()->events->where('id', $event->id)->first()->pivot->registration_status_id == config('registrationstatus.invited'))
                   <a href="{{route('events.acceptInvitation', $event->slug)}}" class="btn details">Accept invitation </a> <br/><br/>
                   <a href="{{route('events.declineInvitation', $event->slug)}}" class="btn details">Decline invitation </a>
               @else
   			<div class="purplebox small"><a class="btn boton v-booking" href="{{route('events.editBooking', $event->slug)}}">View booking </a></div>
   			@endif
   		@endif
   
   			</div>		
   		</div>
   		
   		@if(!empty($event->hashtag))
   		<div class="gradient sidebar twitter-convo">  
   		<h3 class="titulo-Programme">Join the conversation</h3>
   			<div class="tw-inner">
   				<div class="tw-inner-left">
   					<a href="https://twitter.com/search?q=%23PFSPAC" target="_blank" title="Go to twitter">
   					    @if (!empty($event->theme->twitter_image))
   					    <img src="{{asset('uploads/branding/'.$event->theme->twitter_image)}}" width="50" height="" style="/* padding-top: 5px; */">
   					    @else
   					    <img src="{{\Config::get('app.url')}}images/Twitter-bird-50x40.png" width="50" height="" style="/* padding-top: 5px; */">
   					    @endif
   					</a>
   				</div>
   			
   				<div class="tw-inner-right"><p>Tweet about the event at:</p>
   					@foreach(explode(",", $event->hashtag) as $ht)
   						<?php
   						$b=23;
   						$bl = '#';
   						if( $ht[0] == "@" ){ $b=40; $bl = '@'; $ht[0] = ''; }
   						if( $ht[0] == "#" ){ $b=23; $bl = '#'; $ht[0] = ''; }
   						?>
   					<p><a href="https://twitter.com/search?q=%{{$b}}{{trim($ht)}}" target="_blank" title="Go to twitter"><span class="twitter-hash"><strong>{{ $bl }}{{trim($ht)}}</strong></span></a></p>
   					@endforeach
   				</div>
   			</div>
   		</div>
   		@endif
   	</div>
   </div><!--/end row -->
   
   
   @if (sizeof($event->gallery) && in_array(\Config::get('tabs.tab_9'), $event_tabs))
   <link type="text/css" rel="stylesheet" href="{{asset('assets/site/owl-carousel/owl.carousel.css')}}" />
   <link type="text/css" rel="stylesheet" href="{{asset('assets/site/owl-carousel/owl.theme.css')}}" />
   <link type="text/css" rel="stylesheet" href="{{asset('assets/site/owl-carousel/owl.transitions.css')}}" />
   <script src="{{asset('assets/site/owl-carousel/owl.carousel.min.js')}}"></script>
   
   
   <div class="gradient gr_sm gr-pad">
       <h3 class="titulo-Programme">Gallery</h3>
   </div>
       <div id="owl-demo" class="owl-carousel">
   
       @foreach ($event->gallery as $image)
       
           <div><img class="img-responsive" src="{{asset('uploads/gallery/'.$image->image)}}"></div>
       @endforeach
       </div>
   
<script>
$(document).ready(function() {
  $("#owl-demo").owlCarousel({
      autoPlay: 3000, //Set AutoPlay to 3 seconds
      items : 2
  });
});
</script>

@endif
<div class="row">
    <div class="col-lg-9">

    <div id="Programme" style="margin-left:-15px">
	@if(($event->schedule_version == 0 || $event->schedule_version != 0) && sizeof($slots) > 0 && $event->main_schedule_visible_web)
    
    <div class="gradient gr_sm gr-pad">  
    	<h3 class="titulo-Programme">Programme &nbsp;</h3>
    </div>
        
    <?php 
    $event_days = [];
    $date = $event->event_date_from;
    while (strtotime($date) <= strtotime($event->event_date_to)) {
    	$day = date("Y-m-d", strtotime($date));
    	$event_days[] = $day;
    	$date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
    }
    ?>
    
    <div class="panel-group accordion" id="accordion"  style="margin-right:-15px;">
    
    <div class="tabs">
    	<ul class="nav nav-tabs">
    		<?php $first = true; ?>
    		@foreach ($event_days as $seq => $day)
    			<li class="@if($first) active @endif"><a href="#day{{$day}}" data-toggle="tab">DAY {{$seq+1}} - {{date("d/m/Y", strtotime($day))}}</a></li>
    			<?php $first = false; ?>
    		@endforeach
    	</ul>				
    	<div class="tab-content">
    
    		<?php $first = true; ?>
    		@foreach ($event_days as $day)
    			<div id="day{{$day}}" class="tab-pane @if($first)active @endif">
    					@foreach($slots as $slot)
    					<?php if ($day != $slot->start_date)continue;?>
    					<?php $full = ($slot->slot_capacity <= $slot->attendees()->whereIn('registration_status_id', [config('registrationstatus.booked'), config('registrationstatus.waitlisted')])->count() || $slot->stop_booking) ? true : false; ?>
    					<div class="panel panel-primary">
    						<div class="panel-heading active">
    							<h4 class="panel-title"><a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#accordion{{$slot->id}}">{{date("H:i", strtotime($slot->start))}} - {{date("H:i", strtotime($slot->end))}} {{$slot->title}} <span class="flecha">  </span> @if($full)<span class="full-slot">(Waiting list)</span> @endif</a></h4>
    						</div>
    						<div id="accordion{{$slot->id}}" class="panel-collapse collapse">
    							@if(sizeof($slot->sessions))
    							<div class="panel-body">
    								@foreach($slot->sessions as $session)
    									<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion-sub" href="#accordion-sub{{$session->id}}">{{date("H:i", strtotime($session->session_start))}} - {{date("H:i", strtotime($session->session_end))}} {{$session->title}} <span class="flecha">   </span></a></h4>
    									@if(!empty($session->description) || !empty($session->learning_objectives) || sizeof($session->contributors))
    									<div id="accordion-sub{{$session->id}}" class="panel-collapse collapse">
    									<p>{!!$session->description !!}</p>
    									<p>{!! $session->learning_objectives !!}</p>
    									@foreach($session->contributors as $contr)
    										{{$contr->pivot->type}}: {{$contr->badge_name}}, {{$contr->company}} {{$contr->job_role}}
    									@endforeach
    									
    									<br/><br/>
    									</div>
    									@endif
    								@endforeach
    							</div>
    							@endif
    						</div>
    					</div>
    					@endforeach
    			</div>
    			<?php $first = false; ?>		
    		@endforeach
    	</div>
    </div>	
    
    
    <!-- merge </div> -->
    
    
    		<div class="gradient gr_sm gr-pad" >
    			<h3 class="titulo-Programme">CPD</h3>
    		</div>
    		<div class="accreditations"  style="margin-left:15px;">
    			<img src="{{\Config::get('app.url')}}images/cpd-logo.png" width="50" height="101" alt="CPD CII Scheme">
    			<div class="accreditationsLine">&nbsp;</div>
    			<!-- If type event in SMP Events Q1 2019 -->
    			@if($event->eventType->id == 58)
    			<img src="{{\Config::get('app.url')}}images/SMP-logo.png" width="120" height="90" alt="SMP Accredited">
    			@else
    			<img src="{{\Config::get('app.url')}}images/pfs-accredited.png" width="87" height="101" alt="PFS Accredited">
    			@endif
    
    	@if(!empty($cpd_time))
    		<span class="timer">
    			{{$cpd_time['hours']}}<span>h</span> {{$cpd_time['min']}}<span>min</span>
    		</span>
    	@endif
    			<p>This event has been accredited by the Personal Finance Society and the CII and can be included as part of your CII CPD requirement should you consider it relevant to your professional development needs.</p>
    		<br><br>
    		</div>
    @elseif(sizeof($event->schedule) && $event->schedule->visible_website)
    <div class="gradient gr_sm gr-pad" id="accordion">
    <h3>Programme</h3>
    {!! $event->schedule->schedule !!}
    </div>
    @endif
    </div>
    
    @if($assigned_contributors && $event->main_schedule_visible_web && in_array(\Config::get('tabs.tab_3'), $event_tabs))
    <div class="row">  
    	<h3 class="titulo-Programme" style="margin:30px 0; margin-left:29px">Speakers &nbsp;</h3>
    	<?php
    		$first = true;
    		$contributor_displayed = array();
		?>
		<div class="component-speakers">
    	@foreach($sessions as $session)
    		@foreach($session->contributors as $contr)
    			@if(!in_array($contr->id, $contributor_displayed))
    	        <div class="speakers col-xs-6 col-sm-4 col-md-3" id="open_{{$contr->id}}">
    	        	<div class="all-speakers">
    	        		<div class="cuerpo-img">
							@if(!empty($contr->profile_image))
    						    {!! HTML::image('uploads/profile_images/'.$contr->profile_image,'', ['class'=>'img-speaker',]) !!}
								<!-- {!! HTML::image('images/no_image.jpg','', ['class'=>'img-speaker']) !!} -->
								<!-- <img class="img-speakers"src="{{asset('assets/site/img/persona.JPG')}}" alt="">  -->
							@else
    							{!! HTML::image('images/no_image.jpg','', ['class'=>'img-speaker']) !!}

						@endif
    					</div>
    	        		<p class="nombre-speaker">{{$contr->badge_name}}</p>
    	        		<div class="datos-speakers">
    						<!-- <p><strong>Speaker</strong> </p> -->
    						@if(!empty($contr->company))
							<p>
								<!-- <strong>Company:</strong>   -->
								{{$contr->company}}
							</p>
    					    @endif
    					</div>	
    	        	</div>
    			</div>
    			<!-- pop up -->
    			<div id="popup_{{$contr->id}}" class="popup" style="display: none;">
                    <div class="content-popup">
                        <div class="close"><a href="#" id="close_{{$contr->id}}"><img src="https://img.icons8.com/ios-glyphs/26/000000/delete-sign.png"></a></div>
                        <div class="cuerpo-popup">
    						<h2>{{$contr->badge_name}}</h2>
    						<br>
    						<div  class="avatar" style="text-align:center">
    						    @if(!empty($contr->profile_image))
    						    {!! HTML::image('uploads/profile_images/'.$contr->profile_image,'', ['class'=>'img-speaker',]) !!}
								    <!-- {!! HTML::image('images/no_image.jpg','', ['class'=>'img-speaker']) !!} -->
								    <!-- <img class="img-speakers"src="{{asset('assets/site/img/persona.JPG')}}" alt="">  -->
							    @else
    							    {!! HTML::image('images/no_image.jpg','', ['class'=>'img-speaker avatar', 'width'=>'200', 'height' => '250']) !!}
    					        @endif
    							<!-- <img class="img-popup" src="{{asset('assets/site/img/persona.JPG')}}" alt="">  -->
    						</div>
    						@if(!empty($contr->company))
							<br><br>
    	        	        <p><strong>Company:</strong>  {{$contr->company}}</p>
    					    @endif
    						<p>{{$contr->bio}}</p>
                        </div>
                    </div>
    			</div>
    			<div class="popup-overlay" id="popup-overlay"></div>
    			<script type="text/javascript">
                    $(document).ready(function(){
            	        $('#open_{{$contr->id}}').on('click', function(){
                            $('#popup_{{$contr->id}}').fadeIn('slow');
                            $('.popup-overlay').fadeIn('slow');
                            $('.popup-overlay').height($(window).height());
                            return false;
                        });
                    
                        $('#close_{{$contr->id}}').on('click', function(){
                            $('#popup_{{$contr->id}}').fadeOut('slow');
                            $('.popup-overlay').fadeOut('slow');
                            return false;
                        });
                    })
                </script>
    	        @endif
    		@endforeach
		@endforeach
	</div>
</div>	


</div>
		<!-- <div class="gradient gr_sm gr-pad">
			<h3 class=>CPD</h3>
		</div>
        
		<div class="accreditations">
			<img src="{{\Config::get('app.url')}}images/cpd-logo.png" width="50" height="101" alt="CPD CII Scheme">
			<div class="accreditationsLine">&nbsp;</div> -->
			<!-- If type event in SMP Events Q1 2019 -->
			<!-- @if($event->eventType->id == 58)
			<img src="{{\Config::get('app.url')}}images/SMP-logo.png" width="120" height="90" alt="SMP Accredited">
			@else
			<img src="{{\Config::get('app.url')}}images/pfs-accredited.png" width="87" height="101" alt="PFS Accredited">
			@endif

	@if(!empty($cpd_time) && empty($event->manually_cpd))
		<span class="timer">
			{{$cpd_time['hours']}}<span>h</span> {{$cpd_time['min']}}<span>min</span>
		</span>
	@elseif(!empty($event->manually_cpd))
		<span class="timer">
			{{$event->manually_cpd}}<span>min</span>
		</span>
	@endif
			<p>This event has been accredited by the Personal Finance Society and the CII and can be included as part of your CII CPD requirement should you consider it relevant to your professional development needs.</p>
		<br><br>
		</div> -->
    <!-- merge </div> -->
    @endif
    
    <!-- @if($assigned_contributors && $event->main_schedule_visible_web && in_array(\Config::get('tabs.tab_3'), $event_tabs))
    <div class="gradient gr_sm gr-pad">  
        <h3 class="titulo-Programme" style="margin:30px 0">Speakers</h3>
    </div>
    <div class="container tabbox"> 
    	<div class="row" >
    		<div class="col-md-12">
    			<ul id="myTab" class="nav nav-pills nav-stacked col-md-3 col-sm-3">
    			<?php
    				$first = true;
    				$contributor_displayed = array();
    			 ?>
    			@foreach($sessions as $session)
    				@foreach($session->contributors as $contr)
    					@if(!in_array($contr->id, $contributor_displayed))
    					<li @if($first)class="active"@endif><a href="#tabs5-pane{{$contr->id}}" data-toggle="tab">{{$contr->badge_name}}</a></li>
    					<?php
    						$first = false;
    						array_push($contributor_displayed, $contr->id);
    					?>
    					@endif
    				@endforeach
    			@endforeach
    			</ul>
    			<div class="tab-content hidden-xs">
    			<?php
    				$first = true;
    				$contributor_displayed = array();
    			?>
    			@foreach($sessions as $session)
    				@foreach($session->contributors as $contr)
    				@if(!in_array($contr->id, $contributor_displayed))
    				<div id="tabs5-pane{{$contr->id}}" class="tab-pane @if($first)active @endif">
    					@if(!empty($contr->profile_image))
    						<div class="col-sm-3">
    							<br/>
    						{!! HTML::image('uploads/profile_images/'.$contr->profile_image,'', ['class'=>'img-responsive', 'width'=>'150']) !!}
    						</div>
    						
    					<div class="col-sm-9">
    					@else
    					<div class="col-sm-12">
    					@endif
    					
    					<h4>{{$contr->badge_name}} </h4>
    					<p><b>{{$contr->pivot->type}}</b></p>
    					@if(!empty($contr->company))
    					<p><b>Company: </b>{{$contr->company}}</p>
    					@endif
    					<p>{{$contr->bio}}</p>
    					</div>
    				</div>
    				<?php
    					$first = false;
    					array_push($contributor_displayed, $contr->id);
    				?>
    				@endif
    				@endforeach
    			@endforeach
    			</div>
    		</div>
    	</div>
    </div> 
    @endif -->
    
    @if(sizeof($event->venue) > 0 && in_array(\Config::get('tabs.tab_4'), $event_tabs))
    <div class="gradient gr_sm gr-pad">  
        <h3 class="titulo-Programme">Venue &nbsp;</h3>
    </div>
    
    <div class="boxed" id="Venue">
    	<div class="row" >
    	<h4 class="col-md-12 titulo-mapa">{{$event->venue->name}}</h4>
    		<div class="col-md-4 col-sm-4 small-lp">
    			<address>{{$event->venue->address}}<br />
    			{{$event->venue->city}}<br />
    			{{$event->venue->county}}<br />
    			{{$event->venue->postcode}}</address>
    			@if(!empty($event->venue->venue_phone))
    			<div class="venue-links">
				<img src="{{ asset('assets/site/img/phone.png') }}">
				<a href="tel:{{$event->venue->venue_phone}}"> {{$event->venue->venue_phone}}</a></div>
    			@endif
    			@if(!empty($event->venue->website))
    			<div class="venue-links">
				<img src="{{ asset('assets/site/img/internet.png') }}">
				<a href="{{$event->venue->website}}" rel="nofollow" target="_blank"> Venue website</a></div>
    			<br/>
    			@endif
    			@if(!empty($event->venue->details))
    			<br/>
    			<div class="venue-more-details-btn"><button id="more_details" class="btn boton">More details </button></div>
    			@endif
    		</div>
    		@if($event->venue->show_map)
    		<div class="col-md-4 col-sm-4" style="margin-top:61px">
    			<div id="map-event"></div>
    		</div>
    		@endif
    		<div class="col-md-4 col-sm-4 ">
    			@if(!empty($event->venue->image_1))<div class="row details-image"><div class="col-sm-12"><a class="fancybox" rel="venue-image" href="{{Config::get('app.url').'uploads/images/'.$event->venue->image_1}}">{!! HTML::image('uploads/images/'.$event->venue->image_1,'', ['class'=>'img-thumbnail']) !!}</a></div></div> @endif
    			@if(!empty($event->venue->image_2))<div class="row details-image"><div class="col-sm-12 venue-imagen"><a class="fancybox" rel="venue-image" href="{{Config::get('app.url').'uploads/images/'.$event->venue->image_2}}">{!! HTML::image('uploads/images/'.$event->venue->image_2,'', ['class'=>'img-thumbnail']) !!}</a></div></div> @endif
    		</div>
    	</div>
    	<div class="row" id="details" style="display: none;">
    		<div class="col-sm-12 venue-details">{!! $event->venue->details !!}</div>
    	</div>
    </div><!--/end row -->
    @endif
    
    @if((sizeof($quarterly_sponsors) || sizeof($event->sponsors)) && in_array(\Config::get('tabs.tab_8'), $event_tabs) && $event->schedule_version == 0)
    <div class="panel-group sponsors" style="margin-left:-15px; margin-right:0px;">
    	<div class="row" style="margin-left:0px">
    		<div class="col-md-12">
    			<div class="gradient gr_sm gr-pad">
    				<h3 class="partner titulo-Programme">Partners in professionalism</h3>
    			</div>
    		</div>
    	
    	@foreach($event->sponsors()->where('type', 0)->get() as $sponsor)
    	<a href="{{$sponsor->website}}" rel="nofollow" target="_blank">
    		<div class="col-md-3 col-sm-3 col-xs-6">
    			<div class="img-border">
    			@if(!empty($sponsor->logo))
    			<img class="img-responsive" src="{{\Config::get('app.url')}}uploads/sponsors/{{$sponsor->logo}}" />
    			@else
    			{{$sponsor->name}}
    			@endif
    			</div>
    		</div>
    	</a>
        @endforeach
        
        <!-- display quarterly sponsors -->
        @foreach($quarterly_sponsors as $sponsor)
            
            <?php
            for ($i=1; $i <= 10; $i++) {
                $logo_id = "logo_".$i;
                if (empty($sponsor->{$logo_id})) continue;
                
                $sp1 = $sponsors->find($sponsor->{$logo_id});
                if (!sizeof($sp1)) continue;
            ?>
                
                <a href="{{$sp1->website}}" rel="nofollow" target="_blank"><div class="col-md-3 col-sm-3 col-xs-6"><div class="img-border">
                    @if(!empty($sp1->logo))
                    <img class="img-responsive" src="{{\Config::get('app.url')}}uploads/sponsors/{{$sp1->logo}}" />
                    @endif
                    </div></div>
                </a>
            
            <?php }?>
        	
        @endforeach
        </div>
    </div>
    @endif
    
    @if(sizeof($event->eventFaqs) && in_array(\Config::get('tabs.tab_5'), $event_tabs))
    <div id="FAQ" style="margin-right:-15px;">
        <div class="gradient gr_sm gr-pad">  
            <h3 class="titulo-Programme">FAQs</h3>
        </div>
        <div class="panel-group accordion" id="accordionfaq">
    	    @foreach($event->eventFaqs()->where(function($q){
    	        $q->where('visibility', 1)->orWhere('visibility', 2);
    	    })->orderBy('position')->get() as $faq)
    	    <div class="panel panel-primary">
    	    	<div class="panel-heading active">
    
    	    		<h4 class="panel-title">
    					<a data-toggle="collapse" class="collapsed" data-parent="#accordionfaq" href="#accordionfaq{{$faq->id}}">{!!$faq->question!!} 
    						<span class="flecha"> </span>
    					</a>
    				</h4>
    
    	    	</div>
    	    	<div id="accordionfaq{{$faq->id}}" class="panel-collapse collapse">
    	    		<div class="panel-body">{!!$faq->answer!!}</div>
    	    	</div>
    	    </div>
    	    @endforeach
        </div>
    </div>
    
    <!-- fin del wrapper-->
    </div>
    @endif
    <!--
    <div class="row">
    	<div class="col-md-12">
    		<h2>Terms &amp; Conditions</h2>
    		<p>sdfdgd fgd dfgdf gdf gf </p>
    	</div>
    </div>
    -->
    </div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
    	$('#myTab').tabCollapse();
	
	$( "#more_details" ).click(function() {
		//var $this = $(this);
	  $( "#details" ).toggle( "slow", function() {
        //$("html,body").animate({scrollTop: $this.offset().top});
        if($(window).width() < 767){
        	$("html, body").animate({scrollTop: $("#details").offset().top}); 
		}
	  });	
	});
	


	        $('#open').on('click', function(){
                $('#popup').fadeIn('slow');
                $('#popup-overlay').fadeIn('slow');
                $('#popup-overlay').height($(window).height());
                return false;
            });
        
            $('#close').on('click', function(){
                $('#popup').fadeOut('slow');
                $('#popup-overlay').fadeOut('slow');
                return false;
            });

})

@if(sizeof($event->venue) && $event->venue->show_map)
var infowindow = null;

function initialize() {
	
	//starting map focus location
	@if(!empty($event->venue))
	var mypoint = new google.maps.LatLng({{$event->venue->latitude}}, {{$event->venue->longitude}});
	@else
	var mypoint = new google.maps.LatLng();
	@endif
	
	var mapOptions = {
		zoom : 13,
		center : mypoint 
	}
	
	var map = new google.maps.Map(document.getElementById('map-event'), mapOptions);
	
	infowindow = new google.maps.InfoWindow({
		content : "holding..",
		maxWidth: 250
	});
	
	// get filtered events
	/**
	 * Data for the markers consisting of a name, a LatLng and a zIndex for
	 * the order in which these markers should display on top of each
	 * other.
	 */
	
    setMarker(map);
}

function setMarker(map) {
	// Marker sizes are expressed as a Size of X,Y
	// where the origin of the image (0,0) is located
	// in the top left of the image.
	// Origins, anchor positions and coordinates of the marker
	// increase in the X direction to the right and in
	// the Y direction down.
	var image = {
		url: '{{\Config::get('app.url')}}images/marker.png',
		// This marker is 20 pixels wide by 32 pixels tall.
		size: new google.maps.Size(20, 20),
		// The origin for this image is 0,0.
		origin: new google.maps.Point(0,0),
		// The anchor for this image is the base of the flagpole at 0,32.
		anchor:
		new google.maps.Point(10, 20)
	};
	// Shapes define the clickable region of the icon.
	// The type defines an HTML &lt;area&gt; element 'poly' which
	// traces out a polygon as a series of X,Y points. The final
	// coordinate closes the poly by connecting to the first
	// coordinate.
	var shape = {
		coords : [1, 1, 1, 20, 18, 20, 18, 1],
		type : 'poly'
	};

		@if(!empty($event->venue))
		var myLatLng = new google.maps.LatLng({{$event->venue->latitude}}, {{$event->venue->longitude}});
		@else
		var myLatLng = new google.maps.LatLng();
		@endif
		
		var marker = new google.maps.Marker({
			position : myLatLng,
			map : map,
			icon : image,
			shape : shape,
			@if(!empty($event->venue))
			title : '{{$event->venue->name}}',
			description : '<h4>{{$event->venue->name}}</h4><address>{{$event->venue->address}}<br />{{$event->venue->city}}<br />{{$event->venue->county}}<br />{{$event->venue->postcode}}</address><br />'
			@else
			title : '',
			description : ''
			@endif
		});

		google.maps.event.addListener(marker, 'click', function() {
			
			infowindow.setContent(this.description);
			infowindow.open(map, this);
		});
}

google.maps.event.addDomListener(window, 'load', initialize);
@endif
</script>
<!-- BABEL POLYFILL -->
<script src="https://unpkg.com/core-js-bundle@3.1.4/index.js"></script>
<script src = "https://cdn.polyfill.io/v2/polyfill.min.js"> </script>
<!-- END BABEL POLYFILL -->
@endsection
</div>
