<style>
	.content {
		font: 13px/1.231 arial, helvetica, clean, sans-serif;*
	}
</style>
<div class="content">
<h3>Terms and Conditions</h3>
<br/>
<p>We are extremely happy that you are attending one of our Regional Conferences. If you do subsequently find that you are unable to attend, we ask that you give us at least 2 working days notice that you are no longer able to attend.</p>
<p>Please note, if you fail to let us know before this deadline, we will need to charge a £50.00 (inc. VAT) non-attendance fee to cover the cost of catering, booking arrangements and denial of your place to other members. By completing the registration process you agree to these terms and conditions.</p>
<p>All cancellations must be received by email and you will receive confirmation of your cancellation. You may also nominate a replacement if you are unable to attend. If you do attend, your credit card will of course not be charged.</p>
<p>If you register within 2 working days (48 hours) of an event your name may not appear on the delegate list / signing in sheet. Please print off and bring your confirmation email with you to the Conference as proof of your booking.</p>
</div>