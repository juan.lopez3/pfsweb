<style>
	.content {
		font: 13px/1.231 arial, helvetica, clean, sans-serif;*
	}
</style>
<div class="content">
	<H4><b>Who will take payments:</b></H4>
		<p>Transactions for this event will be collected and managed by TFI Group Limited. Your payment will appear on your bank statement as TFI Group Ltd, 020 7233 5644.</p>
		<p>TFI Group Ltd is a UK registered entity. Companies House number 01845643.</p>
		<p>192 Vauxhall Bridge Road, SW1V 1DX London, UK</p>
		<p>E: events@thepfs.org | T: +44(0) 207 808 5616</p>

	<H4><b>Return / Cancellation Policy</b></H4>
	<p style="margin: 0px;"><span style="color: #000000;">All cancellations must be received in writing at&nbsp;<a style="color: #000000;" href="mailto:registration.nationals@pfsevents.org">registration.nationals@pfsevents.org</a>.</span></p>
	<p style="margin: 0px;"><strong><span style="color: #000000;">If members cancel their place after 11:59pm on 14th November 2019 you will be charged a &pound;50.00 non attendance fee.</span></strong></p>
	<p style="margin: 0px;"><span style="color: #000000;">Substitutions can be made at any time.</span></p>
</div>