<!DOCTYPE html>
<html>
<head>
    <title>Attendance Certificate</title>
    <style type="text/css">
    body {
    	border: 1px solid black; 
    	background: url("{!!Config::get('app.url')!!}images/certificate_back.jpg") no-repeat center;
    	font-family: "Arial", "Times New Roman";
    }
	.blue {color:#241539;}
	.session-title{color:#241539; font-weight: bold}
	.footer{font-size: 10px; }
	.op {vertical-align:super}
	.hours {text-decoration: underline;}
	
</style>
</head>
<body>

<center>
<img width="300px" src="images/logo.png" /><br/>
<h1 class="blue">Certificate of attendance </h1>

<h2>{{$user->first_name}} {{$user->last_name}}</h2>
<h3></h3>

<b>{{$event->title}}</b><br/>
{{$event->venue->name}}<br/>
{{date("j", strtotime($event->event_date_from))}}<span class="op">{{date("S", strtotime($event->event_date_from))}}</span> {{date("F Y", strtotime($event->event_date_from))}}

<br/>
<p>Sessions attended:</p>
{!!$sessions!!}
<br/>
<h5 class="hours">Total hours: {{$duration}}</h5>

<br/>
<b>Keith Richards</b><br/>Chief Executive Officer, Personal Finance Society
<br/><br/>
<img src="images/cpd.jpg" /> <img src="images/acentury.jpg" />
<br/>

<p class="footer">Attendance at this event award CPD hours up to the value
shown above as part of your CPD requirement should you
consider it relevant to your professional development needs.
Visit <b>www.cii.co.uk/cpd</b> for more information on the CII CPD scheme</p>

</center>
</body>
</html>