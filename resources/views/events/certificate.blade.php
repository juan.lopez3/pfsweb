<!DOCTYPE html>
<html>
<head>
    <title>Attendance Certificate</title>
<style type="text/css">
     body {font-family: 'helvetica'; font-size: 12pt}
    .bg{ background: #DDE4FF }
    .text{color: #33006F}
    .text-xs-small{font-size: 10px}
    .text-medium{font-size: 12pt}
    .text-large{font-size: 14px}
    .text-sessions{color: #33006F; font-size:14px; font-weight:bold; }
    .op {vertical-align:super}
    .separator{border: none;height: 1px;color: #33006F; background-color: #33006F;}
</style>
</head>
<body>

<img height="170px" src="images/certificate-head.jpg" />

<h1>{{$user->first_name}} {{$user->last_name}}</h1>

<span class="text-medium"><b>{{$event->title}}</b></span><br/>
<div class="text-medium">
    @if ($event->event_date_from == $event->event_date_to)
        {{ date('l d M Y',strtotime($event->event_date_from)) }}
    @else
        {{ date('d',strtotime($event->event_date_from)) }}-{{ date('d M Y',strtotime($event->event_date_to)) }}
    @endif
</div>


<br/>
<p><b>Sessions attended:</b></p>

<table width="100%">
    {!!$sessions!!}
</table>
<br/>
<table width="100%">
    <tr class="text-sessions">
        <td width="75%">Total hours:</td>
        <td width="25%" align="right">{{$duration}}</td>
    </tr>
</table>

<span class="text-xs-small text">Please refer to the CPD Log for individual session learning objectives</span>


<hr class="separator" />
<!-- <img height="60px" src="images/signature.jpg" /> -->
<p class="text-large"><b>Keith Richards</b><br/>
Chief Executive Officer<br/>
Personal Finance Society</p>

<hr class="separator" />

<p class="text-xs-small">Your participation in this event qualifies as part of your ongoing commitment to continuing professional development should you consider its content relevant
to your specific development needs. For more information about your commitment to CPD and our CPD scheme rules please visit <b>www.thepfs.org/CPD</b></p>

<table width="100%">
    <tr>
        <td width="40%">
            <div>&nbsp;</div><div>&nbsp;</div>
            @if($event->eventType->id == 58)
                <img src="images/SMP-logo.png" width="300" height="180" alt="SMP Accredited">
            @else
                <img src="images/footer-logos.png" width="176" height="114" />
            @endif
		</td>
        <td width="60%" align="right">
            @if($event->eventType->id == 58)
                <img src="images/SMP-logo.png" width="300" height="180" alt="SMP Accredited">
            @else
                <img width="325px" src="images/cii_logo.png" />
            @endif
    </tr>
</table>
</body>
</html>