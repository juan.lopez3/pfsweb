<!DOCTYPE html>
<html>
<head>
    <title>{{$event->title}} Contributors</title>
    <style type="text/css">
        body {
            font-size: 12px;
        }
    </style>
</head>
<body>

<center><h2>{{$event->title}} Contributors</h2></center>
<?php $showed = array();?>
@foreach ($event->scheduleSessions as $session)
    @foreach($session->contributors()->orderBy('first_name', 'last_name')->get() as $contributor)
    <?php
    
    if (in_array($contributor->id, $showed)) continue;
    array_push($showed, $contributor->id);
    ?>
    <table>
    <tr>
        @if (!empty($contributor->profile_image) && file_exists('uploads/profile_images/'.$contributor->profile_image))
        <td align="right" width="95px"><img width="90px" src="uploads/profile_images/{{$contributor->profile_image}}" /></td>
        @else
        <td></td>
        @endif
        <td><b>{{$contributor->pivot->type}}: {{$contributor->first_name}} {{$contributor->last_name}},
        @if(!empty($contributor->job_title)){{$contributor->job_title}},@endif @if(!empty($contributor->company)){{$contributor->company}}@endif
        </b><br/>
        {{$contributor->bio}}
        </td>
    </tr>
    </table>
    <br/>
    @endforeach
@endforeach
</body>
</html>