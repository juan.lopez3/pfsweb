<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>{{$event->title}} Schedule</title>
	<link href="{{asset('favicon.ico')}}" rel="icon" type="image/png" />
	<link type="text/css" rel="stylesheet" href="{{asset('assets/site/css/bootstrap.css')}}?md={{config('cache.buster')}}" />
	<link type="text/css" rel="stylesheet" href="{{asset('assets/site/css/overrides.css')}}?md={{config('cache.buster')}}" />
	<style>
		.modal-dialog {
			border: 2px solid black;
		}
		.modal-content {
			background-color: #f2f2f2;
		}
		 .col-md-1, .col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6{
			 padding-left: 3px;
			 padding-right: 3px;
		 }
	</style>
</head>
<body>

@if ($event->schedule_version == 1)
	<br/>
	<div class="tabs">
		<ul class="nav nav-tabs">
            <?php $first = true; ?>
			@foreach ($event_days as $seq => $day)
				<li class="@if($first) active @endif"><a href="#day{{$day}}" data-toggle="tab">DAY {{$seq+1}} - {{date("d/m/Y", strtotime($day))}}</a></li>
                <?php $first = false; ?>
			@endforeach

		</ul>
		<div class="tab-content" style="overflow: hidden">
            <?php $first = true; ?>
			@foreach ($event_days as $day)
				<div id="day{{$day}}" class="tab-pane @if($first)active @endif">

					@if (isset($schedule_header[$day]))
                        <?php
                        if (sizeof($schedule_header[$day]) >= 5) {
                            $column_width = 2;
                        } elseif (sizeof($schedule_header[$day]) == 0) {
                            $column_width = 12;
                        } else {
                            $column_width = 12 / sizeof($schedule_header[$day]);
                        }
                        ?>
						<div class="row">
							<div class="col-xs-offset-1 col-xs-11 text-center">
								@foreach ($schedule_header[$day] as $title)
									<div class="hidden-sm hidden-xs col-md-{{$column_width}}"><b>{{$title}}</b></div>
								@endforeach
							</div>
						</div>
					@endif

					@foreach ($day_slots[$day] as $slot)
                        <?php
                        $start = strtotime($slot->start);
                        $end = strtotime($slot->end);
                        $slot_height = (int) round(abs($end - $start) / 60) * 3;
                        ?>

						<div class="row slot-row">
							<div class="col-xs-1 text-center">
								@if ($slot->show_start) {{date("H:i", strtotime($slot->start))}} <br/> @endif

								@if ($slot->show_end)<br/> {{date("H:i", strtotime($slot->end))}} @endif
							</div>
							<div class="col-xs-11">
								<?php
                                if ($slot->columns >= 5) {
                                    $column_width = 2;
                                } elseif ($slot->columns == 0) {
                                    $column_width = 12;
                                } else {
                                    $column_width = 12 / $slot->columns;
                                }
								?>
								@for($i=1; $i <=$slot->columns; $i++)
									<?php
									// for each slot clean top start time for correct gap calculation
									$start = strtotime($slot->start);
									?>
									<div class="col-md-{{$column_width}}">
										@foreach ($slot->sessions()->where('order_column', $i)->orderBy('session_start')->with('type', 'contributors')->get() as $session)
										<?php
										$session_start = strtotime($session->session_start);
										$session_end = strtotime($session->session_end);
										$session_gap = (int) round(abs($start - $session_start) / 60) * 3;
										$session_height = (int) round(abs($session_end - $session_start) / 60) * 3;
										$start = $session_end;
										?>
										<div class="session col-md-12" style="min-height: 130px; background: {{$session->type->color}}; height: {{$session_height}}px; margin-top: {{$session_gap}}px;">
											<div class="col-xs-10 session-row">
												<span class="time">
													<div class="visible-xs visible-sm">{{$session->meetingSpace->meeting_space_name}}<br/></div>
												</span>
												<span class="session_title">@if (sizeof($slot->sessions) > 2 && strlen($session->title) > 82 && sizeof($session->contributors)){{substr($session->title, 0, 80)}}... @else{{$session->title}} @endif</span>
												@foreach($session->contributors()->where('type', 'Speaker')->get() as $contributor)
													<br/>{{$contributor->first_name}} {{$contributor->last_name}}
												@endforeach
											</div>

											<div class="col-xs-2">
												<div class="text-right">
													<a class="more-details" style="cursor:pointer;" data-toggle="modal" data-target="#more_details{{$session->id}}"><img height="20px" src="{{asset('images/info.png')}}" title="More Details" alt="More Details" /></a>
												</div>
												<div id="more_details{{$session->id}}" class="modal fade" role="dialog">
													<div class="modal-dialog modal-lg">
														<div class="modal-content">
															<div class="modal-header">
																<button type="button" class="close" data-dismiss="modal">&times;</button>
																<h4 class="modal-title">
																	{{date("H:i", strtotime($session->session_start))}} - {{date("H:i", strtotime($session->session_end))}} {{$session->title}}<br/>
																	<img height="15px" src="{{asset('images/pin.png')}}" /> {{$session->meetingSpace->meeting_space_name}}</span> - {{$session->type->title}}
																</h4>
															</div>
															<div class="modal-body">
																<div class="row">
																	<div class="col-xs-12">
																	{!! $session->description !!}

																	@if (!empty($session->learning_objectives))
																		<h4><b>Learning objectives</b></h4>
																		{!! $session->learning_objectives !!}
																	@endif


																	@foreach($session->contributors as $contributor)
																		<div class="row">
																		@if(!empty($contributor->profile_image))
																			<div class="col-sm-3">
																				<br/>
																				{!! HTML::image('uploads/profile_images/'.$contributor->profile_image,'', ['class'=>'img-responsive', 'width'=>'150']) !!}
																			</div>
																		@endif
																			<div class="col-sm-9">
																				<p><b>{{$contributor->pivot->type}}:</b> {{$contributor->badge_name}}</p>


																				@if(!empty($contributor->company))
																					<p><b>Company: </b>{{$contributor->company}}</p>
																				@endif
																				@if (!empty($contributor->bio))
																					<b>Bio:</b>
																					<p>{{$contributor->bio}}</p>
																				@endif
																			</div>
																		</div>
																	@endforeach
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									@endforeach
									</div>
								@endfor
							</div>
						</div>
					@endforeach
				</div>
                <?php $first = false; ?>
			@endforeach
		</div>
	</div>
@endif


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="{{asset('assets/site/js/bootstrap.min.js')}}"></script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-5877180-16', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>