<!DOCTYPE html>

<html>
<head>
    <title>{{$event->title}} Agenda</title>
    <style type="text/css">
	    body {
	    	font-family: Calibri,Candara,Segoe,Segoe UI,Optima,Arial,sans-serif;
	    	font-size: 12px;
	    }
	    .twitter-hash {color: #4FA1DE; font-size: 20px; font-weight: bold;}
	    .twitter-header {font-size: 10px; font-weight: bold;}
	    .time {color: #33006E;}
	    .cpd_log {width: 100%; border: 1px solid black;}
	</style>
</head>
<body>

<table width="100%">
	<tr>
		<td width="10%">
			<img src="images/twitter.jpg" />
		</td>
		<td width="60%">
			@if(!empty($event->hashtag))
			<span class="twitter-header">Join the conversation on Twitter:</span><br/>
				@foreach(explode(",", $event->hashtag) as $tag)
					<span class="twitter-hash">#{{trim($tag)}}</span>
				@endforeach
			@endif
		</td>
		<td width="40%">
		    
		    @if (!empty($event->eventType->agenda_branding))
		      <img src="{{asset('uploads/branding/'.$event->eventType->agenda_branding)}}" height="60px"/>
		    @else
		      <img src="{{asset('images/cii_logo.png')}}" height="110px"/>
		    @endif
		</td>
	</tr>
</table>

<?php $previus_session =false; ?>
<h3>Agenda {{$event->title}}</h3>

<table width="100%">
	<tr>
		<td width="15%">Venue:</td>
		<td width="85%">{{$event->venue->name}}, {{$event->venue->address}}, {{$event->venue->city}}@if(!empty($event->venue->country)), {{$event->venue->country}}@endif, {{$event->venue->postcode}}</td>
	</tr>
	<tr>
		<td width="15%">Date:</td>
		<td width="85%">
			@if ($event->event_date_from == $event->event_date_to)
				{{ date('l d M Y',strtotime($event->event_date_from)) }}
			@else
				{{ date('d',strtotime($event->event_date_from)) }}-{{ date('d M Y',strtotime($event->event_date_to)) }}
			@endif
		</td>
	</tr>
</table>

<br/>

	@if((sizeof($quarterly_sponsors) || sizeof($event->sponsors)) && in_array(\Config::get('tabs.tab_8'), $event->tabs()->lists('id')))
	<p>In association with our Partners in Professionalism:</p>

		@if($event->id == 443 || $event->id == 442)
			@foreach($event->sponsors()->where('type', 0)->where('event_sponsor.order', '>', 0)->orderBy('event_sponsor.order', 'ASC')->limit(19)->get() as $sponsor)
				@if(!empty($sponsor->logo))
					<img style="margin-right: 10px" height="50px" src="{{\Config::get('app.url')}}uploads/sponsors/{{$sponsor->logo}}" />
				@else
					{{$sponsor->name}}
				@endif
			@endforeach
		@else
			@foreach($event->sponsors()->where('type', 0)->get() as $sponsor)
				@if(!empty($sponsor->logo))
					<img style="margin-right: 10px" height="50px" src="{{\Config::get('app.url')}}uploads/sponsors/{{$sponsor->logo}}" />
				@else
					{{$sponsor->name}}
				@endif
			@endforeach
		@endif
		<!-- display quarterly sponsors -->
		@foreach($quarterly_sponsors as $sponsor)
		<?php
		for ($i=1; $i <= 10; $i++) {
			$logo_id = "logo_".$i;
			if (empty($sponsor->{$logo_id})) continue;
			
			$sp1 = $sponsors->find($sponsor->{$logo_id});
		?>
			@if(!empty($sp1->logo))
				<img style="margin-right: 20px" height="50px" src="{{\Config::get('app.url')}}uploads/sponsors/{{$sp1->logo}}" />
			@else
				{{$sp1->name}}
			@endif
		
		<?php } ?>
		@endforeach
	@endif

@foreach($event->scheduleSessions()->with('contributors')->with('slot')->orderBy('start_date')->orderBy('session_start')->get() as $session)
<table width="100%">
	@if(!($previus_session) || $previus_session->slot->start_date != $session->slot->start_date)
		<tr class="pdf-content-table-title-wrapper">
			<td colspan="2" class="pdf-content-table-title">
				{{date("j-M-Y", strtotime($session->slot->start_date))}} 
			</td>
		</tr>
	@endif
	<tr>
		<td width="15%" valign="top"><span class="time">{{date("H:i", strtotime($session->session_start))}} - {{date("H:i", strtotime($session->session_end))}}</span></td>
		<td width="85%">
			<b>{{$session->title}}</b><br/>
			@if(!empty($session->learning_objectives))
			{!!$session->learning_objectives!!}
			@endif
			
			@foreach($session->contributors as $contr)
				{{$contr->badge_name}}, {{$contr->company}} {{$contr->job_role}} 
			@endforeach
		</td>
	</tr>
	<?php $previus_session =$session; ?>
</table>
<br/>
@endforeach

<p>Total CPD provided: {{$cpd_time}}hrs</p>

<p>The content in each session has been carefully selected and can be considered for both structured and unstructured CPD hours, depending how this activity addressed each individual's personal development needs.</p>
<p><b>Structured CPD</b> is the undertaking of any formal learning activity designed to meet a specific learning outcome (this is what an individual is expected to know, understand or do as a result of his or her learning).</p>
<p><b>Unstructured CPD</b> is any activity an individual considers has met a learning outcome, but which may not have been specifically designed to meet their development needs.</p>

<table width="80%">
	<tr>
		<td width="5%"><img src="images/pfs-accredited.png" /></td>
		<td width="5%"><img src="images/cpd.jpg" /></td>
		<td width="70%">Attendance at this event can be included as part of your CPD requirements should you consider it relevant to your professional development needs.</td>
	</tr>
</table>

</body>
</html>