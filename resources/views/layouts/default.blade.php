<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PFS @section('title') @show</title>
	<meta name="keywords" content="@section('meta_keywords')DEFAULT KEYWORDS @show"/>
	<meta name="description" content="@section('meta_description')DEFAULT DESCRIPTION @show"/>
    <link href="{{asset('favicon.ico')}}" rel="icon" type="image/png" />

    <!-- reset cache  -->
    <meta http-equiv="Expires" content="0"> 
    <meta http-equiv="Last-Modified" content="0">  
    <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">  
    <meta http-equiv="Pragma" content="no-cache">

    <!-- START STYLESHEET -->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/site/css/style.css')}}?md={{config('cache.buster')}}" />
    <!-- <link type="text/css" rel="stylesheet" href="{{asset('assets/site/css/bootstrap.css')}}" /> -->
	<link type="text/css" rel="stylesheet" href="{{asset('assets/site/css/overrides.css')}}?md={{config('cache.buster')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/site/css/estilos.css')}}" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- styles main -->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/site/css/wwwthepfsorg.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/site/css/custom-cii.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/site/css/stylePfs.css')}}" />
    
    
    <!-- END STYLESHEET -->
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript">
    $(function () {
        $.scrollUp({
            scrollName: 'scrollUp',      // Element ID
            scrollDistance: 300,         // Distance from top/bottom before showing element (px)
            scrollFrom: 'top',           // 'top' or 'bottom'
            scrollSpeed: 300,            // Speed back to top (ms)
            easingType: 'linear',        // Scroll to top easing (see http://easings.net/)
            animation: 'fade',           // Fade, slide, none
            animationSpeed: 200,         // Animation speed (ms)
            scrollTrigger: false,        // Set a custom triggering element. Can be an HTML string or jQuery object
            scrollTarget: false,         // Set a custom target element for scrolling to. Can be element or number
            scrollText: 'Back to top', // Text for element, can contain HTML
            scrollTitle: false,          // Set a custom <a> title if required.
            scrollImg: false,            // Set true to use image
            activeOverlay: false,        // Set CSS color to display scrollUp active point, e.g '#00FFFF'
            zIndex: 2147483647           // Z-Index for the overlay
        });
    });
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-123729095-1', 'auto');
  ga('send', 'pageview');

</script>

@include('partials.header')
<!-- wrapper -->
<!-- 
    <div class="wrapper"> -->
        @yield('content')
    <!-- </div> -->



@include('partials.footer')
@include('partials.tfiNuevo')

<!-- BABEL POLYFILL -->
<script src="https://unpkg.com/core-js-bundle@3.1.4/index.js"></script>
<script src = "https://cdn.polyfill.io/v2/polyfill.min.js"> </script>
<!-- END BABEL POLYFILL -->
<script src="{{asset('assets/site/js/bootstrap.min.js')}}?md={{\Config::get('cache.buster')}}"></script>
<script src="{{asset('assets/site/js/ie10-viewport-bug-workaround.js')}}?md={{\Config::get('cache.buster')}}"></script>
<script src="{{asset('assets/site/js/custom.js')}}?md={{\Config::get('cache.buster')}}"></script>

<!-- SCRIPS MENU -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<script src="{{asset('assets/site/js/scriptPfs.js')}}?md={{\Config::get('cache.buster')}}"></script>

<!-- FONTS -->
<link rel="stylesheet" type="text/css" href="https://cloud.typography.com/6626596/7686812/css/fonts.css" />
<link rel="https://cdn.rawgit.com/mfd/f3d96ec7f0e8f034cc22ea73b3797b59/raw/856f1dbb8d807aabceb80b6d4f94b464df461b3e/gotham.css">


<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> -->
</body>
<script type="text/javascript">
    $(document).ready(function () {
        $('.icon-tooltip-information').click(function (e) {
            e.preventDefault();
        });
    });
</script>

</html>
