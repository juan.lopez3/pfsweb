<!-- Footer-Content-Begin -->
<style>
    .tarjeta{
        width:40px;
        height: 18px;
        padding-right: 6px;
        padding-left: 0px;
    }
    .tfi{
        width:150px;
        height: 40px;
    }
    .master{
        width: 80px;
        height: 18px; 
    }
    .texto{
        text-align:center;
        color:white;
    }
    .body-tfi{
        position:relative;
        top:-2px;
        background-color:#333;
        margin-left:-15px;
    }
    .content{
        margin-top:10px;
    }
    .tfi{
        margin-top:15px;
    }

    @media (min-width:767px){
        .grupo-tarjetas{
            margin-left: 0px;
        }

        .master ,.tarjeta{
            margin-top:20px;
        }
    }

</style>
	<div  class="wrapper body-tfi">
        <div class="row" style="margin-bottom: -2px;">
            <div class="col-sm-2 texto">
                <img alt="tfi"  class="col-sm-4 tfi" src="{{asset('assets/site/img/tfi.png')}}">
            </div>
            <div class="col-sm-7 texto content">
               <p>Transactions for this event will be collected and managed by TFI Group Limited. TFI Group is a UK registered entity. Companies House number 01845643. Operating office address is 192 Vauxhall Bridge Road, London, SW1V 1DX, United Kingdom.</p>
            </div>
            
            <div class="col-sm-3 grupo-tarjetas">
                <div class="row texto">
                    <img alt="visa"  class=" tarjeta" src="{{asset('assets/site/img/visa.png')}}">
                    <img alt="american express"  class=" tarjeta" src="{{asset('assets/site/img/america.png')}}">
                    <img alt="mastercar"  class="master " src="{{asset('assets/site/img/master.png')}}">
                </div>
            </div>
        </div>
	</div>
<!-- Footer-Content-End -->