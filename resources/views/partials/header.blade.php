
<header>
<!-- Static navbar -->	

<div class="header">
    <!-- <div class="header__toolbar hidden-md-down" id="header-toolbar-wide">
        <div class="container">
            <div>
                <div class="user-data">
                    <a id="login-link" href="/login" class="hidden-md-down">Login</a>
                    <div class="seperator"></div><a id="sign-up-link" href="/sign-up" class="hidden-md-down">Sign up</a>
                    <div class="seperator"></div>
                    <a id="view-basket-url-link" href="{{asset('assets/site/images/basket.svg')}}" class="basket">
                        <span class="amount" id="basket-qty">0</span>
                        <span class="label">Items in basket</span>
                    </a>
                </div>

            </div>
        </div>
    </div> -->
            

<div class="mobile__helper">
	<div class="header__container">
		<div class="container header__inner">
			<!-- <a id="navbar-brand-link" class="header__logo logo navbar-brand" href="/" alt=""><span class="sr-only">Charter Insurance Institute</span></a> -->
			<a  id="navbar-brand-link" class="header__logo navbar-brand" href="http://www.thepfs.org/">
				    <img class="logo" src="{{asset('assets/site/img/PFS_Logo.svg')}}" alt="PFS" title="Personal Finance Society" />
					<!-- <p class="text-logo">Personal</p>
					<p class="text-logo">Finance</p>
					<p class="text-logo">Society</p> -->
				</a>
			
		</div>
	</div><a href="#" class="hidden-lg-up" id="mobile-search-toggler">
						
		<!-- <div class="header"> -->
			<nav class="navbar navbar-toggleable-md" role="navigation">
                <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
                    <div class="navbar__toggler-icon">
                        <span></span><span></span><span></span><span></span>
                    </div>
                </button>
                <div class="collapse navbar-collapse justify-content-start cerrarMenu" id="navbarToggler">
                    <div class="header-nav-wrapper">
                        <ul class="header__nav navbar-nav">


						<?php 
						/*$menu_items = [
							["name","link","children"=>[]]
						];*/
						?>
       
	   <li class="nav-item dropdown">


			<?php //foreach($menu_items as $indice => $menu_item):?>

            <a id="menu__0" class="menu__button-link nav-link dropdown-toggle" href="https://www.thepfs.org/about-us/"
               aria-haspopup="true" aria-expanded="true">About us<i class="icon arrow-down"></i></a>

            <ul id="submenu__0" class="submenu__nav nav justify-content-start dropdown-menu"
                aria-labelledby="menu__0">

                        <div class="sub-nav-wrapper">
                            <div class="container">
                                <div class="sub-nav-menu">
								<?php //foreach($menu_item["children"] as $k => $item):?>
                                            <li class="nav-item">
                                                <a id="/about-us/what-we-do/-link" class="dropdown-item" href="https://www.thepfs.org/about-us/what-we-do/">What we do</a>
                                            </li>
                                            <li class="nav-item">
                                                <a id="/about-us/governance/-link" class="dropdown-item" href="https://www.thepfs.org/about-us/governance/">Governance</a>
                                            </li>
                                            <li class="nav-item">
                                                <a id="/about-us/professional-standards/-link" class="dropdown-item" href="https://www.thepfs.org/about-us/professional-standards/">Professional standards</a>
                                            </li>
                                            <li class="nav-item">
                                                <a id="/about-us/initiatives/-link" class="dropdown-item" href="https://www.thepfs.org/about-us/initiatives/">Initiatives</a>
                                            </li>
								<?php //endforeach;?>
                                </div>
                            </div>
                        </div>

            </ul>
        </li>


        <li class="nav-item dropdown">

            <a id="menu__1" class="menu__button-link nav-link dropdown-toggle" href="https://www.thepfs.org/membership/"
               aria-haspopup="true" aria-expanded="true">Membership<i class="icon arrow-down"></i></a>
            <ul id="submenu__1" class="submenu__nav nav justify-content-start dropdown-menu"
                aria-labelledby="menu__1">

                        <div class="sub-nav-wrapper">
                            <div class="container">
                                <div class="sub-nav-menu">
                                            <li class="nav-item">
                                                <a id="/membership/join-us/-link" class="dropdown-item" href="https://www.thepfs.org/membership/join-us/">Join us</a>
                                            </li>
                                            <li class="nav-item">
                                                <a id="/membership/benefits/-link" class="dropdown-item" href="https://www.thepfs.org/membership/benefits/">Benefits</a>
                                            </li>
                                            <li class="nav-item" >
                                                <a id="/membership/chartered/-link" class="dropdown-item" href="https://www.thepfs.org/membership/chartered/" >Chartered</a>
                                            </li>
                                            <li class="nav-item">
                                                <a id="/membership/search/-link" class="dropdown-item" href="https://www.thepfs.org/membership/search/">Search</a>
                                            </li>
                                            <li class="nav-item">
                                                <a id="/membership/sps/-link" class="dropdown-item" href="https://www.thepfs.org/membership/sps/">SPS</a>
                                            </li>
                                            <li class="nav-item">
                                                <a id="/membership/support/-link" class="dropdown-item" href="https://www.thepfs.org/membership/support/">Support</a>
                                            </li>
                                            <li class="nav-item">
                                                <a id="/membership/regions/-link" class="dropdown-item" href="https://www.thepfs.org/membership/regions/">Regions</a>
                                            </li>

                                </div>
                            </div>
                        </div>

            </ul>
        </li>

        <li class="nav-item dropdown">

            <a id="menu__2" class="menu__button-link nav-link dropdown-toggle" href="https://www.thepfs.org/learning/"
               aria-haspopup="true" aria-expanded="true">Learning<i class="icon arrow-down"></i></a>
            <ul id="submenu__2" class="submenu__nav nav justify-content-start dropdown-menu"
                aria-labelledby="menu__2">

                        <div class="sub-nav-wrapper">
                            <div class="container">
                                <div class="sub-nav-menu">
                                            <li class="nav-item">
                                                <a id="/learning/qualifications/-link" class="dropdown-item" href="https://www.thepfs.org/learning/qualifications/">Qualifications</a>
                                            </li>
                                            <li class="nav-item">
                                                <a id="/learning/learning-content-hub/-link" class="dropdown-item" href="https://www.thepfs.org/learning/learning-content-hub/">Learning content hub</a>
                                            </li>
                                            <li class="nav-item">
                                                <a id="/learning/knowledge-services/-link" class="dropdown-item" href="https://www.thepfs.org/learning/knowledge-services/">Knowledge Services</a>
                                            </li>

                                </div>
                            </div>
                        </div>

            </ul>
        </li>

        <li class="nav-item dropdown">

            <a id="menu__3" class="menu__button-link nav-link dropdown-toggle" href="https://www.thepfs.org/news-insight/"
               aria-haspopup="true" aria-expanded="true">News &amp; insight<i class="icon arrow-down"></i></a>
            <ul id="submenu__3" class="submenu__nav nav justify-content-start dropdown-menu"
                aria-labelledby="menu__3">

                        <div class="sub-nav-wrapper">
                            <div class="container">
                                <div class="sub-nav-menu">
                                            <li class="nav-item">
                                                <a id="/news-insight/news/-link" class="dropdown-item" href="https://www.thepfs.org/news-insight/news/">News</a>
                                            </li>
                                            <li class="nav-item">
                                                <a id="/news-insight/publications/-link" class="dropdown-item" href="https://www.thepfs.org/news-insight/publications/">Publications</a>
                                            </li>
                                            <li class="nav-item">
                                                <a id="/news-insight/press-contacts/-link" class="dropdown-item" href="https://www.thepfs.org/news-insight/press-contacts/">Press contacts</a>
                                            </li>

                                </div>
                            </div>
                        </div>

            </ul>
        </li>
        <li class="nav-item dropdown">

            <a id="menu__4" class="menu__button-link nav-link dropdown-toggle" href="https://www.thepfs.org/events/"
               aria-haspopup="true" aria-expanded="true">Events<i class="icon arrow-down"></i></a>
            <ul id="submenu__4" class="submenu__nav nav justify-content-start dropdown-menu"
                aria-labelledby="menu__4">

                        <div class="sub-nav-wrapper">
                            <div class="container">
                                <div class="sub-nav-menu">
                                            <li class="nav-item">
                                                <a id="/events/regional-conferences/-link" class="dropdown-item" href="https://www.thepfs.org/events/regional-conferences/">Regional conferences</a>
                                            </li>
                                            <li class="nav-item">
                                                <a id="/events/chartered/-link" class="dropdown-item" href="https://www.thepfs.org/events/chartered/">Chartered</a>
                                            </li> 
                                            <li class="nav-item">
                                                <a id="/events/awards/-link" class="dropdown-item" href="https://www.thepfs.org/events/awards/">Personal Finance Awards</a>
                                            </li>
                                            <li class="nav-item">
                                                <a id="/events/futureproof/-link" class="dropdown-item" href="https://www.thepfs.org/events/futureproof/">Annual conference</a>
                                            </li>
                                            <li class="nav-item">
                                                <a id="/events/futureproof/-link" class="dropdown-item" href="https://events.thepfs.org/public/?address=&range=&from=&to=&keywords=investment">Investment roadshows</a>
                                            </li>

                                </div>
                            </div>
                        </div>

            </ul>
        </li>

                        </ul>
                    </div>
                    <!-- <div class="mobile-menu-items">
                        <ul>
                                <li>
                                    <a id="mobile-menu-login-link" href="https://www.thepfs.org/login">Login</a>
                                </li>
                                <li>
                                    <a id="mobile-menu-join-link" href="https://www.thepfs.org/sign-up">Sign up</a>
                                </li>
                        </ul>
                    </div> -->
                </div>
            </nav>
		</div>
	</div>
</nav>
<!-- Event Subnav -->
	<!-- <div class="navbar second-nav navbar-static-top">
		<div class="container">
			<div class="collapse navbar-collapse sec-nav">
				<ul class="nav navbar-nav">
					@foreach($submenu as $item)
						@if($item->id < 13)
						@continue
						<li><a href="{{$item->url}}">{{$item->title}}</a></li>
						<li class="divider-vertical"></li>
						@endif
					@endforeach
				</ul>
			</div>
		</div>
	</div> -->
	</div>
</header>

<!-- VUETIFY -->
<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.min.css" rel="stylesheet">

<!-- FONTS -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"rel="stylesheet">


  <style>
		#app .application .application--wrap{
		min-height: auto;
		
		}
		/* a{
			color:white;
			font-size: 16px;			
			
		} */
		.title-submenu{
			width: 10%;
			padding:0 1rem;
		}
		a :hover{
			color: #34373c;
		}
		.theme--light.v-list .v-list__tile--link:hover {
			background: #c1c2bd;
			
		}
		.v-btn__content:hover {
			background: none;
			text-decoration: underline;
		}
		button .v-btn__content{
			margin-bottom: 7px;
		}
		.v-menu__content{
			background: #6d6d6d;
			position:absolute;

		}
		.v-expansion-panel__header{
			background-color: rgb(108, 108, 106);
			padding: 5%;
		}
		.v-expansion-panel__header:hover{
			background-color: rgb(84, 84, 84);
			font-weight:600;
		}
		.theme--light.v-expansion-panel .v-expansion-panel__container{
			background-color: rgb(132, 132, 129);
		}
		.v-expansion-panel__header__icon{
			display:none;
		}
		.v-expansion-panel{
			box-shadow: none !important;
		}
			
  </style>
  <script type="text/javascript">
      $(document).ready(function () {
          $('.icon-tooltip-information').click(function (e) {
              e.preventDefault();
          });
	});

</script>
<!-- END VUETIFY -->