<div class="footer">
    <div class=" wrapper font-Cae containerFlex">
            <!-- <div class="tfilogo">
                <img src="{{asset('assets/site/img/tfi-2.png')}}" alt="logo tfi">
            </div> -->
            <div class="tfiinfo mt-2">
                <p>Transactions for this event will be collected and managed by TFI Group Limited. TFI Group is a UK registered entity. Companies House number 01845643. Operating office address is 192 Vauxhall Bridge Road, London, SW1V 1DX, United Kingdom.</p>
            </div>
        <div class="logovisa">
            <ul>
                <li class="visa"><img src="{{asset('assets/site/img/visa-2.png')}}" alt="logo"></li>
                <li class="american"><img src="{{asset('assets/site/img/america-2.png')}}" alt="logo"></li>
                <li class="master"><img src="{{asset('assets/site/img/master-2.png')}}" alt="logo"></li>
            </ul>
        </div>
    </div>
</div>
