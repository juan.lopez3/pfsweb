<!-- Footer-Content-Begin -->
<footer>
	<div class="container body-footer">
		<div class="row">
		    <div class="col-12 col-sm-6 escudo" style="margin-bottom: -10px;">
		    	<img class="logo-footer" src="{{asset('assets/site/img/cii-logo.svg')}}" alt="PFS" title="Personal Finance Society" /><br><br><br>
				<p>The Personal Finance Society is part of the <br> Chartered Insurance Institute group.</p><br>
				<!-- <p class="title-footer" >Find out more</p> -->
				<a class="title-footer" href="https://www.thepfs.org/about-us/ ">Find out more</a>
			</div>
		    <div class="col-12 col-sm-3 escudo">
		        <a class="title-footer" href="https://www.thepfs.org/about-us/accessibility-statement/">Accessibility</a><br><br>
		        <a class="title-footer" href="http://www.thepfs.org/about/data-protection-and-privacy-statement/">Privacy</a><br><br>
		        <a class="title-footer" href="http://www.thepfs.org/about/terms-and-conditions/">Terms &amp; conditions</a><br><br>
		        <a class="title-footer" href="http://www.thepfs.org/faq/">FAQs</a><br><br>
				<a class="title-footer" href="http://www.thepfs.org/about/contact-us/">Contact</a><br><br>
		        <!-- <a class="title-footer" href="http://www.thepfs.org/my-pfs/">yourmoney update</a><br><br> -->
			</div>
		    
		    <div class="col-12 col-sm-3 escudo">
		    	<p class="title-footer contact-title">Contact us</p><br>
		    	<p class="contacts">Personal Finance Society</p>
		    	<p class="contacts">42-48 High Road </p>
		    	<p class="contacts">South Woodford </p>
		    	<p class="contacts">London</p>
		    	<p class="contacts">E18 2JP </p><br>
		    	<p class="contacts">Tel: <span class="title-footer">+44 (0)20 8530 0852 </span></p>
		    	<p class="contacts">Email: <span class="title-footer">customer.serv@thepfs.org</span> </p>
		    </div>
			
			
		    <div class="col-12 col-sm-6 redes">
				<p class="title-footer contact-title">Follow us</p><br>
				<a href="http://www.youtube.com/ciimedia"><img alt="YouTube" class="imglast" src="{{asset('assets/site/img/youtube.png')}}" /></a>		
		    	<a href="https://www.linkedin.com/company/the-personal-finance-society/"><img alt="LinkedIn" class="imglast" src="{{asset('assets/site/img/linkedin.png')}}" /></a>
		    	<a href="https://twitter.com/pfsconf"><img alt="Twitter" class="imglast" src="{{asset('assets/site/img/twiter.png')}}" /></a>
		    </div>
    
		    <!-- <div class="col-12 col-sm-6 body-img">
		        <img class="img-footer" src="{{asset('assets/site/img/Logo-PFS-web-footer.png')}}" alt="PFS" title="Personal Finance Society" /><br>
			</div> -->
			
			<div class=" col-12">
				<!-- <p style="visibility: hidden;">.</p> -->
				<p class="copyrighta"></p>
				<!-- <p style="visibility: hidden;">.</p> -->
				<p class="copyright" style=";margin-top:20px;">Copyright ©{{date("Y")}} The Chartered Insurance Institute. All rights reserved.</p>
				<!-- <p class="copyright">Standards. Professionalis. Trust </p> -->
			</div>
		</div>
		
<!-- 
		<div class="col-sm-9 col-sm-7 col-xs-12"> -->
			<!-- <ul>
				<li><a href="http://www.thepfs.org/about/contact-us/">Contact us</a> |</li>
				<li><a href="http://www.thepfs.org/about/terms-and-conditions/">Terms &amp; conditions</a> |</li>
				<li><a href="http://www.thepfs.org/about/accessiblity-statement/">Accessibility statement</a> |</li>
				<li><a href="http://www.thepfs.org/about/data-protection-and-privacy-statement/">Privacy statement</a> |</li>
				<li><a href="http://www.thepfs.org/my-pfs/">yourmoney update</a> |</li>
				<li><a href="http://www.thepfs.org/faq/">FAQs</a></li>
			</ul> -->
			<!-- <span style="font-size:0.9em;">Copyright ©{{date("Y")}} The Personal Finance Society</span><br /><br />
            <img alt="Personal Finance Society" src="{{asset('assets/site/img/Footer-Text.png')}}">
		</div> -->

		<!-- <div class="col-sm-3 col-sm-5 col-xs-12 footersocial">
			<div class="l">Follow us</div>
			<div class="r">
				<a href="http://www.linkedin.com/groups?gid=3550775&trk=myg_ugrp_ovr"><img alt="LinkedIn" class="" src="{{asset('assets/site/img/linkedIn_icon.png')}}" /></a>
				<a href="https://twitter.com/pfsconf"><img alt="Twitter" class="" src="{{asset('assets/site/img/twitter_icon.png')}}" /></a>
				<a href="http://www.youtube.com/ciimedia"><img alt="YouTube" class="imglast" src="{{asset('assets/site/img/youTube_icon.png')}}" /></a>
			</div> 
		</div> -->
	</div>
</footer>
<!-- Footer-Content-End -->