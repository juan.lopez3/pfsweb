<?php
$size = sizeof($breadcrumbs);
$i = 1;
?>

<div class="row my-events notoppadding" style="vertical-align: middle;">
	<div class="col-sm-6">
		@foreach($breadcrumbs as $title => $url)
			@if($i < $size)
			<a class="menu-paginas" href="{{url($url)}}">{{$title}} </a> <span class="arrow-pan"> > </span>
			@else
			<span class="menu-paginas"> {{$title}} </span>
			@endif
			<?php $i++; ?>
		@endforeach
	</div>
	<div class="col-sm-6 persona-login menu-paginas">
		@if(Auth::check())
			You are logged in as <span class="nombre-login">{{Auth::user()->first_name}} {{Auth::user()->last_name}}</span>  | <a href="{{url('/auth/logout')}}">Log out</a>
		@endif
		
		<!-- <a class="btn btn-sm details-sep" href="{{route('profile.myEvents')}}">My Events »</a> -->
	</div>
</div>
<p class="hr"></p>