@extends('layouts.default')
@section('title'){{$event->title}} @endsection
@section('description'){{$event->description}} @endsection

@section('content')
<link type="text/css" rel="stylesheet" href="{{asset('assets/site/css/theme-default/libs/select2/select2.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/site/css/theme-default/libs/wizard/wizard.css')}}" />
<script src="{{asset('assets/admin/fancybox/source/jquery.fancybox.js')}}"></script>
<link type="text/css" rel="stylesheet" href="{{asset('assets/admin/fancybox/source/jquery.fancybox.css')}}" />
<script type="text/javascript">
$(document).ready(function() {
	$('.fancybox').fancybox();
});
</script>
<?php
	$member = (Auth::user()->role_id == role('member')) ? true : false;
?>
<div class="col-sm-12 text-right">
		@if(Auth::check())
			You are logged in as <b>{{Auth::user()->first_name}} {{Auth::user()->last_name}}</b> | <a href="{{url('/auth/logout')}}">Log out</a>
		@endif
		<a class="btn btn-sm details-sep" href="{{route('profile.myEvents')}}">My Profile »</a>
	</div>
<div class="row">
		@if(!empty($event->eventType->branding_image))
	    <div class="col-md-12">
			<div class="branding-cover" style="background: url('{{Config::get("app.url")."uploads/branding/".$event->eventType->branding_image}}') #F7F7F7; background-size: cover;">
	    @else
	    <div class="col-md-12">
			<div class="branding-cover" style="background: #F7F7F7">
	    @endif
	    
	    <h1 class="event-title">{{$event->title}}<br><!--<span><b>{{ date("l j F Y",strtotime($event->event_date_from)) }}</b></span>--></h1>
	    <div class="clearfix"></div>
	    </div>
	</div>
</div>

@include('partials.validationErrors')

<!-- BEGIN VALIDATION FORM WIZARD -->

        @if($status == config('registrationstatus.booked'))
            <div style="display:block;">
                <H2 style="text-align:center">Thank you for your payment for the event</H2>
            </div>
        @endif
        @if($status == config('registrationstatus.complete_manuall_review'))
            <div style="display:block;">
                <H2 style="text-align:center">{{ $description }}</H2>
            </div>
        @endif
        @if($status == config('registrationstatus.declined'))
            <div style="display:block;">
                <H2 style="text-align:center">{{ $description }}</H2>
            </div>
        @endif
        @if($status == config('registrationstatus.declined_by_bank'))
            <div style="display:block;">
                <H2 style="text-align:center">{{ $description }}</H2>
            </div>
        @endif
        @if($status == config('registrationstatus.pending'))
            <div style="display:block;">
                <H2 style="text-align:center">{{ $description }}</H2>
            </div>
        @endif    
        @if($status == config('registrationstatus.rejections_reference_validation'))
            <div style="display:block;">
                <H2 style="text-align:center">{{ $description }}</H2>
            </div>
        @endif                
            <div style="display:block; width:100%; margin-bottom: 1em;  ">
                <div style="display:inline-block; margin-bottom:1em; width:100%;">
                        @if($status == config('registrationstatus.booked'))
                            <div style="display:block;">
                                <H2 style="text-align:center">PAYMENT COMPLETE</H2>
                            </div>
                        @endif
                        @if($status == config('registrationstatus.complete_manuall_review'))
                            <div style="display:block;">
                                <H2 style="text-align:center">PAYMENT COMPLETE</H2>
                            </div>
                        @endif
                        @if($status == config('registrationstatus.declined'))
                            <div style="display:block;">
                                <H2 style="text-align:center">PAYMENT DECLINE</H2>
                            </div>
                        @endif
                        @if($status == config('registrationstatus.declined_by_bank'))
                            <div style="display:block;">
                                <H2 style="text-align:center">PAYMENT DECLINE BY BANK</H2>
                            </div>
                        @endif
                        @if($status == config('registrationstatus.pending'))
                            <div style="display:block;">
                                <H2 style="text-align:center">PAYMENT PENDING</H2>
                            </div>
                        @endif    
                        @if($status == config('registrationstatus.rejections_reference_validation'))
                            <div style="display:block;">
                                <H2 style="text-align:center">PAYMENT REJECTED</H2>
                            </div>
                        @endif    
                </div>
            </div>
            <div style="display:inline-block; margin-bottom:1em; width:100%;">
                <div style="display:block;">
                    <H2 style="text-align:center">Transaction ID: <b>{{$merchantTransactionId}}</b></H2>
                </div>
            </div>
<br>
<div class="col-sm-12 text-right">
    <td>
        <a href="{{route('events.editBooking', $event->slug)}}">View Booking details »</a>
    </td>
<div>
<br>
<!-- END VALIDATION FORM WIZARD -->


<script type="text/javascript" src="{{asset('assets/site/js/libs/wizard/jquery.bootstrap.wizard.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/site/js/libs/jquery-validation/dist/jquery.validate.min.js')}}"></script>
<script src="{{asset('assets/site/js/libs/wizard/jquery.bootstrap.wizard.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/site/js/core/source/AppForm.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/site/js/core/demo/DemoFormWizard.js')}}"></script>

@endsection