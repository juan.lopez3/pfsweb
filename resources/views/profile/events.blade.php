@extends('layouts.default')

@section('title')My Events @endsection

@section('content')
<script type="text/javascript" src="{{asset('assets/site/js/libs/DataTables/jquery.dataTables.min.js')}}"></script>
<!-- <script type="text/javascript" src="{{asset('assets/site/js/libs/DataTables/dataTables.bootstrap.js')}}"></script> -->
<link type="text/css" rel="stylesheet" href="{{asset('assets/site/css/jquery.dataTables.min.css')}}" />

<div class="wrapper">
	@include('partials.top')
	<div class="row">
		<div class="col-sm-10">
			<div class="homebox">
				<h1 class="event-title evento-titulo">My events</h1>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
					@if(Session::has('success'))
					<div class="alert alert-success">{{Session::get('success')}}</div>
					@endif

					<div class="row equal no-padding padding">
						<div class="col-md-4 col-sm-4">
							<div class="panel panel-default gradient">
								<p class="encajado">Your next event:</p>
								<div class="">
									@if(!empty($next_event))
									<p><a href="{{route('events.view', $next_event->slug)}}"><strong class="color-azul">{{$next_event->title}}</strong></a></p>
									<p>
										@if ($next_event->event_date_from == $next_event->event_date_to)
										{{ date('l d M Y',strtotime($next_event->event_date_from)) }}
										@else
										{{ date('d',strtotime($next_event->event_date_from)) }}-{{ date('d M Y',strtotime($next_event->event_date_to)) }}
										@endif
									</p>
									@if(sizeof($next_event->venue)) <p>{{$next_event->venue->city}}</p>@endif
									@else You have no upcoming events
									@endif
								</div>
							</div>
						</div>

						<div class="col-md-4 col-sm-4">
							<div class="panel panel-default gradient">
								<p class="encajado">Messages &amp; Reminders</p>
								<div class="">
									@if(empty($reminders) && !sizeof(Auth::user()->messages()->where('read', 0)->get())) <p>You have no reminders</p> @endif

									@foreach($reminders as $r)
									<p>{!!$r!!}</p>
									@endforeach

									@foreach(Auth::user()->messages()->where('read', 0)->get() as $m)
									<p><a style="cursor: pointer" data-toggle="modal" data-target="#myMessage{{$m->id}}">{{$m->title}}</a></p>
									<div id="myMessage{{$m->id}}" class="modal fade" role="dialog">
										<div class="modal-dialog">
											<!-- Modal content-->
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h4 class="modal-title">{{$m->title}}</h4>
												</div>
												<div class="modal-body">
													{{$m->message}}
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
									@endforeach
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-4">
							<div class="panel panel-default">
								<div class="">
									<div class="col-md-6">
										<p>
											@if(!empty($user->profile_image))
											{!! HTML::image('uploads/profile_images/'.$user->profile_image,'', ['class'=>'img-thumbnail', 'width'=>'300', 'height' => '300']) !!}
											@else
											{!! HTML::image('images/no_image.jpg','', ['class'=>'img-thumbnail', 'width'=>'300', 'height' => '300']) !!}
											@endif
										</p>
									</div>
									<div class="col-md-6">
										<p style="font-weight: bold;">{{Auth::user()->first_name}} {{Auth::user()->last_name}}</p>
										<a class="btn boton" href="{{route('profile.edit')}}">Edit profile </a><br /><br />
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="gradient gr_sm gr-pad">
								<p class="titulos-azules">Your booked forthcoming events</p>
							</div>
						</div>
					</div>



					<table id="forthcoming_events" class="table table-striped table-responsive head" cellspacing="0" width="100%">
						<thead class="events">
							<tr>
								<th class="centrar">Name</th>
								<th class="centrar">Date</th>
								<th class="centrar">Location</th>
								<th class="centrar">Status</th>
								@if(sizeof($user->subRoles) > 0)
								<th class="centrar">Contributor</th>
								@endif
								<th width="110" class="centrar">Actions</th>
							</tr>
						</thead>


						<tbody>
							@foreach ($user->events()->with('region')->where('event_date_from', '>=', date("Y-m-d"))->where('publish', 1)->get() as $e)
							@if($e->pivot->registration_status_id != \Config::get('registrationstatus.booked'))
							<tr>
								@else
							<tr>
								@endif
								<td class="centrar">{{$e->title}}</td>
								<td class="centrar">
								<td>
									@if ($e->event_date_from == $e->event_date_to)
									{{ date('l d M Y',strtotime($e->event_date_from)) }}
									@else
									{{ date('d',strtotime($e->event_date_from)) }}-{{ date('d M Y',strtotime($e->event_date_to)) }}
									@endif
								</td>
								</td>
								<td class="centrar">@if(sizeof($e->venue)) {{$e->venue->city}}@endif</td>
								<td class="centrar">{{Config::get('registrationstatus.'.$e->pivot->registration_status_id)}}</td>
								@if(sizeof($user->subRoles) > 0)
								<td class="centrar">{{implode(", ", $user->subRoles->lists('title'))}}</td>
								@endif
								<td class="centrar">
									@if($e->pivot->registration_status_id != \Config::get('registrationstatus.booked'))
									<a href="{{route('events.view', $e->slug)}}" title="View details">View details »</a>
									@if($e->pivot->registration_status_id == \Config::get('registrationstatus.waiting_payment'))
									<a href="{{route('events.payment', $e->atendees()->where('user_id', Auth::user()->id)->first()->pivot->id)}}" title="Payment">Payment »</a>
									@endif
									@else
									<a href="{{route('events.editBooking', $e->slug)}}" title="View details">View details »</a>
									@endif
								</td>


							</tr>


							@endforeach
						</tbody>
					</table>

					<!-- <hr class="events"/> -->

					<div class="row" style="margin-left:0px; margin-right:0px;">
						<div class="col-md-12">
							<div class="gradient gr_sm gr-pad" style="margin-left:-15px; margin-right:-15px;">
								<p class="titulos-azules">Your past events</p>
							</div>
						</div>


						<table id="previous_events" class="table table-striped table-responsive" cellspacing="0" width="100%">
							<thead class="events">
								<tr>
									<th class="centrar">Name</th>
									<th class="centrar">Date</th>
									<th class="centrar">Attendance</th>
									<th class="centrar">Feedback</th>
									<th class="centrar" width="110">Actions</th>
									<th class="centrar">CPD Certificate</th>
								</tr>
							</thead>

							<tbody>
								@foreach ($user->events()->with('region', 'tabs')->where('event_date_from', '<', date("Y-m-d"))->with('tabs', 'eventType')->get() as $e)
									<tr>
										<td class="centrar">{{$e->title}}</td>
										<td class="centrar">
											@if ($e->event_date_from == $e->event_date_to)
											{{ date('l d M Y',strtotime($e->event_date_from)) }}
											@else
											{{ date('d',strtotime($e->event_date_from)) }}-{{ date('d M Y',strtotime($e->event_date_to)) }}
											@endif
										</td>
										<td class="centrar">@if(!empty($e->atendees->find(Auth::user()->id)->pivot->checkin_time))Attended @else Not attended @endif</td>
										<td class="centrar">
											@if ($e->eventType->feedback_type == 'split')
											<a href="{{route('events.feedback', $e->slug)}}">Leave feedback </a>
											@elseif(empty($e->atendees->find(Auth::user()->id)->pivot->checkin_time)) Not attended
											@elseif(!in_array(config('tabs.tab_7'), $e->tabs->lists('id')))
											<!-- Tabs not active for the event -->
											@elseif(Auth::user()->feedbackAnswers()->where('event_id', $e->id)->count() == 0)
											<a href="{{route('events.feedback', $e->slug)}}">Leave feedback </a>
											@else Completed
											@endif
										</td>
										<td class="centrar">
											<a class="color-azul" href="{{route('events.editBooking', $e->slug)}}">View details </a>
										</td>
										<td class="centrar">
											@if($e->event_date_from < date("Y-m-d") && sizeof($e->atendees()->where('user_id', Auth::user()->id)->first()) > 0 && !empty($e->atendees()->where('user_id', Auth::user()->id)->first()->pivot->checkin_time) && $e->atendees()->where('user_id', Auth::user()->id)->first()->pivot->registration_status_id == \Config::get('registrationstatus.booked'))
												<a class="color-azul" href="{{route('events.getCertificate', $e->slug)}}">Download </a>
												@endif
										</td>
									</tr>
									@endforeach
							</tbody>
						</table>

						<!-- <hr class="events"/> -->

						<div class="row">
							<div class="col-md-12">
								<div class="gradient gr_sm gr-pad">
									<p class="titulos-azules">Recommendations for forthcoming events</p>
								</div>
							</div>
						</div>
						<table id="recommended_events" class="table table-striped table-responsive head" cellspacing="0" width="100%">
							<thead class="events">
								<tr>
									<th class="centrar">Name</th>
									<th class="centrar">Date</th>
									<th class="centrar">Location</th>
									<th class="centrar" width="110">Actions</th>
								</tr>
							</thead>

							<tbody>
								@foreach ($recommended_events as $e)
								<tr>
									<td class="centrar">{{$e->title}}</td>
									<td class="centrar">
										@if ($e->event_date_from == $e->event_date_to)
										{{ date('l d M Y',strtotime($e->event_date_from)) }}
										@else
										{{ date('d',strtotime($e->event_date_from)) }}-{{ date('d M Y',strtotime($e->event_date_to)) }}
										@endif
									</td>
									<td class="centrar">@if(sizeof($e->venue)) {{$e->venue->city}}@endif</td>
									<td class="centrar"><a class="color-azul" href="@if(!empty($e->outside_link)){{$e->outside_link}} @else{{route('events.view', $e->slug)}}@endif">View details </a></td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
				<br><br>

				<script type="text/javascript">
					$(document).ready(function() {
						$('#forthcoming_events').dataTable({
							"order": [
								[1, "desc"]
							],
							"info": false,
							'paging': false
						});

					});

					$(document).ready(function() {
						$('#previous_events').dataTable({
							"order": [
								[1, "desc"]
							],
							"info": false,
							"lengthMenu": [
								[10, 25, 50, 100, -1],
								[10, 25, 50, 100, "All"]
							],
							"oLanguage": {
								"sSearch": ""
							},
						});
						$('div.dataTables_length select').addClass('form-control input-sm');
						$('div.dataTables_filter input').addClass('form-control input-sm');
						$('.dataTables_filter input').attr("placeholder", "Search");
					});

					$(document).ready(function() {
						$('#recommended_events').dataTable({
							"order": [
								[1, "desc"]
							],
							"info": false,
							'paging': false,
							"oLanguage": {
								"sSearch": ""
							}
						});
						$('div.dataTables_filter input').addClass('form-control input-sm');
						$('.dataTables_filter input').attr("placeholder", "Search");
					});
				</script>
				@include('partials.deleteConfirmation')
				@endsection