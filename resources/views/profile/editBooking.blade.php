@extends('layouts.default')

@section('title'){{$event->title}} View / Edit @endsection

@section('content')
<script type="text/javascript" src="{{asset('assets/site/js/libs/DataTables/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/site/js/libs/DataTables/dataTables.bootstrap.js')}}"></script>
<link type="text/css" rel="stylesheet" href="{{asset('assets/site/css/jquery.dataTables.min.css')}}" />
<!-- <link href="http://addtocalendar.com/atc/1.5/atc-style-blue.css" rel="stylesheet" type="text/css"> -->

<script type="text/javascript" src="{{asset('assets/site/js/ouical.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyCFo1QWcA_GRpH7Q_jV14HF2PJ_3qzuiB8"></script>
<script type="text/javascript">
	// (function () {
	// if (window.addtocalendar)if(typeof window.addtocalendar.start == "function")return;
	// if (window.ifaddtocalendar == undefined) { window.ifaddtocalendar = 1;
	//     var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
	//     s.type = 'text/javascript';s.charset = 'UTF-8';s.async = true;
	//     s.src = ('https:' == window.location.protocol ? 'https' : 'http')+'://addtocalendar.com/atc/1.5/atc.min.js';
	//     var h = d[g]('body')[0];h.appendChild(s); }})();
</script>

<style>
	.col-md-1, .col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6{
		padding-left: 3px;
		padding-right: 3px;
	}
</style>

<div class="wrapper">
    @include('partials.validationErrors')
    
    @include('partials.top')
    
    <div class="panel panel-default">
    	<!--
    	<h3>@if($event->event_date_from > date("Y-m-d"))Forthcoming Event @else Past Event @endif</h3>
        <span class="backtoevent"><a href="{{route('profile.myEvents')}}">« Back to My Events</a></span>
    	-->
    	<div class="row">
    		@if(!empty($event->eventType->branding_image))
    		    <div class="col-md-12">
    				<div class="branding-cover" >
    		@else
    		    <div class="col-md-12">
    				<div class="branding-cover" >
    		@endif
    			<h1 class="event-title evento-titulo">{{$event->title}}</h1>
    			<div class="clearfix"></div>
    			</div>
    		</div>
    	</div>
    
    	<div class="row">
    		<div class="col-md-8 col-sm-8 col-xs-12">
    			@if(sizeof($event->venue))
    
    			<div class="gradient gr_sm gr-pad">
    				<h3 class="titulo-about">@if($event->event_date_from >= date("Y-m-d")) Forthcoming event @else Previous event @endif</h3>
    			</div>
    			<div class="col-sm-6" style="padding-right: 0px; padding-left: 0px;">
    				<h5 class="titulo-Programme">Date:</h5>
    				<p>
    					@if ($event->event_date_from == $event->event_date_to)
    						{{ date('l d M Y',strtotime($event->event_date_from)) }}
    					@else
    						{{ date('d',strtotime($event->event_date_from)) }}-{{ date('d M Y',strtotime($event->event_date_to)) }}
    					@endif
    				</p>
    				<br />
    				<h5 class="titulo-Programme">Venue:</h5>
    				@if(!empty($event->venue->website))
    				<p><a href="{{$event->venue->website}}" rel="nofollow" target="_blank" class="color-azul" >{{$event->venue->name}}</a><br/>
    				@else
    				{{$event->venue->name}} <br/>
    				@endif
    				{{$event->venue->address}}, {{$event->venue->city}}, {{$event->venue->postcode}}<br/></p>
				@endif
    				<a href="{{route('events.view', $event->slug)}}" style="width:170px" class="btn boton">View full event details </a>
					<br><br>
				</div>
    			<div class="col-sm-6" style="padding-right: 0px; padding-left: 0px;">
    			@if(sizeof($event->venue) > 0 && in_array(\Config::get('tabs.tab_4'), $event->tabs()->lists('id')))
    				@if($event->venue->show_map)
    				<div id="map-event" style="height:250px;"></div>
    				@endif
    				@endif
    			</div>
    		</div>
    
    		<div class="col-md-4 col-sm-4 col-xs-12">
    			<div class="gradient sidebar">
    				<h3 class="titulo-about" style="padding-right: 0px; padding-left: 10px !important;">Event details</h3>
    				<div class="sidebar-inner">
    				<p><b>Dietary Requirements:</b>{{isset(Auth::user()->dietaryRequirement) ? Auth::user()->dietaryRequirement->title : ""}}<br/>
    				<b>Special requirements:</b> @if(empty(Auth::user()->special_requirements))No special requirements @else{{Auth::user()->special_requirements}}@endif</p>
					
					@if(in_array(\Auth::user()->events()->where('slug', $event->slug)->first()->pivot->registration_status_id, [config('registrationstatus.booked'), config('registrationstatus.waitlisted'), config('registrationstatus.waiting_payment')]))
	            	@if($event->event_date_from > date("Y-m-d") && ((empty($event->amendment_date) || $event->amendment_date > date("Y-m-d"))))
		            	<a href="{{route('events.editRegistration', $event->slug)}}" class="btn boton">Edit Booking »</a>
	            	@elseif($event->event_date_from < date("Y-m-d"))
	            		@if(in_array(\Config::get('tabs.tab_7'), $event->tabs()->lists('id')))
	            		<br><br>
	            		<a href="{{route('events.feedback', $event->slug)}}" style="width:150px;" class="btn boton">Leave feedback »</a>
	            		@endif
	            	@endif

	            	@if($event->event_date_from >= date("Y-m-d"))
	            	<br><br>
	            		<a href="#" class="btn boton" title="Cancel Booking" data-toggle="modal" data-target="#confirm-delete">Cancel Booking »</a>
	                @endif

	                @if($event->event_date_from < date("Y-m-d") && sizeof($event->atendees()->where('user_id', Auth::user()->id)->first()) > 0 && !empty($event->atendees()->where('user_id', Auth::user()->id)->first()->pivot->checkin_time) && $event->atendees()->where('user_id', Auth::user()->id)->first()->pivot->registration_status_id == \Config::get('registrationstatus.booked'))
	                <br><br>
	                <a href="{{route('events.getCertificate', $event->slug)}}" style="width:200px" class="btn boton">Certificate of attendance »</a>
	                @endif
				@endif
    
    				@if($event->cpd_download)
    				<br><br>
    					<a style="width:150px;" class="btn boton" href="{{route('events.agenda', $event->slug)}}">Download agenda </a>
    				@endif
    
    				<br/>
    			    <div class="btn-group new-cal"></div>
    				  <script>
    				      var myCalendar = createCalendar({
    				        options: {
    				          class: 'cal-class antes',
    				          id: 'dropdownCal1'  // You need to pass an ID. If you don't, one will be generated for you.
    				        },
    				        data: {
    				          title: '{{$event->title}}', // Event title
    				          start: new Date('{{$event->event_date_from}}'), // Event start date
    				          end: new Date('@if(!empty($event->event_date_to)){{$event->event_date_to}}@else{{$event->event_date_from}}@endif'),     // You can also choose to set an end time.
    				          // If an end time is set, this will take precedence over duration
    				          address: '@if(sizeof($event->venue)){{$event->venue->address}}@endif',
    				          description: '@foreach($booked_sessions as $s){{$s->slot->title}} {{date("H:i",strtotime($s->slot->start))}} - {{date("H:i", strtotime($s->slot->end))}} \n @endforeach \n{{route("events.view", $event->slug)}}'
    				        }
    				      });
    				      document.querySelector('.new-cal').appendChild(myCalendar);
    				  </script>
    				  </div>
                </div>
    		</div>
    	</div>
    	@if ($event->schedule_version == 1)
    					<div class="gradient gr_sm gr-pad">
    						<h3>Personal planner</h3>
    					</div>
    
    	<div class="tabs">
    		<ul class="nav nav-tabs">
    			<?php $first = true; ?>
    			@foreach ($event_days as $seq => $day)
    				<li class="@if($first) active @endif"><a href="#day{{$day}}" data-toggle="tab">DAY {{$seq+1}} - {{date("d/m/Y", strtotime($day))}}</a></li>
    				<?php $first = false; ?>
    			@endforeach
    				<li><a href="#personal-schedule" onclick="location.reload();" data-toggle="tab"><img height="18px" src="{{asset('images/star.png')}}" /> Personal schedule</a></li>
    		</ul>
    		<div class="tab-content" style="overflow-x: hidden">
    			<?php $first = true; ?>
    			@foreach ($event_days as $day)
    				<div id="day{{$day}}" class="tab-pane @if($first)active @endif">
    
    					<?php $explanation_text = $event->scheduleExplanationTexts()->where('day', $day)->first(); ?>
    
    					@if (sizeof($explanation_text))
    						<div class="row">
    							<div class="col-xs-12"></div>
    						</div>
    						{!! $explanation_text->text !!}
    					@endif
    
    					@if (isset($schedule_header[$day]))
    						<?php
    						if (sizeof($schedule_header[$day]) >= 5) {
    							$column_width = 2;
    						} elseif (sizeof($schedule_header[$day]) == 0) {
    							$column_width = 12;
    						} else {
    							$column_width = 12 / sizeof($schedule_header[$day]);
    						}
    						?>
    						<div class="row">
    							<div class="col-xs-offset-1 col-xs-11 text-center">
    								@foreach ($schedule_header[$day] as $title)
    									<div class="hidden-sm hidden-xs col-md-{{$column_width}}"><b>{{$title}}</b></div>
    								@endforeach
    							</div>
    						</div>
    					@endif
    						
    					@foreach ($day_slots[$day] as $slot)
    						<?php
    						$start = strtotime($slot->start);
    						$end = strtotime($slot->end);
    						$slot_height = (int) round(abs($end - $start) / 60) * 3;
    						?>
    
    						<div class="row slot-row">
    							<div class="col-xs-1 text-center">
    								@if ($slot->show_start) {{date("H:i", strtotime($slot->start))}} <br/> @endif
    
    								@if ($slot->show_end) {{date("H:i", strtotime($slot->end))}} @endif
    							</div>
    							<div class="col-xs-11">
    								<?php
                                    if ($slot->columns >= 5) {
                                        $column_width = 2;
                                    } elseif ($slot->columns == 0) {
                                        $column_width = 12;
                                    } else {
                                        $column_width = 12 / $slot->columns;
                                    }
    								?>
    								@for($i=1; $i <=$slot->columns; $i++)
    									<?php
    									// for each slot clean top start time for correct gap calculation
    									$start = strtotime($slot->start);
    									?>
    
    									<div class="col-md-{{$column_width}}">
    										@foreach ($slot->sessions()->where('order_column', $i)->orderBy('session_start')->with('type', 'contributors')->get() as $session)
    											<?php
    											$session_start = strtotime($session->session_start);
    											$session_end = strtotime($session->session_end);
    											$session_gap = (int) round(abs($start - $session_start) / 60) * 3;
    											$session_height = (int) round(abs($session_end - $session_start) / 60) * 3;
                                                $start = $session_end;
    											?>
    											<div class="session col-md-12" style="min-height: 130px; background: {{$session->type->color}}; height: {{$session_height}}px; margin-top: {{$session_gap}}px;">
    												<div class="@if ($session->selectable)col-xs-10 @else col-xs-12 @endif session-row">
    													<span class="time">
    														<div class="visible-xs visible-sm">{{$session->meetingSpace->meeting_space_name}}<br/></div>
    													</span>
    													<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
    
    
    													<a class="more-details" style="cursor:pointer;" data-toggle="modal" data-target="#more_details{{$session->id}}"><img height="20px" src="{{asset('images/info.png')}}" title="More Details" alt="More Details" /></a>
    													<div id="more_details{{$session->id}}" class="modal fade text-left" role="dialog">
    														<div class="modal-dialog modal-lg">
    															<div class="modal-content">
    																<div class="modal-header">
    																	<button type="button" class="close" data-dismiss="modal">&times;</button>
    																	<h4 class="modal-title">
    																		{{date("H:i", strtotime($session->session_start))}} - {{date("H:i", strtotime($session->session_end))}} {{$session->title}} <br/>
    																		<img height="15px" src="{{asset('images/pin.png')}}" /> {{$session->meetingSpace->meeting_space_name}}</span> - {{$session->type->title}}
    																	</h4>
    																</div>
    																<div class="modal-body">
    																	<div class="row">
    																		<div class="col-xs-12">
    																			{!! $session->description !!}
    
    																			@if (!empty($session->learning_objectives))
    																				<h4><b>Learning objectives</b></h4>
    																				{!! $session->learning_objectives !!}
    																			@endif
    
    
    																			@foreach($session->contributors as $contributor)
    																				<div class="row">
    																					@if(!empty($contributor->profile_image))
    																						<div class="col-sm-3">
    																							<br/>
    																							{!! HTML::image('uploads/profile_images/'.$contributor->profile_image,'', ['class'=>'img-responsive', 'width'=>'150']) !!}
    																						</div>
    																					@endif
    																					<div class="col-sm-9">
    																						<h4>{{$contributor->badge_name}}</h4>
    																						<p><b>{{$contributor->pivot->type}}</b></p>
    																						@if(!empty($contributor->company))
    																							<p><b>Company: </b>{{$contributor->company}}</p>
    																						@endif
    																						@if (!empty($contributor->bio))
    																							<b>Bio:</b>
    																							<p>{{$contributor->bio}}</p>
    																						@endif
    																					</div>
    																				</div>
    																			@endforeach
    																		</div>
    																	</div>
    																</div>
    															</div>
    														</div>
    													</div>
    
    
    													<span class="session_title">@if (sizeof($slot->sessions) > 2 && strlen($session->title) > 82 && sizeof($session->contributors)){{substr($session->title, 0, 80)}}... @else{{$session->title}} @endif</span>
    													@foreach($session->contributors as $contributor)
    														<br/>{{$contributor->first_name}} {{$contributor->last_name}}
    													@endforeach
    												</div>
    
    												@if ($session->selectable)
    												<div class="col-xs-2 star text-right">
    													@if(in_array($session->id, $favourite_sessions))
    														<img height="25px" id="session-start{{$session->id}}" class="favourite slot{{$slot->id}}" data-radio-toggle="session{{$session->id}}" src="{{asset('images/starred.png')}}" />
    													@else
    														<img height="25px" id="session-start{{$session->id}}" class="favourite slot{{$slot->id}}" data-radio-toggle="session{{$session->id}}" src="{{asset('images/star.png')}}" />
    													@endif
    													<div style="display: none;"><input type="checkbox" id="session{{$session->id}}" name="slot{{$slot->id}}" value="{{$session->id}}" class="favourite-session" title="Add to Favourite" @if(in_array($session->id, $favourite_sessions))checked @endif /><br/></div>
    												</div>
    												@endif
    											</div>
    										@endforeach
    									</div>
    								@endfor
    							</div>
    						</div>
    					@endforeach
    				</div>
    				<?php $first = false; ?>
    			@endforeach
    
    			<div id="personal-schedule" class="tab-pane">
    				<div class="row">
    					<div class="col-xs-12">
    						@foreach ($personal_schedule as $day => $sessions)
    							<h3>Day {{array_search($day, $event_days) + 1}} - {{date("d/m/Y", strtotime($day))}}</h3>
    
    							@foreach ($sessions as $session)
    								<div class="row slot-row">
    									<div class="col-xs-12">
    									<div class="session col-md-12" style="background: {{$session->type->color}}; padding: 10px;">
    										<div class="col-xs-10">
    											<span class="time">{{date("H:i", strtotime($session->session_start))}} - {{date("H:i", strtotime($session->session_end))}} <img height="15px" src="{{asset('images/pin.png')}}" /> {{$session->meetingSpace->meeting_space_name}}</span><br/>
    											<span class="type">{{$session->type->title}} <br/></span>
    											<span class="session_title">{{$session->title}}</span>
    
    											@foreach($session->contributors()->where('type', 'Speaker')->get() as $contributor)
    												<br/>{{$contributor->first_name}} {{$contributor->last_name}}
    											@endforeach
    										</div>
    
    										<div class="col-xs-2">
    											<div class="text-right">
    												@if (!empty($session->description) || !empty($session->learning_objectives) || sizeof($session->contributors))
    													<a class="more-details" style="cursor:pointer;" data-toggle="modal" data-target="#personal-more_details{{$session->id}}"><img height="20px" src="{{asset('images/info.png')}}" title="More Details" alt="More Details" /></a>
    												@endif
    												<img height="25px" class="favourite slot{{$slot->id}}" data-radio-toggle="session{{$session->id}}" src="{{asset('images/starred.png')}}" />
    											</div>
    
    											@if (!empty($session->description) || !empty($session->learning_objectives) || sizeof($session->contributors))
    												<div id="personal-more_details{{$session->id}}" class="modal fade" role="dialog">
    													<div class="modal-dialog modal-lg">
    														<div class="modal-content">
    															<div class="modal-header">
    																<button type="button" class="close" data-dismiss="modal">&times;</button>
    																<h4 class="modal-title">
    																	{{date("H:i", strtotime($session->session_start))}} - {{date("H:i", strtotime($session->session_end))}} {{$session->title}}<br/>
    																	<img height="15px" src="{{asset('images/pin.png')}}" /> {{$session->meetingSpace->meeting_space_name}}</span> - {{$session->type->title}}
    																</h4>
    															</div>
    															<div class="modal-body">
    																<div class="row">
    																	<div class="col-xs-12">
    																		{!! $session->description !!}
    
    																		@if (!empty($session->learning_objectives))
    																			<h4><b>Learning objectives</b></h4>
    																			{!! $session->learning_objectives !!}
    																		@endif
    
    
    																		@foreach($session->contributors as $contributor)
    																			<div class="row">
    																				@if(!empty($contributor->profile_image))
    																					<div class="col-sm-3">
    																						<br/>
    																						{!! HTML::image('uploads/profile_images/'.$contributor->profile_image,'', ['class'=>'img-responsive', 'width'=>'150']) !!}
    																					</div>
    																				@endif
    																				<div class="col-sm-9">
    																					<h4>{{$contributor->badge_name}}</h4>
    																					<p><b>{{$contributor->pivot->type}}</b></p>
    																					@if(!empty($contributor->company))
    																						<p><b>Company: </b>{{$contributor->company}}</p>
    																					@endif
    																					@if (!empty($contributor->bio))
    																						<b>Bio:</b>
    																						<p>{{$contributor->bio}}</p>
    																					@endif
    																				</div>
    																			</div>
    																		@endforeach
    																	</div>
    																</div>
    															</div>
    														</div>
    													</div>
    												</div>
    											@endif
    										</div>
    									</div>
    									</div>
    								</div>
    							@endforeach
    						@endforeach
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    
    	<script type="text/javascript">
    		$(".favourite").click(function () {
    			var clicked_radio = $("#"+$(this).data('radio-toggle'));
                var star = "{{asset('images/star.png')}}";
                var starred = "{{asset('images/starred.png')}}";
                // if clicked the same
    			if (clicked_radio.is(':checked')) {
    				// deselected clear slot stars
                    // $("."+clicked_radio.attr('name')).attr('src', star); deselect all for radio
                    $(this).attr('src', star);
                    clicked_radio.prop('checked', true);
                    clicked_radio.prop('checked', false).change();
    			} else {
    			    // clear previous selection set a new
                    //$("."+clicked_radio.attr('name')).attr('src', star); // deselection for radio
                    $(this).attr('src', starred);
                    clicked_radio.prop('checked', true).change();
    			}
            })
    
    		// send changed request
    		$(".favourite-session").bind('change', function () {
    		    // toggle radio
    			$.ajax({
    				type: "POST",
    				data: {session_id: this.value, checked: this.checked, event_id: {{$event->id}} },
    				url: "{{route('events.favouriteSessionClicked')}}",
    				success: function (resp) {
                        var star = "{{asset('images/star.png')}}";
    					// deselect overlapping sessions
                        $.each(resp, function(index, value) {
                            $("#session"+value).prop('checked', false);
                            $("#session-start"+value).attr('src', star);
                        });
                    }
    			});
    		});
    
    		var allRadios = document.getElementsByClassName('favourite-session');
    		var booRadio;
    		var x = 0;
    		for(x = 0; x < allRadios.length; x++){
    			allRadios[x].onclick = function() {
    				if(booRadio == this){
    					this.checked = false;
    					booRadio = null;
    					$.ajax({
    						type: "POST",
    						data: {session_id: this.value, checked: false, event_id: {{$event->id}} },
    						url: "{{route('events.favouriteSessionClicked')}}",
    						success: function(resp) {
                                $.each(resp, function(index, value) {
                                    $("#session"+value).prop('checked', false);
                                    $("#session-start"+value).attr('src', star);
                                });
    						}
    					});
    				}else{
    					booRadio = this;
    				}
    			};
    		}
    
    		$(function() {
    			// for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
    			$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    				// save the latest tab; use cookies if you like 'em better:
    				localStorage.setItem('lastTab', $(this).attr('href'));
    			});
    
    			// go to the latest tab, if it exists:
    			var lastTab = localStorage.getItem('lastTab');
    			if (lastTab) {
    				$('[href="' + lastTab + '"]').tab('show');
    			}
    		});
    	</script>
    
    	@else
    	<div class="row">
    		<div class="col-md-12">
    			<div class="gradient gr_sm gr-pad "><h3 class="titulo-about">Sessions</h3>
    			</div>
    		</div>
    	</div>
    
        <div class="row">
            <div class="col-xs-12">
                <table id="sessions" class="table table-striped table-responsive head" cellspacing="0" width="100%">
    				<thead class="events">
    					<tr>
    						<th class="centrar">Time</th>
    						<th class="centrar">Name</th>
    						<th class="centrar">Booking status</th>
    					</tr>
    				</thead>
    
    				<tbody>
    					@foreach($event->scheduleTimeSlots()->where('bookable', 1)->orderBy('start')->get() as $s)
    					<?php
    						$show = false;
    						$slot_attendee_types = $s->attendeeTypes->lists('id');
    
    						$full = ($s->slot_capacity <= $s->attendees()->whereIn('registration_status_id', [config('registrationstatus.booked'), config('registrationstatus.waitlisted')])->count() || $s->stop_booking) ? true : false;
    
    						foreach($user_attendee_types as $uat) {
    
    							// if attendee has at least 1 attende type assigned to slot attendee types
    							if(in_array($uat, $slot_attendee_types)) {
    								$show = true;
    								break;
    							}
    						}
    						//check chartered status if attendee type not found
    						if(!$show)
    							if($s->available_chartered && Auth::user()->chartered) $show = true;
    
    						// if attendee has no types and not chartered don't show the slot
    						if(!$show) continue;
    					?>
    					<tr>
    						<td class="centrar">
    							{{date("H:i", strtotime($s->start))}} - {{date("H:i", strtotime($s->end))}}
    							@if($full)
    							<span class="full-slot">Waiting list</span>
    							@endif
    						</td>
    						<td class="centrar">{{$s->title}}</td>
    						<td class="centrar" style="font-weight: bold;">
    							@if(sizeof($booked_sessions->where('event_schedule_slot_id', $s->id)->first()) > 0)
    							{{$booked_sessions->where('event_schedule_slot_id', $s->id)->first()->status->title}}
    							@else
    							Not booked
    							@endif
    
    						</td>
    					</tr>
    					@endforeach
    				</tbody>
    			</table>
            </div>
        </div>
    	@endif
    
        @if(sizeof($event_materials) > 0 && in_array(\Config::get('tabs.tab_6'), $event->tabs()->lists('id')))
        	<div class="row">
    		  <div class="col-md-12">
    		    <div class="gradient gr_sm gr-pad"><h3 class="titulo-about">Related documents:</h3>
    		    </div>
    		  </div>
    		  <div class="col-md-12">
    		  	<ul class="files">
            		@foreach($event_materials as $doc)
            			<li>
            				<a href="{{Config::get('app.url')}}uploads/files/{{$doc->document_name}}"><span class="file-icon"></span>{{$doc->display_name}}</a>
            			</li>
            		@endforeach
    
            		@foreach($event->eventType->files as $doc)
                        <li>
                            <a href="{{Config::get('app.url')}}uploads/files/{{$doc->document_name}}"><span class="file-icon"></span>{{$doc->display_name}}</a>
                        </li>
                    @endforeach
            	</ul>
    		</div>
        @endif
        <div class="clearfix"></div>
    
    @if ($event->schedule_version == 0)
        @if($event->event_date_from > date("Y-m-d"))
        <br/><br/>
    	<div class="col-md-12">
            <div class="alert alert-info" role="alert">
            	<h5>Amending your reservation</h5>
                <p>	
    				<?php
    					/*
    					*There is a weird problem with the variable "$event->eventType->amendment_text".
    					*If this  variable isn't access first it wont have value.
    					*/
    
    					$amendment_text = $event->eventType->amendment_text;
    				?>
    
    				@if(!empty(rtrim(strip_tags($amendment_text))))
                	                     	{!! $event->eventType->amendment_text !!}
                               	@else
    					<p>Demand for places at Personal Finance Society regional events remains high. If for any reason you are no longer able to attend please let us know immediately so that we can re-allocate your place. </p>
    					<p>If you have any further questions please take a look at our commonly asked questions. Alternatively please get in touch by email at regionals@pfsevents.org or by telephone on 0207 808 5616.</p>
    					<p>As part of your registration you agreed with the terms and conditions which includes agreeing to pay a £50 charge if you failed to attend the Conference and provide us with 2 working days (09:00am 2 working days before the event, excluding weekends) notice. If you are no longer available to attend, please inform us of this as soon as possible.</p>
    					<p>You can do this by amending your registration above.</p>
    
    				@endif
    			</p>
            </div>
        </div>
        @elseif($event->event_date_from < date("Y-m-d") && sizeof($event->atendees()->where('registration_status_id', 1)->where('user_id', Auth::user()->id)->first()) > 0 && is_null($event->atendees()->where('user_id', Auth::user()->id)->first()->pivot->checkin_time) && $event->atendees()->where('user_id', Auth::user()->id)->first()->pivot->paid == 0 && $event->atendees()->where('user_id', Auth::user()->id)->first()->pivot->fee_waived == 0)
    	<div class="col-md-12">
    		<div class="alert alert-danger" role="alert">
            	@if(!empty($event->eventType->cancel_text))
            		{!! $event->eventType->cancel_text !!}
            	@else
                <p>Our records indicate you did not attend this event. You are subject to a non-attendance fee, please make payment online contact Tel: 0207 808 5616</p>
    			<p>If you believe this is an error in your attendance record, please contact Tel: 0207 808 5616 Email:regionals@pfsevents.org</p>
    			@endif
            </div>
        </div>
        @endif
    @endif
    </div>
    
    
    <div class="row">
    	<div class="col-md-12 col-sm-12 col-xs-12">
        </div>
    </div>
    
    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    	<div class="modal-dialog">
    		<div class="modal-content">
    			{!! Form::open(['route' => ['events.cancelBooking', $event->slug]], ['class' => 'form']) !!}
    			<div class="modal-header">
    				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    				<h4 class="modal-title">Cancel booking</h4>
    			</div>
    			<div class="modal-body">
    				<p>Are you sure you want to cancel booking for this event?</p>
    				{!! Form::textarea('cancel_reason', null, ['class' => 'form-control', 'required', 'rows'=>3, 'placeholder' => 'Please provide booking cancellation reason']) !!}
    			</div>
    			<div class="modal-footer">
    				<button type="button" class="btn boton" data-dismiss="modal">Close window</button>
    				{!! Form::submit('Cancel booking', ['class'=>'btn boton btn-danger btn-ok']) !!}
    			</div>
    			{!! Form::close() !!}
    		</div>
    	</div>
    </div>
</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$('#myTab').tabCollapse();

	$( "#more_details" ).click(function() {
		//var $this = $(this);
	  $( "#details" ).toggle( "slow", function() {
        //$("html,body").animate({scrollTop: $this.offset().top});
        $("html, body").animate({scrollTop: $("#details").offset().top});
	  });
	});
})

@if(sizeof($event->venue) && $event->venue->show_map)
var infowindow = null;

function initialize() {

	//starting map focus location
	@if(!empty($event->venue))
	var mypoint = new google.maps.LatLng({{$event->venue->latitude}}, {{$event->venue->longitude}});
	@else
	var mypoint = new google.maps.LatLng();
	@endif

	var mapOptions = {
		zoom : 13,
		center : mypoint
	}

	var map = new google.maps.Map(document.getElementById('map-event'), mapOptions);

	infowindow = new google.maps.InfoWindow({
		content : "holding..",
		maxWidth: 250
	});

	// get filtered events
	/**
	 * Data for the markers consisting of a name, a LatLng and a zIndex for
	 * the order in which these markers should display on top of each
	 * other.
	 */

    setMarker(map);
}

function setMarker(map) {
	// Marker sizes are expressed as a Size of X,Y
	// where the origin of the image (0,0) is located
	// in the top left of the image.
	// Origins, anchor positions and coordinates of the marker
	// increase in the X direction to the right and in
	// the Y direction down.
	var image = {
		url: '{{\Config::get('app.url')}}images/marker.png',
		// This marker is 20 pixels wide by 32 pixels tall.
		size: new google.maps.Size(20, 20),
		// The origin for this image is 0,0.
		origin: new google.maps.Point(0,0),
		// The anchor for this image is the base of the flagpole at 0,32.
		anchor:
		new google.maps.Point(10, 20)
	};
	// Shapes define the clickable region of the icon.
	// The type defines an HTML &lt;area&gt; element 'poly' which
	// traces out a polygon as a series of X,Y points. The final
	// coordinate closes the poly by connecting to the first
	// coordinate.
	var shape = {
		coords : [1, 1, 1, 20, 18, 20, 18, 1],
		type : 'poly'
	};

		@if(!empty($event->venue))
		var myLatLng = new google.maps.LatLng({{$event->venue->latitude}}, {{$event->venue->longitude}});
		@else
		var myLatLng = new google.maps.LatLng();
		@endif

		var marker = new google.maps.Marker({
			position : myLatLng,
			map : map,
			icon : image,
			shape : shape,
			@if(!empty($event->venue))
			title : '{{$event->venue->name}}',
			description : '<h4>{{$event->venue->name}}</h4><address>{{$event->venue->address}}<br />{{$event->venue->city}}<br />{{$event->venue->county}}<br />{{$event->venue->postcode}}</address><br />'
			@else
			title : '',
			description : ''
			@endif
		});

		google.maps.event.addListener(marker, 'click', function() {

			infowindow.setContent(this.description);
			infowindow.open(map, this);
		});
}

google.maps.event.addDomListener(window, 'load', initialize);
@endif
</script>

@endsection
