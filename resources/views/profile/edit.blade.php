@extends('layouts.default')

@section('title')Edit My Event Profile @endsection

@section('content')
<?php
	$member = (Auth::user()->role_id == role('member')) ? true : false;
?>
<div class="wrapper">
@include('partials.top')
    <div class="row">
    	<div class="col-sm-10">
    		<div class="homebox">
    			<h1 class="event-title evento-titulo">Edit my event profile</h1>
    			<div class="clearfix"></div>
    		</div>
    	</div>
    </div>
    
    <link type="text/css" rel="stylesheet" href="{{asset('assets/site/css/theme-default/libs/select2/select2.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/site/css/theme-default/libs/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/admin/css/theme-default/font-awesome.min.css')}}" />
    
    <script src="{{asset('assets/site/js/libs/select2/select2.min.js')}}"></script>
    <script src="{{asset('assets/site/js/libs/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/site/js/libs/jquery-validation/dist/additional-methods.min.js')}}"></script>
    <script src="{{asset('assets/site/js/core/source/AppForm.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/admin/js/core/demo/DemoFormComponents.js')}}"></script>
    
    <link type="text/css" rel="stylesheet" href="{{asset('assets/admin/js/libs/jquery-fileupload/uploadfile.css')}}" />
    <script type="text/javascript" src="{{asset('assets/admin/js/libs/jquery-fileupload/jquery.uploadfile.min.js')}}"></script>
    
    <div class="row">
    	<div class="col-md-12">
    		<div class="panel panel-default">
    			<div class="">
    				
    				@include('partials.validationErrors')
    				<div class="row">
    					<div class="col-sm-6">
    						<span class="profile-sub-title titulo-Programme">Contact details</span> @if ($user->role_id == config('roles.member')) <a href="https://www.thepfs.org/my-pfs/edit-profile?returnUrl={{route('events.updateProfile', [Auth::user()->pin, Auth::user()->email])}}?redirect={{route('profile.edit')}}" class="btn boton">Edit details </a> @endif
    					</div>
    				
    					<div class="col-sm-6 hidden-xs">
    						<span class="profile-sub-title titulo-Programme">Event details</span> <span class="titulo-Programme">- edit event details below</span> 
						</div>
    				</div>
    				
    				<br/>
    				
    				{!! Form::model($user, ['route' => ['profile.update'], 'method' => 'PATCH', 'class' => 'form-horizontal form-validate']) !!}
    
    				<div class="row">
    					<div class="col-sm-6">
    						@if ($user->role_id == config('roles.non_member'))
    
    							<div class="form-group">
    								<div class="col-sm-3">
    									{!! Form::label('title', 'Title', ['class' => 'control-label']) !!}
    								</div>
    								<div class="col-sm-9 ">
    									{!! Form::select('title', ['Mr'=>'Mr', 'Ms' => 'Ms', 'Mrs'=>'Mrs', 'Miss'=>'Miss', 'Dr'=>'Dr'], null, ['class'=>'form-control form-control-input']) !!}
    								</div>
    							</div>
    
    							<div class="form-group">
    								<div class="col-sm-3">
    									{!! Form::label('first_name', 'First name', ['class' => 'control-label']) !!}
    								</div>
    								<div class="col-sm-9">
    									{!! Form::text('first_name', null, ['class'=>'form-control form-control-input', 'required']) !!}
    								</div>
    							</div>
    
    							<div class="form-group">
    								<div class="col-sm-3">
    									{!! Form::label('last_name', 'Last name', ['class' => 'control-label']) !!}
    								</div>
    								<div class="col-sm-9 ">
    									{!! Form::text('last_name', null, ['class'=>'form-control form-control-input', 'required']) !!}
    								</div>
    							</div>
    
    							<div class="form-group">
    								<div class="col-sm-3">
    									{!! Form::label('email', 'Email', ['class' => 'control-label']) !!}
    								</div>
    								<div class="col-sm-9">
    									{!! Form::input('email', 'email', null, ['class'=>'form-control form-control-input', 'required']) !!}
    								</div>
    							</div>
    
    							<div class="form-group">
    								<div class="col-sm-3">
    									{!! Form::label('phone', 'Telephone', ['class' => 'control-label']) !!}
    								</div>
    								<div class="col-sm-9">
    									{!! Form::text('phone', null, ['class'=>'form-control form-control-input']) !!}
    								</div>
    							</div>
    
    							<div class="form-group">
    								<div class="col-sm-3">
    									{!! Form::label('mobile', 'Mobile', ['class' => 'control-label']) !!}
    								</div>
    								<div class="col-sm-9">
    									{!! Form::text('mobile', null, ['class'=>'form-control form-control-input']) !!}
    								</div>
    							</div>
    
    							<div class="form-group">
    								<div class="col-sm-3">
    									{!! Form::label('fca_number', 'FCA Number', ['class' => 'control-label']) !!}
    								</div>
    								<div class="col-sm-9">
    									{!! Form::text('fca_number', null, ['class'=>'form-control form-control-input']) !!}
    								</div>
    							</div>
    
    							<div class="form-group">
    								<div class="col-sm-3">
    									{!! Form::label('company', 'Company', ['class' => 'control-label']) !!}
    								</div>
    								<div class="col-sm-9">
    									{!! Form::text('company', null, ['class'=>'form-control form-control-input']) !!}
    								</div>
    							</div>
    
    							<div class="form-group">
    								<div class="col-sm-3">
    									{!! Form::label('postcode', 'Postcode', ['class' => 'control-label']) !!}
    								</div>
    								<div class="col-sm-9">
    									{!! Form::text('postcode', null, ['class'=>'form-control form-control-input ', 'required']) !!}
    								</div>
    							</div>
    
    							<div class="form-group">
    								<div class="col-sm-3">
    									{!! Form::label('country_id', 'Country', ['class' => 'control-label']) !!}
    								</div>
    								<div class="col-sm-9">
    									{!! Form::select('country_id', $countries, null, ['class'=>'form-control form-control-input', 'required']) !!}
    								</div>
    							</div>
    
    							<div class="col-xs-12 text-right">
    								<p><button type="button" id="toggle-pw" style="width:150px" class="btn boton">Change password</button></p>
    							</div>
    
    							<div id="password_change">
    								<div class="form-group">
    									{!! Form::label('password', 'Password', ['class' => 'col-sm-3 control-label']) !!}
    									<div class="col-sm-9">
    										{!! Form::password('password', ['class'=>'form-control form-control-input', 'required']) !!}
    									</div>
    								</div>
    
    								<div class="form-group">
    									{!! Form::label('password_confirmation', 'Confirm password', ['class' => 'col-sm-3 control-label']) !!}
    									<div class="col-sm-9">
    										{!! Form::password('password_confirmation', ['class'=>'form-control form-control-input', 'required']) !!}
    									</div>
    								</div>
    							</div>
    							<br/><br/>
    						@else
    						<dl>
    							<dt>Title</dt>
    							<dd>{{$user->title}}</dd>
    							
    							<dt>First name</dt>
    							<dd>{{$user->first_name}}</dd>
    							
    							<dt>Last name</dt>
    							<dd>{{$user->last_name}}</dd>
    							
    							<dt>Email</dt>
    							<dd>{{$user->email}}</dd>
    							
    							<dt>Postcode</dt>
    							<dd>{{$user->postcode}}</dd>
    
    							@if (sizeof($user->country))
    							<dt>Country</dt>
    							<dd>{{$user->country->name}}</dd>
    							@endif
    							
    							<dt>Telephone</dt>
    							<dd>{{$user->phone}}</dd>
    							
    							<dt>Mobile</dt>
    							<dd>{{$user->mobile}}</dd>
    							
    							@if(!empty($user->company))
    							<dt>Company</dt>
    							<dd>{{$user->company}}</dd>
    							@endif
    							
    							<dt>Membership PIN</dt>
    							<dd>{{$user->pin}}</dd>
    							
    							<dt>PFS Designation</dt>
    							<dd>{{\Auth::user()->pfs_class}}</dd>
    						</dl>
    						@endif
    						
    						<div class="alert alert-info" role="alert">
    		                	<h5>Amending your details</h5>
    		                    <p>Please note, that if you amend your profile details within the 2 day period before an event, we cannot guarantee that the venue will be aware of the change</p>
    		                </div>
    						
    					</div>
    					
    					
    					<div class="col-sm-6">
    						<div class="form-group">
    							<div class="col-sm-3">
    								{!! Form::label('profile_image', 'Profile image', ['class' => 'control-label']) !!}
    							</div>
    							<div class="col-sm-9">
    								{!! Form::hidden('profile_image', null, ['id'=>'logo']) !!}
    								<span id="logo_img">
    								@if(!empty($user->profile_image))
    									{!! HTML::image('uploads/profile_images/'.$user->profile_image,'', ['class'=>'img-thumbnail', 'width'=>'150', 'height' => '150']) !!}
    								@else
    									{!! HTML::image('images/no_image.jpg','', ['class'=>'img-thumbnail', 'width'=>'150', 'height' => '150']) !!}
    								@endif
    								</span>
    								<div id="logo_upload">Upload</div>
    								<div id="status"></div>
    							</div>
    						</div>
    						
    						<div class="form-group">
                                <div class="col-sm-3">
                                    {!! Form::label('badge_name', 'Badge bame', ['class' => 'control-label']) !!}
                                </div>
                                <div class="col-sm-9">
                                    {!! Form::text('badge_name', null, ['class'=>'form-control form-control-input']) !!}
                                </div>
                            </div>
    						
    						<div class="form-group">
    							<div class="col-sm-3" style="text-align:left">
    								{!! Form::label('dietary_requirement_id', 'Dietary Requirements', ['class' => 'control-label left']) !!}
    							</div>
    							<div class="col-sm-9">
    								{!! Form::select('dietary_requirement_id', $dietary_requirements, null, ['class'=>'form-control form-control-input select2-list',]) !!}
    							</div>
    						</div>
    						
    						<div class="form-group" id="dietary_other">
    							<div class="col-sm-3" >
    								{!! Form::label('dietary_requirement_other', 'Other', ['class' => 'control-label left']) !!}
    							</div>
    							<div class="col-sm-9">
    								{!! Form::text('dietary_requirement_other', null, ['class'=>'form-control form-control-input']) !!}
    							</div>
    						</div>
    						
    						<div class="form-group">
    							<div class="col-sm-3">
    								{!! Form::label('special_requirements', 'Special Requirements', ['class' => 'control-label left']) !!}
    							</div>
    							<div class="col-sm-9">
    								{!! Form::text('special_requirements', null, ['class'=>'form-control form-control-input']) !!}
    							</div>
    						</div>
    						
    						<div class="form-group">
    							<div class="col-sm-3">
    								{!! Form::label('bio', 'Bio', ['class' => 'control-label']) !!}
    							</div>
    							<div class="col-sm-9">
    								{!! Form::textarea('bio', null, ['class'=>'form-control form-control-input', 'rows'=>2]) !!}
    							</div>
    						</div>
    						
    						<div class="form-group">
    							<div class="col-sm-3">
    								{!! Form::label('job_title', 'Job title', ['class' => 'control-label']) !!}
    							</div>
    							<div class="col-sm-9">
    								{!! Form::text('job_title', null, ['class'=>'form-control form-control-input']) !!}
    							</div>
    						</div>
    						
    						<div class="form-group">
    							<div class="col-sm-3">
    								{!! Form::label('cc_email', 'CC email', ['class' => 'control-label']) !!}
    							</div>
    							<div class="col-sm-9">
    								{!! Form::input('email', 'cc_email', null, ['class'=>'form-control form-control-input']) !!}
    							</div>
    						</div>
    
    						<div class="form-group">
    							<div class="col-sm-3">
    								{!! Form::label('attendee_types', 'My role is mainly', ['class' => 'control-label']) !!}
    							</div>
    							<div class="col-sm-9">
    
    								@foreach($attendee_types as $key => $at)
                                        <?php
                                        $checked = (in_array($key, $user->attendeeTypes->lists('id'))) ? true : false;
                                        $hided = ($key <= 11) ? 'style="display:none;"' : '';
                                        ?>
    
    									<div {!!$hided!!} class="checkbox checkbox-styled">
    										<label>
    											{!! Form::checkbox('attendee_types[]', $key, $checked) !!}<span>{{$at}}</span>
    										</label>
    									</div>
    								@endforeach
    							</div>
    						</div>
    					</div>
    				</div>
    
    				<div class="row>">
    					<div class="form-group pull-right">
    						<br/>
    						{!! Form::submit('Update Profile', ['class' => 'btn boton']) !!}
    					</div>
    				</div>
    				
    				{!! Form::close() !!}
    			</div>
    		</div>
    	</div>
    </div>
</div>
<style>
	#password_change {display: none;}
</style>
<script type="text/javascript">

    $( "#toggle-pw" ).click(function() {
        $( "#password_change" ).toggle( "slow", function() {
            // Animation complete.
        });
    });

	if($('#dietary_requirement_id').val() != 13) $('#dietary_other').hide();
    
    $('#dietary_requirement_id').bind('change', function(event){
    	if(this.value == 13) {
    		$('#dietary_other').show();
    	} else {
    		$('#dietary_other').hide();
    	}
    });
    
	$(document).ready(function()
	{
	var settings = {
		url: "{!! route('profile.uploadLogo') !!}",
		formData: {  
			"_token": "{{ csrf_token() }}"
		},
		dragDrop:true,
		maxFileSize: 1500000,
		fileName: "myfile",
		allowedTypes:"jpg,png,gif,bmp,jpeg",	
		returnType:"json",
		 onSuccess:function(files,data,xhr)
	    {
	       //alert((data));
	       $('#logo').val(data);
	       $('#logo_img').html('<img src="{{\Config::get("app.url")}}uploads/profile_images/'+data+'" class="img-thumbnail" height="150" width="150" />');
	       
	       //location.reload();
	    },
	    showDelete:false
	}
	var uploadObj = $("#logo_upload").uploadFile(settings);
	
	});
</script>
@endsection