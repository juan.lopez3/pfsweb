<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<p>This is automatically generated notification for {{$event->title}} (ID: $event->id) event which will be held on {{$event->event_date_from}} in {{$event->region->title}}</p>
		
		<p><strong>Session {{$slot->title}}</strong> has reached notification level ({{$slot->notification_level}}%) and now has {{$slot->attendees()->whereIn('registration_status_id', [config('registrationstatus.booked'), config('registrationstatus.waitlisted')])->count()}} attendees that have booked the session.</p>
		<p><strong>Session capacity: </strong>{{$slot->slot_capacity}}</p> 
	</body>
</html>