<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
		<style>
			body {font-family: Arial, Helvetica, sans-serif}
		</style>
	</head>
	<body>
	    @if(sizeof($event) && sizeof($event->eventType) && !empty($event->eventType->email_branding))
	    <img src="<?php echo $message->embed(\Config::get('app.url').'uploads/branding/'.$event->eventType->email_branding); ?>" />
	    <p></p>
	    @else
	    
	    @endif
		
		{!!$main_content!!}
	</body>
</html>