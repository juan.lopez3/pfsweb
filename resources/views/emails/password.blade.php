<img src="<?php echo $message->embed(\Config::get('app.url').'images/email_templates/email_banner.png'); ?>" />
<br/>
Click on the link below to reset your events password: {{ url('password/reset/'.$token) }}
<br/><br/><br/><p></p>
Kind Regards,<br/>
Personal Finance Society Conference Office<br/>
0845 166 8415<br/>
regionals@pfsevents.org<br/>
www.thepfs.org