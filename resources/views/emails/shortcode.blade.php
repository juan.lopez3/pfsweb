<img src="<?php echo $message->embed(\Config::get('app.url').'images/email_templates/email_banner.png'); ?>" />
<br/>
<h4>Dear {{$user->first_name}},</h4>
You have requested one time short code for mobile authentication. Your automatically generated shortcode: {{$shortcode}}
<br/><br/><br/><p></p>
We look forward to welcoming you to the Conference.<br/>
Kind Regards,<br/>
Personal Finance Society Conference Office<br/>
0845 166 8415<br/>
regionals@pfsevents.org<br/>
www.thepfs.org