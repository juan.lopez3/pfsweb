<?php
/**
 * Testgateway configuration
 * https://axcessms.docs.oppwa.com
 */

$intesting = env('payment_intesting', true);

//testing cards; https://axcessms.docs.oppwa.com/reference/parameters#testing
$test_payment = [
    "url" => 'https://test.oppwa.com',
    "config" => [
        'authentication.userId' => '8a8294184e736012014e78c4c4e417e0',
        'authentication.password' => '4tJCmj2Bt3',
        'authentication.entityId' => '8a8294184e736012014e78c4c4cb17dc',
    ],

];

$live_payment = [
    "url" => 'https://oppwa.com',
    "config" => [
        'authentication.userId' => '8a8394c55b138ccd015b347792a51e71',
        'authentication.password' => 'eR8RX4KTRe',
        'authentication.entityId' => '8ac9a4cb692f24680169915856ff1c9b',
    ],

];

$environment = $intesting ? $test_payment : $live_payment;

return $environment;
