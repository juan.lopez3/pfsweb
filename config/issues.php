<?php

return [
    'new' => '1',
    'need-confirmation' => '2',
    'confirmed' => '3',
    'active' => '4',
    'completed' => '5',
    'closed' => '6',

    'options' => array(
        1 => 'New',
        2 => 'Need Confirmation',
        3 => 'Confirmed',
        4 => 'Active',
        5 => 'Completed',
        6 => 'Closed',
    ),

    'type' => array(
        0=>'Bug',
        1=>'Query',
        2=>'Development'
    ),

    'priority' => array(
        1=>'Low',
        2=>'Normal',
        3=>'High',
        4=>'Urgent'
    ),

    'admin_email' => array(
        'almantas@aelite.co.uk',
        'dan@aelite.co.uk',
        'lara@aelite.co.uk'
    ),
];