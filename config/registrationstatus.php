<?php

return [
	'booked' => 1,
	'cancelled' => 2,
	'invited' => 3,
	'waitlisted' => 4,
	'late_cancelled' => 5,
	'declined' => 6,
	'waiting_payment' => 7,
	'pending' => 8,
	'complete_manuall_review' => 9,
	'declined_by_bank' => 10,
	'rejections_reference_validation' => 11,
	'rejections_reference_validation' => 12,
	
	1	=> 'Booked',
	2	=> 'Cancelled',
	3	=> 'Invited',
	4	=> 'Waitlisted',
	5	=> 'Late Cancelled',
	6   => 'Declined',
	7   => 'Waiting Payment',
	8   => 'Pending',
	9   => 'Complete Manually Review',
	10  => 'Declined by Bank',
	11  => 'Rejections reference validation',
	12  => 'Rejections reference validation',

];