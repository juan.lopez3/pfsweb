<?php

return [

    'priorities' =>array(
        1 => 'Low',
        2 => 'Medium',
        3 => 'High' 
    ),
    
    'priority_colour' => array(
        1 => '#D3FFD9',
        2 => '#FFEAD3',
        3 => '#FFBFBF',
    )

];