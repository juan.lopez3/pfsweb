<?php

return array(
	'super_administrator' => 1,
	'event_manager' => 2,
	'event_coordinator' => 3,
	'member' => 4,
	'non_member' => 5,
	'pfs_staff' => 6,
	'venue_staff' => 7,
	'host' => 8,
	'uploaded_attendee' => 9,
	'regional_coordinator' => 10,
);

?>