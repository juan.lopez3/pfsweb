<?php

return [
	'registration_confirmation' => 1,
	'registration_amendment' => 2,
	'registration_cancel' => 6,
	'reminder_21' => 3,
	'reminder_5' => 4,
	'thankyou_event' => 5,
	'chairman_confirmation' => 7,
	'speaker_confirmation' => 8,
	'waitinglist' => 9,
	'waitlist_cancellation' => 10,
	'waitlist_confirmation' => 11,
	'invitation' => 12,
	'registration_confirmation_waitlist' => 13,
	'registration_late_cancel' => 14,
	'invitation_admin' => 15,
	'invitation_declined' => 16,
	'registration_confirmation_power_live' => 293,
];

?>