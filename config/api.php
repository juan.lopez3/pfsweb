<?php

return [
    'old' => array(
        'feedback_types' => array('Rating', 'Checkbox', 'Option box', 'Select box', 'Text')
    ),

    'v1' => array(
        'feedback_types' => array(1=>'Rating', 2=>'Checkbox', 3=>'Option box', 4=>'Select box', 5=>'Text')
    ),

];
