<?php
/**
 * 'Event'=>array(
			'AssocciatedModels' => array('model'=>'method'), # all related models
            'ForeignKeys' => array(
                'venue_id' => ['method', 'field']) to display foreign key value
			'IgnoreFields' => array('id'), //field names to ignore
			'FieldLabels' => array(
 * 				'title' => 'new_title', // rename field names
 * 			),
			'Conditions' => array(
				'event_type_id' => array('EventType', 'title', 'id'), // get options for condition
 * 
 * 				// if empty array will return yes/no options
			)
 * 
 */
return array(
	
	'Models' => array(
		'Event'=>array(
			'AssocciatedModels' => array('Venue'=>'venue'),
			'ForeignKeys'    => array(
                'venue_id' => ['venue', 'name'],
                'event_type_id' => ['eventType', 'title'],
                'region_id' => ['region', 'title'],
                'tfi_contact_id' => ['tfiContact', 'first_name']
            ),
			'IgnoreFields' => array('slug', 'notification_level', 'main_schedule_visible_web', 'main_schedule_visible_reg', 'registration_type_id', 'registration_text', 'description', 'banner', 'created_at', 'updated_at', 'bedrooms', 'details', 'image_1', 'image_2', 'comission', 'rating'),
			'FieldLabels' => array(
				'region_id' => 'region',
				'event_type_id' => 'event type',
				'venue_id' => 'venue name',
				'tfi_contact_id' => 'tfi contact'
				
			),
			'Conditions' => array(
				'event_type_id' => array('EventType', 'title', 'id'),
				'region_id' => array('Region', 'title', 'id'),
				'venue_id' => array('Venue', 'name', 'id'),
				'tfi_contact_id' => array('User', 'first_name', 'id'),
				'publish' => array(),
				
			)
		),
		
		
		'Venue'=>array(
			'AssocciatedModels' => array(),
			'ForeignKeys'    => array(
                'venue_staff_id' => ['contactPerson', 'first_name']
            ),
			'IgnoreFields' => array('image_1', 'image_2', 'created_at', 'updated_at'),
			'FieldLabels' => array(
				'venue_staff_id' => 'venue staff',
			),
			'Conditions' => array(
				'show_map' => array(),
				'venue_staff_id' => array('User', 'first_name', 'id'),
			)
		),
		
        
		'User'=>array(
			'AssocciatedModels' => array('UserRole' =>'role', 'DietaryRequirement' =>'dietaryRequirement' ),
			'ForeignKeys'    => array(
                'role_id' => ['role', 'title'],
                'dietary_requirement_id' => ['dietaryRequirement', 'title']
            ),
			'IgnoreFields' => array('password', 'bio', 'remember_token', 'profile_image'),
			'FieldLabels' => array(
				'role_id' => 'role'
			),
			'Conditions' => array(
				'role_id' => array('Role', 'title', 'id'),
				'dietary_requirement_id' => array('DietaryRequirement', 'title', 'id'),
				'chartered' => array()
			)
		),
		
		'Sponsor'=>array(
			'AssocciatedModels' => array(),
			'IgnoreFields' => array(),
			'FieldLabels' => array(),
			'Conditions' => array()
		),
		
		
		'Technology'=>array(
			'AssocciatedModels' => array(),
			'IgnoreFields' => array(),
			'FieldLabels' => array(),
			'Conditions' => array()
		),
		
		'EventAttendee'=>array(
			'AssocciatedModels' => array('User' => 'user', 'Event'=>'event'),
			'ForeignKeys'    => array(
                'role_id' => ['role', 'title'],
                'user_id' => ['user', 'first_name'],
                'registration_status_id' => ['registrationStatus', 'title'],
                'dietary_requirement_id' => ['dietaryRequirement', 'title'],
                'venue_id' => ['venue', 'name'],
                'event_id' => ['event', 'title'],
                'event_type_id' => ['eventType', 'title'],
                'region_id' => ['region', 'title'],
                'tfi_contact_id' => ['tfiContact', 'first_name']
            ),
			'IgnoreFields' => array('password', 'bio', 'remember_token', 'profile_image', 'slug', 'main_schedule_visible_web', 'main_schedule_visible_reg', 'registration_type_id', 'registration_text', 'description', 'banner'),
			'FieldLabels' => array(
			    'user_id'=>'attendee name', 'event_id'=>'event title', 'registration_status_id' => 'registration status',
			    'region_id' => 'region',
                'event_type_id' => 'event type',
                'venue_id' => 'venue name',
                'tfi_contact_id' => 'tfi contact',
                'role_id' => 'role'
            ),
			'Conditions' => array(
				'user_id' => array('User', 'first_name', 'id'),
				'event_id' => array('Event', 'title', 'id'),
				'registration_status_id' => array('RegistrationStatus', 'title', 'id'),
				'dietary_requirement_id' => array('DietaryRequirement', 'title', 'id'),
				'attended' => array(),
				'sponsors_contact' => array(),
				'paid' => array(),
				'role_id' => array('Role', 'title', 'id'),
                'dietary_requirement_id' => array('DietaryRequirement', 'title', 'id'),
                'chartered' => array(),
                'event_type_id' => array('EventType', 'title', 'id'),
                'region_id' => array('Region', 'title', 'id'),
                'venue_id' => array('Venue', 'name', 'id'),
                'tfi_contact_id' => array('User', 'first_name', 'id'),
                'publish' => array(),
                'onsite_booking' => array(),
			)
		),
		
		'UserFeedback'=>array(
			'AssocciatedModels' => array('Event' => 'event', 'User'=>'user'),
			'ForeignKeys'    => array(
                'role_id' => ['role', 'title'],
                'dietary_requirement_id' => ['dietaryRequirement', 'title'],
                'venue_id' => ['venue', 'name'],
                'event_type_id' => ['eventType', 'title'],
                'region_id' => ['region', 'title'],
                'tfi_contact_id' => ['tfiContact', 'first_name']
            ),
			'IgnoreFields' => array('password', 'bio', 'remember_token', 'profile_image', 'slug', 'notification_level', 'main_schedule_visible_web', 'main_schedule_visible_reg', 'registration_type_id', 'registration_text', 'description', 'banner'),
			'FieldLabels' => array(
				'user_id'=>'attendee name', 'event_id'=>'event title', 'registration_status_id' => 'registration status',
                'region_id' => 'region',
                'event_type_id' => 'event type',
                'venue_id' => 'venue name',
                'tfi_contact_id' => 'tfi contact',
                'role_id' => 'role'
			),
			'Conditions' => array(
				'user_id' => array('User', 'first_name', 'id'),
				'event_id' => array('Event', 'title', 'id'),
				'feedback_id' => array('EventFeedback', 'question', 'id'),
			)
		),
	)
);

?>