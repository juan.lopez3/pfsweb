<?php

// registration, event, user, settings, feedback

return array(
	'login' 		=> 'User loged in',
	'logout'		=> 'User loged out',
	
	'eventRegister'	=> 'Registered to event',
	'eventCancel'	=> 'Cancelled event registration',
	
	'actionCreate'	=> 'Created new',
	'actionUpdate'	=> 'Updated',
	'actionDelete'	=> 'Deleted',
	
	'emailSent'		=> 'Email sent.',
);

?>