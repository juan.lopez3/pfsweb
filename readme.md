# PFS WEB
The website will allow to the users know about PFS and its services.
The PFS will allow the registered users know more about the events. 

## Requirements
* MAMP, WAMP or LAMP
* PHP 5.6 or 7.0.
* Git
* [Composer](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx).
* Npm
* MySQL

## Development Environment
This project is developed over laravel 5.0.

## Setting up the Development Environment

##�Project Installation
* Clone this repository.
* Create directors and files ignored by git
```
vendor
node_modules
.env
public/upload
public/agendas
```
* Install these PHP extensions
```
sudo apt-get install php-mbstring
sudo apt-get install php-xml
sudo apt-get install php5.6
```
* Go to the folder project and run
```

composer install
npm install
```

* Download the database from the server of dev.mocionsoft.com/pfsold/tfi_data_155.sql
* Create the database for pfs but don't upload
* First, migrate with next function
* Go to the folder project and run
```
php artisan migrate
```
* Now, Restore the database

* create folder views in storage/framework
```
views
```

* It is required to give permissions to the folder
```
 sudo chmod 777 -Rf pfsweb/
```

* ignore git permissions
```
git config core.fileMode false
```

* place the following command
```
 sudo a2enmod rewrite
 sudo /etc/init.d/apache2 restart
``` 

* In .env write the following code.
```
APP_ENV=production
APP_DEBUG=true
APP_KEY=XjmALoRxp6ZlaAAMnXjgJGg03ZDZc6FL

DB_HOST=127.0.0.1
DB_DATABASE=pfs
DB_USERNAME=root
DB_PASSWORD=root

CACHE_DRIVER=file
SESSION_DRIVER=database
QUEUE_DRIVER=database

```

Tener en cuenta que para el funcionamiento de las rutas debe tener estos textos en la cofiguracion del apache y ademas tener a RewriteEngine en On si no esta hay que agregarlo.
```
<Directory "/var/www/html">
Allowoverride All
</Directory>

RewriteEngine On
```

activar el headers y reiniciar el servidor
```
sudo a2enmod headers
```

* Finally, run in laravel server
```
php artisan serve
```

#redising style
Se creo una hoja de estilos para el rediseño estilo.css.
ruta: public/assets/site/css/estilos.css.
En estilos.css se encuentran la gran mayoria de estilos, pero debido a que se reusaron muchos componentes del diseño anterior se modificaron los estilos de algunas clases ,es decir, hay estilos que que se estan usando en el actual diseño que pertenician al diseño anterior por tanto es posible que muchos estilos se modifiquen en distintas hojas de estilo.


